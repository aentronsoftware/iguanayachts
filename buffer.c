/**********************************************************************

   buffer.c - buffer for measure (from smtec)

 ----------------------------------------------------------------------
   Author: PTM: OS                   Date: 05.09.14
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM068
   descr.:
   Edit  :

 *********************************************************************/

#include "gatewaycfg.h"
#include "uc.h"
#include "hal.h"
#include "buffer.h"


// *** BUFFER ***
// Buffer pour les mesures analogiques
typedef struct {
  uint16 meas[MEASBUF_SIZE];
  uint16  count;
  uint16  index;
  uint32  sum;
  uint16  mean;
  }TMeasBuffer;


static TMeasBuffer MeasBuffer[MEAS_CHANNELS];
//static uint16 LastMeasure[MEAS_CHANNELS];


// *** BUFFER ***
void BUF_Init(int channel)
{
  MeasBuffer[channel].count = 0;
  MeasBuffer[channel].index = 0;
  MeasBuffer[channel].sum = 0;
}

uint16 BUF_Count(int channel){
  return(MeasBuffer[channel].count);
}

uint16 BUF_Index(int channel){
  return(MeasBuffer[channel].index);
}

void BUF_Write(int channel,uint16 val)
{
  uint16 index, index_n;
  uint16 n;
  uint32 sum;

  //LastMeasure[channel] = val;
  n = MeasBuffer[channel].count;
  index = MeasBuffer[channel].index;
  //index de la premiere mesure
  index_n = (index + n) & (MEASBUF_SIZE-1);
  if ( n >= MEASBUF_SIZE) {
    //buffer plein  on bouge index
    index = (index + 1) & (MEASBUF_SIZE-1);
  }
  sum = MeasBuffer[channel].sum;
  if(n>= MEASBUF_SIZE){
    sum -= MeasBuffer[channel].meas[index_n];
  }
  MeasBuffer[channel].meas[index_n] = val;
  sum += val;
  MeasBuffer[channel].sum = sum;
  if (n < MEASBUF_SIZE) n++;
  MeasBuffer[channel].index = index;
  MeasBuffer[channel].count = n;
/*
  if (n>0) sum =  MeasBuffer[channel].sum / n;
  else sum = 0;
*/
  MeasBuffer[channel].mean = sum;
}

uint32 BUF_Sum(int channel)
{
  return(MeasBuffer[channel].sum);
}

uint16 BUF_meas(int channel, uint16 index)
{
  return(MeasBuffer[channel].meas[index]);
}

uint16 BUF_Mean(int channel)
{
  uint16 n;
  uint16 res;
  n = MeasBuffer[channel].count;
  if (n>0) res =  MeasBuffer[channel].sum / n;
  else res = 0;
  return(res);
}
//ne fonctionne pas autour de 0 si valeur est sign�e.
uint32 BUF_SumLast(int channel,int nb,Bool filter){
  uint16 index, i;
  uint32 sum;
  uint16 min,max;
  uint16 meas;

  if(filter) { nb +=2;}
  index = (MeasBuffer[channel].index + MeasBuffer[channel].count - nb);
  sum = 0;
  for (i=0;i<nb ;i++) {
    index &= (MEASBUF_SIZE-1);
    meas = MeasBuffer[channel].meas[index];
    if (i==0) {
      min = meas;
      max = meas;
    }
    else{
      if (meas<min) {min = meas;}
      else if (meas>max) {max = meas;}
    }
    sum += MeasBuffer[channel].meas[index];
    index++;
  }
  if (filter) {
    sum -= min;
    sum -= max;
  }
  return(sum);
}

int32 BUF_Delta(int channel,int nb){
  uint16 index;
  int32 delta;

  if (nb > MeasBuffer[channel].count){
    delta = 0;
  }
  index = (MeasBuffer[channel].index + MeasBuffer[channel].count - 1);

  index &= (MEASBUF_SIZE-1);
  delta =  MeasBuffer[channel].meas[index];

  index -= nb;
  index &= (MEASBUF_SIZE-1);
  delta -= MeasBuffer[channel].meas[index];
  return(delta);
}


// *** fin BUFFER ***




