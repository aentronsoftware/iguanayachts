/*
 * i2c.h
 *
 *  Created on: 11 juin 2012
 *      Author: remip
 */

#ifndef I2C_H_
#define I2C_H_

/* serial EEPROM */
#define EEPROM_DEVICECODE       0xA0
#define EEPROM_SIZE             8192  // size in byte 24C08 = 8Kbit = 1KByte, 24C32 = 64Kbit = 8KByte
#define EEPROM_PAGESIZE           32  // ST page size

#define ADS1015_DEVICECODE      0x90 //1001000x ADDR Pin = GND

#define PCF2116_DEVICECODE      0x74 //01110100

#define DS1307_DEVICECODE       0xD0 //11010000

/*
 * I2C task defines
 */
#define I2C_READ          0
#define I2C_WRITE         1
#define I2C_WRITE_VERIFY  2

#define I2C_PENDING        0 //this state is set by the posting task as the initial value for untreated I2C message
#define I2C_PROCESSING     1 //this state indicates that the message is currently being processed and send to the bus
#define I2C_READ_OK       10 //this state indicates that the read operation succeeded
#define I2C_WRITE_OK      11 //indicates that the write operation succeeded
#define I2C_VERIFY_OK     12 //indicates that the write and verify operation succeeded
#define I2C_READ_FAILED   20 //indicated that the read operation failed
#define I2C_WRITE_FAILED  21 //indicated that the write operation failed
#define I2C_VERIFY_FAILED 22 //indicated that the write operation succeeded but the subsequent verify failed

/*
 * I2C command defines
 */
#define MEM_WRITE         0
#define MEM_READ          1
#define MEM_READ_RETRY    2
#define POW_READ          3
#define POW_WRITE         4
#define ADS_WRITE         5
#define ADS_READ          6
#define LCD_WRITE         7
#define LCD_READ          8
#define RTC_READ          9
#define RTC_WRITE        10


//timeout after what the mailbox_post will exit DO NOT PUT LESS THAN 2 because it is also used as sleep
#define I2C_POST_TIMEOUT    10
//timeout that the task will wait for the result to be available
#define I2C_RESULT_TIMEOUT 100
//struct of mailbox
typedef struct {
  uint8 devsel;   // the device address (Device Select)
  uint8 operation;// I2C_READ (0), I2C_WRITE (1), I2C_WRITE_VERIFY (2)
  uint8 length;   // the number of bytes to be read or written
  uint16 address; // the device's internal address to be read/written to
  uint8 *data;    // a pointer to the data (destination if read, source if write)
  uint8 *status;  // a pointer for returning the status of the I2C operation
}TI2C_mailbox;  //size in MAU : DSP:8, ARM:13

bool1 I2C_Command(uint16 cmd, char * data, uint16 length, uint16 address);
void  I2C_Init(void);

#endif /* I2C_H_ */
