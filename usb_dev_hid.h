#ifndef USB_STRUCT_H_
#define USB_STRUCT_H_


#define MESSAGE_CANOPEN_REPORT_ID 0
#define MESSAGE_CANOPEN_OUT_LEN 15
#define MESSAGE_CANOPEN_IN_LEN  15

//extern tUSBDHIDDevice* USB_HID_DevicePtr;

void USB_Init(void);
bool1 USB_Start(void);
void USB_Stop(void);
bool1 USB_Connected(void);
void USB_Tick(void);
void USB_Unlock(void);


#endif /*USB_STRUCT_H_*/
