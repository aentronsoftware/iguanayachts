;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Oct 28 12:13:01 2019                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Johnny\V31\NewCAN\MMS_F_AEC\Standard_AEC_160\AEC_Standard\PTM079_LV")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_LastTimerRaw+0,32
	.bits	-1,16			; _LastTimerRaw @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_TotalSleepTime+0,32
	.bits		0xffffffff,32
	.bits		0xffffffff,32			; _TotalSleepTime @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_timers+0,32
	.bits	0,16			; _timers[0]._state @ 0
	.space	16
	.bits	0,32			; _timers[0]._d @ 32
	.bits	0,32			; _timers[0]._callback @ 64
	.bits	0,32			; _timers[0]._id @ 96
	.bits		0,32
	.bits		0,32			; _timers[0]._val @ 128
	.bits		0,32
	.bits		0,32			; _timers[0]._interval @ 192
	.bits	0,16			; _timers[1]._state @ 256
	.space	16
	.bits	0,32			; _timers[1]._d @ 288
	.bits	0,32			; _timers[1]._callback @ 320
	.bits	0,32			; _timers[1]._id @ 352
	.bits		0,32
	.bits		0,32			; _timers[1]._val @ 384
	.bits		0,32
	.bits		0,32			; _timers[1]._interval @ 448
	.bits	0,16			; _timers[2]._state @ 512
	.space	16
	.bits	0,32			; _timers[2]._d @ 544
	.bits	0,32			; _timers[2]._callback @ 576
	.bits	0,32			; _timers[2]._id @ 608
	.bits		0,32
	.bits		0,32			; _timers[2]._val @ 640
	.bits		0,32
	.bits		0,32			; _timers[2]._interval @ 704
	.bits	0,16			; _timers[3]._state @ 768
	.space	16
	.bits	0,32			; _timers[3]._d @ 800
	.bits	0,32			; _timers[3]._callback @ 832
	.bits	0,32			; _timers[3]._id @ 864
	.bits		0,32
	.bits		0,32			; _timers[3]._val @ 896
	.bits		0,32
	.bits		0,32			; _timers[3]._interval @ 960
	.bits	0,16			; _timers[4]._state @ 1024
	.space	16
	.bits	0,32			; _timers[4]._d @ 1056
	.bits	0,32			; _timers[4]._callback @ 1088
	.bits	0,32			; _timers[4]._id @ 1120
	.bits		0,32
	.bits		0,32			; _timers[4]._val @ 1152
	.bits		0,32
	.bits		0,32			; _timers[4]._interval @ 1216
	.bits	0,16			; _timers[5]._state @ 1280
	.space	16
	.bits	0,32			; _timers[5]._d @ 1312
	.bits	0,32			; _timers[5]._callback @ 1344
	.bits	0,32			; _timers[5]._id @ 1376
	.bits		0,32
	.bits		0,32			; _timers[5]._val @ 1408
	.bits		0,32
	.bits		0,32			; _timers[5]._interval @ 1472
	.bits	0,16			; _timers[6]._state @ 1536
	.space	16
	.bits	0,32			; _timers[6]._d @ 1568
	.bits	0,32			; _timers[6]._callback @ 1600
	.bits	0,32			; _timers[6]._id @ 1632
	.bits		0,32
	.bits		0,32			; _timers[6]._val @ 1664
	.bits		0,32
	.bits		0,32			; _timers[6]._interval @ 1728
	.bits	0,16			; _timers[7]._state @ 1792
	.space	16
	.bits	0,32			; _timers[7]._d @ 1824
	.bits	0,32			; _timers[7]._callback @ 1856
	.bits	0,32			; _timers[7]._id @ 1888
	.bits		0,32
	.bits		0,32			; _timers[7]._val @ 1920
	.bits		0,32
	.bits		0,32			; _timers[7]._interval @ 1984
$C$IR_1:	.set	128


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("setTimer2")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_setTimer2")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$15)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("setTimer")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_setTimer")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$15)
	.dwendtag $C$DW$3

	.global	_LastTimerRaw
_LastTimerRaw:	.usect	".ebss",1,1,0
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("LastTimerRaw")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_LastTimerRaw")
	.dwattr $C$DW$5, DW_AT_location[DW_OP_addr _LastTimerRaw]
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$5, DW_AT_external
	.global	_TotalSleepTime
_TotalSleepTime:	.usect	".ebss",4,1,1
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("TotalSleepTime")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_TotalSleepTime")
	.dwattr $C$DW$6, DW_AT_location[DW_OP_addr _TotalSleepTime]
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("getElapsedTime")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_getElapsedTime")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
	.global	_timers
_timers:	.usect	".ebss",128,1,1
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("timers")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_timers")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_addr _timers]
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$8, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1494812 
	.sect	".text"
	.global	_SetAlarm

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("SetAlarm")
	.dwattr $C$DW$9, DW_AT_low_pc(_SetAlarm)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_SetAlarm")
	.dwattr $C$DW$9, DW_AT_external
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$9, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x41)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x0e)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 66,column 1,is_stmt,address _SetAlarm

	.dwfde $C$DW$CIE, _SetAlarm
$C$DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg12]
$C$DW$11	.dwtag  DW_TAG_formal_parameter, DW_AT_name("id")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_id")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -28]
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("callback")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_callback")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg14]
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("value")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg0]
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("period")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_period")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -32]

;***************************************************************
;* FNAME: _SetAlarm                     FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter, 20 Auto,  0 SOE     *
;***************************************************************

_SetAlarm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -6]
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("callback")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_callback")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -8]
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("value")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -12]
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("row_number")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_row_number")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -13]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("row")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_row")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -16]
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("real_timer_value")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_real_timer_value")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -20]
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("elapsed_time")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_elapsed_time")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_breg20 -24]
        MOVL      *-SP[8],XAR5          ; [CPU_] |66| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |66| 
        MOVL      *-SP[12],P            ; [CPU_] |66| 
        MOVL      *-SP[10],ACC          ; [CPU_] |66| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 73,column 7,is_stmt
        MOVL      XAR4,#_timers         ; [CPU_U] |73| 
        MOV       *-SP[13],#0           ; [CPU_] |73| 
        MOVL      *-SP[16],XAR4         ; [CPU_] |73| 
        B         $C$L7,UNC             ; [CPU_] |73| 
        ; branch occurs ; [] |73| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 75,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |75| 
        BF        $C$L6,EQ              ; [CPU_] |75| 
        ; branchcc occurs ; [] |75| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |75| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |75| 
        BF        $C$L6,NEQ             ; [CPU_] |75| 
        ; branchcc occurs ; [] |75| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 78,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |78| 
        MOVB      *+XAR4[0],#4,UNC      ; [CPU_] |78| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 79,column 7,is_stmt
        MOV       AL,@_LastTimerRaw     ; [CPU_] |79| 
        ADDB      AL,#1                 ; [CPU_] |79| 
        CMP       AL,*-SP[13]           ; [CPU_] |79| 
        BF        $C$L2,NEQ             ; [CPU_] |79| 
        ; branchcc occurs ; [] |79| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 79,column 43,is_stmt
        INC       @_LastTimerRaw        ; [CPU_] |79| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 81,column 7,is_stmt
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_getElapsedTime")
	.dwattr $C$DW$22, DW_AT_TI_call
        LCR       #_getElapsedTime      ; [CPU_] |81| 
        ; call occurs [#_getElapsedTime] ; [] |81| 
        MOVL      *-SP[24],P            ; [CPU_] |81| 
        MOVL      *-SP[22],ACC          ; [CPU_] |81| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 83,column 7,is_stmt
        MOVL      XAR6,*-SP[10]         ; [CPU_] |83| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |83| 
        MOVL      *-SP[20],ACC          ; [CPU_] |83| 
        MOVL      *-SP[18],XAR6         ; [CPU_] |83| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 84,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |84| 
        SUBB      ACC,#1                ; [CPU_] |84| 
        MOVL      *-SP[4],ACC           ; [CPU_] |84| 
        MOVL      *-SP[2],ACC           ; [CPU_] |84| 
        MOVL      P,*-SP[20]            ; [CPU_] |84| 
        MOVL      ACC,*-SP[18]          ; [CPU_] |84| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$23, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |84| 
        ; call occurs [#ULL$$CMP] ; [] |84| 
        CMPB      AL,#0                 ; [CPU_] |84| 
        B         $C$L3,GEQ             ; [CPU_] |84| 
        ; branchcc occurs ; [] |84| 
        MOVL      XAR6,*-SP[20]         ; [CPU_] |84| 
        MOVL      ACC,*-SP[18]          ; [CPU_] |84| 
        B         $C$L4,UNC             ; [CPU_] |84| 
        ; branch occurs ; [] |84| 
$C$L3:    
        MOVB      ACC,#0                ; [CPU_] |84| 
        SUBB      ACC,#1                ; [CPU_] |84| 
        MOVL      XAR6,ACC              ; [CPU_] |84| 
$C$L4:    
        MOVL      *-SP[20],XAR6         ; [CPU_] |84| 
        MOVL      *-SP[18],ACC          ; [CPU_] |84| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 86,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |86| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |86| 
        MOVL      *+XAR4[4],ACC         ; [CPU_] |86| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 87,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |87| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |87| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |87| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 88,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |88| 
        MOVL      ACC,*-SP[28]          ; [CPU_] |88| 
        MOVL      *+XAR4[6],ACC         ; [CPU_] |88| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 89,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |89| 
        MOVL      P,*-SP[24]            ; [CPU_] |89| 
        MOVL      ACC,*-SP[22]          ; [CPU_] |89| 
        ADDUL     P,*-SP[12]            ; [CPU_] |89| 
        ADDB      XAR4,#8               ; [CPU_] |89| 
        ADDCL     ACC,*-SP[10]          ; [CPU_] |89| 
        MOVL      *+XAR4[0],P           ; [CPU_] |89| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |89| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 90,column 7,is_stmt
        MOVL      XAR5,*-SP[16]         ; [CPU_] |90| 
        MOVZ      AR4,SP                ; [CPU_U] |90| 
        SUBB      XAR4,#32              ; [CPU_U] |90| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |90| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |90| 
        ADDB      XAR5,#12              ; [CPU_] |90| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |90| 
        MOVL      *+XAR5[2],XAR6        ; [CPU_] |90| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 91,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |91| 
        MOVB      *+XAR4[0],#1,UNC      ; [CPU_] |91| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 92,column 7,is_stmt
        MOVW      DP,#_TotalSleepTime   ; [CPU_U] 
        MOVL      XAR6,*-SP[20]         ; [CPU_] |92| 
        MOVL      XAR7,*-SP[18]         ; [CPU_] |92| 
        MOVL      P,@_TotalSleepTime    ; [CPU_] |92| 
        MOVL      ACC,@_TotalSleepTime+2 ; [CPU_] |92| 
        MOVL      *-SP[4],XAR6          ; [CPU_] |92| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |92| 
        SUBUL     P,*-SP[24]            ; [CPU_] |92| 
        SUBBL     ACC,*-SP[22]          ; [CPU_] |92| 
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$24, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |92| 
        ; call occurs [#ULL$$CMP] ; [] |92| 
        CMPB      AL,#0                 ; [CPU_] |92| 
        B         $C$L5,LEQ             ; [CPU_] |92| 
        ; branchcc occurs ; [] |92| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 94,column 9,is_stmt
        MOVL      P,*-SP[20]            ; [CPU_] |94| 
        MOVL      ACC,*-SP[18]          ; [CPU_] |94| 
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_setTimer")
	.dwattr $C$DW$25, DW_AT_TI_call
        LCR       #_setTimer            ; [CPU_] |94| 
        ; call occurs [#_setTimer] ; [] |94| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 97,column 7,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |97| 
        B         $C$L9,UNC             ; [CPU_] |97| 
        ; branch occurs ; [] |97| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 73,column 94,is_stmt
        MOVB      ACC,#16               ; [CPU_] |73| 
        INC       *-SP[13]              ; [CPU_] |73| 
        ADDL      *-SP[16],ACC          ; [CPU_] |73| 
$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 73,column 33,is_stmt
        MOVW      DP,#_LastTimerRaw     ; [CPU_U] 
        MOV       AL,@_LastTimerRaw     ; [CPU_] |73| 
        ADDB      AL,#1                 ; [CPU_] |73| 
        CMP       AL,*-SP[13]           ; [CPU_] |73| 
        B         $C$L8,LT              ; [CPU_] |73| 
        ; branchcc occurs ; [] |73| 
        MOV       AL,*-SP[13]           ; [CPU_] |73| 
        CMPB      AL,#8                 ; [CPU_] |73| 
        B         $C$L1,LT              ; [CPU_] |73| 
        ; branchcc occurs ; [] |73| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 101,column 3,is_stmt
        MOV       AL,#-1                ; [CPU_] |101| 
$C$L9:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 102,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$9, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0x66)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text"
	.global	_DelAlarm

$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("DelAlarm")
	.dwattr $C$DW$27, DW_AT_low_pc(_DelAlarm)
	.dwattr $C$DW$27, DW_AT_high_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_DelAlarm")
	.dwattr $C$DW$27, DW_AT_external
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$27, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$27, DW_AT_TI_begin_line(0x6f)
	.dwattr $C$DW$27, DW_AT_TI_begin_column(0x0e)
	.dwattr $C$DW$27, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 112,column 1,is_stmt,address _DelAlarm

	.dwfde $C$DW$CIE, _DelAlarm
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("handle")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_handle")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DelAlarm                     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_DelAlarm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("handle")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_handle")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |112| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 115,column 3,is_stmt
        CMP       AL,#-1                ; [CPU_] |115| 
        BF        $C$L11,EQ             ; [CPU_] |115| 
        ; branchcc occurs ; [] |115| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 117,column 5,is_stmt
        MOVW      DP,#_LastTimerRaw     ; [CPU_U] 
        MOV       AL,@_LastTimerRaw     ; [CPU_] |117| 
        CMP       AL,*-SP[1]            ; [CPU_] |117| 
        BF        $C$L10,NEQ            ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 118,column 7,is_stmt
        DEC       @_LastTimerRaw        ; [CPU_] |118| 
$C$L10:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 119,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_timers         ; [CPU_U] |119| 
        MOV       ACC,*-SP[1] << 4      ; [CPU_] |119| 
        ADDL      XAR4,ACC              ; [CPU_] |119| 
        MOV       *+XAR4[0],#0          ; [CPU_] |119| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 121,column 3,is_stmt
        MOV       AL,#-1                ; [CPU_] |121| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 122,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$27, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$27, DW_AT_TI_end_line(0x7a)
	.dwattr $C$DW$27, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$27

	.sect	".text"
	.global	_TimeDispatch

$C$DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("TimeDispatch")
	.dwattr $C$DW$31, DW_AT_low_pc(_TimeDispatch)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_TimeDispatch")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0x80)
	.dwattr $C$DW$31, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 129,column 1,is_stmt,address _TimeDispatch

	.dwfde $C$DW$CIE, _TimeDispatch

;***************************************************************
;* FNAME: _TimeDispatch                 FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_TimeDispatch:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -5]
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("next_wakeup")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_next_wakeup")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -10]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("elapsed_time")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_elapsed_time")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -14]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("row")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_row")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 131,column 23,is_stmt
        MOVB      ACC,#0                ; [CPU_] |131| 
        SUBB      ACC,#1                ; [CPU_] |131| 
        MOVL      *-SP[10],ACC          ; [CPU_] |131| 
        MOVL      *-SP[8],ACC           ; [CPU_] |131| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 134,column 24,is_stmt
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("_getElapsedTime")
	.dwattr $C$DW$36, DW_AT_TI_call
        LCR       #_getElapsedTime      ; [CPU_] |134| 
        ; call occurs [#_getElapsedTime] ; [] |134| 
        MOVL      *-SP[14],P            ; [CPU_] |134| 
        MOVL      *-SP[12],ACC          ; [CPU_] |134| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 138,column 7,is_stmt
        MOVL      XAR4,#_timers         ; [CPU_U] |138| 
        MOV       *-SP[5],#0            ; [CPU_] |138| 
        MOVL      *-SP[16],XAR4         ; [CPU_] |138| 
        B         $C$L16,UNC            ; [CPU_] |138| 
        ; branch occurs ; [] |138| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 140,column 5,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |140| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |140| 
        BF        $C$L15,NTC            ; [CPU_] |140| 
        ; branchcc occurs ; [] |140| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 142,column 7,is_stmt
        MOVL      XAR6,*-SP[12]         ; [CPU_] |142| 
        MOVL      ACC,*-SP[14]          ; [CPU_] |142| 
        MOVL      *-SP[4],ACC           ; [CPU_] |142| 
        MOVL      *-SP[2],XAR6          ; [CPU_] |142| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |142| 
        ADDB      XAR4,#8               ; [CPU_] |142| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |142| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |142| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$37, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |142| 
        ; call occurs [#ULL$$CMP] ; [] |142| 
        CMPB      AL,#0                 ; [CPU_] |142| 
        B         $C$L14,GT             ; [CPU_] |142| 
        ; branchcc occurs ; [] |142| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 144,column 9,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |144| 
        CMP64     ACC:P                 ; [CPU_] |144| 
        ADDB      XAR4,#12              ; [CPU_] |144| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |144| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |144| 
        CMP64     ACC:P                 ; [CPU_] |144| 
        BF        $C$L13,NEQ            ; [CPU_] |144| 
        ; branchcc occurs ; [] |144| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 146,column 11,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |146| 
        MOVB      *+XAR4[0],#2,UNC      ; [CPU_] |146| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 147,column 9,is_stmt
        B         $C$L15,UNC            ; [CPU_] |147| 
        ; branch occurs ; [] |147| 
$C$L13:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 151,column 11,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |151| 
        MOVL      P,@_TotalSleepTime    ; [CPU_] |151| 
        MOVL      ACC,@_TotalSleepTime+2 ; [CPU_] |151| 
        ADDB      XAR4,#12              ; [CPU_] |151| 
        ADDUL     P,*+XAR4[0]           ; [CPU_] |151| 
        ADDCL     ACC,*+XAR4[2]         ; [CPU_] |151| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |151| 
        SUBUL     P,*-SP[14]            ; [CPU_] |151| 
        SUBBL     ACC,*-SP[12]          ; [CPU_] |151| 
        ADDB      XAR4,#8               ; [CPU_] |151| 
        MOVL      *+XAR4[0],P           ; [CPU_] |151| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |151| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 152,column 11,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |152| 
        MOVB      *+XAR4[0],#3,UNC      ; [CPU_] |152| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 154,column 11,is_stmt
        MOVL      XAR6,*-SP[8]          ; [CPU_] |154| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |154| 
        MOVL      *-SP[4],ACC           ; [CPU_] |154| 
        MOVL      *-SP[2],XAR6          ; [CPU_] |154| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |154| 
        ADDB      XAR4,#8               ; [CPU_] |154| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |154| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |154| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$38, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |154| 
        ; call occurs [#ULL$$CMP] ; [] |154| 
        CMPB      AL,#0                 ; [CPU_] |154| 
        B         $C$L15,GEQ            ; [CPU_] |154| 
        ; branchcc occurs ; [] |154| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 155,column 13,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |155| 
        ADDB      XAR4,#8               ; [CPU_] |155| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |155| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |155| 
        MOVL      *-SP[10],ACC          ; [CPU_] |155| 
        MOVL      *-SP[8],XAR6          ; [CPU_] |155| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 157,column 7,is_stmt
        B         $C$L15,UNC            ; [CPU_] |157| 
        ; branch occurs ; [] |157| 
$C$L14:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 161,column 9,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |161| 
        ADDB      XAR4,#8               ; [CPU_] |161| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |161| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |161| 
        SUBUL     P,*-SP[14]            ; [CPU_] |161| 
        SUBBL     ACC,*-SP[12]          ; [CPU_] |161| 
        MOVL      *+XAR4[0],P           ; [CPU_] |161| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |161| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 164,column 9,is_stmt
        MOVL      XAR6,*-SP[8]          ; [CPU_] |164| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |164| 
        MOVL      *-SP[4],ACC           ; [CPU_] |164| 
        MOVL      *-SP[2],XAR6          ; [CPU_] |164| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |164| 
        ADDB      XAR4,#8               ; [CPU_] |164| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |164| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |164| 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$39, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |164| 
        ; call occurs [#ULL$$CMP] ; [] |164| 
        CMPB      AL,#0                 ; [CPU_] |164| 
        B         $C$L15,GEQ            ; [CPU_] |164| 
        ; branchcc occurs ; [] |164| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 165,column 11,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |165| 
        ADDB      XAR4,#8               ; [CPU_] |165| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |165| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |165| 
        MOVL      *-SP[10],ACC          ; [CPU_] |165| 
        MOVL      *-SP[8],XAR6          ; [CPU_] |165| 
$C$L15:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 138,column 45,is_stmt
        MOVB      ACC,#16               ; [CPU_] |138| 
        INC       *-SP[5]               ; [CPU_] |138| 
        ADDL      *-SP[16],ACC          ; [CPU_] |138| 
$C$L16:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 138,column 26,is_stmt
        MOVW      DP,#_LastTimerRaw     ; [CPU_U] 
        MOV       AL,@_LastTimerRaw     ; [CPU_] |138| 
        CMP       AL,*-SP[5]            ; [CPU_] |138| 
        B         $C$L12,GEQ            ; [CPU_] |138| 
        ; branchcc occurs ; [] |138| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 171,column 3,is_stmt
        MOVL      XAR6,*-SP[8]          ; [CPU_] |171| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |171| 
        MOVL      @_TotalSleepTime,ACC  ; [CPU_] |171| 
        MOVL      @_TotalSleepTime+2,XAR6 ; [CPU_] |171| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 174,column 3,is_stmt
        MOVL      P,*-SP[10]            ; [CPU_] |174| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |174| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("_setTimer2")
	.dwattr $C$DW$40, DW_AT_TI_call
        LCR       #_setTimer2           ; [CPU_] |174| 
        ; call occurs [#_setTimer2] ; [] |174| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 177,column 7,is_stmt
        MOVL      XAR4,#_timers         ; [CPU_U] |177| 
        MOV       *-SP[5],#0            ; [CPU_] |177| 
        MOVL      *-SP[16],XAR4         ; [CPU_] |177| 
        B         $C$L19,UNC            ; [CPU_] |177| 
        ; branch occurs ; [] |177| 
$C$L17:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 179,column 5,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |179| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |179| 
        BF        $C$L18,NTC            ; [CPU_] |179| 
        ; branchcc occurs ; [] |179| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 181,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |181| 
        AND       *+XAR4[0],#0xfffd     ; [CPU_] |181| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 182,column 7,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |182| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |182| 
        BF        $C$L18,EQ             ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 183,column 9,is_stmt
        MOVL      XAR6,*-SP[16]         ; [CPU_] |183| 
        MOVL      XAR5,*-SP[16]         ; [CPU_] |183| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |183| 
        MOVL      XAR7,*+XAR6[4]        ; [CPU_] |183| 
        MOVL      ACC,*+XAR5[6]         ; [CPU_] |183| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |183| 
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_call
	.dwattr $C$DW$41, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |183| 
        ; call occurs [XAR7] ; [] |183| 
$C$L18:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 177,column 43,is_stmt
        MOVB      ACC,#16               ; [CPU_] |177| 
        INC       *-SP[5]               ; [CPU_] |177| 
        ADDL      *-SP[16],ACC          ; [CPU_] |177| 
$C$L19:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 177,column 26,is_stmt
        MOVW      DP,#_LastTimerRaw     ; [CPU_U] 
        MOV       AL,@_LastTimerRaw     ; [CPU_] |177| 
        CMP       AL,*-SP[5]            ; [CPU_] |177| 
        B         $C$L17,GEQ            ; [CPU_] |177| 
        ; branchcc occurs ; [] |177| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c",line 186,column 1,is_stmt
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$31, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/timer.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0xba)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$31

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_setTimer2
	.global	_setTimer
	.global	_getElapsedTime
	.global	ULL$$CMP

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$43, DW_AT_name("cob_id")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$44, DW_AT_name("rtr")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_name("len")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$46, DW_AT_name("data")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$88	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$47, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$48, DW_AT_name("csSDO")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$49, DW_AT_name("csEmergency")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$50, DW_AT_name("csSYNC")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$51, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$52, DW_AT_name("csPDO")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$53, DW_AT_name("csLSS")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$54, DW_AT_name("errCode")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$55	.dwtag  DW_TAG_member
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$55, DW_AT_name("errRegMask")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$55, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$55, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$56, DW_AT_name("active")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$79	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)

$C$DW$T$80	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x18)
$C$DW$57	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$57, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$80


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$58, DW_AT_name("index")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$59, DW_AT_name("subindex")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$60, DW_AT_name("size")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$61	.dwtag  DW_TAG_member
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$61, DW_AT_name("address")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$85	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)

$C$DW$T$48	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$47)
	.dwendtag $C$DW$T$48

$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$61	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$47)
$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$81	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$47)
$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$6)
$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$9)
$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$81

$C$DW$T$82	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x16)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$69	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$47)
$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("TimerCallback_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$71	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$71, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$25	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$25, DW_AT_address_class(0x16)
$C$DW$72	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$6)
$C$DW$T$36	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$72)
$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x16)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$73	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$9)
$C$DW$T$34	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$73)
$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x16)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$38	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$74	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
$C$DW$75	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$38

$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)

$C$DW$T$65	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$76	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$47)
$C$DW$77	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$28)
$C$DW$78	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
$C$DW$79	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$65

$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)
$C$DW$T$67	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$80	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$67)
$C$DW$T$68	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$80)
$C$DW$T$69	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x16)
$C$DW$T$70	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_address_class(0x16)

$C$DW$T$74	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$47)
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$9)
$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$74

$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$77	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$77, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$77, DW_AT_byte_size(0x01)
$C$DW$84	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$85	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$77

$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)

$C$DW$T$43	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$43, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x01)
$C$DW$86	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$87	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$88	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$89	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$90	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$91	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$92	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$93	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$43

$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)

$C$DW$T$60	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x80)
$C$DW$94	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$94, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$60


$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x06)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$95, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$96, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$97, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$97, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$98, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$99, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$100, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$101	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$31)
$C$DW$T$32	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$101)
$C$DW$T$33	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_address_class(0x16)

$C$DW$T$87	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$87, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x132)
$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$102, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$103, DW_AT_name("objdict")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$104, DW_AT_name("PDO_status")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$105, DW_AT_name("firstIndex")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$106, DW_AT_name("lastIndex")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$107, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$108, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$109, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$110, DW_AT_name("transfers")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$111, DW_AT_name("nodeState")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$112, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$113, DW_AT_name("initialisation")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$114, DW_AT_name("preOperational")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$115, DW_AT_name("operational")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$116, DW_AT_name("stopped")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$117, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$118, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$119, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$120, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$121, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$122, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$123, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$124, DW_AT_name("heartbeatError")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$125, DW_AT_name("NMTable")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$126, DW_AT_name("syncTimer")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$127, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$128, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$129, DW_AT_name("pre_sync")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$130, DW_AT_name("post_TPDO")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$131, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_name("toggle")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$133, DW_AT_name("canHandle")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$134, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$135, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$136, DW_AT_name("globalCallback")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$137, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$138, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$139, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$140, DW_AT_name("dcf_request")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$141, DW_AT_name("error_state")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$142, DW_AT_name("error_history_size")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$143, DW_AT_name("error_number")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$144, DW_AT_name("error_first_element")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$145, DW_AT_name("error_register")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$146, DW_AT_name("error_cobid")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$147, DW_AT_name("error_data")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$148, DW_AT_name("post_emcy")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$149, DW_AT_name("lss_transfer")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$150, DW_AT_name("eeprom_index")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$151, DW_AT_name("eeprom_size")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$87

$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$89	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$89, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x0e)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$152, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$153, DW_AT_name("event_timer")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$154, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$155, DW_AT_name("last_message")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$89

$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$30	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x16)

$C$DW$T$93	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$93, DW_AT_name("struct_s_timer_entry")
	.dwattr $C$DW$T$93, DW_AT_byte_size(0x10)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$156, DW_AT_name("state")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$157, DW_AT_name("d")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$158, DW_AT_name("callback")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_callback")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$159, DW_AT_name("id")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_id")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$160, DW_AT_name("val")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$161, DW_AT_name("interval")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_interval")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$93

$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("s_timer_entry")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)

$C$DW$T$109	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x80)
$C$DW$162	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$162, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$109

$C$DW$T$110	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$110, DW_AT_address_class(0x16)

$C$DW$T$95	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$95, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$95, DW_AT_byte_size(0x14)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$163, DW_AT_name("nodeId")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$164, DW_AT_name("whoami")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$165, DW_AT_name("state")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$166, DW_AT_name("toggle")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$167, DW_AT_name("abortCode")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$168, DW_AT_name("index")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$169, DW_AT_name("subIndex")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$170, DW_AT_name("port")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$171, DW_AT_name("count")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$172, DW_AT_name("offset")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$173, DW_AT_name("datap")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$174, DW_AT_name("dataType")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$175, DW_AT_name("timer")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$176, DW_AT_name("Callback")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$95

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)

$C$DW$T$42	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x3c)
$C$DW$177	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$177, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$42


$C$DW$T$99	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$99, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x04)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$178, DW_AT_name("pSubindex")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$179, DW_AT_name("bSubCount")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$180, DW_AT_name("index")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$99

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$181	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$26)
$C$DW$T$27	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$181)
$C$DW$T$28	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x16)

$C$DW$T$71	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$182	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$9)
$C$DW$183	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$54)
$C$DW$184	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$70)
	.dwendtag $C$DW$T$71

$C$DW$T$72	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_address_class(0x16)
$C$DW$T$73	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$100, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x08)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$185, DW_AT_name("bAccessType")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$186, DW_AT_name("bDataType")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$187, DW_AT_name("size")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$188, DW_AT_name("pObject")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$189, DW_AT_name("bProcessor")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$100

$C$DW$190	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$100)
$C$DW$T$96	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$190)
$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)
$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_reg0]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_reg1]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_reg2]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg3]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg20]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_reg21]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_reg22]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg23]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg24]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg25]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg26]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg28]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_reg29]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg30]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg31]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x20]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_regx 0x21]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_regx 0x22]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_regx 0x23]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_regx 0x24]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_regx 0x25]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_regx 0x26]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg4]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_reg6]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_reg8]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg10]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg12]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg14]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg16]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg17]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg18]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg19]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg5]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_reg7]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg9]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg11]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg13]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_reg15]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_regx 0x30]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_regx 0x33]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_regx 0x34]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_regx 0x37]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x38]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x40]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x43]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x44]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x47]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x48]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x49]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x27]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x28]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_reg27]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

