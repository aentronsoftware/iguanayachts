;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Oct 28 12:13:00 2019                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Johnny\V31\NewCAN\MMS_F_AEC\Standard_AEC_160\AEC_Standard\PTM079_LV")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$66)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$120)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$121)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$34)
	.dwendtag $C$DW$4


$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("getNodeId")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_getNodeId")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$8


$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$66)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$63)
	.dwendtag $C$DW$10

$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("usb_tx_mbox")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_usb_tx_mbox")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0120013 
	.sect	".text"
	.global	_proceedNMTstateChange

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedNMTstateChange")
	.dwattr $C$DW$15, DW_AT_low_pc(_proceedNMTstateChange)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_proceedNMTstateChange")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x2a)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 43,column 1,is_stmt,address _proceedNMTstateChange

	.dwfde $C$DW$CIE, _proceedNMTstateChange
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg12]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("m")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _proceedNMTstateChange        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_proceedNMTstateChange:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -2]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR5          ; [CPU_] |43| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |43| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 44,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |44| 
        MOVB      XAR0,#76              ; [CPU_] |44| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |44| 
        CMPB      AL,#127               ; [CPU_] |44| 
        BF        $C$L1,EQ              ; [CPU_] |44| 
        ; branchcc occurs ; [] |44| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |44| 
        MOVB      XAR0,#76              ; [CPU_] |44| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |44| 
        CMPB      AL,#5                 ; [CPU_] |44| 
        BF        $C$L1,EQ              ; [CPU_] |44| 
        ; branchcc occurs ; [] |44| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |44| 
        MOVB      XAR0,#76              ; [CPU_] |44| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |44| 
        CMPB      AL,#4                 ; [CPU_] |44| 
        BF        $C$L14,NEQ            ; [CPU_] |44| 
        ; branchcc occurs ; [] |44| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 53,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |53| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |53| 
        BF        $C$L12,EQ             ; [CPU_] |53| 
        ; branchcc occurs ; [] |53| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |53| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |53| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |53| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |53| 
        CMP       AL,*+XAR5[4]          ; [CPU_] |53| 
        BF        $C$L14,NEQ            ; [CPU_] |53| 
        ; branchcc occurs ; [] |53| 
        B         $C$L12,UNC            ; [CPU_] |53| 
        ; branch occurs ; [] |53| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 57,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |57| 
        MOVB      XAR0,#76              ; [CPU_] |57| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |57| 
        CMPB      AL,#127               ; [CPU_] |57| 
        BF        $C$L3,EQ              ; [CPU_] |57| 
        ; branchcc occurs ; [] |57| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |57| 
        MOVB      XAR0,#76              ; [CPU_] |57| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |57| 
        CMPB      AL,#4                 ; [CPU_] |57| 
        BF        $C$L14,NEQ            ; [CPU_] |57| 
        ; branchcc occurs ; [] |57| 
$C$L3:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 58,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |58| 
        MOVB      AL,#5                 ; [CPU_] |58| 
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("_setState")
	.dwattr $C$DW$20, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |58| 
        ; call occurs [#_setState] ; [] |58| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 59,column 9,is_stmt
        B         $C$L14,UNC            ; [CPU_] |59| 
        ; branch occurs ; [] |59| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 62,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |62| 
        MOVB      XAR0,#76              ; [CPU_] |62| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |62| 
        CMPB      AL,#127               ; [CPU_] |62| 
        BF        $C$L5,EQ              ; [CPU_] |62| 
        ; branchcc occurs ; [] |62| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |62| 
        MOVB      XAR0,#76              ; [CPU_] |62| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |62| 
        CMPB      AL,#5                 ; [CPU_] |62| 
        BF        $C$L14,NEQ            ; [CPU_] |62| 
        ; branchcc occurs ; [] |62| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 64,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |64| 
        MOVB      AL,#4                 ; [CPU_] |64| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_setState")
	.dwattr $C$DW$21, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |64| 
        ; call occurs [#_setState] ; [] |64| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 65,column 9,is_stmt
        B         $C$L14,UNC            ; [CPU_] |65| 
        ; branch occurs ; [] |65| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 68,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |68| 
        MOVB      XAR0,#76              ; [CPU_] |68| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |68| 
        CMPB      AL,#5                 ; [CPU_] |68| 
        BF        $C$L7,EQ              ; [CPU_] |68| 
        ; branchcc occurs ; [] |68| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |68| 
        MOVB      XAR0,#76              ; [CPU_] |68| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |68| 
        CMPB      AL,#4                 ; [CPU_] |68| 
        BF        $C$L14,NEQ            ; [CPU_] |68| 
        ; branchcc occurs ; [] |68| 
$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 70,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |70| 
        MOVB      AL,#127               ; [CPU_] |70| 
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_setState")
	.dwattr $C$DW$22, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |70| 
        ; call occurs [#_setState] ; [] |70| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 71,column 9,is_stmt
        B         $C$L14,UNC            ; [CPU_] |71| 
        ; branch occurs ; [] |71| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 74,column 10,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |74| 
        MOVB      XAR0,#92              ; [CPU_] |74| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |74| 
        BF        $C$L9,EQ              ; [CPU_] |74| 
        ; branchcc occurs ; [] |74| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 75,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |75| 
        MOVB      XAR0,#92              ; [CPU_] |75| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |75| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |75| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_call
	.dwattr $C$DW$23, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |75| 
        ; call occurs [XAR7] ; [] |75| 
$C$L9:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 76,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |76| 
        MOVB      AL,#0                 ; [CPU_] |76| 
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_setState")
	.dwattr $C$DW$24, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |76| 
        ; call occurs [#_setState] ; [] |76| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 77,column 9,is_stmt
        B         $C$L14,UNC            ; [CPU_] |77| 
        ; branch occurs ; [] |77| 
$C$L10:    

$C$DW$25	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("currentNodeId")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_currentNodeId")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -5]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 81,column 29,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |81| 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_getNodeId")
	.dwattr $C$DW$27, DW_AT_TI_call
        LCR       #_getNodeId           ; [CPU_] |81| 
        ; call occurs [#_getNodeId] ; [] |81| 
        MOV       *-SP[5],AL            ; [CPU_] |81| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 83,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |83| 
        MOVB      XAR0,#94              ; [CPU_] |83| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |83| 
        BF        $C$L11,EQ             ; [CPU_] |83| 
        ; branchcc occurs ; [] |83| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 84,column 16,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |84| 
        MOVB      XAR0,#94              ; [CPU_] |84| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |84| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |84| 
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_call
	.dwattr $C$DW$28, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |84| 
        ; call occurs [XAR7] ; [] |84| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 94,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |94| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |94| 
        MOVB      *+XAR4[0],#255,UNC    ; [CPU_] |94| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 96,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |96| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |96| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_setNodeId")
	.dwattr $C$DW$29, DW_AT_TI_call
        LCR       #_setNodeId           ; [CPU_] |96| 
        ; call occurs [#_setNodeId] ; [] |96| 
	.dwendtag $C$DW$25

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 98,column 10,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |98| 
        MOVB      AL,#0                 ; [CPU_] |98| 
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_setState")
	.dwattr $C$DW$30, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |98| 
        ; call occurs [#_setState] ; [] |98| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 99,column 10,is_stmt
        B         $C$L14,UNC            ; [CPU_] |99| 
        ; branch occurs ; [] |99| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 55,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |55| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |55| 
        CMPB      AL,#128               ; [CPU_] |55| 
        B         $C$L13,GT             ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
        CMPB      AL,#128               ; [CPU_] |55| 
        BF        $C$L6,EQ              ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
        CMPB      AL,#1                 ; [CPU_] |55| 
        BF        $C$L2,EQ              ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
        CMPB      AL,#2                 ; [CPU_] |55| 
        BF        $C$L4,EQ              ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
        B         $C$L14,UNC            ; [CPU_] |55| 
        ; branch occurs ; [] |55| 
$C$L13:    
        CMPB      AL,#129               ; [CPU_] |55| 
        BF        $C$L8,EQ              ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
        CMPB      AL,#130               ; [CPU_] |55| 
        BF        $C$L10,EQ             ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
        B         $C$L14,UNC            ; [CPU_] |55| 
        ; branch occurs ; [] |55| 
$C$L14:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x6a)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.global	_slaveSendBootUp

$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("slaveSendBootUp")
	.dwattr $C$DW$32, DW_AT_low_pc(_slaveSendBootUp)
	.dwattr $C$DW$32, DW_AT_high_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_slaveSendBootUp")
	.dwattr $C$DW$32, DW_AT_external
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$32, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c")
	.dwattr $C$DW$32, DW_AT_TI_begin_line(0x74)
	.dwattr $C$DW$32, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$32, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 117,column 1,is_stmt,address _slaveSendBootUp

	.dwfde $C$DW$CIE, _slaveSendBootUp
$C$DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _slaveSendBootUp              FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_slaveSendBootUp:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -2]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -13]
        MOVL      *-SP[2],XAR4          ; [CPU_] |117| 

$C$DW$36	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -14]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 128,column 14,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |128| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |128| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |128| 
        OR        AL,#0x0700            ; [CPU_] |128| 
        MOV       *-SP[14],AL           ; [CPU_] |128| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 129,column 4,is_stmt
        MOV       *-SP[13],AL           ; [CPU_] |129| 
	.dwendtag $C$DW$36

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 131,column 3,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |131| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 132,column 3,is_stmt
        MOVB      *-SP[11],#1,UNC       ; [CPU_] |132| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 133,column 3,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |133| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 135,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |135| 
        MOVB      XAR0,#249             ; [CPU_] |135| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |135| 
        BF        $C$L17,NEQ            ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
        MOVZ      AR5,SP                ; [CPU_U] |135| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |135| 
        MOVB      AL,#0                 ; [CPU_] |135| 
        SUBB      XAR5,#13              ; [CPU_U] |135| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_MBX_post")
	.dwattr $C$DW$38, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |135| 
        ; call occurs [#_MBX_post] ; [] |135| 
        MOVB      AH,#0                 ; [CPU_] |135| 
        MOVB      XAR6,#0               ; [CPU_] |135| 
        CMPB      AL,#0                 ; [CPU_] |135| 
        BF        $C$L15,EQ             ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
        MOVB      AH,#1                 ; [CPU_] |135| 
$C$L15:    
        CMPB      AH,#0                 ; [CPU_] |135| 
        BF        $C$L16,NEQ            ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
        MOVB      XAR6,#1               ; [CPU_] |135| 
$C$L16:    
        MOV       AL,AR6                ; [CPU_] |135| 
        B         $C$L20,UNC            ; [CPU_] |135| 
        ; branch occurs ; [] |135| 
$C$L17:    
        MOVZ      AR5,SP                ; [CPU_U] |135| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |135| 
        MOVB      AL,#0                 ; [CPU_] |135| 
        SUBB      XAR5,#13              ; [CPU_U] |135| 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_MBX_post")
	.dwattr $C$DW$39, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |135| 
        ; call occurs [#_MBX_post] ; [] |135| 
        MOVB      AH,#0                 ; [CPU_] |135| 
        MOVB      XAR6,#0               ; [CPU_] |135| 
        CMPB      AL,#0                 ; [CPU_] |135| 
        BF        $C$L18,EQ             ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
        MOVB      AH,#1                 ; [CPU_] |135| 
$C$L18:    
        CMPB      AH,#0                 ; [CPU_] |135| 
        BF        $C$L19,NEQ            ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
        MOVB      XAR6,#1               ; [CPU_] |135| 
$C$L19:    
        MOV       AL,AR6                ; [CPU_] |135| 
$C$L20:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c",line 136,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$32, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/Festival/nmtSlave.c")
	.dwattr $C$DW$32, DW_AT_TI_end_line(0x88)
	.dwattr $C$DW$32, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$32

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_setNodeId
	.global	_MBX_post
	.global	_getNodeId
	.global	_setState
	.global	_usb_tx_mbox
	.global	_can_tx_mbox

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$41, DW_AT_name("cob_id")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$42, DW_AT_name("rtr")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$43	.dwtag  DW_TAG_member
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_name("len")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$43, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$44	.dwtag  DW_TAG_member
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$44, DW_AT_name("data")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$44, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$44, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$T$116	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$45	.dwtag  DW_TAG_member
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$45, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$45, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$46	.dwtag  DW_TAG_member
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$46, DW_AT_name("csSDO")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$46, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$46, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$47	.dwtag  DW_TAG_member
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$47, DW_AT_name("csEmergency")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$47, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$47, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$48	.dwtag  DW_TAG_member
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$48, DW_AT_name("csSYNC")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$48, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$48, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$49, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$49, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$50, DW_AT_name("csPDO")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$51	.dwtag  DW_TAG_member
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$51, DW_AT_name("csLSS")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$51, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$52	.dwtag  DW_TAG_member
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$52, DW_AT_name("errCode")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$52, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$52, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$53	.dwtag  DW_TAG_member
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_name("errRegMask")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$53, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_member
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$54, DW_AT_name("active")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$54, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$54, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$99	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x18)
$C$DW$55	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$55, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$99


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$56	.dwtag  DW_TAG_member
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$56, DW_AT_name("index")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$56, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$56, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$57, DW_AT_name("subindex")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$57, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$58, DW_AT_name("size")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$59	.dwtag  DW_TAG_member
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$59, DW_AT_name("address")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$59, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x08)
$C$DW$60	.dwtag  DW_TAG_member
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$60, DW_AT_name("wListElem")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$60, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$60, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$61	.dwtag  DW_TAG_member
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$61, DW_AT_name("wCount")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$61, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$62	.dwtag  DW_TAG_member
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$62, DW_AT_name("fxn")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$62, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$62, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x16)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x30)
$C$DW$63	.dwtag  DW_TAG_member
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$63, DW_AT_name("dataQue")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$63, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$64	.dwtag  DW_TAG_member
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$64, DW_AT_name("freeQue")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$64, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$64, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$65, DW_AT_name("dataSem")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$65, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$66, DW_AT_name("freeSem")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$67	.dwtag  DW_TAG_member
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$67, DW_AT_name("segid")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$67, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$68	.dwtag  DW_TAG_member
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$68, DW_AT_name("size")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$68, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$68, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$69	.dwtag  DW_TAG_member
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$69, DW_AT_name("length")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$69, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$70	.dwtag  DW_TAG_member
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$70, DW_AT_name("name")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$70, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$70, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
$C$DW$T$120	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x04)
$C$DW$71	.dwtag  DW_TAG_member
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$71, DW_AT_name("next")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$71, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$72	.dwtag  DW_TAG_member
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$72, DW_AT_name("prev")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$72, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$72, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)

$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x10)
$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$73, DW_AT_name("job")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$73, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$74	.dwtag  DW_TAG_member
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$74, DW_AT_name("count")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$74, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$75, DW_AT_name("pendQ")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$75, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$76, DW_AT_name("name")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$77	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$27)
	.dwendtag $C$DW$T$28

$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x16)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$78	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)

$C$DW$T$76	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$79	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$66)
$C$DW$80	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$76

$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)
$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$66)
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$6)
$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
$C$DW$84	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$85	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$85, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$86	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
$C$DW$T$55	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$86)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)
$C$DW$87	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$9)
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$87)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$88	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$6)
$C$DW$89	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$90	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$66)
$C$DW$91	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$47)
$C$DW$92	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
$C$DW$93	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$84

$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x16)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$94	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$86)
$C$DW$T$87	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$94)
$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$66)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$9)
$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$93

$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$96	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$96, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x01)
$C$DW$98	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$99	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)

$C$DW$T$62	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$62, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$100	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$101	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$102	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$103	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$104	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$105	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$106	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$107	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$62

$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$79	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x80)
$C$DW$108	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$108, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$79


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x06)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$109, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$110, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$112, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$113, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$114, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$115	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$50)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$115)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)

$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x132)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$116, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$117, DW_AT_name("objdict")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$118, DW_AT_name("PDO_status")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$119, DW_AT_name("firstIndex")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$120, DW_AT_name("lastIndex")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$121, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$122, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$123, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$124, DW_AT_name("transfers")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$125, DW_AT_name("nodeState")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$126, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$127, DW_AT_name("initialisation")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$128, DW_AT_name("preOperational")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$129, DW_AT_name("operational")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$130, DW_AT_name("stopped")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$131, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$132, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$133, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$134, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$135, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$136, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$137, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$138, DW_AT_name("heartbeatError")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$139, DW_AT_name("NMTable")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$140, DW_AT_name("syncTimer")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$141, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$142, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$143, DW_AT_name("pre_sync")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$144, DW_AT_name("post_TPDO")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$145, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$146, DW_AT_name("toggle")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$147, DW_AT_name("canHandle")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$148, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$149, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$150, DW_AT_name("globalCallback")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$151, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$152, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$153, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$154, DW_AT_name("dcf_request")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$155, DW_AT_name("error_state")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$156, DW_AT_name("error_history_size")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$157, DW_AT_name("error_number")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$158, DW_AT_name("error_first_element")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$159, DW_AT_name("error_register")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$160, DW_AT_name("error_cobid")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$161, DW_AT_name("error_data")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$162, DW_AT_name("post_emcy")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$163, DW_AT_name("lss_transfer")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$164, DW_AT_name("eeprom_index")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$165, DW_AT_name("eeprom_size")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x0e)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$166, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$167, DW_AT_name("event_timer")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$168, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$169, DW_AT_name("last_message")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)

$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x14)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$170, DW_AT_name("nodeId")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$171, DW_AT_name("whoami")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$172, DW_AT_name("state")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$173, DW_AT_name("toggle")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$174, DW_AT_name("abortCode")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$175, DW_AT_name("index")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$176, DW_AT_name("subIndex")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$177, DW_AT_name("port")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$178, DW_AT_name("count")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$179, DW_AT_name("offset")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$180, DW_AT_name("datap")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$181, DW_AT_name("dataType")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$182, DW_AT_name("timer")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$183, DW_AT_name("Callback")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110

$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)

$C$DW$T$61	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x3c)
$C$DW$184	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$184, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$61


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x04)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$185, DW_AT_name("pSubindex")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$186, DW_AT_name("bSubCount")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$187, DW_AT_name("index")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$188	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$45)
$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$188)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$189	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$9)
$C$DW$190	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$73)
$C$DW$191	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$89)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x08)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$192, DW_AT_name("bAccessType")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$193, DW_AT_name("bDataType")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$194, DW_AT_name("size")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$195, DW_AT_name("pObject")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$196, DW_AT_name("bProcessor")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$197	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$115)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$197)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg0]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg1]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg2]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg3]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg20]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_reg21]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg22]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg23]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg24]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg25]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg26]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_reg28]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_reg29]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_reg30]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg31]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_regx 0x20]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x21]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_regx 0x22]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_regx 0x23]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_regx 0x24]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_regx 0x25]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_regx 0x26]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg4]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg6]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg8]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg10]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_reg12]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg14]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg16]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg17]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_reg18]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_reg19]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_reg5]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_reg7]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_reg9]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg11]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg13]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_reg15]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x30]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x33]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x34]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x37]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x38]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x40]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x43]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x44]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x47]
$C$DW$253	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$253, DW_AT_location[DW_OP_regx 0x48]
$C$DW$254	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$254, DW_AT_location[DW_OP_regx 0x49]
$C$DW$255	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$255, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$256	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$256, DW_AT_location[DW_OP_regx 0x27]
$C$DW$257	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_regx 0x28]
$C$DW$258	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$258, DW_AT_location[DW_OP_reg27]
$C$DW$259	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$259, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

