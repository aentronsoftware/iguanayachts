;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Oct 28 12:12:56 2019                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../gateway.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Johnny\V31\NewCAN\MMS_F_AEC\Standard_AEC_160\AEC_Standard\PTM079_LV")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_GAT_min+0,32
	.bits	65535,16			; _GAT_min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_GAT_max+0,32
	.bits	0,16			; _GAT_max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Modules_Present+0,32
	.bits	0,16			; _Modules_Present @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Sync_count+0,32
	.bits	70,16			; _Sync_count @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestPositionEnable+0,32
	.bits	1,16			; _TestPositionEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_InitOK+0,32
	.bits	0,16			; _InitOK @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SequenceRunning+0,32
	.bits	0,16			; _SequenceRunning @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_make10ms+0,32
	.bits	0,16			; _make10ms @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ReceiveNew+0,32
	.bits	0,16			; _ReceiveNew @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UnderCurrent+0,32
	.bits	0,16			; _UnderCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestCurrentEnable+0,32
	.bits	0,16			; _TestCurrentEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Heater_status+0,32
	.bits	0,16			; _Heater_status @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_current_counter+0,32
	.bits	0,32			; _GV_current_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_voltage_counter+0,32
	.bits	0,32			; _GV_voltage_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BootNodeId+0,32
	.bits	99,32			; _BootNodeId @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BootCommand+0,32
	.bits	0,32			; _BootCommand @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Unlock")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_USB_Unlock")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("genCRC32Table")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_genCRC32Table")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverVoltage")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external

$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverTemp")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorUnderVoltage")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_SetError")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_ERR_SetError")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_DELTA_Voltage")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ERR_DELTA_Voltage")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_Init")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ADS_Init")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Random")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_HAL_Random")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Unlock")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_HAL_Unlock")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external

$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Stop")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_USB_Stop")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external

$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Reset")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_HAL_Reset")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external

$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$18, DW_AT_declaration
	.dwattr $C$DW$18, DW_AT_external
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$96)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$18


$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("canInit")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_canInit")
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$9)
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$21


$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Init")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_HAL_Init")
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_StartRecorder")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_REC_StartRecorder")
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_SetNodeId")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_DIC_SetNodeId")
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$26


$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$28


$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$30


$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverCurrent")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_WarningMax")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODP_Temperature_WarningMax")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_C_D_Mode")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODP_Current_C_D_Mode")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_WarningMin")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ODP_Temperature_WarningMin")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ODP_NbOfModules")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ODP_NbOfModules")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$47, DW_AT_declaration
	.dwattr $C$DW$47, DW_AT_external
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Bender_Voltage")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODV_Gateway_Bender_Voltage")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinModTemp")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_Gateway_MinModTemp")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Test_Voltage")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_ODV_Gateway_Test_Voltage")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_ISOTets")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODV_Gateway_ISOTets")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Alive_Counter")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ODV_Gateway_Alive_Counter")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxModTemp")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ODV_Gateway_MaxModTemp")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$56, DW_AT_declaration
	.dwattr $C$DW$56, DW_AT_external
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_declaration
	.dwattr $C$DW$57, DW_AT_external
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$58, DW_AT_declaration
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Heater_Status")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ODV_Gateway_Heater_Status")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Debug")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODV_Debug")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC2")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODV_SOC_SOC2")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
	.global	_GAT_min
_GAT_min:	.usect	".ebss",1,1,0
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("GAT_min")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_GAT_min")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _GAT_min]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$67, DW_AT_external
	.global	_GAT_max
_GAT_max:	.usect	".ebss",1,1,0
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("GAT_max")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_GAT_max")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_addr _GAT_max]
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$68, DW_AT_external
	.global	_Modules_Present
_Modules_Present:	.usect	".ebss",1,1,0
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("Modules_Present")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_Modules_Present")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_addr _Modules_Present]
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$69, DW_AT_external
	.global	_Sync_count
_Sync_count:	.usect	".ebss",1,1,0
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("Sync_count")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_Sync_count")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_addr _Sync_count]
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$70, DW_AT_external
	.global	_TestPositionEnable
_TestPositionEnable:	.usect	".ebss",1,1,0
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("TestPositionEnable")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_TestPositionEnable")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_addr _TestPositionEnable]
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$71, DW_AT_external
	.global	_InitOK
_InitOK:	.usect	".ebss",1,1,0
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("InitOK")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_InitOK")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_addr _InitOK]
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$72, DW_AT_external
	.global	_SequenceRunning
_SequenceRunning:	.usect	".ebss",1,1,0
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("SequenceRunning")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_SequenceRunning")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_addr _SequenceRunning]
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$73, DW_AT_external
	.global	_make10ms
_make10ms:	.usect	".ebss",1,1,0
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("make10ms")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_make10ms")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_addr _make10ms]
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$74, DW_AT_external
	.global	_ReceiveNew
_ReceiveNew:	.usect	".ebss",1,1,0
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("ReceiveNew")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_ReceiveNew")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_addr _ReceiveNew]
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$75, DW_AT_external
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("HAL_NewCurPoint")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_HAL_NewCurPoint")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external
	.global	_UnderCurrent
_UnderCurrent:	.usect	".ebss",1,1,0
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("UnderCurrent")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_UnderCurrent")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_addr _UnderCurrent]
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$77, DW_AT_external
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineEvent")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_ODV_MachineEvent")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$78, DW_AT_declaration
	.dwattr $C$DW$78, DW_AT_external
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_external
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_declaration
	.dwattr $C$DW$80, DW_AT_external
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$81, DW_AT_declaration
	.dwattr $C$DW$81, DW_AT_external
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("ODV_BenderISO_IMC_ISOR")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_ODV_BenderISO_IMC_ISOR")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$82, DW_AT_declaration
	.dwattr $C$DW$82, DW_AT_external
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$83, DW_AT_declaration
	.dwattr $C$DW$83, DW_AT_external
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RelayResetTime")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_ODP_RelayResetTime")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("ODV_BenderISO_D_IMC_HV_1")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_ODV_BenderISO_D_IMC_HV_1")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$85, DW_AT_declaration
	.dwattr $C$DW$85, DW_AT_external
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$86, DW_AT_declaration
	.dwattr $C$DW$86, DW_AT_external
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Version")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_ODV_Version")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$87, DW_AT_declaration
	.dwattr $C$DW$87, DW_AT_external
	.global	_TestCurrentEnable
_TestCurrentEnable:	.usect	".ebss",1,1,0
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("TestCurrentEnable")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_TestCurrentEnable")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _TestCurrentEnable]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$88, DW_AT_external
	.global	_Heater_status
_Heater_status:	.usect	".ebss",1,1,0
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("Heater_status")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_Heater_status")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_addr _Heater_status]
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$89, DW_AT_external
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Config")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_ODP_Board_Config")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$91, DW_AT_declaration
	.dwattr $C$DW$91, DW_AT_external
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Sleep_Timeout")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_ODP_Sleep_Timeout")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$92, DW_AT_declaration
	.dwattr $C$DW$92, DW_AT_external
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Sleep_Current")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_ODP_Sleep_Current")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_external

$C$DW$94	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddMultiUnits")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$94, DW_AT_declaration
	.dwattr $C$DW$94, DW_AT_external
$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$94


$C$DW$96	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearError")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_ERR_ClearError")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_declaration
	.dwattr $C$DW$96, DW_AT_external

$C$DW$97	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddVariables")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_PAR_AddVariables")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$97, DW_AT_declaration
	.dwattr $C$DW$97, DW_AT_external
$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$97


$C$DW$99	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$99, DW_AT_declaration
	.dwattr $C$DW$99, DW_AT_external
$C$DW$100	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$157)
$C$DW$101	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$164)
$C$DW$102	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$99


$C$DW$103	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_external
$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$96)
$C$DW$105	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$93)
	.dwendtag $C$DW$103


$C$DW$106	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$106, DW_AT_declaration
	.dwattr $C$DW$106, DW_AT_external
$C$DW$107	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$157)
$C$DW$108	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$164)
$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$106


$C$DW$110	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$110, DW_AT_declaration
	.dwattr $C$DW$110, DW_AT_external
$C$DW$111	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
$C$DW$112	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$51)
$C$DW$113	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$9)
$C$DW$114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$110


$C$DW$115	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Start")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_USB_Start")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$115, DW_AT_declaration
	.dwattr $C$DW$115, DW_AT_external

$C$DW$116	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$116, DW_AT_declaration
	.dwattr $C$DW$116, DW_AT_external
$C$DW$117	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$116


$C$DW$118	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$118, DW_AT_declaration
	.dwattr $C$DW$118, DW_AT_external
$C$DW$119	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$96)
	.dwendtag $C$DW$118


$C$DW$120	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarnings")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_ERR_ClearWarnings")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$120, DW_AT_declaration
	.dwattr $C$DW$120, DW_AT_external
$C$DW$121	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Current")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_ODV_Gateway_Current")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$121, DW_AT_declaration
	.dwattr $C$DW$121, DW_AT_external
$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Voltage")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_ODV_Gateway_Voltage")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$122, DW_AT_declaration
	.dwattr $C$DW$122, DW_AT_external
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Temperature")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_ODV_Gateway_Temperature")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$123, DW_AT_declaration
	.dwattr $C$DW$123, DW_AT_external
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_LogNB")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_ODV_Gateway_LogNB")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_external
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$125, DW_AT_declaration
	.dwattr $C$DW$125, DW_AT_external
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Variables")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_ODV_Recorder_Variables")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$126, DW_AT_declaration
	.dwattr $C$DW$126, DW_AT_external
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Multiunits")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_ODV_Recorder_Multiunits")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$127, DW_AT_declaration
	.dwattr $C$DW$127, DW_AT_external
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$128, DW_AT_declaration
	.dwattr $C$DW$128, DW_AT_external
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$129, DW_AT_declaration
	.dwattr $C$DW$129, DW_AT_external

$C$DW$130	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$130, DW_AT_declaration
	.dwattr $C$DW$130, DW_AT_external
$C$DW$131	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$163)
$C$DW$132	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$130

$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$133, DW_AT_declaration
	.dwattr $C$DW$133, DW_AT_external
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ResetHW")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ODV_ResetHW")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$134, DW_AT_declaration
	.dwattr $C$DW$134, DW_AT_external

$C$DW$135	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$135, DW_AT_declaration
	.dwattr $C$DW$135, DW_AT_external
$C$DW$136	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$135

$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$137, DW_AT_declaration
	.dwattr $C$DW$137, DW_AT_external
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$138, DW_AT_declaration
	.dwattr $C$DW$138, DW_AT_external
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$139, DW_AT_declaration
	.dwattr $C$DW$139, DW_AT_external
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("ODV_StoreParameters")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_ODV_StoreParameters")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$140, DW_AT_declaration
	.dwattr $C$DW$140, DW_AT_external
	.global	_GV_current_counter
_GV_current_counter:	.usect	".ebss",2,1,1
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("GV_current_counter")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_GV_current_counter")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_addr _GV_current_counter]
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$141, DW_AT_external

$C$DW$142	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$142, DW_AT_declaration
	.dwattr $C$DW$142, DW_AT_external
$C$DW$143	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$96)
	.dwendtag $C$DW$142

	.global	_GV_voltage_counter
_GV_voltage_counter:	.usect	".ebss",2,1,1
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("GV_voltage_counter")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_GV_voltage_counter")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_addr _GV_voltage_counter]
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$144, DW_AT_external
	.global	_BootNodeId
_BootNodeId:	.usect	"BootCommand",2,1,1
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("BootNodeId")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_BootNodeId")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_addr _BootNodeId]
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$145, DW_AT_external
	.global	_BoardODdata
_BoardODdata:	.usect	".ebss",2,1,1
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_addr _BoardODdata]
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$146, DW_AT_external
	.global	_BootCommand
_BootCommand:	.usect	"BootCommand",2,1,1
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("BootCommand")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_BootCommand")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_addr _BootCommand]
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$147, DW_AT_external

$C$DW$148	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$148, DW_AT_declaration
	.dwattr $C$DW$148, DW_AT_external
$C$DW$149	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$13)
$C$DW$150	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$105)
$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$147)
$C$DW$152	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$148

$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RestoreDefaultParameters")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_ODV_RestoreDefaultParameters")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$153, DW_AT_declaration
	.dwattr $C$DW$153, DW_AT_external
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$154, DW_AT_declaration
	.dwattr $C$DW$154, DW_AT_external

$C$DW$155	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$155, DW_AT_declaration
	.dwattr $C$DW$155, DW_AT_external
$C$DW$156	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$155

$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$157, DW_AT_declaration
	.dwattr $C$DW$157, DW_AT_external

$C$DW$158	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$158, DW_AT_declaration
	.dwattr $C$DW$158, DW_AT_external
$C$DW$159	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$96)
$C$DW$160	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$9)
$C$DW$161	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$158

$C$DW$162	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$162, DW_AT_declaration
	.dwattr $C$DW$162, DW_AT_external
$C$DW$163	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$163, DW_AT_declaration
	.dwattr $C$DW$163, DW_AT_external
$C$DW$164	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$164, DW_AT_declaration
	.dwattr $C$DW$164, DW_AT_external
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$165, DW_AT_declaration
	.dwattr $C$DW$165, DW_AT_external
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Text")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_ODV_RTC_Text")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$166, DW_AT_declaration
	.dwattr $C$DW$166, DW_AT_external
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$167, DW_AT_declaration
	.dwattr $C$DW$167, DW_AT_external
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$168, DW_AT_declaration
	.dwattr $C$DW$168, DW_AT_external
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("golden_CRC_values")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_golden_CRC_values")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$169, DW_AT_declaration
	.dwattr $C$DW$169, DW_AT_external
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$170, DW_AT_declaration
	.dwattr $C$DW$170, DW_AT_external
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$171, DW_AT_declaration
	.dwattr $C$DW$171, DW_AT_external
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$172, DW_AT_declaration
	.dwattr $C$DW$172, DW_AT_external
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$173, DW_AT_declaration
	.dwattr $C$DW$173, DW_AT_external
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$174, DW_AT_declaration
	.dwattr $C$DW$174, DW_AT_external
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("mailboxSDOout")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_mailboxSDOout")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$175, DW_AT_declaration
	.dwattr $C$DW$175, DW_AT_external
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("can_rx_mbox")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_can_rx_mbox")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$176, DW_AT_declaration
	.dwattr $C$DW$176, DW_AT_external
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$177, DW_AT_declaration
	.dwattr $C$DW$177, DW_AT_external
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_MinCellVoltage")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_ODV_Modules_MinCellVoltage")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$178, DW_AT_declaration
	.dwattr $C$DW$178, DW_AT_external
$C$DW$179	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_MaxCellVoltage")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_ODV_Modules_MaxCellVoltage")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$179, DW_AT_declaration
	.dwattr $C$DW$179, DW_AT_external
$C$DW$180	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Heater")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_ODV_Modules_Heater")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$180, DW_AT_declaration
	.dwattr $C$DW$180, DW_AT_external
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Alarms")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_ODV_Modules_Alarms")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$181, DW_AT_declaration
	.dwattr $C$DW$181, DW_AT_external
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Temperature")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_ODV_Modules_Temperature")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$182, DW_AT_declaration
	.dwattr $C$DW$182, DW_AT_external
$C$DW$183	.dwtag  DW_TAG_variable, DW_AT_name("ODI_gateway_dict_Data")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_ODI_gateway_dict_Data")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$183, DW_AT_declaration
	.dwattr $C$DW$183, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0243212 
	.sect	".text"
	.global	_CheckfirmwareCRC

$C$DW$184	.dwtag  DW_TAG_subprogram, DW_AT_name("CheckfirmwareCRC")
	.dwattr $C$DW$184, DW_AT_low_pc(_CheckfirmwareCRC)
	.dwattr $C$DW$184, DW_AT_high_pc(0x00)
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_CheckfirmwareCRC")
	.dwattr $C$DW$184, DW_AT_external
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$184, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$184, DW_AT_TI_begin_line(0x43)
	.dwattr $C$DW$184, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$184, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../gateway.c",line 68,column 1,is_stmt,address _CheckfirmwareCRC

	.dwfde $C$DW$CIE, _CheckfirmwareCRC

;***************************************************************
;* FNAME: _CheckfirmwareCRC             FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_CheckfirmwareCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_breg20 -2]
$C$DW$186	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_breg20 -4]
$C$DW$187	.dwtag  DW_TAG_variable, DW_AT_name("crc_rec")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_crc_rec")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$187, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../gateway.c",line 72,column 8,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |72| 
        B         $C$L3,UNC             ; [CPU_] |72| 
        ; branch occurs ; [] |72| 
$C$L1:    
	.dwpsn	file "../gateway.c",line 74,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,SP                ; [CPU_U] |74| 
        MOVL      XAR7,#_golden_CRC_values+2 ; [CPU_U] |74| 
        MOV       ACC,*-SP[2] << 3      ; [CPU_] |74| 
        SUBB      XAR4,#12              ; [CPU_U] |74| 
        ADDL      XAR7,ACC              ; [CPU_] |74| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |74| 
	.dwpsn	file "../gateway.c",line 75,column 5,is_stmt
        MOV       ACC,*-SP[8] << #1     ; [CPU_] |75| 
        MOV       *-SP[1],AL            ; [CPU_] |75| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |75| 
        MOVB      XAR5,#0               ; [CPU_] |75| 
        MOVB      ACC,#0                ; [CPU_] |75| 
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$188, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |75| 
        ; call occurs [#_getCRC32_cpu] ; [] |75| 
        MOVL      *-SP[4],ACC           ; [CPU_] |75| 
	.dwpsn	file "../gateway.c",line 76,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |76| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |76| 
        BF        $C$L2,EQ              ; [CPU_] |76| 
        ; branchcc occurs ; [] |76| 
	.dwpsn	file "../gateway.c",line 77,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |77| 
        B         $C$L4,UNC             ; [CPU_] |77| 
        ; branch occurs ; [] |77| 
$C$L2:    
	.dwpsn	file "../gateway.c",line 72,column 47,is_stmt
        INC       *-SP[2]               ; [CPU_] |72| 
$C$L3:    
	.dwpsn	file "../gateway.c",line 72,column 15,is_stmt
        MOVW      DP,#_golden_CRC_values+1 ; [CPU_U] 
        MOV       AL,@_golden_CRC_values+1 ; [CPU_] |72| 
        CMP       AL,*-SP[2]            ; [CPU_] |72| 
        B         $C$L1,HI              ; [CPU_] |72| 
        ; branchcc occurs ; [] |72| 
	.dwpsn	file "../gateway.c",line 88,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |88| 
$C$L4:    
	.dwpsn	file "../gateway.c",line 89,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$184, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$184, DW_AT_TI_end_line(0x59)
	.dwattr $C$DW$184, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$184

	.sect	".text"
	.global	_FnResetNode

$C$DW$190	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetNode")
	.dwattr $C$DW$190, DW_AT_low_pc(_FnResetNode)
	.dwattr $C$DW$190, DW_AT_high_pc(0x00)
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_FnResetNode")
	.dwattr $C$DW$190, DW_AT_external
	.dwattr $C$DW$190, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$190, DW_AT_TI_begin_line(0x5d)
	.dwattr $C$DW$190, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$190, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 93,column 29,is_stmt,address _FnResetNode

	.dwfde $C$DW$CIE, _FnResetNode
$C$DW$191	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnResetNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$192	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |93| 
	.dwpsn	file "../gateway.c",line 96,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$193	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$193, DW_AT_low_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$190, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$190, DW_AT_TI_end_line(0x60)
	.dwattr $C$DW$190, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$190

	.sect	".text"
	.global	_FnResetCommunications

$C$DW$194	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetCommunications")
	.dwattr $C$DW$194, DW_AT_low_pc(_FnResetCommunications)
	.dwattr $C$DW$194, DW_AT_high_pc(0x00)
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_FnResetCommunications")
	.dwattr $C$DW$194, DW_AT_external
	.dwattr $C$DW$194, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$194, DW_AT_TI_begin_line(0x63)
	.dwattr $C$DW$194, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$194, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../gateway.c",line 99,column 39,is_stmt,address _FnResetCommunications

	.dwfde $C$DW$CIE, _FnResetCommunications
$C$DW$195	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetCommunications        FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_FnResetCommunications:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -2]
$C$DW$197	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$197, DW_AT_location[DW_OP_breg20 -3]
$C$DW$198	.dwtag  DW_TAG_variable, DW_AT_name("dummy_m")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_dummy_m")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_breg20 -19]
        MOVL      *-SP[2],XAR4          ; [CPU_] |99| 
	.dwpsn	file "../gateway.c",line 104,column 3,is_stmt
$C$L5:    
        MOVZ      AR5,SP                ; [CPU_U] |104| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |104| 
        MOVB      AL,#0                 ; [CPU_] |104| 
        SUBB      XAR5,#19              ; [CPU_U] |104| 
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$199, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |104| 
        ; call occurs [#_MBX_pend] ; [] |104| 
        CMPB      AL,#0                 ; [CPU_] |104| 
        BF        $C$L5,NEQ             ; [CPU_] |104| 
        ; branchcc occurs ; [] |104| 
	.dwpsn	file "../gateway.c",line 105,column 3,is_stmt
$C$L6:    
        MOVZ      AR5,SP                ; [CPU_U] |105| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |105| 
        MOVB      AL,#0                 ; [CPU_] |105| 
        SUBB      XAR5,#19              ; [CPU_U] |105| 
$C$DW$200	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$200, DW_AT_low_pc(0x00)
	.dwattr $C$DW$200, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$200, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |105| 
        ; call occurs [#_MBX_pend] ; [] |105| 
        CMPB      AL,#0                 ; [CPU_] |105| 
        BF        $C$L6,NEQ             ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "../gateway.c",line 106,column 3,is_stmt
$C$L7:    
        MOVZ      AR5,SP                ; [CPU_U] |106| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |106| 
        MOVB      AL,#0                 ; [CPU_] |106| 
        SUBB      XAR5,#19              ; [CPU_U] |106| 
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$201, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |106| 
        ; call occurs [#_MBX_pend] ; [] |106| 
        CMPB      AL,#0                 ; [CPU_] |106| 
        BF        $C$L7,NEQ             ; [CPU_] |106| 
        ; branchcc occurs ; [] |106| 
	.dwpsn	file "../gateway.c",line 108,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOV       AL,@_ODP_Board_RevisionNumber ; [CPU_] |108| 
        MOV       *-SP[3],AL            ; [CPU_] |108| 
	.dwpsn	file "../gateway.c",line 115,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |115| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_setNodeId")
	.dwattr $C$DW$202, DW_AT_TI_call
        LCR       #_setNodeId           ; [CPU_] |115| 
        ; call occurs [#_setNodeId] ; [] |115| 
	.dwpsn	file "../gateway.c",line 118,column 3,is_stmt
        MOV       AH,*-SP[3]            ; [CPU_] |118| 
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOV       AL,@_ODP_Board_BaudRate ; [CPU_] |118| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("_canInit")
	.dwattr $C$DW$203, DW_AT_TI_call
        LCR       #_canInit             ; [CPU_] |118| 
        ; call occurs [#_canInit] ; [] |118| 
	.dwpsn	file "../gateway.c",line 120,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |120| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("_DIC_SetNodeId")
	.dwattr $C$DW$204, DW_AT_TI_call
        LCR       #_DIC_SetNodeId       ; [CPU_] |120| 
        ; call occurs [#_DIC_SetNodeId] ; [] |120| 
	.dwpsn	file "../gateway.c",line 121,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$194, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$194, DW_AT_TI_end_line(0x79)
	.dwattr $C$DW$194, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$194

	.sect	".text"
	.global	_FnInitialiseNode

$C$DW$206	.dwtag  DW_TAG_subprogram, DW_AT_name("FnInitialiseNode")
	.dwattr $C$DW$206, DW_AT_low_pc(_FnInitialiseNode)
	.dwattr $C$DW$206, DW_AT_high_pc(0x00)
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_FnInitialiseNode")
	.dwattr $C$DW$206, DW_AT_external
	.dwattr $C$DW$206, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$206, DW_AT_TI_begin_line(0x7f)
	.dwattr $C$DW$206, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$206, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../gateway.c",line 127,column 35,is_stmt,address _FnInitialiseNode

	.dwfde $C$DW$CIE, _FnInitialiseNode
$C$DW$207	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnInitialiseNode             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_FnInitialiseNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -2]
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("CrcFirmwareTest")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_CrcFirmwareTest")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |127| 
	.dwpsn	file "../gateway.c",line 128,column 26,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |128| 
	.dwpsn	file "../gateway.c",line 130,column 3,is_stmt
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("_genCRC32Table")
	.dwattr $C$DW$210, DW_AT_TI_call
        LCR       #_genCRC32Table       ; [CPU_] |130| 
        ; call occurs [#_genCRC32Table] ; [] |130| 
	.dwpsn	file "../gateway.c",line 132,column 3,is_stmt
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("_CheckfirmwareCRC")
	.dwattr $C$DW$211, DW_AT_TI_call
        LCR       #_CheckfirmwareCRC    ; [CPU_] |132| 
        ; call occurs [#_CheckfirmwareCRC] ; [] |132| 
        MOV       *-SP[3],AL            ; [CPU_] |132| 
	.dwpsn	file "../gateway.c",line 133,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+19   ; [CPU_U] 
        OR        @_PieCtrlRegs+19,#0x0020 ; [CPU_] |133| 
	.dwpsn	file "../gateway.c",line 134,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |134| 
	.dwpsn	file "../gateway.c",line 135,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,#_ODP_Board_Config ; [CPU_U] |135| 
        MOVL      @_MMSConfig,XAR4      ; [CPU_] |135| 
	.dwpsn	file "../gateway.c",line 137,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |137| 
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("_PAR_InitParam")
	.dwattr $C$DW$212, DW_AT_TI_call
        LCR       #_PAR_InitParam       ; [CPU_] |137| 
        ; call occurs [#_PAR_InitParam] ; [] |137| 
        CMPB      AL,#0                 ; [CPU_] |137| 
        BF        $C$L8,EQ              ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
        MOV       AL,*-SP[3]            ; [CPU_] |137| 
        CMPB      AL,#1                 ; [CPU_] |137| 
        BF        $C$L8,NEQ             ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
	.dwpsn	file "../gateway.c",line 139,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#1,UNC       ; [CPU_] |139| 
	.dwpsn	file "../gateway.c",line 140,column 3,is_stmt
        B         $C$L9,UNC             ; [CPU_] |140| 
        ; branch occurs ; [] |140| 
$C$L8:    
	.dwpsn	file "../gateway.c",line 142,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#2,UNC       ; [CPU_] |142| 
$C$L9:    
	.dwpsn	file "../gateway.c",line 145,column 3,is_stmt
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$213, DW_AT_TI_call
        LCR       #_PAR_SetParamDependantVars ; [CPU_] |145| 
        ; call occurs [#_PAR_SetParamDependantVars] ; [] |145| 
	.dwpsn	file "../gateway.c",line 146,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$206, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$206, DW_AT_TI_end_line(0x92)
	.dwattr $C$DW$206, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$206

	.sect	".text"
	.global	_FnEnterPreOperational

$C$DW$215	.dwtag  DW_TAG_subprogram, DW_AT_name("FnEnterPreOperational")
	.dwattr $C$DW$215, DW_AT_low_pc(_FnEnterPreOperational)
	.dwattr $C$DW$215, DW_AT_high_pc(0x00)
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_FnEnterPreOperational")
	.dwattr $C$DW$215, DW_AT_external
	.dwattr $C$DW$215, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$215, DW_AT_TI_begin_line(0x96)
	.dwattr $C$DW$215, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$215, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 150,column 40,is_stmt,address _FnEnterPreOperational

	.dwfde $C$DW$CIE, _FnEnterPreOperational
$C$DW$216	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnEnterPreOperational        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnEnterPreOperational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$217	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |150| 
	.dwpsn	file "../gateway.c",line 151,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOVB      @_ODV_Controlword,#6,UNC ; [CPU_] |151| 
	.dwpsn	file "../gateway.c",line 152,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$218	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$218, DW_AT_low_pc(0x00)
	.dwattr $C$DW$218, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$215, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$215, DW_AT_TI_end_line(0x98)
	.dwattr $C$DW$215, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$215

	.sect	".text"
	.global	_FnStartNode

$C$DW$219	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStartNode")
	.dwattr $C$DW$219, DW_AT_low_pc(_FnStartNode)
	.dwattr $C$DW$219, DW_AT_high_pc(0x00)
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_FnStartNode")
	.dwattr $C$DW$219, DW_AT_external
	.dwattr $C$DW$219, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$219, DW_AT_TI_begin_line(0x9b)
	.dwattr $C$DW$219, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$219, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 155,column 30,is_stmt,address _FnStartNode

	.dwfde $C$DW$CIE, _FnStartNode
$C$DW$220	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStartNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStartNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |155| 
	.dwpsn	file "../gateway.c",line 157,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$219, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$219, DW_AT_TI_end_line(0x9d)
	.dwattr $C$DW$219, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$219

	.sect	".text"
	.global	_PreSync

$C$DW$223	.dwtag  DW_TAG_subprogram, DW_AT_name("PreSync")
	.dwattr $C$DW$223, DW_AT_low_pc(_PreSync)
	.dwattr $C$DW$223, DW_AT_high_pc(0x00)
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_PreSync")
	.dwattr $C$DW$223, DW_AT_external
	.dwattr $C$DW$223, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$223, DW_AT_TI_begin_line(0xa6)
	.dwattr $C$DW$223, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$223, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 166,column 26,is_stmt,address _PreSync

	.dwfde $C$DW$CIE, _PreSync
$C$DW$224	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PreSync                      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_PreSync:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$225	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_breg20 -2]
$C$DW$226	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_breg20 -3]
$C$DW$227	.dwtag  DW_TAG_variable, DW_AT_name("maxtemp")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_maxtemp")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_breg20 -4]
$C$DW$228	.dwtag  DW_TAG_variable, DW_AT_name("mintemp")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_mintemp")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_breg20 -5]
        MOVL      *-SP[2],XAR4          ; [CPU_] |166| 
	.dwpsn	file "../gateway.c",line 167,column 8,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |167| 
	.dwpsn	file "../gateway.c",line 168,column 2,is_stmt
        MOVW      DP,#_Heater_status    ; [CPU_U] 
        MOV       @_Heater_status,#0    ; [CPU_] |168| 
	.dwpsn	file "../gateway.c",line 169,column 15,is_stmt
        MOV       *-SP[4],#-55          ; [CPU_] |169| 
	.dwpsn	file "../gateway.c",line 169,column 30,is_stmt
        MOVB      *-SP[5],#127,UNC      ; [CPU_] |169| 
	.dwpsn	file "../gateway.c",line 171,column 2,is_stmt
        MOVW      DP,#_ODV_BenderISO_IMC_ISOR ; [CPU_U] 
        MOV       AL,@_ODV_BenderISO_IMC_ISOR ; [CPU_] |171| 
        MOVW      DP,#_ODV_Gateway_ISOTets ; [CPU_U] 
        MOV       @_ODV_Gateway_ISOTets,AL ; [CPU_] |171| 
	.dwpsn	file "../gateway.c",line 172,column 2,is_stmt
        MOVW      DP,#_ODV_BenderISO_D_IMC_HV_1 ; [CPU_U] 
        MOV       AL,@_ODV_BenderISO_D_IMC_HV_1 ; [CPU_] |172| 
        MOVW      DP,#_ODV_Gateway_Bender_Voltage ; [CPU_U] 
        MOV       @_ODV_Gateway_Bender_Voltage,AL ; [CPU_] |172| 
	.dwpsn	file "../gateway.c",line 174,column 4,is_stmt
        MOVW      DP,#_make10ms         ; [CPU_U] 
        MOV       AL,@_make10ms         ; [CPU_] |174| 
        CMPB      AL,#1                 ; [CPU_] |174| 
        BF        $C$L11,NEQ            ; [CPU_] |174| 
        ; branchcc occurs ; [] |174| 
	.dwpsn	file "../gateway.c",line 176,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Alive_Counter ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Alive_Counter ; [CPU_] |176| 
        CMPB      AL,#15                ; [CPU_] |176| 
        B         $C$L10,LO             ; [CPU_] |176| 
        ; branchcc occurs ; [] |176| 
	.dwpsn	file "../gateway.c",line 178,column 6,is_stmt
        MOV       @_ODV_Gateway_Alive_Counter,#0 ; [CPU_] |178| 
	.dwpsn	file "../gateway.c",line 179,column 6,is_stmt
        MOVW      DP,#_make10ms         ; [CPU_U] 
        MOV       @_make10ms,#0         ; [CPU_] |179| 
	.dwpsn	file "../gateway.c",line 180,column 5,is_stmt
        B         $C$L12,UNC            ; [CPU_] |180| 
        ; branch occurs ; [] |180| 
$C$L10:    
	.dwpsn	file "../gateway.c",line 183,column 6,is_stmt
        INC       @_ODV_Gateway_Alive_Counter ; [CPU_] |183| 
	.dwpsn	file "../gateway.c",line 184,column 6,is_stmt
        MOVW      DP,#_make10ms         ; [CPU_U] 
        MOV       @_make10ms,#0         ; [CPU_] |184| 
	.dwpsn	file "../gateway.c",line 186,column 4,is_stmt
        B         $C$L12,UNC            ; [CPU_] |186| 
        ; branch occurs ; [] |186| 
$C$L11:    
	.dwpsn	file "../gateway.c",line 189,column 5,is_stmt
        INC       @_make10ms            ; [CPU_] |189| 
$C$L12:    
	.dwpsn	file "../gateway.c",line 192,column 3,is_stmt
        DEC       @_Sync_count          ; [CPU_] |192| 
	.dwpsn	file "../gateway.c",line 193,column 3,is_stmt
        MOV       AL,@_Sync_count       ; [CPU_] |193| 
        BF        $C$L25,NEQ            ; [CPU_] |193| 
        ; branchcc occurs ; [] |193| 
	.dwpsn	file "../gateway.c",line 194,column 5,is_stmt
        MOV       @_Modules_Present,#0  ; [CPU_] |194| 
	.dwpsn	file "../gateway.c",line 195,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |195| 
        CMPB      AL,#70                ; [CPU_] |195| 
        B         $C$L20,GEQ            ; [CPU_] |195| 
        ; branchcc occurs ; [] |195| 
$C$L13:    
	.dwpsn	file "../gateway.c",line 196,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |196| 
        MOV       ACC,*-SP[3]           ; [CPU_] |196| 
        ADDL      XAR4,ACC              ; [CPU_] |196| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |196| 
        CMPB      AL,#255               ; [CPU_] |196| 
        BF        $C$L19,EQ             ; [CPU_] |196| 
        ; branchcc occurs ; [] |196| 
	.dwpsn	file "../gateway.c",line 197,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |197| 
        MOVL      XAR4,#_ODV_Modules_MinCellVoltage ; [CPU_U] |197| 
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        ADDL      XAR4,ACC              ; [CPU_] |197| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |197| 
        CMP       AL,@_GAT_min          ; [CPU_] |197| 
        B         $C$L14,HIS            ; [CPU_] |197| 
        ; branchcc occurs ; [] |197| 
	.dwpsn	file "../gateway.c",line 197,column 54,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |197| 
        MOVL      XAR7,#_ODV_Modules_MinCellVoltage ; [CPU_U] |197| 
        ADDL      XAR7,ACC              ; [CPU_] |197| 
        MOV       AL,*XAR7              ; [CPU_] |197| 
        MOV       @_GAT_min,AL          ; [CPU_] |197| 
$C$L14:    
	.dwpsn	file "../gateway.c",line 198,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |198| 
        MOVL      XAR4,#_ODV_Modules_MaxCellVoltage ; [CPU_U] |198| 
        ADDL      XAR4,ACC              ; [CPU_] |198| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |198| 
        CMP       AL,@_GAT_max          ; [CPU_] |198| 
        B         $C$L15,LOS            ; [CPU_] |198| 
        ; branchcc occurs ; [] |198| 
	.dwpsn	file "../gateway.c",line 198,column 54,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |198| 
        MOVL      XAR7,#_ODV_Modules_MaxCellVoltage ; [CPU_U] |198| 
        ADDL      XAR7,ACC              ; [CPU_] |198| 
        MOV       AL,*XAR7              ; [CPU_] |198| 
        MOV       @_GAT_max,AL          ; [CPU_] |198| 
$C$L15:    
	.dwpsn	file "../gateway.c",line 199,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |199| 
        MOVL      XAR4,#_ODV_Modules_Temperature ; [CPU_U] |199| 
        ADDL      XAR4,ACC              ; [CPU_] |199| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |199| 
        CMP       AL,*-SP[4]            ; [CPU_] |199| 
        B         $C$L16,LEQ            ; [CPU_] |199| 
        ; branchcc occurs ; [] |199| 
	.dwpsn	file "../gateway.c",line 199,column 51,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |199| 
        MOVL      XAR7,#_ODV_Modules_Temperature ; [CPU_U] |199| 
        ADDL      XAR7,ACC              ; [CPU_] |199| 
        MOV       AL,*XAR7              ; [CPU_] |199| 
        MOV       *-SP[4],AL            ; [CPU_] |199| 
$C$L16:    
	.dwpsn	file "../gateway.c",line 200,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |200| 
        MOVL      XAR4,#_ODV_Modules_Temperature ; [CPU_U] |200| 
        ADDL      XAR4,ACC              ; [CPU_] |200| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |200| 
        CMP       AL,*-SP[5]            ; [CPU_] |200| 
        B         $C$L17,GEQ            ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
	.dwpsn	file "../gateway.c",line 200,column 51,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |200| 
        MOVL      XAR7,#_ODV_Modules_Temperature ; [CPU_U] |200| 
        ADDL      XAR7,ACC              ; [CPU_] |200| 
        MOV       AL,*XAR7              ; [CPU_] |200| 
        MOV       *-SP[5],AL            ; [CPU_] |200| 
$C$L17:    
	.dwpsn	file "../gateway.c",line 201,column 3,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |201| 
        MOVL      XAR4,#_ODV_Modules_Heater ; [CPU_U] |201| 
        ADDL      XAR4,ACC              ; [CPU_] |201| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |201| 
        ADD       @_Heater_status,AL    ; [CPU_] |201| 
	.dwpsn	file "../gateway.c",line 205,column 9,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |205| 
        BF        $C$L19,EQ             ; [CPU_] |205| 
        ; branchcc occurs ; [] |205| 
	.dwpsn	file "../gateway.c",line 206,column 11,is_stmt
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        INC       @_Modules_Present     ; [CPU_] |206| 
	.dwpsn	file "../gateway.c",line 207,column 11,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |207| 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |207| 
        ADDL      XAR4,ACC              ; [CPU_] |207| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |207| 
        BF        $C$L18,EQ             ; [CPU_] |207| 
        ; branchcc occurs ; [] |207| 
        MOV       ACC,*-SP[3]           ; [CPU_] |207| 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |207| 
        ADDL      XAR4,ACC              ; [CPU_] |207| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |207| 
        CMPB      AL,#255               ; [CPU_] |207| 
        B         $C$L18,HIS            ; [CPU_] |207| 
        ; branchcc occurs ; [] |207| 
	.dwpsn	file "../gateway.c",line 209,column 13,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |209| 
        MOVZ      AR4,*-SP[3]           ; [CPU_] |209| 
        MOVL      XAR5,#_ODV_Modules_Alarms ; [CPU_U] |209| 
        MOV       T,#24                 ; [CPU_] |209| 
        MOV       PL,#0                 ; [CPU_] |209| 
        MOV       PH,#32768             ; [CPU_] |209| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        ADDL      XAR5,ACC              ; [CPU_] |209| 
        ADDB      XAR4,#1               ; [CPU_] |209| 
        MOV       ACC,AR4               ; [CPU_] |209| 
        LSLL      ACC,T                 ; [CPU_] |209| 
        ADDU      ACC,*+XAR5[0]         ; [CPU_] |209| 
        ADDL      P,ACC                 ; [CPU_] |209| 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,P ; [CPU_] |209| 
	.dwpsn	file "../gateway.c",line 210,column 13,is_stmt
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |210| 
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$229, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |210| 
        ; call occurs [#_ERR_SetError] ; [] |210| 
	.dwpsn	file "../gateway.c",line 211,column 11,is_stmt
        B         $C$L19,UNC            ; [CPU_] |211| 
        ; branch occurs ; [] |211| 
$C$L18:    
	.dwpsn	file "../gateway.c",line 214,column 12,is_stmt
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$230, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |214| 
        ; call occurs [#_ERR_ClearError] ; [] |214| 
$C$L19:    
	.dwpsn	file "../gateway.c",line 218,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |218| 
        MOV       ACC,*-SP[3]           ; [CPU_] |218| 
        ADDL      XAR4,ACC              ; [CPU_] |218| 
        MOVB      *+XAR4[0],#255,UNC    ; [CPU_] |218| 
	.dwpsn	file "../gateway.c",line 219,column 7,is_stmt
        INC       *-SP[3]               ; [CPU_] |219| 
	.dwpsn	file "../gateway.c",line 195,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |195| 
        CMPB      AL,#70                ; [CPU_] |195| 
        B         $C$L13,LT             ; [CPU_] |195| 
        ; branchcc occurs ; [] |195| 
$C$L20:    
	.dwpsn	file "../gateway.c",line 222,column 2,is_stmt
        MOVW      DP,#_Heater_status    ; [CPU_U] 
        MOV       AL,@_Heater_status    ; [CPU_] |222| 
        BF        $C$L21,EQ             ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
	.dwpsn	file "../gateway.c",line 224,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOVB      @_ODV_Gateway_Heater_Status,#1,UNC ; [CPU_] |224| 
	.dwpsn	file "../gateway.c",line 225,column 2,is_stmt
        B         $C$L22,UNC            ; [CPU_] |225| 
        ; branch occurs ; [] |225| 
$C$L21:    
	.dwpsn	file "../gateway.c",line 228,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Gateway_Heater_Status,#0 ; [CPU_] |228| 
$C$L22:    
	.dwpsn	file "../gateway.c",line 231,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |231| 
        MOVW      DP,#_ODV_Gateway_MaxModTemp ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxModTemp,AL ; [CPU_] |231| 
	.dwpsn	file "../gateway.c",line 232,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |232| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        MOV       @_ODV_Gateway_MinModTemp,AL ; [CPU_] |232| 
	.dwpsn	file "../gateway.c",line 233,column 5,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        MOV       AL,@_GAT_min          ; [CPU_] |233| 
        MOVW      DP,#_ODV_Gateway_MinCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MinCellVoltage,AL ; [CPU_] |233| 
	.dwpsn	file "../gateway.c",line 234,column 5,is_stmt
        MOVW      DP,#_GAT_max          ; [CPU_U] 
        MOV       AL,@_GAT_max          ; [CPU_] |234| 
        MOVW      DP,#_ODV_Gateway_MaxCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxCellVoltage,AL ; [CPU_] |234| 
	.dwpsn	file "../gateway.c",line 235,column 5,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        SUB       AL,@_GAT_min          ; [CPU_] |235| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxDeltaCellVoltage,AL ; [CPU_] |235| 
	.dwpsn	file "../gateway.c",line 237,column 5,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax_bal_delta ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax_bal_delta ; [CPU_] |237| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_] |237| 
        B         $C$L23,HIS            ; [CPU_] |237| 
        ; branchcc occurs ; [] |237| 
	.dwpsn	file "../gateway.c",line 239,column 6,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Current ; [CPU_] |239| 
        MOV       AL,AH                 ; [CPU_] |239| 
        SETC      SXM                   ; [CPU_] 
        ASR       AL,1                  ; [CPU_] |239| 
        LSR       AL,14                 ; [CPU_] |239| 
        ADD       AL,AH                 ; [CPU_] |239| 
        ASR       AL,2                  ; [CPU_] |239| 
        MOV       ACC,AL                ; [CPU_] |239| 
        MOV32     R0H,ACC               ; [CPU_] |239| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R0H,R0H               ; [CPU_] |239| 
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$231, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |239| 
        ; call occurs [#_CNV_Round] ; [] |239| 
        MOVL      XAR6,ACC              ; [CPU_] |239| 
        MOVB      ACC,#3                ; [CPU_] |239| 
        CMPL      ACC,XAR6              ; [CPU_] |239| 
        B         $C$L24,GEQ            ; [CPU_] |239| 
        ; branchcc occurs ; [] |239| 
	.dwpsn	file "../gateway.c",line 241,column 7,is_stmt
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_ERR_DELTA_Voltage")
	.dwattr $C$DW$232, DW_AT_TI_call
        LCR       #_ERR_DELTA_Voltage   ; [CPU_] |241| 
        ; call occurs [#_ERR_DELTA_Voltage] ; [] |241| 
	.dwpsn	file "../gateway.c",line 243,column 5,is_stmt
        B         $C$L24,UNC            ; [CPU_] |243| 
        ; branch occurs ; [] |243| 
$C$L23:    
	.dwpsn	file "../gateway.c",line 244,column 10,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin_bal_delta ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin_bal_delta ; [CPU_] |244| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_] |244| 
        B         $C$L24,HIS            ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "../gateway.c",line 246,column 6,is_stmt
        MOV       AL,#0                 ; [CPU_] |246| 
        MOV       AH,#128               ; [CPU_] |246| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$233, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |246| 
        ; call occurs [#_ERR_HandleWarning] ; [] |246| 
$C$L24:    
	.dwpsn	file "../gateway.c",line 249,column 5,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        MOV       @_GAT_min,#65535      ; [CPU_] |249| 
	.dwpsn	file "../gateway.c",line 250,column 5,is_stmt
        MOV       @_GAT_max,#0          ; [CPU_] |250| 
	.dwpsn	file "../gateway.c",line 251,column 5,is_stmt
        MOVB      @_Sync_count,#71,UNC  ; [CPU_] |251| 
	.dwpsn	file "../gateway.c",line 253,column 1,is_stmt
$C$L25:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$223, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$223, DW_AT_TI_end_line(0xfd)
	.dwattr $C$DW$223, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$223

	.sect	".text"
	.global	_FnStopNode

$C$DW$235	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStopNode")
	.dwattr $C$DW$235, DW_AT_low_pc(_FnStopNode)
	.dwattr $C$DW$235, DW_AT_high_pc(0x00)
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_FnStopNode")
	.dwattr $C$DW$235, DW_AT_external
	.dwattr $C$DW$235, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$235, DW_AT_TI_begin_line(0x100)
	.dwattr $C$DW$235, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$235, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 256,column 29,is_stmt,address _FnStopNode

	.dwfde $C$DW$CIE, _FnStopNode
$C$DW$236	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStopNode                   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStopNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |256| 
	.dwpsn	file "../gateway.c",line 258,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |258| 
	.dwpsn	file "../gateway.c",line 259,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$238	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$238, DW_AT_low_pc(0x00)
	.dwattr $C$DW$238, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$235, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$235, DW_AT_TI_end_line(0x103)
	.dwattr $C$DW$235, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$235

	.sect	".text"
	.global	_canOpenInit

$C$DW$239	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$239, DW_AT_low_pc(_canOpenInit)
	.dwattr $C$DW$239, DW_AT_high_pc(0x00)
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$239, DW_AT_external
	.dwattr $C$DW$239, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$239, DW_AT_TI_begin_line(0x107)
	.dwattr $C$DW$239, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$239, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../gateway.c",line 265,column 1,is_stmt,address _canOpenInit

	.dwfde $C$DW$CIE, _canOpenInit

;***************************************************************
;* FNAME: _canOpenInit                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_canOpenInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../gateway.c",line 267,column 3,is_stmt
        MOVL      XAR4,#_ODI_gateway_dict_Data ; [CPU_U] |267| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      @_BoardODdata,XAR4    ; [CPU_] |267| 
	.dwpsn	file "../gateway.c",line 269,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |269| 
        MOVB      XAR0,#84              ; [CPU_] |269| 
        MOVL      XAR4,#_FnInitialiseNode ; [CPU_U] |269| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |269| 
	.dwpsn	file "../gateway.c",line 270,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |270| 
        MOVB      XAR0,#86              ; [CPU_] |270| 
        MOVL      XAR4,#_FnEnterPreOperational ; [CPU_U] |270| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |270| 
	.dwpsn	file "../gateway.c",line 271,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |271| 
        MOVB      XAR0,#88              ; [CPU_] |271| 
        MOVL      XAR4,#_FnStartNode    ; [CPU_U] |271| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |271| 
	.dwpsn	file "../gateway.c",line 272,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |272| 
        MOVB      XAR0,#90              ; [CPU_] |272| 
        MOVL      XAR4,#_FnStopNode     ; [CPU_U] |272| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |272| 
	.dwpsn	file "../gateway.c",line 273,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |273| 
        MOVB      XAR0,#92              ; [CPU_] |273| 
        MOVL      XAR4,#_FnResetNode    ; [CPU_U] |273| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |273| 
	.dwpsn	file "../gateway.c",line 274,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |274| 
        MOVB      XAR0,#94              ; [CPU_] |274| 
        MOVL      XAR4,#_FnResetCommunications ; [CPU_U] |274| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |274| 
	.dwpsn	file "../gateway.c",line 276,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |276| 
        MOVB      XAR0,#252             ; [CPU_] |276| 
        MOVL      XAR4,#_PAR_StoreODSubIndex ; [CPU_U] |276| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |276| 
	.dwpsn	file "../gateway.c",line 278,column 3,is_stmt
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |278| 
        MOVB      ACC,#0                ; [CPU_] |278| 
        MOVB      XAR0,#254             ; [CPU_] |278| 
        MOVL      *+XAR4[AR0],ACC       ; [CPU_] |278| 
	.dwpsn	file "../gateway.c",line 279,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |279| 
        MOVB      XAR0,#242             ; [CPU_] |279| 
        MOVL      XAR4,#_PreSync        ; [CPU_U] |279| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |279| 
	.dwpsn	file "../gateway.c",line 282,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |282| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |282| 
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_setState")
	.dwattr $C$DW$240, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |282| 
        ; call occurs [#_setState] ; [] |282| 
	.dwpsn	file "../gateway.c",line 283,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |283| 
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_name("_FnResetCommunications")
	.dwattr $C$DW$241, DW_AT_TI_call
        LCR       #_FnResetCommunications ; [CPU_] |283| 
        ; call occurs [#_FnResetCommunications] ; [] |283| 
	.dwpsn	file "../gateway.c",line 290,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#5                 ; [CPU_] |290| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |290| 
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_name("_setState")
	.dwattr $C$DW$242, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |290| 
        ; call occurs [#_setState] ; [] |290| 
	.dwpsn	file "../gateway.c",line 292,column 1,is_stmt
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$239, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$239, DW_AT_TI_end_line(0x124)
	.dwattr $C$DW$239, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$239

	.sect	".text"
	.global	_taskFnGestionMachine

$C$DW$244	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnGestionMachine")
	.dwattr $C$DW$244, DW_AT_low_pc(_taskFnGestionMachine)
	.dwattr $C$DW$244, DW_AT_high_pc(0x00)
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_taskFnGestionMachine")
	.dwattr $C$DW$244, DW_AT_external
	.dwattr $C$DW$244, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$244, DW_AT_TI_begin_line(0x12e)
	.dwattr $C$DW$244, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$244, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 302,column 32,is_stmt,address _taskFnGestionMachine

	.dwfde $C$DW$CIE, _taskFnGestionMachine

;***************************************************************
;* FNAME: _taskFnGestionMachine         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_taskFnGestionMachine:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$245	.dwtag  DW_TAG_variable, DW_AT_name("old_time")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_old_time")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../gateway.c",line 308,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |308| 
        MOVB      AL,#2                 ; [CPU_] |308| 
$C$DW$246	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$246, DW_AT_low_pc(0x00)
	.dwattr $C$DW$246, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$246, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |308| 
        ; call occurs [#_SEM_pend] ; [] |308| 
	.dwpsn	file "../gateway.c",line 309,column 3,is_stmt
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("_HAL_Init")
	.dwattr $C$DW$247, DW_AT_TI_call
        LCR       #_HAL_Init            ; [CPU_] |309| 
        ; call occurs [#_HAL_Init] ; [] |309| 
	.dwpsn	file "../gateway.c",line 310,column 3,is_stmt
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_name("_ADS_Init")
	.dwattr $C$DW$248, DW_AT_TI_call
        LCR       #_ADS_Init            ; [CPU_] |310| 
        ; call occurs [#_ADS_Init] ; [] |310| 
	.dwpsn	file "../gateway.c",line 311,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |311| 
        BF        $C$L27,NEQ            ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
$C$L26:    
	.dwpsn	file "../gateway.c",line 312,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |312| 
        MOVB      AL,#1                 ; [CPU_] |312| 
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$249, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |312| 
        ; call occurs [#_SEM_pend] ; [] |312| 
	.dwpsn	file "../gateway.c",line 311,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |311| 
        BF        $C$L26,EQ             ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
$C$L27:    
	.dwpsn	file "../gateway.c",line 314,column 3,is_stmt
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_name("_USB_Start")
	.dwattr $C$DW$250, DW_AT_TI_call
        LCR       #_USB_Start           ; [CPU_] |314| 
        ; call occurs [#_USB_Start] ; [] |314| 
	.dwpsn	file "../gateway.c",line 315,column 3,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |315| 
        MOVL      *-SP[2],ACC           ; [CPU_] |315| 
	.dwpsn	file "../gateway.c",line 316,column 9,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      ACC,@_BootCommand     ; [CPU_] |316| 
        BF        $C$L30,NEQ            ; [CPU_] |316| 
        ; branchcc occurs ; [] |316| 
$C$L28:    
	.dwpsn	file "../gateway.c",line 320,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVB      XAR6,#63              ; [CPU_] |320| 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |320| 
        MOVB      AH,#0                 ; [CPU_] |320| 
        ANDB      AL,#0x3f              ; [CPU_] |320| 
        CMPL      ACC,XAR6              ; [CPU_] |320| 
        BF        $C$L29,NEQ            ; [CPU_] |320| 
        ; branchcc occurs ; [] |320| 
        MOVB      XAR6,#62              ; [CPU_] |320| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |320| 
        MOVB      AH,#0                 ; [CPU_] |320| 
        ANDB      AL,#0x3f              ; [CPU_] |320| 
        CMPL      ACC,XAR6              ; [CPU_] |320| 
        BF        $C$L29,NEQ            ; [CPU_] |320| 
        ; branchcc occurs ; [] |320| 
	.dwpsn	file "../gateway.c",line 323,column 7,is_stmt
        MOV       AL,#-1                ; [CPU_] |323| 
$C$DW$251	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$251, DW_AT_low_pc(0x00)
	.dwattr $C$DW$251, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$251, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |323| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |323| 
	.dwpsn	file "../gateway.c",line 324,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |324| 
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$252, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |324| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |324| 
$C$L29:    
	.dwpsn	file "../gateway.c",line 326,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |326| 
        MOVL      *-SP[2],ACC           ; [CPU_] |326| 
	.dwpsn	file "../gateway.c",line 327,column 5,is_stmt
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_name("_USB_Unlock")
	.dwattr $C$DW$253, DW_AT_TI_call
        LCR       #_USB_Unlock          ; [CPU_] |327| 
        ; call occurs [#_USB_Unlock] ; [] |327| 
	.dwpsn	file "../gateway.c",line 328,column 5,is_stmt
        MOVB      AL,#2                 ; [CPU_] |328| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |328| 
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$254, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |328| 
        ; call occurs [#_SEM_pend] ; [] |328| 
	.dwpsn	file "../gateway.c",line 316,column 9,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      ACC,@_BootCommand     ; [CPU_] |316| 
        BF        $C$L28,EQ             ; [CPU_] |316| 
        ; branchcc occurs ; [] |316| 
$C$L30:    
	.dwpsn	file "../gateway.c",line 330,column 3,is_stmt
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_name("_USB_Stop")
	.dwattr $C$DW$255, DW_AT_TI_call
        LCR       #_USB_Stop            ; [CPU_] |330| 
        ; call occurs [#_USB_Stop] ; [] |330| 
	.dwpsn	file "../gateway.c",line 331,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |331| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |331| 
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$256, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |331| 
        ; call occurs [#_SEM_pend] ; [] |331| 
	.dwpsn	file "../gateway.c",line 332,column 3,is_stmt
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_name("_HAL_Reset")
	.dwattr $C$DW$257, DW_AT_TI_call
        LCR       #_HAL_Reset           ; [CPU_] |332| 
        ; call occurs [#_HAL_Reset] ; [] |332| 
	.dwpsn	file "../gateway.c",line 333,column 3,is_stmt
        MOV       AL,#28009             ; [CPU_] |333| 
        MOV       AH,#21093             ; [CPU_] |333| 
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      @_BootCommand,ACC     ; [CPU_] |333| 
	.dwpsn	file "../gateway.c",line 334,column 3,is_stmt
 LB 0x3F7FF6 
	.dwpsn	file "../gateway.c",line 335,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$258	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$258, DW_AT_low_pc(0x00)
	.dwattr $C$DW$258, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$244, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$244, DW_AT_TI_end_line(0x14f)
	.dwattr $C$DW$244, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$244

	.sect	".text"
	.global	_TaskSciSendReceive

$C$DW$259	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskSciSendReceive")
	.dwattr $C$DW$259, DW_AT_low_pc(_TaskSciSendReceive)
	.dwattr $C$DW$259, DW_AT_high_pc(0x00)
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_TaskSciSendReceive")
	.dwattr $C$DW$259, DW_AT_external
	.dwattr $C$DW$259, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$259, DW_AT_TI_begin_line(0x15f)
	.dwattr $C$DW$259, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$259, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../gateway.c",line 351,column 30,is_stmt,address _TaskSciSendReceive

	.dwfde $C$DW$CIE, _TaskSciSendReceive

;***************************************************************
;* FNAME: _TaskSciSendReceive           FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 21 Auto,  0 SOE     *
;***************************************************************

_TaskSciSendReceive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$260	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_breg20 -4]
$C$DW$261	.dwtag  DW_TAG_variable, DW_AT_name("insulation_counter")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_insulation_counter")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_breg20 -6]
$C$DW$262	.dwtag  DW_TAG_variable, DW_AT_name("current_counter")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_current_counter")
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$262, DW_AT_location[DW_OP_breg20 -8]
$C$DW$263	.dwtag  DW_TAG_variable, DW_AT_name("voltage_counter")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_voltage_counter")
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$263, DW_AT_location[DW_OP_breg20 -10]
$C$DW$264	.dwtag  DW_TAG_variable, DW_AT_name("sleep_counter")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_sleep_counter")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_breg20 -12]
$C$DW$265	.dwtag  DW_TAG_variable, DW_AT_name("current_counter2")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_current_counter2")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_breg20 -14]
$C$DW$266	.dwtag  DW_TAG_variable, DW_AT_name("reset_relay")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_reset_relay")
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$266, DW_AT_location[DW_OP_breg20 -16]
$C$DW$267	.dwtag  DW_TAG_variable, DW_AT_name("com_counter")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_com_counter")
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$267, DW_AT_location[DW_OP_breg20 -17]
$C$DW$268	.dwtag  DW_TAG_variable, DW_AT_name("cur")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_cur")
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$268, DW_AT_location[DW_OP_breg20 -18]
$C$DW$269	.dwtag  DW_TAG_variable, DW_AT_name("volt")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_volt")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_breg20 -19]
$C$DW$270	.dwtag  DW_TAG_variable, DW_AT_name("curf")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_curf")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_breg20 -22]
$C$DW$271	.dwtag  DW_TAG_variable, DW_AT_name("checkenable")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_checkenable")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$271, DW_AT_location[DW_OP_breg20 -23]
	.dwpsn	file "../gateway.c",line 352,column 45,is_stmt
        MOVB      ACC,#0                ; [CPU_] |352| 
        MOVL      *-SP[6],ACC           ; [CPU_] |352| 
	.dwpsn	file "../gateway.c",line 352,column 66,is_stmt
        MOVL      *-SP[8],ACC           ; [CPU_] |352| 
	.dwpsn	file "../gateway.c",line 352,column 87,is_stmt
        MOVL      *-SP[10],ACC          ; [CPU_] |352| 
	.dwpsn	file "../gateway.c",line 352,column 106,is_stmt
        MOVL      *-SP[12],ACC          ; [CPU_] |352| 
	.dwpsn	file "../gateway.c",line 352,column 128,is_stmt
        MOVL      *-SP[14],ACC          ; [CPU_] |352| 
	.dwpsn	file "../gateway.c",line 352,column 145,is_stmt
        MOVL      *-SP[16],ACC          ; [CPU_] |352| 
	.dwpsn	file "../gateway.c",line 353,column 22,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |353| 
	.dwpsn	file "../gateway.c",line 356,column 21,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |356| 
	.dwpsn	file "../gateway.c",line 357,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#-1 ; [CPU_] |357| 
	.dwpsn	file "../gateway.c",line 358,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |358| 
        BF        $C$L32,NEQ            ; [CPU_] |358| 
        ; branchcc occurs ; [] |358| 
$C$L31:    
	.dwpsn	file "../gateway.c",line 359,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |359| 
        MOVB      AL,#1                 ; [CPU_] |359| 
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$272, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |359| 
        ; call occurs [#_SEM_pend] ; [] |359| 
	.dwpsn	file "../gateway.c",line 358,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |358| 
        BF        $C$L31,EQ             ; [CPU_] |358| 
        ; branchcc occurs ; [] |358| 
$C$L32:    
	.dwpsn	file "../gateway.c",line 361,column 10,is_stmt
        CMPB      AL,#1                 ; [CPU_] |361| 
        BF        $C$L34,EQ             ; [CPU_] |361| 
        ; branchcc occurs ; [] |361| 
$C$L33:    
	.dwpsn	file "../gateway.c",line 363,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |363| 
        MOVB      AL,#1                 ; [CPU_] |363| 
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$273, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |363| 
        ; call occurs [#_SEM_pend] ; [] |363| 
	.dwpsn	file "../gateway.c",line 361,column 10,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |361| 
        CMPB      AL,#1                 ; [CPU_] |361| 
        BF        $C$L33,NEQ            ; [CPU_] |361| 
        ; branchcc occurs ; [] |361| 
$C$L34:    
	.dwpsn	file "../gateway.c",line 365,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |365| 
        MOVB      AL,#10                ; [CPU_] |365| 
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$274, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |365| 
        ; call occurs [#_SEM_pend] ; [] |365| 
	.dwpsn	file "../gateway.c",line 366,column 3,is_stmt
        MOVW      DP,#_ODV_MachineEvent ; [CPU_U] 
        MOV       @_ODV_MachineEvent,#0 ; [CPU_] |366| 
	.dwpsn	file "../gateway.c",line 367,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |367| 
	.dwpsn	file "../gateway.c",line 368,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |368| 
	.dwpsn	file "../gateway.c",line 369,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |369| 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 370,column 3,is_stmt
        MOVB      XAR5,#0               ; [CPU_] |370| 
        MOVB      AL,#9                 ; [CPU_] |370| 
        MOVB      AH,#7                 ; [CPU_] |370| 
        MOVL      XAR4,#_ODV_Gateway_Date_Time ; [CPU_U] |370| 
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$275, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |370| 
        ; call occurs [#_I2C_Command] ; [] |370| 
	.dwpsn	file "../gateway.c",line 371,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |371| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |371| 
        BF        $C$L35,NTC            ; [CPU_] |371| 
        ; branchcc occurs ; [] |371| 
	.dwpsn	file "../gateway.c",line 372,column 5,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xffbf ; [CPU_] |372| 
$C$L35:    
	.dwpsn	file "../gateway.c",line 374,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |374| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |374| 
        BF        $C$L36,TC             ; [CPU_] |374| 
        ; branchcc occurs ; [] |374| 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |374| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |374| 
        LSR       AL,1                  ; [CPU_] |374| 
        CMPB      AL,#1                 ; [CPU_] |374| 
        BF        $C$L36,NEQ            ; [CPU_] |374| 
        ; branchcc occurs ; [] |374| 
	.dwpsn	file "../gateway.c",line 375,column 5,is_stmt
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOV       @_ODP_CommError_TimeOut,#0 ; [CPU_] |375| 
	.dwpsn	file "../gateway.c",line 376,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |376| 
$C$L36:    
	.dwpsn	file "../gateway.c",line 379,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |379| 
        MOV       AL,#500               ; [CPU_] |379| 
$C$DW$276	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$276, DW_AT_low_pc(0x00)
	.dwattr $C$DW$276, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$276, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |379| 
        ; call occurs [#_SEM_pend] ; [] |379| 
	.dwpsn	file "../gateway.c",line 380,column 3,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |380| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        MOV       @_Modules_Present,AL  ; [CPU_] |380| 
	.dwpsn	file "../gateway.c",line 381,column 10,is_stmt
$C$L37:    
	.dwpsn	file "../gateway.c",line 383,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |383| 
        ANDB      AL,#0x0f              ; [CPU_] |383| 
        CMPB      AL,#8                 ; [CPU_] |383| 
        BF        $C$L38,NEQ            ; [CPU_] |383| 
        ; branchcc occurs ; [] |383| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |383| 
        CMPB      AL,#8                 ; [CPU_] |383| 
        BF        $C$L38,EQ             ; [CPU_] |383| 
        ; branchcc occurs ; [] |383| 
	.dwpsn	file "../gateway.c",line 384,column 7,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |384| 
        MOVL      *-SP[4],ACC           ; [CPU_] |384| 
	.dwpsn	file "../gateway.c",line 385,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#8,UNC ; [CPU_] |385| 
$C$L38:    
	.dwpsn	file "../gateway.c",line 387,column 5,is_stmt
        MOVW      DP,#_Sync_count       ; [CPU_U] 
        MOV       AL,@_Sync_count       ; [CPU_] |387| 
        CMPB      AL,#71                ; [CPU_] |387| 
        BF        $C$L40,NEQ            ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
	.dwpsn	file "../gateway.c",line 388,column 7,is_stmt
        DEC       @_Sync_count          ; [CPU_] |388| 
	.dwpsn	file "../gateway.c",line 389,column 7,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |389| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        CMP       AL,@_Modules_Present  ; [CPU_] |389| 
        BF        $C$L39,EQ             ; [CPU_] |389| 
        ; branchcc occurs ; [] |389| 
	.dwpsn	file "../gateway.c",line 390,column 9,is_stmt
        INC       *-SP[17]              ; [CPU_] |390| 
	.dwpsn	file "../gateway.c",line 391,column 9,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |391| 
        CMPB      AL,#6                 ; [CPU_] |391| 
        BF        $C$L40,NEQ            ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |391| 
        BF        $C$L40,NEQ            ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
	.dwpsn	file "../gateway.c",line 392,column 11,is_stmt
        MOV       AL,#128               ; [CPU_] |392| 
        MOV       AH,#32768             ; [CPU_] |392| 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |392| 
	.dwpsn	file "../gateway.c",line 393,column 11,is_stmt
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |393| 
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$277, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |393| 
        ; call occurs [#_ERR_SetError] ; [] |393| 
	.dwpsn	file "../gateway.c",line 395,column 7,is_stmt
        B         $C$L40,UNC            ; [CPU_] |395| 
        ; branch occurs ; [] |395| 
$C$L39:    
	.dwpsn	file "../gateway.c",line 396,column 12,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |396| 
        BF        $C$L40,EQ             ; [CPU_] |396| 
        ; branchcc occurs ; [] |396| 
	.dwpsn	file "../gateway.c",line 397,column 9,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |397| 
$C$L40:    
	.dwpsn	file "../gateway.c",line 400,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |400| 
        MOV       AL,AH                 ; [CPU_] |400| 
        ASR       AL,1                  ; [CPU_] |400| 
        LSR       AL,14                 ; [CPU_] |400| 
        ADD       AL,AH                 ; [CPU_] |400| 
        ASR       AL,2                  ; [CPU_] |400| 
        MOV       *-SP[19],AL           ; [CPU_] |400| 
	.dwpsn	file "../gateway.c",line 401,column 5,is_stmt
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |401| 
        MOV       AL,AH                 ; [CPU_] |401| 
        MOVW      DP,#_ODV_Gateway_Test_Voltage ; [CPU_U] 
        LSR       AL,15                 ; [CPU_] |401| 
        ADD       AL,AH                 ; [CPU_] |401| 
        ASR       AL,1                  ; [CPU_] |401| 
        MOV       @_ODV_Gateway_Test_Voltage,AL ; [CPU_] |401| 
	.dwpsn	file "../gateway.c",line 402,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |402| 
        B         $C$L105,LEQ           ; [CPU_] |402| 
        ; branchcc occurs ; [] |402| 
        CMPB      AL,#4                 ; [CPU_] |402| 
        B         $C$L105,GT            ; [CPU_] |402| 
        ; branchcc occurs ; [] |402| 
	.dwpsn	file "../gateway.c",line 403,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |403| 
        BF        $C$L42,NEQ            ; [CPU_] |403| 
        ; branchcc occurs ; [] |403| 
	.dwpsn	file "../gateway.c",line 404,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |404| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |404| 
        BF        $C$L41,TC             ; [CPU_] |404| 
        ; branchcc occurs ; [] |404| 
	.dwpsn	file "../gateway.c",line 405,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |405| 
$C$L41:    
	.dwpsn	file "../gateway.c",line 406,column 9,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |406| 
	.dwpsn	file "../gateway.c",line 407,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |407| 
	.dwpsn	file "../gateway.c",line 408,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |408| 
$C$L42:    
	.dwpsn	file "../gateway.c",line 425,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOVIZ     R1H,#16512            ; [CPU_] |425| 
        I16TOF32  R0H,@_ODV_Gateway_Current ; [CPU_] |425| 
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$278, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |425| 
        ; call occurs [#FS$$DIV] ; [] |425| 
        MOV32     *-SP[22],R0H          ; [CPU_] |425| 
	.dwpsn	file "../gateway.c",line 426,column 7,is_stmt
$C$DW$279	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$279, DW_AT_low_pc(0x00)
	.dwattr $C$DW$279, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$279, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |426| 
        ; call occurs [#_CNV_Round] ; [] |426| 
        MOV       *-SP[18],AL           ; [CPU_] |426| 
	.dwpsn	file "../gateway.c",line 428,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |428| 
        CMP       AL,*-SP[18]           ; [CPU_] |428| 
        B         $C$L43,LT             ; [CPU_] |428| 
        ; branchcc occurs ; [] |428| 
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |428| 
        CMP       AL,*-SP[18]           ; [CPU_] |428| 
        B         $C$L44,LEQ            ; [CPU_] |428| 
        ; branchcc occurs ; [] |428| 
$C$L43:    
	.dwpsn	file "../gateway.c",line 430,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |430| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        ADDL      @_GV_current_counter,ACC ; [CPU_] |430| 
	.dwpsn	file "../gateway.c",line 431,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |431| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |431| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        CMPL      ACC,@_GV_current_counter ; [CPU_] |431| 
        B         $C$L44,HIS            ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
	.dwpsn	file "../gateway.c",line 432,column 9,is_stmt
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$280, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |432| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |432| 
$C$L44:    
	.dwpsn	file "../gateway.c",line 436,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |436| 
        CMP       AL,*-SP[19]           ; [CPU_] |436| 
        B         $C$L45,GEQ            ; [CPU_] |436| 
        ; branchcc occurs ; [] |436| 
	.dwpsn	file "../gateway.c",line 438,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |438| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        ADDL      @_GV_voltage_counter,ACC ; [CPU_] |438| 
	.dwpsn	file "../gateway.c",line 439,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |439| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |439| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        CMPL      ACC,@_GV_voltage_counter ; [CPU_] |439| 
        B         $C$L45,HIS            ; [CPU_] |439| 
        ; branchcc occurs ; [] |439| 
	.dwpsn	file "../gateway.c",line 440,column 9,is_stmt
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$281, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |440| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |440| 
$C$L45:    
	.dwpsn	file "../gateway.c",line 445,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |445| 
        CMP       AL,*-SP[19]           ; [CPU_] |445| 
        B         $C$L46,LEQ            ; [CPU_] |445| 
        ; branchcc occurs ; [] |445| 
	.dwpsn	file "../gateway.c",line 450,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |450| 
	.dwpsn	file "../gateway.c",line 451,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |451| 
        MOVL      *-SP[4],ACC           ; [CPU_] |451| 
	.dwpsn	file "../gateway.c",line 452,column 9,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |452| 
$C$L46:    
	.dwpsn	file "../gateway.c",line 457,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |457| 
        CMP       AL,*-SP[19]           ; [CPU_] |457| 
        B         $C$L47,LEQ            ; [CPU_] |457| 
        ; branchcc occurs ; [] |457| 
	.dwpsn	file "../gateway.c",line 459,column 5,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |459| 
        MOVL      ACC,XAR4              ; [CPU_] |459| 
$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$282, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |459| 
        ; call occurs [#_ERR_HandleWarning] ; [] |459| 
	.dwpsn	file "../gateway.c",line 460,column 7,is_stmt
        B         $C$L48,UNC            ; [CPU_] |460| 
        ; branch occurs ; [] |460| 
$C$L47:    
	.dwpsn	file "../gateway.c",line 463,column 5,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |463| 
        MOVL      ACC,XAR4              ; [CPU_] |463| 
$C$DW$283	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$283, DW_AT_low_pc(0x00)
	.dwattr $C$DW$283, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$283, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |463| 
        ; call occurs [#_ERR_ClearWarning] ; [] |463| 
$C$L48:    
	.dwpsn	file "../gateway.c",line 467,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |467| 
        CMP       AL,*-SP[19]           ; [CPU_] |467| 
        B         $C$L49,GEQ            ; [CPU_] |467| 
        ; branchcc occurs ; [] |467| 
	.dwpsn	file "../gateway.c",line 469,column 5,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |469| 
        MOVL      ACC,XAR4              ; [CPU_] |469| 
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$284, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |469| 
        ; call occurs [#_ERR_HandleWarning] ; [] |469| 
	.dwpsn	file "../gateway.c",line 470,column 7,is_stmt
        B         $C$L50,UNC            ; [CPU_] |470| 
        ; branch occurs ; [] |470| 
$C$L49:    
	.dwpsn	file "../gateway.c",line 473,column 5,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |473| 
        MOVL      ACC,XAR4              ; [CPU_] |473| 
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$285, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |473| 
        ; call occurs [#_ERR_ClearWarning] ; [] |473| 
$C$L50:    
	.dwpsn	file "../gateway.c",line 476,column 7,is_stmt
        MOV32     R0H,*-SP[22]          ; [CPU_] |476| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |476| 
        NOP       ; [CPU_] 
        CMPF32    R0H,#0                ; [CPU_] |476| 
        MOVST0    ZF, NF                ; [CPU_] |476| 
        B         $C$L51,LT             ; [CPU_] |476| 
        ; branchcc occurs ; [] |476| 
        MOV32     R0H,*-SP[22]          ; [CPU_] |476| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |476| 
        B         $C$L52,UNC            ; [CPU_] |476| 
        ; branch occurs ; [] |476| 
$C$L51:    
        MOV32     R0H,*-SP[22]          ; [CPU_] |476| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |476| 
        NOP       ; [CPU_] 
        NEGF32    R0H,R0H               ; [CPU_] |476| 
$C$L52:    
        MOVW      DP,#_ODP_Sleep_Current ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_Sleep_Current ; [CPU_] |476| 
        NOP       ; [CPU_] 
        CMPF32    R0H,R1H               ; [CPU_] |476| 
        MOVST0    ZF, NF                ; [CPU_] |476| 
        B         $C$L53,GEQ            ; [CPU_] |476| 
        ; branchcc occurs ; [] |476| 
	.dwpsn	file "../gateway.c",line 477,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |477| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |477| 
        MOVL      *-SP[12],ACC          ; [CPU_] |477| 
	.dwpsn	file "../gateway.c",line 478,column 9,is_stmt
        MOV       T,#1000               ; [CPU_] |478| 
        MOVW      DP,#_ODP_Sleep_Timeout ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_Sleep_Timeout ; [CPU_] |478| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |478| 
        B         $C$L54,HIS            ; [CPU_] |478| 
        ; branchcc occurs ; [] |478| 
	.dwpsn	file "../gateway.c",line 479,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |479| 
        MOVL      *-SP[12],ACC          ; [CPU_] |479| 
	.dwpsn	file "../gateway.c",line 480,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#64,UNC ; [CPU_] |480| 
	.dwpsn	file "../gateway.c",line 481,column 11,is_stmt
        MOV       AL,#0                 ; [CPU_] |481| 
        MOV       AH,#64                ; [CPU_] |481| 
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$286, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |481| 
        ; call occurs [#_ERR_HandleWarning] ; [] |481| 
	.dwpsn	file "../gateway.c",line 483,column 7,is_stmt
        B         $C$L54,UNC            ; [CPU_] |483| 
        ; branch occurs ; [] |483| 
$C$L53:    
	.dwpsn	file "../gateway.c",line 484,column 12,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |484| 
        BF        $C$L54,EQ             ; [CPU_] |484| 
        ; branchcc occurs ; [] |484| 
	.dwpsn	file "../gateway.c",line 484,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |484| 
        SUBL      *-SP[12],ACC          ; [CPU_] |484| 
$C$L54:    
	.dwpsn	file "../gateway.c",line 485,column 7,is_stmt
        MOVW      DP,#_ODP_RelayResetTime ; [CPU_U] 
        MOV       AL,@_ODP_RelayResetTime ; [CPU_] |485| 
        BF        $C$L56,EQ             ; [CPU_] |485| 
        ; branchcc occurs ; [] |485| 
        MOV       AL,*-SP[19]           ; [CPU_] |485| 
        CMPB      AL,#8                 ; [CPU_] |485| 
        B         $C$L56,GEQ            ; [CPU_] |485| 
        ; branchcc occurs ; [] |485| 
	.dwpsn	file "../gateway.c",line 486,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |486| 
        ADDL      ACC,*-SP[16]          ; [CPU_] |486| 
        MOVL      *-SP[16],ACC          ; [CPU_] |486| 
	.dwpsn	file "../gateway.c",line 487,column 9,is_stmt
        MOV       T,#60000              ; [CPU_] |487| 
        MPYU      ACC,T,@_ODP_RelayResetTime ; [CPU_] |487| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |487| 
        B         $C$L55,HI             ; [CPU_] |487| 
        ; branchcc occurs ; [] |487| 
	.dwpsn	file "../gateway.c",line 488,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |488| 
        MOVL      *-SP[16],ACC          ; [CPU_] |488| 
	.dwpsn	file "../gateway.c",line 489,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |489| 
	.dwpsn	file "../gateway.c",line 490,column 9,is_stmt
        B         $C$L57,UNC            ; [CPU_] |490| 
        ; branch occurs ; [] |490| 
$C$L55:    
	.dwpsn	file "../gateway.c",line 491,column 14,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#50000            ; [CPU_] |491| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |491| 
        B         $C$L57,HI             ; [CPU_] |491| 
        ; branchcc occurs ; [] |491| 
	.dwpsn	file "../gateway.c",line 491,column 40,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |491| 
	.dwpsn	file "../gateway.c",line 492,column 7,is_stmt
        B         $C$L57,UNC            ; [CPU_] |492| 
        ; branch occurs ; [] |492| 
$C$L56:    
	.dwpsn	file "../gateway.c",line 493,column 12,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |493| 
        BF        $C$L57,EQ             ; [CPU_] |493| 
        ; branchcc occurs ; [] |493| 
	.dwpsn	file "../gateway.c",line 493,column 33,is_stmt
        MOVB      ACC,#0                ; [CPU_] |493| 
        MOVL      *-SP[16],ACC          ; [CPU_] |493| 
$C$L57:    
	.dwpsn	file "../gateway.c",line 494,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmax ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Resistor_Tmax ; [CPU_] |494| 
        MPYB      ACC,T,#10             ; [CPU_] |494| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_] |494| 
        B         $C$L58,LEQ            ; [CPU_] |494| 
        ; branchcc occurs ; [] |494| 
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmin ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Resistor_Tmin ; [CPU_] |494| 
        MPYB      ACC,T,#10             ; [CPU_] |494| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_] |494| 
        B         $C$L58,GEQ            ; [CPU_] |494| 
        ; branchcc occurs ; [] |494| 
        MOVW      DP,#_ODP_SafetyLimits_Tmax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Tmax ; [CPU_] |494| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_Temperature ; [CPU_] |494| 
        B         $C$L58,LT             ; [CPU_] |494| 
        ; branchcc occurs ; [] |494| 
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Tmin ; [CPU_] |494| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_Temperature ; [CPU_] |494| 
        B         $C$L59,LEQ            ; [CPU_] |494| 
        ; branchcc occurs ; [] |494| 
$C$L58:    
	.dwpsn	file "../gateway.c",line 496,column 9,is_stmt
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$287, DW_AT_TI_call
        LCR       #_ERR_ErrorOverTemp   ; [CPU_] |496| 
        ; call occurs [#_ERR_ErrorOverTemp] ; [] |496| 
$C$L59:    
	.dwpsn	file "../gateway.c",line 498,column 7,is_stmt
        MOVW      DP,#_ODP_Temperature_WarningMax ; [CPU_U] 
        MOV       AL,@_ODP_Temperature_WarningMax ; [CPU_] |498| 
        MOVW      DP,#_ODV_Gateway_MaxModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxModTemp ; [CPU_] |498| 
        B         $C$L60,LT             ; [CPU_] |498| 
        ; branchcc occurs ; [] |498| 
        MOVW      DP,#_ODP_Temperature_WarningMin ; [CPU_U] 
        MOV       AL,@_ODP_Temperature_WarningMin ; [CPU_] |498| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |498| 
        B         $C$L61,LEQ            ; [CPU_] |498| 
        ; branchcc occurs ; [] |498| 
$C$L60:    
	.dwpsn	file "../gateway.c",line 499,column 9,is_stmt
        MOVL      XAR4,#262144          ; [CPU_U] |499| 
        MOVL      ACC,XAR4              ; [CPU_] |499| 
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$288, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |499| 
        ; call occurs [#_ERR_HandleWarning] ; [] |499| 
	.dwpsn	file "../gateway.c",line 500,column 7,is_stmt
        B         $C$L62,UNC            ; [CPU_] |500| 
        ; branch occurs ; [] |500| 
$C$L61:    
	.dwpsn	file "../gateway.c",line 501,column 12,is_stmt
        MOVL      XAR4,#262144          ; [CPU_U] |501| 
        MOVL      ACC,XAR4              ; [CPU_] |501| 
$C$DW$289	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$289, DW_AT_low_pc(0x00)
	.dwattr $C$DW$289, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$289, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |501| 
        ; call occurs [#_ERR_ClearWarning] ; [] |501| 
$C$L62:    
	.dwpsn	file "../gateway.c",line 502,column 7,is_stmt
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |502| 
        NEG       AL                    ; [CPU_] |502| 
        CMP       AL,*-SP[18]           ; [CPU_] |502| 
        B         $C$L63,GT             ; [CPU_] |502| 
        ; branchcc occurs ; [] |502| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |502| 
        CMP       AL,*-SP[18]           ; [CPU_] |502| 
        B         $C$L64,GEQ            ; [CPU_] |502| 
        ; branchcc occurs ; [] |502| 
$C$L63:    
	.dwpsn	file "../gateway.c",line 503,column 9,is_stmt
        MOVL      XAR4,#131072          ; [CPU_U] |503| 
        MOVL      ACC,XAR4              ; [CPU_] |503| 
$C$DW$290	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$290, DW_AT_low_pc(0x00)
	.dwattr $C$DW$290, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$290, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |503| 
        ; call occurs [#_ERR_HandleWarning] ; [] |503| 
	.dwpsn	file "../gateway.c",line 508,column 7,is_stmt
        B         $C$L105,UNC           ; [CPU_] |508| 
        ; branch occurs ; [] |508| 
$C$L64:    
	.dwpsn	file "../gateway.c",line 509,column 12,is_stmt
        MOVL      XAR4,#131072          ; [CPU_U] |509| 
        MOVL      ACC,XAR4              ; [CPU_] |509| 
$C$DW$291	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$291, DW_AT_low_pc(0x00)
	.dwattr $C$DW$291, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$291, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |509| 
        ; call occurs [#_ERR_ClearWarning] ; [] |509| 
	.dwpsn	file "../gateway.c",line 512,column 5,is_stmt
        B         $C$L105,UNC           ; [CPU_] |512| 
        ; branch occurs ; [] |512| 
$C$L65:    
	.dwpsn	file "../gateway.c",line 515,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |515| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |515| 
        LSR       AL,1                  ; [CPU_] |515| 
        CMPB      AL,#1                 ; [CPU_] |515| 
        BF        $C$L66,NEQ            ; [CPU_] |515| 
        ; branchcc occurs ; [] |515| 
	.dwpsn	file "../gateway.c",line 516,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |516| 
	.dwpsn	file "../gateway.c",line 517,column 9,is_stmt
        B         $C$L67,UNC            ; [CPU_] |517| 
        ; branch occurs ; [] |517| 
$C$L66:    
	.dwpsn	file "../gateway.c",line 518,column 14,is_stmt
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |518| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |518| 
        BF        $C$L67,TC             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
	.dwpsn	file "../gateway.c",line 519,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |519| 
$C$L67:    
	.dwpsn	file "../gateway.c",line 521,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |521| 
        CMP       AL,*-SP[19]           ; [CPU_] |521| 
        B         $C$L68,LEQ            ; [CPU_] |521| 
        ; branchcc occurs ; [] |521| 
	.dwpsn	file "../gateway.c",line 522,column 10,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |522| 
$C$L68:    
	.dwpsn	file "../gateway.c",line 524,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |524| 
        BF        $C$L107,EQ            ; [CPU_] |524| 
        ; branchcc occurs ; [] |524| 
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |524| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        CMP       AL,@_Modules_Present  ; [CPU_] |524| 
        BF        $C$L107,NEQ           ; [CPU_] |524| 
        ; branchcc occurs ; [] |524| 
	.dwpsn	file "../gateway.c",line 525,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |525| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |525| 
        BF        $C$L69,TC             ; [CPU_] |525| 
        ; branchcc occurs ; [] |525| 
	.dwpsn	file "../gateway.c",line 525,column 41,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |525| 
$C$L69:    
	.dwpsn	file "../gateway.c",line 526,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xffbf ; [CPU_] |526| 
	.dwpsn	file "../gateway.c",line 527,column 11,is_stmt
        AND       @_GpioDataRegs+1,#0xfeff ; [CPU_] |527| 
	.dwpsn	file "../gateway.c",line 528,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |528| 
        MOVL      *-SP[4],ACC           ; [CPU_] |528| 
	.dwpsn	file "../gateway.c",line 529,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |529| 
	.dwpsn	file "../gateway.c",line 530,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#6,UNC ; [CPU_] |530| 
	.dwpsn	file "../gateway.c",line 532,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |532| 
        ; branch occurs ; [] |532| 
$C$L70:    
	.dwpsn	file "../gateway.c",line 535,column 9,is_stmt
        MOVB      XAR6,#100             ; [CPU_] |535| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |535| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |535| 
        CMPL      ACC,XAR6              ; [CPU_] |535| 
        B         $C$L71,LO             ; [CPU_] |535| 
        ; branchcc occurs ; [] |535| 
	.dwpsn	file "../gateway.c",line 535,column 44,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |535| 
$C$L71:    
	.dwpsn	file "../gateway.c",line 536,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |536| 
        BF        $C$L107,EQ            ; [CPU_] |536| 
        ; branchcc occurs ; [] |536| 
	.dwpsn	file "../gateway.c",line 537,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Voltage ; [CPU_] |537| 
        B         $C$L73,LT             ; [CPU_] |537| 
        ; branchcc occurs ; [] |537| 
	.dwpsn	file "../gateway.c",line 538,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |538| 
        MOVL      *-SP[4],ACC           ; [CPU_] |538| 
	.dwpsn	file "../gateway.c",line 539,column 13,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |539| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |539| 
        BF        $C$L72,TC             ; [CPU_] |539| 
        ; branchcc occurs ; [] |539| 
	.dwpsn	file "../gateway.c",line 544,column 15,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |544| 
	.dwpsn	file "../gateway.c",line 545,column 15,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#3,UNC ; [CPU_] |545| 
	.dwpsn	file "../gateway.c",line 547,column 13,is_stmt
        B         $C$L107,UNC           ; [CPU_] |547| 
        ; branch occurs ; [] |547| 
$C$L72:    
	.dwpsn	file "../gateway.c",line 549,column 15,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#4,UNC ; [CPU_] |549| 
	.dwpsn	file "../gateway.c",line 550,column 15,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |550| 
	.dwpsn	file "../gateway.c",line 552,column 11,is_stmt
        B         $C$L107,UNC           ; [CPU_] |552| 
        ; branch occurs ; [] |552| 
$C$L73:    
	.dwpsn	file "../gateway.c",line 554,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |554| 
$C$DW$292	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$292, DW_AT_low_pc(0x00)
	.dwattr $C$DW$292, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$292, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |554| 
        ; call occurs [#_ERR_SetError] ; [] |554| 
	.dwpsn	file "../gateway.c",line 557,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |557| 
        ; branch occurs ; [] |557| 
$C$L74:    
	.dwpsn	file "../gateway.c",line 560,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |560| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |560| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |560| 
        CMPL      ACC,XAR6              ; [CPU_] |560| 
        B         $C$L75,LO             ; [CPU_] |560| 
        ; branchcc occurs ; [] |560| 
	.dwpsn	file "../gateway.c",line 560,column 43,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |560| 
$C$L75:    
	.dwpsn	file "../gateway.c",line 561,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |561| 
        CMP       AL,*-SP[19]           ; [CPU_] |561| 
        B         $C$L76,GT             ; [CPU_] |561| 
        ; branchcc occurs ; [] |561| 
        MOV       AL,*-SP[23]           ; [CPU_] |561| 
        BF        $C$L76,EQ             ; [CPU_] |561| 
        ; branchcc occurs ; [] |561| 
	.dwpsn	file "../gateway.c",line 562,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |562| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |562| 
        MOVL      *-SP[10],ACC          ; [CPU_] |562| 
	.dwpsn	file "../gateway.c",line 563,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |563| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |563| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |563| 
        B         $C$L77,HIS            ; [CPU_] |563| 
        ; branchcc occurs ; [] |563| 
	.dwpsn	file "../gateway.c",line 563,column 78,is_stmt
$C$DW$293	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$293, DW_AT_low_pc(0x00)
	.dwattr $C$DW$293, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$293, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |563| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |563| 
	.dwpsn	file "../gateway.c",line 564,column 9,is_stmt
        B         $C$L77,UNC            ; [CPU_] |564| 
        ; branch occurs ; [] |564| 
$C$L76:    
	.dwpsn	file "../gateway.c",line 565,column 14,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |565| 
        BF        $C$L77,EQ             ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
	.dwpsn	file "../gateway.c",line 565,column 39,is_stmt
        MOVB      ACC,#1                ; [CPU_] |565| 
        SUBL      *-SP[10],ACC          ; [CPU_] |565| 
$C$L77:    
	.dwpsn	file "../gateway.c",line 566,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |566| 
        CMP       AL,*-SP[18]           ; [CPU_] |566| 
        B         $C$L78,GT             ; [CPU_] |566| 
        ; branchcc occurs ; [] |566| 
        MOV       AL,*-SP[23]           ; [CPU_] |566| 
        BF        $C$L78,EQ             ; [CPU_] |566| 
        ; branchcc occurs ; [] |566| 
	.dwpsn	file "../gateway.c",line 567,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |567| 
        ADDL      ACC,*-SP[14]          ; [CPU_] |567| 
        MOVL      *-SP[14],ACC          ; [CPU_] |567| 
	.dwpsn	file "../gateway.c",line 568,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |568| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |568| 
        CMPL      ACC,*-SP[14]          ; [CPU_] |568| 
        B         $C$L79,HIS            ; [CPU_] |568| 
        ; branchcc occurs ; [] |568| 
	.dwpsn	file "../gateway.c",line 568,column 79,is_stmt
$C$DW$294	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$294, DW_AT_low_pc(0x00)
	.dwattr $C$DW$294, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$294, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |568| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |568| 
	.dwpsn	file "../gateway.c",line 569,column 9,is_stmt
        B         $C$L79,UNC            ; [CPU_] |569| 
        ; branch occurs ; [] |569| 
$C$L78:    
	.dwpsn	file "../gateway.c",line 570,column 14,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |570| 
        BF        $C$L79,EQ             ; [CPU_] |570| 
        ; branchcc occurs ; [] |570| 
	.dwpsn	file "../gateway.c",line 570,column 40,is_stmt
        MOVB      ACC,#1                ; [CPU_] |570| 
        SUBL      *-SP[14],ACC          ; [CPU_] |570| 
$C$L79:    
	.dwpsn	file "../gateway.c",line 571,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |571| 
        CMP       AL,*-SP[19]           ; [CPU_] |571| 
        B         $C$L80,LEQ            ; [CPU_] |571| 
        ; branchcc occurs ; [] |571| 
	.dwpsn	file "../gateway.c",line 572,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |572| 
	.dwpsn	file "../gateway.c",line 573,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |573| 
        MOVL      *-SP[4],ACC           ; [CPU_] |573| 
	.dwpsn	file "../gateway.c",line 574,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |574| 
$C$L80:    
	.dwpsn	file "../gateway.c",line 576,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |576| 
        ANDB      AL,#0x0f              ; [CPU_] |576| 
        CMPB      AL,#1                 ; [CPU_] |576| 
        BF        $C$L81,NEQ            ; [CPU_] |576| 
        ; branchcc occurs ; [] |576| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |576| 
        CMP       AL,*-SP[19]           ; [CPU_] |576| 
        B         $C$L81,LEQ            ; [CPU_] |576| 
        ; branchcc occurs ; [] |576| 
        MOV       AL,*-SP[23]           ; [CPU_] |576| 
        BF        $C$L81,EQ             ; [CPU_] |576| 
        ; branchcc occurs ; [] |576| 
	.dwpsn	file "../gateway.c",line 577,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |577| 
	.dwpsn	file "../gateway.c",line 578,column 11,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |578| 
        MOVL      ACC,XAR4              ; [CPU_] |578| 
$C$DW$295	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$295, DW_AT_low_pc(0x00)
	.dwattr $C$DW$295, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$295, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |578| 
        ; call occurs [#_ERR_ClearWarning] ; [] |578| 
	.dwpsn	file "../gateway.c",line 579,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |579| 
        MOVL      *-SP[4],ACC           ; [CPU_] |579| 
	.dwpsn	file "../gateway.c",line 580,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |580| 
$C$L81:    
	.dwpsn	file "../gateway.c",line 582,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |582| 
        BF        $C$L107,EQ            ; [CPU_] |582| 
        ; branchcc occurs ; [] |582| 
        MOVIZ     R0H,#15948            ; [CPU_] |582| 
        MOV32     R1H,*-SP[22]          ; [CPU_] |582| 
        MOVXI     R0H,#52429            ; [CPU_] |582| 
        CMPF32    R1H,R0H               ; [CPU_] |582| 
        MOVST0    ZF, NF                ; [CPU_] |582| 
        B         $C$L107,LT            ; [CPU_] |582| 
        ; branchcc occurs ; [] |582| 
	.dwpsn	file "../gateway.c",line 583,column 11,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |583| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,ACC ; [CPU_] |583| 
	.dwpsn	file "../gateway.c",line 585,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |585| 
        ; branch occurs ; [] |585| 
$C$L82:    
	.dwpsn	file "../gateway.c",line 589,column 8,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |589| 
        MOVL      ACC,XAR4              ; [CPU_] |589| 
$C$DW$296	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$296, DW_AT_low_pc(0x00)
	.dwattr $C$DW$296, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$296, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |589| 
        ; call occurs [#_ERR_HandleWarning] ; [] |589| 
	.dwpsn	file "../gateway.c",line 592,column 8,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |592| 
	.dwpsn	file "../gateway.c",line 594,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |594| 
        CMP       AL,*-SP[19]           ; [CPU_] |594| 
        B         $C$L83,LT             ; [CPU_] |594| 
        ; branchcc occurs ; [] |594| 
	.dwpsn	file "../gateway.c",line 596,column 10,is_stmt
        MOV       AL,*-SP[18]           ; [CPU_] |596| 
        CMPB      AL,#3                 ; [CPU_] |596| 
        B         $C$L83,GT             ; [CPU_] |596| 
        ; branchcc occurs ; [] |596| 
	.dwpsn	file "../gateway.c",line 598,column 10,is_stmt
	.dwpsn	file "../gateway.c",line 601,column 11,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOV       ACC,#60000            ; [CPU_] |601| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |601| 
        B         $C$L83,HIS            ; [CPU_] |601| 
        ; branchcc occurs ; [] |601| 
	.dwpsn	file "../gateway.c",line 601,column 39,is_stmt
$C$DW$297	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$297, DW_AT_low_pc(0x00)
	.dwattr $C$DW$297, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$297, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |601| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |601| 
$C$L83:    
	.dwpsn	file "../gateway.c",line 607,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |607| 
        CMP       AL,*-SP[19]           ; [CPU_] |607| 
        B         $C$L84,GT             ; [CPU_] |607| 
        ; branchcc occurs ; [] |607| 
	.dwpsn	file "../gateway.c",line 608,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |608| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |608| 
        MOVL      *-SP[10],ACC          ; [CPU_] |608| 
	.dwpsn	file "../gateway.c",line 609,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |609| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |609| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |609| 
        B         $C$L85,HIS            ; [CPU_] |609| 
        ; branchcc occurs ; [] |609| 
	.dwpsn	file "../gateway.c",line 609,column 78,is_stmt
$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$298, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |609| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |609| 
	.dwpsn	file "../gateway.c",line 610,column 9,is_stmt
        B         $C$L85,UNC            ; [CPU_] |610| 
        ; branch occurs ; [] |610| 
$C$L84:    
	.dwpsn	file "../gateway.c",line 611,column 14,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |611| 
        BF        $C$L85,EQ             ; [CPU_] |611| 
        ; branchcc occurs ; [] |611| 
	.dwpsn	file "../gateway.c",line 611,column 39,is_stmt
        MOVB      ACC,#1                ; [CPU_] |611| 
        SUBL      *-SP[10],ACC          ; [CPU_] |611| 
$C$L85:    
	.dwpsn	file "../gateway.c",line 613,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |613| 
        NEG       AL                    ; [CPU_] |613| 
        CMP       AL,*-SP[18]           ; [CPU_] |613| 
        B         $C$L86,GEQ            ; [CPU_] |613| 
        ; branchcc occurs ; [] |613| 
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |613| 
        NEG       AL                    ; [CPU_] |613| 
        CMP       AL,*-SP[18]           ; [CPU_] |613| 
        B         $C$L87,LT             ; [CPU_] |613| 
        ; branchcc occurs ; [] |613| 
        MOV       AL,*-SP[23]           ; [CPU_] |613| 
        BF        $C$L87,EQ             ; [CPU_] |613| 
        ; branchcc occurs ; [] |613| 
$C$L86:    
	.dwpsn	file "../gateway.c",line 615,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |615| 
        ADDL      ACC,*-SP[14]          ; [CPU_] |615| 
        MOVL      *-SP[14],ACC          ; [CPU_] |615| 
	.dwpsn	file "../gateway.c",line 616,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |616| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |616| 
        CMPL      ACC,*-SP[14]          ; [CPU_] |616| 
        B         $C$L88,HIS            ; [CPU_] |616| 
        ; branchcc occurs ; [] |616| 
	.dwpsn	file "../gateway.c",line 616,column 79,is_stmt
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$299, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |616| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |616| 
	.dwpsn	file "../gateway.c",line 617,column 9,is_stmt
        B         $C$L88,UNC            ; [CPU_] |617| 
        ; branch occurs ; [] |617| 
$C$L87:    
	.dwpsn	file "../gateway.c",line 618,column 14,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |618| 
        BF        $C$L88,EQ             ; [CPU_] |618| 
        ; branchcc occurs ; [] |618| 
	.dwpsn	file "../gateway.c",line 618,column 40,is_stmt
        MOVB      ACC,#1                ; [CPU_] |618| 
        SUBL      *-SP[14],ACC          ; [CPU_] |618| 
$C$L88:    
	.dwpsn	file "../gateway.c",line 620,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |620| 
        ANDB      AL,#0x0f              ; [CPU_] |620| 
        CMPB      AL,#1                 ; [CPU_] |620| 
        BF        $C$L107,NEQ           ; [CPU_] |620| 
        ; branchcc occurs ; [] |620| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |620| 
        CMP       AL,*-SP[19]           ; [CPU_] |620| 
        B         $C$L107,GEQ           ; [CPU_] |620| 
        ; branchcc occurs ; [] |620| 
        MOV       AL,*-SP[23]           ; [CPU_] |620| 
        BF        $C$L107,EQ            ; [CPU_] |620| 
        ; branchcc occurs ; [] |620| 
	.dwpsn	file "../gateway.c",line 621,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |621| 
	.dwpsn	file "../gateway.c",line 622,column 11,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |622| 
        MOVL      ACC,XAR4              ; [CPU_] |622| 
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$300, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |622| 
        ; call occurs [#_ERR_ClearWarning] ; [] |622| 
	.dwpsn	file "../gateway.c",line 623,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |623| 
        MOVL      *-SP[4],ACC           ; [CPU_] |623| 
	.dwpsn	file "../gateway.c",line 624,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |624| 
	.dwpsn	file "../gateway.c",line 626,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |626| 
        ; branch occurs ; [] |626| 
$C$L89:    
	.dwpsn	file "../gateway.c",line 630,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |630| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |630| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |630| 
        CMPL      ACC,XAR6              ; [CPU_] |630| 
        B         $C$L90,LO             ; [CPU_] |630| 
        ; branchcc occurs ; [] |630| 
	.dwpsn	file "../gateway.c",line 630,column 43,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |630| 
$C$L90:    
	.dwpsn	file "../gateway.c",line 631,column 9,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |631| 
        BF        $C$L91,EQ             ; [CPU_] |631| 
        ; branchcc occurs ; [] |631| 
	.dwpsn	file "../gateway.c",line 631,column 34,is_stmt
        MOVB      ACC,#1                ; [CPU_] |631| 
        SUBL      *-SP[10],ACC          ; [CPU_] |631| 
$C$L91:    
	.dwpsn	file "../gateway.c",line 632,column 9,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |632| 
        BF        $C$L92,EQ             ; [CPU_] |632| 
        ; branchcc occurs ; [] |632| 
	.dwpsn	file "../gateway.c",line 632,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |632| 
        SUBL      *-SP[14],ACC          ; [CPU_] |632| 
$C$L92:    
	.dwpsn	file "../gateway.c",line 633,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |633| 
        ANDB      AL,#0x0f              ; [CPU_] |633| 
        CMPB      AL,#2                 ; [CPU_] |633| 
        BF        $C$L93,EQ             ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |633| 
        CMP       AL,*-SP[19]           ; [CPU_] |633| 
        B         $C$L94,LT             ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
        MOV       AL,*-SP[23]           ; [CPU_] |633| 
        BF        $C$L94,EQ             ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
$C$L93:    
	.dwpsn	file "../gateway.c",line 634,column 11,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |634| 
        MOVL      ACC,XAR4              ; [CPU_] |634| 
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$301, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |634| 
        ; call occurs [#_ERR_HandleWarning] ; [] |634| 
	.dwpsn	file "../gateway.c",line 635,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |635| 
	.dwpsn	file "../gateway.c",line 637,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |637| 
        MOVL      *-SP[4],ACC           ; [CPU_] |637| 
	.dwpsn	file "../gateway.c",line 638,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |638| 
$C$L94:    
	.dwpsn	file "../gateway.c",line 640,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |640| 
        CMP       AL,*-SP[19]           ; [CPU_] |640| 
        B         $C$L95,LEQ            ; [CPU_] |640| 
        ; branchcc occurs ; [] |640| 
	.dwpsn	file "../gateway.c",line 641,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |641| 
	.dwpsn	file "../gateway.c",line 642,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |642| 
        MOVL      *-SP[4],ACC           ; [CPU_] |642| 
	.dwpsn	file "../gateway.c",line 643,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |643| 
$C$L95:    
	.dwpsn	file "../gateway.c",line 645,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |645| 
        ANDB      AL,#0x0f              ; [CPU_] |645| 
        CMPB      AL,#4                 ; [CPU_] |645| 
        BF        $C$L96,EQ             ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |645| 
        CMP       AL,*-SP[19]           ; [CPU_] |645| 
        B         $C$L107,GT            ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
        MOV       AL,*-SP[23]           ; [CPU_] |645| 
        BF        $C$L107,EQ            ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
$C$L96:    
	.dwpsn	file "../gateway.c",line 646,column 11,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |646| 
        MOVL      ACC,XAR4              ; [CPU_] |646| 
$C$DW$302	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$302, DW_AT_low_pc(0x00)
	.dwattr $C$DW$302, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$302, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |646| 
        ; call occurs [#_ERR_HandleWarning] ; [] |646| 
	.dwpsn	file "../gateway.c",line 647,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#4,UNC ; [CPU_] |647| 
	.dwpsn	file "../gateway.c",line 649,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |649| 
        MOVL      *-SP[4],ACC           ; [CPU_] |649| 
	.dwpsn	file "../gateway.c",line 650,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |650| 
	.dwpsn	file "../gateway.c",line 652,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |652| 
        ; branch occurs ; [] |652| 
$C$L97:    
	.dwpsn	file "../gateway.c",line 655,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |655| 
        CMP       AL,*-SP[19]           ; [CPU_] |655| 
        B         $C$L98,GT             ; [CPU_] |655| 
        ; branchcc occurs ; [] |655| 
	.dwpsn	file "../gateway.c",line 656,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |656| 
	.dwpsn	file "../gateway.c",line 657,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |657| 
        MOVL      *-SP[4],ACC           ; [CPU_] |657| 
	.dwpsn	file "../gateway.c",line 658,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |658| 
	.dwpsn	file "../gateway.c",line 659,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |659| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        ANDB      AL,#0x0f              ; [CPU_] |659| 
        MOV       @_ODV_MachineMode,AL  ; [CPU_] |659| 
	.dwpsn	file "../gateway.c",line 660,column 9,is_stmt
        B         $C$L107,UNC           ; [CPU_] |660| 
        ; branch occurs ; [] |660| 
$C$L98:    
	.dwpsn	file "../gateway.c",line 663,column 11,is_stmt
        MOV       T,#10000              ; [CPU_] |663| 
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Delay ; [CPU_U] 
        MPYXU     P,T,@_ODP_SafetyLimits_Resistor_Delay ; [CPU_] |663| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |663| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |663| 
        CMPL      ACC,P                 ; [CPU_] |663| 
        B         $C$L107,LO            ; [CPU_] |663| 
        ; branchcc occurs ; [] |663| 
	.dwpsn	file "../gateway.c",line 664,column 13,is_stmt
$C$DW$303	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$303, DW_AT_low_pc(0x00)
	.dwattr $C$DW$303, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$303, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |664| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |664| 
	.dwpsn	file "../gateway.c",line 668,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |668| 
        ; branch occurs ; [] |668| 
$C$L99:    
	.dwpsn	file "../gateway.c",line 671,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |671| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |671| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |671| 
        CMPL      ACC,XAR6              ; [CPU_] |671| 
        B         $C$L107,LO            ; [CPU_] |671| 
        ; branchcc occurs ; [] |671| 
	.dwpsn	file "../gateway.c",line 672,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |672| 
	.dwpsn	file "../gateway.c",line 673,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |673| 
	.dwpsn	file "../gateway.c",line 674,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |674| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |674| 
        BF        $C$L107,TC            ; [CPU_] |674| 
        ; branchcc occurs ; [] |674| 
	.dwpsn	file "../gateway.c",line 674,column 38,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |674| 
	.dwpsn	file "../gateway.c",line 676,column 9,is_stmt
        B         $C$L107,UNC           ; [CPU_] |676| 
        ; branch occurs ; [] |676| 
$C$L100:    
	.dwpsn	file "../gateway.c",line 679,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |679| 
        MOVL      *-SP[4],ACC           ; [CPU_] |679| 
	.dwpsn	file "../gateway.c",line 680,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#128,UNC ; [CPU_] |680| 
	.dwpsn	file "../gateway.c",line 681,column 9,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |681| 
$C$L101:    
	.dwpsn	file "../gateway.c",line 684,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |684| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |684| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |684| 
        CMPL      ACC,XAR6              ; [CPU_] |684| 
        B         $C$L103,LO            ; [CPU_] |684| 
        ; branchcc occurs ; [] |684| 
	.dwpsn	file "../gateway.c",line 685,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |685| 
	.dwpsn	file "../gateway.c",line 686,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |686| 
	.dwpsn	file "../gateway.c",line 687,column 11,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |687| 
        BF        $C$L102,NEQ           ; [CPU_] |687| 
        ; branchcc occurs ; [] |687| 
	.dwpsn	file "../gateway.c",line 687,column 29,is_stmt
$C$DW$304	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$304, DW_AT_low_pc(0x00)
	.dwattr $C$DW$304, DW_AT_name("_PAR_AddLog")
	.dwattr $C$DW$304, DW_AT_TI_call
        LCR       #_PAR_AddLog          ; [CPU_] |687| 
        ; call occurs [#_PAR_AddLog] ; [] |687| 
$C$L102:    
	.dwpsn	file "../gateway.c",line 688,column 11,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |688| 
	.dwpsn	file "../gateway.c",line 689,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |689| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |689| 
        BF        $C$L103,TC            ; [CPU_] |689| 
        ; branchcc occurs ; [] |689| 
	.dwpsn	file "../gateway.c",line 689,column 38,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |689| 
$C$L103:    
	.dwpsn	file "../gateway.c",line 691,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |691| 
        BF        $C$L104,EQ            ; [CPU_] |691| 
        ; branchcc occurs ; [] |691| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |691| 
        BF        $C$L107,NEQ           ; [CPU_] |691| 
        ; branchcc occurs ; [] |691| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      XAR4,#10000           ; [CPU_U] |691| 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |691| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |691| 
        CMPL      ACC,XAR4              ; [CPU_] |691| 
        B         $C$L107,LO            ; [CPU_] |691| 
        ; branchcc occurs ; [] |691| 
$C$L104:    
	.dwpsn	file "../gateway.c",line 692,column 11,is_stmt
$C$DW$305	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$305, DW_AT_low_pc(0x00)
	.dwattr $C$DW$305, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$305, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |692| 
        ; call occurs [#_ERR_ClearError] ; [] |692| 
        CMPB      AL,#0                 ; [CPU_] |692| 
        BF        $C$L107,EQ            ; [CPU_] |692| 
        ; branchcc occurs ; [] |692| 
	.dwpsn	file "../gateway.c",line 693,column 13,is_stmt
$C$DW$306	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$306, DW_AT_low_pc(0x00)
	.dwattr $C$DW$306, DW_AT_name("_ERR_ClearWarnings")
	.dwattr $C$DW$306, DW_AT_TI_call
        LCR       #_ERR_ClearWarnings   ; [CPU_] |693| 
        ; call occurs [#_ERR_ClearWarnings] ; [] |693| 
	.dwpsn	file "../gateway.c",line 695,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |695| 
	.dwpsn	file "../gateway.c",line 696,column 13,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |696| 
	.dwpsn	file "../gateway.c",line 699,column 7,is_stmt
        B         $C$L107,UNC           ; [CPU_] |699| 
        ; branch occurs ; [] |699| 
$C$L105:    
	.dwpsn	file "../gateway.c",line 512,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |512| 
        CMPB      AL,#4                 ; [CPU_] |512| 
        B         $C$L106,GT            ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#4                 ; [CPU_] |512| 
        BF        $C$L74,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#0                 ; [CPU_] |512| 
        BF        $C$L65,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#1                 ; [CPU_] |512| 
        BF        $C$L89,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#2                 ; [CPU_] |512| 
        BF        $C$L82,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#3                 ; [CPU_] |512| 
        BF        $C$L97,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        B         $C$L101,UNC           ; [CPU_] |512| 
        ; branch occurs ; [] |512| 
$C$L106:    
        CMPB      AL,#6                 ; [CPU_] |512| 
        BF        $C$L70,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#8                 ; [CPU_] |512| 
        BF        $C$L99,EQ             ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#64                ; [CPU_] |512| 
        BF        $C$L100,EQ            ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        CMPB      AL,#128               ; [CPU_] |512| 
        BF        $C$L101,EQ            ; [CPU_] |512| 
        ; branchcc occurs ; [] |512| 
        B         $C$L101,UNC           ; [CPU_] |512| 
        ; branch occurs ; [] |512| 
$C$L107:    
	.dwpsn	file "../gateway.c",line 701,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |701| 
        MOVB      AL,#1                 ; [CPU_] |701| 
$C$DW$307	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$307, DW_AT_low_pc(0x00)
	.dwattr $C$DW$307, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$307, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |701| 
        ; call occurs [#_SEM_pend] ; [] |701| 
	.dwpsn	file "../gateway.c",line 381,column 10,is_stmt
        B         $C$L37,UNC            ; [CPU_] |381| 
        ; branch occurs ; [] |381| 
	.dwattr $C$DW$259, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$259, DW_AT_TI_end_line(0x2bf)
	.dwattr $C$DW$259, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$259

	.sect	".text"
	.global	_SecurityCallBack

$C$DW$308	.dwtag  DW_TAG_subprogram, DW_AT_name("SecurityCallBack")
	.dwattr $C$DW$308, DW_AT_low_pc(_SecurityCallBack)
	.dwattr $C$DW$308, DW_AT_high_pc(0x00)
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_SecurityCallBack")
	.dwattr $C$DW$308, DW_AT_external
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$308, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$308, DW_AT_TI_begin_line(0x2c3)
	.dwattr $C$DW$308, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$308, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 707,column 95,is_stmt,address _SecurityCallBack

	.dwfde $C$DW$CIE, _SecurityCallBack
$C$DW$309	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$309, DW_AT_location[DW_OP_reg12]
$C$DW$310	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$310, DW_AT_location[DW_OP_reg14]
$C$DW$311	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$311, DW_AT_location[DW_OP_reg0]
$C$DW$312	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$312, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SecurityCallBack             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SecurityCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$313	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$313, DW_AT_location[DW_OP_breg20 -2]
$C$DW$314	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$314, DW_AT_location[DW_OP_breg20 -4]
$C$DW$315	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$315, DW_AT_location[DW_OP_breg20 -5]
$C$DW$316	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$316, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |707| 
        MOV       *-SP[5],AL            ; [CPU_] |707| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |707| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |707| 
	.dwpsn	file "../gateway.c",line 708,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |708| 
        BF        $C$L108,NEQ           ; [CPU_] |708| 
        ; branchcc occurs ; [] |708| 
	.dwpsn	file "../gateway.c",line 709,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |709| 
        BF        $C$L108,NEQ           ; [CPU_] |709| 
        ; branchcc occurs ; [] |709| 
	.dwpsn	file "../gateway.c",line 709,column 28,is_stmt
$C$DW$317	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$317, DW_AT_low_pc(0x00)
	.dwattr $C$DW$317, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$317, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |709| 
        ; call occurs [#_HAL_Random] ; [] |709| 
$C$L108:    
	.dwpsn	file "../gateway.c",line 711,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |711| 
        CMPB      AL,#1                 ; [CPU_] |711| 
        BF        $C$L110,NEQ           ; [CPU_] |711| 
        ; branchcc occurs ; [] |711| 
	.dwpsn	file "../gateway.c",line 712,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |712| 
        BF        $C$L109,NEQ           ; [CPU_] |712| 
        ; branchcc occurs ; [] |712| 
	.dwpsn	file "../gateway.c",line 712,column 28,is_stmt
$C$DW$318	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$318, DW_AT_low_pc(0x00)
	.dwattr $C$DW$318, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$318, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |712| 
        ; call occurs [#_HAL_Random] ; [] |712| 
        B         $C$L110,UNC           ; [CPU_] |712| 
        ; branch occurs ; [] |712| 
$C$L109:    
	.dwpsn	file "../gateway.c",line 713,column 10,is_stmt
$C$DW$319	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$319, DW_AT_low_pc(0x00)
	.dwattr $C$DW$319, DW_AT_name("_HAL_Unlock")
	.dwattr $C$DW$319, DW_AT_TI_call
        LCR       #_HAL_Unlock          ; [CPU_] |713| 
        ; call occurs [#_HAL_Unlock] ; [] |713| 
$C$L110:    
	.dwpsn	file "../gateway.c",line 715,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |715| 
	.dwpsn	file "../gateway.c",line 716,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$320	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$320, DW_AT_low_pc(0x00)
	.dwattr $C$DW$320, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$308, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$308, DW_AT_TI_end_line(0x2cc)
	.dwattr $C$DW$308, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$308

	.sect	".text"
	.global	_LogNBCallback

$C$DW$321	.dwtag  DW_TAG_subprogram, DW_AT_name("LogNBCallback")
	.dwattr $C$DW$321, DW_AT_low_pc(_LogNBCallback)
	.dwattr $C$DW$321, DW_AT_high_pc(0x00)
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_LogNBCallback")
	.dwattr $C$DW$321, DW_AT_external
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$321, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$321, DW_AT_TI_begin_line(0x2cf)
	.dwattr $C$DW$321, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$321, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 719,column 92,is_stmt,address _LogNBCallback

	.dwfde $C$DW$CIE, _LogNBCallback
$C$DW$322	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$322, DW_AT_location[DW_OP_reg12]
$C$DW$323	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$323, DW_AT_location[DW_OP_reg14]
$C$DW$324	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$324, DW_AT_location[DW_OP_reg0]
$C$DW$325	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$325, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LogNBCallback                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LogNBCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$326	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$326, DW_AT_location[DW_OP_breg20 -2]
$C$DW$327	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$327, DW_AT_location[DW_OP_breg20 -4]
$C$DW$328	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$328, DW_AT_location[DW_OP_breg20 -5]
$C$DW$329	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$329, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |719| 
        MOV       *-SP[5],AL            ; [CPU_] |719| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |719| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |719| 
	.dwpsn	file "../gateway.c",line 721,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |721| 
        CMPB      AL,#1                 ; [CPU_] |721| 
        BF        $C$L111,NEQ           ; [CPU_] |721| 
        ; branchcc occurs ; [] |721| 
	.dwpsn	file "../gateway.c",line 722,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_LogNB ; [CPU_] |722| 
$C$DW$330	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$330, DW_AT_low_pc(0x00)
	.dwattr $C$DW$330, DW_AT_name("_PAR_GetLogNB")
	.dwattr $C$DW$330, DW_AT_TI_call
        LCR       #_PAR_GetLogNB        ; [CPU_] |722| 
        ; call occurs [#_PAR_GetLogNB] ; [] |722| 
	.dwpsn	file "../gateway.c",line 723,column 3,is_stmt
        B         $C$L112,UNC           ; [CPU_] |723| 
        ; branch occurs ; [] |723| 
$C$L111:    
	.dwpsn	file "../gateway.c",line 725,column 5,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |725| 
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       @_ODV_Gateway_LogNB,AL ; [CPU_] |725| 
$C$L112:    
	.dwpsn	file "../gateway.c",line 727,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |727| 
	.dwpsn	file "../gateway.c",line 728,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$331	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$331, DW_AT_low_pc(0x00)
	.dwattr $C$DW$331, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$321, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$321, DW_AT_TI_end_line(0x2d8)
	.dwattr $C$DW$321, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$321

	.sect	".text"
	.global	_BatteryCallBack

$C$DW$332	.dwtag  DW_TAG_subprogram, DW_AT_name("BatteryCallBack")
	.dwattr $C$DW$332, DW_AT_low_pc(_BatteryCallBack)
	.dwattr $C$DW$332, DW_AT_high_pc(0x00)
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_BatteryCallBack")
	.dwattr $C$DW$332, DW_AT_external
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$332, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$332, DW_AT_TI_begin_line(0x2da)
	.dwattr $C$DW$332, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$332, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../gateway.c",line 730,column 94,is_stmt,address _BatteryCallBack

	.dwfde $C$DW$CIE, _BatteryCallBack
$C$DW$333	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$333, DW_AT_location[DW_OP_reg12]
$C$DW$334	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$334, DW_AT_location[DW_OP_reg14]
$C$DW$335	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$335, DW_AT_location[DW_OP_reg0]
$C$DW$336	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$336, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BatteryCallBack              FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_BatteryCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$337	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$337, DW_AT_location[DW_OP_breg20 -6]
$C$DW$338	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$338, DW_AT_location[DW_OP_breg20 -8]
$C$DW$339	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$339, DW_AT_location[DW_OP_breg20 -9]
$C$DW$340	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$340, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[10],AH           ; [CPU_] |730| 
        MOV       *-SP[9],AL            ; [CPU_] |730| 
        MOVL      *-SP[8],XAR5          ; [CPU_] |730| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |730| 
	.dwpsn	file "../gateway.c",line 731,column 3,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |731| 
        CMPB      AL,#1                 ; [CPU_] |731| 
        BF        $C$L113,NEQ           ; [CPU_] |731| 
        ; branchcc occurs ; [] |731| 
	.dwpsn	file "../gateway.c",line 732,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |732| 
        CMPB      AL,#1                 ; [CPU_] |732| 
        BF        $C$L113,NEQ           ; [CPU_] |732| 
        ; branchcc occurs ; [] |732| 
	.dwpsn	file "../gateway.c",line 733,column 7,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOVIZ     R0H,#19035            ; [CPU_] |733| 
        MOV32     R1H,@_ODP_Battery_Capacity ; [CPU_] |733| 
        MOVXI     R0H,#47616            ; [CPU_] |733| 
        MPYF32    R0H,R0H,R1H           ; [CPU_] |733| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16512        ; [CPU_] |733| 
$C$DW$341	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$341, DW_AT_low_pc(0x00)
	.dwattr $C$DW$341, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$341, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |733| 
        ; call occurs [#_CNV_Round] ; [] |733| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |733| 
	.dwpsn	file "../gateway.c",line 734,column 7,is_stmt
        MOVW      DP,#_ODV_SOC_SOC2     ; [CPU_U] 
        MOVU      ACC,@_ODV_SOC_SOC2    ; [CPU_] |734| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      XT,ACC                ; [CPU_] |734| 
        MOVB      ACC,#100              ; [CPU_] |734| 
        MOVL      *-SP[4],ACC           ; [CPU_] |734| 
        QMPYXUL   P,XT,@_PAR_Capacity_Total ; [CPU_] |734| 
        MOV       *-SP[2],#0            ; [CPU_] |734| 
        MOVL      XAR6,P                ; [CPU_] |734| 
        MOV       *-SP[1],#0            ; [CPU_] |734| 
        MOVL      ACC,XAR6              ; [CPU_] |734| 
        IMPYL     P,XT,@_PAR_Capacity_Total ; [CPU_] |734| 
$C$DW$342	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$342, DW_AT_low_pc(0x00)
	.dwattr $C$DW$342, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$342, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |734| 
        ; call occurs [#ULL$$DIV] ; [] |734| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,P ; [CPU_] |734| 
	.dwpsn	file "../gateway.c",line 735,column 7,is_stmt
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        ZAPA      ; [CPU_] |735| 
        MOVL      @_PAR_Capacity_TotalLife_Used,P ; [CPU_] |735| 
        MOVL      @_PAR_Capacity_TotalLife_Used+2,ACC ; [CPU_] |735| 
$C$L113:    
	.dwpsn	file "../gateway.c",line 738,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |738| 
	.dwpsn	file "../gateway.c",line 739,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$343	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$343, DW_AT_low_pc(0x00)
	.dwattr $C$DW$343, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$332, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$332, DW_AT_TI_end_line(0x2e3)
	.dwattr $C$DW$332, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$332

	.sect	".text"
	.global	_WriteTextCallback

$C$DW$344	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteTextCallback")
	.dwattr $C$DW$344, DW_AT_low_pc(_WriteTextCallback)
	.dwattr $C$DW$344, DW_AT_high_pc(0x00)
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_WriteTextCallback")
	.dwattr $C$DW$344, DW_AT_external
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$344, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$344, DW_AT_TI_begin_line(0x2e5)
	.dwattr $C$DW$344, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$344, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 741,column 96,is_stmt,address _WriteTextCallback

	.dwfde $C$DW$CIE, _WriteTextCallback
$C$DW$345	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$345, DW_AT_location[DW_OP_reg12]
$C$DW$346	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$346, DW_AT_location[DW_OP_reg14]
$C$DW$347	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$347, DW_AT_location[DW_OP_reg0]
$C$DW$348	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$348, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteTextCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteTextCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$349	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$349, DW_AT_location[DW_OP_breg20 -2]
$C$DW$350	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_breg20 -4]
$C$DW$351	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$351, DW_AT_location[DW_OP_breg20 -5]
$C$DW$352	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$352, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |741| 
        MOV       *-SP[5],AL            ; [CPU_] |741| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |741| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |741| 
	.dwpsn	file "../gateway.c",line 743,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |743| 
        CMPB      AL,#1                 ; [CPU_] |743| 
        BF        $C$L114,NEQ           ; [CPU_] |743| 
        ; branchcc occurs ; [] |743| 
	.dwpsn	file "../gateway.c",line 745,column 5,is_stmt
        MOVB      AL,#10                ; [CPU_] |745| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |745| 
        MOVB      AH,#8                 ; [CPU_] |745| 
        MOVB      XAR5,#0               ; [CPU_] |745| 
$C$DW$353	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$353, DW_AT_low_pc(0x00)
	.dwattr $C$DW$353, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$353, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |745| 
        ; call occurs [#_I2C_Command] ; [] |745| 
	.dwpsn	file "../gateway.c",line 746,column 3,is_stmt
        B         $C$L115,UNC           ; [CPU_] |746| 
        ; branch occurs ; [] |746| 
$C$L114:    
	.dwpsn	file "../gateway.c",line 749,column 5,is_stmt
        MOVB      AL,#9                 ; [CPU_] |749| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |749| 
        MOVB      AH,#8                 ; [CPU_] |749| 
        MOVB      XAR5,#0               ; [CPU_] |749| 
$C$DW$354	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$354, DW_AT_low_pc(0x00)
	.dwattr $C$DW$354, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$354, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |749| 
        ; call occurs [#_I2C_Command] ; [] |749| 
$C$L115:    
	.dwpsn	file "../gateway.c",line 751,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |751| 
	.dwpsn	file "../gateway.c",line 752,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$355	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$355, DW_AT_low_pc(0x00)
	.dwattr $C$DW$355, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$344, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$344, DW_AT_TI_end_line(0x2f0)
	.dwattr $C$DW$344, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$344

	.sect	".text"
	.global	_MotCurCallback

$C$DW$356	.dwtag  DW_TAG_subprogram, DW_AT_name("MotCurCallback")
	.dwattr $C$DW$356, DW_AT_low_pc(_MotCurCallback)
	.dwattr $C$DW$356, DW_AT_high_pc(0x00)
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_MotCurCallback")
	.dwattr $C$DW$356, DW_AT_external
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$356, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$356, DW_AT_TI_begin_line(0x2f2)
	.dwattr $C$DW$356, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$356, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 754,column 93,is_stmt,address _MotCurCallback

	.dwfde $C$DW$CIE, _MotCurCallback
$C$DW$357	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$357, DW_AT_location[DW_OP_reg12]
$C$DW$358	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$358, DW_AT_location[DW_OP_reg14]
$C$DW$359	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$359, DW_AT_location[DW_OP_reg0]
$C$DW$360	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$360, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MotCurCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MotCurCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$361	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$361, DW_AT_location[DW_OP_breg20 -2]
$C$DW$362	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$362, DW_AT_location[DW_OP_breg20 -4]
$C$DW$363	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$363, DW_AT_location[DW_OP_breg20 -5]
$C$DW$364	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$364, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |754| 
        MOV       *-SP[5],AL            ; [CPU_] |754| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |754| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |754| 
	.dwpsn	file "../gateway.c",line 755,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |755| 
        CMPB      AL,#1                 ; [CPU_] |755| 
        BF        $C$L116,NEQ           ; [CPU_] |755| 
        ; branchcc occurs ; [] |755| 
	.dwpsn	file "../gateway.c",line 756,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |756| 
        CMPB      AL,#2                 ; [CPU_] |756| 
        BF        $C$L116,NEQ           ; [CPU_] |756| 
        ; branchcc occurs ; [] |756| 
	.dwpsn	file "../gateway.c",line 757,column 7,is_stmt
        MOVW      DP,#_HAL_NewCurPoint  ; [CPU_U] 
        MOVB      @_HAL_NewCurPoint,#1,UNC ; [CPU_] |757| 
$C$L116:    
	.dwpsn	file "../gateway.c",line 760,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |760| 
	.dwpsn	file "../gateway.c",line 761,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$365	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$365, DW_AT_low_pc(0x00)
	.dwattr $C$DW$365, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$356, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$356, DW_AT_TI_end_line(0x2f9)
	.dwattr $C$DW$356, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$356

	.sect	".text"
	.global	_SciSendCallback

$C$DW$366	.dwtag  DW_TAG_subprogram, DW_AT_name("SciSendCallback")
	.dwattr $C$DW$366, DW_AT_low_pc(_SciSendCallback)
	.dwattr $C$DW$366, DW_AT_high_pc(0x00)
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_SciSendCallback")
	.dwattr $C$DW$366, DW_AT_external
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$366, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$366, DW_AT_TI_begin_line(0x2fc)
	.dwattr $C$DW$366, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$366, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 764,column 94,is_stmt,address _SciSendCallback

	.dwfde $C$DW$CIE, _SciSendCallback
$C$DW$367	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$367, DW_AT_location[DW_OP_reg12]
$C$DW$368	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$368, DW_AT_location[DW_OP_reg14]
$C$DW$369	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$369, DW_AT_location[DW_OP_reg0]
$C$DW$370	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$370, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SciSendCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SciSendCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$371	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$371, DW_AT_location[DW_OP_breg20 -2]
$C$DW$372	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$372, DW_AT_location[DW_OP_breg20 -4]
$C$DW$373	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$373, DW_AT_location[DW_OP_breg20 -5]
$C$DW$374	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$374, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |764| 
        MOV       *-SP[5],AL            ; [CPU_] |764| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |764| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |764| 
	.dwpsn	file "../gateway.c",line 765,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |765| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |765| 
        MOVL      XAR5,#_ODV_SciSend    ; [CPU_U] |765| 
$C$DW$375	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$375, DW_AT_low_pc(0x00)
	.dwattr $C$DW$375, DW_AT_name("_MBX_post")
	.dwattr $C$DW$375, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |765| 
        ; call occurs [#_MBX_post] ; [] |765| 
	.dwpsn	file "../gateway.c",line 768,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |768| 
	.dwpsn	file "../gateway.c",line 769,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$376	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$376, DW_AT_low_pc(0x00)
	.dwattr $C$DW$376, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$366, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$366, DW_AT_TI_end_line(0x301)
	.dwattr $C$DW$366, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$366

	.sect	".text"
	.global	_SinCosCallback

$C$DW$377	.dwtag  DW_TAG_subprogram, DW_AT_name("SinCosCallback")
	.dwattr $C$DW$377, DW_AT_low_pc(_SinCosCallback)
	.dwattr $C$DW$377, DW_AT_high_pc(0x00)
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_SinCosCallback")
	.dwattr $C$DW$377, DW_AT_external
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$377, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$377, DW_AT_TI_begin_line(0x303)
	.dwattr $C$DW$377, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$377, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 771,column 93,is_stmt,address _SinCosCallback

	.dwfde $C$DW$CIE, _SinCosCallback
$C$DW$378	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$378, DW_AT_location[DW_OP_reg12]
$C$DW$379	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$379, DW_AT_location[DW_OP_reg14]
$C$DW$380	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$380, DW_AT_location[DW_OP_reg0]
$C$DW$381	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$381, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SinCosCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SinCosCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$382	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$382, DW_AT_location[DW_OP_breg20 -2]
$C$DW$383	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$383, DW_AT_location[DW_OP_breg20 -4]
$C$DW$384	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$384, DW_AT_location[DW_OP_breg20 -5]
$C$DW$385	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$385, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |771| 
        MOV       *-SP[5],AL            ; [CPU_] |771| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |771| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |771| 
	.dwpsn	file "../gateway.c",line 773,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |773| 
	.dwpsn	file "../gateway.c",line 774,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$386	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$386, DW_AT_low_pc(0x00)
	.dwattr $C$DW$386, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$377, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$377, DW_AT_TI_end_line(0x306)
	.dwattr $C$DW$377, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$377

	.sect	".text"
	.global	_StartRecorderCallBack

$C$DW$387	.dwtag  DW_TAG_subprogram, DW_AT_name("StartRecorderCallBack")
	.dwattr $C$DW$387, DW_AT_low_pc(_StartRecorderCallBack)
	.dwattr $C$DW$387, DW_AT_high_pc(0x00)
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_StartRecorderCallBack")
	.dwattr $C$DW$387, DW_AT_external
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$387, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$387, DW_AT_TI_begin_line(0x309)
	.dwattr $C$DW$387, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$387, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 777,column 100,is_stmt,address _StartRecorderCallBack

	.dwfde $C$DW$CIE, _StartRecorderCallBack
$C$DW$388	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$388, DW_AT_location[DW_OP_reg12]
$C$DW$389	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$389, DW_AT_location[DW_OP_reg14]
$C$DW$390	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$390, DW_AT_location[DW_OP_reg0]
$C$DW$391	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$391, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _StartRecorderCallBack        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_StartRecorderCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$392	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$392, DW_AT_location[DW_OP_breg20 -2]
$C$DW$393	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$393, DW_AT_location[DW_OP_breg20 -4]
$C$DW$394	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$394, DW_AT_location[DW_OP_breg20 -5]
$C$DW$395	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$395, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |777| 
        MOV       *-SP[5],AL            ; [CPU_] |777| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |777| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |777| 
	.dwpsn	file "../gateway.c",line 779,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |779| 
        CMPB      AL,#1                 ; [CPU_] |779| 
        BF        $C$L117,NEQ           ; [CPU_] |779| 
        ; branchcc occurs ; [] |779| 
	.dwpsn	file "../gateway.c",line 780,column 5,is_stmt
$C$DW$396	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$396, DW_AT_low_pc(0x00)
	.dwattr $C$DW$396, DW_AT_name("_REC_StartRecorder")
	.dwattr $C$DW$396, DW_AT_TI_call
        LCR       #_REC_StartRecorder   ; [CPU_] |780| 
        ; call occurs [#_REC_StartRecorder] ; [] |780| 
$C$L117:    
	.dwpsn	file "../gateway.c",line 782,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |782| 
	.dwpsn	file "../gateway.c",line 783,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$397	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$397, DW_AT_low_pc(0x00)
	.dwattr $C$DW$397, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$387, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$387, DW_AT_TI_end_line(0x30f)
	.dwattr $C$DW$387, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$387

	.sect	".text"
	.global	_MultiunitsCallback

$C$DW$398	.dwtag  DW_TAG_subprogram, DW_AT_name("MultiunitsCallback")
	.dwattr $C$DW$398, DW_AT_low_pc(_MultiunitsCallback)
	.dwattr $C$DW$398, DW_AT_high_pc(0x00)
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_MultiunitsCallback")
	.dwattr $C$DW$398, DW_AT_external
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$398, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$398, DW_AT_TI_begin_line(0x311)
	.dwattr $C$DW$398, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$398, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 785,column 97,is_stmt,address _MultiunitsCallback

	.dwfde $C$DW$CIE, _MultiunitsCallback
$C$DW$399	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$399, DW_AT_location[DW_OP_reg12]
$C$DW$400	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$400, DW_AT_location[DW_OP_reg14]
$C$DW$401	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$401, DW_AT_location[DW_OP_reg0]
$C$DW$402	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$402, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MultiunitsCallback           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MultiunitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$403	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$403, DW_AT_location[DW_OP_breg20 -2]
$C$DW$404	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$404, DW_AT_location[DW_OP_breg20 -4]
$C$DW$405	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$405, DW_AT_location[DW_OP_breg20 -5]
$C$DW$406	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$406, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |785| 
        MOV       *-SP[5],AL            ; [CPU_] |785| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |785| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |785| 
	.dwpsn	file "../gateway.c",line 787,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |787| 
        CMPB      AL,#1                 ; [CPU_] |787| 
        BF        $C$L118,NEQ           ; [CPU_] |787| 
        ; branchcc occurs ; [] |787| 
	.dwpsn	file "../gateway.c",line 788,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Multiunits ; [CPU_] |788| 
$C$DW$407	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$407, DW_AT_low_pc(0x00)
	.dwattr $C$DW$407, DW_AT_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$407, DW_AT_TI_call
        LCR       #_PAR_AddMultiUnits   ; [CPU_] |788| 
        ; call occurs [#_PAR_AddMultiUnits] ; [] |788| 
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       @_ODV_Recorder_Multiunits,AL ; [CPU_] |788| 
$C$L118:    
	.dwpsn	file "../gateway.c",line 790,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |790| 
	.dwpsn	file "../gateway.c",line 791,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$408	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$408, DW_AT_low_pc(0x00)
	.dwattr $C$DW$408, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$398, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$398, DW_AT_TI_end_line(0x317)
	.dwattr $C$DW$398, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$398

	.sect	".text"
	.global	_VariablesCallback

$C$DW$409	.dwtag  DW_TAG_subprogram, DW_AT_name("VariablesCallback")
	.dwattr $C$DW$409, DW_AT_low_pc(_VariablesCallback)
	.dwattr $C$DW$409, DW_AT_high_pc(0x00)
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_VariablesCallback")
	.dwattr $C$DW$409, DW_AT_external
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$409, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$409, DW_AT_TI_begin_line(0x319)
	.dwattr $C$DW$409, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$409, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 793,column 96,is_stmt,address _VariablesCallback

	.dwfde $C$DW$CIE, _VariablesCallback
$C$DW$410	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$410, DW_AT_location[DW_OP_reg12]
$C$DW$411	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg14]
$C$DW$412	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$412, DW_AT_location[DW_OP_reg0]
$C$DW$413	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$413, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VariablesCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VariablesCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$414	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$414, DW_AT_location[DW_OP_breg20 -2]
$C$DW$415	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$415, DW_AT_location[DW_OP_breg20 -4]
$C$DW$416	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$416, DW_AT_location[DW_OP_breg20 -5]
$C$DW$417	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$417, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |793| 
        MOV       *-SP[5],AL            ; [CPU_] |793| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |793| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |793| 
	.dwpsn	file "../gateway.c",line 795,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |795| 
        CMPB      AL,#1                 ; [CPU_] |795| 
        BF        $C$L119,NEQ           ; [CPU_] |795| 
        ; branchcc occurs ; [] |795| 
	.dwpsn	file "../gateway.c",line 796,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Variables ; [CPU_] |796| 
$C$DW$418	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$418, DW_AT_low_pc(0x00)
	.dwattr $C$DW$418, DW_AT_name("_PAR_AddVariables")
	.dwattr $C$DW$418, DW_AT_TI_call
        LCR       #_PAR_AddVariables    ; [CPU_] |796| 
        ; call occurs [#_PAR_AddVariables] ; [] |796| 
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       @_ODV_Recorder_Variables,AL ; [CPU_] |796| 
$C$L119:    
	.dwpsn	file "../gateway.c",line 798,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |798| 
	.dwpsn	file "../gateway.c",line 799,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$419	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$419, DW_AT_low_pc(0x00)
	.dwattr $C$DW$419, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$409, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$409, DW_AT_TI_end_line(0x31f)
	.dwattr $C$DW$409, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$409

	.sect	".text"
	.global	_WriteOutputs8BitCallback

$C$DW$420	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs8BitCallback")
	.dwattr $C$DW$420, DW_AT_low_pc(_WriteOutputs8BitCallback)
	.dwattr $C$DW$420, DW_AT_high_pc(0x00)
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_WriteOutputs8BitCallback")
	.dwattr $C$DW$420, DW_AT_external
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$420, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$420, DW_AT_TI_begin_line(0x322)
	.dwattr $C$DW$420, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$420, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 802,column 103,is_stmt,address _WriteOutputs8BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs8BitCallback
$C$DW$421	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg12]
$C$DW$422	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg14]
$C$DW$423	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg0]
$C$DW$424	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$424, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs8BitCallback     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs8BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$425	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$425, DW_AT_location[DW_OP_breg20 -2]
$C$DW$426	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$426, DW_AT_location[DW_OP_breg20 -4]
$C$DW$427	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$427, DW_AT_location[DW_OP_breg20 -5]
$C$DW$428	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$428, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |802| 
        MOV       *-SP[5],AL            ; [CPU_] |802| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |802| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |802| 
	.dwpsn	file "../gateway.c",line 803,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 804,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |804| 
	.dwpsn	file "../gateway.c",line 805,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$429	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$429, DW_AT_low_pc(0x00)
	.dwattr $C$DW$429, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$420, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$420, DW_AT_TI_end_line(0x325)
	.dwattr $C$DW$420, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$420

	.sect	".text"
	.global	_WriteOutputs16BitCallback

$C$DW$430	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs16BitCallback")
	.dwattr $C$DW$430, DW_AT_low_pc(_WriteOutputs16BitCallback)
	.dwattr $C$DW$430, DW_AT_high_pc(0x00)
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_WriteOutputs16BitCallback")
	.dwattr $C$DW$430, DW_AT_external
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$430, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$430, DW_AT_TI_begin_line(0x327)
	.dwattr $C$DW$430, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$430, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 807,column 104,is_stmt,address _WriteOutputs16BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs16BitCallback
$C$DW$431	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$431, DW_AT_location[DW_OP_reg12]
$C$DW$432	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$432, DW_AT_location[DW_OP_reg14]
$C$DW$433	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$433, DW_AT_location[DW_OP_reg0]
$C$DW$434	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$434, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs16BitCallback    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs16BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$435	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$435, DW_AT_location[DW_OP_breg20 -2]
$C$DW$436	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$436, DW_AT_location[DW_OP_breg20 -4]
$C$DW$437	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$437, DW_AT_location[DW_OP_breg20 -5]
$C$DW$438	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$438, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |807| 
        MOV       *-SP[5],AL            ; [CPU_] |807| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |807| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |807| 
	.dwpsn	file "../gateway.c",line 808,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 811,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |811| 
	.dwpsn	file "../gateway.c",line 812,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$439	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$439, DW_AT_low_pc(0x00)
	.dwattr $C$DW$439, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$430, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$430, DW_AT_TI_end_line(0x32c)
	.dwattr $C$DW$430, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$430

	.sect	".text"
	.global	_ReadInputs8BitsCallback

$C$DW$440	.dwtag  DW_TAG_subprogram, DW_AT_name("ReadInputs8BitsCallback")
	.dwattr $C$DW$440, DW_AT_low_pc(_ReadInputs8BitsCallback)
	.dwattr $C$DW$440, DW_AT_high_pc(0x00)
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_ReadInputs8BitsCallback")
	.dwattr $C$DW$440, DW_AT_external
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$440, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$440, DW_AT_TI_begin_line(0x32e)
	.dwattr $C$DW$440, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$440, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 814,column 102,is_stmt,address _ReadInputs8BitsCallback

	.dwfde $C$DW$CIE, _ReadInputs8BitsCallback
$C$DW$441	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg12]
$C$DW$442	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg14]
$C$DW$443	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg0]
$C$DW$444	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$444, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ReadInputs8BitsCallback      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ReadInputs8BitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$445	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$445, DW_AT_location[DW_OP_breg20 -2]
$C$DW$446	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$446, DW_AT_location[DW_OP_breg20 -4]
$C$DW$447	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$447, DW_AT_location[DW_OP_breg20 -5]
$C$DW$448	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$448, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |814| 
        MOV       *-SP[5],AL            ; [CPU_] |814| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |814| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |814| 
	.dwpsn	file "../gateway.c",line 816,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |816| 
	.dwpsn	file "../gateway.c",line 817,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$449	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$449, DW_AT_low_pc(0x00)
	.dwattr $C$DW$449, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$440, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$440, DW_AT_TI_end_line(0x331)
	.dwattr $C$DW$440, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$440

	.sect	".text"
	.global	_ControlWordCallBack

$C$DW$450	.dwtag  DW_TAG_subprogram, DW_AT_name("ControlWordCallBack")
	.dwattr $C$DW$450, DW_AT_low_pc(_ControlWordCallBack)
	.dwattr $C$DW$450, DW_AT_high_pc(0x00)
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_ControlWordCallBack")
	.dwattr $C$DW$450, DW_AT_external
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$450, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$450, DW_AT_TI_begin_line(0x333)
	.dwattr $C$DW$450, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$450, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 819,column 98,is_stmt,address _ControlWordCallBack

	.dwfde $C$DW$CIE, _ControlWordCallBack
$C$DW$451	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$451, DW_AT_location[DW_OP_reg12]
$C$DW$452	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$452, DW_AT_location[DW_OP_reg14]
$C$DW$453	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$453, DW_AT_location[DW_OP_reg0]
$C$DW$454	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$454, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ControlWordCallBack          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ControlWordCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$455	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$455, DW_AT_location[DW_OP_breg20 -2]
$C$DW$456	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$456, DW_AT_location[DW_OP_breg20 -4]
$C$DW$457	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$457, DW_AT_location[DW_OP_breg20 -5]
$C$DW$458	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$458, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |819| 
        MOV       *-SP[5],AL            ; [CPU_] |819| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |819| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |819| 
	.dwpsn	file "../gateway.c",line 820,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 821,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |821| 
	.dwpsn	file "../gateway.c",line 822,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$459	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$459, DW_AT_low_pc(0x00)
	.dwattr $C$DW$459, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$450, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$450, DW_AT_TI_end_line(0x336)
	.dwattr $C$DW$450, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$450

	.sect	".text"
	.global	_VersionCallback

$C$DW$460	.dwtag  DW_TAG_subprogram, DW_AT_name("VersionCallback")
	.dwattr $C$DW$460, DW_AT_low_pc(_VersionCallback)
	.dwattr $C$DW$460, DW_AT_high_pc(0x00)
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_VersionCallback")
	.dwattr $C$DW$460, DW_AT_external
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$460, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$460, DW_AT_TI_begin_line(0x338)
	.dwattr $C$DW$460, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$460, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 824,column 94,is_stmt,address _VersionCallback

	.dwfde $C$DW$CIE, _VersionCallback
$C$DW$461	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$461, DW_AT_location[DW_OP_reg12]
$C$DW$462	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_reg14]
$C$DW$463	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$463, DW_AT_location[DW_OP_reg0]
$C$DW$464	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$464, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VersionCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VersionCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$465	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$465, DW_AT_location[DW_OP_breg20 -2]
$C$DW$466	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_breg20 -4]
$C$DW$467	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_breg20 -5]
$C$DW$468	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$468, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |824| 
        MOV       *-SP[5],AL            ; [CPU_] |824| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |824| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |824| 
	.dwpsn	file "../gateway.c",line 825,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |825| 
        BF        $C$L120,NEQ           ; [CPU_] |825| 
        ; branchcc occurs ; [] |825| 
	.dwpsn	file "../gateway.c",line 825,column 27,is_stmt
        MOVW      DP,#_ODV_Version      ; [CPU_U] 
        MOVB      @_ODV_Version,#40,UNC ; [CPU_] |825| 
$C$L120:    
	.dwpsn	file "../gateway.c",line 826,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |826| 
	.dwpsn	file "../gateway.c",line 827,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$469	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$469, DW_AT_low_pc(0x00)
	.dwattr $C$DW$469, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$460, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$460, DW_AT_TI_end_line(0x33b)
	.dwattr $C$DW$460, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$460

	.sect	".text"
	.global	_WriteAnalogueOutputsCallback

$C$DW$470	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteAnalogueOutputsCallback")
	.dwattr $C$DW$470, DW_AT_low_pc(_WriteAnalogueOutputsCallback)
	.dwattr $C$DW$470, DW_AT_high_pc(0x00)
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_WriteAnalogueOutputsCallback")
	.dwattr $C$DW$470, DW_AT_external
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$470, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$470, DW_AT_TI_begin_line(0x33d)
	.dwattr $C$DW$470, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$470, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 829,column 107,is_stmt,address _WriteAnalogueOutputsCallback

	.dwfde $C$DW$CIE, _WriteAnalogueOutputsCallback
$C$DW$471	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$471, DW_AT_location[DW_OP_reg12]
$C$DW$472	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$472, DW_AT_location[DW_OP_reg14]
$C$DW$473	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$473, DW_AT_location[DW_OP_reg0]
$C$DW$474	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$474, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteAnalogueOutputsCallback FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteAnalogueOutputsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$475	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$475, DW_AT_location[DW_OP_breg20 -2]
$C$DW$476	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$476, DW_AT_location[DW_OP_breg20 -4]
$C$DW$477	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$477, DW_AT_location[DW_OP_breg20 -5]
$C$DW$478	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$478, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |829| 
        MOV       *-SP[5],AL            ; [CPU_] |829| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |829| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |829| 
	.dwpsn	file "../gateway.c",line 831,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |831| 
	.dwpsn	file "../gateway.c",line 832,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$479	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$479, DW_AT_low_pc(0x00)
	.dwattr $C$DW$479, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$470, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$470, DW_AT_TI_end_line(0x340)
	.dwattr $C$DW$470, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$470

	.sect	".text"
	.global	_CommErrorSetCallback

$C$DW$480	.dwtag  DW_TAG_subprogram, DW_AT_name("CommErrorSetCallback")
	.dwattr $C$DW$480, DW_AT_low_pc(_CommErrorSetCallback)
	.dwattr $C$DW$480, DW_AT_high_pc(0x00)
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_CommErrorSetCallback")
	.dwattr $C$DW$480, DW_AT_external
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$480, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$480, DW_AT_TI_begin_line(0x343)
	.dwattr $C$DW$480, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$480, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 835,column 99,is_stmt,address _CommErrorSetCallback

	.dwfde $C$DW$CIE, _CommErrorSetCallback
$C$DW$481	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$481, DW_AT_location[DW_OP_reg12]
$C$DW$482	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$482, DW_AT_location[DW_OP_reg14]
$C$DW$483	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$483, DW_AT_location[DW_OP_reg0]
$C$DW$484	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$484, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CommErrorSetCallback         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CommErrorSetCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$485	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$485, DW_AT_location[DW_OP_breg20 -2]
$C$DW$486	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$486, DW_AT_location[DW_OP_breg20 -4]
$C$DW$487	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_breg20 -5]
$C$DW$488	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$488, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |835| 
        MOV       *-SP[5],AL            ; [CPU_] |835| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |835| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |835| 
	.dwpsn	file "../gateway.c",line 836,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 839,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |839| 
	.dwpsn	file "../gateway.c",line 840,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$489	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$489, DW_AT_low_pc(0x00)
	.dwattr $C$DW$489, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$480, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$480, DW_AT_TI_end_line(0x348)
	.dwattr $C$DW$480, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$480

	.sect	".text"
	.global	_SaveAllParameters

$C$DW$490	.dwtag  DW_TAG_subprogram, DW_AT_name("SaveAllParameters")
	.dwattr $C$DW$490, DW_AT_low_pc(_SaveAllParameters)
	.dwattr $C$DW$490, DW_AT_high_pc(0x00)
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_SaveAllParameters")
	.dwattr $C$DW$490, DW_AT_external
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$490, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$490, DW_AT_TI_begin_line(0x34a)
	.dwattr $C$DW$490, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$490, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 842,column 96,is_stmt,address _SaveAllParameters

	.dwfde $C$DW$CIE, _SaveAllParameters
$C$DW$491	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$491, DW_AT_location[DW_OP_reg12]
$C$DW$492	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$492, DW_AT_location[DW_OP_reg14]
$C$DW$493	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg0]
$C$DW$494	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$494, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SaveAllParameters            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SaveAllParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$495	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$495, DW_AT_location[DW_OP_breg20 -2]
$C$DW$496	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$496, DW_AT_location[DW_OP_breg20 -4]
$C$DW$497	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$497, DW_AT_location[DW_OP_breg20 -5]
$C$DW$498	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$498, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |842| 
        MOV       *-SP[5],AL            ; [CPU_] |842| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |842| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |842| 
	.dwpsn	file "../gateway.c",line 843,column 3,is_stmt
        MOVW      DP,#_ODV_StoreParameters ; [CPU_U] 
        MOV       AL,#30309             ; [CPU_] |843| 
        MOV       AH,#29537             ; [CPU_] |843| 
        CMPL      ACC,@_ODV_StoreParameters ; [CPU_] |843| 
        BF        $C$L121,NEQ           ; [CPU_] |843| 
        ; branchcc occurs ; [] |843| 
	.dwpsn	file "../gateway.c",line 844,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |844| 
        MOVL      @_ODV_StoreParameters,ACC ; [CPU_] |844| 
	.dwpsn	file "../gateway.c",line 845,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |845| 
$C$DW$499	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$499, DW_AT_low_pc(0x00)
	.dwattr $C$DW$499, DW_AT_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$499, DW_AT_TI_call
        LCR       #_PAR_WriteAllPermanentParam ; [CPU_] |845| 
        ; call occurs [#_PAR_WriteAllPermanentParam] ; [] |845| 
$C$L121:    
	.dwpsn	file "../gateway.c",line 847,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |847| 
	.dwpsn	file "../gateway.c",line 848,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$500	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$500, DW_AT_low_pc(0x00)
	.dwattr $C$DW$500, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$490, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$490, DW_AT_TI_end_line(0x350)
	.dwattr $C$DW$490, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$490

	.sect	".text"
	.global	_LoadDefaultParameters

$C$DW$501	.dwtag  DW_TAG_subprogram, DW_AT_name("LoadDefaultParameters")
	.dwattr $C$DW$501, DW_AT_low_pc(_LoadDefaultParameters)
	.dwattr $C$DW$501, DW_AT_high_pc(0x00)
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_LoadDefaultParameters")
	.dwattr $C$DW$501, DW_AT_external
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$501, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$501, DW_AT_TI_begin_line(0x352)
	.dwattr $C$DW$501, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$501, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 850,column 100,is_stmt,address _LoadDefaultParameters

	.dwfde $C$DW$CIE, _LoadDefaultParameters
$C$DW$502	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$502, DW_AT_location[DW_OP_reg12]
$C$DW$503	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$503, DW_AT_location[DW_OP_reg14]
$C$DW$504	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$504, DW_AT_location[DW_OP_reg0]
$C$DW$505	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$505, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LoadDefaultParameters        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LoadDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$506	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$506, DW_AT_location[DW_OP_breg20 -2]
$C$DW$507	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$507, DW_AT_location[DW_OP_breg20 -4]
$C$DW$508	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$508, DW_AT_location[DW_OP_breg20 -5]
$C$DW$509	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$509, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |850| 
        MOV       *-SP[5],AL            ; [CPU_] |850| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |850| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |850| 
	.dwpsn	file "../gateway.c",line 851,column 3,is_stmt
        MOVW      DP,#_ODV_RestoreDefaultParameters ; [CPU_U] 
        MOV       AL,#24932             ; [CPU_] |851| 
        MOV       AH,#27759             ; [CPU_] |851| 
        CMPL      ACC,@_ODV_RestoreDefaultParameters ; [CPU_] |851| 
        BF        $C$L122,NEQ           ; [CPU_] |851| 
        ; branchcc occurs ; [] |851| 
	.dwpsn	file "../gateway.c",line 852,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |852| 
        MOVL      @_ODV_RestoreDefaultParameters,ACC ; [CPU_] |852| 
	.dwpsn	file "../gateway.c",line 853,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |853| 
$C$DW$510	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$510, DW_AT_low_pc(0x00)
	.dwattr $C$DW$510, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$510, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |853| 
        ; call occurs [#_PAR_UpdateCode] ; [] |853| 
	.dwpsn	file "../gateway.c",line 854,column 5,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |854| 
        MOVL      @_BootCommand,ACC     ; [CPU_] |854| 
$C$L122:    
	.dwpsn	file "../gateway.c",line 856,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |856| 
	.dwpsn	file "../gateway.c",line 857,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$511	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$511, DW_AT_low_pc(0x00)
	.dwattr $C$DW$511, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$501, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$501, DW_AT_TI_end_line(0x359)
	.dwattr $C$DW$501, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$501

	.sect	".text"
	.global	_DebugCallBack

$C$DW$512	.dwtag  DW_TAG_subprogram, DW_AT_name("DebugCallBack")
	.dwattr $C$DW$512, DW_AT_low_pc(_DebugCallBack)
	.dwattr $C$DW$512, DW_AT_high_pc(0x00)
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_DebugCallBack")
	.dwattr $C$DW$512, DW_AT_external
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$512, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$512, DW_AT_TI_begin_line(0x35c)
	.dwattr $C$DW$512, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$512, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 860,column 92,is_stmt,address _DebugCallBack

	.dwfde $C$DW$CIE, _DebugCallBack
$C$DW$513	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$513, DW_AT_location[DW_OP_reg12]
$C$DW$514	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$514, DW_AT_location[DW_OP_reg14]
$C$DW$515	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$515, DW_AT_location[DW_OP_reg0]
$C$DW$516	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$516, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DebugCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_DebugCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$517	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$517, DW_AT_location[DW_OP_breg20 -2]
$C$DW$518	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$518, DW_AT_location[DW_OP_breg20 -4]
$C$DW$519	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$519, DW_AT_location[DW_OP_breg20 -5]
$C$DW$520	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$520, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |860| 
        MOV       *-SP[5],AL            ; [CPU_] |860| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |860| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |860| 
	.dwpsn	file "../gateway.c",line 861,column 3,is_stmt
        MOVW      DP,#_ODV_Debug        ; [CPU_U] 
        MOV       @_ODV_Debug,#0        ; [CPU_] |861| 
	.dwpsn	file "../gateway.c",line 862,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |862| 
	.dwpsn	file "../gateway.c",line 863,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$521	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$521, DW_AT_low_pc(0x00)
	.dwattr $C$DW$521, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$512, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$512, DW_AT_TI_end_line(0x35f)
	.dwattr $C$DW$512, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$512

	.sect	".text"
	.global	_ConfigCallback

$C$DW$522	.dwtag  DW_TAG_subprogram, DW_AT_name("ConfigCallback")
	.dwattr $C$DW$522, DW_AT_low_pc(_ConfigCallback)
	.dwattr $C$DW$522, DW_AT_high_pc(0x00)
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_ConfigCallback")
	.dwattr $C$DW$522, DW_AT_external
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$522, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$522, DW_AT_TI_begin_line(0x361)
	.dwattr $C$DW$522, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$522, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 865,column 93,is_stmt,address _ConfigCallback

	.dwfde $C$DW$CIE, _ConfigCallback
$C$DW$523	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$523, DW_AT_location[DW_OP_reg12]
$C$DW$524	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$524, DW_AT_location[DW_OP_reg14]
$C$DW$525	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$525, DW_AT_location[DW_OP_reg0]
$C$DW$526	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$526, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ConfigCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ConfigCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$527	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$527, DW_AT_location[DW_OP_breg20 -2]
$C$DW$528	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$528, DW_AT_location[DW_OP_breg20 -4]
$C$DW$529	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$529, DW_AT_location[DW_OP_breg20 -5]
$C$DW$530	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$530, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |865| 
        MOV       *-SP[5],AL            ; [CPU_] |865| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |865| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |865| 
	.dwpsn	file "../gateway.c",line 866,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |866| 
        CMPB      AL,#1                 ; [CPU_] |866| 
        BF        $C$L125,NEQ           ; [CPU_] |866| 
        ; branchcc occurs ; [] |866| 
	.dwpsn	file "../gateway.c",line 867,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |867| 
        BF        $C$L123,EQ            ; [CPU_] |867| 
        ; branchcc occurs ; [] |867| 
        CMPB      AL,#128               ; [CPU_] |867| 
        BF        $C$L125,NEQ           ; [CPU_] |867| 
        ; branchcc occurs ; [] |867| 
$C$L123:    
	.dwpsn	file "../gateway.c",line 868,column 7,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |868| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |868| 
        BF        $C$L124,NTC           ; [CPU_] |868| 
        ; branchcc occurs ; [] |868| 
	.dwpsn	file "../gateway.c",line 869,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xffbf ; [CPU_] |869| 
	.dwpsn	file "../gateway.c",line 870,column 7,is_stmt
        B         $C$L125,UNC           ; [CPU_] |870| 
        ; branch occurs ; [] |870| 
$C$L124:    
	.dwpsn	file "../gateway.c",line 872,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |872| 
$C$L125:    
	.dwpsn	file "../gateway.c",line 875,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |875| 
	.dwpsn	file "../gateway.c",line 876,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$531	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$531, DW_AT_low_pc(0x00)
	.dwattr $C$DW$531, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$522, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$522, DW_AT_TI_end_line(0x36c)
	.dwattr $C$DW$522, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$522

	.sect	".text"
	.global	_ResetCallBack

$C$DW$532	.dwtag  DW_TAG_subprogram, DW_AT_name("ResetCallBack")
	.dwattr $C$DW$532, DW_AT_low_pc(_ResetCallBack)
	.dwattr $C$DW$532, DW_AT_high_pc(0x00)
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_ResetCallBack")
	.dwattr $C$DW$532, DW_AT_external
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$532, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$532, DW_AT_TI_begin_line(0x36e)
	.dwattr $C$DW$532, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$532, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 878,column 92,is_stmt,address _ResetCallBack

	.dwfde $C$DW$CIE, _ResetCallBack
$C$DW$533	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$533, DW_AT_location[DW_OP_reg12]
$C$DW$534	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$534, DW_AT_location[DW_OP_reg14]
$C$DW$535	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$535, DW_AT_location[DW_OP_reg0]
$C$DW$536	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$536, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ResetCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ResetCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$537	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$537, DW_AT_location[DW_OP_breg20 -2]
$C$DW$538	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$538, DW_AT_location[DW_OP_breg20 -4]
$C$DW$539	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$539, DW_AT_location[DW_OP_breg20 -5]
$C$DW$540	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$540, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |878| 
        MOV       *-SP[5],AL            ; [CPU_] |878| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |878| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |878| 
	.dwpsn	file "../gateway.c",line 879,column 3,is_stmt
        MOVW      DP,#_ODV_ResetHW      ; [CPU_U] 
        MOV       AL,#29295             ; [CPU_] |879| 
        MOV       AH,#31333             ; [CPU_] |879| 
        CMPL      ACC,@_ODV_ResetHW     ; [CPU_] |879| 
        BF        $C$L126,NEQ           ; [CPU_] |879| 
        ; branchcc occurs ; [] |879| 
	.dwpsn	file "../gateway.c",line 880,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |880| 
        MOVL      @_ODV_ResetHW,ACC     ; [CPU_] |880| 
	.dwpsn	file "../gateway.c",line 881,column 5,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |881| 
        MOVL      @_BootCommand,ACC     ; [CPU_] |881| 
$C$L126:    
	.dwpsn	file "../gateway.c",line 883,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |883| 
	.dwpsn	file "../gateway.c",line 884,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$541	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$541, DW_AT_low_pc(0x00)
	.dwattr $C$DW$541, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$532, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$532, DW_AT_TI_end_line(0x374)
	.dwattr $C$DW$532, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$532

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_PAR_GetLogNB
	.global	_PAR_AddLog
	.global	_PAR_SetParamDependantVars
	.global	_USB_Unlock
	.global	_genCRC32Table
	.global	_ERR_ErrorOverVoltage
	.global	_ERR_ErrorOverTemp
	.global	_ERR_ErrorUnderVoltage
	.global	_ERR_SetError
	.global	_ERR_DELTA_Voltage
	.global	_ADS_Init
	.global	_HAL_Random
	.global	_HAL_Unlock
	.global	_USB_Stop
	.global	_HAL_Reset
	.global	_setNodeId
	.global	_canInit
	.global	_HAL_Init
	.global	_REC_StartRecorder
	.global	_DIC_SetNodeId
	.global	_ERR_ClearWarning
	.global	_ERR_HandleWarning
	.global	_ERR_ErrorOverCurrent
	.global	_ODP_SafetyLimits_Resistor_Tmax
	.global	_ODP_SafetyLimits_Resistor_Tmin
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_ODP_SafetyLimits_OverVoltage
	.global	_ODP_SafetyLimits_Resistor_Delay
	.global	_ODP_SafetyLimits_Overcurrent
	.global	_ODV_Current_ChargeAllowed
	.global	_ODP_Temperature_WarningMax
	.global	_ODP_Current_C_D_Mode
	.global	_ODV_Current_DischargeAllowed
	.global	_ODP_SafetyLimits_Current_delay
	.global	_ODP_SafetyLimits_Voltage_delay
	.global	_ODP_Temperature_WarningMin
	.global	_ODP_SafetyLimits_UnderCurrent
	.global	_ODP_NbOfModules
	.global	_ODP_SafetyLimits_Charge_In_Thres_Cur
	.global	_ODV_Gateway_Bender_Voltage
	.global	_ODV_Gateway_MinModTemp
	.global	_ODV_Gateway_MaxDeltaCellVoltage
	.global	_ODV_Gateway_Test_Voltage
	.global	_ODV_Gateway_ISOTets
	.global	_ODV_Gateway_Alive_Counter
	.global	_ODV_Gateway_MaxModTemp
	.global	_ODP_SafetyLimits_Tmin
	.global	_ODV_Gateway_MaxCellVoltage
	.global	_ODP_SafetyLimits_Umin_bal_delta
	.global	_ODP_SafetyLimits_Umax_bal_delta
	.global	_ODP_SafetyLimits_Umax
	.global	_ODV_Gateway_Heater_Status
	.global	_ODP_SafetyLimits_Tmax
	.global	_ODV_Debug
	.global	_ODV_SOC_SOC2
	.global	_ODV_Controlword
	.global	_ODV_Write_Outputs_16_Bit
	.global	_HAL_NewCurPoint
	.global	_ODV_MachineEvent
	.global	_ODV_MachineMode
	.global	_ODP_CommError_Delay
	.global	_ODP_CommError_TimeOut
	.global	_ODV_BenderISO_IMC_ISOR
	.global	_ODP_SafetyLimits_Umin
	.global	_ODP_RelayResetTime
	.global	_ODV_BenderISO_D_IMC_HV_1
	.global	_TimeLogIndex
	.global	_ODV_Version
	.global	_ODP_Board_BaudRate
	.global	_ODP_Board_Config
	.global	_ODP_Sleep_Timeout
	.global	_ODP_Sleep_Current
	.global	_PAR_AddMultiUnits
	.global	_ERR_ClearError
	.global	_PAR_AddVariables
	.global	_MBX_post
	.global	_setState
	.global	_MBX_pend
	.global	_I2C_Command
	.global	_USB_Start
	.global	_PAR_UpdateCode
	.global	_PAR_InitParam
	.global	_ERR_ClearWarnings
	.global	_ODV_Gateway_Current
	.global	_ODV_Gateway_Voltage
	.global	_ODV_Gateway_Temperature
	.global	_ODV_Gateway_LogNB
	.global	_ODV_Gateway_State
	.global	_ODV_Recorder_Variables
	.global	_ODV_Recorder_Multiunits
	.global	_ODV_SciSend
	.global	_ODV_Gateway_MinCellVoltage
	.global	_SEM_pend
	.global	_MMSConfig
	.global	_ODV_ResetHW
	.global	_CNV_Round
	.global	_ODP_RandomNB
	.global	_ODV_SysTick_ms
	.global	_ODP_Battery_Capacity
	.global	_ODV_StoreParameters
	.global	_PAR_WriteAllPermanentParam
	.global	_getCRC32_cpu
	.global	_ODV_RestoreDefaultParameters
	.global	_PAR_Capacity_Left
	.global	_PAR_WriteStatisticParam
	.global	_PAR_Capacity_Total
	.global	_PAR_StoreODSubIndex
	.global	_ODP_OnTime
	.global	_ODV_Gateway_Errorcode
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODP_Board_RevisionNumber
	.global	_ODV_RTC_Text
	.global	_ODV_Gateway_Date_Time
	.global	_PAR_Capacity_TotalLife_Used
	.global	_golden_CRC_values
	.global	_TSK_timerSem
	.global	_ODV_Read_Analogue_Input_16_Bit
	.global	_PieCtrlRegs
	.global	_GpioDataRegs
	.global	_sci_rx_mbox
	.global	_mailboxSDOout
	.global	_can_rx_mbox
	.global	_can_tx_mbox
	.global	_ODV_Modules_MinCellVoltage
	.global	_ODV_Modules_MaxCellVoltage
	.global	_ODV_Modules_Heater
	.global	_ODV_Modules_Alarms
	.global	_ODV_Modules_Temperature
	.global	_ODI_gateway_dict_Data
	.global	FS$$DIV
	.global	ULL$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$146	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$146, DW_AT_byte_size(0x01)
$C$DW$542	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$543	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$146

$C$DW$T$147	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$544, DW_AT_name("cob_id")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$545, DW_AT_name("rtr")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$546, DW_AT_name("len")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$547, DW_AT_name("data")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$137	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$548, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$549, DW_AT_name("csSDO")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$550, DW_AT_name("csEmergency")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$551, DW_AT_name("csSYNC")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$552, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$553, DW_AT_name("csPDO")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$554, DW_AT_name("csLSS")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$555, DW_AT_name("errCode")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$556, DW_AT_name("errRegMask")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$557, DW_AT_name("active")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)

$C$DW$T$129	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$129, DW_AT_byte_size(0x18)
$C$DW$558	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$558, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$129


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$559, DW_AT_name("index")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$560, DW_AT_name("subindex")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$561, DW_AT_name("size")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$562, DW_AT_name("address")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$134	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
$C$DW$T$135	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$T$135, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$563, DW_AT_name("SwitchOn")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$564, DW_AT_name("EnableVolt")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$565, DW_AT_name("QuickStop")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$566, DW_AT_name("EnableOperation")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$567, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$568, DW_AT_name("ResetFault")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$569, DW_AT_name("Halt")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$570, DW_AT_name("Oms")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$571, DW_AT_name("Rsvd")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$572, DW_AT_name("Manufacturer")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$573, DW_AT_name("SwitchOn")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$574, DW_AT_name("EnableVolt")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$575, DW_AT_name("QuickStop")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$576, DW_AT_name("EnableOperation")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$576, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$577, DW_AT_name("Rsvd0")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$577, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$578, DW_AT_name("ResetFault")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$579, DW_AT_name("Halt")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$580, DW_AT_name("Oms")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$581, DW_AT_name("Rsvd")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$581, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$582, DW_AT_name("Manufacturer")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$582, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$583, DW_AT_name("SwitchOn")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$583, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$584, DW_AT_name("EnableVolt")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$584, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$585, DW_AT_name("QuickStop")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$585, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$586, DW_AT_name("EnableOperation")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$586, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$587, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$587, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$588, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$589, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$590, DW_AT_name("ResetFault")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$591, DW_AT_name("Halt")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$591, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$592, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$592, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$593, DW_AT_name("Rsvd")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$594, DW_AT_name("Manufacturer")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$595, DW_AT_name("can_wk")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$596, DW_AT_name("sw_wk")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$597, DW_AT_name("ch_wk")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$597, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$598, DW_AT_name("SOC2")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$598, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$599, DW_AT_name("lem")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$600, DW_AT_name("onerelay")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_onerelay")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$601, DW_AT_name("b6")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_b6")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$602, DW_AT_name("b7")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_b7")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$603, DW_AT_name("en24v")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_en24v")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$604, DW_AT_name("b9")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_b9")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$605, DW_AT_name("b10")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_b10")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$606, DW_AT_name("b11")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_b11")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$607, DW_AT_name("b12")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$608, DW_AT_name("b13")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$609, DW_AT_name("b14")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$610, DW_AT_name("b15")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$148	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$148, DW_AT_language(DW_LANG_C)
$C$DW$T$149	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x16)

$C$DW$T$28	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$611, DW_AT_name("ControlWord")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$612, DW_AT_name("AnyMode")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$613, DW_AT_name("VelocityMode")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$614, DW_AT_name("PositionMode")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$150	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x02)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$615, DW_AT_name("rsvd1")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$616, DW_AT_name("rsvd2")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$617, DW_AT_name("AIO2")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$618, DW_AT_name("rsvd3")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$619, DW_AT_name("AIO4")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$620, DW_AT_name("rsvd4")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$621, DW_AT_name("AIO6")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$622, DW_AT_name("rsvd5")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$623, DW_AT_name("rsvd6")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$623, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$624, DW_AT_name("rsvd7")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$625, DW_AT_name("AIO10")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$626, DW_AT_name("rsvd8")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$627, DW_AT_name("AIO12")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$628, DW_AT_name("rsvd9")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$629, DW_AT_name("AIO14")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$630, DW_AT_name("rsvd10")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$631, DW_AT_name("rsvd11")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x02)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$632, DW_AT_name("all")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$633, DW_AT_name("bit")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$634, DW_AT_name("GPIO0")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$635, DW_AT_name("GPIO1")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$636, DW_AT_name("GPIO2")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$636, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$637, DW_AT_name("GPIO3")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$637, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$638, DW_AT_name("GPIO4")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$639, DW_AT_name("GPIO5")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$639, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$640, DW_AT_name("GPIO6")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$640, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$641, DW_AT_name("GPIO7")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$641, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$642, DW_AT_name("GPIO8")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$642, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$643, DW_AT_name("GPIO9")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$643, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$644, DW_AT_name("GPIO10")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$644, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$645, DW_AT_name("GPIO11")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$645, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$646, DW_AT_name("GPIO12")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$646, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$647, DW_AT_name("GPIO13")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$647, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$648, DW_AT_name("GPIO14")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$648, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$649, DW_AT_name("GPIO15")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$649, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$650, DW_AT_name("GPIO16")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$650, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$651, DW_AT_name("GPIO17")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$651, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$652, DW_AT_name("GPIO18")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$652, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$653, DW_AT_name("GPIO19")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$653, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$654, DW_AT_name("GPIO20")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$654, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$655, DW_AT_name("GPIO21")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$655, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$656, DW_AT_name("GPIO22")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$656, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$657, DW_AT_name("GPIO23")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$657, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$658, DW_AT_name("GPIO24")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$658, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$659, DW_AT_name("GPIO25")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$659, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$660, DW_AT_name("GPIO26")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$660, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$661, DW_AT_name("GPIO27")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$662, DW_AT_name("GPIO28")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$663, DW_AT_name("GPIO29")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$663, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$664, DW_AT_name("GPIO30")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$664, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$665, DW_AT_name("GPIO31")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$665, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$666, DW_AT_name("all")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$667, DW_AT_name("bit")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$668, DW_AT_name("GPIO32")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$669, DW_AT_name("GPIO33")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$670, DW_AT_name("GPIO34")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$671, DW_AT_name("GPIO35")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$672, DW_AT_name("GPIO36")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$673, DW_AT_name("GPIO37")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$673, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$674, DW_AT_name("GPIO38")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$674, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$675, DW_AT_name("GPIO39")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$675, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$676, DW_AT_name("GPIO40")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$676, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$677, DW_AT_name("GPIO41")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$677, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$678, DW_AT_name("GPIO42")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$678, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$679, DW_AT_name("GPIO43")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$680, DW_AT_name("GPIO44")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$681, DW_AT_name("rsvd1")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$682, DW_AT_name("rsvd2")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$682, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$683, DW_AT_name("GPIO50")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$683, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$684, DW_AT_name("GPIO51")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$685, DW_AT_name("GPIO52")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$686, DW_AT_name("GPIO53")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$687, DW_AT_name("GPIO54")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$688, DW_AT_name("GPIO55")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$689, DW_AT_name("GPIO56")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$690, DW_AT_name("GPIO57")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$691, DW_AT_name("GPIO58")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$692, DW_AT_name("rsvd3")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$692, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$36, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$693, DW_AT_name("all")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$694, DW_AT_name("bit")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x20)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$695, DW_AT_name("GPADAT")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$696, DW_AT_name("GPASET")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$697, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$698, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$699, DW_AT_name("GPBDAT")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$700, DW_AT_name("GPBSET")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$701, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$702, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$703, DW_AT_name("rsvd1")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$704, DW_AT_name("AIODAT")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$705, DW_AT_name("AIOSET")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$706, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$707, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38

$C$DW$708	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$38)
$C$DW$T$153	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$708)

$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x08)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$709, DW_AT_name("wListElem")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$710, DW_AT_name("wCount")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$711, DW_AT_name("fxn")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x16)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)

$C$DW$T$53	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$53, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x30)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$712, DW_AT_name("dataQue")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$713, DW_AT_name("freeQue")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$714, DW_AT_name("dataSem")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$715, DW_AT_name("freeSem")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$716, DW_AT_name("segid")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$717, DW_AT_name("size")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$718, DW_AT_name("length")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$719, DW_AT_name("name")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53

$C$DW$T$154	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$154, DW_AT_language(DW_LANG_C)
$C$DW$T$156	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$156, DW_AT_address_class(0x16)
$C$DW$T$157	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$720, DW_AT_name("ACK1")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$720, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$721, DW_AT_name("ACK2")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$721, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$722, DW_AT_name("ACK3")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$722, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$723, DW_AT_name("ACK4")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$723, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$724, DW_AT_name("ACK5")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$724, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$725, DW_AT_name("ACK6")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$725, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$726, DW_AT_name("ACK7")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$726, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$727, DW_AT_name("ACK8")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$727, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$728, DW_AT_name("ACK9")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$728, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$729, DW_AT_name("ACK10")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$729, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$730, DW_AT_name("ACK11")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$730, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$731, DW_AT_name("ACK12")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$731, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$732, DW_AT_name("rsvd1")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$55, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x01)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$733, DW_AT_name("all")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$734, DW_AT_name("bit")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x01)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$735, DW_AT_name("ENPIE")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$735, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$736, DW_AT_name("PIEVECT")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$736, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$57, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$737, DW_AT_name("all")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$738, DW_AT_name("bit")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$58, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x01)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$739, DW_AT_name("INTx1")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$739, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$740, DW_AT_name("INTx2")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$740, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$741, DW_AT_name("INTx3")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$741, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$742, DW_AT_name("INTx4")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$742, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$743, DW_AT_name("INTx5")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$743, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$744, DW_AT_name("INTx6")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$744, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$745, DW_AT_name("INTx7")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$745, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$746, DW_AT_name("INTx8")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$746, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$747, DW_AT_name("rsvd1")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$747, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$59, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$748, DW_AT_name("all")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$749, DW_AT_name("bit")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$60, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x01)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$750, DW_AT_name("INTx1")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$750, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$751, DW_AT_name("INTx2")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$751, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$752, DW_AT_name("INTx3")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$752, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$753, DW_AT_name("INTx4")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$753, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$754, DW_AT_name("INTx5")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$754, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$755, DW_AT_name("INTx6")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$755, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$756, DW_AT_name("INTx7")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$756, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$757, DW_AT_name("INTx8")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$757, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$758, DW_AT_name("rsvd1")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$758, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$61, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x01)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$759, DW_AT_name("all")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$760, DW_AT_name("bit")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$62, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x1a)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$761, DW_AT_name("PIECTRL")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$762, DW_AT_name("PIEACK")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$763, DW_AT_name("PIEIER1")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$764, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$765, DW_AT_name("PIEIER2")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$766, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$767, DW_AT_name("PIEIER3")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$768, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$769, DW_AT_name("PIEIER4")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$770, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$771, DW_AT_name("PIEIER5")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$772, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$773, DW_AT_name("PIEIER6")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$774, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$775, DW_AT_name("PIEIER7")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$776, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$777, DW_AT_name("PIEIER8")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$778, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$779, DW_AT_name("PIEIER9")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$780, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$781, DW_AT_name("PIEIER10")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$782, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$783, DW_AT_name("PIEIER11")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$784, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$785, DW_AT_name("PIEIER12")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$786, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62

$C$DW$787	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$62)
$C$DW$T$160	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$787)

$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x04)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$788, DW_AT_name("next")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$789, DW_AT_name("prev")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$64

$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$63	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$63, DW_AT_address_class(0x16)

$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x10)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$790, DW_AT_name("job")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$791, DW_AT_name("count")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$792, DW_AT_name("pendQ")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$793, DW_AT_name("name")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66

$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$162	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$162, DW_AT_address_class(0x16)
$C$DW$T$163	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$163, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$164	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$164, DW_AT_language(DW_LANG_C)

$C$DW$T$43	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$794	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$T$43

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$97	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)
$C$DW$795	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$96)
	.dwendtag $C$DW$T$97

$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
$C$DW$T$99	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$111, DW_AT_language(DW_LANG_C)
$C$DW$T$100	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)

$C$DW$T$106	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)
$C$DW$796	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$96)
$C$DW$797	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$106

$C$DW$T$107	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$107, DW_AT_address_class(0x16)
$C$DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)

$C$DW$T$130	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)
$C$DW$798	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$96)
$C$DW$799	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$6)
$C$DW$800	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$9)
$C$DW$801	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$130

$C$DW$T$131	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$131, DW_AT_address_class(0x16)
$C$DW$T$132	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$175	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$175, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x62)
$C$DW$802	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$802, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$175

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$803	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$803, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$804	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$6)
$C$DW$T$85	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$804)
$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)

$C$DW$T$187	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$187, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$187, DW_AT_byte_size(0x62)
$C$DW$805	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$805, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$187

$C$DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$104	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$104, DW_AT_address_class(0x16)

$C$DW$T$188	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$188, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$188, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$188, DW_AT_byte_size(0x13)
$C$DW$806	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$806, DW_AT_upper_bound(0x12)
	.dwendtag $C$DW$T$188

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$189	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$189, DW_AT_language(DW_LANG_C)
$C$DW$807	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$9)
$C$DW$T$83	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$807)
$C$DW$T$84	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_address_class(0x16)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$198	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$198, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$198, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$198, DW_AT_byte_size(0x01)
$C$DW$808	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$808, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$198

$C$DW$T$67	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)

$C$DW$T$37	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x08)
$C$DW$809	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$809, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$37

$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)

$C$DW$T$87	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)
$C$DW$810	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$6)
$C$DW$811	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$87

$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)
$C$DW$T$103	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$103, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)
$C$DW$812	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$96)
$C$DW$813	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$77)
$C$DW$814	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$6)
$C$DW$815	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$114

$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x16)
$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)
$C$DW$816	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$116)
$C$DW$T$117	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$816)
$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)

$C$DW$T$123	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
$C$DW$817	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$96)
$C$DW$818	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$9)
$C$DW$819	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$123

$C$DW$T$124	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$124, DW_AT_address_class(0x16)
$C$DW$T$125	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
$C$DW$820	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$13)
$C$DW$T$208	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$208, DW_AT_type(*$C$DW$820)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$68	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$51	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$51, DW_AT_address_class(0x16)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)

$C$DW$T$210	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$210, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$210, DW_AT_byte_size(0x10)
$C$DW$821	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$821, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$210


$C$DW$T$69	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$69, DW_AT_name("crc_record")
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x08)
$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$822, DW_AT_name("crc_alg_ID")
	.dwattr $C$DW$822, DW_AT_TI_symbol_name("_crc_alg_ID")
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$823, DW_AT_name("page_id")
	.dwattr $C$DW$823, DW_AT_TI_symbol_name("_page_id")
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$824, DW_AT_name("addr")
	.dwattr $C$DW$824, DW_AT_TI_symbol_name("_addr")
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$825, DW_AT_name("size")
	.dwattr $C$DW$825, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$826, DW_AT_name("crc_value")
	.dwattr $C$DW$826, DW_AT_TI_symbol_name("_crc_value")
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$69

$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_RECORD")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)

$C$DW$T$71	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x08)
$C$DW$827	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$827, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$71


$C$DW$T$72	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$72, DW_AT_name("crc_table")
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x0a)
$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$828, DW_AT_name("rec_size")
	.dwattr $C$DW$828, DW_AT_TI_symbol_name("_rec_size")
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$829, DW_AT_name("num_recs")
	.dwattr $C$DW$829, DW_AT_TI_symbol_name("_num_recs")
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$830	.dwtag  DW_TAG_member
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$830, DW_AT_name("recs")
	.dwattr $C$DW$830, DW_AT_TI_symbol_name("_recs")
	.dwattr $C$DW$830, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$830, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$72

$C$DW$T$213	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_TABLE")
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$213, DW_AT_language(DW_LANG_C)

$C$DW$T$126	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$126, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x01)
$C$DW$831	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$832	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$126

$C$DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)

$C$DW$T$92	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$92, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x01)
$C$DW$833	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$834	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$835	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$836	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$837	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$838	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$839	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$840	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$92

$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)

$C$DW$T$109	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x80)
$C$DW$841	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$841, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$109


$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x06)
$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$842, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$842, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$843, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$843, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$844, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$844, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$845, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$845, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$846, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$846, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$847, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$847, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73

$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$848	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$80)
$C$DW$T$81	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$848)
$C$DW$T$82	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x16)

$C$DW$T$136	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$136, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x132)
$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$849, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$849, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$850, DW_AT_name("objdict")
	.dwattr $C$DW$850, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$851, DW_AT_name("PDO_status")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$852, DW_AT_name("firstIndex")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$853, DW_AT_name("lastIndex")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$854, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$855, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$856, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$857, DW_AT_name("transfers")
	.dwattr $C$DW$857, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$858, DW_AT_name("nodeState")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$859, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$860, DW_AT_name("initialisation")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$861, DW_AT_name("preOperational")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$862, DW_AT_name("operational")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$863, DW_AT_name("stopped")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$864, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$865, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$866, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$867, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$868, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$869, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$870, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$871, DW_AT_name("heartbeatError")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$872, DW_AT_name("NMTable")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$873, DW_AT_name("syncTimer")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$874, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$875, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$876, DW_AT_name("pre_sync")
	.dwattr $C$DW$876, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$877, DW_AT_name("post_TPDO")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$878, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$879, DW_AT_name("toggle")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$880, DW_AT_name("canHandle")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$881, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$882, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$883, DW_AT_name("globalCallback")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$884, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$885	.dwtag  DW_TAG_member
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$885, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$885, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$885, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$885, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$886, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$887, DW_AT_name("dcf_request")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$888, DW_AT_name("error_state")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$889, DW_AT_name("error_history_size")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$890, DW_AT_name("error_number")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$891, DW_AT_name("error_first_element")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$892	.dwtag  DW_TAG_member
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$892, DW_AT_name("error_register")
	.dwattr $C$DW$892, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$892, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$892, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$893	.dwtag  DW_TAG_member
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$893, DW_AT_name("error_cobid")
	.dwattr $C$DW$893, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$893, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$893, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$894	.dwtag  DW_TAG_member
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$894, DW_AT_name("error_data")
	.dwattr $C$DW$894, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$894, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$894, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$895	.dwtag  DW_TAG_member
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$895, DW_AT_name("post_emcy")
	.dwattr $C$DW$895, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$895, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$895, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$896	.dwtag  DW_TAG_member
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$896, DW_AT_name("lss_transfer")
	.dwattr $C$DW$896, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$896, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$896, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$897	.dwtag  DW_TAG_member
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$897, DW_AT_name("eeprom_index")
	.dwattr $C$DW$897, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$897, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$897, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$898	.dwtag  DW_TAG_member
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$898, DW_AT_name("eeprom_size")
	.dwattr $C$DW$898, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$898, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$898, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$136

$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$96	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$96, DW_AT_address_class(0x16)

$C$DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$138, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x0e)
$C$DW$899	.dwtag  DW_TAG_member
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$899, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$899, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$899, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$899, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$900	.dwtag  DW_TAG_member
	.dwattr $C$DW$900, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$900, DW_AT_name("event_timer")
	.dwattr $C$DW$900, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$900, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$900, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$901	.dwtag  DW_TAG_member
	.dwattr $C$DW$901, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$901, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$901, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$901, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$901, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$902	.dwtag  DW_TAG_member
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$902, DW_AT_name("last_message")
	.dwattr $C$DW$902, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$902, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$902, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$138

$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)

$C$DW$T$140	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$140, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x14)
$C$DW$903	.dwtag  DW_TAG_member
	.dwattr $C$DW$903, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$903, DW_AT_name("nodeId")
	.dwattr $C$DW$903, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$903, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$903, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$904	.dwtag  DW_TAG_member
	.dwattr $C$DW$904, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$904, DW_AT_name("whoami")
	.dwattr $C$DW$904, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$904, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$904, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$905	.dwtag  DW_TAG_member
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$905, DW_AT_name("state")
	.dwattr $C$DW$905, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$905, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$905, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$906	.dwtag  DW_TAG_member
	.dwattr $C$DW$906, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$906, DW_AT_name("toggle")
	.dwattr $C$DW$906, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$906, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$906, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$907	.dwtag  DW_TAG_member
	.dwattr $C$DW$907, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$907, DW_AT_name("abortCode")
	.dwattr $C$DW$907, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$907, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$907, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$908	.dwtag  DW_TAG_member
	.dwattr $C$DW$908, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$908, DW_AT_name("index")
	.dwattr $C$DW$908, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$908, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$908, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$909	.dwtag  DW_TAG_member
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$909, DW_AT_name("subIndex")
	.dwattr $C$DW$909, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$909, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$909, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$910	.dwtag  DW_TAG_member
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$910, DW_AT_name("port")
	.dwattr $C$DW$910, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$910, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$910, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$911	.dwtag  DW_TAG_member
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$911, DW_AT_name("count")
	.dwattr $C$DW$911, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$911, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$911, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$912	.dwtag  DW_TAG_member
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$912, DW_AT_name("offset")
	.dwattr $C$DW$912, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$912, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$912, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$913	.dwtag  DW_TAG_member
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$913, DW_AT_name("datap")
	.dwattr $C$DW$913, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$913, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$913, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$914	.dwtag  DW_TAG_member
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$914, DW_AT_name("dataType")
	.dwattr $C$DW$914, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$914, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$914, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$915	.dwtag  DW_TAG_member
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$915, DW_AT_name("timer")
	.dwattr $C$DW$915, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$915, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$915, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$916	.dwtag  DW_TAG_member
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$916, DW_AT_name("Callback")
	.dwattr $C$DW$916, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$916, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$916, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$140

$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)

$C$DW$T$91	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x3c)
$C$DW$917	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$917, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$91


$C$DW$T$144	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$144, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$144, DW_AT_byte_size(0x04)
$C$DW$918	.dwtag  DW_TAG_member
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$918, DW_AT_name("pSubindex")
	.dwattr $C$DW$918, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$918, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$918, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$919	.dwtag  DW_TAG_member
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$919, DW_AT_name("bSubCount")
	.dwattr $C$DW$919, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$919, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$919, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$920	.dwtag  DW_TAG_member
	.dwattr $C$DW$920, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$920, DW_AT_name("index")
	.dwattr $C$DW$920, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$920, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$920, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$144

$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$921	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$75)
$C$DW$T$76	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$921)
$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)

$C$DW$T$120	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)
$C$DW$922	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$9)
$C$DW$923	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$103)
$C$DW$924	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$119)
	.dwendtag $C$DW$T$120

$C$DW$T$121	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$121, DW_AT_address_class(0x16)
$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)

$C$DW$T$145	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$145, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x08)
$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$925, DW_AT_name("bAccessType")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$926	.dwtag  DW_TAG_member
	.dwattr $C$DW$926, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$926, DW_AT_name("bDataType")
	.dwattr $C$DW$926, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$926, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$926, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$927, DW_AT_name("size")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$928, DW_AT_name("pObject")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$929, DW_AT_name("bProcessor")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$145

$C$DW$930	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$930, DW_AT_type(*$C$DW$T$145)
$C$DW$T$141	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$930)
$C$DW$T$142	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)
$C$DW$T$143	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$931	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$931, DW_AT_location[DW_OP_reg0]
$C$DW$932	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$932, DW_AT_location[DW_OP_reg1]
$C$DW$933	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$933, DW_AT_location[DW_OP_reg2]
$C$DW$934	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$934, DW_AT_location[DW_OP_reg3]
$C$DW$935	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$935, DW_AT_location[DW_OP_reg20]
$C$DW$936	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$936, DW_AT_location[DW_OP_reg21]
$C$DW$937	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$937, DW_AT_location[DW_OP_reg22]
$C$DW$938	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$938, DW_AT_location[DW_OP_reg23]
$C$DW$939	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$939, DW_AT_location[DW_OP_reg24]
$C$DW$940	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$940, DW_AT_location[DW_OP_reg25]
$C$DW$941	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$941, DW_AT_location[DW_OP_reg26]
$C$DW$942	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$942, DW_AT_location[DW_OP_reg28]
$C$DW$943	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$943, DW_AT_location[DW_OP_reg29]
$C$DW$944	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$944, DW_AT_location[DW_OP_reg30]
$C$DW$945	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$945, DW_AT_location[DW_OP_reg31]
$C$DW$946	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$946, DW_AT_location[DW_OP_regx 0x20]
$C$DW$947	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$947, DW_AT_location[DW_OP_regx 0x21]
$C$DW$948	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$948, DW_AT_location[DW_OP_regx 0x22]
$C$DW$949	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$949, DW_AT_location[DW_OP_regx 0x23]
$C$DW$950	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$950, DW_AT_location[DW_OP_regx 0x24]
$C$DW$951	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$951, DW_AT_location[DW_OP_regx 0x25]
$C$DW$952	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$952, DW_AT_location[DW_OP_regx 0x26]
$C$DW$953	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$953, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$954	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$954, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$955	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$955, DW_AT_location[DW_OP_reg4]
$C$DW$956	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$956, DW_AT_location[DW_OP_reg6]
$C$DW$957	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$957, DW_AT_location[DW_OP_reg8]
$C$DW$958	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$958, DW_AT_location[DW_OP_reg10]
$C$DW$959	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$959, DW_AT_location[DW_OP_reg12]
$C$DW$960	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$960, DW_AT_location[DW_OP_reg14]
$C$DW$961	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$961, DW_AT_location[DW_OP_reg16]
$C$DW$962	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$962, DW_AT_location[DW_OP_reg17]
$C$DW$963	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$963, DW_AT_location[DW_OP_reg18]
$C$DW$964	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$964, DW_AT_location[DW_OP_reg19]
$C$DW$965	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$965, DW_AT_location[DW_OP_reg5]
$C$DW$966	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$966, DW_AT_location[DW_OP_reg7]
$C$DW$967	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$967, DW_AT_location[DW_OP_reg9]
$C$DW$968	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$968, DW_AT_location[DW_OP_reg11]
$C$DW$969	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$969, DW_AT_location[DW_OP_reg13]
$C$DW$970	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$970, DW_AT_location[DW_OP_reg15]
$C$DW$971	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$971, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$972	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$972, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$973	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$973, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$974	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$974, DW_AT_location[DW_OP_regx 0x30]
$C$DW$975	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$975, DW_AT_location[DW_OP_regx 0x33]
$C$DW$976	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$976, DW_AT_location[DW_OP_regx 0x34]
$C$DW$977	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$977, DW_AT_location[DW_OP_regx 0x37]
$C$DW$978	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$978, DW_AT_location[DW_OP_regx 0x38]
$C$DW$979	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$979, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$980	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$980, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$981	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$981, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$982	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$982, DW_AT_location[DW_OP_regx 0x40]
$C$DW$983	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$983, DW_AT_location[DW_OP_regx 0x43]
$C$DW$984	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$984, DW_AT_location[DW_OP_regx 0x44]
$C$DW$985	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$985, DW_AT_location[DW_OP_regx 0x47]
$C$DW$986	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$986, DW_AT_location[DW_OP_regx 0x48]
$C$DW$987	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$987, DW_AT_location[DW_OP_regx 0x49]
$C$DW$988	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$988, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$989	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$989, DW_AT_location[DW_OP_regx 0x27]
$C$DW$990	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$990, DW_AT_location[DW_OP_regx 0x28]
$C$DW$991	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$991, DW_AT_location[DW_OP_reg27]
$C$DW$992	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$992, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

