;***************************************************************
;* TMS320C2000 C/C++ Codegen                         PC v6.2.0 *
;* Date/Time created: Mon Oct 28 12:12:58 2019                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.0 Copyright (c) 1996-2013 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Johnny\V31\NewCAN\MMS_F_AEC\Standard_AEC_160\AEC_Standard\PTM079_LV")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SDO_Successful+0,32
	.bits	0,16			; _SDO_Successful @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_DIC_NodeId+0,32
	.bits	1,16			; _DIC_NodeId @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_post")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SEM_post")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$125)
	.dwendtag $C$DW$1

	.global	_Dummy_Status
_Dummy_Status:	.usect	".ebss",1,1,0
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("Dummy_Status")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_Dummy_Status")
	.dwattr $C$DW$3, DW_AT_location[DW_OP_addr _Dummy_Status]
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$3, DW_AT_external
	.global	_SDO_Successful
_SDO_Successful:	.usect	".ebss",1,1,0
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("SDO_Successful")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_SDO_Successful")
	.dwattr $C$DW$4, DW_AT_location[DW_OP_addr _SDO_Successful]
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$4, DW_AT_external
	.global	_DIC_NodeId
_DIC_NodeId:	.usect	".ebss",1,1,0
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("DIC_NodeId")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_DIC_NodeId")
	.dwattr $C$DW$5, DW_AT_location[DW_OP_addr _DIC_NodeId]
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$5, DW_AT_external

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("writeNetworkDictCallBack")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_writeNetworkDictCallBack")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$67)
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$9)
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$6)
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$13)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$6)
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$3)
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$110)
	.dwendtag $C$DW$6


$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("closeSDOtransfer")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_closeSDOtransfer")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$67)
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$6)
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$15


$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("getReadResultNetworkDict")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_getReadResultNetworkDict")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$67)
$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$6)
$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$3)
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$74)
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$74)
	.dwendtag $C$DW$19


$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$122)
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$126)
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$36)
	.dwendtag $C$DW$25


$C$DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$122)
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$126)
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$36)
	.dwendtag $C$DW$29


$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("readNetworkDictCallback")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_readNetworkDictCallback")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$67)
$C$DW$35	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$6)
$C$DW$36	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$9)
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$6)
$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$110)
$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$33


$C$DW$41	.dwtag  DW_TAG_subprogram, DW_AT_name("getWriteResultNetworkDict")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_getWriteResultNetworkDict")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$67)
$C$DW$43	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$74)
	.dwendtag $C$DW$41


$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("getSDOlineOnUseFromIndex")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_getSDOlineOnUseFromIndex")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$67)
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$9)
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
$C$DW$49	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$24)
	.dwendtag $C$DW$45


$C$DW$50	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$125)
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$36)
	.dwendtag $C$DW$50

$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
	.global	_Read_pdata
_Read_pdata:	.usect	".ebss",2,1,1
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("Read_pdata")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_Read_pdata")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_addr _Read_pdata]
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$54, DW_AT_external

$C$DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("_getODentry")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("__getODentry")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$67)
$C$DW$57	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$9)
$C$DW$58	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
$C$DW$59	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$3)
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$74)
$C$DW$61	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$24)
$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$6)
$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$55


$C$DW$65	.dwtag  DW_TAG_subprogram, DW_AT_name("CLK_getltime")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_CLK_getltime")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external

$C$DW$66	.dwtag  DW_TAG_subprogram, DW_AT_name("_setODentry")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("__setODentry")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$67)
$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$9)
$C$DW$69	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$6)
$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$3)
$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$74)
$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$6)
$C$DW$73	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$6)
$C$DW$74	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$66

$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("semaphoreSDOdone")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_semaphoreSDOdone")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("mailboxSDOout")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_mailboxSDOout")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.0\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0870412 
	.sect	".text"
	.global	_WriteDictEnabled

$C$DW$78	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteDictEnabled")
	.dwattr $C$DW$78, DW_AT_low_pc(_WriteDictEnabled)
	.dwattr $C$DW$78, DW_AT_high_pc(0x00)
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_WriteDictEnabled")
	.dwattr $C$DW$78, DW_AT_external
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$78, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$78, DW_AT_TI_begin_line(0x31)
	.dwattr $C$DW$78, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$78, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 49,column 53,is_stmt,address _WriteDictEnabled

	.dwfde $C$DW$CIE, _WriteDictEnabled
$C$DW$79	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg0]
$C$DW$80	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteDictEnabled             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_WriteDictEnabled:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_breg20 -1]
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[2],AH            ; [CPU_] |49| 
        MOV       *-SP[1],AL            ; [CPU_] |49| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 50,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |50| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 51,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$83	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$83, DW_AT_low_pc(0x00)
	.dwattr $C$DW$83, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$78, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$78, DW_AT_TI_end_line(0x33)
	.dwattr $C$DW$78, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$78

	.sect	".text"
	.global	_DIC_SetNodeId

$C$DW$84	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_SetNodeId")
	.dwattr $C$DW$84, DW_AT_low_pc(_DIC_SetNodeId)
	.dwattr $C$DW$84, DW_AT_high_pc(0x00)
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_DIC_SetNodeId")
	.dwattr $C$DW$84, DW_AT_external
	.dwattr $C$DW$84, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$84, DW_AT_TI_begin_line(0x35)
	.dwattr $C$DW$84, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$84, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 53,column 33,is_stmt,address _DIC_SetNodeId

	.dwfde $C$DW$CIE, _DIC_SetNodeId
$C$DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("fnodeId")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_fnodeId")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DIC_SetNodeId                FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_DIC_SetNodeId:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("fnodeId")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_fnodeId")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |53| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 54,column 3,is_stmt
        MOVW      DP,#_DIC_NodeId       ; [CPU_U] 
        MOV       @_DIC_NodeId,AL       ; [CPU_] |54| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 55,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$84, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$84, DW_AT_TI_end_line(0x37)
	.dwattr $C$DW$84, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$84

	.sect	".text"
	.global	_DIC_CheckEqual

$C$DW$88	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_CheckEqual")
	.dwattr $C$DW$88, DW_AT_low_pc(_DIC_CheckEqual)
	.dwattr $C$DW$88, DW_AT_high_pc(0x00)
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_DIC_CheckEqual")
	.dwattr $C$DW$88, DW_AT_external
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$88, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$88, DW_AT_TI_begin_line(0x39)
	.dwattr $C$DW$88, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$88, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 57,column 83,is_stmt,address _DIC_CheckEqual

	.dwfde $C$DW$CIE, _DIC_CheckEqual
$C$DW$89	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg0]
$C$DW$90	.dwtag  DW_TAG_formal_parameter, DW_AT_name("subindex")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg1]
$C$DW$91	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index2")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_index2")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg12]
$C$DW$92	.dwtag  DW_TAG_formal_parameter, DW_AT_name("subindex2")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_subindex2")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DIC_CheckEqual               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_DIC_CheckEqual:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -1]
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("subindex")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_breg20 -2]
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("index2")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_index2")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_breg20 -3]
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("subindex2")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_subindex2")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |57| 
        MOV       *-SP[3],AR4           ; [CPU_] |57| 
        MOV       *-SP[2],AH            ; [CPU_] |57| 
        MOV       *-SP[1],AL            ; [CPU_] |57| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 58,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |58| 
        MOV       AH,*-SP[3]            ; [CPU_] |58| 
        CMP       AH,*-SP[1]            ; [CPU_] |58| 
        BF        $C$L1,NEQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOV       AH,*-SP[4]            ; [CPU_] |58| 
        CMP       AH,*-SP[2]            ; [CPU_] |58| 
        BF        $C$L1,NEQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOVB      AL,#1                 ; [CPU_] |58| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 59,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$88, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$88, DW_AT_TI_end_line(0x3b)
	.dwattr $C$DW$88, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$88

	.sect	".text"
	.global	_DIC_ReadLocalDict

$C$DW$98	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_ReadLocalDict")
	.dwattr $C$DW$98, DW_AT_low_pc(_DIC_ReadLocalDict)
	.dwattr $C$DW$98, DW_AT_high_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_DIC_ReadLocalDict")
	.dwattr $C$DW$98, DW_AT_external
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$98, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$98, DW_AT_TI_begin_line(0x3e)
	.dwattr $C$DW$98, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$98, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 62,column 55,is_stmt,address _DIC_ReadLocalDict

	.dwfde $C$DW$CIE, _DIC_ReadLocalDict
$C$DW$99	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg0]
$C$DW$100	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DIC_ReadLocalDict            FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_DIC_ReadLocalDict:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -8]
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -9]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -12]
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("data_type")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_data_type")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_breg20 -13]
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -16]
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("pdest_data")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_pdest_data")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_breg20 -18]
        MOV       *-SP[9],AH            ; [CPU_] |62| 
        MOV       *-SP[8],AL            ; [CPU_] |62| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 63,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |63| 
        MOVL      *-SP[12],ACC          ; [CPU_] |63| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 66,column 21,is_stmt
        MOVL      *-SP[18],ACC          ; [CPU_] |66| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 68,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |68| 
        MOVZ      AR4,SP                ; [CPU_U] |68| 
        SUBB      XAR5,#12              ; [CPU_U] |68| 
        SUBB      XAR4,#13              ; [CPU_U] |68| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVU      ACC,AR5               ; [CPU_] |68| 
        MOVZ      AR5,SP                ; [CPU_U] |68| 
        MOVL      *-SP[2],ACC           ; [CPU_] |68| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |68| 
        MOV       *-SP[5],#0            ; [CPU_] |68| 
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |68| 
        MOV       *-SP[7],#0            ; [CPU_] |68| 
        MOV       AL,*-SP[8]            ; [CPU_] |68| 
        MOV       AH,*-SP[9]            ; [CPU_] |68| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |68| 
        SUBB      XAR5,#18              ; [CPU_U] |68| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("__getODentry")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |68| 
        ; call occurs [#__getODentry] ; [] |68| 
        MOVL      *-SP[16],ACC          ; [CPU_] |68| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 70,column 3,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |70| 
        MOVL      ACC,*-SP[16]          ; [CPU_] |70| 
        BF        $C$L2,NEQ             ; [CPU_] |70| 
        ; branchcc occurs ; [] |70| 
        MOVB      XAR6,#1               ; [CPU_] |70| 
$C$L2:    
        MOV       AL,AR6                ; [CPU_] |70| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 71,column 1,is_stmt
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$98, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$98, DW_AT_TI_end_line(0x47)
	.dwattr $C$DW$98, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$98

	.sect	".text"
	.global	_DIC_WriteLocalDict

$C$DW$109	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_WriteLocalDict")
	.dwattr $C$DW$109, DW_AT_low_pc(_DIC_WriteLocalDict)
	.dwattr $C$DW$109, DW_AT_high_pc(0x00)
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_DIC_WriteLocalDict")
	.dwattr $C$DW$109, DW_AT_external
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$109, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$109, DW_AT_TI_begin_line(0x4a)
	.dwattr $C$DW$109, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$109, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 74,column 56,is_stmt,address _DIC_WriteLocalDict

	.dwfde $C$DW$CIE, _DIC_WriteLocalDict
$C$DW$110	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg0]
$C$DW$111	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DIC_WriteLocalDict           FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            5 Parameter,  9 Auto,  0 SOE     *
;***************************************************************

_DIC_WriteLocalDict:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_breg20 -6]
$C$DW$113	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_breg20 -7]
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -10]
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -12]
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("pdest_data")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_pdest_data")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[7],AH            ; [CPU_] |74| 
        MOV       *-SP[6],AL            ; [CPU_] |74| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 75,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |75| 
        MOVL      *-SP[10],ACC          ; [CPU_] |75| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 78,column 21,is_stmt
        MOVL      *-SP[14],ACC          ; [CPU_] |78| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 79,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |79| 
        MOV       AH,*-SP[7]            ; [CPU_] |79| 
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("_WriteDictEnabled")
	.dwattr $C$DW$117, DW_AT_TI_call
        LCR       #_WriteDictEnabled    ; [CPU_] |79| 
        ; call occurs [#_WriteDictEnabled] ; [] |79| 
        CMPB      AL,#0                 ; [CPU_] |79| 
        BF        $C$L4,EQ              ; [CPU_] |79| 
        ; branchcc occurs ; [] |79| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 80,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |80| 
        SUBB      XAR4,#10              ; [CPU_U] |80| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |80| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |80| 
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |80| 
        MOV       *-SP[4],#0            ; [CPU_] |80| 
        MOVB      *-SP[5],#2,UNC        ; [CPU_] |80| 
        MOV       AL,*-SP[6]            ; [CPU_] |80| 
        MOV       AH,*-SP[7]            ; [CPU_] |80| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |80| 
        SUBB      XAR5,#14              ; [CPU_U] |80| 
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("__setODentry")
	.dwattr $C$DW$118, DW_AT_TI_call
        LCR       #__setODentry         ; [CPU_] |80| 
        ; call occurs [#__setODentry] ; [] |80| 
        MOVL      *-SP[12],ACC          ; [CPU_] |80| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 81,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |81| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |81| 
        BF        $C$L3,NEQ             ; [CPU_] |81| 
        ; branchcc occurs ; [] |81| 
        MOVB      XAR6,#1               ; [CPU_] |81| 
$C$L3:    
        MOV       AL,AR6                ; [CPU_] |81| 
        B         $C$L5,UNC             ; [CPU_] |81| 
        ; branch occurs ; [] |81| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 84,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |84| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 86,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$109, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$109, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$109, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$109

	.sect	".text"
	.global	_DIC_ReadNetworkDict

$C$DW$120	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_ReadNetworkDict")
	.dwattr $C$DW$120, DW_AT_low_pc(_DIC_ReadNetworkDict)
	.dwattr $C$DW$120, DW_AT_high_pc(0x00)
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_DIC_ReadNetworkDict")
	.dwattr $C$DW$120, DW_AT_external
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$120, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$120, DW_AT_TI_begin_line(0x5d)
	.dwattr $C$DW$120, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$120, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 93,column 70,is_stmt,address _DIC_ReadNetworkDict

	.dwfde $C$DW$CIE, _DIC_ReadNetworkDict
$C$DW$121	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeid")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_nodeid")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg0]
$C$DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg1]
$C$DW$123	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DIC_ReadNetworkDict          FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  9 Auto,  0 SOE     *
;***************************************************************

_DIC_ReadNetworkDict:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("nodeid")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_nodeid")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_breg20 -2]
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_breg20 -3]
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_breg20 -4]
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("statuscan")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_statuscan")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg20 -5]
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -6]
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -7]
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[4],AR4           ; [CPU_] |93| 
        MOV       *-SP[3],AH            ; [CPU_] |93| 
        MOV       *-SP[2],AL            ; [CPU_] |93| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 94,column 18,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |94| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 95,column 10,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |95| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 96,column 13,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |96| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 98,column 3,is_stmt
        B         $C$L11,UNC            ; [CPU_] |98| 
        ; branch occurs ; [] |98| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 99,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |99| 
        MOV       *-SP[1],AL            ; [CPU_] |99| 
        MOVZ      AR5,*-SP[4]           ; [CPU_] |99| 
        MOV       AH,*-SP[3]            ; [CPU_] |99| 
        MOV       AL,*-SP[2]            ; [CPU_] |99| 
        MOVZ      AR4,SP                ; [CPU_U] |99| 
        SUBB      XAR4,#5               ; [CPU_U] |99| 
$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_name("_DIC_ReadNetworkDict2")
	.dwattr $C$DW$131, DW_AT_TI_call
        LCR       #_DIC_ReadNetworkDict2 ; [CPU_] |99| 
        ; call occurs [#_DIC_ReadNetworkDict2] ; [] |99| 
        CMPB      AL,#0                 ; [CPU_] |99| 
        BF        $C$L10,EQ             ; [CPU_] |99| 
        ; branchcc occurs ; [] |99| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 100,column 7,is_stmt
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$132, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |100| 
        ; call occurs [#_CLK_getltime] ; [] |100| 
        MOVL      *-SP[10],ACC          ; [CPU_] |100| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 101,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |101| 
        ; branch occurs ; [] |101| 
$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 102,column 9,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |102| 
        MOVB      AL,#1                 ; [CPU_] |102| 
$C$DW$133	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$133, DW_AT_low_pc(0x00)
	.dwattr $C$DW$133, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$133, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |102| 
        ; call occurs [#_SEM_pend] ; [] |102| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 101,column 14,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |101| 
        CMPB      AL,#1                 ; [CPU_] |101| 
        B         $C$L9,HI              ; [CPU_] |101| 
        ; branchcc occurs ; [] |101| 
$C$DW$134	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$134, DW_AT_low_pc(0x00)
	.dwattr $C$DW$134, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$134, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |101| 
        ; call occurs [#_CLK_getltime] ; [] |101| 
        MOVB      XAR6,#102             ; [CPU_] |101| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |101| 
        CMPL      ACC,XAR6              ; [CPU_] |101| 
        B         $C$L7,LO              ; [CPU_] |101| 
        ; branchcc occurs ; [] |101| 
$C$L9:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 104,column 7,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |104| 
        CMPB      AL,#10                ; [CPU_] |104| 
        BF        $C$L10,NEQ            ; [CPU_] |104| 
        ; branchcc occurs ; [] |104| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 105,column 9,is_stmt
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |105| 
$C$L10:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 107,column 5,is_stmt
        INC       *-SP[6]               ; [CPU_] |107| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 98,column 10,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |98| 
        CMPB      AL,#3                 ; [CPU_] |98| 
        B         $C$L12,HIS            ; [CPU_] |98| 
        ; branchcc occurs ; [] |98| 
        MOV       AL,*-SP[7]            ; [CPU_] |98| 
        BF        $C$L6,EQ              ; [CPU_] |98| 
        ; branchcc occurs ; [] |98| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 109,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |109| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 110,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$135	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$135, DW_AT_low_pc(0x00)
	.dwattr $C$DW$135, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$120, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$120, DW_AT_TI_end_line(0x6e)
	.dwattr $C$DW$120, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$120

	.sect	".text"
	.global	_DIC_ReadNetworkDictTimeout

$C$DW$136	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_ReadNetworkDictTimeout")
	.dwattr $C$DW$136, DW_AT_low_pc(_DIC_ReadNetworkDictTimeout)
	.dwattr $C$DW$136, DW_AT_high_pc(0x00)
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_DIC_ReadNetworkDictTimeout")
	.dwattr $C$DW$136, DW_AT_external
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$136, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$136, DW_AT_TI_begin_line(0x70)
	.dwattr $C$DW$136, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$136, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 112,column 79,is_stmt,address _DIC_ReadNetworkDictTimeout

	.dwfde $C$DW$CIE, _DIC_ReadNetworkDictTimeout
$C$DW$137	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg0]
$C$DW$138	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg1]
$C$DW$139	.dwtag  DW_TAG_formal_parameter, DW_AT_name("timeout")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_timeout")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DIC_ReadNetworkDictTimeout   FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_DIC_ReadNetworkDictTimeout:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$140, DW_AT_location[DW_OP_breg20 -2]
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_breg20 -3]
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("timeout")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_timeout")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_breg20 -4]
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("statuscan")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_statuscan")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_breg20 -5]
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_breg20 -6]
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$145, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR4           ; [CPU_] |112| 
        MOV       *-SP[3],AH            ; [CPU_] |112| 
        MOV       *-SP[2],AL            ; [CPU_] |112| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 113,column 18,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |113| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 114,column 13,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |114| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 116,column 3,is_stmt
        MOVW      DP,#_DIC_NodeId       ; [CPU_U] 
        MOV       AL,*-SP[3]            ; [CPU_] |116| 
        MOVZ      AR4,SP                ; [CPU_U] |116| 
        MOV       *-SP[1],AL            ; [CPU_] |116| 
        MOVZ      AR5,*-SP[3]           ; [CPU_] |116| 
        MOV       AH,*-SP[2]            ; [CPU_] |116| 
        MOV       AL,@_DIC_NodeId       ; [CPU_] |116| 
        SUBB      XAR4,#5               ; [CPU_U] |116| 
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_name("_DIC_ReadNetworkDict2")
	.dwattr $C$DW$146, DW_AT_TI_call
        LCR       #_DIC_ReadNetworkDict2 ; [CPU_] |116| 
        ; call occurs [#_DIC_ReadNetworkDict2] ; [] |116| 
        CMPB      AL,#0                 ; [CPU_] |116| 
        BF        $C$L16,EQ             ; [CPU_] |116| 
        ; branchcc occurs ; [] |116| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 117,column 5,is_stmt
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$147, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |117| 
        ; call occurs [#_CLK_getltime] ; [] |117| 
        MOVL      *-SP[8],ACC           ; [CPU_] |117| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 118,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |118| 
        ; branch occurs ; [] |118| 
$C$L13:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 119,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |119| 
        MOVB      AL,#1                 ; [CPU_] |119| 
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$148, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |119| 
        ; call occurs [#_SEM_pend] ; [] |119| 
$C$L14:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 118,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |118| 
        CMPB      AL,#1                 ; [CPU_] |118| 
        B         $C$L15,HI             ; [CPU_] |118| 
        ; branchcc occurs ; [] |118| 
$C$DW$149	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$149, DW_AT_low_pc(0x00)
	.dwattr $C$DW$149, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$149, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |118| 
        ; call occurs [#_CLK_getltime] ; [] |118| 
        MOVZ      AR6,*-SP[4]           ; [CPU_] |118| 
        SUBL      ACC,*-SP[8]           ; [CPU_] |118| 
        CMPL      ACC,XAR6              ; [CPU_] |118| 
        B         $C$L13,LO             ; [CPU_] |118| 
        ; branchcc occurs ; [] |118| 
$C$L15:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 121,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |121| 
        CMPB      AL,#10                ; [CPU_] |121| 
        BF        $C$L16,NEQ            ; [CPU_] |121| 
        ; branchcc occurs ; [] |121| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 122,column 7,is_stmt
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |122| 
$C$L16:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 124,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |124| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 125,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$136, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$136, DW_AT_TI_end_line(0x7d)
	.dwattr $C$DW$136, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$136

	.sect	".text"
	.global	_DIC_WriteNetworkDict

$C$DW$151	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_WriteNetworkDict")
	.dwattr $C$DW$151, DW_AT_low_pc(_DIC_WriteNetworkDict)
	.dwattr $C$DW$151, DW_AT_high_pc(0x00)
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_DIC_WriteNetworkDict")
	.dwattr $C$DW$151, DW_AT_external
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$151, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$151, DW_AT_TI_begin_line(0x80)
	.dwattr $C$DW$151, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$151, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 128,column 71,is_stmt,address _DIC_WriteNetworkDict

	.dwfde $C$DW$CIE, _DIC_WriteNetworkDict
$C$DW$152	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeid")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_nodeid")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg0]
$C$DW$153	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg1]
$C$DW$154	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DIC_WriteNetworkDict         FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  9 Auto,  0 SOE     *
;***************************************************************

_DIC_WriteNetworkDict:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("nodeid")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_nodeid")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_breg20 -2]
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$156, DW_AT_location[DW_OP_breg20 -3]
$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$157, DW_AT_location[DW_OP_breg20 -4]
$C$DW$158	.dwtag  DW_TAG_variable, DW_AT_name("statuscan")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_statuscan")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$158, DW_AT_location[DW_OP_breg20 -5]
$C$DW$159	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$159, DW_AT_location[DW_OP_breg20 -6]
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$160, DW_AT_location[DW_OP_breg20 -7]
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$161, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[4],AR4           ; [CPU_] |128| 
        MOV       *-SP[3],AH            ; [CPU_] |128| 
        MOV       *-SP[2],AL            ; [CPU_] |128| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 129,column 18,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |129| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 130,column 10,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |130| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 131,column 13,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |131| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 132,column 15,is_stmt
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |132| 
        ; call occurs [#_CLK_getltime] ; [] |132| 
        MOVL      *-SP[10],ACC          ; [CPU_] |132| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 133,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |133| 
        MOV       AH,*-SP[4]            ; [CPU_] |133| 
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("_WriteDictEnabled")
	.dwattr $C$DW$163, DW_AT_TI_call
        LCR       #_WriteDictEnabled    ; [CPU_] |133| 
        ; call occurs [#_WriteDictEnabled] ; [] |133| 
        CMPB      AL,#0                 ; [CPU_] |133| 
        BF        $C$L23,EQ             ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 134,column 5,is_stmt
        B         $C$L22,UNC            ; [CPU_] |134| 
        ; branch occurs ; [] |134| 
$C$L17:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 135,column 7,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |135| 
        MOV       *-SP[1],AL            ; [CPU_] |135| 
        MOVZ      AR5,*-SP[4]           ; [CPU_] |135| 
        MOV       AH,*-SP[3]            ; [CPU_] |135| 
        MOV       AL,*-SP[2]            ; [CPU_] |135| 
        MOVZ      AR4,SP                ; [CPU_U] |135| 
        SUBB      XAR4,#5               ; [CPU_U] |135| 
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_name("_DIC_WriteNetworkDict2")
	.dwattr $C$DW$164, DW_AT_TI_call
        LCR       #_DIC_WriteNetworkDict2 ; [CPU_] |135| 
        ; call occurs [#_DIC_WriteNetworkDict2] ; [] |135| 
        CMPB      AL,#0                 ; [CPU_] |135| 
        BF        $C$L21,EQ             ; [CPU_] |135| 
        ; branchcc occurs ; [] |135| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 136,column 9,is_stmt
        B         $C$L19,UNC            ; [CPU_] |136| 
        ; branch occurs ; [] |136| 
$C$L18:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 137,column 11,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |137| 
        MOVB      AL,#1                 ; [CPU_] |137| 
$C$DW$165	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$165, DW_AT_low_pc(0x00)
	.dwattr $C$DW$165, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$165, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |137| 
        ; call occurs [#_SEM_pend] ; [] |137| 
$C$L19:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 136,column 16,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |136| 
        CMPB      AL,#1                 ; [CPU_] |136| 
        B         $C$L20,HI             ; [CPU_] |136| 
        ; branchcc occurs ; [] |136| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_CLK_getltime")
	.dwattr $C$DW$166, DW_AT_TI_call
        LCR       #_CLK_getltime        ; [CPU_] |136| 
        ; call occurs [#_CLK_getltime] ; [] |136| 
        MOVB      XAR6,#102             ; [CPU_] |136| 
        SUBL      ACC,*-SP[10]          ; [CPU_] |136| 
        CMPL      ACC,XAR6              ; [CPU_] |136| 
        B         $C$L18,LO             ; [CPU_] |136| 
        ; branchcc occurs ; [] |136| 
$C$L20:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 139,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |139| 
        CMPB      AL,#11                ; [CPU_] |139| 
        BF        $C$L21,NEQ            ; [CPU_] |139| 
        ; branchcc occurs ; [] |139| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 140,column 11,is_stmt
        MOVB      *-SP[7],#1,UNC        ; [CPU_] |140| 
$C$L21:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 142,column 7,is_stmt
        INC       *-SP[6]               ; [CPU_] |142| 
$C$L22:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 134,column 12,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |134| 
        CMPB      AL,#3                 ; [CPU_] |134| 
        B         $C$L23,HIS            ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
        MOV       AL,*-SP[7]            ; [CPU_] |134| 
        BF        $C$L17,EQ             ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
$C$L23:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 145,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |145| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 146,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$151, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$151, DW_AT_TI_end_line(0x92)
	.dwattr $C$DW$151, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$151

	.sect	".text"
	.global	_DIC_GlobalDictCallback

$C$DW$168	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_GlobalDictCallback")
	.dwattr $C$DW$168, DW_AT_low_pc(_DIC_GlobalDictCallback)
	.dwattr $C$DW$168, DW_AT_high_pc(0x00)
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_DIC_GlobalDictCallback")
	.dwattr $C$DW$168, DW_AT_external
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$168, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$168, DW_AT_TI_begin_line(0x95)
	.dwattr $C$DW$168, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$168, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 149,column 102,is_stmt,address _DIC_GlobalDictCallback

	.dwfde $C$DW$CIE, _DIC_GlobalDictCallback
$C$DW$169	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg12]
$C$DW$170	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg14]
$C$DW$171	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg0]
$C$DW$172	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DIC_GlobalDictCallback       FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  9 Auto,  0 SOE     *
;***************************************************************

_DIC_GlobalDictCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_breg20 -2]
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_breg20 -4]
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -5]
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -6]
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("error")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_error")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_breg20 -8]
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("line")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_line")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_breg20 -9]
        MOV       *-SP[6],AH            ; [CPU_] |149| 
        MOV       *-SP[5],AL            ; [CPU_] |149| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |149| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |149| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 150,column 15,is_stmt
        MOVB      ACC,#0                ; [CPU_] |150| 
        MOVL      *-SP[8],ACC           ; [CPU_] |150| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 152,column 3,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |152| 
        MOV       AH,*-SP[5]            ; [CPU_] |152| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |152| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |152| 
        MOVZ      AR5,SP                ; [CPU_U] |152| 
        SUBB      XAR5,#9               ; [CPU_U] |152| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_getSDOlineOnUseFromIndex")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #_getSDOlineOnUseFromIndex ; [CPU_] |152| 
        ; call occurs [#_getSDOlineOnUseFromIndex] ; [] |152| 
        CMPB      AL,#0                 ; [CPU_] |152| 
        BF        $C$L26,NEQ            ; [CPU_] |152| 
        ; branchcc occurs ; [] |152| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 154,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |154| 
        BF        $C$L24,NEQ            ; [CPU_] |154| 
        ; branchcc occurs ; [] |154| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 155,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |155| 
        MOV       AH,*+XAR4[3]          ; [CPU_] |155| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |155| 
        MOVW      DP,#_DIC_NodeId       ; [CPU_U] 
        MOV       AL,@_DIC_NodeId       ; [CPU_] |155| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("_DIC_ReadNetworkDict")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #_DIC_ReadNetworkDict ; [CPU_] |155| 
        ; call occurs [#_DIC_ReadNetworkDict] ; [] |155| 
        CMPB      AL,#0                 ; [CPU_] |155| 
        BF        $C$L27,NEQ            ; [CPU_] |155| 
        ; branchcc occurs ; [] |155| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 156,column 9,is_stmt
        MOV       AL,#33                ; [CPU_] |156| 
        MOV       AH,#2048              ; [CPU_] |156| 
        MOVL      *-SP[8],ACC           ; [CPU_] |156| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 157,column 5,is_stmt
        B         $C$L27,UNC            ; [CPU_] |157| 
        ; branch occurs ; [] |157| 
$C$L24:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 158,column 10,is_stmt
        CMPB      AL,#1                 ; [CPU_] |158| 
        BF        $C$L25,NEQ            ; [CPU_] |158| 
        ; branchcc occurs ; [] |158| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 159,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |159| 
        MOV       AH,*+XAR4[3]          ; [CPU_] |159| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |159| 
        MOVW      DP,#_DIC_NodeId       ; [CPU_U] 
        MOV       AL,@_DIC_NodeId       ; [CPU_] |159| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_DIC_WriteNetworkDict")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_DIC_WriteNetworkDict ; [CPU_] |159| 
        ; call occurs [#_DIC_WriteNetworkDict] ; [] |159| 
        CMPB      AL,#0                 ; [CPU_] |159| 
        BF        $C$L27,NEQ            ; [CPU_] |159| 
        ; branchcc occurs ; [] |159| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 160,column 9,is_stmt
        MOV       AL,#33                ; [CPU_] |160| 
        MOV       AH,#2048              ; [CPU_] |160| 
        MOVL      *-SP[8],ACC           ; [CPU_] |160| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 161,column 5,is_stmt
        B         $C$L27,UNC            ; [CPU_] |161| 
        ; branch occurs ; [] |161| 
$C$L25:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 164,column 7,is_stmt
        MOV       AL,#33                ; [CPU_] |164| 
        MOV       AH,#2048              ; [CPU_] |164| 
        MOVL      *-SP[8],ACC           ; [CPU_] |164| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 166,column 3,is_stmt
        B         $C$L27,UNC            ; [CPU_] |166| 
        ; branch occurs ; [] |166| 
$C$L26:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 169,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |169| 
        MOVL      *-SP[8],ACC           ; [CPU_] |169| 
$C$L27:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 171,column 3,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |171| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 172,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$168, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$168, DW_AT_TI_end_line(0xac)
	.dwattr $C$DW$168, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$168

	.sect	".text"
	.global	_DIC_ReadNetworkDict2

$C$DW$183	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_ReadNetworkDict2")
	.dwattr $C$DW$183, DW_AT_low_pc(_DIC_ReadNetworkDict2)
	.dwattr $C$DW$183, DW_AT_high_pc(0x00)
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_DIC_ReadNetworkDict2")
	.dwattr $C$DW$183, DW_AT_external
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$183, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$183, DW_AT_TI_begin_line(0xc1)
	.dwattr $C$DW$183, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$183, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 193,column 107,is_stmt,address _DIC_ReadNetworkDict2

	.dwfde $C$DW$CIE, _DIC_ReadNetworkDict2
$C$DW$184	.dwtag  DW_TAG_formal_parameter, DW_AT_name("node_id")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_reg0]
$C$DW$185	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_reg1]
$C$DW$186	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bLocalSubindex")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_bLocalSubindex")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_reg14]
$C$DW$187	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$187, DW_AT_location[DW_OP_breg20 -27]
$C$DW$188	.dwtag  DW_TAG_formal_parameter, DW_AT_name("status")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$188, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DIC_ReadNetworkDict2         FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter, 17 Auto,  0 SOE     *
;***************************************************************

_DIC_ReadNetworkDict2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$189	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$189, DW_AT_location[DW_OP_breg20 -8]
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_breg20 -9]
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("bLocalSubindex")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_bLocalSubindex")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_breg20 -10]
$C$DW$192	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_breg20 -12]
$C$DW$193	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_breg20 -24]
        MOV       *-SP[10],AR5          ; [CPU_] |193| 
        MOV       *-SP[9],AH            ; [CPU_] |193| 
        MOVL      *-SP[12],XAR4         ; [CPU_] |193| 
        MOV       *-SP[8],AL            ; [CPU_] |193| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 196,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |196| 
        MOVL      *-SP[22],ACC          ; [CPU_] |196| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 198,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |198| 
        MOVZ      AR4,SP                ; [CPU_U] |198| 
        SUBB      XAR5,#22              ; [CPU_U] |198| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        SUBB      XAR4,#18              ; [CPU_U] |198| 
        MOVU      ACC,AR5               ; [CPU_] |198| 
        MOVZ      AR5,SP                ; [CPU_U] |198| 
        MOVL      *-SP[2],ACC           ; [CPU_] |198| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |198| 
        MOV       *-SP[5],#0            ; [CPU_] |198| 
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |198| 
        MOVB      *-SP[7],#4,UNC        ; [CPU_] |198| 
        MOV       AL,*-SP[9]            ; [CPU_] |198| 
        MOV       AH,*-SP[10]           ; [CPU_] |198| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |198| 
        SUBB      XAR5,#24              ; [CPU_U] |198| 
$C$DW$194	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$194, DW_AT_low_pc(0x00)
	.dwattr $C$DW$194, DW_AT_name("__getODentry")
	.dwattr $C$DW$194, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |198| 
        ; call occurs [#__getODentry] ; [] |198| 
        TEST      ACC                   ; [CPU_] |198| 
        BF        $C$L28,NEQ            ; [CPU_] |198| 
        ; branchcc occurs ; [] |198| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 200,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |200| 
        MOV       *-SP[17],AL           ; [CPU_] |200| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 201,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |201| 
        MOV       *-SP[16],AL           ; [CPU_] |201| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 202,column 5,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |202| 
        MOV       *-SP[15],AL           ; [CPU_] |202| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 203,column 5,is_stmt
        MOV       *-SP[14],#0           ; [CPU_] |203| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 204,column 5,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |204| 
        MOVL      *-SP[20],ACC          ; [CPU_] |204| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 205,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |205| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |205| 
        MOVB      AL,#0                 ; [CPU_] |205| 
        SUBB      XAR5,#24              ; [CPU_U] |205| 
$C$DW$195	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$195, DW_AT_low_pc(0x00)
	.dwattr $C$DW$195, DW_AT_name("_MBX_post")
	.dwattr $C$DW$195, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |205| 
        ; call occurs [#_MBX_post] ; [] |205| 
        B         $C$L29,UNC            ; [CPU_] |205| 
        ; branch occurs ; [] |205| 
$C$L28:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 208,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |208| 
$C$L29:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 209,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$183, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$183, DW_AT_TI_end_line(0xd1)
	.dwattr $C$DW$183, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$183

	.sect	".text"
	.global	_DIC_ReadNetworkDictDirect

$C$DW$197	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_ReadNetworkDictDirect")
	.dwattr $C$DW$197, DW_AT_low_pc(_DIC_ReadNetworkDictDirect)
	.dwattr $C$DW$197, DW_AT_high_pc(0x00)
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_DIC_ReadNetworkDictDirect")
	.dwattr $C$DW$197, DW_AT_external
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$197, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$197, DW_AT_TI_begin_line(0xd3)
	.dwattr $C$DW$197, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$197, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 211,column 130,is_stmt,address _DIC_ReadNetworkDictDirect

	.dwfde $C$DW$CIE, _DIC_ReadNetworkDictDirect
$C$DW$198	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdata")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg12]
$C$DW$199	.dwtag  DW_TAG_formal_parameter, DW_AT_name("size")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg0]
$C$DW$200	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataType")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_breg20 -21]
$C$DW$201	.dwtag  DW_TAG_formal_parameter, DW_AT_name("node_id")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_breg20 -22]
$C$DW$202	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_breg20 -23]
$C$DW$203	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_breg20 -24]
$C$DW$204	.dwtag  DW_TAG_formal_parameter, DW_AT_name("status")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DIC_ReadNetworkDictDirect    FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_DIC_ReadNetworkDictDirect:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$205	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_breg20 -2]
$C$DW$206	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_breg20 -4]
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_breg20 -6]
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -18]
        MOVL      *-SP[6],XAR5          ; [CPU_] |211| 
        MOVL      *-SP[4],ACC           ; [CPU_] |211| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |211| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 214,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |214| 
        MOVL      *-SP[18],ACC          ; [CPU_] |214| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 215,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |215| 
        MOVL      *-SP[16],ACC          ; [CPU_] |215| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 216,column 3,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |216| 
        MOV       *-SP[12],AL           ; [CPU_] |216| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 217,column 3,is_stmt
        MOV       AL,*-SP[22]           ; [CPU_] |217| 
        MOV       *-SP[11],AL           ; [CPU_] |217| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 218,column 3,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |218| 
        MOV       *-SP[10],AL           ; [CPU_] |218| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 219,column 3,is_stmt
        MOV       AL,*-SP[24]           ; [CPU_] |219| 
        MOV       *-SP[9],AL            ; [CPU_] |219| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 220,column 3,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |220| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 221,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |221| 
        MOVL      *-SP[14],ACC          ; [CPU_] |221| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 222,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |222| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |222| 
        MOVB      AL,#0                 ; [CPU_] |222| 
        SUBB      XAR5,#18              ; [CPU_U] |222| 
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("_MBX_post")
	.dwattr $C$DW$209, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |222| 
        ; call occurs [#_MBX_post] ; [] |222| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 223,column 1,is_stmt
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$197, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$197, DW_AT_TI_end_line(0xdf)
	.dwattr $C$DW$197, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$197

	.sect	".text"
	.global	_DIC_ReadNetworkDictDirect2

$C$DW$211	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_ReadNetworkDictDirect2")
	.dwattr $C$DW$211, DW_AT_low_pc(_DIC_ReadNetworkDictDirect2)
	.dwattr $C$DW$211, DW_AT_high_pc(0x00)
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_DIC_ReadNetworkDictDirect2")
	.dwattr $C$DW$211, DW_AT_external
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$211, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$211, DW_AT_TI_begin_line(0xe1)
	.dwattr $C$DW$211, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$211, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 225,column 132,is_stmt,address _DIC_ReadNetworkDictDirect2

	.dwfde $C$DW$CIE, _DIC_ReadNetworkDictDirect2
$C$DW$212	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdata")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg12]
$C$DW$213	.dwtag  DW_TAG_formal_parameter, DW_AT_name("size")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg0]
$C$DW$214	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataType")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_breg20 -21]
$C$DW$215	.dwtag  DW_TAG_formal_parameter, DW_AT_name("node_id")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_breg20 -22]
$C$DW$216	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_breg20 -23]
$C$DW$217	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -24]
$C$DW$218	.dwtag  DW_TAG_formal_parameter, DW_AT_name("status")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DIC_ReadNetworkDictDirect2   FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_DIC_ReadNetworkDictDirect2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$219	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$219, DW_AT_location[DW_OP_breg20 -2]
$C$DW$220	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_breg20 -4]
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_breg20 -6]
$C$DW$222	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_breg20 -18]
        MOVL      *-SP[6],XAR5          ; [CPU_] |225| 
        MOVL      *-SP[4],ACC           ; [CPU_] |225| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |225| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 228,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |228| 
        MOVL      *-SP[18],ACC          ; [CPU_] |228| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 229,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |229| 
        MOVL      *-SP[16],ACC          ; [CPU_] |229| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 230,column 3,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |230| 
        MOV       *-SP[12],AL           ; [CPU_] |230| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 231,column 3,is_stmt
        MOV       AL,*-SP[22]           ; [CPU_] |231| 
        MOV       *-SP[11],AL           ; [CPU_] |231| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 232,column 3,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |232| 
        MOV       *-SP[10],AL           ; [CPU_] |232| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 233,column 3,is_stmt
        MOV       AL,*-SP[24]           ; [CPU_] |233| 
        MOV       *-SP[9],AL            ; [CPU_] |233| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 234,column 3,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |234| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 235,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |235| 
        MOVL      *-SP[14],ACC          ; [CPU_] |235| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 236,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |236| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |236| 
        MOVB      AL,#0                 ; [CPU_] |236| 
        SUBB      XAR5,#18              ; [CPU_U] |236| 
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("_MBX_post")
	.dwattr $C$DW$223, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |236| 
        ; call occurs [#_MBX_post] ; [] |236| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 237,column 1,is_stmt
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$211, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$211, DW_AT_TI_end_line(0xed)
	.dwattr $C$DW$211, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$211

	.sect	".text"
	.global	_AfterReadDict_Callback

$C$DW$225	.dwtag  DW_TAG_subprogram, DW_AT_name("AfterReadDict_Callback")
	.dwattr $C$DW$225, DW_AT_low_pc(_AfterReadDict_Callback)
	.dwattr $C$DW$225, DW_AT_high_pc(0x00)
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_AfterReadDict_Callback")
	.dwattr $C$DW$225, DW_AT_external
	.dwattr $C$DW$225, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$225, DW_AT_TI_begin_line(0xef)
	.dwattr $C$DW$225, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$225, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 239,column 53,is_stmt,address _AfterReadDict_Callback

	.dwfde $C$DW$CIE, _AfterReadDict_Callback
$C$DW$226	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_reg12]
$C$DW$227	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeId")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AfterReadDict_Callback       FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_AfterReadDict_Callback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$228	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_breg20 -6]
$C$DW$229	.dwtag  DW_TAG_variable, DW_AT_name("nodeId")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$229, DW_AT_location[DW_OP_breg20 -7]
$C$DW$230	.dwtag  DW_TAG_variable, DW_AT_name("abortCode")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$230, DW_AT_location[DW_OP_breg20 -10]
$C$DW$231	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$231, DW_AT_location[DW_OP_breg20 -12]
        MOV       *-SP[7],AL            ; [CPU_] |239| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |239| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 241,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |241| 
        MOVL      *-SP[12],ACC          ; [CPU_] |241| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 243,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |243| 
        MOVZ      AR4,SP                ; [CPU_U] |243| 
        SUBB      XAR5,#12              ; [CPU_U] |243| 
        SUBB      XAR4,#10              ; [CPU_U] |243| 
        MOVL      *-SP[2],XAR5          ; [CPU_] |243| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |243| 
        MOV       AL,*-SP[7]            ; [CPU_] |243| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |243| 
        MOVW      DP,#_Read_pdata       ; [CPU_U] 
        MOVL      XAR5,@_Read_pdata     ; [CPU_] |243| 
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_name("_getReadResultNetworkDict")
	.dwattr $C$DW$232, DW_AT_TI_call
        LCR       #_getReadResultNetworkDict ; [CPU_] |243| 
        ; call occurs [#_getReadResultNetworkDict] ; [] |243| 
        CMPB      AL,#1                 ; [CPU_] |243| 
        BF        $C$L30,NEQ            ; [CPU_] |243| 
        ; branchcc occurs ; [] |243| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 244,column 5,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |244| 
        BF        $C$L30,NEQ            ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 244,column 37,is_stmt
        MOVW      DP,#_SDO_Successful   ; [CPU_U] 
        MOVB      @_SDO_Successful,#1,UNC ; [CPU_] |244| 
$C$L30:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 246,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |246| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |246| 
        MOVB      AH,#2                 ; [CPU_] |246| 
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_name("_closeSDOtransfer")
	.dwattr $C$DW$233, DW_AT_TI_call
        LCR       #_closeSDOtransfer    ; [CPU_] |246| 
        ; call occurs [#_closeSDOtransfer] ; [] |246| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 247,column 3,is_stmt
        MOVL      XAR4,#_semaphoreSDOdone ; [CPU_U] |247| 
$C$DW$234	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$234, DW_AT_low_pc(0x00)
	.dwattr $C$DW$234, DW_AT_name("_SEM_post")
	.dwattr $C$DW$234, DW_AT_TI_call
        LCR       #_SEM_post            ; [CPU_] |247| 
        ; call occurs [#_SEM_post] ; [] |247| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 248,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$235	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$235, DW_AT_low_pc(0x00)
	.dwattr $C$DW$235, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$225, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$225, DW_AT_TI_end_line(0xf8)
	.dwattr $C$DW$225, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$225

	.sect	".text"
	.global	_DIC_WriteNetworkDict2

$C$DW$236	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_WriteNetworkDict2")
	.dwattr $C$DW$236, DW_AT_low_pc(_DIC_WriteNetworkDict2)
	.dwattr $C$DW$236, DW_AT_high_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_DIC_WriteNetworkDict2")
	.dwattr $C$DW$236, DW_AT_external
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$236, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$236, DW_AT_TI_begin_line(0xfc)
	.dwattr $C$DW$236, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$236, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 252,column 108,is_stmt,address _DIC_WriteNetworkDict2

	.dwfde $C$DW$CIE, _DIC_WriteNetworkDict2
$C$DW$237	.dwtag  DW_TAG_formal_parameter, DW_AT_name("node_id")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_reg0]
$C$DW$238	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_reg1]
$C$DW$239	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bLocalSubindex")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_bLocalSubindex")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_reg14]
$C$DW$240	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_breg20 -27]
$C$DW$241	.dwtag  DW_TAG_formal_parameter, DW_AT_name("status")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DIC_WriteNetworkDict2        FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter, 17 Auto,  0 SOE     *
;***************************************************************

_DIC_WriteNetworkDict2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$242	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_breg20 -8]
$C$DW$243	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_breg20 -9]
$C$DW$244	.dwtag  DW_TAG_variable, DW_AT_name("bLocalSubindex")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_bLocalSubindex")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_breg20 -10]
$C$DW$245	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_breg20 -12]
$C$DW$246	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -24]
        MOV       *-SP[10],AR5          ; [CPU_] |252| 
        MOV       *-SP[9],AH            ; [CPU_] |252| 
        MOVL      *-SP[12],XAR4         ; [CPU_] |252| 
        MOV       *-SP[8],AL            ; [CPU_] |252| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 255,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |255| 
        MOVL      *-SP[22],ACC          ; [CPU_] |255| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 257,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |257| 
        MOVZ      AR4,SP                ; [CPU_U] |257| 
        SUBB      XAR5,#22              ; [CPU_U] |257| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        SUBB      XAR4,#18              ; [CPU_U] |257| 
        MOVU      ACC,AR5               ; [CPU_] |257| 
        MOVZ      AR5,SP                ; [CPU_U] |257| 
        MOVL      *-SP[2],ACC           ; [CPU_] |257| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |257| 
        MOV       *-SP[5],#0            ; [CPU_] |257| 
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |257| 
        MOVB      *-SP[7],#4,UNC        ; [CPU_] |257| 
        MOV       AL,*-SP[9]            ; [CPU_] |257| 
        MOV       AH,*-SP[10]           ; [CPU_] |257| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |257| 
        SUBB      XAR5,#24              ; [CPU_U] |257| 
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("__getODentry")
	.dwattr $C$DW$247, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |257| 
        ; call occurs [#__getODentry] ; [] |257| 
        TEST      ACC                   ; [CPU_] |257| 
        BF        $C$L31,NEQ            ; [CPU_] |257| 
        ; branchcc occurs ; [] |257| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 259,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |259| 
        MOV       *-SP[17],AL           ; [CPU_] |259| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 260,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |260| 
        MOV       *-SP[16],AL           ; [CPU_] |260| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 261,column 5,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |261| 
        MOV       *-SP[15],AL           ; [CPU_] |261| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 262,column 5,is_stmt
        MOVB      *-SP[14],#1,UNC       ; [CPU_] |262| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 263,column 5,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |263| 
        MOVL      *-SP[20],ACC          ; [CPU_] |263| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 264,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |264| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |264| 
        MOVB      AL,#0                 ; [CPU_] |264| 
        SUBB      XAR5,#24              ; [CPU_U] |264| 
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_name("_MBX_post")
	.dwattr $C$DW$248, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |264| 
        ; call occurs [#_MBX_post] ; [] |264| 
        B         $C$L32,UNC            ; [CPU_] |264| 
        ; branch occurs ; [] |264| 
$C$L31:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 267,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |267| 
$C$L32:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 268,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$236, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$236, DW_AT_TI_end_line(0x10c)
	.dwattr $C$DW$236, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$236

	.sect	".text"
	.global	_DIC_WriteNetworkDictDirect

$C$DW$250	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_WriteNetworkDictDirect")
	.dwattr $C$DW$250, DW_AT_low_pc(_DIC_WriteNetworkDictDirect)
	.dwattr $C$DW$250, DW_AT_high_pc(0x00)
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_DIC_WriteNetworkDictDirect")
	.dwattr $C$DW$250, DW_AT_external
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$250, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$250, DW_AT_TI_begin_line(0x10e)
	.dwattr $C$DW$250, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$250, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 270,column 132,is_stmt,address _DIC_WriteNetworkDictDirect

	.dwfde $C$DW$CIE, _DIC_WriteNetworkDictDirect
$C$DW$251	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdata")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_reg12]
$C$DW$252	.dwtag  DW_TAG_formal_parameter, DW_AT_name("size")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_reg0]
$C$DW$253	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataType")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_breg20 -21]
$C$DW$254	.dwtag  DW_TAG_formal_parameter, DW_AT_name("node_id")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_breg20 -22]
$C$DW$255	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_breg20 -23]
$C$DW$256	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$256, DW_AT_location[DW_OP_breg20 -24]
$C$DW$257	.dwtag  DW_TAG_formal_parameter, DW_AT_name("status")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$257, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DIC_WriteNetworkDictDirect   FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_DIC_WriteNetworkDictDirect:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$258	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_breg20 -2]
$C$DW$259	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_breg20 -4]
$C$DW$260	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_breg20 -6]
$C$DW$261	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_breg20 -18]
        MOVL      *-SP[6],XAR5          ; [CPU_] |270| 
        MOVL      *-SP[4],ACC           ; [CPU_] |270| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |270| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 273,column 3,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |273| 
        MOVL      *-SP[18],ACC          ; [CPU_] |273| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 274,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |274| 
        MOVL      *-SP[16],ACC          ; [CPU_] |274| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 275,column 3,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |275| 
        MOV       *-SP[12],AL           ; [CPU_] |275| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 276,column 3,is_stmt
        MOV       AL,*-SP[22]           ; [CPU_] |276| 
        MOV       *-SP[11],AL           ; [CPU_] |276| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 277,column 3,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |277| 
        MOV       *-SP[10],AL           ; [CPU_] |277| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 278,column 3,is_stmt
        MOV       AL,*-SP[24]           ; [CPU_] |278| 
        MOV       *-SP[9],AL            ; [CPU_] |278| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 279,column 3,is_stmt
        MOVB      *-SP[8],#1,UNC        ; [CPU_] |279| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 280,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |280| 
        MOVL      *-SP[14],ACC          ; [CPU_] |280| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 281,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |281| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |281| 
        MOVB      AL,#0                 ; [CPU_] |281| 
        SUBB      XAR5,#18              ; [CPU_U] |281| 
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_name("_MBX_post")
	.dwattr $C$DW$262, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |281| 
        ; call occurs [#_MBX_post] ; [] |281| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 282,column 1,is_stmt
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$263	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$263, DW_AT_low_pc(0x00)
	.dwattr $C$DW$263, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$250, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$250, DW_AT_TI_end_line(0x11a)
	.dwattr $C$DW$250, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$250

	.sect	".text"
	.global	_AfterWriteDict_Callback

$C$DW$264	.dwtag  DW_TAG_subprogram, DW_AT_name("AfterWriteDict_Callback")
	.dwattr $C$DW$264, DW_AT_low_pc(_AfterWriteDict_Callback)
	.dwattr $C$DW$264, DW_AT_high_pc(0x00)
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_AfterWriteDict_Callback")
	.dwattr $C$DW$264, DW_AT_external
	.dwattr $C$DW$264, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$264, DW_AT_TI_begin_line(0x11c)
	.dwattr $C$DW$264, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$264, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 284,column 54,is_stmt,address _AfterWriteDict_Callback

	.dwfde $C$DW$CIE, _AfterWriteDict_Callback
$C$DW$265	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_reg12]
$C$DW$266	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeId")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$266, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _AfterWriteDict_Callback      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_AfterWriteDict_Callback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$267	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$267, DW_AT_location[DW_OP_breg20 -2]
$C$DW$268	.dwtag  DW_TAG_variable, DW_AT_name("nodeId")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$268, DW_AT_location[DW_OP_breg20 -3]
$C$DW$269	.dwtag  DW_TAG_variable, DW_AT_name("abortCode")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[3],AL            ; [CPU_] |284| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |284| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 287,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |287| 
        MOVZ      AR5,SP                ; [CPU_U] |287| 
        SUBB      XAR5,#6               ; [CPU_U] |287| 
$C$DW$270	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$270, DW_AT_low_pc(0x00)
	.dwattr $C$DW$270, DW_AT_name("_getWriteResultNetworkDict")
	.dwattr $C$DW$270, DW_AT_TI_call
        LCR       #_getWriteResultNetworkDict ; [CPU_] |287| 
        ; call occurs [#_getWriteResultNetworkDict] ; [] |287| 
        CMPB      AL,#1                 ; [CPU_] |287| 
        BF        $C$L33,NEQ            ; [CPU_] |287| 
        ; branchcc occurs ; [] |287| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 288,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |288| 
        BF        $C$L33,NEQ            ; [CPU_] |288| 
        ; branchcc occurs ; [] |288| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 288,column 37,is_stmt
        MOVW      DP,#_SDO_Successful   ; [CPU_U] 
        MOVB      @_SDO_Successful,#1,UNC ; [CPU_] |288| 
$C$L33:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 290,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |290| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |290| 
        MOVB      AH,#2                 ; [CPU_] |290| 
$C$DW$271	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$271, DW_AT_low_pc(0x00)
	.dwattr $C$DW$271, DW_AT_name("_closeSDOtransfer")
	.dwattr $C$DW$271, DW_AT_TI_call
        LCR       #_closeSDOtransfer    ; [CPU_] |290| 
        ; call occurs [#_closeSDOtransfer] ; [] |290| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 291,column 3,is_stmt
        MOVL      XAR4,#_semaphoreSDOdone ; [CPU_U] |291| 
$C$DW$272	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$272, DW_AT_low_pc(0x00)
	.dwattr $C$DW$272, DW_AT_name("_SEM_post")
	.dwattr $C$DW$272, DW_AT_TI_call
        LCR       #_SEM_post            ; [CPU_] |291| 
        ; call occurs [#_SEM_post] ; [] |291| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 292,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$264, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$264, DW_AT_TI_end_line(0x124)
	.dwattr $C$DW$264, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$264

	.sect	".text"
	.global	_StartSDOTransaction

$C$DW$274	.dwtag  DW_TAG_subprogram, DW_AT_name("StartSDOTransaction")
	.dwattr $C$DW$274, DW_AT_low_pc(_StartSDOTransaction)
	.dwattr $C$DW$274, DW_AT_high_pc(0x00)
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_StartSDOTransaction")
	.dwattr $C$DW$274, DW_AT_external
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$274, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$274, DW_AT_TI_begin_line(0x12c)
	.dwattr $C$DW$274, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$274, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 300,column 45,is_stmt,address _StartSDOTransaction

	.dwfde $C$DW$CIE, _StartSDOTransaction
$C$DW$275	.dwtag  DW_TAG_formal_parameter, DW_AT_name("msg")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$275, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _StartSDOTransaction          FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_StartSDOTransaction:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$276	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$276, DW_AT_location[DW_OP_breg20 -8]
$C$DW$277	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$277, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[8],XAR4          ; [CPU_] |300| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 306,column 3,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |306| 
        MOVB      XAR0,#10              ; [CPU_] |306| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |306| 
        BF        $C$L34,NEQ            ; [CPU_] |306| 
        ; branchcc occurs ; [] |306| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 308,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |308| 
        MOVW      DP,#_Read_pdata       ; [CPU_U] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |308| 
        MOVL      @_Read_pdata,ACC      ; [CPU_] |308| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 309,column 5,is_stmt
        MOVL      XAR7,*-SP[8]          ; [CPU_] |309| 
        ADDB      XAR7,#9               ; [CPU_] |309| 
        MOV       AL,*XAR7              ; [CPU_] |309| 
        MOV       *-SP[1],AL            ; [CPU_] |309| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |309| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |309| 
        MOV       *-SP[2],AL            ; [CPU_] |309| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |309| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |309| 
        MOVL      *-SP[4],ACC           ; [CPU_] |309| 
        MOVL      XAR5,*-SP[8]          ; [CPU_] |309| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |309| 
        MOVB      XAR0,#8               ; [CPU_] |309| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOV       AL,*+XAR5[7]          ; [CPU_] |309| 
        MOV       AH,*+XAR4[AR0]        ; [CPU_] |309| 
        MOVL      XAR5,#_AfterReadDict_Callback ; [CPU_U] |309| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |309| 
$C$DW$278	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$278, DW_AT_low_pc(0x00)
	.dwattr $C$DW$278, DW_AT_name("_readNetworkDictCallback")
	.dwattr $C$DW$278, DW_AT_TI_call
        LCR       #_readNetworkDictCallback ; [CPU_] |309| 
        ; call occurs [#_readNetworkDictCallback] ; [] |309| 
        MOVU      ACC,AL                ; [CPU_] |309| 
        MOVL      *-SP[10],ACC          ; [CPU_] |309| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 311,column 3,is_stmt
        B         $C$L35,UNC            ; [CPU_] |311| 
        ; branch occurs ; [] |311| 
$C$L34:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 313,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |313| 
        MOV       AL,*+XAR4[7]          ; [CPU_] |313| 
        MOV       *-SP[1],AL            ; [CPU_] |313| 
        MOVL      XAR7,*-SP[8]          ; [CPU_] |313| 
        ADDB      XAR7,#8               ; [CPU_] |313| 
        MOV       AL,*XAR7              ; [CPU_] |313| 
        MOV       *-SP[2],AL            ; [CPU_] |313| 
        MOVL      XAR7,*-SP[8]          ; [CPU_] |313| 
        ADDB      XAR7,#9               ; [CPU_] |313| 
        MOV       AL,*XAR7              ; [CPU_] |313| 
        MOV       *-SP[3],AL            ; [CPU_] |313| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |313| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |313| 
        MOVL      XAR4,#_AfterWriteDict_Callback ; [CPU_U] |313| 
        MOV       *-SP[4],AL            ; [CPU_] |313| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |313| 
        MOVL      XAR5,*-SP[8]          ; [CPU_] |313| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |313| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      ACC,*+XAR5[2]         ; [CPU_] |313| 
        MOVL      XAR5,*+XAR4[0]        ; [CPU_] |313| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |313| 
$C$DW$279	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$279, DW_AT_low_pc(0x00)
	.dwattr $C$DW$279, DW_AT_name("_writeNetworkDictCallBack")
	.dwattr $C$DW$279, DW_AT_TI_call
        LCR       #_writeNetworkDictCallBack ; [CPU_] |313| 
        ; call occurs [#_writeNetworkDictCallBack] ; [] |313| 
        MOVU      ACC,AL                ; [CPU_] |313| 
        MOVL      *-SP[10],ACC          ; [CPU_] |313| 
$C$L35:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 319,column 3,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |319| 
        MOVB      XAR6,#0               ; [CPU_] |319| 
        BF        $C$L36,NEQ            ; [CPU_] |319| 
        ; branchcc occurs ; [] |319| 
        MOVB      XAR6,#1               ; [CPU_] |319| 
$C$L36:    
        MOV       AL,AR6                ; [CPU_] |319| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 320,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$274, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$274, DW_AT_TI_end_line(0x140)
	.dwattr $C$DW$274, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$274

	.sect	".text"
	.global	_taskFnMailboxSDOout

$C$DW$281	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnMailboxSDOout")
	.dwattr $C$DW$281, DW_AT_low_pc(_taskFnMailboxSDOout)
	.dwattr $C$DW$281, DW_AT_high_pc(0x00)
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_taskFnMailboxSDOout")
	.dwattr $C$DW$281, DW_AT_external
	.dwattr $C$DW$281, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$281, DW_AT_TI_begin_line(0x145)
	.dwattr $C$DW$281, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$281, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 325,column 31,is_stmt,address _taskFnMailboxSDOout

	.dwfde $C$DW$CIE, _taskFnMailboxSDOout

;***************************************************************
;* FNAME: _taskFnMailboxSDOout          FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 13 Auto,  0 SOE     *
;***************************************************************

_taskFnMailboxSDOout:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$282	.dwtag  DW_TAG_variable, DW_AT_name("msg")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_msg")
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$282, DW_AT_location[DW_OP_breg20 -12]
$C$DW$283	.dwtag  DW_TAG_variable, DW_AT_name("trials")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_trials")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_breg20 -13]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 329,column 9,is_stmt
$C$L37:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 330,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |330| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |330| 
        MOV       AL,#65535             ; [CPU_] |330| 
        SUBB      XAR5,#12              ; [CPU_U] |330| 
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$284, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |330| 
        ; call occurs [#_MBX_pend] ; [] |330| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 333,column 5,is_stmt
        MOVW      DP,#_SDO_Successful   ; [CPU_U] 
        MOV       @_SDO_Successful,#0   ; [CPU_] |333| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 333,column 29,is_stmt
        MOV       *-SP[13],#0           ; [CPU_] |333| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 334,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |334| 
        MOVB      *+XAR4[0],#1,UNC      ; [CPU_] |334| 
$C$L38:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 336,column 7,is_stmt
        MOVL      XAR4,#_semaphoreSDOdone ; [CPU_U] |336| 
        MOVB      AL,#0                 ; [CPU_] |336| 
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$285, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |336| 
        ; call occurs [#_SEM_pend] ; [] |336| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 337,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |337| 
        SUBB      XAR4,#12              ; [CPU_U] |337| 
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_StartSDOTransaction")
	.dwattr $C$DW$286, DW_AT_TI_call
        LCR       #_StartSDOTransaction ; [CPU_] |337| 
        ; call occurs [#_StartSDOTransaction] ; [] |337| 
        CMPB      AL,#0                 ; [CPU_] |337| 
        BF        $C$L39,EQ             ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 339,column 9,is_stmt
        MOVL      XAR4,#_semaphoreSDOdone ; [CPU_U] |339| 
        MOVB      AL,#102               ; [CPU_] |339| 
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$287, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |339| 
        ; call occurs [#_SEM_pend] ; [] |339| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 340,column 7,is_stmt
        B         $C$L40,UNC            ; [CPU_] |340| 
        ; branch occurs ; [] |340| 
$C$L39:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 342,column 9,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |342| 
        MOVB      AL,#10                ; [CPU_] |342| 
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$288, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |342| 
        ; call occurs [#_SEM_pend] ; [] |342| 
$C$L40:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 345,column 13,is_stmt
        MOVW      DP,#_SDO_Successful   ; [CPU_U] 
        MOV       AL,@_SDO_Successful   ; [CPU_] |345| 
        BF        $C$L41,NEQ            ; [CPU_] |345| 
        ; branchcc occurs ; [] |345| 
        INC       *-SP[13]              ; [CPU_] |345| 
        BF        $C$L38,EQ             ; [CPU_] |345| 
        ; branchcc occurs ; [] |345| 
$C$L41:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 346,column 5,is_stmt
        MOV       AL,@_SDO_Successful   ; [CPU_] |346| 
        BF        $C$L43,EQ             ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 347,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |347| 
        BF        $C$L42,NEQ            ; [CPU_] |347| 
        ; branchcc occurs ; [] |347| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 347,column 35,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |347| 
        MOVB      *+XAR4[0],#10,UNC     ; [CPU_] |347| 
        B         $C$L37,UNC            ; [CPU_] |347| 
        ; branch occurs ; [] |347| 
$C$L42:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 348,column 12,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |348| 
        MOVB      *+XAR4[0],#11,UNC     ; [CPU_] |348| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 349,column 5,is_stmt
        B         $C$L37,UNC            ; [CPU_] |349| 
        ; branch occurs ; [] |349| 
$C$L43:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 351,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |351| 
        BF        $C$L44,NEQ            ; [CPU_] |351| 
        ; branchcc occurs ; [] |351| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 351,column 35,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |351| 
        MOVB      *+XAR4[0],#20,UNC     ; [CPU_] |351| 
        B         $C$L37,UNC            ; [CPU_] |351| 
        ; branch occurs ; [] |351| 
$C$L44:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 352,column 12,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |352| 
        MOVB      *+XAR4[0],#21,UNC     ; [CPU_] |352| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c",line 329,column 9,is_stmt
        B         $C$L37,UNC            ; [CPU_] |329| 
        ; branch occurs ; [] |329| 
	.dwattr $C$DW$281, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Johnny/V31/NewCAN/MMS_F_AEC/Standard_AEC_160/AEC_Standard/common/dict_util.c")
	.dwattr $C$DW$281, DW_AT_TI_end_line(0x163)
	.dwattr $C$DW$281, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$281

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_SEM_post
	.global	_writeNetworkDictCallBack
	.global	_closeSDOtransfer
	.global	_getReadResultNetworkDict
	.global	_MBX_pend
	.global	_MBX_post
	.global	_readNetworkDictCallback
	.global	_getWriteResultNetworkDict
	.global	_getSDOlineOnUseFromIndex
	.global	_SEM_pend
	.global	_BoardODdata
	.global	__getODentry
	.global	_CLK_getltime
	.global	__setODentry
	.global	_semaphoreSDOdone
	.global	_TSK_timerSem
	.global	_mailboxSDOout

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$289, DW_AT_name("cob_id")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$290, DW_AT_name("rtr")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$291, DW_AT_name("len")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$292, DW_AT_name("data")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$293, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$294, DW_AT_name("csSDO")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$295, DW_AT_name("csEmergency")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$296, DW_AT_name("csSYNC")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$297, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$298, DW_AT_name("csPDO")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$299, DW_AT_name("csLSS")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$300, DW_AT_name("errCode")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$301, DW_AT_name("errRegMask")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$302, DW_AT_name("active")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$99	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x18)
$C$DW$303	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$303, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$100


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$304, DW_AT_name("index")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$305, DW_AT_name("subindex")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$306, DW_AT_name("size")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$307, DW_AT_name("address")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$105	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)
$C$DW$T$106	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$106, DW_AT_address_class(0x16)

$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x0c)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$308, DW_AT_name("pdata")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$309, DW_AT_name("size")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$310, DW_AT_name("status")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$311, DW_AT_name("dataType")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$312, DW_AT_name("node_id")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$313, DW_AT_name("index")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$314, DW_AT_name("subindex")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$315, DW_AT_name("access")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("TDicMessage")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x08)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$316, DW_AT_name("wListElem")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$317, DW_AT_name("wCount")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$318, DW_AT_name("fxn")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33

$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$28	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x16)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x30)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$319, DW_AT_name("dataQue")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$320, DW_AT_name("freeQue")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$321, DW_AT_name("dataSem")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$322, DW_AT_name("freeSem")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$323, DW_AT_name("segid")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$324, DW_AT_name("size")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$325, DW_AT_name("length")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$326, DW_AT_name("name")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
$C$DW$T$121	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$121, DW_AT_address_class(0x16)
$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)

$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x04)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$327, DW_AT_name("next")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$328, DW_AT_name("prev")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x16)

$C$DW$T$44	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$44, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x10)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$329, DW_AT_name("job")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$330, DW_AT_name("count")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$331, DW_AT_name("pendQ")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$332, DW_AT_name("name")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44

$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$124	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$124, DW_AT_address_class(0x16)
$C$DW$T$125	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$333	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$29)
	.dwendtag $C$DW$T$30

$C$DW$T$31	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$31, DW_AT_address_class(0x16)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)

$C$DW$T$68	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)
$C$DW$334	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$67)
	.dwendtag $C$DW$T$68

$C$DW$T$69	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x16)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)

$C$DW$T$77	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)
$C$DW$335	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$67)
$C$DW$336	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$77

$C$DW$T$78	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x16)
$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$T$79	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$101	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$337	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$67)
$C$DW$338	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$6)
$C$DW$339	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$9)
$C$DW$340	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$101

$C$DW$T$102	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_address_class(0x16)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$341	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$341, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x16)
$C$DW$342	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$6)
$C$DW$T$56	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$342)
$C$DW$T$57	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x16)
$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$155	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$155, DW_AT_language(DW_LANG_C)
$C$DW$343	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$9)
$C$DW$T$54	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$343)
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x16)
$C$DW$T$76	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$76, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$160	.dwtag  DW_TAG_typedef, DW_AT_name("LgUns")
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$160, DW_AT_language(DW_LANG_C)

$C$DW$T$58	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
$C$DW$344	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$6)
$C$DW$345	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$58

$C$DW$T$59	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x16)
$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)

$C$DW$T$85	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
$C$DW$346	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$67)
$C$DW$347	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$48)
$C$DW$348	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$6)
$C$DW$349	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$85

$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)
$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)
$C$DW$350	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$87)
$C$DW$T$88	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$350)
$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)
$C$DW$T$90	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_address_class(0x16)

$C$DW$T$94	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$351	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$67)
$C$DW$352	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$9)
$C$DW$353	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$94

$C$DW$T$95	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_address_class(0x16)
$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$38	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x16)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)

$C$DW$T$97	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$97, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x01)
$C$DW$354	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$355	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$97

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$63	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$63, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x01)
$C$DW$356	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$357	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$358	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$359	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$360	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$361	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$362	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$363	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$63

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$80	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x80)
$C$DW$364	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$364, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$80


$C$DW$T$45	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$45, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x06)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$365, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$366, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$367, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$368, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$369, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$370, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$45

$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$371	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$51)
$C$DW$T$52	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$371)
$C$DW$T$53	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_address_class(0x16)

$C$DW$T$107	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$107, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x132)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$372, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$373, DW_AT_name("objdict")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$374, DW_AT_name("PDO_status")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$375, DW_AT_name("firstIndex")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$376, DW_AT_name("lastIndex")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$377, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$378, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$379, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$380, DW_AT_name("transfers")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$381, DW_AT_name("nodeState")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$382, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$383, DW_AT_name("initialisation")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$384, DW_AT_name("preOperational")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$385, DW_AT_name("operational")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$386, DW_AT_name("stopped")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$387, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$388, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$389, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$390, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$391, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$392, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$393, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$394, DW_AT_name("heartbeatError")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$395, DW_AT_name("NMTable")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$396, DW_AT_name("syncTimer")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$397, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$398, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$399, DW_AT_name("pre_sync")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$400, DW_AT_name("post_TPDO")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$401, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$402, DW_AT_name("toggle")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$403, DW_AT_name("canHandle")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$404, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$405, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$406, DW_AT_name("globalCallback")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$407, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$408, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$409, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$410, DW_AT_name("dcf_request")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$411, DW_AT_name("error_state")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$412, DW_AT_name("error_history_size")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$413, DW_AT_name("error_number")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$414, DW_AT_name("error_first_element")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$415, DW_AT_name("error_register")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$416, DW_AT_name("error_cobid")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$417, DW_AT_name("error_data")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$418, DW_AT_name("post_emcy")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$419, DW_AT_name("lss_transfer")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$420, DW_AT_name("eeprom_index")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$421, DW_AT_name("eeprom_size")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$107

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)
$C$DW$T$67	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_address_class(0x16)

$C$DW$T$109	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$109, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x0e)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$422, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$423, DW_AT_name("event_timer")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$424, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$425, DW_AT_name("last_message")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$109

$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$50	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_address_class(0x16)

$C$DW$T$111	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$111, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$111, DW_AT_byte_size(0x14)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$426, DW_AT_name("nodeId")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$427, DW_AT_name("whoami")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$428, DW_AT_name("state")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$429, DW_AT_name("toggle")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$430, DW_AT_name("abortCode")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$431, DW_AT_name("index")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$432, DW_AT_name("subIndex")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$433, DW_AT_name("port")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$434, DW_AT_name("count")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$435, DW_AT_name("offset")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$436, DW_AT_name("datap")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$437, DW_AT_name("dataType")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$438, DW_AT_name("timer")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$439, DW_AT_name("Callback")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$111

$C$DW$T$61	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)

$C$DW$T$62	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x3c)
$C$DW$440	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$440, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$62


$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x04)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$441, DW_AT_name("pSubindex")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$442, DW_AT_name("bSubCount")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$443, DW_AT_name("index")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$444	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$46)
$C$DW$T$47	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$444)
$C$DW$T$48	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$48, DW_AT_address_class(0x16)

$C$DW$T$91	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
$C$DW$445	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$9)
$C$DW$446	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$74)
$C$DW$447	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$90)
	.dwendtag $C$DW$T$91

$C$DW$T$92	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x16)
$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)

$C$DW$T$116	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$116, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x08)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$448, DW_AT_name("bAccessType")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$449, DW_AT_name("bDataType")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$450, DW_AT_name("size")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$451, DW_AT_name("pObject")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$452, DW_AT_name("bProcessor")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$116

$C$DW$453	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$116)
$C$DW$T$112	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$453)
$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)
$C$DW$T$114	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$T$114, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$454	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$454, DW_AT_location[DW_OP_reg0]
$C$DW$455	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$455, DW_AT_location[DW_OP_reg1]
$C$DW$456	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$456, DW_AT_location[DW_OP_reg2]
$C$DW$457	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$457, DW_AT_location[DW_OP_reg3]
$C$DW$458	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$458, DW_AT_location[DW_OP_reg20]
$C$DW$459	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$459, DW_AT_location[DW_OP_reg21]
$C$DW$460	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$460, DW_AT_location[DW_OP_reg22]
$C$DW$461	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$461, DW_AT_location[DW_OP_reg23]
$C$DW$462	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$462, DW_AT_location[DW_OP_reg24]
$C$DW$463	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$463, DW_AT_location[DW_OP_reg25]
$C$DW$464	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$464, DW_AT_location[DW_OP_reg26]
$C$DW$465	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$465, DW_AT_location[DW_OP_reg28]
$C$DW$466	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$466, DW_AT_location[DW_OP_reg29]
$C$DW$467	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$467, DW_AT_location[DW_OP_reg30]
$C$DW$468	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$468, DW_AT_location[DW_OP_reg31]
$C$DW$469	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$469, DW_AT_location[DW_OP_regx 0x20]
$C$DW$470	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$470, DW_AT_location[DW_OP_regx 0x21]
$C$DW$471	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$471, DW_AT_location[DW_OP_regx 0x22]
$C$DW$472	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$472, DW_AT_location[DW_OP_regx 0x23]
$C$DW$473	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$473, DW_AT_location[DW_OP_regx 0x24]
$C$DW$474	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$474, DW_AT_location[DW_OP_regx 0x25]
$C$DW$475	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$475, DW_AT_location[DW_OP_regx 0x26]
$C$DW$476	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$476, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$477	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$477, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$478	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$478, DW_AT_location[DW_OP_reg4]
$C$DW$479	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$479, DW_AT_location[DW_OP_reg6]
$C$DW$480	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$480, DW_AT_location[DW_OP_reg8]
$C$DW$481	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$481, DW_AT_location[DW_OP_reg10]
$C$DW$482	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$482, DW_AT_location[DW_OP_reg12]
$C$DW$483	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$483, DW_AT_location[DW_OP_reg14]
$C$DW$484	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$484, DW_AT_location[DW_OP_reg16]
$C$DW$485	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$485, DW_AT_location[DW_OP_reg17]
$C$DW$486	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$486, DW_AT_location[DW_OP_reg18]
$C$DW$487	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$487, DW_AT_location[DW_OP_reg19]
$C$DW$488	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$488, DW_AT_location[DW_OP_reg5]
$C$DW$489	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$489, DW_AT_location[DW_OP_reg7]
$C$DW$490	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$490, DW_AT_location[DW_OP_reg9]
$C$DW$491	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$491, DW_AT_location[DW_OP_reg11]
$C$DW$492	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$492, DW_AT_location[DW_OP_reg13]
$C$DW$493	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg15]
$C$DW$494	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$494, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$495	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$495, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$496	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$496, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$497	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$497, DW_AT_location[DW_OP_regx 0x30]
$C$DW$498	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$498, DW_AT_location[DW_OP_regx 0x33]
$C$DW$499	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$499, DW_AT_location[DW_OP_regx 0x34]
$C$DW$500	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$500, DW_AT_location[DW_OP_regx 0x37]
$C$DW$501	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$501, DW_AT_location[DW_OP_regx 0x38]
$C$DW$502	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$502, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$503	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$503, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$504	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$504, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$505	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$505, DW_AT_location[DW_OP_regx 0x40]
$C$DW$506	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$506, DW_AT_location[DW_OP_regx 0x43]
$C$DW$507	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$507, DW_AT_location[DW_OP_regx 0x44]
$C$DW$508	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$508, DW_AT_location[DW_OP_regx 0x47]
$C$DW$509	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$509, DW_AT_location[DW_OP_regx 0x48]
$C$DW$510	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$510, DW_AT_location[DW_OP_regx 0x49]
$C$DW$511	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$511, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$512	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$512, DW_AT_location[DW_OP_regx 0x27]
$C$DW$513	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$513, DW_AT_location[DW_OP_regx 0x28]
$C$DW$514	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$514, DW_AT_location[DW_OP_reg27]
$C$DW$515	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$515, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

