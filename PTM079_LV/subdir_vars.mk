################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../F28069.cmd \
../F2806x_Headers_BIOS.cmd 

TCF_SRCS += \
../gateway.tcf 

S??_SRCS += \
./gatewaycfg.s?? 

C_SRCS += \
../ADS1015.c \
../F2806x_DevInit079.c \
../Init280x068.c \
../buffer.c \
../can.c \
../convert.c \
../error.c \
../gateway.c \
./gatewaycfg_c.c \
../hal.c \
../i2c.c \
../lcd.c \
../param.c \
../recorder.c \
../temperature.c \
../usb_dev_hid.c 

OBJS += \
./ADS1015.obj \
./F2806x_DevInit079.obj \
./Init280x068.obj \
./buffer.obj \
./can.obj \
./convert.obj \
./error.obj \
./gateway.obj \
./gatewaycfg.obj \
./gatewaycfg_c.obj \
./hal.obj \
./i2c.obj \
./lcd.obj \
./param.obj \
./recorder.obj \
./temperature.obj \
./usb_dev_hid.obj 

GEN_MISC_FILES += \
./gateway.cdb 

GEN_HDRS += \
./gatewaycfg.h \
./gatewaycfg.h?? 

S??_DEPS += \
./gatewaycfg.pp 

C_DEPS += \
./ADS1015.pp \
./F2806x_DevInit079.pp \
./Init280x068.pp \
./buffer.pp \
./can.pp \
./convert.pp \
./error.pp \
./gateway.pp \
./gatewaycfg_c.pp \
./hal.pp \
./i2c.pp \
./lcd.pp \
./param.pp \
./recorder.pp \
./temperature.pp \
./usb_dev_hid.pp 

GEN_CMDS += \
./gatewaycfg.cmd 

GEN_FILES += \
./gatewaycfg.cmd \
./gatewaycfg.s?? \
./gatewaycfg_c.c 

GEN_HDRS__QUOTED += \
"gatewaycfg.h" \
"gatewaycfg.h??" 

GEN_MISC_FILES__QUOTED += \
"gateway.cdb" 

GEN_FILES__QUOTED += \
"gatewaycfg.cmd" \
"gatewaycfg.s??" \
"gatewaycfg_c.c" 

C_DEPS__QUOTED += \
"ADS1015.pp" \
"F2806x_DevInit079.pp" \
"Init280x068.pp" \
"buffer.pp" \
"can.pp" \
"convert.pp" \
"error.pp" \
"gateway.pp" \
"gatewaycfg_c.pp" \
"hal.pp" \
"i2c.pp" \
"lcd.pp" \
"param.pp" \
"recorder.pp" \
"temperature.pp" \
"usb_dev_hid.pp" 

S??_DEPS__QUOTED += \
"gatewaycfg.pp" 

OBJS__QUOTED += \
"ADS1015.obj" \
"F2806x_DevInit079.obj" \
"Init280x068.obj" \
"buffer.obj" \
"can.obj" \
"convert.obj" \
"error.obj" \
"gateway.obj" \
"gatewaycfg.obj" \
"gatewaycfg_c.obj" \
"hal.obj" \
"i2c.obj" \
"lcd.obj" \
"param.obj" \
"recorder.obj" \
"temperature.obj" \
"usb_dev_hid.obj" 

C_SRCS__QUOTED += \
"../ADS1015.c" \
"../F2806x_DevInit079.c" \
"../Init280x068.c" \
"../buffer.c" \
"../can.c" \
"../convert.c" \
"../error.c" \
"../gateway.c" \
"./gatewaycfg_c.c" \
"../hal.c" \
"../i2c.c" \
"../lcd.c" \
"../param.c" \
"../recorder.c" \
"../temperature.c" \
"../usb_dev_hid.c" 

GEN_CMDS__FLAG += \
-l"./gatewaycfg.cmd" 

S??_SRCS__QUOTED += \
"./gatewaycfg.s??" 

S??_OBJS__QUOTED += \
"gatewaycfg.obj" 


