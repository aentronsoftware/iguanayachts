/**********************************************************************

   Init280x.c - initialisation of all modules

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 26.08.14
   For   : PTM
   Compil: TI CCS5.4
   Target: PTM068
   descr.: init of the firmware
   Edit  :

 *********************************************************************/


#include "uc.h"
#include "dspspecs.h"
#include "F2806x_EPwm_defines.h"
#include "f2806xbmsk.h"
//#include "sysparam.h"
//#include "pwm.h"
#include "i2c.h"
#include "powerman.h"
#include "gateway_dict.h"
#define  _INIT_
#include "can.h"
#include "objdatatype.h"
#include "testbios.h"
//#include "CLAShared.h"
//#include "sincos.h"
#include "sci1.h"
#include "hal.h"
#include "buffer.h"

#define ERROR_ACTIVE    1
#define PWMOUT_COUNT    (UC_PHI/16/6/50) //20833
// Prototype statements for functions found within this file.

//interrupts
void periodic_update_isr(void);

// Globals
uint8  INI_WdogTimer;     /* RW Watchdog timer load value */

void InitPwmPhase(volatile struct EPWM_REGS *epwmregs, uint16 period, uint16 init, uint16 phase)
/* ePWM module init */
{
   // Setup TBCLK
   epwmregs->TBCTL.bit.FREE_SOFT = 2;         //Free running counter            pdf: spru791a.pdf page=93
   epwmregs->TBPRD = period;  //Set timer period                             pdf: spru791a.pdf page=92
   epwmregs->TBCTL.bit.PHSEN = TB_DISABLE;    //Disable phase loading           pdf: spru791a.pdf page=94
   epwmregs->TBPHS.half.TBPHS = 0;       //Phase is 0                      pdf: spru791a.pdf page=94
   epwmregs->TBCTR = phase;                  //init counter                   pdf: spru791a.pdf page=92
   epwmregs->TBCTL.bit.HSPCLKDIV = TB_DIV1;   //Clock ratio to SYSCLKOUT        pdf: spru791a.pdf page=93
   epwmregs->TBCTL.bit.CLKDIV = TB_DIV1;      //Time-base Clock Prescale        pdf: spru791a.pdf page=93
   epwmregs->TBCTL.bit.SYNCOSEL = TB_SYNC_IN;

   // Setup shadow register load on ZERO                                        pdf: spru791a.pdf page=97
   epwmregs->CMPCTL.bit.SHDWAMODE = CC_SHADOW;
   epwmregs->CMPCTL.bit.SHDWBMODE = CC_SHADOW;
   epwmregs->CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
   epwmregs->CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

   // Set Compare values                                                        pdf: spru791a.pdf page=95
   epwmregs->CMPA.half.CMPA = init;    // Set compare A value
   epwmregs->CMPB = init;         // Set Compare B value

   // Set actions for outputA                                                   pdf: spru791a.pdf page=98
   epwmregs->AQCTLA.bit.CAD = AQ_CLEAR;  // Clear Lx on event A, down count
   epwmregs->AQCTLA.bit.CAU = AQ_SET;    // Set Lx on event A, up count

   // Set actions for outputB                                                   pdf: spru791a.pdf page=99
   epwmregs->AQCTLB.bit.CBD = AQ_CLEAR;  // Clear /Hx on event A, down count (activate high mosfet)
   epwmregs->AQCTLB.bit.CBU = AQ_SET;    // Set   /Hx on event A, up count

   //set dead band module
   /*epwmregs->DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
   epwmregs->DBCTL.bit.POLSEL = DB_ACTV_HI;
   epwmregs->DBRED = 100;
   epwmregs->DBFED = 100;*/

   //start counter
   epwmregs->TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; //Count up and down           pdf: spru791a.pdf page=94
}


#define CLK_DIV_1     0
#define CLK_DIV_2     1
#define CLK_DIV_4     2
#define CLK_DIV_8     3
#define CLK_DIV_16    4
#define CLK_DIV_32    5
#define CLK_DIV_64    6
#define CLK_DIV_128   7



#define HSP_CLK_DIV_1   0
#define HSP_CLK_DIV_2   1
#define HSP_CLK_DIV_4   2
#define HSP_CLK_DIV_6   3
#define HSP_CLK_DIV_8   4
#define HSP_CLK_DIV_10  5
#define HSP_CLK_DIV_12  6
#define HSP_CLK_DIV_14  7

void InitPwm(){
/* init of used PWM */
  //Stop the clock signal for the pwms
  EALLOW;
  SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;       //                              pdf: spru712d.pdf page=42
  EDIS;
  //init start of conversion valve current
  InitPwmPhase(&EPwm4Regs,VALVE_COUNT,0,0);
  InitPwmPhase(&EPwm5Regs,VALVE_COUNT,0,0);
  InitPwmPhase(&EPwm1Regs,MEAS_COUNT,0,0);

  EPwm1Regs.TBCTL.bit.HSPCLKDIV = HSP_CLK_DIV_10;   //Clock ratio to SYSCLKOUT        pdf: spru791a.pdf page=93
  //EPwm1Regs.TBCTL.bit.CLKDIV = CLK_DIV_8;

  // Synchronize the counters for pwms
  EALLOW;
  SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;       //                              pdf: spru712d.pdf page=42
  //GPIO6 is used for switching on/off 24V
  GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 1;   // 0=GPIO, 1=EPWM4A, 2=EPWMSYNCI, 3=EPWMSYNCO
  //GPIO7,8 and 9 are pwm for controlling relays
  GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 1;   // 0=GPIO, 1=EPWM4B, 2=SCIRXDA, 3=ECAP2
  GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 1;   // 0=GPIO,  1=EPWM5A,  2=Resv,  3=ADCSOCA
  GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 1;   // 0=GPIO,  1=EPWM5B,  2=SCITXDB,  3=ECAP3

  EDIS;
}

void init_qep(void)
{
    EQep1Regs.QUPRD= POS_COUNT;       // Unit Timer for 1000Hz                    pdf: SPRUFK8.pdf page=36
    EQep1Regs.QUTMR = PWM_COUNT*4;
    EQep1Regs.QDECCTL.bit.QSRC=00;    // Quadrature count mode                    pdf: SPRUFK8.pdf page=30
    EQep1Regs.QDECCTL.bit.QIP=1;      // Negates QEPI input
    EQep1Regs.QDECCTL.bit.QAP=0;      // Negates QEPA input

    //eQEP Control (QEPCTL)                                                       pdf: SPRUFK8.pdf page=31
    EQep1Regs.QEPCTL.bit.FREE_SOFT=2; // Pos counter is unaffected by emulation suspend
    EQep1Regs.QEPCTL.bit.PCRM=1;      // Position counter reset at the maximum position
    //EQep1Regs.QEPCTL.bit.SEI=2;       // Init register on rising edge of strobe event
    EQep1Regs.QEPCTL.bit.SEL=0;       // latch on rising edge of strobe event
    EQep1Regs.QEPCTL.bit.IEL=1;       // Latch on rising edge of index
    EQep1Regs.QEPCTL.bit.QPEN=1;      // QEP enable                               pdf: SPRUFK8.pdf page=31
    EQep1Regs.QEPCTL.bit.QCLM=1;      // Latch on unit time out
    EQep1Regs.QEPCTL.bit.UTE=1;       // Unit Timeout Enable

    EQep1Regs.QPOSMAX= 0xFFFFFFFF;  // Maximum Position Count                   pdf: SPRUFK8.pdf page=34

    //eQEP Capture Control                                                        pdf: SPRUFK8.pdf page=33
    EQep1Regs.QCAPCTL.bit.UPPS=2;     // read 1/(2^UPPS) unit position event
    EQep1Regs.QCAPCTL.bit.CCPS=2;     // f(CAPCLK) = SYSCLKOUT/(2^CCPS)
    EQep1Regs.QCAPCTL.bit.CEN=1;      // QEP Capture Enable

    //eQEP Interrupt Enable (QEINT)                       pdf: SPRUFK8.pdf page=37
    EQep1Regs.QEINT.bit.UTO = 1;      // enable unit timer interrupt

    EQep1Regs.QPOSINIT = 0;
}


#define ADC_usDELAY 10000L

void init_math(void)
/* AD init and also call the PWM init */
{
  DELAY_US(ADC_usDELAY);
  AdcRegs.ADCCTL1.all = ADC_RESET_FLAG;
  asm(" NOP ");
  asm(" NOP ");

  EALLOW;
  AdcRegs.ADCCTL1.bit.ADCBGPWD	= 1;	/* Power up band gap */

  DELAY_US(ADC_usDELAY);					/* Delay before powering up rest of ADC */

  AdcRegs.ADCCTL1.bit.ADCREFSEL	= 0;  //mode 1 VREFHI is the pin ADC A0 -> don't use this mode
  AdcRegs.ADCCTL1.bit.ADCREFPWD	= 1;	/* Power up reference */
  AdcRegs.ADCCTL1.bit.ADCPWDN 	= 1;	/* Power up rest of ADC */
  AdcRegs.ADCCTL1.bit.ADCENABLE	= 1;	/* Enable ADC */

  asm(" RPT#100 || NOP");

  AdcRegs.ADCCTL1.bit.INTPULSEPOS=1;
  AdcRegs.ADCCTL1.bit.TEMPCONV=0;

  DELAY_US(ADC_usDELAY);

#define ADCTRIG_PWM1_SOCA  5
#define ADCTRIG_PWM4_SOCA 11
#define ADC_CLK_CYCLES    22

  /******* CHANNEL SELECT *******/
  AdcRegs.SOCPRICTL.bit.SOCPRIORITY 	= 4;	/* SOC0..3 are high priority rest are RB */
  AdcRegs.ADCSAMPLEMODE.bit.SIMULEN0 	= 0;	 //Simultaneous sampling channelA&B
  AdcRegs.ADCSOC0CTL.bit.CHSEL 	  = 9;   	/* ChSelect: ADC B1-> valve I4 */
  AdcRegs.ADCSOC0CTL.bit.TRIGSEL 	= ADCTRIG_PWM4_SOCA;	/* Set SOC0 start trigger on EPWM4 socA, due to round-robin SOC0 converts first then SOC1*/
  AdcRegs.ADCSOC0CTL.bit.ACQPS 	  = ADC_CLK_CYCLES;	/* Set SOC0 S/H Window to 34 ADC Clock Cycles, (33 ACQPS plus 1)*/
  AdcRegs.ADCSOC1CTL.bit.CHSEL 	  = 1;	/* ChSelect: ADC A1-> valve I3 */
  AdcRegs.ADCSOC1CTL.bit.TRIGSEL  = ADCTRIG_PWM4_SOCA;
  AdcRegs.ADCSOC1CTL.bit.ACQPS 	  = ADC_CLK_CYCLES;

  AdcRegs.ADCSAMPLEMODE.bit.SIMULEN2 	= 0;	 /*Simultaneous sampling channelA&B */
  AdcRegs.ADCSOC2CTL.bit.CHSEL 	  = 8;	/* ChSelect: ADC B0-> valve I2 */
  AdcRegs.ADCSOC2CTL.bit.TRIGSEL  = ADCTRIG_PWM4_SOCA;	/* Set SOC2 start trigger on epwm4 soca */
  AdcRegs.ADCSOC2CTL.bit.ACQPS 	  = ADC_CLK_CYCLES;
  AdcRegs.ADCSOC3CTL.bit.CHSEL 	  = 0;	/* ChSelect: ADC A0->Valve I1 */
  AdcRegs.ADCSOC3CTL.bit.TRIGSEL 	= ADCTRIG_PWM4_SOCA;
  AdcRegs.ADCSOC3CTL.bit.ACQPS 	  = ADC_CLK_CYCLES;
//  AdcRegs.INTSEL1N2.bit.INT2SEL	= 3;	/* EOC3 is source of INT2 */
//  AdcRegs.INTSEL1N2.bit.INT2E		= 1;	/* INT2 enable */
  AdcRegs.INTSEL9N10.bit.INT9SEL  = 3;  /* EOC3 is source of INT9 */
  AdcRegs.INTSEL9N10.bit.INT9E    = 1;  /* INT9 enable */


  AdcRegs.ADCSAMPLEMODE.bit.SIMULEN4  = 1;   /*Simultaneous sampling channelA&B */
  AdcRegs.ADCSOC4CTL.bit.CHSEL 	 = 2 ;	//ADC A2-> encoder sin
  AdcRegs.ADCSOC4CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC4CTL.bit.ACQPS 	 = ADC_CLK_CYCLES;
  AdcRegs.ADCSOC5CTL.bit.CHSEL 	 = 2;	//ADC B2-> encoder cos
  AdcRegs.ADCSOC5CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC5CTL.bit.ACQPS 	 = ADC_CLK_CYCLES;
  AdcRegs.INTSEL1N2.bit.INT1SEL = 5;  /* EOC1 is source of INT1 */


  AdcRegs.ADCSAMPLEMODE.bit.SIMULEN6  = 1;   /*Simultaneous sampling channelA&B */
  AdcRegs.ADCSOC6CTL.bit.CHSEL   = 4 ;   //ADC A4-> safety line 1
  AdcRegs.ADCSOC6CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC6CTL.bit.ACQPS   = ADC_CLK_CYCLES;
  AdcRegs.ADCSOC7CTL.bit.CHSEL   = 4;    //ADC B4-> safety line 2
  AdcRegs.ADCSOC7CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC7CTL.bit.ACQPS   = ADC_CLK_CYCLES;


  AdcRegs.ADCSAMPLEMODE.bit.SIMULEN8  = 1;   /*Simultaneous sampling channelA&B */
  AdcRegs.ADCSOC8CTL.bit.CHSEL   = 5 ;   //ADC A5-> Temp1
  AdcRegs.ADCSOC8CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC8CTL.bit.ACQPS   = ADC_CLK_CYCLES;
  AdcRegs.ADCSOC9CTL.bit.CHSEL   = 5;    //ADC B5-> Temp2
  AdcRegs.ADCSOC9CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC9CTL.bit.ACQPS   = ADC_CLK_CYCLES;

  AdcRegs.ADCSAMPLEMODE.bit.SIMULEN10  = 1;   /*Simultaneous sampling channelA&B */
  AdcRegs.ADCSOC10CTL.bit.CHSEL   = 6 ;   //ADC A6-> Vin
  AdcRegs.ADCSOC10CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC10CTL.bit.ACQPS   = ADC_CLK_CYCLES;
  AdcRegs.ADCSOC11CTL.bit.CHSEL   = 6;    //ADC B6-> Pressure3
  AdcRegs.ADCSOC11CTL.bit.TRIGSEL = ADCTRIG_PWM1_SOCA;
  AdcRegs.ADCSOC11CTL.bit.ACQPS   = ADC_CLK_CYCLES;

  AdcRegs.INTSEL1N2.bit.INT2SEL = 11;  /* EOC11 is source of INT2 */
  AdcRegs.INTSEL1N2.bit.INT2E   = 1;  /* INT2 enable */

  EDIS;

  /* Set up Motor Event Trigger with CNT_PRD enable for Time-base of EPWM1 */
  EPwm4Regs.ETSEL.bit.SOCAEN  = 1;     /* Enable SOCA */
  EPwm4Regs.ETSEL.bit.SOCASEL = ET_CTR_ZERO; /* Enable CNT_zero event for SOCA */
  EPwm4Regs.ETPS.bit.SOCAPRD  = ET_3RD;     /* Generate SOCA on the first event */
  EPwm4Regs.ETCLR.bit.SOCA    = 1;       /* Clear SOCA flag */

  EPwm1Regs.ETSEL.bit.SOCAEN  = 1;     /* Enable SOCA */
  EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_ZERO; /* Enable CNT_zero event for SOCA */
  EPwm1Regs.ETPS.bit.SOCAPRD  = ET_1ST;     /* Generate SOCA on the first event */
  EPwm4Regs.ETCLR.bit.SOCA    = 1;       /* Clear SOCA flag */



  // Initialize PWM module
  InitPwm();
  // Init qep
  init_qep();

}

#pragma CODE_SECTION(init_system,"init")
void init_system(void)
/* BIOS init */
{
	int i;
	uint16* ptr;
  asm(" .ref _c_int00");
	asm(" SETC OBJMODE");
	EALLOW;
	SysCtrlRegs.WDCR = 0x68;	//disable watchdog
	EDIS;
	ptr = (uint16 *)0xa000;
	for (i = 0; i < 0x3000; ++i) {
    *ptr++=0;
  }
	memcpy(&bios_runstart,&bios_loadstart,&bios_loadend - &bios_loadstart);
	memcpy(&sysinit_runstart,&sysinit_loadstart,&sysinit_loadend-&sysinit_loadstart);
	asm(" LB _c_int00 ");
}

interrupt void millisecint(void);
interrupt void ADC_Int1(void);
interrupt void I2C_INT1A(void);
interrupt void hwiFnCan0(void);
interrupt void manage_can_bus(void);
interrupt void HAL_ReadAnalogueInputs(void);
interrupt void HAL_ReadAnalogueInputs2(void);
interrupt void Cla1Task_Int(void);
void USB_USB0DeviceIntHandler(void);

//#define CLA_MODE
//extern void cinit(void);
void InitFlash(void);
void DeviceInit(void);
#pragma DATA_SECTION(ActivityFlag,"HeaderActiv")
const uint16 ActivityFlag = 0xFFF0; //a decommenter si programmation avec ccs

void main(void)
/* main */
{
  int i;

  DeviceInit();	// Device Life support & GPIO
  // Only used if running from FLASH
  // Note that the variable FLASH is defined by the compiler

  // Copy time critical code and Flash setup code to RAM
  // The  RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
  // symbols are created by the linker. Refer to the linker files.
  EALLOW;
  memcpy(&hwi_vec_runstart,&hwi_vec_loadstart,&hwi_vec_loadend-&hwi_vec_loadstart);
  EDIS;
  memcpy(&RamfuncsRunStart,&RamfuncsLoadStart, &RamfuncsLoadEnd - &RamfuncsLoadStart);
  //memcpy(&CodeARunStart,&CodeALoadStart,&CodeALoadEnd - &CodeALoadStart);
  // Call Flash Initialization to setup flash waitstates
  // This function must reside in RAM
  InitFlash();	// Call the flash wrapper init function
  for(i=0;i<MEAS_CHANNELS;i++){
    BUF_Init(i);
  }
  // Timing sync for background loops
  // Timer period definitions found in device specific PeripheralHeaderIncludes.h
  //CpuTimer1Regs.PRD.all =  mSec1;		// A tasks

  // Initialize all modules (adc etc)
  init_math();
  InitCpuTimers();
  //SCI1_InitGpio();
  // Configure CPU-Timer 0 to interrupt every ISR Period:
  // 80MHz CPU Freq, ISR Period (in uSeconds)
  // This function is found in DSP280x_CpuTimers.c
  ConfigCpuTimer(&CpuTimer0, UC_PHI/1000000, 1000000/POS_FREQUENCY);

  // Reassign ISRs.
  // Reassign the PIE vector for TINT0 to point to a different
  // ISR then the shell routine found in DSP280x_DefaultIsr.c.
  // This is done if the user does not want to use the shell ISR routine
  // but instead wants to use their own ISR.
  //InitPieVectTable();		//ne dois pas etre faite en dsp_bios

  //-------------------------------------------------------------
  // CLA module initialization
  // 1) Enable the CLA clock (in DeviceInit())
  // 2) Init the CLA interrupt vectors
  // 3) Allow the IACK instruction to flag a CLA interrupt/start a task
  // 4) Assign CLA program memory to the CLA
  // 5) Enable CLA interrupts (MIER)
  //-------------------------------------------------------------
#ifdef CLA_MODE
  memcpy(&Cla1funcsRunStart,&Cla1funcsLoadStart, &Cla1funcsLoadEnd - &Cla1funcsLoadStart);
  memcpy(&Cla1mathRunStart,&Cla1mathLoadStart, &Cla1mathLoadEnd - &Cla1mathLoadStart);
  EALLOW;
  Cla1Regs.MVECT1 = (uint16) (&Cla1Task1 - &Cla1Prog_Start)*sizeof(Uint32);
  //Cla1Regs.MVECT2 = (uint16) (&Cla1Task2 - &Cla1Prog_Start)*sizeof(Uint32);
  Cla1Regs.MPISRCSEL1.bit.PERINT1SEL  = 1; //(0 : adcint1 default source, 1: software)
  //Cla1Regs.MPISRCSEL1.bit.PERINT2SEL  = 1;
  Cla1Regs.MCTL.bit.IACKE = 1;
  Cla1Regs.MMEMCFG.bit.PROGE = 1;
  Cla1Regs.MMEMCFG.bit.RAM1E = 1;
  asm("  RPT #3 || NOP");
  Cla1Regs.MIER.bit.INT1 = 1;
  //Cla1Regs.MIER.bit.INT2 = 1;
  EDIS;
#endif

  //SIN_Init();

  // Enable PIE
  PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
  I2C_Init();
  //SCI1_RS232Init();
  //PAR_SetParamDependantVars();
  EALLOW;	// This is needed to write to EALLOW protected registers
#ifdef DEBUG_MODE
  PieVectTable.ADCINT1 = &sincosinter;
#endif
#ifdef CLA_MODE
  PieVectTable.ADCINT1 = &ADC_Int1;
  PieVectTable.CLA1_INT1 = &Cla1Task_Int;
  AdcRegs.INTSEL1N2.bit.INT1E   = 1;  /* INT1 enable */
  //PieVectTable.ADCINT2 = &ADC_Int2;
  //PieVectTable.CLA1_INT2 = &Cla2Task_Int;
#endif
  PieVectTable.I2CINT1A = &I2C_INT1A;
  PieVectTable.ADCINT2 = &HAL_ReadAnalogueInputs2;
  PieVectTable.ADCINT9 = &HAL_ReadAnalogueInputs;
  PieVectTable.EQEP1_INT = &millisecint;
  PieVectTable.ECAN1INTA = &hwiFnCan0;
  PieVectTable.ECAN0INTA = &manage_can_bus;
  PieVectTable.USB0_INT = &USB_USB0DeviceIntHandler;
  EDIS;   // This is needed to disable write to EALLOW protected registers

  // Enable adc int1&2&9
  PieCtrlRegs.PIEIER1.bit.INTx1 = 1;//ADCINT1
  PieCtrlRegs.PIEIER1.bit.INTx2 = 1;//ADCINT2
  PieCtrlRegs.PIEIER1.bit.INTx6 = 1; //ADCINT9
  //PieCtrlRegs.PIEIER3.bit.INTx1 = 1;
  // Enable cpu0 int
  //PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
  // Enable eQEP1 int1
  PieCtrlRegs.PIEIER5.bit.INTx1 = 1;
  // Enable I2CA int
  PieCtrlRegs.PIEIER8.bit.INTx1 = 1;

#ifdef CLA_MODE
  // Enable cla int1&2
  PieCtrlRegs.PIEIER11.bit.INTx1 = 1;
  //PieCtrlRegs.PIEIER11.bit.INTx2 = 1;
#endif
  // Enable CPU INT1 for TINT0:
  IER |= M_INT1 | M_INT5 | M_INT8 | M_INT11;
  // Enable Global real time interrupt DBGM
  // Enable global Interrupts and higher priority real-time debug events:
  EINT;   // Enable Global interrupt INTM
  ERTM;	// Enable Global real time interrupt DBGM

  //StartCpuTimer0();

  // IDLE loop. Just sit and loop forever:
  //return(1);
}

