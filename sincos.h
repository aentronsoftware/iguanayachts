/**********************************************************************

   sincos.h - sin/cos enncoder interpolation

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : Rolex
   Compil: TI CCS5.2
   Target: PTM054
   descr.: encoder interpolation
   Edit  :
 *********************************************************************/

#ifndef SINCOS_H_
#define SINCOS_H_

extern bool1 SIN_IndexFlag;
/*set when the Index of the motor encoder mark is encountered
  must be reset by application */

void SIN_Init(void);
/* interpolation factor: subdivision of the full sine wave
 * if the encoder has N lines per turn, the interpolated encoder count is
 * N * interpolation_factor per turn
 */

//ODV_
#endif /* SINCOS_H_ */
