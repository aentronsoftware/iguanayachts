/*
 * hal.h
 *
 *  Created on: 8 mai 2013
 *      Author: remip
 */

#ifndef HAL_H_
#define HAL_H_

//generic
#define HAL_OUT1  1
#define HAL_OUT2  2
#define HAL_OUT3  3
#define HAL_OUT4  4
#define HAL_OUT5  5
#define HAL_OUT6  6

#define HAL_OUT_MAX 6

#define HAL_IN1   17
#define HAL_IN2   18
#define HAL_IN3   19
#define HAL_IN4   20

//defines
#define HAL_ON  1
#define HAL_OFF 0


#define PIC_MEASURE_TEMP1                  0
#define PIC_MEASURE_TEMP2                  1
#define PIC_MEASURE_LV25                   2
#define PIC_MEASURE_HAS500                 3
#define PIC_MEASURE_INSULATION1            4
#define PIC_MEASURE_INSULATION2            5
#define PIC_MEASURE_RELAY1                 6
#define PIC_MEASURE_RELAY2                 7
#define PIC_MEASURE_RELAY3                 8
#define PIC_MEASURE_LOCK_CUR              15

#define PIC_MEASURE_MAX   15

typedef int16 TMeasure[PIC_MEASURE_MAX];


#define TEMP_VAL_NB   43
typedef struct{
  int16 temp;
  float adval;
}TTempTable;
extern const TTempTable TempTable[TEMP_VAL_NB];

typedef struct {
  uint16 can_wk:1;
  uint16 sw_wk:1;
  uint16 ch_wk:1;
  uint16 SOC2:1;
  uint16 lem:1;
  uint16 onerelay:1;
  uint16 relayextra:1;
  uint16 b7:1;
  uint16 en24v:1;
  uint16 b9:1;
  uint16 b10:1;
  uint16 b11:1;
  uint16 b12:1;
  uint16 b13:1;
  uint16 b14:1;
  uint16 b15:1;
}TMMSConfig;
extern TMMSConfig* MMSConfig;

//Definition for buffer.c
#define MEASBUF_SIZE  64
#define MEAS_CHANNELS  PIC_MEASURE_MAX

#define MOTOR1_INDEX 0
#define MOTOR2_INDEX 1

#define MOTOR_INDEX_MAX 2

//************ GATEWAY_STATE_XXX *************************
#define GATEWAY_STATE_NORMAL               1	/// close relay
#define GATEWAY_STATE_CHARGE_ONLY          2	
#define GATEWAY_STATE_DISCHARGE_ONLY       4
#define GATEWAY_STATE_STILL                0	/// open relay
#define GATEWAY_STATE_CRASH                8

#define GATEWAY_STATE_ERROR_BIT         0x80
#define GATEWAY_STATE_WARNING_BIT       0x00

#define GATEWAY_MODE_NOINI -1
#define GATEWAY_MODE_CRASH  8
#define GATEWAY_MODE_STILL  0
#define GATEWAY_MODE_DONLY  4
#define GATEWAY_MODE_CONLY  2
#define GATEWAY_MODE_NORM   1
#define GATEWAY_MODE_PRECH  3
#define GATEWAY_MODE_24ON   6
#define GATEWAY_MODE_ERROR_IN 0x40
#define GATEWAY_MODE_ERROR  0x80


#define DRIVEABILITY_MODE_NONE     0
#define DRIVEABILITY_MODE_SWITCH1  1
#define DRIVEABILITY_MODE_SWITCH2  2

#define THROTTLE_INPUT_VOLTAGE_SPEED_LOW 100  //2600rpm*0.1 = 260rpm ->(/15) = 17.33 qc/s

//#define DECELERATE_TIMEOUT      1500 //10s
//#define DECELERATE_SPEED_MIN        6 //si POSITION_COUNTER_MAX = 200ms => 30qc/s

#define PARAM_INDEX_START 0x200A
#define PARAM_MAX         28

#define PIC_STATE_STATUS_BIT_PARAM_MODIFIED 1

#define P_RATIO    100
#define FLOW_RATIO 10

#define PO2_AIR 15922 //159.22*P_RATIO mmHg 20.95%*760mmHg
#define PATMO_STANDARD 76000 //760*P_RATIO mmHg

void HAL_Init(void);


#define BUTTON_MINUS      0 //Button-
#define BUTTON_PLUS       1 //Button+
#define BUTTON_V          2 //Button V
#define BUTTON_D          3 //Button D

#define BUTTON_NONE     255

#define BUTTON_MAX   4

#define ADS_AD_MAX      1500
#define ADS_ONE_POS     ADS_AD_MAX/10
#define ADS_HALF_VAL    ADS_AD_MAX/2
#define ADS_POTENTIOMETRE_VALUE   ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_POTENTIOMETRE]
#define ADS_BUTTONV_VALUE         ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_BUTTONV]
#define ADS_BUTTOND_VALUE         ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_BUTTOND]

//*************** EVENTS **********************
#define EVENT_NONE               0
#define EVENT_INVERSE_FORWARD    1
#define EVENT_INVERSE_REVERSE    2
#define EVENT_STOP_MIN           3
#define EVENT_STOP_MAX           4
#define EVENT_UNDERCURRENT       5
#define EVENT_UNDERCURRENT_START 6
#define EVENT_UNDERCURRENT_TIME  7

//*************** INVERSE STATES *************
#define INVERSE_NONE      0
#define INVERSE_ENABLE    1
#define INVERSE_RUNNING   2


void HAL_ButtonStateMachine(int16 button_index);


#define PUSH_TYPE_SHORT   0
#define PUSH_TYPE_NORMAL  1
#define PUSH_TYPE_LONG    2
#define PUSH_TYPE_EXLONG  3
#define PUSH_TYPE_NONE  255
#define LUT_TABLE_SIZE  34

bool1 HAL_GetButton(uint8 *button_index,uint8 *push_type);

#define PDO_PERIOD 20 //ms
extern bool1 HAL_NewCurPoint;
extern int32 HAL_Position;
extern int32 HAL_LastPosition;
extern uint16 HAL_DoorClosed;
extern uint16 HAL_RelayState;
extern uint32 PAR_Capacity_Left;
extern uint32 PAR_Capacity_Total;
extern int32 HAL_Current_Sum;
extern uint64 PAR_Capacity_TotalLife_Used;
extern int32 PAR_Operating_Hours_day;

void HAL_Unlock(void);
void HAL_Random(void);
void HAL_Reset(void);
void USB_Stop(void);

extern const uint32 AnalogData_X[LUT_TABLE_SIZE];
extern const uint32 Output_Voltage_Y[LUT_TABLE_SIZE];

//extern const int AnalogData_X[26] = { 530, 551, 571,592, 612,633, 654,674,695,715,736,757,777,798,819,839,860,880,901,921,942,962,983,1003,1024,1045};
//extern const int Output_Voltage_Y[26] = { 250, 260, 270, 280, 290,300,310,320,330,340,350,360,370,380,390,400,410,420,430,440,450,460,470,480,490,500};

// Function Prototypes
//
//void error(void);
//void initECAP(void);
//void initEPWM(void);
////////////////////////////

#endif /* HAL_H_ */
