/**********************************************************************

   hal.c - hardware abstraction layer

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 02.05.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.:
   Edit  :

 *********************************************************************/

#include "gatewaycfg.h"
#include "uc.h"
#include <clk.h>
#include "dspspecs.h"
#include "ngtestspec.h"
#include "gateway_dict.h"
#include "objdatatype.h"
#include "convert.h"
#include "recorder.h"
#include "hal.h"
#include "ERROR.H"
#include "sci1.h"
#include "IQmathLib.h"
#include "pid_reg3.h"
#include "buffer.h"
#include "ADS1015.h"
#include "param.h"

uint16  UpdateTimer = 0;
#define UPDATE_TIMER 10

bool1 HAL_NewCurPoint = FALSE;

int16 PositionCounter = POSITION_COUNTER_MAX;
int32 HAL_Position = 0;
int32 HAL_LastPosition = 0;
int32 HAL_Current_Sum = 0;
int32 SOC_Delay_Sec = 0, initializeSOC = 0;
int32 GV_Blinking_counter1 = 0;
uint16 CurrentDelayTime = 0;
int16 OldCurrent = 0;
uint16 WorkingHours = 0;
uint16 CurrCounterSec = 0;
uint16 OnceRecalibrateSOC = 0;
uint16 Low_Voltage_ResetCounter = 0;
uint16 Over_Voltage_ResetCounter = 0;
int16 RelayPLUS_CLose = 0;


/*
///  ISO  UNS16 ODP_VersionParameters = 0x7F;
uint32 UsCount = 0;
uint32 StartIso = 0;
uint16 HAL_IsoPeriod = 0; //period in Hz
uint16 HAL_IsoDuty = 0; //duty cycle %
uint16 HAL_OldVal = 0;
*/

uint16 HAL_TimeSOC = 0;

#define RELAY_ERROR   20
#define RELAY_TIMEOUT ODP_Contactor_Delay
#define RELAY_SEUILA  ODP_Contactor_Setup  //7.5V
#define RELAY_SEUILB  ODP_Contactor_Hold   //2.25V
#define RELAY_SLOPE   ODP_Contactor_Slope

#define RELAY_TIMEDELAY_STATE 16
#define RELAY_STILL_STATE     15

#define RELAY_BIT1  1
#define RELAY_BIT2  2
#define RELAY_BIT3  4

//EPwm4Regs.CMPA

#define RELAY_VALUE1  EPwm5Regs.CMPB  //P3- /Pwmvalve3
#define RELAY_VALUE2  EPwm5Regs.CMPA.half.CMPA  //P2 pre  /Pwmvalve2
#define RELAY_VALUE3  EPwm4Regs.CMPB  //P1+ /pwmvalve1
#define RELAY_VALUE4  EPwm4Regs.CMPA.half.CMPA  //P4- /Pwmvalve4

#define HVIL_Status	  GpioDataRegs.GPADAT.bit.GPIO21

uint16 HAL_DoorClosed = 0;
uint16 HAL_RelayState = RELAY_STILL_STATE;
TMMSConfig* MMSConfig;

//int16 ODV_Gateway_Voltage_Copy = 0;

void HAL_Init(void){

}

#define BLINK_PERIOD 500
#define BLINK_DUTY   250

int16 BlinkCounter = 0;
extern UNS32 CommTimeout;
/******************************************************************************
          millisecint
      ========================

INTERRUPT SOURCE : QEP1 interrupt  EQEP1_INT
FREQUENCY : 1ms

FUNCTION :
*******************************************************************************/
interrupt void millisecint(void) // 1ms
{

  EINT;
  AcknowlegeInt = PIEACK_GROUP5;
  EnableCaptureCurrentInterrupts; //allow adc et cla int to interrupt

  /////////////////////////////////////////////////////////////////////
   GpioDataRegs.GPADAT.bit.GPIO16 = 0;
   GpioDataRegs.GPADAT.bit.GPIO17 = 0;
   GpioDataRegs.GPADAT.bit.GPIO18 = 0;

   if (ODV_ErrorDsp_ErrorNumber == 0 && ODV_ErrorDsp_WarningNumber != WARN_CYCLE)
   {
  	 /// Green stays
  	 GpioDataRegs.GPADAT.bit.GPIO19 = 1;
  	 GpioDataRegs.GPADAT.bit.GPIO28 = 0;
   }
   else
   {
  	 GpioDataRegs.GPADAT.bit.GPIO19 = 0;

	   /// red flashing
	   GV_Blinking_counter1++;
	   if (GV_Blinking_counter1 > 200)
	   {
		   if (GpioDataRegs.GPADAT.bit.GPIO28 == 1)
		   {
			   GpioDataRegs.GPADAT.bit.GPIO28 = 0;
			   GV_Blinking_counter1 = 0;
		   }
		   else
		   {
			   GpioDataRegs.GPADAT.bit.GPIO28 = 1;
			   GV_Blinking_counter1 = 0;
		   }
	   }
   }
   ///////////////////////////////////////////////////////////////////
  //inc milisec time
  ++ODV_SysTick_ms;
  //inc second time
  if (ODV_SysTick_ms%1000 == 999)
  {
    ++ODP_OnTime;
    ++HAL_TimeSOC;
    ++CurrentDelayTime;
    ++WorkingHours;
    ++SOC_Delay_Sec;
    ++Low_Voltage_ResetCounter;
    ++Over_Voltage_ResetCounter;
    ++initializeSOC;
  }

  if(CurrentDelayTime > 2)  /// V52  16.04.2021
  {
	  if(ODV_Gateway_Current == 0) ODV_Gateway_Current = ODV_Gateway_Current_Average;

	  int16 NewVurrent = ODV_Gateway_Current_Average;
	  if (abs((abs(OldCurrent) - abs(NewVurrent))) >= (ODP_Gateway_Current_Resolution*0.25))
	  {
		  ODV_Gateway_Current = ODV_Gateway_Current_Average;
		  OldCurrent = NewVurrent;
	  }
	  CurrentDelayTime = 0;
  }

  if (ODP_CommError_TimeOut > 0){
    if (ODV_SysTick_ms - CommTimeout > ODP_CommError_TimeOut && ODV_MachineMode > GATEWAY_MODE_STILL
        && ODV_MachineMode < GATEWAY_MODE_24ON)
    {
    	if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorComm();
			ODV_MachineMode = GATEWAY_MODE_ERROR_IN;
		}		
  }

  ClearUTO; // Clear UTO flag and clear INT flag              pdf: spru790a.pdf page=29
  ClearINT;

  ///////////////////////////////////////////////
  // 1 Hour counter Start 3600 Sec = 1 Hour
  /////////////////////////////////////////////
  if (WorkingHours > 3600)
  {
	  PAR_Operating_Hours_day++;
 	  WorkingHours = 0;
  }

  /*// To reset the Values to "0"*/

  if(ODP_Gateway_Voltage_EEPROM_Limit == 1)
  {
	   PAR_Operating_Hours_day = 0;
	   ODP_CommError_OverTemp_ErrCounter = 0;
	   ODP_CommError_OverVoltage_ErrCounter = 0;
	   ODP_CommError_LowVoltage_ErrCounter = 0;
  }

   ODV_Battery_Operating_Hours = PAR_Operating_Hours_day;
   if (ODV_Battery_Operating_Hours>87600) ODV_Battery_Operating_Hours = 87600;

   /////////////////////////////////////////////
   //End of Working Hours Log into EEPROM
   ////////////////////////////////////////////

  //start 1st conversion in order to stabilize adc because 1st sampling is very bad
  //AdcRegs.ADCSOCFRC1.bit.SOC0 = 1;
  //while(AdcRegs.ADCSOCFLG1.bit.SOC0){}
  //start all adc, will be done in order from 0 to 15 and will take approximately 13us
  //AdcRegs.ADCSOCFRC1.all = 0xFFFF;
  //end of conversion will call the readinput

  /**************** Compute real time vars ********************/
  /************************************************************/

  if(BlinkCounter > 0){
    --BlinkCounter;
    if((BlinkCounter == BLINK_DUTY) && (ODV_MachineMode == GATEWAY_MODE_STILL)){
      //led - off in init mode
      GpioDataRegs.GPASET.bit.GPIO7 = 1;
    }
  }
  else{
    BlinkCounter = BLINK_PERIOD;
    if(GpioDataRegs.GPADAT.bit.GPIO7){
      //led - on
      GpioDataRegs.GPACLEAR.bit.GPIO7 = 1;
    }
  }

  //SCI1_Update();
  /* record the must and is values for future analysis */
  if (ODV_Recorder_Control.Pos_Record)
  {
    REC_Record();
  }
}


#define AD_VREF 3300.0 //3300mV
#define AD_RESOL 4095  //AD 10 bits
#define VALVE_RMES 0.5 //5.1 ohm

//#define AD_TO_mVOLT  (AD_VREF/AD_RESOL*20.2/2.2)       //convert ad result to voltage for current source

#define VALVE_AD_TO_0_1mAMP   (AD_VREF*10/AD_RESOL/VALVE_RMES) //convert ad result to current


//called by end of conversion 3 //5kHz VALVE_COUNT frequency PWMEPwm4Regs.ETPS.bit.SOCAPRD  = ET_1ST;
#pragma CODE_SECTION(HAL_ReadAnalogueInputs,"ramfuncs")
interrupt void HAL_ReadAnalogueInputs(void){

 //ToggleTestPin1;
  AcknowlegeInt = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

  if (ReadCurrent1 >= ODP_Contactor_Overcurrent || ReadCurrent2 >= ODP_Contactor_Overcurrent || ReadCurrent3 >= ODP_Contactor_Overcurrent)
	  {
	   	  if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
	   	  {
	   		  HAL_RelayState = RELAY_ERROR;
	   		if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorDigOvld();
	   	  }
	  }
  if ((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT1) && ReadCurrent1 >= ODP_Contactor_Overcurrent) 
	{
   	  if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
   	  {
   		  HAL_RelayState = RELAY_ERROR;
   		if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_NEG();
   	  }
	}
  if ((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT2) && ReadCurrent2 >= ODP_Contactor_Overcurrent) 
	  {
   	  if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
   	  {
	   	  HAL_RelayState = RELAY_ERROR;
	   	if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PRE();
   	  }
	  }
  if ((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT3) && ReadCurrent3 >= ODP_Contactor_Overcurrent) 
	  {
   	  if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
   	  {
	  	HAL_RelayState = RELAY_ERROR;
	  	if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PLUS();
   	  }
	}
  //ToggleTestPin1;
  AdcRegs.ADCINTFLGCLR.bit.ADCINT9 = 1;
}

#define MEASURE_COMPUTE_PERIOD 16

int MeasureCounter = MEASURE_COMPUTE_PERIOD;

//called by end of measure conversions
interrupt void HAL_ReadAnalogueInputs2(void){
  TMeasure measure;

  AcknowlegeInt = PIEACK_GROUP1;   // Acknowledge interrupt to PIE
  //ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_LV25] =
  measure[PIC_MEASURE_LV25] = (int16)AdcResult.ADCRESULT4;
  ODV_Gateway_ISO_Monitor = (int16)AdcResult.ADCRESULT5;
  //ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_HAS500] =
  measure[PIC_MEASURE_HAS500] = (int16)AdcResult.ADCRESULT5;
  measure[PIC_MEASURE_TEMP1] = (int16)AdcResult.ADCRESULT8;
  measure[PIC_MEASURE_TEMP2] = (int16)AdcResult.ADCRESULT9;
  measure[PIC_MEASURE_INSULATION1] = AdcResult.ADCRESULT6;
  measure[PIC_MEASURE_INSULATION2] = AdcResult.ADCRESULT7;
  MBX_post(&mailboxMeasures,&measure,0);
  AdcRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;
}

#define MEAS_T_NB 16
#define RTEMP_REF 10000 //10 kohm
#define NTC_TEMP0         250  //�C*10
#define NTC_STEP_DEGREES   50  //�C*10

//Resultat en �C*10
int16 HAL_LectureTemperature(int measure_index){
  float val;
  int i = measure_index;

  val = BUF_SumLast(i,MEAS_T_NB,TRUE) / MEAS_T_NB;
  i = 0;
  while((i<TEMP_VAL_NB-1) && (val < TempTable[i].adval)){
    ++i;
  }
  if((i > 0) && (i<TEMP_VAL_NB)){
    val = (TempTable[i-1].adval - val) / (TempTable[i-1].adval-TempTable[i].adval);
    i = val * NTC_STEP_DEGREES + TempTable[i-1].temp*10;
  }
  else{
    i = TempTable[i].temp*10;
  }
  return i;
}

void TskFnComputeMeasures(void){
  TMeasure measure;
  int i;
  float temp, temp1;

  int16 timeout = 50, oldval, newval;
  uint32 start = 0, count = 0;
  ODV_Recorder_Period = 1000;
  ODV_Recorder_Vectors = 0x8000000000706;
  ODV_Recorder_Start = 0;
  while(InitOK == 0){//wait for init
    TSK_sleep(1);
  };
  TSK_sleep(50);
  //REC_StartRecorder();
  while(1){

    while(MBX_pend(&mailboxMeasures,&measure,0)){
      for(i=PIC_MEASURE_TEMP1;i<=PIC_MEASURE_INSULATION2;i++){
        BUF_Write(i,measure[i]);
      }
    }

    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP1]= HAL_LectureTemperature(PIC_MEASURE_TEMP1);
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP2]= HAL_LectureTemperature(PIC_MEASURE_TEMP2);
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_INSULATION1] = ReadCurrent0; //32760-BUF_SumLast(PIC_MEASURE_INSULATION1,MEAS_T_NB,TRUE)/2;// MEAS_T_NB;
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_INSULATION2] = AdcResult.ADCRESULT11; //32760-BUF_SumLast(PIC_MEASURE_INSULATION2,MEAS_T_NB,TRUE)/2;// MEAS_T_NB;
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_RELAY1] = ReadCurrent3;
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_RELAY2] = ReadCurrent1;
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_RELAY3] = ReadCurrent2;
    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_LV25] = BUF_SumLast(PIC_MEASURE_LV25,MEAS_T_NB,TRUE) / MEAS_T_NB + ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_LV25];

    int TempCu = PIC_MEASURE_HAS500;

    ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_HAS500] = (int)(BUF_SumLast(PIC_MEASURE_HAS500,MEAS_T_NB,TRUE) / MEAS_T_NB) + ODP_Analogue_Input_Offset_Integer[TempCu];

    temp = ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP1]* ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_TEMP1];
    ODV_Gateway_Temperature = CNV_Round(temp); //1�C resolution

    if (temp < ODP_SafetyLimits_Tmin || temp > ODP_SafetyLimits_Tmax){
      ODV_Current_ChargeAllowed = 0;
    } else {
      if (temp < ODP_Temperature_ChargeLimits[0]){
       ODV_Current_ChargeAllowed = CNV_Round(ODP_Power_ChargeLimits[0] * ODP_SafetyLimits_Imax_charge / 100.0);
      }
      else for (i=0; i<7; i++){
        if (ODP_Temperature_ChargeLimits[i] >= ODP_Temperature_ChargeLimits[i+1]) {
          ODV_Current_ChargeAllowed = CNV_Round(ODP_Power_ChargeLimits[i] * ODP_SafetyLimits_Imax_charge / 100.0);
          break;
        }
        if (temp > ODP_Temperature_ChargeLimits[i] && temp <= ODP_Temperature_ChargeLimits[i+1]){
          temp1 = (temp - ODP_Temperature_ChargeLimits[i]) / (ODP_Temperature_ChargeLimits[i+1] - ODP_Temperature_ChargeLimits[i]);
          temp1 = temp1 * ((int16)(ODP_Power_ChargeLimits[i+1]-ODP_Power_ChargeLimits[i])) + ODP_Power_ChargeLimits[i];
          ODV_Current_ChargeAllowed = CNV_Round(temp1 * ODP_SafetyLimits_Imax_charge / 100.0);
          break;
        }
      }
    }
    if (temp < ODP_SafetyLimits_Tmin || temp > ODP_SafetyLimits_Tmax){
      ODV_Current_DischargeAllowed = 0;
    } else {
      if (temp < ODP_Temperature_DischargeLimits[0]){
        ODV_Current_DischargeAllowed = CNV_Round(ODP_Power_DischargeLimits[0] * ODP_SafetyLimits_Imax_dis / 100.0);
      }
      else for (i=0; i<7; i++){
        if (ODP_Temperature_DischargeLimits[i] >= ODP_Temperature_DischargeLimits[i+1]) {
          ODV_Current_DischargeAllowed = CNV_Round(ODP_Power_DischargeLimits[i] * ODP_SafetyLimits_Imax_dis / 100.0);
          break;
        }
        if (temp > ODP_Temperature_DischargeLimits[i] && temp <= ODP_Temperature_DischargeLimits[i+1]){
          temp1 = (temp - ODP_Temperature_DischargeLimits[i]) / (ODP_Temperature_DischargeLimits[i+1] - ODP_Temperature_DischargeLimits[i]);
          temp1 = temp1 * ((int16)(ODP_Power_DischargeLimits[i+1]-ODP_Power_DischargeLimits[i])) + ODP_Power_DischargeLimits[i];
          ODV_Current_DischargeAllowed = CNV_Round(temp1 * ODP_SafetyLimits_Imax_dis / 100.0);
          break;
        }
      }
    }

    //ratio : 1incAD = 3300/4095*13.8/9.1/150*600/25 = 0.19553 V
    if (ODV_ErrorDsp_ErrorNumber == 0)
    {
    	ODV_Gateway_Voltage = CNV_Round(ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_LV25] * ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_LV25] * VOLT_SCALE / 1000.0); //0.25V resolution
    }
    else
    {
    	ODV_Gateway_Voltage = 0;
    }

    //ratio : HAS300 1incAD = 3300/4095*3*500/4000 = 0.1813 A; HASS200 1incAD = 3.3*200/(1.33*.625*4095) = 0.19389
    /*if (ODP_Settings_AUD_Gateway_Current == 1)
    {
    	ODV_Gateway_Current = CNV_Round(ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_HAS500] * ODP_Analogue_Input_Scaling_Float[PIC_MEASURE_HAS500] * CUR_SCALE); //0.25A resolution
    }
    else
    {*/
    int16  LEM_ADC= (int16)AdcResult.ADCRESULT5;   /// V50  25.02.2020
    ODV_Gateway_Current_Average = (LEM_ADC + ODP_Analogue_Input_Offset_Integer[PIC_MEASURE_HAS500]);
   // }

	//HAL_Current_Sum += ODV_Gateway_Current;
    if (ODV_Gateway_Current_Average > 0)
    {
    	PAR_Capacity_TotalLife_Used += ODV_Gateway_Current_Average;
    }

    if(ODV_Gateway_SOC_Calibration != 0)
    {
    	//ODV_Board_Manufactuer= 0; /// Test Johnny 29.03.2021
    	//CurrCounterSec = 0;
    	//PAR_Capacity_Left = (ODV_Gateway_SOC_Calibration) * (PAR_Capacity_Total/100);
    }
    else
    {
    	if(CurrCounterSec > 1000)
    	{
    	    //if (PAR_Capacity_Left < 500) PAR_Capacity_Left = 0;
    		CurrCounterSec = 0;
    		//HAL_Current_Sum += ODV_Gateway_Current; /// change this to Seconds  /// Johnny Testing 29.03.2021 Current measurments 1Sec

    		if ((int64)PAR_Capacity_Left + ODV_Gateway_Current < 0)
    		{
    			//PAR_Capacity_Left = 0;
    		}
    		else
    		{
    			if(ODV_Gateway_SOC_Calibration == 0)
    			{
    				if (abs(ODV_Gateway_Current) < (ODP_HybridSOC_Current*4))
    				{
    					PAR_Capacity_Left += ODP_Gateway_StandbyCurrent; /// around 55mA Standby Mode Current#
    					if (OnceRecalibrateSOC != 1)
    						{
    							OnceRecalibrateSOC  = 0;
    						}
    				}
    				else
    				{
    					PAR_Capacity_Left += ODV_Gateway_Current;
    					OnceRecalibrateSOC = 2;
    					SOC_Delay_Sec = 1;
    				}
    			}
    			else
    			{
    				//PAR_Capacity_Left = (ODV_Gateway_SOC_Calibration) * (PAR_Capacity_Total/100);
    			}
    		}
    	}
    }
    CurrCounterSec = CurrCounterSec+1;
    /*
     * Settings for Max and Min fro SOC reset
     */
    if (((ODV_Gateway_Voltage/VOLT_SCALE)  >= ODP_SafetyLimits_Umax) && (ODV_ErrorDsp_ErrorNumber == 0)&& (RelayPLUS_CLose == 5))
    {
    	if (Over_Voltage_ResetCounter > 60)
    	{
    		Over_Voltage_ResetCounter = 0;
    		if (ODV_Gateway_RelayCommand == 1) PAR_Capacity_Left = PAR_Capacity_Total;
    	}
    }
    else
    {
    	Over_Voltage_ResetCounter = 0;
    }

    if(((ODV_Gateway_Voltage/VOLT_SCALE)  <= ODP_SafetyLimits_Umin) && (ODV_ErrorDsp_ErrorNumber == 0) && (RelayPLUS_CLose == 5))
    {
    	if (Low_Voltage_ResetCounter > 60)
    	{
    		Low_Voltage_ResetCounter = 0;
    		if (ODV_Gateway_RelayCommand == 1) PAR_Capacity_Left = 0;
    	}
    }
    else
    {
    	Low_Voltage_ResetCounter = 0;
    }

    if (MMSConfig->lem == 0) ODV_Gateway_Current = 0;
    ODV_Gateway_Power = (int32)ODV_Gateway_Voltage * (int32)ODV_Gateway_Current;
    if (ODV_Gateway_Temperature > ODP_Temperature_Max){
      ODP_Temperature_Max = ODV_Gateway_Temperature;
      //PAR_StoreODSubIndex(BoardODdata,ODA_PAR_Temperature_Max);
    }
    if (ODV_Gateway_Temperature < ODP_Temperature_Min){
      ODP_Temperature_Min = ODV_Gateway_Temperature;
      //PAR_StoreODSubIndex(BoardODdata,ODA_PAR_Temperature_Min);
    }
    if (ODV_Gateway_Current > ODP_Current_Max){
      ODP_Current_Max = ODV_Gateway_Current;
      //PAR_StoreODSubIndex(BoardODdata,ODA_PAR_Current_Max);
    }
    if (ODV_Gateway_Current < ODP_Current_Min){
      ODP_Current_Min = ODV_Gateway_Current;
    }
    if (ODV_Gateway_Voltage < ODP_Voltage_Min){
      ODP_Voltage_Min = ODV_Gateway_Voltage;
    }
    if (ODV_Gateway_Voltage > ODP_Voltage_Max){
      ODP_Voltage_Max = ODV_Gateway_Voltage;
    }

   
   // if (ODV_Gateway_Current > 0) PAR_Capacity_TotalLife_Used += ODV_Gateway_Current;

    //if ((int64)PAR_Capacity_Left + ODV_Gateway_Current < 0) PAR_Capacity_Left = 0;

    if ((PAR_Capacity_Left > PAR_Capacity_Total) && (PAR_Capacity_Left < PAR_Capacity_Total + 3000))
    {
    	//PAR_Capacity_Left = PAR_Capacity_Total;
    }

    if(ODV_Gateway_RelayCommand == 0) RelayPLUS_CLose = 0;

    if (MMSConfig->SOC2 == 1)
    {
    	if ((SOC_Delay_Sec > 60) || (initializeSOC > 4 && initializeSOC < 6))
    	{
    		SOC_Delay_Sec = 1;

			if((OnceRecalibrateSOC == 0) && (ODV_ErrorDsp_ErrorNumber == 0) && ((ODV_Gateway_Voltage/VOLT_SCALE) > ODP_SafetyLimits_Umin) && (RelayPLUS_CLose == 5))
			{
				int SOC_Calibration = (100 * (long)(ODV_Gateway_Voltage - ODP_SafetyLimits_Umin*VOLT_SCALE) / ((ODP_SafetyLimits_Umax - ODP_SafetyLimits_Umin)*VOLT_SCALE));
				if (SOC_Calibration>100) SOC_Calibration=100;
				if (SOC_Calibration<0) SOC_Calibration=0;
				if(ODV_Gateway_RelayCommand == 1) PAR_Capacity_Left = (SOC_Calibration) * (PAR_Capacity_Total/100);
				OnceRecalibrateSOC = 1;
				initializeSOC = 8;
			}
    	}
    } else {
     // i = 100 * (long)(ODV_Gateway_Voltage - ODP_SafetyLimits_UnderVoltage*VOLT_SCALE) / ((ODP_SafetyLimits_OverVoltage - ODP_SafetyLimits_UnderVoltage)*VOLT_SCALE);
    }
    if (i>100) i=100;
    if (i<0) i=0;
    if (ABS(ODV_Gateway_Current) < ODP_HybridSOC_Current || (ODV_SOC_SOC2 == 0 && i>0))
    {
     ++count;
      if (count > (uint32)ODP_HybridSOC_Time*1000)
      { //if ~2s below 1A then update voltage
        count = 0;
       // ODV_SOC_SOC2 = i;
      //  HAL_Current_Sum = 0;
      }
    }
    else if (count > 0) --count;
    /// ODV_SOC_SOC2 = i; always SOC update

    if (PAR_Capacity_Total == 0) PAR_Capacity_Total = 3600*CUR_SCALE*40.6; /// 1ms to 1Sec
    ODV_SOC_SOC1 = CNV_Round(100.0 * PAR_Capacity_Left / PAR_Capacity_Total);
    i = CNV_Round(100.0 * PAR_Capacity_Left / PAR_Capacity_Total); /// + CNV_Round(100.0 * HAL_Current_Sum / PAR_Capacity_Total);
    if (i>100) i=100;
    if (i<0) i=0;

    ODV_Gateway_SOC = i; /// SOC always calculate


   /*if (HAL_TimeSOC > ODP_Gateway_EEPROM_Write) { //every 3mins
      HAL_TimeSOC = 0;
      PAR_WriteStatisticParam(-5);
	}
    ODP_Gateway_EEPROM_Write = (((126-ODV_Gateway_Voltage))- ODP_SafetyLimits_Umin);*/

    ODV_Battery_Total_Cycles = PAR_Capacity_TotalLife_Used / (PAR_Capacity_Total * 1000);
    ODV_Battery_Total_ChargedkWh = PAR_Capacity_TotalLife_Used / 3600000 / CUR_SCALE * ODP_NbOfModules * 0.0048; //kWh (Life Cycle KW) 1ms
    ODV_Battery_Capacity_Left = CNV_Round((ODV_SOC_SOC1) * ODP_Battery_Capacity / 10); //0.1Ah
    if (ODP_Battery_Cycles != 0) ODV_Gateway_SOH = 100 - (100*ODV_Battery_Total_Cycles / ODP_Battery_Cycles);
   // if (ODV_Battery_Total_Cycles > 2500) ERR_HandleWarning(WARN_CYCLE);
    if (ODV_Gateway_Current< (-1 *(ODP_HybridSOC_Current*4)))
    {
    	ODV_Battery_Time_Remaining = PAR_Capacity_Left / 60 / ABS(ODV_Gateway_Current);
    }
    else if (ODV_Gateway_Current > (ODP_HybridSOC_Current* 4))
    {
    	ODV_Battery_Time_Remaining = (PAR_Capacity_Total - PAR_Capacity_Left) / 60 / (ODV_Gateway_Current);
    }
    else
    {
    	ODV_Battery_Time_Remaining = 0;
    }
    //else ODV_Battery_Time_Remaining = 0xFFFF;

	if (ODP_Gateway_Relay_Error_Count > ODP_Settings_AUD_Gateway_Current_Ringsaver_MAX)
	{
		//ERR_HandleWarning(WARN_SLEEP);
	}
	//else ERR_ClearWarning(WARN_SLEEP);
	if (ODP_Settings_AUD_Gateway_Current_Ringsaver_MAX == 1)
	{
		ODP_Gateway_Relay_Error_Count = 0;
		PAR_WriteStatisticParam(-5);
	}

    switch (HAL_RelayState){
      //relay1
      case 0:
        start = CLK_getltime();
        RELAY_VALUE1 = RELAY_SEUILA;//switch on relay
        HAL_RelayState = 1;
        oldval = ReadCurrent1;
        break;
      case 1:
        newval = ReadCurrent1;
        if (CLK_getltime() - start >= (RELAY_TIMEOUT))
      	   {
        	 HAL_RelayState = RELAY_ERROR;
        	 if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_NEG();
      	   }
        if (oldval-newval >= RELAY_SLOPE) HAL_RelayState = 2;
      	   	oldval = newval;

	   	  /*if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
	   	  {
	   		HAL_RelayState = 2;
	   		oldval = newval;
	   	  }
	   	  else
	   	  {
	   		  if (CLK_getltime() - start >= (RELAY_TIMEOUT))
	   		  {
	   			  HAL_RelayState = RELAY_ERROR;
	   			  if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_NEG();
	   		  }
	   		  if (oldval-newval >= RELAY_SLOPE) HAL_RelayState = 2;
	   		  oldval = newval;
	   	  }*/
        break;
      case 2:
        RELAY_VALUE1 = RELAY_SEUILB;//
        HAL_RelayState = RELAY_TIMEDELAY_STATE;
        ODV_Read_Inputs_8_Bit[0] |= RELAY_BIT1;
        start = CLK_getltime();
        break;
      //relay2
      case 4:
        start = CLK_getltime();
        RELAY_VALUE2 = RELAY_SEUILA;//switch on relay
        HAL_RelayState = 5;
        oldval = ReadCurrent2;
        break;
      case 5:
        newval = ReadCurrent2;
 		  if (CLK_getltime() - start >= (RELAY_TIMEOUT))
 		  {
		   HAL_RelayState = RELAY_ERROR;
		   if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PRE();
 		  }
 		  if (oldval-newval >= RELAY_SLOPE) HAL_RelayState = 6;
 		  oldval = newval;

	   	  /*if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
	   	  {
	   		HAL_RelayState = 6;
	   		oldval = newval;
	   	  }
	   	  else
	   	  {
	   		  if (CLK_getltime() - start >= (RELAY_TIMEOUT))
	   		  {
			   HAL_RelayState = RELAY_ERROR;
			   if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PRE();
	   		  }
	   		  if (oldval-newval >= RELAY_SLOPE) HAL_RelayState = 6;
	   		  oldval = newval;
	   	  }*/
        break;
      case 6:
        RELAY_VALUE2 = RELAY_SEUILB;//
        HAL_RelayState = RELAY_TIMEDELAY_STATE;
        ODV_Read_Inputs_8_Bit[0] |= RELAY_BIT2;
        start = CLK_getltime();
        break;
      //relay3
      case 8:
        start = CLK_getltime();
        RELAY_VALUE3 = RELAY_SEUILA;//switch on relay
        if(ODP_Contactor_ControlRelay4 == 1)  RELAY_VALUE4 = RELAY_SEUILA;//switch on LED relay
        HAL_RelayState = 9;
        oldval = ReadCurrent3;

        break;
      case 9:
        newval = ReadCurrent3;
 		  if (CLK_getltime() - start >= (RELAY_TIMEOUT))
 		  {
 			  HAL_RelayState = RELAY_ERROR;
 			 if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PLUS();
 		  }
 		  if (oldval-newval >= RELAY_SLOPE) HAL_RelayState = 10;
 		  oldval = newval;

 		 if(MMSConfig->relayextra == 1)
 		 {
 			ODP_Contactor_ControlRelay4 = 1;
 		 }
 		 else
 		 {
 			ODP_Contactor_ControlRelay4 = 0;
 		 }

 		 ////////////////////////////////////////////
 		 // Initialize when Relays are Closed with CANbus
 		initializeSOC = 1;
 		OnceRecalibrateSOC = 0;
 		RelayPLUS_CLose = 5;

        break;
      case 10:
        RELAY_VALUE3 = RELAY_SEUILB;
        if(ODP_Contactor_ControlRelay4 == 1)  RELAY_VALUE4 = RELAY_SEUILB;//  LED RElay
        HAL_RelayState = RELAY_TIMEDELAY_STATE;
        ODV_Read_Inputs_8_Bit[0] |= RELAY_BIT3;
        start = CLK_getltime();
        break;

      case RELAY_STILL_STATE:
        if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT1)) && ((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT1) == 0)){
          HAL_RelayState = 0; //switch on
        }
        else if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT2)) && ((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT2) == 0)){
          HAL_RelayState = 4; //switch on
        }
        else if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT3)) && ((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT3) == 0)){
          HAL_RelayState = 8; //switch on
        }
        else{
          if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT1) == 0) && (ODV_Read_Inputs_8_Bit[0] & RELAY_BIT1)){
            RELAY_VALUE1 = 0;// switch off
            ODV_Read_Inputs_8_Bit[0] &= ~RELAY_BIT1;
          }
          if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT2) == 0) && (ODV_Read_Inputs_8_Bit[0] & RELAY_BIT2)){
            RELAY_VALUE2 = 0;// switch off
            ODV_Read_Inputs_8_Bit[0] &= ~RELAY_BIT2;
          }
          if(((ODV_Write_Outputs_16_Bit[0] & RELAY_BIT3) == 0) && (ODV_Read_Inputs_8_Bit[0] & RELAY_BIT3))
          {
            RELAY_VALUE3 = 0; // switch off
            RELAY_VALUE4 = 0; // switch off
            ODV_Read_Inputs_8_Bit[0] &= ~RELAY_BIT3;
          }
        }
        ODV_Gateway_Status = (ODV_Read_Inputs_8_Bit[0] > RELAY_BIT1);
        break;
      case RELAY_TIMEDELAY_STATE:
        if (CLK_getltime() - start >= timeout || ODV_Write_Outputs_16_Bit[0] == 0){
          HAL_RelayState = RELAY_STILL_STATE;
        }
        break;
       
      case RELAY_ERROR:
	   if (ODP_Settings_AUD_Active_Deactive_Relay == 1)	
    	  {
		/// Johnny Test
		if (((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT1) && ReadCurrent1 >= ODP_Contactor_Overcurrent))
			{
			if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_NEG();
			}  
		if (((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT2) && ReadCurrent2 >= ODP_Contactor_Overcurrent))
			{
			if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PRE();
			}
		if (((ODV_Read_Inputs_8_Bit[0] & RELAY_BIT3) && ReadCurrent3 >= ODP_Contactor_Overcurrent))
			{
			if (ODV_ErrorDsp_ErrorNumber == 0) ERR_CONTACTOR_PLUS();
			}
		  }
		if (ODP_Settings_AUD_Active_Deactive_Relay == 1)
		{
        //switch off relays
        RELAY_VALUE1 = 0;
        RELAY_VALUE2 = 0;
        RELAY_VALUE3 = 0;
        RELAY_VALUE4 = 0;
        ODV_Write_Outputs_16_Bit[0] = 0;
        ODV_Read_Inputs_8_Bit[0] = 0;  
        HAL_RelayState = RELAY_STILL_STATE;
		}

		/*if (cur < ODP_Settings_AUD_Gateway_Current_Ringsaver)
		{
			ODP_Gateway_Relay_Error_Count++;
			PAR_WriteStatisticParam(-5);
		}*/

        break;
      default:
        break;
    }

    TSK_sleep(1);
  }

}

const uint16 permtable[11] = {1,3,5,7,11,13,17,19,23,29,31};
#define permutebit(val,val2,bit) (((val2 & bit)>0) ? (val |= (uint32)bit) : (val &= ~(uint32)bit))

void HAL_Unlock(void)
{
  uint32 num, pass, num2;
  uint16 i;
  num = ODV_Security & 0xFFFFFFFF;
  num2 = num;
  pass = (ODV_Security>>32) & 0xFFFFFFFF;
  for (i=0; i<11; i++) {
    permutebit(num,pass,((uint32)1<<permtable[i]));
    permutebit(pass,num2,((uint32)1<<permtable[i]));
  }
  if (num == ODP_RandomNB) {
    ODP_Password += pass;
    if (pass == 0){ //reset counter
      ODP_Password = ODP_OnTime;
    }
    ODP_RandomNB = 0;
    PAR_StoreODSubIndex(BoardODdata,ODA_PAR_Password);
    PAR_StoreODSubIndex(BoardODdata,ODA_PAR_RandomNB);
    ERR_ClearWarnings();
  }
  ODV_Security = 0;
}

void HAL_Random(void)
{
  /* use new seed */
  srand(ODP_OnTime & 0xFFFF);
  ODP_RandomNB = (65535.0*rand()/(RAND_MAX+1.0)); //genere nb aleatoire entre 0 et 65535
  srand(((ODP_OnTime>>16) + (ODV_SysTick_ms>>8)) & 0xFFFF);
  ODP_RandomNB += ((UNS32)(65535.0*rand()/(RAND_MAX+1.0))<<16); //genere nb aleatoire entre 0 et 65535
  PAR_StoreODSubIndex(BoardODdata,ODA_PAR_RandomNB);
}

interrupt void HAL_Dummy(void)
{
  AcknowlegeInt = 0xFFF;   // Acknowledge interrupt to PIE
}

void HAL_Reset(void){
  DINT;
  EALLOW;
  AdcRegs.INTSEL9N10.bit.INT9E = 0;  /* INT9 enable */
  AdcRegs.INTSEL1N2.bit.INT2E  = 0;  /* INT2 enable */

  PieVectTable.I2CINT1A = &HAL_Dummy;
  PieVectTable.ADCINT2 = &HAL_Dummy;
  PieVectTable.ADCINT9 = &HAL_Dummy;
  PieVectTable.EQEP1_INT = &HAL_Dummy;
  PieVectTable.ECAN1INTA = &HAL_Dummy;
  PieVectTable.ECAN0INTA = &HAL_Dummy;
  PieVectTable.USB0_INT = &HAL_Dummy;

  EQep1Regs.QEINT.bit.UTO = 0;      // Disable unit timer interrupt
  EDIS;
  EINT;
  while(PieCtrlRegs.PIEIFR1.all!=0);
  PieCtrlRegs.PIEIER1.all = 0;
  while(PieCtrlRegs.PIEIFR5.all!=0);
  PieCtrlRegs.PIEIER5.all = 0;
  while(PieCtrlRegs.PIEIFR8.all!=0);
  PieCtrlRegs.PIEIER8.all = 0;
  while(PieCtrlRegs.PIEIFR9.all!=0);
  PieCtrlRegs.PIEIER9.all = 0;

  PieCtrlRegs.PIEACK.all = 0xFFFF;
  PieCtrlRegs.PIEIER1.bit.INTx6 = 0;
  //
  IER = 0;
}
