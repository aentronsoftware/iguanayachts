 /**********************************************************************

   sci1.c - sci1 hardware description file

 ----------------------------------------------------------------------
   Author: PTM: RP/OS                     Date: Wed 09.07.2014
   For   : SMTEC SA
   Compil: CCSv5     Target: DSP2806x
   descr.: define hardware constants for C and assembler
   Edit  :
 ******************************************************************************/

#include "uc.h"
#include "dspspecs.h"
#include "ngtestspec.h"
#include "gateway_dict.h"
#include "sci1.h"
#include "hal.h"
#include "error.h"


//Definition pour la communication SCI entre processeurs
#define SCI_START_FLAG 0xA8
#define SCI_END_FLAG   0xA8
#define SCI_CTRL       0xA9
#define SCI_STUFFING   0xDF
#define SCI_DESTUFFING 0x20

#if UC_PHI == 80000000   //TI F2806x LOSPCP=2
const T_BaudCfg BaudTable[MAXBAUD]=
 {{9600,1,4},{19200,0,129},{38400,0,64},{57600,0,42},{115200,0,21}
 ,{230400,0,10},{250000,0,9},{500000,0,4}
 };
#else
//  ERROR : no baud rate table defined for your UC_PHI
const T_BaudCfg BaudTable[MAXBAUD]=
 {{9600,1,4},{19200,0,129},{38400,0,64},{57600,0,42},{115200,0,21}
 ,{230400,0,10},{250000,0,9},{500000,0,4}
 };
#endif

uint16 RS232RecvState = STATE_RX_IDLE;  /* Reset state machine*/
uint16 SCI1_CheckSum = 0;
uint16 RS232Timer = 0;
uint16 SCI_CharLen;
uint16 *SCI_CharPtr;
T_UsbMessage SCI_MsgInRS232;
bool1 ByteStuffed = FALSE;
bool1 ComStarted = FALSE;
bool1 SCI_MsgAvailable = FALSE;
volatile struct SCI_REGS* SciRegs;

void SCI1_InitGpio(void)
{
//EN FAIT 068
#ifdef PTM068
  EALLOW;
  GpioCtrlRegs.GPAPUD.bit.GPIO22 = 0;
  GpioCtrlRegs.GPAPUD.bit.GPIO23 = 0;
  GpioCtrlRegs.GPAQSEL2.bit.GPIO23 = 3;
  GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 3;
  GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 3;
  EDIS;
#endif
}


void SCI1_RS232Init(void)
{
  uint16 baudindex = BAUD_9600;

  EALLOW;
#ifdef PTM068
  SciRegs = &ScibRegs;
  PieVectTable.SCIRXINTB = &RS232_RXI_Int;
  //PieVectTable.SCITXINTB = &RS232_TEI_Int;
#endif
  SysCtrlRegs.LOSPCP.all = LOW_CLOCK_PRESCALER; //already in device init put again just to be sure
  EDIS;

  SciRegs->SCICTL1.all =0x0000; //disable sci
  SciRegs->SCIFFTX.all=0x4040; //SCIFFENA(b14), Reset the FIFO pointer to 0 and hold in reset (, Clear TXFIFO interrupt (b6)
  SciRegs->SCIFFRX.all=0x404F; //clear overflow, Reset the FIFO pointer to zero, Clear RXFFINT (b6)

  /* sci */
  //set mode N81 and baudrate
  SciRegs->SCICCR.all = SCI_N81;  //N81
  if (baudindex >= MAXBAUD) baudindex = 0;
  SciRegs->SCIHBAUD = BaudTable[baudindex].n;
  SciRegs->SCILBAUD = BaudTable[baudindex].brr;
  //initalize the SCI FIFO p2.17,p2.18,p2.19
  SciRegs->SCIFFTX.all=0xE000; // (/reset)b15, SCIFFENA(b14), /FIFOreset(b13)
  SciRegs->SCIFFRX.all=0x2021; //(b13)/fiforeset, fifo int enable(b5), Set RXFFIL to 1 00001(b0-4)
  SciRegs->SCIFFCT.all=0x0;    //No delay between every transfer from FIFO
  //enable the interrupt
#ifdef PTM068
  PieCtrlRegs.PIEIER9.bit.INTx3 = 1; //enable scibrx int
  //PieCtrlRegs.PIEIER9.bit.INTx4 = 1; //enable scibtx int
#endif
  SciRegs->SCICTL1.all = 0x0063; //enable error int, transmitter and receiver
  RS232RecvState = STATE_RX_IDLE;  /* Reset state machine*/
  IER |= M_INT9;
}

void RS232ReceiveEnable(void)
{
  //disable
  SciRegs->SCIFFRX.bit.RXFFIENA = 0;    /*| Disable rx ints  */
  SciRegs->SCICTL1.bit.RXERRINTENA = 0; /*| */
  SciRegs->SCICTL1.bit.RXENA = 0;       /*| Disable rx  */
  SciRegs->SCICTL1.bit.TXENA = 0;       /*| Disable tx  */
  SciRegs->SCICTL1.bit.SWRESET = 0;     /*| Set and hold in reset sci */
  SciRegs->SCIFFTX.bit.TXFIFOXRESET = 0;/*| Set /reset tx fifo */
  SciRegs->SCIFFRX.bit.RXFIFORESET = 0; /*| Set /reset rx fifo */
  SciRegs->SCICTL1.bit.SWRESET = 1;     /*| Release reset */
  SciRegs->SCIFFTX.bit.TXFIFOXRESET = 1;/*| Release tx fifo reset */
  SciRegs->SCIFFRX.bit.RXFIFORESET = 1; /*| Release rx fifo reset */
  //enable
  SciRegs->SCIFFRX.bit.RXFFIL = 1;      /*| set receive fifo int to 1 char*/
  SciRegs->SCICTL1.bit.TXENA = 1;       /*| Enable tx */
  SciRegs->SCICTL1.bit.RXENA = 1;       /*| Enable RX     */
  SciRegs->SCICTL1.bit.RXERRINTENA = 1; /*| Enable rx err int */
  SciRegs->SCIFFRX.bit.RXFFIENA = 1;    /*| Enable rx int   */
  RS232RecvState = STATE_RX_IDLE;/* Reset state machine */
  ByteStuffed = FALSE;
}


void RS232ReceiveDisable(void)
{ //disable
  SciRegs->SCIFFRX.bit.RXFFIENA = 0;    /*| Disable rx ints */
  SciRegs->SCICTL1.bit.RXERRINTENA = 0; /*| */
  SciRegs->SCICTL1.bit.RXENA = 0;       /*| Disable rx  */
  SciRegs->SCICTL1.bit.TXENA = 0;       /*| Disable tx  */
}

void SCI1_Update(void){//called by millisecint
  if (RS232Timer){
    if (--RS232Timer == 0) //timeout
      RS232ReceiveEnable(); //reset
  }
}

uint16 GetWord(uint16 index){
  if (index < MAX_DATA/2){
    SCI_MsgInRS232.len -= 2;
    return (SCI_MsgInRS232.data[index*2]*256 + SCI_MsgInRS232.data[index*2+1]);
  }
  else
    return 0;
}

void SetWord(uint16 index, uint16 data){
  if (index < MAX_DATA/2){
    SCI_MsgInRS232.data[index*2] = data>>8;
    SCI_MsgInRS232.data[index*2+1] = data & 0xFF;
    SCI_MsgInRS232.len += 2;
  }
}

void SCI1_Command(void){//called by task
  uint16 i, start, len, data;
  uint32 size;
  uint32 *pdata;
  uint8 type;
  switch (SCI_MsgInRS232.cmd){
    case CMD_ERROR:
      SCI_MsgInRS232.len = 0;
    break;
    case CMD_READ_STATE:
      SCI_MsgInRS232.len = 0;
      SetWord(0, ODV_ErrorDsp_ErrorNumber);
      SetWord(1, ODV_ErrorDsp_WarningNumber);
      SetWord(2, FIRMWARE_MODE);
      SetWord(3, BoardODdata->nodeState);
    break;
    case CMD_READ_PARAM:
      start = GetWord(0);
      len = GetWord(1);
      SCI_MsgInRS232.len = 0;
      if ((start > PARAM_MAX)||(len > MAX_PACKET_SIZEW)||(start+len > PARAM_MAX)){
        SCI_MsgInRS232.cmd = CMD_ERROR;
        ODV_ErrorDsp_WarningNumber = WARN_RANGE;
        break;
      }
      for (i=0;i<len;i++){
        size = 0;
        _getODentry(BoardODdata,PARAM_INDEX_START+start+i,0,&data,&size,&type,0,0,GET_OD_ENTRY_MODE_3);
        SetWord(i,data);
      }
    break;
    case CMD_WRITE_PARAM:
      start = GetWord(0);
      len = GetWord(1);
      if ((start > PARAM_MAX)||(len > MAX_PACKET_SIZEW)||(start+len > PARAM_MAX)){
        SCI_MsgInRS232.len = 0;
        SCI_MsgInRS232.cmd = CMD_ERROR;
        ODV_ErrorDsp_WarningNumber = WARN_RANGE;
        break;
      }
      for (i=0;i<len;i++){
        size = 0;
        data = GetWord(i+2);
        _setODentry(BoardODdata,PARAM_INDEX_START+start+i,0,&data,&size,0,0,SET_OD_ENTRY_MODE_3);
      }
      SCI_MsgInRS232.len = 0;
    break;
    case CMD_READ_DICT:
      start = GetWord(0);
      len = GetWord(1);
      SCI_MsgInRS232.len = 0;
      size = 0;
      _getODentry(BoardODdata,start,len,(void *)&pdata,&size,&type,0,0,GET_OD_ENTRY_MODE_1);
      SetWord(0,type*256+size);
      if (size <= 2){
        SetWord(1,*pdata);
      }
      else {
        SetWord(1,(*pdata)>>16); SetWord(2,*pdata);
      }
    break;
    case CMD_WRITE_DICT:
      start = GetWord(0);
      len = GetWord(1);
      size = SCI_MsgInRS232.len - 2;
      if (size <= 2){
        *pdata = GetWord(2);
      }
      else{
        *pdata = GetWord(2)*256 + GetWord(3);
      }
      _setODentry(BoardODdata,start,len,(void*)&pdata,&size,0,0,SET_OD_ENTRY_MODE_3);
    break;
    default:
      SCI_MsgInRS232.cmd = CMD_ERROR;
      SCI_MsgInRS232.len = 0;
    break;
  }
  SCI1_SendMessage(&SCI_MsgInRS232);
}

#define SCI_FIFO_WORD_MAX 4

void Put_Sci1(uint8 ch){
  uint16 fifo_len = SciRegs->SCIFFTX.bit.TXFFST;
  while(fifo_len >= SCI_FIFO_WORD_MAX){
    fifo_len = SciRegs->SCIFFTX.bit.TXFFST;
  }
  SciRegs->SCITXBUF = ch;
}

uint8 Get_Sci1(void){
  uint16 fifo_len = SciRegs->SCIFFRX.bit.RXFFST;
  uint8 ch;
  while(fifo_len == 0){
    fifo_len = SciRegs->SCIFFTX.bit.TXFFST;
  }
  ch = SciRegs->SCIRXBUF.bit.RXDT;
  if (ByteStuffed) ch |= SCI_DESTUFFING;
  return (ch);
}

void Write_Sci1(uint8 ch){
  if ((ch == SCI_START_FLAG)||(ch == SCI_CTRL)||(ch == SCI_END_FLAG)){
    Put_Sci1(SCI_CTRL);
    Put_Sci1(ch & SCI_STUFFING);
  }
  else{
    Put_Sci1(ch);
  }
  SCI1_CheckSum += ch;
}

void SCI1_SendMessage(T_UsbMessage *msg){
  uint16 len, cks;
  uint8 i;
  
  len =  msg->len;
  SCI1_CheckSum = 0;
  Put_Sci1(SCI_START_FLAG);
  Write_Sci1(msg->cmd);
  for (i=0; i<len; i++) {
    Write_Sci1(msg->data[i]);
  }
  cks = SCI1_CheckSum;
  Write_Sci1(cks / 0x100);
  Write_Sci1(cks & 0xFF);
  Put_Sci1(SCI_END_FLAG);
}


#if 1
interrupt void RS232_RXI_Int(void)
/* Receive interrupt : triggered by receive data register full (SSR.RDRF) */
{
  uint8 incar;

  incar = SciRegs->SCIRXBUF.bit.RXDT;  /* Read received char */
  SciRegs->SCIFFRX.bit.RXFFOVRCLR = 1;//clear overflow
  SciRegs->SCIFFRX.bit.RXFFINTCLR = 1;//Reset interrupt flag
  AcknowlegeInt |= M_INT9;

  if (SciRegs->SCIRXST.bit.RXERROR)
  { //A reception error occured -> Reset the communication
    RS232ReceiveEnable();
    return;
  }
  if(!MBX_post(&sci_rx_mbox, &incar, 0)){

  }
}



#else
interrupt void RS232_RXI_Int(void)
/* Receive interrupt : triggered by receive data register full (SSR.RDRF) */
{
  uint8 incar;
  int16 n;
  uint16 cks = 0, len;

  incar = SciRegs->SCIRXBUF.bit.RXDT;  /* Read received char */
  SciRegs->SCIFFRX.bit.RXFFOVRCLR = 1;//clear overflow
  SciRegs->SCIFFRX.bit.RXFFINTCLR = 1;//Reset interrupt flag
  AcknowlegeInt |= M_INT9;

  if (SciRegs->SCIRXST.bit.RXERROR)
  { //A reception error occured -> Reset the communication
    RS232ReceiveEnable();
    return;
  }
  if (incar == SCI_START_FLAG)
  {
    if (RS232RecvState != STATE_RX_WAIT_DATA_TO_CRC){
      RS232Timer = RS_232_TIMEOUT;  /* start msg receive timeout */
      /* set receiving and timeout flags */
      RS232RecvState = STATE_RX_WAIT_CMD;
      return;
    }
    else{
      RS232Timer = 0;
      RS232RecvState = STATE_RX_CKS;
    }
  }
  if (incar == SCI_CTRL)
  {
    ByteStuffed = TRUE;
    return;
  }
  if (ByteStuffed){
    ByteStuffed = FALSE;
    incar |= SCI_DESTUFFING;
  }
  switch (RS232RecvState)
  {
    case STATE_RX_IDLE:
    break;

    case STATE_RX_WAIT_CMD:
    ComStarted = TRUE;
    SCI_MsgInRS232.cmd = incar;
    SCI_MsgInRS232.len = 0;
    SCI_CharPtr = (uint16*)&SCI_MsgInRS232.data;
    RS232RecvState = STATE_RX_WAIT_DATA_TO_CRC;
    break;

    case STATE_RX_WAIT_DATA_TO_CRC:
    SCI_MsgInRS232.len += 1;
    *SCI_CharPtr++ = (uint16)incar;
    break;

    case STATE_RX_CKS:
    ComStarted = FALSE;
    cks = SCI_MsgInRS232.cmd;
    len = SCI_MsgInRS232.len;
    if (len >= 2) len -= 2;
    for (n=0; n<len; n++){
      cks += SCI_MsgInRS232.data[n];
    }
    if (cks == (((uint16)SCI_MsgInRS232.data[len]<<8) + SCI_MsgInRS232.data[len+1]))
    { // cks OK
      SCI_MsgAvailable = TRUE;
    }
    else
    { /* BAD cks */
      SCI_MsgInRS232.cmd = CMD_ERROR;
      SCI_MsgAvailable = TRUE;
    }
    RS232RecvState = STATE_RX_IDLE;
    break;

    default:
    break;
  }/*switch*/
}/*end RS232_RXI_Int*/
#endif

interrupt void RS232_TEI_Int(void)
{
  SciRegs->SCIFFTX.bit.TXFFINTCLR = 1;//reset scibtx int
  AcknowlegeInt |= M_INT9;
}


