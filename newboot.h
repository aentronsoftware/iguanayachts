
#ifndef _BOOT_
#define _BOOT_

#define ROM_VERSION 37

#ifdef REV_47_1A
  #define LED_ON  1
  #define LED_OFF 0
  #define FLA_CRC_OK     0
  #define FLA_CRC_BAD    1
  #include "Flash280x_API_Library.h"
  Uint32 JumpAddr = 0x3FFB50;	   // Jump adress when finish loading
  Uint32 ReturnAddr(void) { return JumpAddr;}
#endif

/*#if WATCHDOG != 2
  ERROR : #define WATCHDOG must be 2 (hardware.h)
#endif*/
  
/* FLASH DOWNLOAD protocol */
#define tECHO      0xF0    /* echo the received message */
#define tREAD_BLK  0xF1    /* read a block of memory */
#define tCHK_CRC   0xF2    /* check CRC-CCITT of memory area */
#define tF_PROG    0xF3    /* programm an area of flash with blankcheck */
#define tF_RESET   0xF4    /* reset the cpu -> restart */
#define tF_NOP     0xF5    /* do nothing */
#define tROM_VER   0xF6    /* return the rom version */
#define tBAUDRATE  0xF7    /* change the baudrate */
#define tDETECT    0xF8    /* NETCONF: detect an unconfigured slave in a net */
#define tUNCONFIG  0xF9    /* NETCONF: unconfigure slave */
#define tCONFIG    0xFA    /* NETCONF: master offers a new address */
#define tSET_ADDR  0xFB    /* NETCONF: set a new address  */
#define tBLINK     0xFC    /* set the error led on or off */
#define tCONFIRM   0xFD    /* NETCONF: new V3.7 master confirms detect/config */
#define tPROG_CRYP 0xFE	   /* Decrypt received data and programm a C28 area of flash */

#define tERASE	   0xD0	   /* Erase C28 Flash memory */
#define tCHK_ERASE 0xD1	   /* Check if a block of C28 Flash memory is Erase */
#define tDSP_NUM   0xD2	   /* Return DSP number 2808, 2806 ... */
#define tSTART_ADD 0xD3    /* Send the restart address */
#define tCHK_VERS  0xD4	   /* Check Flash API Version */

/* HARDWARE SELF TEST commands */
#define TST_RAM       0xE0
#define TST_FLASH     0xE1
#define TST_SETPIN    0xE2
#define TST_GETPIN    0xE3
#define TST_PIN0      0xE4
#define TST_PIN1      0xE5
#define TST_SCI0      0xE6
#define TST_CUROFFSET 0xE7
#define TST_CURRENT   0xE8
#define TST_ENCODER   0xE9
#define TST_NMI       0xEA
#define TST_ADDRESS   0xEB  /* BCD switches on MIP-WDC only */

/* WARNING : the header is in INTEL (little endian) format ! */
typedef struct{
    uint16 activity_flag;    /* active if number of nul bits is odd */
    uint16 crc;      /* crc of code */
    uint32 execaddr;    /* execution address */
    uint32 size;      /* size of code */
    uint32 ftime;           /* date and time of code file (encoded) */
    uint32 loadtime;    /* date and time of download (encoded) */
    char *fname;     /* file name (turbo pascal string !) */
}THeader;

void RS232_DownloadSupport(Bool received);

#endif

