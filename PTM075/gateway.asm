;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jun 29 19:41:31 2020                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../gateway.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM075")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestCurrentEnable+0,32
	.bits	0,16			; _TestCurrentEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Oldmaxtemp+0,32
	.bits	0,16			; _Oldmaxtemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_InitOK+0,32
	.bits	0,16			; _InitOK @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UnderCurrent+0,32
	.bits	0,16			; _UnderCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_OldGAT_max+0,32
	.bits	0,16			; _OldGAT_max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Delay_Error+0,32
	.bits	0,16			; _Delay_Error @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Oldmintemp+0,32
	.bits	0,16			; _Oldmintemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_OldGAT_min+0,32
	.bits	0,16			; _OldGAT_min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Modules_Present+0,32
	.bits	0,16			; _Modules_Present @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ReceiveNew+0,32
	.bits	0,16			; _ReceiveNew @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_GAT_min+0,32
	.bits	65535,16			; _GAT_min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Sync_count+0,32
	.bits	70,16			; _Sync_count @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_make10ms+0,32
	.bits	0,16			; _make10ms @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestPositionEnable+0,32
	.bits	1,16			; _TestPositionEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SequenceRunning+0,32
	.bits	0,16			; _SequenceRunning @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Heater_status+0,32
	.bits	0,16			; _Heater_status @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_GAT_max+0,32
	.bits	0,16			; _GAT_max @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BootCommand+0,32
	.bits	0,32			; _BootCommand @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BootNodeId+0,32
	.bits	99,32			; _BootNodeId @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_current_counter+0,32
	.bits	0,32			; _GV_current_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_voltage_counter+0,32
	.bits	0,32			; _GV_voltage_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Temp_counter+0,32
	.bits	0,32			; _GV_Temp_counter @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Unlock")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_USB_Unlock")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("genCRC32Table")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_genCRC32Table")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverVoltage")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external

$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverTemp")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorUnderVoltage")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_SetError")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_ERR_SetError")
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$10


$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_DELTA_Voltage")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ERR_DELTA_Voltage")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_Init")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ADS_Init")
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Random")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_HAL_Random")
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Init")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_HAL_Init")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external

$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Unlock")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_HAL_Unlock")
	.dwattr $C$DW$16, DW_AT_declaration
	.dwattr $C$DW$16, DW_AT_external

$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$96)
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$17


$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Reset")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_HAL_Reset")
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_StartRecorder")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_REC_StartRecorder")
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_SetNodeId")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_DIC_SetNodeId")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$22


$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverCurrent")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$25


$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$27


$C$DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("canInit")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_canInit")
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$29


$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Stop")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_USB_Stop")
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_WarningMin")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODP_Temperature_WarningMin")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_WarningMax")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODP_Temperature_WarningMax")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_TempHeater_OFF_Max")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ODP_Settings_AUD_TempHeater_OFF_Max")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Temperature_Delay")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Temperature_Delay")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Heater_Voltage_OFF")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Heater_Voltage_OFF")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Safety_Low_Voltage_Delay")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Safety_Low_Voltage_Delay")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Temp_Heater_ON_Min")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Temp_Heater_ON_Min")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ODP_NbOfModules")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ODP_NbOfModules")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$47, DW_AT_declaration
	.dwattr $C$DW$47, DW_AT_external
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_C_D_Mode")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ODP_Current_C_D_Mode")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Test_Voltage")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_Gateway_Test_Voltage")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Heater_Status")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODV_Gateway_Heater_Status")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxModTemp")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ODV_Gateway_MaxModTemp")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Gateway_Delay_Relay_Error")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ODP_Gateway_Delay_Relay_Error")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Gateway_IsoResistor_Limit_Max")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ODP_Gateway_IsoResistor_Limit_Max")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$56, DW_AT_declaration
	.dwattr $C$DW$56, DW_AT_external
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$57, DW_AT_declaration
	.dwattr $C$DW$57, DW_AT_external
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$58, DW_AT_declaration
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
	.global	_TestCurrentEnable
_TestCurrentEnable:	.usect	".ebss",1,1,0
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("TestCurrentEnable")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_TestCurrentEnable")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_addr _TestCurrentEnable]
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$65, DW_AT_external
	.global	_Oldmaxtemp
_Oldmaxtemp:	.usect	".ebss",1,1,0
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("Oldmaxtemp")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_Oldmaxtemp")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_addr _Oldmaxtemp]
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$66, DW_AT_external
	.global	_InitOK
_InitOK:	.usect	".ebss",1,1,0
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("InitOK")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_InitOK")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_addr _InitOK]
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$67, DW_AT_external
	.global	_UnderCurrent
_UnderCurrent:	.usect	".ebss",1,1,0
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("UnderCurrent")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_UnderCurrent")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_addr _UnderCurrent]
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_external
	.global	_OldGAT_max
_OldGAT_max:	.usect	".ebss",1,1,0
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("OldGAT_max")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_OldGAT_max")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_addr _OldGAT_max]
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$69, DW_AT_external
	.global	_Delay_Error
_Delay_Error:	.usect	".ebss",1,1,0
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("Delay_Error")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_Delay_Error")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_addr _Delay_Error]
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$70, DW_AT_external
	.global	_Oldmintemp
_Oldmintemp:	.usect	".ebss",1,1,0
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("Oldmintemp")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_Oldmintemp")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_addr _Oldmintemp]
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$71, DW_AT_external
	.global	_OldGAT_min
_OldGAT_min:	.usect	".ebss",1,1,0
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("OldGAT_min")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_OldGAT_min")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_addr _OldGAT_min]
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$72, DW_AT_external
	.global	_Modules_Present
_Modules_Present:	.usect	".ebss",1,1,0
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("Modules_Present")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_Modules_Present")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_addr _Modules_Present]
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$73, DW_AT_external
	.global	_ReceiveNew
_ReceiveNew:	.usect	".ebss",1,1,0
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("ReceiveNew")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ReceiveNew")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_addr _ReceiveNew]
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$74, DW_AT_external
	.global	_GAT_min
_GAT_min:	.usect	".ebss",1,1,0
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("GAT_min")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_GAT_min")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_addr _GAT_min]
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$75, DW_AT_external
	.global	_Sync_count
_Sync_count:	.usect	".ebss",1,1,0
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("Sync_count")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_Sync_count")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_addr _Sync_count]
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$76, DW_AT_external
	.global	_make10ms
_make10ms:	.usect	".ebss",1,1,0
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("make10ms")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_make10ms")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_addr _make10ms]
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$77, DW_AT_external
	.global	_TestPositionEnable
_TestPositionEnable:	.usect	".ebss",1,1,0
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("TestPositionEnable")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_TestPositionEnable")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_addr _TestPositionEnable]
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$78, DW_AT_external
	.global	_SequenceRunning
_SequenceRunning:	.usect	".ebss",1,1,0
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("SequenceRunning")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_SequenceRunning")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_addr _SequenceRunning]
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$79, DW_AT_external
	.global	_Heater_status
_Heater_status:	.usect	".ebss",1,1,0
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("Heater_status")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_Heater_status")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _Heater_status]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_external
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Config")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_ODP_Board_Config")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$81, DW_AT_declaration
	.dwattr $C$DW$81, DW_AT_external
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$82, DW_AT_declaration
	.dwattr $C$DW$82, DW_AT_external
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Sleep_Current")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_ODP_Sleep_Current")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$83, DW_AT_declaration
	.dwattr $C$DW$83, DW_AT_external
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$85, DW_AT_declaration
	.dwattr $C$DW$85, DW_AT_external
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RelayResetTime")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_ODP_RelayResetTime")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$86, DW_AT_declaration
	.dwattr $C$DW$86, DW_AT_external
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$87, DW_AT_declaration
	.dwattr $C$DW$87, DW_AT_external
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineEvent")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_ODV_MachineEvent")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$88, DW_AT_declaration
	.dwattr $C$DW$88, DW_AT_external
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$89, DW_AT_declaration
	.dwattr $C$DW$89, DW_AT_external
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$91, DW_AT_declaration
	.dwattr $C$DW$91, DW_AT_external
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("HAL_NewCurPoint")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_HAL_NewCurPoint")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_declaration
	.dwattr $C$DW$92, DW_AT_external
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Version")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_ODV_Version")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_external
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Sleep_Timeout")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_ODP_Sleep_Timeout")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$94, DW_AT_declaration
	.dwattr $C$DW$94, DW_AT_external
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Debug")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_ODV_Debug")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$95, DW_AT_declaration
	.dwattr $C$DW$95, DW_AT_external
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SOC_SOC2")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_ODV_SOC_SOC2")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_declaration
	.dwattr $C$DW$96, DW_AT_external

$C$DW$97	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$97, DW_AT_declaration
	.dwattr $C$DW$97, DW_AT_external
$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$96)
$C$DW$99	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$93)
	.dwendtag $C$DW$97


$C$DW$100	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$100, DW_AT_declaration
	.dwattr $C$DW$100, DW_AT_external
$C$DW$101	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$157)
$C$DW$102	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$164)
$C$DW$103	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$100


$C$DW$104	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddMultiUnits")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$104, DW_AT_declaration
	.dwattr $C$DW$104, DW_AT_external
$C$DW$105	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$104

$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Multiunits")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_ODV_Recorder_Multiunits")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$106, DW_AT_declaration
	.dwattr $C$DW$106, DW_AT_external
$C$DW$107	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Variables")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_ODV_Recorder_Variables")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$107, DW_AT_declaration
	.dwattr $C$DW$107, DW_AT_external

$C$DW$108	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddVariables")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_PAR_AddVariables")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$108, DW_AT_declaration
	.dwattr $C$DW$108, DW_AT_external
$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$108


$C$DW$110	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$110, DW_AT_declaration
	.dwattr $C$DW$110, DW_AT_external
$C$DW$111	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$157)
$C$DW$112	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$164)
$C$DW$113	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$110


$C$DW$114	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Start")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_USB_Start")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$114, DW_AT_declaration
	.dwattr $C$DW$114, DW_AT_external

$C$DW$115	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$115, DW_AT_declaration
	.dwattr $C$DW$115, DW_AT_external
$C$DW$116	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$115


$C$DW$117	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarnings")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_ERR_ClearWarnings")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$117, DW_AT_declaration
	.dwattr $C$DW$117, DW_AT_external

$C$DW$118	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearError")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_ERR_ClearError")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$118, DW_AT_declaration
	.dwattr $C$DW$118, DW_AT_external

$C$DW$119	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$119, DW_AT_declaration
	.dwattr $C$DW$119, DW_AT_external
$C$DW$120	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$9)
$C$DW$121	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$51)
$C$DW$122	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$9)
$C$DW$123	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$119


$C$DW$124	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_external
$C$DW$125	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$96)
	.dwendtag $C$DW$124

$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$126, DW_AT_declaration
	.dwattr $C$DW$126, DW_AT_external
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_declaration
	.dwattr $C$DW$127, DW_AT_external

$C$DW$128	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$128, DW_AT_declaration
	.dwattr $C$DW$128, DW_AT_external
$C$DW$129	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$163)
$C$DW$130	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$49)
	.dwendtag $C$DW$128

$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$131, DW_AT_declaration
	.dwattr $C$DW$131, DW_AT_external
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_LogNB")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_ODV_Gateway_LogNB")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_declaration
	.dwattr $C$DW$132, DW_AT_external
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$133, DW_AT_declaration
	.dwattr $C$DW$133, DW_AT_external
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_declaration
	.dwattr $C$DW$134, DW_AT_external
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinModTemp")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_ODV_Gateway_MinModTemp")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$135, DW_AT_declaration
	.dwattr $C$DW$135, DW_AT_external
	.global	_GAT_max
_GAT_max:	.usect	".ebss",1,1,0
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("GAT_max")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_GAT_max")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_addr _GAT_max]
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$136, DW_AT_external
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Current")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_ODV_Gateway_Current")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$137, DW_AT_declaration
	.dwattr $C$DW$137, DW_AT_external
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Voltage")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_ODV_Gateway_Voltage")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$138, DW_AT_declaration
	.dwattr $C$DW$138, DW_AT_external
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Temperature")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_ODV_Gateway_Temperature")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$139, DW_AT_declaration
	.dwattr $C$DW$139, DW_AT_external
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$140, DW_AT_declaration
	.dwattr $C$DW$140, DW_AT_external
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$141, DW_AT_declaration
	.dwattr $C$DW$141, DW_AT_external
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ResetHW")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_ODV_ResetHW")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$142, DW_AT_declaration
	.dwattr $C$DW$142, DW_AT_external
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("ODV_StoreParameters")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_ODV_StoreParameters")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$143, DW_AT_declaration
	.dwattr $C$DW$143, DW_AT_external
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$144, DW_AT_declaration
	.dwattr $C$DW$144, DW_AT_external
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RestoreDefaultParameters")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_ODV_RestoreDefaultParameters")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$145, DW_AT_declaration
	.dwattr $C$DW$145, DW_AT_external

$C$DW$146	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$146, DW_AT_declaration
	.dwattr $C$DW$146, DW_AT_external
$C$DW$147	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$96)
$C$DW$148	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$9)
$C$DW$149	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$146


$C$DW$150	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$150, DW_AT_declaration
	.dwattr $C$DW$150, DW_AT_external
$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$150


$C$DW$152	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$152, DW_AT_declaration
	.dwattr $C$DW$152, DW_AT_external
$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$96)
	.dwendtag $C$DW$152

	.global	_BoardODdata
_BoardODdata:	.usect	".ebss",2,1,1
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_addr _BoardODdata]
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$154, DW_AT_external
	.global	_BootCommand
_BootCommand:	.usect	"BootCommand",2,1,1
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("BootCommand")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_BootCommand")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_addr _BootCommand]
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$155, DW_AT_external
	.global	_BootNodeId
_BootNodeId:	.usect	"BootCommand",2,1,1
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("BootNodeId")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_BootNodeId")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_addr _BootNodeId]
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$156, DW_AT_external

$C$DW$157	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$157, DW_AT_declaration
	.dwattr $C$DW$157, DW_AT_external
$C$DW$158	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$157

	.global	_GV_current_counter
_GV_current_counter:	.usect	".ebss",2,1,1
$C$DW$159	.dwtag  DW_TAG_variable, DW_AT_name("GV_current_counter")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_GV_current_counter")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_addr _GV_current_counter]
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$159, DW_AT_external
	.global	_GV_voltage_counter
_GV_voltage_counter:	.usect	".ebss",2,1,1
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("GV_voltage_counter")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_GV_voltage_counter")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_addr _GV_voltage_counter]
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$160, DW_AT_external
	.global	_GV_Temp_counter
_GV_Temp_counter:	.usect	".ebss",2,1,1
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("GV_Temp_counter")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_GV_Temp_counter")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_addr _GV_Temp_counter]
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$161, DW_AT_external

$C$DW$162	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$162, DW_AT_declaration
	.dwattr $C$DW$162, DW_AT_external
$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$13)
$C$DW$164	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$105)
$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$147)
$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$162

$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$167, DW_AT_declaration
	.dwattr $C$DW$167, DW_AT_external
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$168, DW_AT_declaration
	.dwattr $C$DW$168, DW_AT_external
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$169, DW_AT_declaration
	.dwattr $C$DW$169, DW_AT_external
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$170, DW_AT_declaration
	.dwattr $C$DW$170, DW_AT_external
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$171, DW_AT_declaration
	.dwattr $C$DW$171, DW_AT_external
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$172, DW_AT_declaration
	.dwattr $C$DW$172, DW_AT_external
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$173, DW_AT_declaration
	.dwattr $C$DW$173, DW_AT_external
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$174, DW_AT_declaration
	.dwattr $C$DW$174, DW_AT_external
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$175, DW_AT_declaration
	.dwattr $C$DW$175, DW_AT_external
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Text")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_ODV_RTC_Text")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$176, DW_AT_declaration
	.dwattr $C$DW$176, DW_AT_external
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("golden_CRC_values")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_golden_CRC_values")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$177, DW_AT_declaration
	.dwattr $C$DW$177, DW_AT_external
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$178, DW_AT_declaration
	.dwattr $C$DW$178, DW_AT_external
$C$DW$179	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$179, DW_AT_declaration
	.dwattr $C$DW$179, DW_AT_external
$C$DW$180	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$180, DW_AT_declaration
	.dwattr $C$DW$180, DW_AT_external
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$181, DW_AT_declaration
	.dwattr $C$DW$181, DW_AT_external
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("mailboxSDOout")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_mailboxSDOout")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$182, DW_AT_declaration
	.dwattr $C$DW$182, DW_AT_external
$C$DW$183	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$183, DW_AT_declaration
	.dwattr $C$DW$183, DW_AT_external
$C$DW$184	.dwtag  DW_TAG_variable, DW_AT_name("can_rx_mbox")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_can_rx_mbox")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$184, DW_AT_declaration
	.dwattr $C$DW$184, DW_AT_external
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$185, DW_AT_declaration
	.dwattr $C$DW$185, DW_AT_external
$C$DW$186	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Alarms")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_ODV_Modules_Alarms")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$186, DW_AT_declaration
	.dwattr $C$DW$186, DW_AT_external
$C$DW$187	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Temperature")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_ODV_Modules_Temperature")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$187, DW_AT_declaration
	.dwattr $C$DW$187, DW_AT_external
$C$DW$188	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Heater")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_ODV_Modules_Heater")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$188, DW_AT_declaration
	.dwattr $C$DW$188, DW_AT_external
$C$DW$189	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_MaxCellVoltage")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_ODV_Modules_MaxCellVoltage")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$189, DW_AT_declaration
	.dwattr $C$DW$189, DW_AT_external
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Temperature_MIN")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_ODV_Modules_Temperature_MIN")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$190, DW_AT_declaration
	.dwattr $C$DW$190, DW_AT_external
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_MinCellVoltage")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_ODV_Modules_MinCellVoltage")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$191, DW_AT_declaration
	.dwattr $C$DW$191, DW_AT_external
$C$DW$192	.dwtag  DW_TAG_variable, DW_AT_name("ODI_gateway_dict_Data")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_ODI_gateway_dict_Data")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$192, DW_AT_declaration
	.dwattr $C$DW$192, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1892813 
	.sect	".text"
	.clink
	.global	_CheckfirmwareCRC

$C$DW$193	.dwtag  DW_TAG_subprogram, DW_AT_name("CheckfirmwareCRC")
	.dwattr $C$DW$193, DW_AT_low_pc(_CheckfirmwareCRC)
	.dwattr $C$DW$193, DW_AT_high_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_CheckfirmwareCRC")
	.dwattr $C$DW$193, DW_AT_external
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$193, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$193, DW_AT_TI_begin_line(0x44)
	.dwattr $C$DW$193, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$193, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../gateway.c",line 69,column 1,is_stmt,address _CheckfirmwareCRC

	.dwfde $C$DW$CIE, _CheckfirmwareCRC

;***************************************************************
;* FNAME: _CheckfirmwareCRC             FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_CheckfirmwareCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$194	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_breg20 -2]
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -4]
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("crc_rec")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_crc_rec")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../gateway.c",line 73,column 8,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |73| 
        B         $C$L3,UNC             ; [CPU_] |73| 
        ; branch occurs ; [] |73| 
$C$L1:    
	.dwpsn	file "../gateway.c",line 75,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,SP                ; [CPU_U] |75| 
        MOVL      XAR7,#_golden_CRC_values+2 ; [CPU_U] |75| 
        MOV       ACC,*-SP[2] << 3      ; [CPU_] |75| 
        SUBB      XAR4,#12              ; [CPU_U] |75| 
        ADDL      XAR7,ACC              ; [CPU_] |75| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |75| 
	.dwpsn	file "../gateway.c",line 76,column 5,is_stmt
        MOV       ACC,*-SP[8] << #1     ; [CPU_] |76| 
        MOV       *-SP[1],AL            ; [CPU_] |76| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |76| 
        MOVB      XAR5,#0               ; [CPU_] |76| 
        MOVB      ACC,#0                ; [CPU_] |76| 
$C$DW$197	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$197, DW_AT_low_pc(0x00)
	.dwattr $C$DW$197, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$197, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |76| 
        ; call occurs [#_getCRC32_cpu] ; [] |76| 
        MOVL      *-SP[4],ACC           ; [CPU_] |76| 
	.dwpsn	file "../gateway.c",line 77,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |77| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |77| 
        BF        $C$L2,EQ              ; [CPU_] |77| 
        ; branchcc occurs ; [] |77| 
	.dwpsn	file "../gateway.c",line 78,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |78| 
        B         $C$L4,UNC             ; [CPU_] |78| 
        ; branch occurs ; [] |78| 
$C$L2:    
	.dwpsn	file "../gateway.c",line 73,column 47,is_stmt
        INC       *-SP[2]               ; [CPU_] |73| 
$C$L3:    
	.dwpsn	file "../gateway.c",line 73,column 15,is_stmt
        MOVW      DP,#_golden_CRC_values+1 ; [CPU_U] 
        MOV       AL,@_golden_CRC_values+1 ; [CPU_] |73| 
        CMP       AL,*-SP[2]            ; [CPU_] |73| 
        B         $C$L1,HI              ; [CPU_] |73| 
        ; branchcc occurs ; [] |73| 
	.dwpsn	file "../gateway.c",line 89,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |89| 
$C$L4:    
	.dwpsn	file "../gateway.c",line 90,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$193, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$193, DW_AT_TI_end_line(0x5a)
	.dwattr $C$DW$193, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$193

	.sect	".text"
	.clink
	.global	_FnResetNode

$C$DW$199	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetNode")
	.dwattr $C$DW$199, DW_AT_low_pc(_FnResetNode)
	.dwattr $C$DW$199, DW_AT_high_pc(0x00)
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_FnResetNode")
	.dwattr $C$DW$199, DW_AT_external
	.dwattr $C$DW$199, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$199, DW_AT_TI_begin_line(0x5e)
	.dwattr $C$DW$199, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$199, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 94,column 29,is_stmt,address _FnResetNode

	.dwfde $C$DW$CIE, _FnResetNode
$C$DW$200	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnResetNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$201	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |94| 
	.dwpsn	file "../gateway.c",line 97,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$199, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$199, DW_AT_TI_end_line(0x61)
	.dwattr $C$DW$199, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$199

	.sect	".text"
	.clink
	.global	_FnResetCommunications

$C$DW$203	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetCommunications")
	.dwattr $C$DW$203, DW_AT_low_pc(_FnResetCommunications)
	.dwattr $C$DW$203, DW_AT_high_pc(0x00)
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_FnResetCommunications")
	.dwattr $C$DW$203, DW_AT_external
	.dwattr $C$DW$203, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$203, DW_AT_TI_begin_line(0x64)
	.dwattr $C$DW$203, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$203, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../gateway.c",line 100,column 39,is_stmt,address _FnResetCommunications

	.dwfde $C$DW$CIE, _FnResetCommunications
$C$DW$204	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetCommunications        FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_FnResetCommunications:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$205	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_breg20 -2]
$C$DW$206	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_breg20 -3]
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("dummy_m")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_dummy_m")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_breg20 -19]
        MOVL      *-SP[2],XAR4          ; [CPU_] |100| 
	.dwpsn	file "../gateway.c",line 105,column 3,is_stmt
$C$L5:    
        MOVZ      AR5,SP                ; [CPU_U] |105| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |105| 
        MOVB      AL,#0                 ; [CPU_] |105| 
        SUBB      XAR5,#19              ; [CPU_U] |105| 
$C$DW$208	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$208, DW_AT_low_pc(0x00)
	.dwattr $C$DW$208, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$208, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |105| 
        ; call occurs [#_MBX_pend] ; [] |105| 
        CMPB      AL,#0                 ; [CPU_] |105| 
        BF        $C$L5,NEQ             ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "../gateway.c",line 106,column 3,is_stmt
$C$L6:    
        MOVZ      AR5,SP                ; [CPU_U] |106| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |106| 
        MOVB      AL,#0                 ; [CPU_] |106| 
        SUBB      XAR5,#19              ; [CPU_U] |106| 
$C$DW$209	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$209, DW_AT_low_pc(0x00)
	.dwattr $C$DW$209, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$209, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |106| 
        ; call occurs [#_MBX_pend] ; [] |106| 
        CMPB      AL,#0                 ; [CPU_] |106| 
        BF        $C$L6,NEQ             ; [CPU_] |106| 
        ; branchcc occurs ; [] |106| 
	.dwpsn	file "../gateway.c",line 107,column 3,is_stmt
$C$L7:    
        MOVZ      AR5,SP                ; [CPU_U] |107| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |107| 
        MOVB      AL,#0                 ; [CPU_] |107| 
        SUBB      XAR5,#19              ; [CPU_U] |107| 
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$210, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |107| 
        ; call occurs [#_MBX_pend] ; [] |107| 
        CMPB      AL,#0                 ; [CPU_] |107| 
        BF        $C$L7,NEQ             ; [CPU_] |107| 
        ; branchcc occurs ; [] |107| 
	.dwpsn	file "../gateway.c",line 109,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOV       AL,@_ODP_Board_RevisionNumber ; [CPU_] |109| 
        MOV       *-SP[3],AL            ; [CPU_] |109| 
	.dwpsn	file "../gateway.c",line 116,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |116| 
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_name("_setNodeId")
	.dwattr $C$DW$211, DW_AT_TI_call
        LCR       #_setNodeId           ; [CPU_] |116| 
        ; call occurs [#_setNodeId] ; [] |116| 
	.dwpsn	file "../gateway.c",line 119,column 3,is_stmt
        MOV       AH,*-SP[3]            ; [CPU_] |119| 
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOV       AL,@_ODP_Board_BaudRate ; [CPU_] |119| 
$C$DW$212	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$212, DW_AT_low_pc(0x00)
	.dwattr $C$DW$212, DW_AT_name("_canInit")
	.dwattr $C$DW$212, DW_AT_TI_call
        LCR       #_canInit             ; [CPU_] |119| 
        ; call occurs [#_canInit] ; [] |119| 
	.dwpsn	file "../gateway.c",line 121,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |121| 
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("_DIC_SetNodeId")
	.dwattr $C$DW$213, DW_AT_TI_call
        LCR       #_DIC_SetNodeId       ; [CPU_] |121| 
        ; call occurs [#_DIC_SetNodeId] ; [] |121| 
	.dwpsn	file "../gateway.c",line 122,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$203, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$203, DW_AT_TI_end_line(0x7a)
	.dwattr $C$DW$203, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$203

	.sect	".text"
	.clink
	.global	_FnInitialiseNode

$C$DW$215	.dwtag  DW_TAG_subprogram, DW_AT_name("FnInitialiseNode")
	.dwattr $C$DW$215, DW_AT_low_pc(_FnInitialiseNode)
	.dwattr $C$DW$215, DW_AT_high_pc(0x00)
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_FnInitialiseNode")
	.dwattr $C$DW$215, DW_AT_external
	.dwattr $C$DW$215, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$215, DW_AT_TI_begin_line(0x80)
	.dwattr $C$DW$215, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$215, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../gateway.c",line 128,column 35,is_stmt,address _FnInitialiseNode

	.dwfde $C$DW$CIE, _FnInitialiseNode
$C$DW$216	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnInitialiseNode             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_FnInitialiseNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$217	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -2]
$C$DW$218	.dwtag  DW_TAG_variable, DW_AT_name("CrcFirmwareTest")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_CrcFirmwareTest")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |128| 
	.dwpsn	file "../gateway.c",line 129,column 26,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |129| 
	.dwpsn	file "../gateway.c",line 131,column 3,is_stmt
$C$DW$219	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$219, DW_AT_low_pc(0x00)
	.dwattr $C$DW$219, DW_AT_name("_genCRC32Table")
	.dwattr $C$DW$219, DW_AT_TI_call
        LCR       #_genCRC32Table       ; [CPU_] |131| 
        ; call occurs [#_genCRC32Table] ; [] |131| 
	.dwpsn	file "../gateway.c",line 133,column 3,is_stmt
$C$DW$220	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$220, DW_AT_low_pc(0x00)
	.dwattr $C$DW$220, DW_AT_name("_CheckfirmwareCRC")
	.dwattr $C$DW$220, DW_AT_TI_call
        LCR       #_CheckfirmwareCRC    ; [CPU_] |133| 
        ; call occurs [#_CheckfirmwareCRC] ; [] |133| 
        MOV       *-SP[3],AL            ; [CPU_] |133| 
	.dwpsn	file "../gateway.c",line 134,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+19   ; [CPU_U] 
        OR        @_PieCtrlRegs+19,#0x0020 ; [CPU_] |134| 
	.dwpsn	file "../gateway.c",line 135,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |135| 
	.dwpsn	file "../gateway.c",line 136,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,#_ODP_Board_Config ; [CPU_U] |136| 
        MOVL      @_MMSConfig,XAR4      ; [CPU_] |136| 
	.dwpsn	file "../gateway.c",line 138,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |138| 
$C$DW$221	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$221, DW_AT_low_pc(0x00)
	.dwattr $C$DW$221, DW_AT_name("_PAR_InitParam")
	.dwattr $C$DW$221, DW_AT_TI_call
        LCR       #_PAR_InitParam       ; [CPU_] |138| 
        ; call occurs [#_PAR_InitParam] ; [] |138| 
        CMPB      AL,#0                 ; [CPU_] |138| 
        BF        $C$L8,EQ              ; [CPU_] |138| 
        ; branchcc occurs ; [] |138| 
        MOV       AL,*-SP[3]            ; [CPU_] |138| 
        CMPB      AL,#1                 ; [CPU_] |138| 
        BF        $C$L8,NEQ             ; [CPU_] |138| 
        ; branchcc occurs ; [] |138| 
	.dwpsn	file "../gateway.c",line 140,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#1,UNC       ; [CPU_] |140| 
	.dwpsn	file "../gateway.c",line 141,column 3,is_stmt
        B         $C$L9,UNC             ; [CPU_] |141| 
        ; branch occurs ; [] |141| 
$C$L8:    
	.dwpsn	file "../gateway.c",line 143,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#2,UNC       ; [CPU_] |143| 
$C$L9:    
	.dwpsn	file "../gateway.c",line 146,column 3,is_stmt
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$222, DW_AT_TI_call
        LCR       #_PAR_SetParamDependantVars ; [CPU_] |146| 
        ; call occurs [#_PAR_SetParamDependantVars] ; [] |146| 
	.dwpsn	file "../gateway.c",line 147,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$215, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$215, DW_AT_TI_end_line(0x93)
	.dwattr $C$DW$215, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$215

	.sect	".text"
	.clink
	.global	_FnEnterPreOperational

$C$DW$224	.dwtag  DW_TAG_subprogram, DW_AT_name("FnEnterPreOperational")
	.dwattr $C$DW$224, DW_AT_low_pc(_FnEnterPreOperational)
	.dwattr $C$DW$224, DW_AT_high_pc(0x00)
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_FnEnterPreOperational")
	.dwattr $C$DW$224, DW_AT_external
	.dwattr $C$DW$224, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$224, DW_AT_TI_begin_line(0x97)
	.dwattr $C$DW$224, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$224, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 151,column 40,is_stmt,address _FnEnterPreOperational

	.dwfde $C$DW$CIE, _FnEnterPreOperational
$C$DW$225	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnEnterPreOperational        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnEnterPreOperational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$226	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |151| 
	.dwpsn	file "../gateway.c",line 152,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOVB      @_ODV_Controlword,#6,UNC ; [CPU_] |152| 
	.dwpsn	file "../gateway.c",line 153,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$227	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$227, DW_AT_low_pc(0x00)
	.dwattr $C$DW$227, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$224, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$224, DW_AT_TI_end_line(0x99)
	.dwattr $C$DW$224, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$224

	.sect	".text"
	.clink
	.global	_FnStartNode

$C$DW$228	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStartNode")
	.dwattr $C$DW$228, DW_AT_low_pc(_FnStartNode)
	.dwattr $C$DW$228, DW_AT_high_pc(0x00)
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_FnStartNode")
	.dwattr $C$DW$228, DW_AT_external
	.dwattr $C$DW$228, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$228, DW_AT_TI_begin_line(0x9c)
	.dwattr $C$DW$228, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$228, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 156,column 30,is_stmt,address _FnStartNode

	.dwfde $C$DW$CIE, _FnStartNode
$C$DW$229	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStartNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStartNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$230	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$230, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |156| 
	.dwpsn	file "../gateway.c",line 158,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$228, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$228, DW_AT_TI_end_line(0x9e)
	.dwattr $C$DW$228, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$228

	.sect	".text"
	.clink
	.global	_PreSync

$C$DW$232	.dwtag  DW_TAG_subprogram, DW_AT_name("PreSync")
	.dwattr $C$DW$232, DW_AT_low_pc(_PreSync)
	.dwattr $C$DW$232, DW_AT_high_pc(0x00)
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_PreSync")
	.dwattr $C$DW$232, DW_AT_external
	.dwattr $C$DW$232, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$232, DW_AT_TI_begin_line(0xaa)
	.dwattr $C$DW$232, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$232, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 170,column 26,is_stmt,address _PreSync

	.dwfde $C$DW$CIE, _PreSync
$C$DW$233	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$233, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PreSync                      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_PreSync:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$234	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$234, DW_AT_location[DW_OP_breg20 -2]
$C$DW$235	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_breg20 -3]
$C$DW$236	.dwtag  DW_TAG_variable, DW_AT_name("maxtemp")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_maxtemp")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_breg20 -4]
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("mintemp")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_mintemp")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -5]
        MOVL      *-SP[2],XAR4          ; [CPU_] |170| 
	.dwpsn	file "../gateway.c",line 171,column 8,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |171| 
	.dwpsn	file "../gateway.c",line 172,column 2,is_stmt
        MOVW      DP,#_Heater_status    ; [CPU_U] 
        MOV       @_Heater_status,#0    ; [CPU_] |172| 
	.dwpsn	file "../gateway.c",line 173,column 15,is_stmt
        MOV       *-SP[4],#-55          ; [CPU_] |173| 
	.dwpsn	file "../gateway.c",line 173,column 30,is_stmt
        MOVB      *-SP[5],#127,UNC      ; [CPU_] |173| 
	.dwpsn	file "../gateway.c",line 175,column 3,is_stmt
        DEC       @_Sync_count          ; [CPU_] |175| 
	.dwpsn	file "../gateway.c",line 176,column 3,is_stmt
        MOV       AL,@_Sync_count       ; [CPU_] |176| 
        BF        $C$L27,NEQ            ; [CPU_] |176| 
        ; branchcc occurs ; [] |176| 
	.dwpsn	file "../gateway.c",line 177,column 5,is_stmt
        MOV       @_Modules_Present,#0  ; [CPU_] |177| 
	.dwpsn	file "../gateway.c",line 178,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |178| 
        CMPB      AL,#70                ; [CPU_] |178| 
        B         $C$L17,GEQ            ; [CPU_] |178| 
        ; branchcc occurs ; [] |178| 
$C$L10:    
	.dwpsn	file "../gateway.c",line 179,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |179| 
        MOV       ACC,*-SP[3]           ; [CPU_] |179| 
        ADDL      XAR4,ACC              ; [CPU_] |179| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |179| 
        CMPB      AL,#255               ; [CPU_] |179| 
        BF        $C$L16,EQ             ; [CPU_] |179| 
        ; branchcc occurs ; [] |179| 
	.dwpsn	file "../gateway.c",line 180,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |180| 
        MOVL      XAR4,#_ODV_Modules_MinCellVoltage ; [CPU_U] |180| 
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        ADDL      XAR4,ACC              ; [CPU_] |180| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |180| 
        CMP       AL,@_GAT_min          ; [CPU_] |180| 
        B         $C$L11,HIS            ; [CPU_] |180| 
        ; branchcc occurs ; [] |180| 
	.dwpsn	file "../gateway.c",line 180,column 54,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |180| 
        MOVL      XAR7,#_ODV_Modules_MinCellVoltage ; [CPU_U] |180| 
        ADDL      XAR7,ACC              ; [CPU_] |180| 
        MOV       AL,*XAR7              ; [CPU_] |180| 
        MOV       @_GAT_min,AL          ; [CPU_] |180| 
$C$L11:    
	.dwpsn	file "../gateway.c",line 181,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |181| 
        MOVL      XAR4,#_ODV_Modules_MaxCellVoltage ; [CPU_U] |181| 
        ADDL      XAR4,ACC              ; [CPU_] |181| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |181| 
        CMP       AL,@_GAT_max          ; [CPU_] |181| 
        B         $C$L12,LOS            ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
	.dwpsn	file "../gateway.c",line 181,column 54,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |181| 
        MOVL      XAR7,#_ODV_Modules_MaxCellVoltage ; [CPU_U] |181| 
        ADDL      XAR7,ACC              ; [CPU_] |181| 
        MOV       AL,*XAR7              ; [CPU_] |181| 
        MOV       @_GAT_max,AL          ; [CPU_] |181| 
$C$L12:    
	.dwpsn	file "../gateway.c",line 182,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |182| 
        MOVL      XAR4,#_ODV_Modules_Temperature ; [CPU_U] |182| 
        ADDL      XAR4,ACC              ; [CPU_] |182| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |182| 
        CMP       AL,*-SP[4]            ; [CPU_] |182| 
        B         $C$L13,LEQ            ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
	.dwpsn	file "../gateway.c",line 182,column 51,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |182| 
        MOVL      XAR7,#_ODV_Modules_Temperature ; [CPU_U] |182| 
        ADDL      XAR7,ACC              ; [CPU_] |182| 
        MOV       AL,*XAR7              ; [CPU_] |182| 
        MOV       *-SP[4],AL            ; [CPU_] |182| 
$C$L13:    
	.dwpsn	file "../gateway.c",line 183,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |183| 
        MOVL      XAR4,#_ODV_Modules_Temperature_MIN ; [CPU_U] |183| 
        ADDL      XAR4,ACC              ; [CPU_] |183| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |183| 
        CMP       AL,*-SP[5]            ; [CPU_] |183| 
        B         $C$L14,HIS            ; [CPU_] |183| 
        ; branchcc occurs ; [] |183| 
	.dwpsn	file "../gateway.c",line 183,column 55,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |183| 
        MOVL      XAR7,#_ODV_Modules_Temperature_MIN ; [CPU_U] |183| 
        ADDL      XAR7,ACC              ; [CPU_] |183| 
        MOV       AL,*XAR7              ; [CPU_] |183| 
        MOV       *-SP[5],AL            ; [CPU_] |183| 
$C$L14:    
	.dwpsn	file "../gateway.c",line 184,column 3,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |184| 
        MOVL      XAR4,#_ODV_Modules_Heater ; [CPU_U] |184| 
        ADDL      XAR4,ACC              ; [CPU_] |184| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |184| 
        ADD       @_Heater_status,AL    ; [CPU_] |184| 
	.dwpsn	file "../gateway.c",line 186,column 9,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |186| 
        BF        $C$L16,EQ             ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
	.dwpsn	file "../gateway.c",line 187,column 11,is_stmt
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        INC       @_Modules_Present     ; [CPU_] |187| 
	.dwpsn	file "../gateway.c",line 188,column 11,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |188| 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |188| 
        ADDL      XAR4,ACC              ; [CPU_] |188| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |188| 
        BF        $C$L15,EQ             ; [CPU_] |188| 
        ; branchcc occurs ; [] |188| 
        MOV       ACC,*-SP[3]           ; [CPU_] |188| 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |188| 
        ADDL      XAR4,ACC              ; [CPU_] |188| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |188| 
        CMPB      AL,#255               ; [CPU_] |188| 
        B         $C$L15,HIS            ; [CPU_] |188| 
        ; branchcc occurs ; [] |188| 
	.dwpsn	file "../gateway.c",line 190,column 12,is_stmt
        INC       @_Delay_Error         ; [CPU_] |190| 
	.dwpsn	file "../gateway.c",line 191,column 12,is_stmt
        MOVW      DP,#_ODP_Gateway_Delay_Relay_Error ; [CPU_U] 
        MOV       T,@_ODP_Gateway_Delay_Relay_Error ; [CPU_] |191| 
        MPYB      ACC,T,#20             ; [CPU_] |191| 
        MOVW      DP,#_Delay_Error      ; [CPU_U] 
        CMP       AL,@_Delay_Error      ; [CPU_] |191| 
        B         $C$L16,HIS            ; [CPU_] |191| 
        ; branchcc occurs ; [] |191| 
	.dwpsn	file "../gateway.c",line 193,column 13,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |193| 
        MOVZ      AR4,*-SP[3]           ; [CPU_] |193| 
        MOVL      XAR5,#_ODV_Modules_Alarms ; [CPU_U] |193| 
        MOV       T,#24                 ; [CPU_] |193| 
        MOV       PL,#0                 ; [CPU_] |193| 
        MOV       PH,#32768             ; [CPU_] |193| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        ADDL      XAR5,ACC              ; [CPU_] |193| 
        ADDB      XAR4,#1               ; [CPU_] |193| 
        MOV       ACC,AR4               ; [CPU_] |193| 
        LSLL      ACC,T                 ; [CPU_] |193| 
        ADDU      ACC,*+XAR5[0]         ; [CPU_] |193| 
        ADDL      P,ACC                 ; [CPU_] |193| 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,P ; [CPU_] |193| 
	.dwpsn	file "../gateway.c",line 194,column 13,is_stmt
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |194| 
$C$DW$238	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$238, DW_AT_low_pc(0x00)
	.dwattr $C$DW$238, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$238, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |194| 
        ; call occurs [#_ERR_SetError] ; [] |194| 
	.dwpsn	file "../gateway.c",line 195,column 13,is_stmt
        MOVW      DP,#_Delay_Error      ; [CPU_U] 
        MOV       @_Delay_Error,#0      ; [CPU_] |195| 
	.dwpsn	file "../gateway.c",line 198,column 5,is_stmt
        B         $C$L16,UNC            ; [CPU_] |198| 
        ; branch occurs ; [] |198| 
$C$L15:    
	.dwpsn	file "../gateway.c",line 201,column 12,is_stmt
$C$DW$239	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$239, DW_AT_low_pc(0x00)
	.dwattr $C$DW$239, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$239, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |201| 
        ; call occurs [#_ERR_ClearError] ; [] |201| 
	.dwpsn	file "../gateway.c",line 202,column 12,is_stmt
        MOVW      DP,#_Delay_Error      ; [CPU_U] 
        MOV       @_Delay_Error,#0      ; [CPU_] |202| 
$C$L16:    
	.dwpsn	file "../gateway.c",line 206,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |206| 
        MOV       ACC,*-SP[3]           ; [CPU_] |206| 
        ADDL      XAR4,ACC              ; [CPU_] |206| 
        MOVB      *+XAR4[0],#255,UNC    ; [CPU_] |206| 
	.dwpsn	file "../gateway.c",line 207,column 7,is_stmt
        INC       *-SP[3]               ; [CPU_] |207| 
	.dwpsn	file "../gateway.c",line 178,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |178| 
        CMPB      AL,#70                ; [CPU_] |178| 
        B         $C$L10,LT             ; [CPU_] |178| 
        ; branchcc occurs ; [] |178| 
$C$L17:    
	.dwpsn	file "../gateway.c",line 210,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |210| 
        CMP       AL,#-55               ; [CPU_] |210| 
        BF        $C$L18,EQ             ; [CPU_] |210| 
        ; branchcc occurs ; [] |210| 
	.dwpsn	file "../gateway.c",line 212,column 7,is_stmt
        MOVW      DP,#_Oldmaxtemp       ; [CPU_U] 
        MOV       @_Oldmaxtemp,AL       ; [CPU_] |212| 
$C$L18:    
	.dwpsn	file "../gateway.c",line 215,column 6,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |215| 
        CMPB      AL,#127               ; [CPU_] |215| 
        BF        $C$L19,EQ             ; [CPU_] |215| 
        ; branchcc occurs ; [] |215| 
	.dwpsn	file "../gateway.c",line 217,column 7,is_stmt
        MOVW      DP,#_Oldmintemp       ; [CPU_U] 
        MOV       @_Oldmintemp,AL       ; [CPU_] |217| 
$C$L19:    
	.dwpsn	file "../gateway.c",line 219,column 6,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        MOVZ      AR6,@_GAT_min         ; [CPU_] |219| 
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#65535            ; [CPU_] |219| 
        CMPL      ACC,XAR6              ; [CPU_] |219| 
        BF        $C$L20,EQ             ; [CPU_] |219| 
        ; branchcc occurs ; [] |219| 
	.dwpsn	file "../gateway.c",line 221,column 7,is_stmt
        MOV       AL,@_GAT_min          ; [CPU_] |221| 
        MOV       @_OldGAT_min,AL       ; [CPU_] |221| 
$C$L20:    
	.dwpsn	file "../gateway.c",line 223,column 6,is_stmt
        MOV       AL,@_GAT_max          ; [CPU_] |223| 
        CMPB      AL,#1                 ; [CPU_] |223| 
        BF        $C$L21,EQ             ; [CPU_] |223| 
        ; branchcc occurs ; [] |223| 
	.dwpsn	file "../gateway.c",line 225,column 7,is_stmt
        MOV       @_OldGAT_max,AL       ; [CPU_] |225| 
$C$L21:    
	.dwpsn	file "../gateway.c",line 228,column 6,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_TempHeater_OFF_Max ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_TempHeater_OFF_Max ; [CPU_] |228| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |228| 
        B         $C$L22,LOS            ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |228| 
        MOV       AL,AH                 ; [CPU_] |228| 
        ASR       AL,1                  ; [CPU_] |228| 
        MOVW      DP,#_ODP_Settings_AUD_Heater_Voltage_OFF ; [CPU_U] 
        LSR       AL,14                 ; [CPU_] |228| 
        ADD       AL,AH                 ; [CPU_] |228| 
        ASR       AL,2                  ; [CPU_] |228| 
        CMP       AL,@_ODP_Settings_AUD_Heater_Voltage_OFF ; [CPU_] |228| 
        B         $C$L23,HIS            ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
$C$L22:    
	.dwpsn	file "../gateway.c",line 230,column 8,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Gateway_Heater_Status,#0 ; [CPU_] |230| 
	.dwpsn	file "../gateway.c",line 231,column 7,is_stmt
        B         $C$L24,UNC            ; [CPU_] |231| 
        ; branch occurs ; [] |231| 
$C$L23:    
	.dwpsn	file "../gateway.c",line 232,column 12,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_Temp_Heater_ON_Min ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_Temp_Heater_ON_Min ; [CPU_] |232| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |232| 
        B         $C$L24,LOS            ; [CPU_] |232| 
        ; branchcc occurs ; [] |232| 
	.dwpsn	file "../gateway.c",line 234,column 8,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOVB      @_ODV_Gateway_Heater_Status,#1,UNC ; [CPU_] |234| 
$C$L24:    
	.dwpsn	file "../gateway.c",line 246,column 3,is_stmt
        MOVW      DP,#_Oldmaxtemp       ; [CPU_U] 
        MOV       AL,@_Oldmaxtemp       ; [CPU_] |246| 
        MOVW      DP,#_ODV_Gateway_MaxModTemp ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxModTemp,AL ; [CPU_] |246| 
	.dwpsn	file "../gateway.c",line 247,column 3,is_stmt
        MOVW      DP,#_Oldmintemp       ; [CPU_U] 
        MOV       AL,@_Oldmintemp       ; [CPU_] |247| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        MOV       @_ODV_Gateway_MinModTemp,AL ; [CPU_] |247| 
	.dwpsn	file "../gateway.c",line 248,column 3,is_stmt
        MOVW      DP,#_OldGAT_min       ; [CPU_U] 
        MOV       AL,@_OldGAT_min       ; [CPU_] |248| 
        MOVW      DP,#_ODV_Gateway_MinCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MinCellVoltage,AL ; [CPU_] |248| 
	.dwpsn	file "../gateway.c",line 249,column 3,is_stmt
        MOVW      DP,#_OldGAT_max       ; [CPU_U] 
        MOV       AL,@_OldGAT_max       ; [CPU_] |249| 
        MOVW      DP,#_ODV_Gateway_MaxCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxCellVoltage,AL ; [CPU_] |249| 
	.dwpsn	file "../gateway.c",line 250,column 3,is_stmt
        MOVW      DP,#_OldGAT_min       ; [CPU_U] 
        SUB       AL,@_OldGAT_min       ; [CPU_] |250| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxDeltaCellVoltage,AL ; [CPU_] |250| 
	.dwpsn	file "../gateway.c",line 251,column 3,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |251| 
        BF        $C$L26,EQ             ; [CPU_] |251| 
        ; branchcc occurs ; [] |251| 
	.dwpsn	file "../gateway.c",line 253,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax_bal_delta ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax_bal_delta ; [CPU_] |253| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_] |253| 
        B         $C$L25,HIS            ; [CPU_] |253| 
        ; branchcc occurs ; [] |253| 
	.dwpsn	file "../gateway.c",line 255,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Current ; [CPU_] |255| 
        MOV       AL,AH                 ; [CPU_] |255| 
        SETC      SXM                   ; [CPU_] 
        ASR       AL,1                  ; [CPU_] |255| 
        LSR       AL,14                 ; [CPU_] |255| 
        ADD       AL,AH                 ; [CPU_] |255| 
        ASR       AL,2                  ; [CPU_] |255| 
        MOV       ACC,AL                ; [CPU_] |255| 
        MOV32     R0H,ACC               ; [CPU_] |255| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        I32TOF32  R0H,R0H               ; [CPU_] |255| 
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$240, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |255| 
        ; call occurs [#_CNV_Round] ; [] |255| 
        MOVL      XAR6,ACC              ; [CPU_] |255| 
        MOVB      ACC,#3                ; [CPU_] |255| 
        CMPL      ACC,XAR6              ; [CPU_] |255| 
        B         $C$L26,GEQ            ; [CPU_] |255| 
        ; branchcc occurs ; [] |255| 
	.dwpsn	file "../gateway.c",line 257,column 8,is_stmt
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_name("_ERR_DELTA_Voltage")
	.dwattr $C$DW$241, DW_AT_TI_call
        LCR       #_ERR_DELTA_Voltage   ; [CPU_] |257| 
        ; call occurs [#_ERR_DELTA_Voltage] ; [] |257| 
	.dwpsn	file "../gateway.c",line 259,column 5,is_stmt
        B         $C$L26,UNC            ; [CPU_] |259| 
        ; branch occurs ; [] |259| 
$C$L25:    
	.dwpsn	file "../gateway.c",line 260,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin_bal_delta ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin_bal_delta ; [CPU_] |260| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_] |260| 
        B         $C$L26,HIS            ; [CPU_] |260| 
        ; branchcc occurs ; [] |260| 
	.dwpsn	file "../gateway.c",line 262,column 5,is_stmt
        MOV       AL,#0                 ; [CPU_] |262| 
        MOV       AH,#128               ; [CPU_] |262| 
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$242, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |262| 
        ; call occurs [#_ERR_HandleWarning] ; [] |262| 
$C$L26:    
	.dwpsn	file "../gateway.c",line 266,column 5,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        MOV       @_GAT_min,#65535      ; [CPU_] |266| 
	.dwpsn	file "../gateway.c",line 267,column 5,is_stmt
        MOV       @_GAT_max,#0          ; [CPU_] |267| 
	.dwpsn	file "../gateway.c",line 268,column 5,is_stmt
        MOVB      @_Sync_count,#71,UNC  ; [CPU_] |268| 
	.dwpsn	file "../gateway.c",line 270,column 1,is_stmt
$C$L27:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$232, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$232, DW_AT_TI_end_line(0x10e)
	.dwattr $C$DW$232, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$232

	.sect	".text"
	.clink
	.global	_FnStopNode

$C$DW$244	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStopNode")
	.dwattr $C$DW$244, DW_AT_low_pc(_FnStopNode)
	.dwattr $C$DW$244, DW_AT_high_pc(0x00)
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_FnStopNode")
	.dwattr $C$DW$244, DW_AT_external
	.dwattr $C$DW$244, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$244, DW_AT_TI_begin_line(0x111)
	.dwattr $C$DW$244, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$244, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 273,column 29,is_stmt,address _FnStopNode

	.dwfde $C$DW$CIE, _FnStopNode
$C$DW$245	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStopNode                   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStopNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$246	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |273| 
	.dwpsn	file "../gateway.c",line 275,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |275| 
	.dwpsn	file "../gateway.c",line 276,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$244, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$244, DW_AT_TI_end_line(0x114)
	.dwattr $C$DW$244, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$244

	.sect	".text"
	.clink
	.global	_canOpenInit

$C$DW$248	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$248, DW_AT_low_pc(_canOpenInit)
	.dwattr $C$DW$248, DW_AT_high_pc(0x00)
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$248, DW_AT_external
	.dwattr $C$DW$248, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$248, DW_AT_TI_begin_line(0x118)
	.dwattr $C$DW$248, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$248, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../gateway.c",line 282,column 1,is_stmt,address _canOpenInit

	.dwfde $C$DW$CIE, _canOpenInit

;***************************************************************
;* FNAME: _canOpenInit                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_canOpenInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../gateway.c",line 284,column 3,is_stmt
        MOVL      XAR4,#_ODI_gateway_dict_Data ; [CPU_U] |284| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      @_BoardODdata,XAR4    ; [CPU_] |284| 
	.dwpsn	file "../gateway.c",line 286,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |286| 
        MOVB      XAR0,#84              ; [CPU_] |286| 
        MOVL      XAR4,#_FnInitialiseNode ; [CPU_U] |286| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |286| 
	.dwpsn	file "../gateway.c",line 287,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |287| 
        MOVB      XAR0,#86              ; [CPU_] |287| 
        MOVL      XAR4,#_FnEnterPreOperational ; [CPU_U] |287| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |287| 
	.dwpsn	file "../gateway.c",line 288,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |288| 
        MOVB      XAR0,#88              ; [CPU_] |288| 
        MOVL      XAR4,#_FnStartNode    ; [CPU_U] |288| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |288| 
	.dwpsn	file "../gateway.c",line 289,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |289| 
        MOVB      XAR0,#90              ; [CPU_] |289| 
        MOVL      XAR4,#_FnStopNode     ; [CPU_U] |289| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |289| 
	.dwpsn	file "../gateway.c",line 290,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |290| 
        MOVB      XAR0,#92              ; [CPU_] |290| 
        MOVL      XAR4,#_FnResetNode    ; [CPU_U] |290| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |290| 
	.dwpsn	file "../gateway.c",line 291,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |291| 
        MOVB      XAR0,#94              ; [CPU_] |291| 
        MOVL      XAR4,#_FnResetCommunications ; [CPU_U] |291| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |291| 
	.dwpsn	file "../gateway.c",line 293,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |293| 
        MOVB      XAR0,#252             ; [CPU_] |293| 
        MOVL      XAR4,#_PAR_StoreODSubIndex ; [CPU_U] |293| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |293| 
	.dwpsn	file "../gateway.c",line 295,column 3,is_stmt
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |295| 
        MOVB      ACC,#0                ; [CPU_] |295| 
        MOVB      XAR0,#254             ; [CPU_] |295| 
        MOVL      *+XAR4[AR0],ACC       ; [CPU_] |295| 
	.dwpsn	file "../gateway.c",line 296,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |296| 
        MOVB      XAR0,#242             ; [CPU_] |296| 
        MOVL      XAR4,#_PreSync        ; [CPU_U] |296| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |296| 
	.dwpsn	file "../gateway.c",line 299,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |299| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |299| 
$C$DW$249	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$249, DW_AT_low_pc(0x00)
	.dwattr $C$DW$249, DW_AT_name("_setState")
	.dwattr $C$DW$249, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |299| 
        ; call occurs [#_setState] ; [] |299| 
	.dwpsn	file "../gateway.c",line 300,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |300| 
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_name("_FnResetCommunications")
	.dwattr $C$DW$250, DW_AT_TI_call
        LCR       #_FnResetCommunications ; [CPU_] |300| 
        ; call occurs [#_FnResetCommunications] ; [] |300| 
	.dwpsn	file "../gateway.c",line 307,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#5                 ; [CPU_] |307| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |307| 
$C$DW$251	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$251, DW_AT_low_pc(0x00)
	.dwattr $C$DW$251, DW_AT_name("_setState")
	.dwattr $C$DW$251, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |307| 
        ; call occurs [#_setState] ; [] |307| 
	.dwpsn	file "../gateway.c",line 309,column 1,is_stmt
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$248, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$248, DW_AT_TI_end_line(0x135)
	.dwattr $C$DW$248, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$248

	.sect	".text"
	.clink
	.global	_taskFnGestionMachine

$C$DW$253	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnGestionMachine")
	.dwattr $C$DW$253, DW_AT_low_pc(_taskFnGestionMachine)
	.dwattr $C$DW$253, DW_AT_high_pc(0x00)
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_taskFnGestionMachine")
	.dwattr $C$DW$253, DW_AT_external
	.dwattr $C$DW$253, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$253, DW_AT_TI_begin_line(0x13f)
	.dwattr $C$DW$253, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$253, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 319,column 32,is_stmt,address _taskFnGestionMachine

	.dwfde $C$DW$CIE, _taskFnGestionMachine

;***************************************************************
;* FNAME: _taskFnGestionMachine         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_taskFnGestionMachine:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$254	.dwtag  DW_TAG_variable, DW_AT_name("old_time")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_old_time")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../gateway.c",line 325,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |325| 
        MOVB      AL,#2                 ; [CPU_] |325| 
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$255, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |325| 
        ; call occurs [#_SEM_pend] ; [] |325| 
	.dwpsn	file "../gateway.c",line 326,column 3,is_stmt
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_name("_HAL_Init")
	.dwattr $C$DW$256, DW_AT_TI_call
        LCR       #_HAL_Init            ; [CPU_] |326| 
        ; call occurs [#_HAL_Init] ; [] |326| 
	.dwpsn	file "../gateway.c",line 327,column 3,is_stmt
$C$DW$257	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$257, DW_AT_low_pc(0x00)
	.dwattr $C$DW$257, DW_AT_name("_ADS_Init")
	.dwattr $C$DW$257, DW_AT_TI_call
        LCR       #_ADS_Init            ; [CPU_] |327| 
        ; call occurs [#_ADS_Init] ; [] |327| 
	.dwpsn	file "../gateway.c",line 328,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |328| 
        BF        $C$L29,NEQ            ; [CPU_] |328| 
        ; branchcc occurs ; [] |328| 
$C$L28:    
	.dwpsn	file "../gateway.c",line 329,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |329| 
        MOVB      AL,#1                 ; [CPU_] |329| 
$C$DW$258	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$258, DW_AT_low_pc(0x00)
	.dwattr $C$DW$258, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$258, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |329| 
        ; call occurs [#_SEM_pend] ; [] |329| 
	.dwpsn	file "../gateway.c",line 328,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |328| 
        BF        $C$L28,EQ             ; [CPU_] |328| 
        ; branchcc occurs ; [] |328| 
$C$L29:    
	.dwpsn	file "../gateway.c",line 331,column 3,is_stmt
$C$DW$259	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$259, DW_AT_low_pc(0x00)
	.dwattr $C$DW$259, DW_AT_name("_USB_Start")
	.dwattr $C$DW$259, DW_AT_TI_call
        LCR       #_USB_Start           ; [CPU_] |331| 
        ; call occurs [#_USB_Start] ; [] |331| 
	.dwpsn	file "../gateway.c",line 332,column 3,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |332| 
        MOVL      *-SP[2],ACC           ; [CPU_] |332| 
	.dwpsn	file "../gateway.c",line 333,column 9,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      ACC,@_BootCommand     ; [CPU_] |333| 
        BF        $C$L32,NEQ            ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
$C$L30:    
	.dwpsn	file "../gateway.c",line 337,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVB      XAR6,#63              ; [CPU_] |337| 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |337| 
        MOVB      AH,#0                 ; [CPU_] |337| 
        ANDB      AL,#0x3f              ; [CPU_] |337| 
        CMPL      ACC,XAR6              ; [CPU_] |337| 
        BF        $C$L31,NEQ            ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
        MOVB      XAR6,#62              ; [CPU_] |337| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |337| 
        MOVB      AH,#0                 ; [CPU_] |337| 
        ANDB      AL,#0x3f              ; [CPU_] |337| 
        CMPL      ACC,XAR6              ; [CPU_] |337| 
        BF        $C$L31,NEQ            ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
	.dwpsn	file "../gateway.c",line 340,column 7,is_stmt
        MOV       AL,#-1                ; [CPU_] |340| 
$C$DW$260	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$260, DW_AT_low_pc(0x00)
	.dwattr $C$DW$260, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$260, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |340| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |340| 
	.dwpsn	file "../gateway.c",line 341,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |341| 
$C$DW$261	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$261, DW_AT_low_pc(0x00)
	.dwattr $C$DW$261, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$261, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |341| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |341| 
$C$L31:    
	.dwpsn	file "../gateway.c",line 343,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |343| 
        MOVL      *-SP[2],ACC           ; [CPU_] |343| 
	.dwpsn	file "../gateway.c",line 344,column 5,is_stmt
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_name("_USB_Unlock")
	.dwattr $C$DW$262, DW_AT_TI_call
        LCR       #_USB_Unlock          ; [CPU_] |344| 
        ; call occurs [#_USB_Unlock] ; [] |344| 
	.dwpsn	file "../gateway.c",line 345,column 5,is_stmt
        MOVB      AL,#2                 ; [CPU_] |345| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |345| 
$C$DW$263	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$263, DW_AT_low_pc(0x00)
	.dwattr $C$DW$263, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$263, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |345| 
        ; call occurs [#_SEM_pend] ; [] |345| 
	.dwpsn	file "../gateway.c",line 333,column 9,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      ACC,@_BootCommand     ; [CPU_] |333| 
        BF        $C$L30,EQ             ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
$C$L32:    
	.dwpsn	file "../gateway.c",line 347,column 3,is_stmt
$C$DW$264	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$264, DW_AT_low_pc(0x00)
	.dwattr $C$DW$264, DW_AT_name("_USB_Stop")
	.dwattr $C$DW$264, DW_AT_TI_call
        LCR       #_USB_Stop            ; [CPU_] |347| 
        ; call occurs [#_USB_Stop] ; [] |347| 
	.dwpsn	file "../gateway.c",line 348,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |348| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |348| 
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$265, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |348| 
        ; call occurs [#_SEM_pend] ; [] |348| 
	.dwpsn	file "../gateway.c",line 349,column 3,is_stmt
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("_HAL_Reset")
	.dwattr $C$DW$266, DW_AT_TI_call
        LCR       #_HAL_Reset           ; [CPU_] |349| 
        ; call occurs [#_HAL_Reset] ; [] |349| 
	.dwpsn	file "../gateway.c",line 350,column 3,is_stmt
        MOV       AL,#28009             ; [CPU_] |350| 
        MOV       AH,#21093             ; [CPU_] |350| 
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      @_BootCommand,ACC     ; [CPU_] |350| 
	.dwpsn	file "../gateway.c",line 351,column 3,is_stmt
 LB 0x3F7FF6 
	.dwpsn	file "../gateway.c",line 352,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$253, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$253, DW_AT_TI_end_line(0x160)
	.dwattr $C$DW$253, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$253

	.sect	".text"
	.clink
	.global	_TaskSciSendReceive

$C$DW$268	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskSciSendReceive")
	.dwattr $C$DW$268, DW_AT_low_pc(_TaskSciSendReceive)
	.dwattr $C$DW$268, DW_AT_high_pc(0x00)
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_TaskSciSendReceive")
	.dwattr $C$DW$268, DW_AT_external
	.dwattr $C$DW$268, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$268, DW_AT_TI_begin_line(0x170)
	.dwattr $C$DW$268, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$268, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../gateway.c",line 368,column 30,is_stmt,address _TaskSciSendReceive

	.dwfde $C$DW$CIE, _TaskSciSendReceive

;***************************************************************
;* FNAME: _TaskSciSendReceive           FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 21 Auto,  0 SOE     *
;***************************************************************

_TaskSciSendReceive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$269	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_breg20 -4]
$C$DW$270	.dwtag  DW_TAG_variable, DW_AT_name("insulation_counter")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_insulation_counter")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_breg20 -6]
$C$DW$271	.dwtag  DW_TAG_variable, DW_AT_name("current_counter")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_current_counter")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$271, DW_AT_location[DW_OP_breg20 -8]
$C$DW$272	.dwtag  DW_TAG_variable, DW_AT_name("voltage_counter")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_voltage_counter")
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$272, DW_AT_location[DW_OP_breg20 -10]
$C$DW$273	.dwtag  DW_TAG_variable, DW_AT_name("sleep_counter")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_sleep_counter")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$273, DW_AT_location[DW_OP_breg20 -12]
$C$DW$274	.dwtag  DW_TAG_variable, DW_AT_name("current_counter2")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_current_counter2")
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$274, DW_AT_location[DW_OP_breg20 -14]
$C$DW$275	.dwtag  DW_TAG_variable, DW_AT_name("reset_relay")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_reset_relay")
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$275, DW_AT_location[DW_OP_breg20 -16]
$C$DW$276	.dwtag  DW_TAG_variable, DW_AT_name("com_counter")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_com_counter")
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$276, DW_AT_location[DW_OP_breg20 -17]
$C$DW$277	.dwtag  DW_TAG_variable, DW_AT_name("cur")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_cur")
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$277, DW_AT_location[DW_OP_breg20 -18]
$C$DW$278	.dwtag  DW_TAG_variable, DW_AT_name("volt")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_volt")
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$278, DW_AT_location[DW_OP_breg20 -19]
$C$DW$279	.dwtag  DW_TAG_variable, DW_AT_name("curf")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_curf")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_breg20 -22]
$C$DW$280	.dwtag  DW_TAG_variable, DW_AT_name("checkenable")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_checkenable")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_breg20 -23]
	.dwpsn	file "../gateway.c",line 369,column 45,is_stmt
        MOVB      ACC,#0                ; [CPU_] |369| 
        MOVL      *-SP[6],ACC           ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 369,column 66,is_stmt
        MOVL      *-SP[8],ACC           ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 369,column 87,is_stmt
        MOVL      *-SP[10],ACC          ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 369,column 106,is_stmt
        MOVL      *-SP[12],ACC          ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 369,column 128,is_stmt
        MOVL      *-SP[14],ACC          ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 369,column 145,is_stmt
        MOVL      *-SP[16],ACC          ; [CPU_] |369| 
	.dwpsn	file "../gateway.c",line 370,column 22,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |370| 
	.dwpsn	file "../gateway.c",line 373,column 21,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |373| 
	.dwpsn	file "../gateway.c",line 374,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#-1 ; [CPU_] |374| 
	.dwpsn	file "../gateway.c",line 375,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |375| 
        BF        $C$L34,NEQ            ; [CPU_] |375| 
        ; branchcc occurs ; [] |375| 
$C$L33:    
	.dwpsn	file "../gateway.c",line 376,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |376| 
        MOVB      AL,#1                 ; [CPU_] |376| 
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$281, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |376| 
        ; call occurs [#_SEM_pend] ; [] |376| 
	.dwpsn	file "../gateway.c",line 375,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |375| 
        BF        $C$L33,EQ             ; [CPU_] |375| 
        ; branchcc occurs ; [] |375| 
$C$L34:    
	.dwpsn	file "../gateway.c",line 378,column 10,is_stmt
        CMPB      AL,#1                 ; [CPU_] |378| 
        BF        $C$L36,EQ             ; [CPU_] |378| 
        ; branchcc occurs ; [] |378| 
$C$L35:    
	.dwpsn	file "../gateway.c",line 380,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |380| 
        MOVB      AL,#1                 ; [CPU_] |380| 
$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$282, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |380| 
        ; call occurs [#_SEM_pend] ; [] |380| 
	.dwpsn	file "../gateway.c",line 378,column 10,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |378| 
        CMPB      AL,#1                 ; [CPU_] |378| 
        BF        $C$L35,NEQ            ; [CPU_] |378| 
        ; branchcc occurs ; [] |378| 
$C$L36:    
	.dwpsn	file "../gateway.c",line 382,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |382| 
        MOVB      AL,#10                ; [CPU_] |382| 
$C$DW$283	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$283, DW_AT_low_pc(0x00)
	.dwattr $C$DW$283, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$283, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |382| 
        ; call occurs [#_SEM_pend] ; [] |382| 
	.dwpsn	file "../gateway.c",line 383,column 3,is_stmt
        MOVW      DP,#_ODV_MachineEvent ; [CPU_U] 
        MOV       @_ODV_MachineEvent,#0 ; [CPU_] |383| 
	.dwpsn	file "../gateway.c",line 384,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |384| 
	.dwpsn	file "../gateway.c",line 385,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |385| 
	.dwpsn	file "../gateway.c",line 386,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |386| 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |386| 
	.dwpsn	file "../gateway.c",line 387,column 3,is_stmt
        MOVB      XAR5,#0               ; [CPU_] |387| 
        MOVB      AL,#9                 ; [CPU_] |387| 
        MOVB      AH,#7                 ; [CPU_] |387| 
        MOVL      XAR4,#_ODV_Gateway_Date_Time ; [CPU_U] |387| 
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$284, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |387| 
        ; call occurs [#_I2C_Command] ; [] |387| 
	.dwpsn	file "../gateway.c",line 388,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |388| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |388| 
        BF        $C$L37,NTC            ; [CPU_] |388| 
        ; branchcc occurs ; [] |388| 
	.dwpsn	file "../gateway.c",line 389,column 5,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xffbf ; [CPU_] |389| 
$C$L37:    
	.dwpsn	file "../gateway.c",line 391,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |391| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |391| 
        BF        $C$L38,TC             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |391| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |391| 
        LSR       AL,1                  ; [CPU_] |391| 
        CMPB      AL,#1                 ; [CPU_] |391| 
        BF        $C$L38,NEQ            ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
	.dwpsn	file "../gateway.c",line 392,column 5,is_stmt
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOV       @_ODP_CommError_TimeOut,#0 ; [CPU_] |392| 
	.dwpsn	file "../gateway.c",line 393,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |393| 
$C$L38:    
	.dwpsn	file "../gateway.c",line 396,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |396| 
        MOV       AL,#500               ; [CPU_] |396| 
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$285, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |396| 
        ; call occurs [#_SEM_pend] ; [] |396| 
	.dwpsn	file "../gateway.c",line 397,column 3,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |397| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        MOV       @_Modules_Present,AL  ; [CPU_] |397| 
	.dwpsn	file "../gateway.c",line 398,column 10,is_stmt
$C$L39:    
	.dwpsn	file "../gateway.c",line 400,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |400| 
        ANDB      AL,#0x0f              ; [CPU_] |400| 
        CMPB      AL,#8                 ; [CPU_] |400| 
        BF        $C$L40,NEQ            ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |400| 
        CMPB      AL,#8                 ; [CPU_] |400| 
        BF        $C$L40,EQ             ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
	.dwpsn	file "../gateway.c",line 401,column 7,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |401| 
        MOVL      *-SP[4],ACC           ; [CPU_] |401| 
	.dwpsn	file "../gateway.c",line 402,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#8,UNC ; [CPU_] |402| 
$C$L40:    
	.dwpsn	file "../gateway.c",line 404,column 5,is_stmt
        MOVW      DP,#_Sync_count       ; [CPU_U] 
        MOV       AL,@_Sync_count       ; [CPU_] |404| 
        CMPB      AL,#71                ; [CPU_] |404| 
        BF        $C$L42,NEQ            ; [CPU_] |404| 
        ; branchcc occurs ; [] |404| 
	.dwpsn	file "../gateway.c",line 405,column 7,is_stmt
        DEC       @_Sync_count          ; [CPU_] |405| 
	.dwpsn	file "../gateway.c",line 406,column 7,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOVU      ACC,@_ODP_NbOfModules ; [CPU_] |406| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        MOVZ      AR6,@_Modules_Present ; [CPU_] |406| 
        CMPL      ACC,XAR6              ; [CPU_] |406| 
        BF        $C$L41,EQ             ; [CPU_] |406| 
        ; branchcc occurs ; [] |406| 
	.dwpsn	file "../gateway.c",line 407,column 9,is_stmt
        INC       *-SP[17]              ; [CPU_] |407| 
	.dwpsn	file "../gateway.c",line 408,column 9,is_stmt
        MOVW      DP,#_ODP_Gateway_IsoResistor_Limit_Max ; [CPU_U] 
        MOVZ      AR6,*-SP[17]          ; [CPU_] |408| 
        MOV       AL,@_ODP_Gateway_IsoResistor_Limit_Max ; [CPU_] |408| 
        ADDB      AL,#6                 ; [CPU_] |408| 
        MOVU      ACC,AL                ; [CPU_] |408| 
        CMPL      ACC,XAR6              ; [CPU_] |408| 
        BF        $C$L42,NEQ            ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |408| 
        BF        $C$L42,NEQ            ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
	.dwpsn	file "../gateway.c",line 409,column 11,is_stmt
        MOV       AL,#128               ; [CPU_] |409| 
        MOV       AH,#32768             ; [CPU_] |409| 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |409| 
	.dwpsn	file "../gateway.c",line 410,column 11,is_stmt
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |410| 
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$286, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |410| 
        ; call occurs [#_ERR_SetError] ; [] |410| 
	.dwpsn	file "../gateway.c",line 412,column 7,is_stmt
        B         $C$L42,UNC            ; [CPU_] |412| 
        ; branch occurs ; [] |412| 
$C$L41:    
	.dwpsn	file "../gateway.c",line 413,column 12,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |413| 
        BF        $C$L42,EQ             ; [CPU_] |413| 
        ; branchcc occurs ; [] |413| 
	.dwpsn	file "../gateway.c",line 414,column 9,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |414| 
$C$L42:    
	.dwpsn	file "../gateway.c",line 417,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |417| 
        MOV       AL,AH                 ; [CPU_] |417| 
        ASR       AL,1                  ; [CPU_] |417| 
        LSR       AL,14                 ; [CPU_] |417| 
        ADD       AL,AH                 ; [CPU_] |417| 
        ASR       AL,2                  ; [CPU_] |417| 
        MOV       *-SP[19],AL           ; [CPU_] |417| 
	.dwpsn	file "../gateway.c",line 418,column 5,is_stmt
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |418| 
        MOV       AL,AH                 ; [CPU_] |418| 
        MOVW      DP,#_ODV_Gateway_Test_Voltage ; [CPU_U] 
        LSR       AL,15                 ; [CPU_] |418| 
        ADD       AL,AH                 ; [CPU_] |418| 
        ASR       AL,1                  ; [CPU_] |418| 
        MOV       @_ODV_Gateway_Test_Voltage,AL ; [CPU_] |418| 
	.dwpsn	file "../gateway.c",line 419,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |419| 
        B         $C$L110,LEQ           ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
        CMPB      AL,#4                 ; [CPU_] |419| 
        B         $C$L110,GT            ; [CPU_] |419| 
        ; branchcc occurs ; [] |419| 
	.dwpsn	file "../gateway.c",line 420,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |420| 
        BF        $C$L44,NEQ            ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
	.dwpsn	file "../gateway.c",line 421,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |421| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |421| 
        BF        $C$L43,TC             ; [CPU_] |421| 
        ; branchcc occurs ; [] |421| 
	.dwpsn	file "../gateway.c",line 422,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |422| 
$C$L43:    
	.dwpsn	file "../gateway.c",line 423,column 9,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |423| 
	.dwpsn	file "../gateway.c",line 424,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |424| 
	.dwpsn	file "../gateway.c",line 425,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xefff ; [CPU_] |425| 
$C$L44:    
	.dwpsn	file "../gateway.c",line 428,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOVIZ     R1H,#16512            ; [CPU_] |428| 
        I16TOF32  R0H,@_ODV_Gateway_Current ; [CPU_] |428| 
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$287, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |428| 
        ; call occurs [#FS$$DIV] ; [] |428| 
        MOV32     *-SP[22],R0H          ; [CPU_] |428| 
	.dwpsn	file "../gateway.c",line 429,column 7,is_stmt
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$288, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |429| 
        ; call occurs [#_CNV_Round] ; [] |429| 
        MOV       *-SP[18],AL           ; [CPU_] |429| 
	.dwpsn	file "../gateway.c",line 431,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |431| 
        CMP       AL,*-SP[18]           ; [CPU_] |431| 
        B         $C$L45,LT             ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |431| 
        CMP       AL,*-SP[18]           ; [CPU_] |431| 
        B         $C$L46,LEQ            ; [CPU_] |431| 
        ; branchcc occurs ; [] |431| 
$C$L45:    
	.dwpsn	file "../gateway.c",line 433,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |433| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        ADDL      @_GV_current_counter,ACC ; [CPU_] |433| 
	.dwpsn	file "../gateway.c",line 434,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |434| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |434| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        CMPL      ACC,@_GV_current_counter ; [CPU_] |434| 
        B         $C$L46,HIS            ; [CPU_] |434| 
        ; branchcc occurs ; [] |434| 
	.dwpsn	file "../gateway.c",line 435,column 9,is_stmt
$C$DW$289	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$289, DW_AT_low_pc(0x00)
	.dwattr $C$DW$289, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$289, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |435| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |435| 
$C$L46:    
	.dwpsn	file "../gateway.c",line 439,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |439| 
        CMP       AL,*-SP[19]           ; [CPU_] |439| 
        B         $C$L47,GEQ            ; [CPU_] |439| 
        ; branchcc occurs ; [] |439| 
	.dwpsn	file "../gateway.c",line 441,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |441| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        ADDL      @_GV_voltage_counter,ACC ; [CPU_] |441| 
	.dwpsn	file "../gateway.c",line 442,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |442| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |442| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        CMPL      ACC,@_GV_voltage_counter ; [CPU_] |442| 
        B         $C$L47,HIS            ; [CPU_] |442| 
        ; branchcc occurs ; [] |442| 
	.dwpsn	file "../gateway.c",line 443,column 9,is_stmt
$C$DW$290	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$290, DW_AT_low_pc(0x00)
	.dwattr $C$DW$290, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$290, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |443| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |443| 
$C$L47:    
	.dwpsn	file "../gateway.c",line 448,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |448| 
        CMP       AL,*-SP[19]           ; [CPU_] |448| 
        B         $C$L48,LEQ            ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
	.dwpsn	file "../gateway.c",line 450,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |450| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        ADDL      @_GV_voltage_counter,ACC ; [CPU_] |450| 
	.dwpsn	file "../gateway.c",line 451,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |451| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |451| 
        MOVW      DP,#_GV_voltage_counter ; [CPU_U] 
        CMPL      ACC,@_GV_voltage_counter ; [CPU_] |451| 
        B         $C$L48,HIS            ; [CPU_] |451| 
        ; branchcc occurs ; [] |451| 
	.dwpsn	file "../gateway.c",line 453,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |453| 
	.dwpsn	file "../gateway.c",line 454,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |454| 
        MOVL      *-SP[4],ACC           ; [CPU_] |454| 
	.dwpsn	file "../gateway.c",line 455,column 9,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |455| 
$C$L48:    
	.dwpsn	file "../gateway.c",line 460,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |460| 
        CMP       AL,*-SP[19]           ; [CPU_] |460| 
        B         $C$L49,LEQ            ; [CPU_] |460| 
        ; branchcc occurs ; [] |460| 
	.dwpsn	file "../gateway.c",line 462,column 5,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |462| 
        MOVL      ACC,XAR4              ; [CPU_] |462| 
$C$DW$291	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$291, DW_AT_low_pc(0x00)
	.dwattr $C$DW$291, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$291, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |462| 
        ; call occurs [#_ERR_HandleWarning] ; [] |462| 
	.dwpsn	file "../gateway.c",line 463,column 7,is_stmt
        B         $C$L50,UNC            ; [CPU_] |463| 
        ; branch occurs ; [] |463| 
$C$L49:    
	.dwpsn	file "../gateway.c",line 466,column 5,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |466| 
        MOVL      ACC,XAR4              ; [CPU_] |466| 
$C$DW$292	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$292, DW_AT_low_pc(0x00)
	.dwattr $C$DW$292, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$292, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |466| 
        ; call occurs [#_ERR_ClearWarning] ; [] |466| 
$C$L50:    
	.dwpsn	file "../gateway.c",line 470,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |470| 
        CMP       AL,*-SP[19]           ; [CPU_] |470| 
        B         $C$L51,GEQ            ; [CPU_] |470| 
        ; branchcc occurs ; [] |470| 
	.dwpsn	file "../gateway.c",line 472,column 5,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |472| 
        MOVL      ACC,XAR4              ; [CPU_] |472| 
$C$DW$293	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$293, DW_AT_low_pc(0x00)
	.dwattr $C$DW$293, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$293, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |472| 
        ; call occurs [#_ERR_HandleWarning] ; [] |472| 
	.dwpsn	file "../gateway.c",line 473,column 7,is_stmt
        B         $C$L52,UNC            ; [CPU_] |473| 
        ; branch occurs ; [] |473| 
$C$L51:    
	.dwpsn	file "../gateway.c",line 476,column 5,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |476| 
        MOVL      ACC,XAR4              ; [CPU_] |476| 
$C$DW$294	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$294, DW_AT_low_pc(0x00)
	.dwattr $C$DW$294, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$294, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |476| 
        ; call occurs [#_ERR_ClearWarning] ; [] |476| 
$C$L52:    
	.dwpsn	file "../gateway.c",line 479,column 7,is_stmt
        MOV32     R0H,*-SP[22]          ; [CPU_] |479| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |479| 
        NOP       ; [CPU_] 
        CMPF32    R0H,#0                ; [CPU_] |479| 
        MOVST0    ZF, NF                ; [CPU_] |479| 
        B         $C$L53,LT             ; [CPU_] |479| 
        ; branchcc occurs ; [] |479| 
        MOV32     R0H,*-SP[22]          ; [CPU_] |479| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |479| 
        B         $C$L54,UNC            ; [CPU_] |479| 
        ; branch occurs ; [] |479| 
$C$L53:    
        MOV32     R0H,*-SP[22]          ; [CPU_] |479| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |479| 
        NOP       ; [CPU_] 
        NEGF32    R0H,R0H               ; [CPU_] |479| 
$C$L54:    
        MOVW      DP,#_ODP_Sleep_Current ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_Sleep_Current ; [CPU_] |479| 
        NOP       ; [CPU_] 
        CMPF32    R0H,R1H               ; [CPU_] |479| 
        MOVST0    ZF, NF                ; [CPU_] |479| 
        B         $C$L55,GEQ            ; [CPU_] |479| 
        ; branchcc occurs ; [] |479| 
	.dwpsn	file "../gateway.c",line 480,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |480| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |480| 
        MOVL      *-SP[12],ACC          ; [CPU_] |480| 
	.dwpsn	file "../gateway.c",line 481,column 9,is_stmt
        MOV       T,#1000               ; [CPU_] |481| 
        MOVW      DP,#_ODP_Sleep_Timeout ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_Sleep_Timeout ; [CPU_] |481| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |481| 
        B         $C$L56,HIS            ; [CPU_] |481| 
        ; branchcc occurs ; [] |481| 
	.dwpsn	file "../gateway.c",line 482,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |482| 
        MOVL      *-SP[12],ACC          ; [CPU_] |482| 
	.dwpsn	file "../gateway.c",line 483,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#64,UNC ; [CPU_] |483| 
	.dwpsn	file "../gateway.c",line 484,column 11,is_stmt
        MOV       AL,#0                 ; [CPU_] |484| 
        MOV       AH,#64                ; [CPU_] |484| 
$C$DW$295	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$295, DW_AT_low_pc(0x00)
	.dwattr $C$DW$295, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$295, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |484| 
        ; call occurs [#_ERR_HandleWarning] ; [] |484| 
	.dwpsn	file "../gateway.c",line 486,column 7,is_stmt
        B         $C$L56,UNC            ; [CPU_] |486| 
        ; branch occurs ; [] |486| 
$C$L55:    
	.dwpsn	file "../gateway.c",line 487,column 12,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |487| 
        BF        $C$L56,EQ             ; [CPU_] |487| 
        ; branchcc occurs ; [] |487| 
	.dwpsn	file "../gateway.c",line 487,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |487| 
        SUBL      *-SP[12],ACC          ; [CPU_] |487| 
$C$L56:    
	.dwpsn	file "../gateway.c",line 488,column 7,is_stmt
        MOVW      DP,#_ODP_RelayResetTime ; [CPU_U] 
        MOV       AL,@_ODP_RelayResetTime ; [CPU_] |488| 
        BF        $C$L58,EQ             ; [CPU_] |488| 
        ; branchcc occurs ; [] |488| 
        MOV       AL,*-SP[19]           ; [CPU_] |488| 
        CMPB      AL,#8                 ; [CPU_] |488| 
        B         $C$L58,GEQ            ; [CPU_] |488| 
        ; branchcc occurs ; [] |488| 
	.dwpsn	file "../gateway.c",line 489,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |489| 
        ADDL      ACC,*-SP[16]          ; [CPU_] |489| 
        MOVL      *-SP[16],ACC          ; [CPU_] |489| 
	.dwpsn	file "../gateway.c",line 490,column 9,is_stmt
        MOV       T,#60000              ; [CPU_] |490| 
        MPYU      ACC,T,@_ODP_RelayResetTime ; [CPU_] |490| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |490| 
        B         $C$L57,HI             ; [CPU_] |490| 
        ; branchcc occurs ; [] |490| 
	.dwpsn	file "../gateway.c",line 491,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |491| 
        MOVL      *-SP[16],ACC          ; [CPU_] |491| 
	.dwpsn	file "../gateway.c",line 492,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |492| 
	.dwpsn	file "../gateway.c",line 493,column 9,is_stmt
        B         $C$L59,UNC            ; [CPU_] |493| 
        ; branch occurs ; [] |493| 
$C$L57:    
	.dwpsn	file "../gateway.c",line 494,column 14,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#50000            ; [CPU_] |494| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |494| 
        B         $C$L59,HI             ; [CPU_] |494| 
        ; branchcc occurs ; [] |494| 
	.dwpsn	file "../gateway.c",line 494,column 40,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |494| 
	.dwpsn	file "../gateway.c",line 495,column 7,is_stmt
        B         $C$L59,UNC            ; [CPU_] |495| 
        ; branch occurs ; [] |495| 
$C$L58:    
	.dwpsn	file "../gateway.c",line 496,column 12,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |496| 
        BF        $C$L59,EQ             ; [CPU_] |496| 
        ; branchcc occurs ; [] |496| 
	.dwpsn	file "../gateway.c",line 496,column 33,is_stmt
        MOVB      ACC,#0                ; [CPU_] |496| 
        MOVL      *-SP[16],ACC          ; [CPU_] |496| 
$C$L59:    
	.dwpsn	file "../gateway.c",line 497,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmax ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Resistor_Tmax ; [CPU_] |497| 
        MPYB      ACC,T,#10             ; [CPU_] |497| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_] |497| 
        B         $C$L60,LEQ            ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmin ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Resistor_Tmin ; [CPU_] |497| 
        MPYB      ACC,T,#10             ; [CPU_] |497| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_] |497| 
        B         $C$L60,GEQ            ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
        MOVW      DP,#_ODP_SafetyLimits_Tmax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Tmax ; [CPU_] |497| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_Temperature ; [CPU_] |497| 
        B         $C$L60,LT             ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Tmin ; [CPU_] |497| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_Temperature ; [CPU_] |497| 
        B         $C$L63,LEQ            ; [CPU_] |497| 
        ; branchcc occurs ; [] |497| 
$C$L60:    
	.dwpsn	file "../gateway.c",line 500,column 8,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       @_ODV_Read_Analogue_Input_16_Bit+1,#-550 ; [CPU_] |500| 
        BF        $C$L61,EQ             ; [CPU_] |500| 
        ; branchcc occurs ; [] |500| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       @_ODV_Gateway_Temperature,#-55 ; [CPU_] |500| 
        BF        $C$L62,NEQ            ; [CPU_] |500| 
        ; branchcc occurs ; [] |500| 
$C$L61:    
	.dwpsn	file "../gateway.c",line 502,column 9,is_stmt
        MOVB      ACC,#0                ; [CPU_] |502| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        MOVL      @_GV_Temp_counter,ACC ; [CPU_] |502| 
$C$L62:    
	.dwpsn	file "../gateway.c",line 504,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |504| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        ADDL      @_GV_Temp_counter,ACC ; [CPU_] |504| 
	.dwpsn	file "../gateway.c",line 505,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |505| 
        MOVW      DP,#_ODP_Settings_AUD_Temperature_Delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_Settings_AUD_Temperature_Delay ; [CPU_] |505| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        CMPL      ACC,@_GV_Temp_counter ; [CPU_] |505| 
        B         $C$L64,HIS            ; [CPU_] |505| 
        ; branchcc occurs ; [] |505| 
	.dwpsn	file "../gateway.c",line 507,column 12,is_stmt
$C$DW$296	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$296, DW_AT_low_pc(0x00)
	.dwattr $C$DW$296, DW_AT_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$296, DW_AT_TI_call
        LCR       #_ERR_ErrorOverTemp   ; [CPU_] |507| 
        ; call occurs [#_ERR_ErrorOverTemp] ; [] |507| 
	.dwpsn	file "../gateway.c",line 509,column 7,is_stmt
        B         $C$L64,UNC            ; [CPU_] |509| 
        ; branch occurs ; [] |509| 
$C$L63:    
	.dwpsn	file "../gateway.c",line 512,column 8,is_stmt
        MOVB      ACC,#0                ; [CPU_] |512| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        MOVL      @_GV_Temp_counter,ACC ; [CPU_] |512| 
$C$L64:    
	.dwpsn	file "../gateway.c",line 514,column 7,is_stmt
        MOVW      DP,#_ODP_Temperature_WarningMax ; [CPU_U] 
        MOV       AL,@_ODP_Temperature_WarningMax ; [CPU_] |514| 
        MOVW      DP,#_ODV_Gateway_MaxModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxModTemp ; [CPU_] |514| 
        B         $C$L65,LT             ; [CPU_] |514| 
        ; branchcc occurs ; [] |514| 
        MOVW      DP,#_ODP_Temperature_WarningMin ; [CPU_U] 
        MOV       AL,@_ODP_Temperature_WarningMin ; [CPU_] |514| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |514| 
        B         $C$L66,LEQ            ; [CPU_] |514| 
        ; branchcc occurs ; [] |514| 
$C$L65:    
	.dwpsn	file "../gateway.c",line 516,column 9,is_stmt
        MOVL      XAR4,#262144          ; [CPU_U] |516| 
        MOVL      ACC,XAR4              ; [CPU_] |516| 
$C$DW$297	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$297, DW_AT_low_pc(0x00)
	.dwattr $C$DW$297, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$297, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |516| 
        ; call occurs [#_ERR_HandleWarning] ; [] |516| 
	.dwpsn	file "../gateway.c",line 517,column 7,is_stmt
        B         $C$L67,UNC            ; [CPU_] |517| 
        ; branch occurs ; [] |517| 
$C$L66:    
	.dwpsn	file "../gateway.c",line 518,column 12,is_stmt
        MOVL      XAR4,#262144          ; [CPU_U] |518| 
        MOVL      ACC,XAR4              ; [CPU_] |518| 
$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$298, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |518| 
        ; call occurs [#_ERR_ClearWarning] ; [] |518| 
$C$L67:    
	.dwpsn	file "../gateway.c",line 519,column 7,is_stmt
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |519| 
        NEG       AL                    ; [CPU_] |519| 
        CMP       AL,*-SP[18]           ; [CPU_] |519| 
        B         $C$L68,GT             ; [CPU_] |519| 
        ; branchcc occurs ; [] |519| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |519| 
        CMP       AL,*-SP[18]           ; [CPU_] |519| 
        B         $C$L69,GEQ            ; [CPU_] |519| 
        ; branchcc occurs ; [] |519| 
$C$L68:    
	.dwpsn	file "../gateway.c",line 520,column 9,is_stmt
        MOVL      XAR4,#131072          ; [CPU_U] |520| 
        MOVL      ACC,XAR4              ; [CPU_] |520| 
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$299, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |520| 
        ; call occurs [#_ERR_HandleWarning] ; [] |520| 
	.dwpsn	file "../gateway.c",line 525,column 7,is_stmt
        B         $C$L110,UNC           ; [CPU_] |525| 
        ; branch occurs ; [] |525| 
$C$L69:    
	.dwpsn	file "../gateway.c",line 526,column 12,is_stmt
        MOVL      XAR4,#131072          ; [CPU_U] |526| 
        MOVL      ACC,XAR4              ; [CPU_] |526| 
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$300, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |526| 
        ; call occurs [#_ERR_ClearWarning] ; [] |526| 
	.dwpsn	file "../gateway.c",line 529,column 5,is_stmt
        B         $C$L110,UNC           ; [CPU_] |529| 
        ; branch occurs ; [] |529| 
$C$L70:    
	.dwpsn	file "../gateway.c",line 532,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |532| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |532| 
        LSR       AL,1                  ; [CPU_] |532| 
        CMPB      AL,#1                 ; [CPU_] |532| 
        BF        $C$L71,NEQ            ; [CPU_] |532| 
        ; branchcc occurs ; [] |532| 
	.dwpsn	file "../gateway.c",line 533,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |533| 
	.dwpsn	file "../gateway.c",line 534,column 9,is_stmt
        B         $C$L72,UNC            ; [CPU_] |534| 
        ; branch occurs ; [] |534| 
$C$L71:    
	.dwpsn	file "../gateway.c",line 535,column 14,is_stmt
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |535| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |535| 
        BF        $C$L72,TC             ; [CPU_] |535| 
        ; branchcc occurs ; [] |535| 
	.dwpsn	file "../gateway.c",line 536,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |536| 
$C$L72:    
	.dwpsn	file "../gateway.c",line 538,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |538| 
        CMP       AL,*-SP[19]           ; [CPU_] |538| 
        B         $C$L73,LEQ            ; [CPU_] |538| 
        ; branchcc occurs ; [] |538| 
	.dwpsn	file "../gateway.c",line 539,column 10,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |539| 
$C$L73:    
	.dwpsn	file "../gateway.c",line 541,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |541| 
        BF        $C$L112,EQ            ; [CPU_] |541| 
        ; branchcc occurs ; [] |541| 
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOVU      ACC,@_ODP_NbOfModules ; [CPU_] |541| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        MOVZ      AR6,@_Modules_Present ; [CPU_] |541| 
        CMPL      ACC,XAR6              ; [CPU_] |541| 
        BF        $C$L112,NEQ           ; [CPU_] |541| 
        ; branchcc occurs ; [] |541| 
	.dwpsn	file "../gateway.c",line 542,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |542| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |542| 
        BF        $C$L74,TC             ; [CPU_] |542| 
        ; branchcc occurs ; [] |542| 
	.dwpsn	file "../gateway.c",line 542,column 41,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |542| 
$C$L74:    
	.dwpsn	file "../gateway.c",line 543,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xffbf ; [CPU_] |543| 
	.dwpsn	file "../gateway.c",line 544,column 11,is_stmt
        OR        @_GpioDataRegs,#0x1000 ; [CPU_] |544| 
	.dwpsn	file "../gateway.c",line 545,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |545| 
        MOVL      *-SP[4],ACC           ; [CPU_] |545| 
	.dwpsn	file "../gateway.c",line 546,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |546| 
	.dwpsn	file "../gateway.c",line 547,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#6,UNC ; [CPU_] |547| 
	.dwpsn	file "../gateway.c",line 549,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |549| 
        ; branch occurs ; [] |549| 
$C$L75:    
	.dwpsn	file "../gateway.c",line 552,column 9,is_stmt
        MOVB      XAR6,#100             ; [CPU_] |552| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |552| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |552| 
        CMPL      ACC,XAR6              ; [CPU_] |552| 
        B         $C$L76,LO             ; [CPU_] |552| 
        ; branchcc occurs ; [] |552| 
	.dwpsn	file "../gateway.c",line 552,column 44,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |552| 
$C$L76:    
	.dwpsn	file "../gateway.c",line 553,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |553| 
        BF        $C$L112,EQ            ; [CPU_] |553| 
        ; branchcc occurs ; [] |553| 
	.dwpsn	file "../gateway.c",line 554,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Voltage ; [CPU_] |554| 
        B         $C$L78,LT             ; [CPU_] |554| 
        ; branchcc occurs ; [] |554| 
	.dwpsn	file "../gateway.c",line 555,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |555| 
        MOVL      *-SP[4],ACC           ; [CPU_] |555| 
	.dwpsn	file "../gateway.c",line 556,column 13,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |556| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |556| 
        BF        $C$L77,TC             ; [CPU_] |556| 
        ; branchcc occurs ; [] |556| 
	.dwpsn	file "../gateway.c",line 561,column 15,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |561| 
	.dwpsn	file "../gateway.c",line 562,column 15,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#3,UNC ; [CPU_] |562| 
	.dwpsn	file "../gateway.c",line 564,column 13,is_stmt
        B         $C$L112,UNC           ; [CPU_] |564| 
        ; branch occurs ; [] |564| 
$C$L77:    
	.dwpsn	file "../gateway.c",line 566,column 15,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#4,UNC ; [CPU_] |566| 
	.dwpsn	file "../gateway.c",line 567,column 15,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |567| 
	.dwpsn	file "../gateway.c",line 569,column 11,is_stmt
        B         $C$L112,UNC           ; [CPU_] |569| 
        ; branch occurs ; [] |569| 
$C$L78:    
	.dwpsn	file "../gateway.c",line 571,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |571| 
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$301, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |571| 
        ; call occurs [#_ERR_SetError] ; [] |571| 
	.dwpsn	file "../gateway.c",line 574,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |574| 
        ; branch occurs ; [] |574| 
$C$L79:    
	.dwpsn	file "../gateway.c",line 577,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |577| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |577| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |577| 
        CMPL      ACC,XAR6              ; [CPU_] |577| 
        B         $C$L80,LO             ; [CPU_] |577| 
        ; branchcc occurs ; [] |577| 
	.dwpsn	file "../gateway.c",line 577,column 43,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |577| 
$C$L80:    
	.dwpsn	file "../gateway.c",line 578,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |578| 
        CMP       AL,*-SP[19]           ; [CPU_] |578| 
        B         $C$L81,GT             ; [CPU_] |578| 
        ; branchcc occurs ; [] |578| 
        MOV       AL,*-SP[23]           ; [CPU_] |578| 
        BF        $C$L81,EQ             ; [CPU_] |578| 
        ; branchcc occurs ; [] |578| 
	.dwpsn	file "../gateway.c",line 579,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |579| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |579| 
        MOVL      *-SP[10],ACC          ; [CPU_] |579| 
	.dwpsn	file "../gateway.c",line 580,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |580| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |580| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |580| 
        B         $C$L82,HIS            ; [CPU_] |580| 
        ; branchcc occurs ; [] |580| 
	.dwpsn	file "../gateway.c",line 580,column 78,is_stmt
$C$DW$302	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$302, DW_AT_low_pc(0x00)
	.dwattr $C$DW$302, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$302, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |580| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |580| 
	.dwpsn	file "../gateway.c",line 581,column 9,is_stmt
        B         $C$L82,UNC            ; [CPU_] |581| 
        ; branch occurs ; [] |581| 
$C$L81:    
	.dwpsn	file "../gateway.c",line 582,column 14,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |582| 
        BF        $C$L82,EQ             ; [CPU_] |582| 
        ; branchcc occurs ; [] |582| 
	.dwpsn	file "../gateway.c",line 582,column 39,is_stmt
        MOVB      ACC,#1                ; [CPU_] |582| 
        SUBL      *-SP[10],ACC          ; [CPU_] |582| 
$C$L82:    
	.dwpsn	file "../gateway.c",line 583,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |583| 
        CMP       AL,*-SP[18]           ; [CPU_] |583| 
        B         $C$L83,GT             ; [CPU_] |583| 
        ; branchcc occurs ; [] |583| 
        MOV       AL,*-SP[23]           ; [CPU_] |583| 
        BF        $C$L83,EQ             ; [CPU_] |583| 
        ; branchcc occurs ; [] |583| 
	.dwpsn	file "../gateway.c",line 584,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |584| 
        ADDL      ACC,*-SP[14]          ; [CPU_] |584| 
        MOVL      *-SP[14],ACC          ; [CPU_] |584| 
	.dwpsn	file "../gateway.c",line 585,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |585| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |585| 
        CMPL      ACC,*-SP[14]          ; [CPU_] |585| 
        B         $C$L84,HIS            ; [CPU_] |585| 
        ; branchcc occurs ; [] |585| 
	.dwpsn	file "../gateway.c",line 585,column 79,is_stmt
$C$DW$303	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$303, DW_AT_low_pc(0x00)
	.dwattr $C$DW$303, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$303, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |585| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |585| 
	.dwpsn	file "../gateway.c",line 586,column 9,is_stmt
        B         $C$L84,UNC            ; [CPU_] |586| 
        ; branch occurs ; [] |586| 
$C$L83:    
	.dwpsn	file "../gateway.c",line 587,column 14,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |587| 
        BF        $C$L84,EQ             ; [CPU_] |587| 
        ; branchcc occurs ; [] |587| 
	.dwpsn	file "../gateway.c",line 587,column 40,is_stmt
        MOVB      ACC,#1                ; [CPU_] |587| 
        SUBL      *-SP[14],ACC          ; [CPU_] |587| 
$C$L84:    
	.dwpsn	file "../gateway.c",line 588,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |588| 
        CMP       AL,*-SP[19]           ; [CPU_] |588| 
        B         $C$L85,LEQ            ; [CPU_] |588| 
        ; branchcc occurs ; [] |588| 
        MOV       AL,*-SP[23]           ; [CPU_] |588| 
        BF        $C$L85,EQ             ; [CPU_] |588| 
        ; branchcc occurs ; [] |588| 
	.dwpsn	file "../gateway.c",line 589,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |589| 
	.dwpsn	file "../gateway.c",line 590,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |590| 
        MOVL      *-SP[4],ACC           ; [CPU_] |590| 
	.dwpsn	file "../gateway.c",line 591,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |591| 
$C$L85:    
	.dwpsn	file "../gateway.c",line 593,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |593| 
        ANDB      AL,#0x0f              ; [CPU_] |593| 
        CMPB      AL,#1                 ; [CPU_] |593| 
        BF        $C$L86,NEQ            ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |593| 
        CMP       AL,*-SP[19]           ; [CPU_] |593| 
        B         $C$L86,LEQ            ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
        MOV       AL,*-SP[23]           ; [CPU_] |593| 
        BF        $C$L86,EQ             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
	.dwpsn	file "../gateway.c",line 594,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |594| 
	.dwpsn	file "../gateway.c",line 595,column 11,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |595| 
        MOVL      ACC,XAR4              ; [CPU_] |595| 
$C$DW$304	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$304, DW_AT_low_pc(0x00)
	.dwattr $C$DW$304, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$304, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |595| 
        ; call occurs [#_ERR_ClearWarning] ; [] |595| 
	.dwpsn	file "../gateway.c",line 596,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |596| 
        MOVL      *-SP[4],ACC           ; [CPU_] |596| 
	.dwpsn	file "../gateway.c",line 597,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |597| 
$C$L86:    
	.dwpsn	file "../gateway.c",line 599,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |599| 
        BF        $C$L112,EQ            ; [CPU_] |599| 
        ; branchcc occurs ; [] |599| 
        MOVIZ     R0H,#15948            ; [CPU_] |599| 
        MOV32     R1H,*-SP[22]          ; [CPU_] |599| 
        MOVXI     R0H,#52429            ; [CPU_] |599| 
        CMPF32    R1H,R0H               ; [CPU_] |599| 
        MOVST0    ZF, NF                ; [CPU_] |599| 
        B         $C$L112,LT            ; [CPU_] |599| 
        ; branchcc occurs ; [] |599| 
	.dwpsn	file "../gateway.c",line 600,column 11,is_stmt
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      ACC,@_PAR_Capacity_Total ; [CPU_] |600| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,ACC ; [CPU_] |600| 
	.dwpsn	file "../gateway.c",line 602,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |602| 
        ; branch occurs ; [] |602| 
$C$L87:    
	.dwpsn	file "../gateway.c",line 606,column 8,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |606| 
        MOVL      ACC,XAR4              ; [CPU_] |606| 
$C$DW$305	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$305, DW_AT_low_pc(0x00)
	.dwattr $C$DW$305, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$305, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |606| 
        ; call occurs [#_ERR_HandleWarning] ; [] |606| 
	.dwpsn	file "../gateway.c",line 607,column 8,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |607| 
	.dwpsn	file "../gateway.c",line 609,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |609| 
        CMP       AL,*-SP[19]           ; [CPU_] |609| 
        B         $C$L88,LT             ; [CPU_] |609| 
        ; branchcc occurs ; [] |609| 
	.dwpsn	file "../gateway.c",line 611,column 10,is_stmt
        MOV       AL,*-SP[18]           ; [CPU_] |611| 
        CMPB      AL,#3                 ; [CPU_] |611| 
        B         $C$L88,GT             ; [CPU_] |611| 
        ; branchcc occurs ; [] |611| 
	.dwpsn	file "../gateway.c",line 613,column 10,is_stmt
	.dwpsn	file "../gateway.c",line 616,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |616| 
        MOVW      DP,#_ODP_Settings_AUD_Safety_Low_Voltage_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_Settings_AUD_Safety_Low_Voltage_Delay ; [CPU_] |616| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVU      ACC,AL                ; [CPU_] |616| 
        CMPL      ACC,@_ODV_SysTick_ms  ; [CPU_] |616| 
        B         $C$L88,HIS            ; [CPU_] |616| 
        ; branchcc occurs ; [] |616| 
	.dwpsn	file "../gateway.c",line 616,column 84,is_stmt
$C$DW$306	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$306, DW_AT_low_pc(0x00)
	.dwattr $C$DW$306, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$306, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |616| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |616| 
$C$L88:    
	.dwpsn	file "../gateway.c",line 620,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |620| 
        CMP       AL,*-SP[19]           ; [CPU_] |620| 
        B         $C$L89,GT             ; [CPU_] |620| 
        ; branchcc occurs ; [] |620| 
	.dwpsn	file "../gateway.c",line 621,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |621| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |621| 
        MOVL      *-SP[10],ACC          ; [CPU_] |621| 
	.dwpsn	file "../gateway.c",line 622,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |622| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |622| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |622| 
        B         $C$L90,HIS            ; [CPU_] |622| 
        ; branchcc occurs ; [] |622| 
	.dwpsn	file "../gateway.c",line 622,column 78,is_stmt
$C$DW$307	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$307, DW_AT_low_pc(0x00)
	.dwattr $C$DW$307, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$307, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |622| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |622| 
	.dwpsn	file "../gateway.c",line 623,column 9,is_stmt
        B         $C$L90,UNC            ; [CPU_] |623| 
        ; branch occurs ; [] |623| 
$C$L89:    
	.dwpsn	file "../gateway.c",line 624,column 14,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |624| 
        BF        $C$L90,EQ             ; [CPU_] |624| 
        ; branchcc occurs ; [] |624| 
	.dwpsn	file "../gateway.c",line 624,column 39,is_stmt
        MOVB      ACC,#1                ; [CPU_] |624| 
        SUBL      *-SP[10],ACC          ; [CPU_] |624| 
$C$L90:    
	.dwpsn	file "../gateway.c",line 626,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |626| 
        NEG       AL                    ; [CPU_] |626| 
        CMP       AL,*-SP[18]           ; [CPU_] |626| 
        B         $C$L91,GEQ            ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |626| 
        NEG       AL                    ; [CPU_] |626| 
        CMP       AL,*-SP[18]           ; [CPU_] |626| 
        B         $C$L92,LT             ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
        MOV       AL,*-SP[23]           ; [CPU_] |626| 
        BF        $C$L92,EQ             ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
$C$L91:    
	.dwpsn	file "../gateway.c",line 628,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |628| 
        ADDL      ACC,*-SP[14]          ; [CPU_] |628| 
        MOVL      *-SP[14],ACC          ; [CPU_] |628| 
	.dwpsn	file "../gateway.c",line 629,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |629| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |629| 
        CMPL      ACC,*-SP[14]          ; [CPU_] |629| 
        B         $C$L93,HIS            ; [CPU_] |629| 
        ; branchcc occurs ; [] |629| 
	.dwpsn	file "../gateway.c",line 629,column 79,is_stmt
$C$DW$308	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$308, DW_AT_low_pc(0x00)
	.dwattr $C$DW$308, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$308, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |629| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |629| 
	.dwpsn	file "../gateway.c",line 630,column 9,is_stmt
        B         $C$L93,UNC            ; [CPU_] |630| 
        ; branch occurs ; [] |630| 
$C$L92:    
	.dwpsn	file "../gateway.c",line 631,column 14,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |631| 
        BF        $C$L93,EQ             ; [CPU_] |631| 
        ; branchcc occurs ; [] |631| 
	.dwpsn	file "../gateway.c",line 631,column 40,is_stmt
        MOVB      ACC,#1                ; [CPU_] |631| 
        SUBL      *-SP[14],ACC          ; [CPU_] |631| 
$C$L93:    
	.dwpsn	file "../gateway.c",line 633,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |633| 
        ANDB      AL,#0x0f              ; [CPU_] |633| 
        CMPB      AL,#1                 ; [CPU_] |633| 
        BF        $C$L112,NEQ           ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |633| 
        CMP       AL,*-SP[19]           ; [CPU_] |633| 
        B         $C$L112,GEQ           ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
        MOV       AL,*-SP[23]           ; [CPU_] |633| 
        BF        $C$L112,EQ            ; [CPU_] |633| 
        ; branchcc occurs ; [] |633| 
	.dwpsn	file "../gateway.c",line 634,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |634| 
	.dwpsn	file "../gateway.c",line 635,column 11,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |635| 
        MOVL      ACC,XAR4              ; [CPU_] |635| 
$C$DW$309	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$309, DW_AT_low_pc(0x00)
	.dwattr $C$DW$309, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$309, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |635| 
        ; call occurs [#_ERR_ClearWarning] ; [] |635| 
	.dwpsn	file "../gateway.c",line 636,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |636| 
        MOVL      *-SP[4],ACC           ; [CPU_] |636| 
	.dwpsn	file "../gateway.c",line 637,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |637| 
	.dwpsn	file "../gateway.c",line 639,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |639| 
        ; branch occurs ; [] |639| 
$C$L94:    
	.dwpsn	file "../gateway.c",line 643,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |643| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |643| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |643| 
        CMPL      ACC,XAR6              ; [CPU_] |643| 
        B         $C$L95,LO             ; [CPU_] |643| 
        ; branchcc occurs ; [] |643| 
	.dwpsn	file "../gateway.c",line 643,column 43,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |643| 
$C$L95:    
	.dwpsn	file "../gateway.c",line 644,column 9,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |644| 
        BF        $C$L96,EQ             ; [CPU_] |644| 
        ; branchcc occurs ; [] |644| 
	.dwpsn	file "../gateway.c",line 644,column 34,is_stmt
        MOVB      ACC,#1                ; [CPU_] |644| 
        SUBL      *-SP[10],ACC          ; [CPU_] |644| 
$C$L96:    
	.dwpsn	file "../gateway.c",line 645,column 9,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |645| 
        BF        $C$L97,EQ             ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
	.dwpsn	file "../gateway.c",line 645,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |645| 
        SUBL      *-SP[14],ACC          ; [CPU_] |645| 
$C$L97:    
	.dwpsn	file "../gateway.c",line 646,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |646| 
        ANDB      AL,#0x0f              ; [CPU_] |646| 
        CMPB      AL,#2                 ; [CPU_] |646| 
        BF        $C$L98,EQ             ; [CPU_] |646| 
        ; branchcc occurs ; [] |646| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |646| 
        CMP       AL,*-SP[19]           ; [CPU_] |646| 
        B         $C$L99,LT             ; [CPU_] |646| 
        ; branchcc occurs ; [] |646| 
        MOV       AL,*-SP[23]           ; [CPU_] |646| 
        BF        $C$L99,EQ             ; [CPU_] |646| 
        ; branchcc occurs ; [] |646| 
$C$L98:    
	.dwpsn	file "../gateway.c",line 647,column 11,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |647| 
        MOVL      ACC,XAR4              ; [CPU_] |647| 
$C$DW$310	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$310, DW_AT_low_pc(0x00)
	.dwattr $C$DW$310, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$310, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |647| 
        ; call occurs [#_ERR_HandleWarning] ; [] |647| 
	.dwpsn	file "../gateway.c",line 648,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |648| 
	.dwpsn	file "../gateway.c",line 650,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |650| 
        MOVL      *-SP[4],ACC           ; [CPU_] |650| 
	.dwpsn	file "../gateway.c",line 651,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |651| 
$C$L99:    
	.dwpsn	file "../gateway.c",line 653,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |653| 
        CMP       AL,*-SP[19]           ; [CPU_] |653| 
        B         $C$L100,LEQ           ; [CPU_] |653| 
        ; branchcc occurs ; [] |653| 
	.dwpsn	file "../gateway.c",line 654,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |654| 
	.dwpsn	file "../gateway.c",line 655,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |655| 
        MOVL      *-SP[4],ACC           ; [CPU_] |655| 
	.dwpsn	file "../gateway.c",line 656,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |656| 
$C$L100:    
	.dwpsn	file "../gateway.c",line 658,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |658| 
        ANDB      AL,#0x0f              ; [CPU_] |658| 
        CMPB      AL,#4                 ; [CPU_] |658| 
        BF        $C$L101,EQ            ; [CPU_] |658| 
        ; branchcc occurs ; [] |658| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |658| 
        CMP       AL,*-SP[19]           ; [CPU_] |658| 
        B         $C$L112,GT            ; [CPU_] |658| 
        ; branchcc occurs ; [] |658| 
        MOV       AL,*-SP[23]           ; [CPU_] |658| 
        BF        $C$L112,EQ            ; [CPU_] |658| 
        ; branchcc occurs ; [] |658| 
$C$L101:    
	.dwpsn	file "../gateway.c",line 659,column 11,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |659| 
        MOVL      ACC,XAR4              ; [CPU_] |659| 
$C$DW$311	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$311, DW_AT_low_pc(0x00)
	.dwattr $C$DW$311, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$311, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |659| 
        ; call occurs [#_ERR_HandleWarning] ; [] |659| 
	.dwpsn	file "../gateway.c",line 660,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#4,UNC ; [CPU_] |660| 
	.dwpsn	file "../gateway.c",line 662,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |662| 
        MOVL      *-SP[4],ACC           ; [CPU_] |662| 
	.dwpsn	file "../gateway.c",line 663,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |663| 
	.dwpsn	file "../gateway.c",line 665,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |665| 
        ; branch occurs ; [] |665| 
$C$L102:    
	.dwpsn	file "../gateway.c",line 668,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |668| 
        CMP       AL,*-SP[19]           ; [CPU_] |668| 
        B         $C$L103,GT            ; [CPU_] |668| 
        ; branchcc occurs ; [] |668| 
	.dwpsn	file "../gateway.c",line 670,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |670| 
	.dwpsn	file "../gateway.c",line 671,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |671| 
        MOVL      *-SP[4],ACC           ; [CPU_] |671| 
	.dwpsn	file "../gateway.c",line 672,column 11,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |672| 
	.dwpsn	file "../gateway.c",line 673,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |673| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        ANDB      AL,#0x0f              ; [CPU_] |673| 
        MOV       @_ODV_MachineMode,AL  ; [CPU_] |673| 
	.dwpsn	file "../gateway.c",line 674,column 9,is_stmt
        B         $C$L112,UNC           ; [CPU_] |674| 
        ; branch occurs ; [] |674| 
$C$L103:    
	.dwpsn	file "../gateway.c",line 677,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |677| 
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Delay ; [CPU_U] 
        MPYXU     P,T,@_ODP_SafetyLimits_Resistor_Delay ; [CPU_] |677| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |677| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |677| 
        CMPL      ACC,P                 ; [CPU_] |677| 
        B         $C$L112,LO            ; [CPU_] |677| 
        ; branchcc occurs ; [] |677| 
	.dwpsn	file "../gateway.c",line 678,column 13,is_stmt
$C$DW$312	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$312, DW_AT_low_pc(0x00)
	.dwattr $C$DW$312, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$312, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |678| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |678| 
	.dwpsn	file "../gateway.c",line 684,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |684| 
        ; branch occurs ; [] |684| 
$C$L104:    
	.dwpsn	file "../gateway.c",line 687,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |687| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |687| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |687| 
        CMPL      ACC,XAR6              ; [CPU_] |687| 
        B         $C$L112,LO            ; [CPU_] |687| 
        ; branchcc occurs ; [] |687| 
	.dwpsn	file "../gateway.c",line 688,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |688| 
	.dwpsn	file "../gateway.c",line 689,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xefff ; [CPU_] |689| 
	.dwpsn	file "../gateway.c",line 690,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |690| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |690| 
        BF        $C$L112,TC            ; [CPU_] |690| 
        ; branchcc occurs ; [] |690| 
	.dwpsn	file "../gateway.c",line 690,column 38,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |690| 
	.dwpsn	file "../gateway.c",line 692,column 9,is_stmt
        B         $C$L112,UNC           ; [CPU_] |692| 
        ; branch occurs ; [] |692| 
$C$L105:    
	.dwpsn	file "../gateway.c",line 695,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |695| 
        MOVL      *-SP[4],ACC           ; [CPU_] |695| 
	.dwpsn	file "../gateway.c",line 696,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#128,UNC ; [CPU_] |696| 
	.dwpsn	file "../gateway.c",line 697,column 9,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |697| 
$C$L106:    
	.dwpsn	file "../gateway.c",line 700,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |700| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |700| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |700| 
        CMPL      ACC,XAR6              ; [CPU_] |700| 
        B         $C$L108,LO            ; [CPU_] |700| 
        ; branchcc occurs ; [] |700| 
	.dwpsn	file "../gateway.c",line 701,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |701| 
	.dwpsn	file "../gateway.c",line 702,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xefff ; [CPU_] |702| 
	.dwpsn	file "../gateway.c",line 703,column 11,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |703| 
        BF        $C$L107,NEQ           ; [CPU_] |703| 
        ; branchcc occurs ; [] |703| 
	.dwpsn	file "../gateway.c",line 703,column 29,is_stmt
$C$DW$313	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$313, DW_AT_low_pc(0x00)
	.dwattr $C$DW$313, DW_AT_name("_PAR_AddLog")
	.dwattr $C$DW$313, DW_AT_TI_call
        LCR       #_PAR_AddLog          ; [CPU_] |703| 
        ; call occurs [#_PAR_AddLog] ; [] |703| 
$C$L107:    
	.dwpsn	file "../gateway.c",line 704,column 11,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |704| 
	.dwpsn	file "../gateway.c",line 705,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |705| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |705| 
        BF        $C$L108,TC            ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
	.dwpsn	file "../gateway.c",line 705,column 38,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |705| 
$C$L108:    
	.dwpsn	file "../gateway.c",line 707,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |707| 
        BF        $C$L109,EQ            ; [CPU_] |707| 
        ; branchcc occurs ; [] |707| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |707| 
        BF        $C$L112,NEQ           ; [CPU_] |707| 
        ; branchcc occurs ; [] |707| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      XAR4,#10000           ; [CPU_U] |707| 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |707| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |707| 
        CMPL      ACC,XAR4              ; [CPU_] |707| 
        B         $C$L112,LO            ; [CPU_] |707| 
        ; branchcc occurs ; [] |707| 
$C$L109:    
	.dwpsn	file "../gateway.c",line 708,column 11,is_stmt
$C$DW$314	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$314, DW_AT_low_pc(0x00)
	.dwattr $C$DW$314, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$314, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |708| 
        ; call occurs [#_ERR_ClearError] ; [] |708| 
        CMPB      AL,#0                 ; [CPU_] |708| 
        BF        $C$L112,EQ            ; [CPU_] |708| 
        ; branchcc occurs ; [] |708| 
	.dwpsn	file "../gateway.c",line 709,column 13,is_stmt
$C$DW$315	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$315, DW_AT_low_pc(0x00)
	.dwattr $C$DW$315, DW_AT_name("_ERR_ClearWarnings")
	.dwattr $C$DW$315, DW_AT_TI_call
        LCR       #_ERR_ClearWarnings   ; [CPU_] |709| 
        ; call occurs [#_ERR_ClearWarnings] ; [] |709| 
	.dwpsn	file "../gateway.c",line 711,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |711| 
	.dwpsn	file "../gateway.c",line 712,column 13,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |712| 
	.dwpsn	file "../gateway.c",line 715,column 7,is_stmt
        B         $C$L112,UNC           ; [CPU_] |715| 
        ; branch occurs ; [] |715| 
$C$L110:    
	.dwpsn	file "../gateway.c",line 529,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |529| 
        CMPB      AL,#4                 ; [CPU_] |529| 
        B         $C$L111,GT            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#4                 ; [CPU_] |529| 
        BF        $C$L79,EQ             ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#0                 ; [CPU_] |529| 
        BF        $C$L70,EQ             ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#1                 ; [CPU_] |529| 
        BF        $C$L94,EQ             ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#2                 ; [CPU_] |529| 
        BF        $C$L87,EQ             ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#3                 ; [CPU_] |529| 
        BF        $C$L102,EQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        B         $C$L106,UNC           ; [CPU_] |529| 
        ; branch occurs ; [] |529| 
$C$L111:    
        CMPB      AL,#6                 ; [CPU_] |529| 
        BF        $C$L75,EQ             ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#8                 ; [CPU_] |529| 
        BF        $C$L104,EQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#64                ; [CPU_] |529| 
        BF        $C$L105,EQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        CMPB      AL,#128               ; [CPU_] |529| 
        BF        $C$L106,EQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
        B         $C$L106,UNC           ; [CPU_] |529| 
        ; branch occurs ; [] |529| 
$C$L112:    
	.dwpsn	file "../gateway.c",line 717,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |717| 
        MOVB      AL,#1                 ; [CPU_] |717| 
$C$DW$316	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$316, DW_AT_low_pc(0x00)
	.dwattr $C$DW$316, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$316, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |717| 
        ; call occurs [#_SEM_pend] ; [] |717| 
	.dwpsn	file "../gateway.c",line 398,column 10,is_stmt
        B         $C$L39,UNC            ; [CPU_] |398| 
        ; branch occurs ; [] |398| 
	.dwattr $C$DW$268, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$268, DW_AT_TI_end_line(0x2cf)
	.dwattr $C$DW$268, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$268

	.sect	".text"
	.clink
	.global	_SecurityCallBack

$C$DW$317	.dwtag  DW_TAG_subprogram, DW_AT_name("SecurityCallBack")
	.dwattr $C$DW$317, DW_AT_low_pc(_SecurityCallBack)
	.dwattr $C$DW$317, DW_AT_high_pc(0x00)
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_SecurityCallBack")
	.dwattr $C$DW$317, DW_AT_external
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$317, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$317, DW_AT_TI_begin_line(0x2d3)
	.dwattr $C$DW$317, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$317, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 723,column 95,is_stmt,address _SecurityCallBack

	.dwfde $C$DW$CIE, _SecurityCallBack
$C$DW$318	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$318, DW_AT_location[DW_OP_reg12]
$C$DW$319	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$319, DW_AT_location[DW_OP_reg14]
$C$DW$320	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$320, DW_AT_location[DW_OP_reg0]
$C$DW$321	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$321, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SecurityCallBack             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SecurityCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$322	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$322, DW_AT_location[DW_OP_breg20 -2]
$C$DW$323	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$323, DW_AT_location[DW_OP_breg20 -4]
$C$DW$324	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$324, DW_AT_location[DW_OP_breg20 -5]
$C$DW$325	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$325, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |723| 
        MOV       *-SP[5],AL            ; [CPU_] |723| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |723| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |723| 
	.dwpsn	file "../gateway.c",line 724,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |724| 
        BF        $C$L113,NEQ           ; [CPU_] |724| 
        ; branchcc occurs ; [] |724| 
	.dwpsn	file "../gateway.c",line 725,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |725| 
        BF        $C$L113,NEQ           ; [CPU_] |725| 
        ; branchcc occurs ; [] |725| 
	.dwpsn	file "../gateway.c",line 725,column 28,is_stmt
$C$DW$326	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$326, DW_AT_low_pc(0x00)
	.dwattr $C$DW$326, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$326, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |725| 
        ; call occurs [#_HAL_Random] ; [] |725| 
$C$L113:    
	.dwpsn	file "../gateway.c",line 727,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |727| 
        CMPB      AL,#1                 ; [CPU_] |727| 
        BF        $C$L115,NEQ           ; [CPU_] |727| 
        ; branchcc occurs ; [] |727| 
	.dwpsn	file "../gateway.c",line 728,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |728| 
        BF        $C$L114,NEQ           ; [CPU_] |728| 
        ; branchcc occurs ; [] |728| 
	.dwpsn	file "../gateway.c",line 728,column 28,is_stmt
$C$DW$327	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$327, DW_AT_low_pc(0x00)
	.dwattr $C$DW$327, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$327, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |728| 
        ; call occurs [#_HAL_Random] ; [] |728| 
        B         $C$L115,UNC           ; [CPU_] |728| 
        ; branch occurs ; [] |728| 
$C$L114:    
	.dwpsn	file "../gateway.c",line 729,column 10,is_stmt
$C$DW$328	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$328, DW_AT_low_pc(0x00)
	.dwattr $C$DW$328, DW_AT_name("_HAL_Unlock")
	.dwattr $C$DW$328, DW_AT_TI_call
        LCR       #_HAL_Unlock          ; [CPU_] |729| 
        ; call occurs [#_HAL_Unlock] ; [] |729| 
$C$L115:    
	.dwpsn	file "../gateway.c",line 731,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |731| 
	.dwpsn	file "../gateway.c",line 732,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$329	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$329, DW_AT_low_pc(0x00)
	.dwattr $C$DW$329, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$317, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$317, DW_AT_TI_end_line(0x2dc)
	.dwattr $C$DW$317, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$317

	.sect	".text"
	.clink
	.global	_LogNBCallback

$C$DW$330	.dwtag  DW_TAG_subprogram, DW_AT_name("LogNBCallback")
	.dwattr $C$DW$330, DW_AT_low_pc(_LogNBCallback)
	.dwattr $C$DW$330, DW_AT_high_pc(0x00)
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_LogNBCallback")
	.dwattr $C$DW$330, DW_AT_external
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$330, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$330, DW_AT_TI_begin_line(0x2df)
	.dwattr $C$DW$330, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$330, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 735,column 92,is_stmt,address _LogNBCallback

	.dwfde $C$DW$CIE, _LogNBCallback
$C$DW$331	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$331, DW_AT_location[DW_OP_reg12]
$C$DW$332	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$332, DW_AT_location[DW_OP_reg14]
$C$DW$333	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$333, DW_AT_location[DW_OP_reg0]
$C$DW$334	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$334, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LogNBCallback                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LogNBCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$335	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$335, DW_AT_location[DW_OP_breg20 -2]
$C$DW$336	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$336, DW_AT_location[DW_OP_breg20 -4]
$C$DW$337	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$337, DW_AT_location[DW_OP_breg20 -5]
$C$DW$338	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$338, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |735| 
        MOV       *-SP[5],AL            ; [CPU_] |735| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |735| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |735| 
	.dwpsn	file "../gateway.c",line 737,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |737| 
        CMPB      AL,#1                 ; [CPU_] |737| 
        BF        $C$L116,NEQ           ; [CPU_] |737| 
        ; branchcc occurs ; [] |737| 
	.dwpsn	file "../gateway.c",line 738,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_LogNB ; [CPU_] |738| 
$C$DW$339	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$339, DW_AT_low_pc(0x00)
	.dwattr $C$DW$339, DW_AT_name("_PAR_GetLogNB")
	.dwattr $C$DW$339, DW_AT_TI_call
        LCR       #_PAR_GetLogNB        ; [CPU_] |738| 
        ; call occurs [#_PAR_GetLogNB] ; [] |738| 
	.dwpsn	file "../gateway.c",line 739,column 3,is_stmt
        B         $C$L117,UNC           ; [CPU_] |739| 
        ; branch occurs ; [] |739| 
$C$L116:    
	.dwpsn	file "../gateway.c",line 741,column 5,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |741| 
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       @_ODV_Gateway_LogNB,AL ; [CPU_] |741| 
$C$L117:    
	.dwpsn	file "../gateway.c",line 743,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |743| 
	.dwpsn	file "../gateway.c",line 744,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$340	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$340, DW_AT_low_pc(0x00)
	.dwattr $C$DW$340, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$330, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$330, DW_AT_TI_end_line(0x2e8)
	.dwattr $C$DW$330, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$330

	.sect	".text"
	.clink
	.global	_BatteryCallBack

$C$DW$341	.dwtag  DW_TAG_subprogram, DW_AT_name("BatteryCallBack")
	.dwattr $C$DW$341, DW_AT_low_pc(_BatteryCallBack)
	.dwattr $C$DW$341, DW_AT_high_pc(0x00)
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_BatteryCallBack")
	.dwattr $C$DW$341, DW_AT_external
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$341, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$341, DW_AT_TI_begin_line(0x2ea)
	.dwattr $C$DW$341, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$341, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../gateway.c",line 746,column 94,is_stmt,address _BatteryCallBack

	.dwfde $C$DW$CIE, _BatteryCallBack
$C$DW$342	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$342, DW_AT_location[DW_OP_reg12]
$C$DW$343	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$343, DW_AT_location[DW_OP_reg14]
$C$DW$344	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$344, DW_AT_location[DW_OP_reg0]
$C$DW$345	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$345, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BatteryCallBack              FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_BatteryCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$346	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$346, DW_AT_location[DW_OP_breg20 -6]
$C$DW$347	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$347, DW_AT_location[DW_OP_breg20 -8]
$C$DW$348	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$348, DW_AT_location[DW_OP_breg20 -9]
$C$DW$349	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$349, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[10],AH           ; [CPU_] |746| 
        MOV       *-SP[9],AL            ; [CPU_] |746| 
        MOVL      *-SP[8],XAR5          ; [CPU_] |746| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |746| 
	.dwpsn	file "../gateway.c",line 747,column 3,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |747| 
        CMPB      AL,#1                 ; [CPU_] |747| 
        BF        $C$L118,NEQ           ; [CPU_] |747| 
        ; branchcc occurs ; [] |747| 
	.dwpsn	file "../gateway.c",line 748,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |748| 
        CMPB      AL,#1                 ; [CPU_] |748| 
        BF        $C$L118,NEQ           ; [CPU_] |748| 
        ; branchcc occurs ; [] |748| 
	.dwpsn	file "../gateway.c",line 749,column 7,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOVIZ     R0H,#19035            ; [CPU_] |749| 
        MOV32     R1H,@_ODP_Battery_Capacity ; [CPU_] |749| 
        MOVXI     R0H,#47616            ; [CPU_] |749| 
        MPYF32    R0H,R0H,R1H           ; [CPU_] |749| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16512        ; [CPU_] |749| 
$C$DW$350	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$350, DW_AT_low_pc(0x00)
	.dwattr $C$DW$350, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$350, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |749| 
        ; call occurs [#_CNV_Round] ; [] |749| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |749| 
	.dwpsn	file "../gateway.c",line 750,column 7,is_stmt
        MOVW      DP,#_ODV_SOC_SOC2     ; [CPU_U] 
        MOVU      ACC,@_ODV_SOC_SOC2    ; [CPU_] |750| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      XT,ACC                ; [CPU_] |750| 
        MOVB      ACC,#100              ; [CPU_] |750| 
        MOVL      *-SP[4],ACC           ; [CPU_] |750| 
        QMPYXUL   P,XT,@_PAR_Capacity_Total ; [CPU_] |750| 
        MOV       *-SP[2],#0            ; [CPU_] |750| 
        MOVL      XAR6,P                ; [CPU_] |750| 
        MOV       *-SP[1],#0            ; [CPU_] |750| 
        MOVL      ACC,XAR6              ; [CPU_] |750| 
        IMPYL     P,XT,@_PAR_Capacity_Total ; [CPU_] |750| 
$C$DW$351	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$351, DW_AT_low_pc(0x00)
	.dwattr $C$DW$351, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$351, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |750| 
        ; call occurs [#ULL$$DIV] ; [] |750| 
        MOVW      DP,#_PAR_Capacity_Left ; [CPU_U] 
        MOVL      @_PAR_Capacity_Left,P ; [CPU_] |750| 
	.dwpsn	file "../gateway.c",line 751,column 7,is_stmt
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        ZAPA      ; [CPU_] |751| 
        MOVL      @_PAR_Capacity_TotalLife_Used,P ; [CPU_] |751| 
        MOVL      @_PAR_Capacity_TotalLife_Used+2,ACC ; [CPU_] |751| 
$C$L118:    
	.dwpsn	file "../gateway.c",line 754,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |754| 
	.dwpsn	file "../gateway.c",line 755,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$352	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$352, DW_AT_low_pc(0x00)
	.dwattr $C$DW$352, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$341, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$341, DW_AT_TI_end_line(0x2f3)
	.dwattr $C$DW$341, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$341

	.sect	".text"
	.clink
	.global	_WriteTextCallback

$C$DW$353	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteTextCallback")
	.dwattr $C$DW$353, DW_AT_low_pc(_WriteTextCallback)
	.dwattr $C$DW$353, DW_AT_high_pc(0x00)
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_WriteTextCallback")
	.dwattr $C$DW$353, DW_AT_external
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$353, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$353, DW_AT_TI_begin_line(0x2f5)
	.dwattr $C$DW$353, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$353, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 757,column 96,is_stmt,address _WriteTextCallback

	.dwfde $C$DW$CIE, _WriteTextCallback
$C$DW$354	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$354, DW_AT_location[DW_OP_reg12]
$C$DW$355	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$355, DW_AT_location[DW_OP_reg14]
$C$DW$356	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$356, DW_AT_location[DW_OP_reg0]
$C$DW$357	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$357, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteTextCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteTextCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$358	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$358, DW_AT_location[DW_OP_breg20 -2]
$C$DW$359	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$359, DW_AT_location[DW_OP_breg20 -4]
$C$DW$360	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$360, DW_AT_location[DW_OP_breg20 -5]
$C$DW$361	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$361, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |757| 
        MOV       *-SP[5],AL            ; [CPU_] |757| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |757| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |757| 
	.dwpsn	file "../gateway.c",line 759,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |759| 
        CMPB      AL,#1                 ; [CPU_] |759| 
        BF        $C$L119,NEQ           ; [CPU_] |759| 
        ; branchcc occurs ; [] |759| 
	.dwpsn	file "../gateway.c",line 761,column 5,is_stmt
        MOVB      AL,#10                ; [CPU_] |761| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |761| 
        MOVB      AH,#8                 ; [CPU_] |761| 
        MOVB      XAR5,#0               ; [CPU_] |761| 
$C$DW$362	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$362, DW_AT_low_pc(0x00)
	.dwattr $C$DW$362, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$362, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |761| 
        ; call occurs [#_I2C_Command] ; [] |761| 
	.dwpsn	file "../gateway.c",line 762,column 3,is_stmt
        B         $C$L120,UNC           ; [CPU_] |762| 
        ; branch occurs ; [] |762| 
$C$L119:    
	.dwpsn	file "../gateway.c",line 765,column 5,is_stmt
        MOVB      AL,#9                 ; [CPU_] |765| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |765| 
        MOVB      AH,#8                 ; [CPU_] |765| 
        MOVB      XAR5,#0               ; [CPU_] |765| 
$C$DW$363	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$363, DW_AT_low_pc(0x00)
	.dwattr $C$DW$363, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$363, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |765| 
        ; call occurs [#_I2C_Command] ; [] |765| 
$C$L120:    
	.dwpsn	file "../gateway.c",line 767,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |767| 
	.dwpsn	file "../gateway.c",line 768,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$364	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$364, DW_AT_low_pc(0x00)
	.dwattr $C$DW$364, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$353, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$353, DW_AT_TI_end_line(0x300)
	.dwattr $C$DW$353, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$353

	.sect	".text"
	.clink
	.global	_MotCurCallback

$C$DW$365	.dwtag  DW_TAG_subprogram, DW_AT_name("MotCurCallback")
	.dwattr $C$DW$365, DW_AT_low_pc(_MotCurCallback)
	.dwattr $C$DW$365, DW_AT_high_pc(0x00)
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_MotCurCallback")
	.dwattr $C$DW$365, DW_AT_external
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$365, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$365, DW_AT_TI_begin_line(0x302)
	.dwattr $C$DW$365, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$365, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 770,column 93,is_stmt,address _MotCurCallback

	.dwfde $C$DW$CIE, _MotCurCallback
$C$DW$366	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$366, DW_AT_location[DW_OP_reg12]
$C$DW$367	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$367, DW_AT_location[DW_OP_reg14]
$C$DW$368	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$368, DW_AT_location[DW_OP_reg0]
$C$DW$369	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$369, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MotCurCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MotCurCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$370	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$370, DW_AT_location[DW_OP_breg20 -2]
$C$DW$371	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$371, DW_AT_location[DW_OP_breg20 -4]
$C$DW$372	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$372, DW_AT_location[DW_OP_breg20 -5]
$C$DW$373	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$373, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |770| 
        MOV       *-SP[5],AL            ; [CPU_] |770| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |770| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |770| 
	.dwpsn	file "../gateway.c",line 771,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |771| 
        CMPB      AL,#1                 ; [CPU_] |771| 
        BF        $C$L121,NEQ           ; [CPU_] |771| 
        ; branchcc occurs ; [] |771| 
	.dwpsn	file "../gateway.c",line 772,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |772| 
        CMPB      AL,#2                 ; [CPU_] |772| 
        BF        $C$L121,NEQ           ; [CPU_] |772| 
        ; branchcc occurs ; [] |772| 
	.dwpsn	file "../gateway.c",line 773,column 7,is_stmt
        MOVW      DP,#_HAL_NewCurPoint  ; [CPU_U] 
        MOVB      @_HAL_NewCurPoint,#1,UNC ; [CPU_] |773| 
$C$L121:    
	.dwpsn	file "../gateway.c",line 776,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |776| 
	.dwpsn	file "../gateway.c",line 777,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$374	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$374, DW_AT_low_pc(0x00)
	.dwattr $C$DW$374, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$365, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$365, DW_AT_TI_end_line(0x309)
	.dwattr $C$DW$365, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$365

	.sect	".text"
	.clink
	.global	_SciSendCallback

$C$DW$375	.dwtag  DW_TAG_subprogram, DW_AT_name("SciSendCallback")
	.dwattr $C$DW$375, DW_AT_low_pc(_SciSendCallback)
	.dwattr $C$DW$375, DW_AT_high_pc(0x00)
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_SciSendCallback")
	.dwattr $C$DW$375, DW_AT_external
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$375, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$375, DW_AT_TI_begin_line(0x30c)
	.dwattr $C$DW$375, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$375, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 780,column 94,is_stmt,address _SciSendCallback

	.dwfde $C$DW$CIE, _SciSendCallback
$C$DW$376	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$376, DW_AT_location[DW_OP_reg12]
$C$DW$377	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$377, DW_AT_location[DW_OP_reg14]
$C$DW$378	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$378, DW_AT_location[DW_OP_reg0]
$C$DW$379	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$379, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SciSendCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SciSendCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$380	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$380, DW_AT_location[DW_OP_breg20 -2]
$C$DW$381	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$381, DW_AT_location[DW_OP_breg20 -4]
$C$DW$382	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$382, DW_AT_location[DW_OP_breg20 -5]
$C$DW$383	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$383, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |780| 
        MOV       *-SP[5],AL            ; [CPU_] |780| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |780| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |780| 
	.dwpsn	file "../gateway.c",line 781,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |781| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |781| 
        MOVL      XAR5,#_ODV_SciSend    ; [CPU_U] |781| 
$C$DW$384	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$384, DW_AT_low_pc(0x00)
	.dwattr $C$DW$384, DW_AT_name("_MBX_post")
	.dwattr $C$DW$384, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |781| 
        ; call occurs [#_MBX_post] ; [] |781| 
	.dwpsn	file "../gateway.c",line 784,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |784| 
	.dwpsn	file "../gateway.c",line 785,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$385	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$385, DW_AT_low_pc(0x00)
	.dwattr $C$DW$385, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$375, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$375, DW_AT_TI_end_line(0x311)
	.dwattr $C$DW$375, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$375

	.sect	".text"
	.clink
	.global	_SinCosCallback

$C$DW$386	.dwtag  DW_TAG_subprogram, DW_AT_name("SinCosCallback")
	.dwattr $C$DW$386, DW_AT_low_pc(_SinCosCallback)
	.dwattr $C$DW$386, DW_AT_high_pc(0x00)
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_SinCosCallback")
	.dwattr $C$DW$386, DW_AT_external
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$386, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$386, DW_AT_TI_begin_line(0x313)
	.dwattr $C$DW$386, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$386, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 787,column 93,is_stmt,address _SinCosCallback

	.dwfde $C$DW$CIE, _SinCosCallback
$C$DW$387	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$387, DW_AT_location[DW_OP_reg12]
$C$DW$388	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$388, DW_AT_location[DW_OP_reg14]
$C$DW$389	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$389, DW_AT_location[DW_OP_reg0]
$C$DW$390	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$390, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SinCosCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SinCosCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$391	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$391, DW_AT_location[DW_OP_breg20 -2]
$C$DW$392	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$392, DW_AT_location[DW_OP_breg20 -4]
$C$DW$393	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$393, DW_AT_location[DW_OP_breg20 -5]
$C$DW$394	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$394, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |787| 
        MOV       *-SP[5],AL            ; [CPU_] |787| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |787| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |787| 
	.dwpsn	file "../gateway.c",line 789,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |789| 
	.dwpsn	file "../gateway.c",line 790,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$395	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$395, DW_AT_low_pc(0x00)
	.dwattr $C$DW$395, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$386, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$386, DW_AT_TI_end_line(0x316)
	.dwattr $C$DW$386, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$386

	.sect	".text"
	.clink
	.global	_StartRecorderCallBack

$C$DW$396	.dwtag  DW_TAG_subprogram, DW_AT_name("StartRecorderCallBack")
	.dwattr $C$DW$396, DW_AT_low_pc(_StartRecorderCallBack)
	.dwattr $C$DW$396, DW_AT_high_pc(0x00)
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_StartRecorderCallBack")
	.dwattr $C$DW$396, DW_AT_external
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$396, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$396, DW_AT_TI_begin_line(0x319)
	.dwattr $C$DW$396, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$396, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 793,column 100,is_stmt,address _StartRecorderCallBack

	.dwfde $C$DW$CIE, _StartRecorderCallBack
$C$DW$397	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$397, DW_AT_location[DW_OP_reg12]
$C$DW$398	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$398, DW_AT_location[DW_OP_reg14]
$C$DW$399	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$399, DW_AT_location[DW_OP_reg0]
$C$DW$400	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$400, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _StartRecorderCallBack        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_StartRecorderCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$401	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$401, DW_AT_location[DW_OP_breg20 -2]
$C$DW$402	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$402, DW_AT_location[DW_OP_breg20 -4]
$C$DW$403	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$403, DW_AT_location[DW_OP_breg20 -5]
$C$DW$404	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$404, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |793| 
        MOV       *-SP[5],AL            ; [CPU_] |793| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |793| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |793| 
	.dwpsn	file "../gateway.c",line 795,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |795| 
        CMPB      AL,#1                 ; [CPU_] |795| 
        BF        $C$L122,NEQ           ; [CPU_] |795| 
        ; branchcc occurs ; [] |795| 
	.dwpsn	file "../gateway.c",line 796,column 5,is_stmt
$C$DW$405	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$405, DW_AT_low_pc(0x00)
	.dwattr $C$DW$405, DW_AT_name("_REC_StartRecorder")
	.dwattr $C$DW$405, DW_AT_TI_call
        LCR       #_REC_StartRecorder   ; [CPU_] |796| 
        ; call occurs [#_REC_StartRecorder] ; [] |796| 
$C$L122:    
	.dwpsn	file "../gateway.c",line 798,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |798| 
	.dwpsn	file "../gateway.c",line 799,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$406	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$406, DW_AT_low_pc(0x00)
	.dwattr $C$DW$406, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$396, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$396, DW_AT_TI_end_line(0x31f)
	.dwattr $C$DW$396, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$396

	.sect	".text"
	.clink
	.global	_MultiunitsCallback

$C$DW$407	.dwtag  DW_TAG_subprogram, DW_AT_name("MultiunitsCallback")
	.dwattr $C$DW$407, DW_AT_low_pc(_MultiunitsCallback)
	.dwattr $C$DW$407, DW_AT_high_pc(0x00)
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_MultiunitsCallback")
	.dwattr $C$DW$407, DW_AT_external
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$407, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$407, DW_AT_TI_begin_line(0x321)
	.dwattr $C$DW$407, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$407, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 801,column 97,is_stmt,address _MultiunitsCallback

	.dwfde $C$DW$CIE, _MultiunitsCallback
$C$DW$408	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$408, DW_AT_location[DW_OP_reg12]
$C$DW$409	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$409, DW_AT_location[DW_OP_reg14]
$C$DW$410	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$410, DW_AT_location[DW_OP_reg0]
$C$DW$411	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MultiunitsCallback           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MultiunitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$412	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$412, DW_AT_location[DW_OP_breg20 -2]
$C$DW$413	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$413, DW_AT_location[DW_OP_breg20 -4]
$C$DW$414	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$414, DW_AT_location[DW_OP_breg20 -5]
$C$DW$415	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$415, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |801| 
        MOV       *-SP[5],AL            ; [CPU_] |801| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |801| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |801| 
	.dwpsn	file "../gateway.c",line 803,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |803| 
        CMPB      AL,#1                 ; [CPU_] |803| 
        BF        $C$L123,NEQ           ; [CPU_] |803| 
        ; branchcc occurs ; [] |803| 
	.dwpsn	file "../gateway.c",line 804,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Multiunits ; [CPU_] |804| 
$C$DW$416	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$416, DW_AT_low_pc(0x00)
	.dwattr $C$DW$416, DW_AT_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$416, DW_AT_TI_call
        LCR       #_PAR_AddMultiUnits   ; [CPU_] |804| 
        ; call occurs [#_PAR_AddMultiUnits] ; [] |804| 
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       @_ODV_Recorder_Multiunits,AL ; [CPU_] |804| 
$C$L123:    
	.dwpsn	file "../gateway.c",line 806,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |806| 
	.dwpsn	file "../gateway.c",line 807,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$417	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$417, DW_AT_low_pc(0x00)
	.dwattr $C$DW$417, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$407, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$407, DW_AT_TI_end_line(0x327)
	.dwattr $C$DW$407, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$407

	.sect	".text"
	.clink
	.global	_VariablesCallback

$C$DW$418	.dwtag  DW_TAG_subprogram, DW_AT_name("VariablesCallback")
	.dwattr $C$DW$418, DW_AT_low_pc(_VariablesCallback)
	.dwattr $C$DW$418, DW_AT_high_pc(0x00)
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_VariablesCallback")
	.dwattr $C$DW$418, DW_AT_external
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$418, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$418, DW_AT_TI_begin_line(0x329)
	.dwattr $C$DW$418, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$418, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 809,column 96,is_stmt,address _VariablesCallback

	.dwfde $C$DW$CIE, _VariablesCallback
$C$DW$419	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$419, DW_AT_location[DW_OP_reg12]
$C$DW$420	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$420, DW_AT_location[DW_OP_reg14]
$C$DW$421	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg0]
$C$DW$422	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VariablesCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VariablesCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$423	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$423, DW_AT_location[DW_OP_breg20 -2]
$C$DW$424	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$424, DW_AT_location[DW_OP_breg20 -4]
$C$DW$425	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$425, DW_AT_location[DW_OP_breg20 -5]
$C$DW$426	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$426, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |809| 
        MOV       *-SP[5],AL            ; [CPU_] |809| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |809| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |809| 
	.dwpsn	file "../gateway.c",line 811,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |811| 
        CMPB      AL,#1                 ; [CPU_] |811| 
        BF        $C$L124,NEQ           ; [CPU_] |811| 
        ; branchcc occurs ; [] |811| 
	.dwpsn	file "../gateway.c",line 812,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Variables ; [CPU_] |812| 
$C$DW$427	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$427, DW_AT_low_pc(0x00)
	.dwattr $C$DW$427, DW_AT_name("_PAR_AddVariables")
	.dwattr $C$DW$427, DW_AT_TI_call
        LCR       #_PAR_AddVariables    ; [CPU_] |812| 
        ; call occurs [#_PAR_AddVariables] ; [] |812| 
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       @_ODV_Recorder_Variables,AL ; [CPU_] |812| 
$C$L124:    
	.dwpsn	file "../gateway.c",line 814,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |814| 
	.dwpsn	file "../gateway.c",line 815,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$428	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$428, DW_AT_low_pc(0x00)
	.dwattr $C$DW$428, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$418, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$418, DW_AT_TI_end_line(0x32f)
	.dwattr $C$DW$418, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$418

	.sect	".text"
	.clink
	.global	_WriteOutputs8BitCallback

$C$DW$429	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs8BitCallback")
	.dwattr $C$DW$429, DW_AT_low_pc(_WriteOutputs8BitCallback)
	.dwattr $C$DW$429, DW_AT_high_pc(0x00)
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_WriteOutputs8BitCallback")
	.dwattr $C$DW$429, DW_AT_external
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$429, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$429, DW_AT_TI_begin_line(0x332)
	.dwattr $C$DW$429, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$429, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 818,column 103,is_stmt,address _WriteOutputs8BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs8BitCallback
$C$DW$430	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$430, DW_AT_location[DW_OP_reg12]
$C$DW$431	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$431, DW_AT_location[DW_OP_reg14]
$C$DW$432	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$432, DW_AT_location[DW_OP_reg0]
$C$DW$433	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$433, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs8BitCallback     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs8BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$434	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$434, DW_AT_location[DW_OP_breg20 -2]
$C$DW$435	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$435, DW_AT_location[DW_OP_breg20 -4]
$C$DW$436	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$436, DW_AT_location[DW_OP_breg20 -5]
$C$DW$437	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$437, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |818| 
        MOV       *-SP[5],AL            ; [CPU_] |818| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |818| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |818| 
	.dwpsn	file "../gateway.c",line 819,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 820,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |820| 
	.dwpsn	file "../gateway.c",line 821,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$438	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$438, DW_AT_low_pc(0x00)
	.dwattr $C$DW$438, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$429, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$429, DW_AT_TI_end_line(0x335)
	.dwattr $C$DW$429, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$429

	.sect	".text"
	.clink
	.global	_WriteOutputs16BitCallback

$C$DW$439	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs16BitCallback")
	.dwattr $C$DW$439, DW_AT_low_pc(_WriteOutputs16BitCallback)
	.dwattr $C$DW$439, DW_AT_high_pc(0x00)
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_WriteOutputs16BitCallback")
	.dwattr $C$DW$439, DW_AT_external
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$439, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$439, DW_AT_TI_begin_line(0x337)
	.dwattr $C$DW$439, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$439, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 823,column 104,is_stmt,address _WriteOutputs16BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs16BitCallback
$C$DW$440	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$440, DW_AT_location[DW_OP_reg12]
$C$DW$441	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg14]
$C$DW$442	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg0]
$C$DW$443	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs16BitCallback    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs16BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$444	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$444, DW_AT_location[DW_OP_breg20 -2]
$C$DW$445	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$445, DW_AT_location[DW_OP_breg20 -4]
$C$DW$446	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$446, DW_AT_location[DW_OP_breg20 -5]
$C$DW$447	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$447, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |823| 
        MOV       *-SP[5],AL            ; [CPU_] |823| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |823| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |823| 
	.dwpsn	file "../gateway.c",line 824,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 827,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |827| 
	.dwpsn	file "../gateway.c",line 828,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$448	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$448, DW_AT_low_pc(0x00)
	.dwattr $C$DW$448, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$439, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$439, DW_AT_TI_end_line(0x33c)
	.dwattr $C$DW$439, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$439

	.sect	".text"
	.clink
	.global	_ReadInputs8BitsCallback

$C$DW$449	.dwtag  DW_TAG_subprogram, DW_AT_name("ReadInputs8BitsCallback")
	.dwattr $C$DW$449, DW_AT_low_pc(_ReadInputs8BitsCallback)
	.dwattr $C$DW$449, DW_AT_high_pc(0x00)
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_ReadInputs8BitsCallback")
	.dwattr $C$DW$449, DW_AT_external
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$449, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$449, DW_AT_TI_begin_line(0x33e)
	.dwattr $C$DW$449, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$449, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 830,column 102,is_stmt,address _ReadInputs8BitsCallback

	.dwfde $C$DW$CIE, _ReadInputs8BitsCallback
$C$DW$450	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$450, DW_AT_location[DW_OP_reg12]
$C$DW$451	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$451, DW_AT_location[DW_OP_reg14]
$C$DW$452	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$452, DW_AT_location[DW_OP_reg0]
$C$DW$453	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$453, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ReadInputs8BitsCallback      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ReadInputs8BitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$454	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$454, DW_AT_location[DW_OP_breg20 -2]
$C$DW$455	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$455, DW_AT_location[DW_OP_breg20 -4]
$C$DW$456	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$456, DW_AT_location[DW_OP_breg20 -5]
$C$DW$457	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$457, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |830| 
        MOV       *-SP[5],AL            ; [CPU_] |830| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |830| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |830| 
	.dwpsn	file "../gateway.c",line 832,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |832| 
	.dwpsn	file "../gateway.c",line 833,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$458	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$458, DW_AT_low_pc(0x00)
	.dwattr $C$DW$458, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$449, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$449, DW_AT_TI_end_line(0x341)
	.dwattr $C$DW$449, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$449

	.sect	".text"
	.clink
	.global	_ControlWordCallBack

$C$DW$459	.dwtag  DW_TAG_subprogram, DW_AT_name("ControlWordCallBack")
	.dwattr $C$DW$459, DW_AT_low_pc(_ControlWordCallBack)
	.dwattr $C$DW$459, DW_AT_high_pc(0x00)
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_ControlWordCallBack")
	.dwattr $C$DW$459, DW_AT_external
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$459, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$459, DW_AT_TI_begin_line(0x343)
	.dwattr $C$DW$459, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$459, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 835,column 98,is_stmt,address _ControlWordCallBack

	.dwfde $C$DW$CIE, _ControlWordCallBack
$C$DW$460	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$460, DW_AT_location[DW_OP_reg12]
$C$DW$461	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$461, DW_AT_location[DW_OP_reg14]
$C$DW$462	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_reg0]
$C$DW$463	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$463, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ControlWordCallBack          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ControlWordCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$464	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$464, DW_AT_location[DW_OP_breg20 -2]
$C$DW$465	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$465, DW_AT_location[DW_OP_breg20 -4]
$C$DW$466	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_breg20 -5]
$C$DW$467	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |835| 
        MOV       *-SP[5],AL            ; [CPU_] |835| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |835| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |835| 
	.dwpsn	file "../gateway.c",line 836,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 837,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |837| 
	.dwpsn	file "../gateway.c",line 838,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$468	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$468, DW_AT_low_pc(0x00)
	.dwattr $C$DW$468, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$459, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$459, DW_AT_TI_end_line(0x346)
	.dwattr $C$DW$459, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$459

	.sect	".text"
	.clink
	.global	_VersionCallback

$C$DW$469	.dwtag  DW_TAG_subprogram, DW_AT_name("VersionCallback")
	.dwattr $C$DW$469, DW_AT_low_pc(_VersionCallback)
	.dwattr $C$DW$469, DW_AT_high_pc(0x00)
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_VersionCallback")
	.dwattr $C$DW$469, DW_AT_external
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$469, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$469, DW_AT_TI_begin_line(0x348)
	.dwattr $C$DW$469, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$469, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 840,column 94,is_stmt,address _VersionCallback

	.dwfde $C$DW$CIE, _VersionCallback
$C$DW$470	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$470, DW_AT_location[DW_OP_reg12]
$C$DW$471	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$471, DW_AT_location[DW_OP_reg14]
$C$DW$472	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$472, DW_AT_location[DW_OP_reg0]
$C$DW$473	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$473, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VersionCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VersionCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$474	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$474, DW_AT_location[DW_OP_breg20 -2]
$C$DW$475	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$475, DW_AT_location[DW_OP_breg20 -4]
$C$DW$476	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$476, DW_AT_location[DW_OP_breg20 -5]
$C$DW$477	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$477, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |840| 
        MOV       *-SP[5],AL            ; [CPU_] |840| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |840| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |840| 
	.dwpsn	file "../gateway.c",line 841,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |841| 
        BF        $C$L125,NEQ           ; [CPU_] |841| 
        ; branchcc occurs ; [] |841| 
	.dwpsn	file "../gateway.c",line 841,column 27,is_stmt
        MOVW      DP,#_ODV_Version      ; [CPU_U] 
        MOVB      @_ODV_Version,#1,UNC  ; [CPU_] |841| 
$C$L125:    
	.dwpsn	file "../gateway.c",line 842,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |842| 
	.dwpsn	file "../gateway.c",line 843,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$478	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$478, DW_AT_low_pc(0x00)
	.dwattr $C$DW$478, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$469, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$469, DW_AT_TI_end_line(0x34b)
	.dwattr $C$DW$469, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$469

	.sect	".text"
	.clink
	.global	_WriteAnalogueOutputsCallback

$C$DW$479	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteAnalogueOutputsCallback")
	.dwattr $C$DW$479, DW_AT_low_pc(_WriteAnalogueOutputsCallback)
	.dwattr $C$DW$479, DW_AT_high_pc(0x00)
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_WriteAnalogueOutputsCallback")
	.dwattr $C$DW$479, DW_AT_external
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$479, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$479, DW_AT_TI_begin_line(0x34d)
	.dwattr $C$DW$479, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$479, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 845,column 107,is_stmt,address _WriteAnalogueOutputsCallback

	.dwfde $C$DW$CIE, _WriteAnalogueOutputsCallback
$C$DW$480	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$480, DW_AT_location[DW_OP_reg12]
$C$DW$481	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$481, DW_AT_location[DW_OP_reg14]
$C$DW$482	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$482, DW_AT_location[DW_OP_reg0]
$C$DW$483	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$483, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteAnalogueOutputsCallback FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteAnalogueOutputsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$484	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$484, DW_AT_location[DW_OP_breg20 -2]
$C$DW$485	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$485, DW_AT_location[DW_OP_breg20 -4]
$C$DW$486	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$486, DW_AT_location[DW_OP_breg20 -5]
$C$DW$487	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |845| 
        MOV       *-SP[5],AL            ; [CPU_] |845| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |845| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |845| 
	.dwpsn	file "../gateway.c",line 847,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |847| 
	.dwpsn	file "../gateway.c",line 848,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$488	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$488, DW_AT_low_pc(0x00)
	.dwattr $C$DW$488, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$479, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$479, DW_AT_TI_end_line(0x350)
	.dwattr $C$DW$479, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$479

	.sect	".text"
	.clink
	.global	_CommErrorSetCallback

$C$DW$489	.dwtag  DW_TAG_subprogram, DW_AT_name("CommErrorSetCallback")
	.dwattr $C$DW$489, DW_AT_low_pc(_CommErrorSetCallback)
	.dwattr $C$DW$489, DW_AT_high_pc(0x00)
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_CommErrorSetCallback")
	.dwattr $C$DW$489, DW_AT_external
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$489, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$489, DW_AT_TI_begin_line(0x353)
	.dwattr $C$DW$489, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$489, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 851,column 99,is_stmt,address _CommErrorSetCallback

	.dwfde $C$DW$CIE, _CommErrorSetCallback
$C$DW$490	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$490, DW_AT_location[DW_OP_reg12]
$C$DW$491	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$491, DW_AT_location[DW_OP_reg14]
$C$DW$492	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$492, DW_AT_location[DW_OP_reg0]
$C$DW$493	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CommErrorSetCallback         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CommErrorSetCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$494	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$494, DW_AT_location[DW_OP_breg20 -2]
$C$DW$495	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$495, DW_AT_location[DW_OP_breg20 -4]
$C$DW$496	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$496, DW_AT_location[DW_OP_breg20 -5]
$C$DW$497	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$497, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |851| 
        MOV       *-SP[5],AL            ; [CPU_] |851| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |851| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |851| 
	.dwpsn	file "../gateway.c",line 852,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 855,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |855| 
	.dwpsn	file "../gateway.c",line 856,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$498	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$498, DW_AT_low_pc(0x00)
	.dwattr $C$DW$498, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$489, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$489, DW_AT_TI_end_line(0x358)
	.dwattr $C$DW$489, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$489

	.sect	".text"
	.clink
	.global	_SaveAllParameters

$C$DW$499	.dwtag  DW_TAG_subprogram, DW_AT_name("SaveAllParameters")
	.dwattr $C$DW$499, DW_AT_low_pc(_SaveAllParameters)
	.dwattr $C$DW$499, DW_AT_high_pc(0x00)
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_SaveAllParameters")
	.dwattr $C$DW$499, DW_AT_external
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$499, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$499, DW_AT_TI_begin_line(0x35a)
	.dwattr $C$DW$499, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$499, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 858,column 96,is_stmt,address _SaveAllParameters

	.dwfde $C$DW$CIE, _SaveAllParameters
$C$DW$500	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$500, DW_AT_location[DW_OP_reg12]
$C$DW$501	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$501, DW_AT_location[DW_OP_reg14]
$C$DW$502	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$502, DW_AT_location[DW_OP_reg0]
$C$DW$503	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$503, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SaveAllParameters            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SaveAllParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$504	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$504, DW_AT_location[DW_OP_breg20 -2]
$C$DW$505	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$505, DW_AT_location[DW_OP_breg20 -4]
$C$DW$506	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$506, DW_AT_location[DW_OP_breg20 -5]
$C$DW$507	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$507, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |858| 
        MOV       *-SP[5],AL            ; [CPU_] |858| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |858| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |858| 
	.dwpsn	file "../gateway.c",line 859,column 3,is_stmt
        MOVW      DP,#_ODV_StoreParameters ; [CPU_U] 
        MOV       AL,#30309             ; [CPU_] |859| 
        MOV       AH,#29537             ; [CPU_] |859| 
        CMPL      ACC,@_ODV_StoreParameters ; [CPU_] |859| 
        BF        $C$L126,NEQ           ; [CPU_] |859| 
        ; branchcc occurs ; [] |859| 
	.dwpsn	file "../gateway.c",line 860,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |860| 
        MOVL      @_ODV_StoreParameters,ACC ; [CPU_] |860| 
	.dwpsn	file "../gateway.c",line 861,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |861| 
$C$DW$508	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$508, DW_AT_low_pc(0x00)
	.dwattr $C$DW$508, DW_AT_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$508, DW_AT_TI_call
        LCR       #_PAR_WriteAllPermanentParam ; [CPU_] |861| 
        ; call occurs [#_PAR_WriteAllPermanentParam] ; [] |861| 
$C$L126:    
	.dwpsn	file "../gateway.c",line 863,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |863| 
	.dwpsn	file "../gateway.c",line 864,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$509	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$509, DW_AT_low_pc(0x00)
	.dwattr $C$DW$509, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$499, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$499, DW_AT_TI_end_line(0x360)
	.dwattr $C$DW$499, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$499

	.sect	".text"
	.clink
	.global	_LoadDefaultParameters

$C$DW$510	.dwtag  DW_TAG_subprogram, DW_AT_name("LoadDefaultParameters")
	.dwattr $C$DW$510, DW_AT_low_pc(_LoadDefaultParameters)
	.dwattr $C$DW$510, DW_AT_high_pc(0x00)
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_LoadDefaultParameters")
	.dwattr $C$DW$510, DW_AT_external
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$510, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$510, DW_AT_TI_begin_line(0x362)
	.dwattr $C$DW$510, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$510, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 866,column 100,is_stmt,address _LoadDefaultParameters

	.dwfde $C$DW$CIE, _LoadDefaultParameters
$C$DW$511	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$511, DW_AT_location[DW_OP_reg12]
$C$DW$512	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$512, DW_AT_location[DW_OP_reg14]
$C$DW$513	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$513, DW_AT_location[DW_OP_reg0]
$C$DW$514	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$514, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LoadDefaultParameters        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LoadDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$515	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$515, DW_AT_location[DW_OP_breg20 -2]
$C$DW$516	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$516, DW_AT_location[DW_OP_breg20 -4]
$C$DW$517	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$517, DW_AT_location[DW_OP_breg20 -5]
$C$DW$518	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$518, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |866| 
        MOV       *-SP[5],AL            ; [CPU_] |866| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |866| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |866| 
	.dwpsn	file "../gateway.c",line 867,column 3,is_stmt
        MOVW      DP,#_ODV_RestoreDefaultParameters ; [CPU_U] 
        MOV       AL,#24932             ; [CPU_] |867| 
        MOV       AH,#27759             ; [CPU_] |867| 
        CMPL      ACC,@_ODV_RestoreDefaultParameters ; [CPU_] |867| 
        BF        $C$L127,NEQ           ; [CPU_] |867| 
        ; branchcc occurs ; [] |867| 
	.dwpsn	file "../gateway.c",line 868,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |868| 
        MOVL      @_ODV_RestoreDefaultParameters,ACC ; [CPU_] |868| 
	.dwpsn	file "../gateway.c",line 869,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |869| 
$C$DW$519	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$519, DW_AT_low_pc(0x00)
	.dwattr $C$DW$519, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$519, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |869| 
        ; call occurs [#_PAR_UpdateCode] ; [] |869| 
	.dwpsn	file "../gateway.c",line 870,column 5,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |870| 
        MOVL      @_BootCommand,ACC     ; [CPU_] |870| 
$C$L127:    
	.dwpsn	file "../gateway.c",line 872,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |872| 
	.dwpsn	file "../gateway.c",line 873,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$520	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$520, DW_AT_low_pc(0x00)
	.dwattr $C$DW$520, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$510, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$510, DW_AT_TI_end_line(0x369)
	.dwattr $C$DW$510, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$510

	.sect	".text"
	.clink
	.global	_DebugCallBack

$C$DW$521	.dwtag  DW_TAG_subprogram, DW_AT_name("DebugCallBack")
	.dwattr $C$DW$521, DW_AT_low_pc(_DebugCallBack)
	.dwattr $C$DW$521, DW_AT_high_pc(0x00)
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_DebugCallBack")
	.dwattr $C$DW$521, DW_AT_external
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$521, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$521, DW_AT_TI_begin_line(0x36c)
	.dwattr $C$DW$521, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$521, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 876,column 92,is_stmt,address _DebugCallBack

	.dwfde $C$DW$CIE, _DebugCallBack
$C$DW$522	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$522, DW_AT_location[DW_OP_reg12]
$C$DW$523	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$523, DW_AT_location[DW_OP_reg14]
$C$DW$524	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$524, DW_AT_location[DW_OP_reg0]
$C$DW$525	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$525, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DebugCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_DebugCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$526	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$526, DW_AT_location[DW_OP_breg20 -2]
$C$DW$527	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$527, DW_AT_location[DW_OP_breg20 -4]
$C$DW$528	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$528, DW_AT_location[DW_OP_breg20 -5]
$C$DW$529	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$529, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |876| 
        MOV       *-SP[5],AL            ; [CPU_] |876| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |876| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |876| 
	.dwpsn	file "../gateway.c",line 877,column 3,is_stmt
        MOVW      DP,#_ODV_Debug        ; [CPU_U] 
        MOV       @_ODV_Debug,#0        ; [CPU_] |877| 
	.dwpsn	file "../gateway.c",line 878,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |878| 
	.dwpsn	file "../gateway.c",line 879,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$530	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$530, DW_AT_low_pc(0x00)
	.dwattr $C$DW$530, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$521, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$521, DW_AT_TI_end_line(0x36f)
	.dwattr $C$DW$521, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$521

	.sect	".text"
	.clink
	.global	_ConfigCallback

$C$DW$531	.dwtag  DW_TAG_subprogram, DW_AT_name("ConfigCallback")
	.dwattr $C$DW$531, DW_AT_low_pc(_ConfigCallback)
	.dwattr $C$DW$531, DW_AT_high_pc(0x00)
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_ConfigCallback")
	.dwattr $C$DW$531, DW_AT_external
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$531, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$531, DW_AT_TI_begin_line(0x371)
	.dwattr $C$DW$531, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$531, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 881,column 93,is_stmt,address _ConfigCallback

	.dwfde $C$DW$CIE, _ConfigCallback
$C$DW$532	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$532, DW_AT_location[DW_OP_reg12]
$C$DW$533	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$533, DW_AT_location[DW_OP_reg14]
$C$DW$534	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$534, DW_AT_location[DW_OP_reg0]
$C$DW$535	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$535, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ConfigCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ConfigCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$536	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$536, DW_AT_location[DW_OP_breg20 -2]
$C$DW$537	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$537, DW_AT_location[DW_OP_breg20 -4]
$C$DW$538	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$538, DW_AT_location[DW_OP_breg20 -5]
$C$DW$539	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$539, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |881| 
        MOV       *-SP[5],AL            ; [CPU_] |881| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |881| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |881| 
	.dwpsn	file "../gateway.c",line 882,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |882| 
        CMPB      AL,#1                 ; [CPU_] |882| 
        BF        $C$L130,NEQ           ; [CPU_] |882| 
        ; branchcc occurs ; [] |882| 
	.dwpsn	file "../gateway.c",line 883,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |883| 
        BF        $C$L128,EQ            ; [CPU_] |883| 
        ; branchcc occurs ; [] |883| 
        CMPB      AL,#128               ; [CPU_] |883| 
        BF        $C$L130,NEQ           ; [CPU_] |883| 
        ; branchcc occurs ; [] |883| 
$C$L128:    
	.dwpsn	file "../gateway.c",line 884,column 7,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |884| 
        TBIT      *+XAR4[0],#8          ; [CPU_] |884| 
        BF        $C$L129,NTC           ; [CPU_] |884| 
        ; branchcc occurs ; [] |884| 
	.dwpsn	file "../gateway.c",line 885,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        AND       @_GpioDataRegs,#0xffbf ; [CPU_] |885| 
	.dwpsn	file "../gateway.c",line 886,column 7,is_stmt
        B         $C$L130,UNC           ; [CPU_] |886| 
        ; branch occurs ; [] |886| 
$C$L129:    
	.dwpsn	file "../gateway.c",line 888,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs     ; [CPU_U] 
        OR        @_GpioDataRegs,#0x0040 ; [CPU_] |888| 
$C$L130:    
	.dwpsn	file "../gateway.c",line 891,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |891| 
	.dwpsn	file "../gateway.c",line 892,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$540	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$540, DW_AT_low_pc(0x00)
	.dwattr $C$DW$540, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$531, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$531, DW_AT_TI_end_line(0x37c)
	.dwattr $C$DW$531, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$531

	.sect	".text"
	.clink
	.global	_ResetCallBack

$C$DW$541	.dwtag  DW_TAG_subprogram, DW_AT_name("ResetCallBack")
	.dwattr $C$DW$541, DW_AT_low_pc(_ResetCallBack)
	.dwattr $C$DW$541, DW_AT_high_pc(0x00)
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_ResetCallBack")
	.dwattr $C$DW$541, DW_AT_external
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$541, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$541, DW_AT_TI_begin_line(0x37e)
	.dwattr $C$DW$541, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$541, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 894,column 92,is_stmt,address _ResetCallBack

	.dwfde $C$DW$CIE, _ResetCallBack
$C$DW$542	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$542, DW_AT_location[DW_OP_reg12]
$C$DW$543	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$543, DW_AT_location[DW_OP_reg14]
$C$DW$544	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$544, DW_AT_location[DW_OP_reg0]
$C$DW$545	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$545, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ResetCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ResetCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$546	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$546, DW_AT_location[DW_OP_breg20 -2]
$C$DW$547	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$547, DW_AT_location[DW_OP_breg20 -4]
$C$DW$548	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$548, DW_AT_location[DW_OP_breg20 -5]
$C$DW$549	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$549, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |894| 
        MOV       *-SP[5],AL            ; [CPU_] |894| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |894| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |894| 
	.dwpsn	file "../gateway.c",line 895,column 3,is_stmt
        MOVW      DP,#_ODV_ResetHW      ; [CPU_U] 
        MOV       AL,#29295             ; [CPU_] |895| 
        MOV       AH,#31333             ; [CPU_] |895| 
        CMPL      ACC,@_ODV_ResetHW     ; [CPU_] |895| 
        BF        $C$L131,NEQ           ; [CPU_] |895| 
        ; branchcc occurs ; [] |895| 
	.dwpsn	file "../gateway.c",line 896,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |896| 
        MOVL      @_ODV_ResetHW,ACC     ; [CPU_] |896| 
	.dwpsn	file "../gateway.c",line 897,column 5,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |897| 
        MOVL      @_BootCommand,ACC     ; [CPU_] |897| 
$C$L131:    
	.dwpsn	file "../gateway.c",line 899,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |899| 
	.dwpsn	file "../gateway.c",line 900,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$550	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$550, DW_AT_low_pc(0x00)
	.dwattr $C$DW$550, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$541, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$541, DW_AT_TI_end_line(0x384)
	.dwattr $C$DW$541, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$541

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_PAR_GetLogNB
	.global	_PAR_AddLog
	.global	_PAR_SetParamDependantVars
	.global	_USB_Unlock
	.global	_genCRC32Table
	.global	_ERR_ErrorOverVoltage
	.global	_ERR_ErrorOverTemp
	.global	_ERR_ErrorUnderVoltage
	.global	_ERR_SetError
	.global	_ERR_DELTA_Voltage
	.global	_ADS_Init
	.global	_HAL_Random
	.global	_HAL_Init
	.global	_HAL_Unlock
	.global	_setNodeId
	.global	_HAL_Reset
	.global	_REC_StartRecorder
	.global	_DIC_SetNodeId
	.global	_ERR_ErrorOverCurrent
	.global	_ERR_ClearWarning
	.global	_ERR_HandleWarning
	.global	_canInit
	.global	_USB_Stop
	.global	_ODP_Temperature_WarningMin
	.global	_ODP_Temperature_WarningMax
	.global	_ODP_SafetyLimits_Resistor_Delay
	.global	_ODP_SafetyLimits_Resistor_Tmin
	.global	_ODP_SafetyLimits_Current_delay
	.global	_ODP_SafetyLimits_Voltage_delay
	.global	_ODP_Settings_AUD_TempHeater_OFF_Max
	.global	_ODP_Settings_AUD_Temperature_Delay
	.global	_ODP_Settings_AUD_Heater_Voltage_OFF
	.global	_ODP_Settings_AUD_Safety_Low_Voltage_Delay
	.global	_ODP_Settings_AUD_Temp_Heater_ON_Min
	.global	_ODV_Current_ChargeAllowed
	.global	_ODP_SafetyLimits_UnderCurrent
	.global	_ODV_Current_DischargeAllowed
	.global	_ODP_NbOfModules
	.global	_ODP_Current_C_D_Mode
	.global	_ODP_SafetyLimits_Umax
	.global	_ODV_Gateway_Test_Voltage
	.global	_ODP_SafetyLimits_Tmax
	.global	_ODP_SafetyLimits_Umin
	.global	_ODV_Gateway_Heater_Status
	.global	_ODV_Gateway_MaxModTemp
	.global	_ODP_Gateway_Delay_Relay_Error
	.global	_ODP_Gateway_IsoResistor_Limit_Max
	.global	_ODP_SafetyLimits_OverVoltage
	.global	_ODP_SafetyLimits_Overcurrent
	.global	_ODP_SafetyLimits_Resistor_Tmax
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_ODP_SafetyLimits_Umax_bal_delta
	.global	_ODP_SafetyLimits_Tmin
	.global	_ODP_SafetyLimits_Charge_In_Thres_Cur
	.global	_ODP_SafetyLimits_Umin_bal_delta
	.global	_ODP_Board_Config
	.global	_ODP_CommError_Delay
	.global	_ODP_Sleep_Current
	.global	_ODP_Board_BaudRate
	.global	_ODV_MachineMode
	.global	_ODP_RelayResetTime
	.global	_ODP_CommError_TimeOut
	.global	_ODV_MachineEvent
	.global	_ODV_Controlword
	.global	_ODV_Write_Outputs_16_Bit
	.global	_TimeLogIndex
	.global	_HAL_NewCurPoint
	.global	_ODV_Version
	.global	_ODP_Sleep_Timeout
	.global	_ODV_Debug
	.global	_ODV_SOC_SOC2
	.global	_setState
	.global	_MBX_post
	.global	_PAR_AddMultiUnits
	.global	_ODV_Recorder_Multiunits
	.global	_ODV_Recorder_Variables
	.global	_PAR_AddVariables
	.global	_MBX_pend
	.global	_USB_Start
	.global	_PAR_UpdateCode
	.global	_ERR_ClearWarnings
	.global	_ERR_ClearError
	.global	_I2C_Command
	.global	_PAR_InitParam
	.global	_ODV_Gateway_MaxDeltaCellVoltage
	.global	_ODV_Gateway_State
	.global	_SEM_pend
	.global	_ODV_Gateway_MinCellVoltage
	.global	_ODV_Gateway_LogNB
	.global	_ODV_Gateway_MaxCellVoltage
	.global	_ODV_SciSend
	.global	_ODV_Gateway_MinModTemp
	.global	_ODV_Gateway_Current
	.global	_ODV_Gateway_Voltage
	.global	_ODV_Gateway_Temperature
	.global	_MMSConfig
	.global	_PAR_Capacity_Left
	.global	_ODV_ResetHW
	.global	_ODV_StoreParameters
	.global	_PAR_Capacity_Total
	.global	_ODV_RestoreDefaultParameters
	.global	_PAR_StoreODSubIndex
	.global	_PAR_WriteStatisticParam
	.global	_PAR_WriteAllPermanentParam
	.global	_CNV_Round
	.global	_getCRC32_cpu
	.global	_ODP_Board_RevisionNumber
	.global	_ODP_OnTime
	.global	_ODV_Gateway_Errorcode
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODP_Battery_Capacity
	.global	_ODV_SysTick_ms
	.global	_ODP_RandomNB
	.global	_ODV_Gateway_Date_Time
	.global	_PAR_Capacity_TotalLife_Used
	.global	_ODV_RTC_Text
	.global	_golden_CRC_values
	.global	_TSK_timerSem
	.global	_ODV_Read_Analogue_Input_16_Bit
	.global	_PieCtrlRegs
	.global	_GpioDataRegs
	.global	_mailboxSDOout
	.global	_sci_rx_mbox
	.global	_can_rx_mbox
	.global	_can_tx_mbox
	.global	_ODV_Modules_Alarms
	.global	_ODV_Modules_Temperature
	.global	_ODV_Modules_Heater
	.global	_ODV_Modules_MaxCellVoltage
	.global	_ODV_Modules_Temperature_MIN
	.global	_ODV_Modules_MinCellVoltage
	.global	_ODI_gateway_dict_Data
	.global	FS$$DIV
	.global	ULL$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$146	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$146, DW_AT_byte_size(0x01)
$C$DW$551	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$552	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$146

$C$DW$T$147	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$553, DW_AT_name("cob_id")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$554, DW_AT_name("rtr")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$555, DW_AT_name("len")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$556, DW_AT_name("data")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$137	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$557, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$558, DW_AT_name("csSDO")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$559, DW_AT_name("csEmergency")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$560, DW_AT_name("csSYNC")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$561, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$562, DW_AT_name("csPDO")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$563, DW_AT_name("csLSS")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$564, DW_AT_name("errCode")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$565, DW_AT_name("errRegMask")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$566, DW_AT_name("active")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)

$C$DW$T$129	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$129, DW_AT_byte_size(0x18)
$C$DW$567	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$567, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$129


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$568, DW_AT_name("index")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$569, DW_AT_name("subindex")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$570, DW_AT_name("size")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$571, DW_AT_name("address")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$134	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
$C$DW$T$135	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$T$135, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$572, DW_AT_name("SwitchOn")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$573, DW_AT_name("EnableVolt")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$574, DW_AT_name("QuickStop")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$575, DW_AT_name("EnableOperation")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$576, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$576, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$577, DW_AT_name("ResetFault")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$577, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$578, DW_AT_name("Halt")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$579, DW_AT_name("Oms")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$580, DW_AT_name("Rsvd")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$581, DW_AT_name("Manufacturer")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$581, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$582, DW_AT_name("SwitchOn")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$582, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$583, DW_AT_name("EnableVolt")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$583, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$584, DW_AT_name("QuickStop")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$584, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$585, DW_AT_name("EnableOperation")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$585, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$586, DW_AT_name("Rsvd0")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$586, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$587, DW_AT_name("ResetFault")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$587, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$588, DW_AT_name("Halt")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$589, DW_AT_name("Oms")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$590, DW_AT_name("Rsvd")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$591, DW_AT_name("Manufacturer")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$591, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$592, DW_AT_name("SwitchOn")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$592, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$593, DW_AT_name("EnableVolt")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$594, DW_AT_name("QuickStop")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$595, DW_AT_name("EnableOperation")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$596, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$597, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$597, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$598, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$598, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$599, DW_AT_name("ResetFault")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$600, DW_AT_name("Halt")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$601, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$602, DW_AT_name("Rsvd")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$603, DW_AT_name("Manufacturer")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$604, DW_AT_name("can_wk")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$605, DW_AT_name("sw_wk")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$606, DW_AT_name("ch_wk")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$607, DW_AT_name("SOC2")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$608, DW_AT_name("lem")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$609, DW_AT_name("onerelay")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_onerelay")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$610, DW_AT_name("b6")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_b6")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$611, DW_AT_name("b7")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_b7")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$612, DW_AT_name("en24v")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_en24v")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$613, DW_AT_name("b9")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_b9")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$614, DW_AT_name("b10")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_b10")
	.dwattr $C$DW$614, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$615, DW_AT_name("b11")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_b11")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$616, DW_AT_name("b12")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$617, DW_AT_name("b13")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$618, DW_AT_name("b14")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$619, DW_AT_name("b15")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$148	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$148, DW_AT_language(DW_LANG_C)
$C$DW$T$149	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x16)

$C$DW$T$28	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$620, DW_AT_name("ControlWord")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$621, DW_AT_name("AnyMode")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$622, DW_AT_name("VelocityMode")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$623, DW_AT_name("PositionMode")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$150	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x02)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$624, DW_AT_name("rsvd1")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$625, DW_AT_name("rsvd2")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$626, DW_AT_name("AIO2")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$627, DW_AT_name("rsvd3")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$628, DW_AT_name("AIO4")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$629, DW_AT_name("rsvd4")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$630, DW_AT_name("AIO6")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$631, DW_AT_name("rsvd5")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$632, DW_AT_name("rsvd6")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$632, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$633, DW_AT_name("rsvd7")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$634, DW_AT_name("AIO10")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$635, DW_AT_name("rsvd8")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$636, DW_AT_name("AIO12")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$636, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$637, DW_AT_name("rsvd9")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$637, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$638, DW_AT_name("AIO14")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$639, DW_AT_name("rsvd10")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$639, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$640, DW_AT_name("rsvd11")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$640, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x02)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$641, DW_AT_name("all")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$642, DW_AT_name("bit")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$643, DW_AT_name("GPIO0")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$643, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$644, DW_AT_name("GPIO1")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$644, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$645, DW_AT_name("GPIO2")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$645, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$646, DW_AT_name("GPIO3")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$646, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$647, DW_AT_name("GPIO4")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$647, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$648, DW_AT_name("GPIO5")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$648, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$649, DW_AT_name("GPIO6")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$649, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$650, DW_AT_name("GPIO7")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$650, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$651, DW_AT_name("GPIO8")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$651, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$652, DW_AT_name("GPIO9")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$652, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$653, DW_AT_name("GPIO10")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$653, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$654, DW_AT_name("GPIO11")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$654, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$655, DW_AT_name("GPIO12")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$655, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$656, DW_AT_name("GPIO13")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$656, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$657, DW_AT_name("GPIO14")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$657, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$658, DW_AT_name("GPIO15")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$658, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$659, DW_AT_name("GPIO16")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$659, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$660, DW_AT_name("GPIO17")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$660, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$661, DW_AT_name("GPIO18")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$662, DW_AT_name("GPIO19")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$663, DW_AT_name("GPIO20")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$663, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$664, DW_AT_name("GPIO21")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$664, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$665, DW_AT_name("GPIO22")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$665, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$666, DW_AT_name("GPIO23")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$666, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$667, DW_AT_name("GPIO24")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$667, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$668, DW_AT_name("GPIO25")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$669, DW_AT_name("GPIO26")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$670, DW_AT_name("GPIO27")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$671, DW_AT_name("GPIO28")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$672, DW_AT_name("GPIO29")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$673, DW_AT_name("GPIO30")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$673, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$674, DW_AT_name("GPIO31")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$674, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$34, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$675, DW_AT_name("all")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$676, DW_AT_name("bit")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$35, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$677, DW_AT_name("GPIO32")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$677, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$678, DW_AT_name("GPIO33")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$678, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$679, DW_AT_name("GPIO34")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$680, DW_AT_name("GPIO35")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$681, DW_AT_name("GPIO36")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$682, DW_AT_name("GPIO37")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$682, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$683, DW_AT_name("GPIO38")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$683, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$684, DW_AT_name("GPIO39")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$685, DW_AT_name("GPIO40")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$686, DW_AT_name("GPIO41")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$687, DW_AT_name("GPIO42")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$688, DW_AT_name("GPIO43")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$689, DW_AT_name("GPIO44")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$690, DW_AT_name("rsvd1")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$691, DW_AT_name("rsvd2")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$692, DW_AT_name("GPIO50")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$692, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$693, DW_AT_name("GPIO51")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$693, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$694, DW_AT_name("GPIO52")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$695, DW_AT_name("GPIO53")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$696, DW_AT_name("GPIO54")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$696, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$697, DW_AT_name("GPIO55")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$697, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$698, DW_AT_name("GPIO56")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$699, DW_AT_name("GPIO57")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$699, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$700, DW_AT_name("GPIO58")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$700, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$701, DW_AT_name("rsvd3")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$701, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$36, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$702, DW_AT_name("all")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$703, DW_AT_name("bit")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x20)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$704, DW_AT_name("GPADAT")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$705, DW_AT_name("GPASET")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$706, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$707, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$708, DW_AT_name("GPBDAT")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$709, DW_AT_name("GPBSET")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$710, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$711, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$712, DW_AT_name("rsvd1")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$713, DW_AT_name("AIODAT")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$714, DW_AT_name("AIOSET")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$715, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$716, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38

$C$DW$717	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$38)
$C$DW$T$153	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$717)

$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x08)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$718, DW_AT_name("wListElem")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$719, DW_AT_name("wCount")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$720, DW_AT_name("fxn")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x16)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)

$C$DW$T$53	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$53, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x30)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$721, DW_AT_name("dataQue")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$722, DW_AT_name("freeQue")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$723, DW_AT_name("dataSem")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$724, DW_AT_name("freeSem")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$725, DW_AT_name("segid")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$726, DW_AT_name("size")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$727, DW_AT_name("length")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$728, DW_AT_name("name")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53

$C$DW$T$154	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$154, DW_AT_language(DW_LANG_C)
$C$DW$T$156	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$156, DW_AT_address_class(0x16)
$C$DW$T$157	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$729, DW_AT_name("ACK1")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$729, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$730, DW_AT_name("ACK2")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$730, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$731, DW_AT_name("ACK3")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$731, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$732, DW_AT_name("ACK4")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$733, DW_AT_name("ACK5")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$733, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$734, DW_AT_name("ACK6")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$734, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$735, DW_AT_name("ACK7")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$735, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$736, DW_AT_name("ACK8")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$736, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$737, DW_AT_name("ACK9")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$737, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$738, DW_AT_name("ACK10")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$738, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$739, DW_AT_name("ACK11")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$739, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$740, DW_AT_name("ACK12")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$740, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$741, DW_AT_name("rsvd1")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$741, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$55, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x01)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$742, DW_AT_name("all")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$743, DW_AT_name("bit")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x01)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$744, DW_AT_name("ENPIE")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$744, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$745, DW_AT_name("PIEVECT")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$745, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$57, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$746, DW_AT_name("all")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$747, DW_AT_name("bit")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$58, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x01)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$748, DW_AT_name("INTx1")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$748, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$749, DW_AT_name("INTx2")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$749, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$750, DW_AT_name("INTx3")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$750, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$751, DW_AT_name("INTx4")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$751, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$752, DW_AT_name("INTx5")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$752, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$753, DW_AT_name("INTx6")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$753, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$754, DW_AT_name("INTx7")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$754, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$755, DW_AT_name("INTx8")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$755, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$756, DW_AT_name("rsvd1")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$756, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$59, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$757, DW_AT_name("all")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$758, DW_AT_name("bit")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$60, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x01)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$759, DW_AT_name("INTx1")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$759, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$760, DW_AT_name("INTx2")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$760, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$761, DW_AT_name("INTx3")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$761, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$762, DW_AT_name("INTx4")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$762, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$763, DW_AT_name("INTx5")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$763, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$764, DW_AT_name("INTx6")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$764, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$765, DW_AT_name("INTx7")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$765, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$766, DW_AT_name("INTx8")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$766, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$767, DW_AT_name("rsvd1")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$767, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$61, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x01)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$768, DW_AT_name("all")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$769, DW_AT_name("bit")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$62, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x1a)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$770, DW_AT_name("PIECTRL")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$771, DW_AT_name("PIEACK")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$772, DW_AT_name("PIEIER1")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$773, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$774, DW_AT_name("PIEIER2")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$775, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$776, DW_AT_name("PIEIER3")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$777, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$778, DW_AT_name("PIEIER4")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$779, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$780, DW_AT_name("PIEIER5")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$781, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$782, DW_AT_name("PIEIER6")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$783, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$784, DW_AT_name("PIEIER7")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$785, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$786, DW_AT_name("PIEIER8")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$787, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$788, DW_AT_name("PIEIER9")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$789, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$790, DW_AT_name("PIEIER10")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$791, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$792, DW_AT_name("PIEIER11")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$793, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$794, DW_AT_name("PIEIER12")
	.dwattr $C$DW$794, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$795, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62

$C$DW$796	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$62)
$C$DW$T$160	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$796)

$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x04)
$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$797, DW_AT_name("next")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$798, DW_AT_name("prev")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$64

$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$63	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$63, DW_AT_address_class(0x16)

$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x10)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$799, DW_AT_name("job")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$800, DW_AT_name("count")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$801, DW_AT_name("pendQ")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$802, DW_AT_name("name")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66

$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$162	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$162, DW_AT_address_class(0x16)
$C$DW$T$163	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$163, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$164	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$164, DW_AT_language(DW_LANG_C)

$C$DW$T$43	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$803	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$42)
	.dwendtag $C$DW$T$43

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$97	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)
$C$DW$804	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$96)
	.dwendtag $C$DW$T$97

$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
$C$DW$T$99	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$111, DW_AT_language(DW_LANG_C)
$C$DW$T$100	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)

$C$DW$T$106	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)
$C$DW$805	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$96)
$C$DW$806	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$106

$C$DW$T$107	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$107, DW_AT_address_class(0x16)
$C$DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)

$C$DW$T$130	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)
$C$DW$807	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$96)
$C$DW$808	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$6)
$C$DW$809	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$9)
$C$DW$810	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$130

$C$DW$T$131	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$131, DW_AT_address_class(0x16)
$C$DW$T$132	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$175	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$175, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x62)
$C$DW$811	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$811, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$175

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$812	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$812, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$813	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$6)
$C$DW$T$85	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$813)
$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)

$C$DW$T$187	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$187, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$187, DW_AT_byte_size(0x62)
$C$DW$814	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$814, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$187

$C$DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$104	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$104, DW_AT_address_class(0x16)

$C$DW$T$188	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$188, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$188, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$188, DW_AT_byte_size(0x13)
$C$DW$815	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$815, DW_AT_upper_bound(0x12)
	.dwendtag $C$DW$T$188

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$189	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$189, DW_AT_language(DW_LANG_C)
$C$DW$816	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$9)
$C$DW$T$83	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$816)
$C$DW$T$84	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_address_class(0x16)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$198	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$198, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$198, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$198, DW_AT_byte_size(0x01)
$C$DW$817	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$817, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$198

$C$DW$T$67	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)

$C$DW$T$37	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x08)
$C$DW$818	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$818, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$37

$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)

$C$DW$T$87	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)
$C$DW$819	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$6)
$C$DW$820	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$87

$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)
$C$DW$T$103	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$103, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)
$C$DW$821	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$96)
$C$DW$822	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$77)
$C$DW$823	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$6)
$C$DW$824	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$114

$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x16)
$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)
$C$DW$825	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$116)
$C$DW$T$117	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$825)
$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)

$C$DW$T$123	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
$C$DW$826	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$96)
$C$DW$827	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$9)
$C$DW$828	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$123

$C$DW$T$124	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$124, DW_AT_address_class(0x16)
$C$DW$T$125	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
$C$DW$829	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$13)
$C$DW$T$208	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$208, DW_AT_type(*$C$DW$829)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$68	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$51	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$51, DW_AT_address_class(0x16)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)

$C$DW$T$210	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$210, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$210, DW_AT_byte_size(0x10)
$C$DW$830	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$830, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$210


$C$DW$T$69	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$69, DW_AT_name("crc_record")
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x08)
$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$831, DW_AT_name("crc_alg_ID")
	.dwattr $C$DW$831, DW_AT_TI_symbol_name("_crc_alg_ID")
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$832, DW_AT_name("page_id")
	.dwattr $C$DW$832, DW_AT_TI_symbol_name("_page_id")
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$833, DW_AT_name("addr")
	.dwattr $C$DW$833, DW_AT_TI_symbol_name("_addr")
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$834, DW_AT_name("size")
	.dwattr $C$DW$834, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$835	.dwtag  DW_TAG_member
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$835, DW_AT_name("crc_value")
	.dwattr $C$DW$835, DW_AT_TI_symbol_name("_crc_value")
	.dwattr $C$DW$835, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$835, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$69

$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_RECORD")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)

$C$DW$T$71	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x08)
$C$DW$836	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$836, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$71


$C$DW$T$72	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$72, DW_AT_name("crc_table")
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x0a)
$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$837, DW_AT_name("rec_size")
	.dwattr $C$DW$837, DW_AT_TI_symbol_name("_rec_size")
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$838, DW_AT_name("num_recs")
	.dwattr $C$DW$838, DW_AT_TI_symbol_name("_num_recs")
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$839, DW_AT_name("recs")
	.dwattr $C$DW$839, DW_AT_TI_symbol_name("_recs")
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$72

$C$DW$T$213	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_TABLE")
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$213, DW_AT_language(DW_LANG_C)

$C$DW$T$126	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$126, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x01)
$C$DW$840	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$841	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$126

$C$DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)

$C$DW$T$92	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$92, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x01)
$C$DW$842	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$843	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$844	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$845	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$846	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$847	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$848	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$849	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$92

$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)

$C$DW$T$109	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x80)
$C$DW$850	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$850, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$109


$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x06)
$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$851, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$852, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$853, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$854, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$855, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$856, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73

$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$857	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$80)
$C$DW$T$81	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$857)
$C$DW$T$82	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x16)

$C$DW$T$136	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$136, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x132)
$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$858, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$859, DW_AT_name("objdict")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$860, DW_AT_name("PDO_status")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$861, DW_AT_name("firstIndex")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$862, DW_AT_name("lastIndex")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$863, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$864, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$865, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$866, DW_AT_name("transfers")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$867, DW_AT_name("nodeState")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$868, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$869, DW_AT_name("initialisation")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$870, DW_AT_name("preOperational")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$871, DW_AT_name("operational")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$872, DW_AT_name("stopped")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$873, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$874, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$875, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$876, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$876, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$877, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$878, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$879, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$880, DW_AT_name("heartbeatError")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$881, DW_AT_name("NMTable")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$882, DW_AT_name("syncTimer")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$883, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$884, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$885	.dwtag  DW_TAG_member
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$885, DW_AT_name("pre_sync")
	.dwattr $C$DW$885, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$885, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$885, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$886, DW_AT_name("post_TPDO")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$887, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$888, DW_AT_name("toggle")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$889, DW_AT_name("canHandle")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$890, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$891, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$892	.dwtag  DW_TAG_member
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$892, DW_AT_name("globalCallback")
	.dwattr $C$DW$892, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$892, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$892, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$893	.dwtag  DW_TAG_member
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$893, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$893, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$893, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$893, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$894	.dwtag  DW_TAG_member
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$894, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$894, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$894, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$894, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$895	.dwtag  DW_TAG_member
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$895, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$895, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$895, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$895, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$896	.dwtag  DW_TAG_member
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$896, DW_AT_name("dcf_request")
	.dwattr $C$DW$896, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$896, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$896, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$897	.dwtag  DW_TAG_member
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$897, DW_AT_name("error_state")
	.dwattr $C$DW$897, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$897, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$897, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$898	.dwtag  DW_TAG_member
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$898, DW_AT_name("error_history_size")
	.dwattr $C$DW$898, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$898, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$898, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$899	.dwtag  DW_TAG_member
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$899, DW_AT_name("error_number")
	.dwattr $C$DW$899, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$899, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$899, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$900	.dwtag  DW_TAG_member
	.dwattr $C$DW$900, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$900, DW_AT_name("error_first_element")
	.dwattr $C$DW$900, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$900, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$900, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$901	.dwtag  DW_TAG_member
	.dwattr $C$DW$901, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$901, DW_AT_name("error_register")
	.dwattr $C$DW$901, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$901, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$901, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$902	.dwtag  DW_TAG_member
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$902, DW_AT_name("error_cobid")
	.dwattr $C$DW$902, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$902, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$902, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$903	.dwtag  DW_TAG_member
	.dwattr $C$DW$903, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$903, DW_AT_name("error_data")
	.dwattr $C$DW$903, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$903, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$903, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$904	.dwtag  DW_TAG_member
	.dwattr $C$DW$904, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$904, DW_AT_name("post_emcy")
	.dwattr $C$DW$904, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$904, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$904, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$905	.dwtag  DW_TAG_member
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$905, DW_AT_name("lss_transfer")
	.dwattr $C$DW$905, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$905, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$905, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$906	.dwtag  DW_TAG_member
	.dwattr $C$DW$906, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$906, DW_AT_name("eeprom_index")
	.dwattr $C$DW$906, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$906, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$906, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$907	.dwtag  DW_TAG_member
	.dwattr $C$DW$907, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$907, DW_AT_name("eeprom_size")
	.dwattr $C$DW$907, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$907, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$907, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$136

$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$96	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$96, DW_AT_address_class(0x16)

$C$DW$T$138	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$138, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x0e)
$C$DW$908	.dwtag  DW_TAG_member
	.dwattr $C$DW$908, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$908, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$908, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$908, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$908, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$909	.dwtag  DW_TAG_member
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$909, DW_AT_name("event_timer")
	.dwattr $C$DW$909, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$909, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$909, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$910	.dwtag  DW_TAG_member
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$910, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$910, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$910, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$910, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$911	.dwtag  DW_TAG_member
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$911, DW_AT_name("last_message")
	.dwattr $C$DW$911, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$911, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$911, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$138

$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)

$C$DW$T$140	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$140, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x14)
$C$DW$912	.dwtag  DW_TAG_member
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$912, DW_AT_name("nodeId")
	.dwattr $C$DW$912, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$912, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$912, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$913	.dwtag  DW_TAG_member
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$913, DW_AT_name("whoami")
	.dwattr $C$DW$913, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$913, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$913, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$914	.dwtag  DW_TAG_member
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$914, DW_AT_name("state")
	.dwattr $C$DW$914, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$914, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$914, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$915	.dwtag  DW_TAG_member
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$915, DW_AT_name("toggle")
	.dwattr $C$DW$915, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$915, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$915, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$916	.dwtag  DW_TAG_member
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$916, DW_AT_name("abortCode")
	.dwattr $C$DW$916, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$916, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$916, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$917	.dwtag  DW_TAG_member
	.dwattr $C$DW$917, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$917, DW_AT_name("index")
	.dwattr $C$DW$917, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$917, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$917, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$918	.dwtag  DW_TAG_member
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$918, DW_AT_name("subIndex")
	.dwattr $C$DW$918, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$918, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$918, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$919	.dwtag  DW_TAG_member
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$919, DW_AT_name("port")
	.dwattr $C$DW$919, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$919, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$919, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$920	.dwtag  DW_TAG_member
	.dwattr $C$DW$920, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$920, DW_AT_name("count")
	.dwattr $C$DW$920, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$920, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$920, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$921	.dwtag  DW_TAG_member
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$921, DW_AT_name("offset")
	.dwattr $C$DW$921, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$921, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$921, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$922	.dwtag  DW_TAG_member
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$922, DW_AT_name("datap")
	.dwattr $C$DW$922, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$922, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$922, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$923	.dwtag  DW_TAG_member
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$923, DW_AT_name("dataType")
	.dwattr $C$DW$923, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$923, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$923, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$924	.dwtag  DW_TAG_member
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$924, DW_AT_name("timer")
	.dwattr $C$DW$924, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$924, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$924, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$925, DW_AT_name("Callback")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$140

$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)

$C$DW$T$91	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x3c)
$C$DW$926	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$926, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$91


$C$DW$T$144	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$144, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$144, DW_AT_byte_size(0x04)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$927, DW_AT_name("pSubindex")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$928, DW_AT_name("bSubCount")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$929, DW_AT_name("index")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$144

$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$930	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$930, DW_AT_type(*$C$DW$T$75)
$C$DW$T$76	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$930)
$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)

$C$DW$T$120	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)
$C$DW$931	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$931, DW_AT_type(*$C$DW$T$9)
$C$DW$932	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$932, DW_AT_type(*$C$DW$T$103)
$C$DW$933	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$933, DW_AT_type(*$C$DW$T$119)
	.dwendtag $C$DW$T$120

$C$DW$T$121	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$121, DW_AT_address_class(0x16)
$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)

$C$DW$T$145	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$145, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x08)
$C$DW$934	.dwtag  DW_TAG_member
	.dwattr $C$DW$934, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$934, DW_AT_name("bAccessType")
	.dwattr $C$DW$934, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$934, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$934, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$935	.dwtag  DW_TAG_member
	.dwattr $C$DW$935, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$935, DW_AT_name("bDataType")
	.dwattr $C$DW$935, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$935, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$935, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$936	.dwtag  DW_TAG_member
	.dwattr $C$DW$936, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$936, DW_AT_name("size")
	.dwattr $C$DW$936, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$936, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$936, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$937	.dwtag  DW_TAG_member
	.dwattr $C$DW$937, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$937, DW_AT_name("pObject")
	.dwattr $C$DW$937, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$937, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$937, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$938	.dwtag  DW_TAG_member
	.dwattr $C$DW$938, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$938, DW_AT_name("bProcessor")
	.dwattr $C$DW$938, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$938, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$938, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$145

$C$DW$939	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$939, DW_AT_type(*$C$DW$T$145)
$C$DW$T$141	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$939)
$C$DW$T$142	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)
$C$DW$T$143	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$940	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$940, DW_AT_location[DW_OP_reg0]
$C$DW$941	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$941, DW_AT_location[DW_OP_reg1]
$C$DW$942	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$942, DW_AT_location[DW_OP_reg2]
$C$DW$943	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$943, DW_AT_location[DW_OP_reg3]
$C$DW$944	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$944, DW_AT_location[DW_OP_reg20]
$C$DW$945	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$945, DW_AT_location[DW_OP_reg21]
$C$DW$946	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$946, DW_AT_location[DW_OP_reg22]
$C$DW$947	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$947, DW_AT_location[DW_OP_reg23]
$C$DW$948	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$948, DW_AT_location[DW_OP_reg24]
$C$DW$949	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$949, DW_AT_location[DW_OP_reg25]
$C$DW$950	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$950, DW_AT_location[DW_OP_reg26]
$C$DW$951	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$951, DW_AT_location[DW_OP_reg28]
$C$DW$952	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$952, DW_AT_location[DW_OP_reg29]
$C$DW$953	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$953, DW_AT_location[DW_OP_reg30]
$C$DW$954	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$954, DW_AT_location[DW_OP_reg31]
$C$DW$955	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$955, DW_AT_location[DW_OP_regx 0x20]
$C$DW$956	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$956, DW_AT_location[DW_OP_regx 0x21]
$C$DW$957	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$957, DW_AT_location[DW_OP_regx 0x22]
$C$DW$958	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$958, DW_AT_location[DW_OP_regx 0x23]
$C$DW$959	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$959, DW_AT_location[DW_OP_regx 0x24]
$C$DW$960	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$960, DW_AT_location[DW_OP_regx 0x25]
$C$DW$961	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$961, DW_AT_location[DW_OP_regx 0x26]
$C$DW$962	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$962, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$963	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$963, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$964	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$964, DW_AT_location[DW_OP_reg4]
$C$DW$965	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$965, DW_AT_location[DW_OP_reg6]
$C$DW$966	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$966, DW_AT_location[DW_OP_reg8]
$C$DW$967	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$967, DW_AT_location[DW_OP_reg10]
$C$DW$968	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$968, DW_AT_location[DW_OP_reg12]
$C$DW$969	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$969, DW_AT_location[DW_OP_reg14]
$C$DW$970	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$970, DW_AT_location[DW_OP_reg16]
$C$DW$971	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$971, DW_AT_location[DW_OP_reg17]
$C$DW$972	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$972, DW_AT_location[DW_OP_reg18]
$C$DW$973	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$973, DW_AT_location[DW_OP_reg19]
$C$DW$974	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$974, DW_AT_location[DW_OP_reg5]
$C$DW$975	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$975, DW_AT_location[DW_OP_reg7]
$C$DW$976	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$976, DW_AT_location[DW_OP_reg9]
$C$DW$977	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$977, DW_AT_location[DW_OP_reg11]
$C$DW$978	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$978, DW_AT_location[DW_OP_reg13]
$C$DW$979	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$979, DW_AT_location[DW_OP_reg15]
$C$DW$980	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$980, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$981	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$981, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$982	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$982, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$983	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$983, DW_AT_location[DW_OP_regx 0x30]
$C$DW$984	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$984, DW_AT_location[DW_OP_regx 0x33]
$C$DW$985	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$985, DW_AT_location[DW_OP_regx 0x34]
$C$DW$986	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$986, DW_AT_location[DW_OP_regx 0x37]
$C$DW$987	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$987, DW_AT_location[DW_OP_regx 0x38]
$C$DW$988	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$988, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$989	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$989, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$990	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$990, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$991	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$991, DW_AT_location[DW_OP_regx 0x40]
$C$DW$992	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$992, DW_AT_location[DW_OP_regx 0x43]
$C$DW$993	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$993, DW_AT_location[DW_OP_regx 0x44]
$C$DW$994	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$994, DW_AT_location[DW_OP_regx 0x47]
$C$DW$995	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$995, DW_AT_location[DW_OP_regx 0x48]
$C$DW$996	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$996, DW_AT_location[DW_OP_regx 0x49]
$C$DW$997	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$997, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$998	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$998, DW_AT_location[DW_OP_regx 0x27]
$C$DW$999	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$999, DW_AT_location[DW_OP_regx 0x28]
$C$DW$1000	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$1000, DW_AT_location[DW_OP_reg27]
$C$DW$1001	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$1001, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

