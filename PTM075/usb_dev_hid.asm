;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jun 29 19:41:33 2020                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../usb_dev_hid.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM075")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UsbCounter+0,32
	.bits	0,16			; _UsbCounter @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_g_psReportIdle+0,32
	.bits	0,16			; _g_psReportIdle[0]._ucDuration4mS @ 0
	.bits	0,16			; _g_psReportIdle[0]._ucReportID @ 16
	.bits	0,16			; _g_psReportIdle[0]._usTimeTillNextmS @ 32
	.space	16
	.bits	0,32			; _g_psReportIdle[0]._ulTimeSinceReportmS @ 64
$C$IR_1:	.set	6

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_2,16
	.field  	_g_pHIDInEndpoint2+0,32
	.bits	7,16			; _g_pHIDInEndpoint2[0] @ 0
	.bits	5,16			; _g_pHIDInEndpoint2[1] @ 16
	.bits	131,16			; _g_pHIDInEndpoint2[2] @ 32
	.bits	3,16			; _g_pHIDInEndpoint2[3] @ 48
	.bits	64,16			; _g_pHIDInEndpoint2[4] @ 64
	.bits	0,16			; _g_pHIDInEndpoint2[5] @ 80
	.bits	1,16			; _g_pHIDInEndpoint2[6] @ 96
$C$IR_2:	.set	7

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_3,16
	.field  	_g_pHIDOutEndpoint2+0,32
	.bits	7,16			; _g_pHIDOutEndpoint2[0] @ 0
	.bits	5,16			; _g_pHIDOutEndpoint2[1] @ 16
	.bits	3,16			; _g_pHIDOutEndpoint2[2] @ 32
	.bits	3,16			; _g_pHIDOutEndpoint2[3] @ 48
	.bits	64,16			; _g_pHIDOutEndpoint2[4] @ 64
	.bits	0,16			; _g_pHIDOutEndpoint2[5] @ 80
	.bits	1,16			; _g_pHIDOutEndpoint2[6] @ 96
$C$IR_3:	.set	7


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("USBDHIDTerm")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_USBDHIDTerm")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$1

$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("g_psHIDSections")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_g_psHIDSections")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$192)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("g_pHIDDeviceDescriptor")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_g_pHIDDeviceDescriptor")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("canDispatch")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_canDispatch")
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$140)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$190)
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$5

_UsbCounter:	.usect	".ebss",1,1,0
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("UsbCounter")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_UsbCounter")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_addr _UsbCounter]

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$231)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$203)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$207)
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$111)
	.dwendtag $C$DW$10


$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$140)
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$137)
	.dwendtag $C$DW$14


$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$231)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$203)
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$207)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$111)
	.dwendtag $C$DW$17


$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$231)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$206)
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$111)
	.dwendtag $C$DW$21

	.global	_BufferRxLen
_BufferRxLen:	.usect	".ebss",2,1,1
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("BufferRxLen")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_BufferRxLen")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_addr _BufferRxLen]
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$24, DW_AT_external
	.sect	".econst:_g_pPumpClassDescriptors"
	.clink
	.align	2
_g_pPumpClassDescriptors:
	.bits	_g_pucPumpReportDescriptor,32		; _g_pPumpClassDescriptors[0] @ 0

$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("g_pPumpClassDescriptors")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_g_pPumpClassDescriptors")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_addr _g_pPumpClassDescriptors]
	.global	_USB_HID_DevicePtr
_USB_HID_DevicePtr:	.usect	".ebss",2,1,1
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("USB_HID_DevicePtr")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_USB_HID_DevicePtr")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_addr _USB_HID_DevicePtr]
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$195)
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("USBDHIDPacketRead")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_USBDHIDPacketRead")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$3)
$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$86)
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$13)
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$60)
	.dwendtag $C$DW$28


$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("USBDHIDInit")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_USBDHIDInit")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$13)
$C$DW$35	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$197)
	.dwendtag $C$DW$33


$C$DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("USBDHIDReportWrite")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_USBDHIDReportWrite")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$3)
$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$86)
$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$13)
$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$60)
	.dwendtag $C$DW$36

$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
	.global	_g_pSerialNumberString
	.sect	".econst:.string:_g_pSerialNumberString"
	.clink
	.align	1
_g_pSerialNumberString:
	.bits	4,16			; _g_pSerialNumberString[0] @ 0
	.bits	3,16			; _g_pSerialNumberString[1] @ 16
	.bits	48,16			; _g_pSerialNumberString[2] @ 32
	.bits	0,16			; _g_pSerialNumberString[3] @ 48

$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("g_pSerialNumberString")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_g_pSerialNumberString")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_addr _g_pSerialNumberString]
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$42, DW_AT_external
	.global	_g_pLangDescriptor
	.sect	".econst:.string:_g_pLangDescriptor"
	.clink
	.align	1
_g_pLangDescriptor:
	.bits	4,16			; _g_pLangDescriptor[0] @ 0
	.bits	3,16			; _g_pLangDescriptor[1] @ 16
	.bits	9,16			; _g_pLangDescriptor[2] @ 32
	.bits	4,16			; _g_pLangDescriptor[3] @ 48

$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("g_pLangDescriptor")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_g_pLangDescriptor")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_addr _g_pLangDescriptor]
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$43, DW_AT_external
	.global	_g_sHIDInEndpointSection2
	.sect	".econst"
	.align	2
_g_sHIDInEndpointSection2:
	.bits	7,16			; _g_sHIDInEndpointSection2._ucSize @ 0
	.space	16
	.bits	_g_pHIDInEndpoint2,32		; _g_sHIDInEndpointSection2._pucData @ 32

$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("g_sHIDInEndpointSection2")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_g_sHIDInEndpointSection2")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_addr _g_sHIDInEndpointSection2]
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$44, DW_AT_external
	.global	_g_sHIDOutEndpointSection2
	.sect	".econst"
	.align	2
_g_sHIDOutEndpointSection2:
	.bits	7,16			; _g_sHIDOutEndpointSection2._ucSize @ 0
	.space	16
	.bits	_g_pHIDOutEndpoint2,32		; _g_sHIDOutEndpointSection2._pucData @ 32

$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("g_sHIDOutEndpointSection2")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_g_sHIDOutEndpointSection2")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_addr _g_sHIDOutEndpointSection2]
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$45, DW_AT_external
	.global	_g_psReportIdle
_g_psReportIdle:	.usect	".ebss",6,1,1
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("g_psReportIdle")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_g_psReportIdle")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_addr _g_psReportIdle]
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$46, DW_AT_external
	.global	_g_pHIDInEndpoint2
_g_pHIDInEndpoint2:	.usect	".ebss",7,1,0
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("g_pHIDInEndpoint2")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_g_pHIDInEndpoint2")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_addr _g_pHIDInEndpoint2]
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$228)
	.dwattr $C$DW$47, DW_AT_external
	.global	_g_pHIDOutEndpoint2
_g_pHIDOutEndpoint2:	.usect	".ebss",7,1,0
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("g_pHIDOutEndpoint2")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_g_pHIDOutEndpoint2")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_addr _g_pHIDOutEndpoint2]
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$228)
	.dwattr $C$DW$48, DW_AT_external
	.sect	".econst"
	.align	1
_g_sPumpHIDDescriptor:
	.bits	9,16			; _g_sPumpHIDDescriptor._bLength @ 0
	.bits	33,16			; _g_sPumpHIDDescriptor._bDescriptorType @ 16
	.bits	17,16			; _g_sPumpHIDDescriptor._bcdHID._LSB @ 32
	.bits	1,16			; _g_sPumpHIDDescriptor._bcdHID._MSB @ 48
	.bits	0,16			; _g_sPumpHIDDescriptor._bCountryCode @ 64
	.bits	1,16			; _g_sPumpHIDDescriptor._bNumDescriptors @ 80
	.bits	34,16			; _g_sPumpHIDDescriptor._sClassDescriptor[0]._bDescriptorType @ 96
	.bits	33,16			; _g_sPumpHIDDescriptor._sClassDescriptor[0]._wDescriptorLength._LSB @ 112
	.bits	0,16			; _g_sPumpHIDDescriptor._sClassDescriptor[0]._wDescriptorLength._MSB @ 128

$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("g_sPumpHIDDescriptor")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_g_sPumpHIDDescriptor")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_addr _g_sPumpHIDDescriptor]
	.global	_g_pProductString
	.sect	".econst:.string:_g_pProductString"
	.clink
	.align	1
_g_pProductString:
	.bits	10,16			; _g_pProductString[0] @ 0
	.bits	3,16			; _g_pProductString[1] @ 16
	.bits	71,16			; _g_pProductString[2] @ 32
	.bits	0,16			; _g_pProductString[3] @ 48
	.bits	65,16			; _g_pProductString[4] @ 64
	.bits	0,16			; _g_pProductString[5] @ 80
	.bits	84,16			; _g_pProductString[6] @ 96
	.bits	0,16			; _g_pProductString[7] @ 112
	.bits	69,16			; _g_pProductString[8] @ 128
	.bits	0,16			; _g_pProductString[9] @ 144

$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("g_pProductString")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_g_pProductString")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_addr _g_pProductString]
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$218)
	.dwattr $C$DW$50, DW_AT_external
	.global	_g_pStringDescriptors
	.sect	".econst:_g_pStringDescriptors"
	.clink
	.align	2
_g_pStringDescriptors:
	.bits	_g_pLangDescriptor,32		; _g_pStringDescriptors[0] @ 0
	.bits	_g_pManufacturerString,32		; _g_pStringDescriptors[1] @ 32
	.bits	_g_pProductString,32		; _g_pStringDescriptors[2] @ 64
	.bits	_g_pSerialNumberString,32		; _g_pStringDescriptors[3] @ 96
	.bits	_g_pHIDInterfaceString,32		; _g_pStringDescriptors[4] @ 128
	.bits	_g_pConfigString,32		; _g_pStringDescriptors[5] @ 160

$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("g_pStringDescriptors")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_g_pStringDescriptors")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_addr _g_pStringDescriptors]
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$214)
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
	.global	_g_pManufacturerString
	.sect	".econst:.string:_g_pManufacturerString"
	.clink
	.align	1
_g_pManufacturerString:
	.bits	16,16			; _g_pManufacturerString[0] @ 0
	.bits	3,16			; _g_pManufacturerString[1] @ 16
	.bits	65,16			; _g_pManufacturerString[2] @ 32
	.bits	0,16			; _g_pManufacturerString[3] @ 48
	.bits	69,16			; _g_pManufacturerString[4] @ 64
	.bits	0,16			; _g_pManufacturerString[5] @ 80
	.bits	78,16			; _g_pManufacturerString[6] @ 96
	.bits	0,16			; _g_pManufacturerString[7] @ 112
	.bits	84,16			; _g_pManufacturerString[8] @ 128
	.bits	0,16			; _g_pManufacturerString[9] @ 144
	.bits	82,16			; _g_pManufacturerString[10] @ 160
	.bits	0,16			; _g_pManufacturerString[11] @ 176
	.bits	79,16			; _g_pManufacturerString[12] @ 192
	.bits	0,16			; _g_pManufacturerString[13] @ 208
	.bits	78,16			; _g_pManufacturerString[14] @ 224
	.bits	0,16			; _g_pManufacturerString[15] @ 240

$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("g_pManufacturerString")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_g_pManufacturerString")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_addr _g_pManufacturerString]
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$53, DW_AT_external
	.global	_g_sHIDInstance
_g_sHIDInstance:	.usect	".ebss",22,1,1
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("g_sHIDInstance")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_g_sHIDInstance")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_addr _g_sHIDInstance]
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$54, DW_AT_external
	.global	_g_pHIDInterfaceString
	.sect	".econst:.string:_g_pHIDInterfaceString"
	.clink
	.align	1
_g_pHIDInterfaceString:
	.bits	28,16			; _g_pHIDInterfaceString[0] @ 0
	.bits	3,16			; _g_pHIDInterfaceString[1] @ 16
	.bits	72,16			; _g_pHIDInterfaceString[2] @ 32
	.bits	0,16			; _g_pHIDInterfaceString[3] @ 48
	.bits	73,16			; _g_pHIDInterfaceString[4] @ 64
	.bits	0,16			; _g_pHIDInterfaceString[5] @ 80
	.bits	68,16			; _g_pHIDInterfaceString[6] @ 96
	.bits	0,16			; _g_pHIDInterfaceString[7] @ 112
	.bits	32,16			; _g_pHIDInterfaceString[8] @ 128
	.bits	0,16			; _g_pHIDInterfaceString[9] @ 144
	.bits	73,16			; _g_pHIDInterfaceString[10] @ 160
	.bits	0,16			; _g_pHIDInterfaceString[11] @ 176
	.bits	110,16			; _g_pHIDInterfaceString[12] @ 192
	.bits	0,16			; _g_pHIDInterfaceString[13] @ 208
	.bits	116,16			; _g_pHIDInterfaceString[14] @ 224
	.bits	0,16			; _g_pHIDInterfaceString[15] @ 240
	.bits	101,16			; _g_pHIDInterfaceString[16] @ 256
	.bits	0,16			; _g_pHIDInterfaceString[17] @ 272
	.bits	114,16			; _g_pHIDInterfaceString[18] @ 288
	.bits	0,16			; _g_pHIDInterfaceString[19] @ 304
	.bits	102,16			; _g_pHIDInterfaceString[20] @ 320
	.bits	0,16			; _g_pHIDInterfaceString[21] @ 336
	.bits	97,16			; _g_pHIDInterfaceString[22] @ 352
	.bits	0,16			; _g_pHIDInterfaceString[23] @ 368
	.bits	99,16			; _g_pHIDInterfaceString[24] @ 384
	.bits	0,16			; _g_pHIDInterfaceString[25] @ 400
	.bits	101,16			; _g_pHIDInterfaceString[26] @ 416
	.bits	0,16			; _g_pHIDInterfaceString[27] @ 432

$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("g_pHIDInterfaceString")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_g_pHIDInterfaceString")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_addr _g_pHIDInterfaceString]
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$55, DW_AT_external
	.global	_g_sHIDPumpDevice
	.sect	".econst"
	.align	2
_g_sHIDPumpDevice:
	.bits	1137,16			; _g_sHIDPumpDevice._usVID @ 0
	.bits	4201,16			; _g_sHIDPumpDevice._usPID @ 16
	.bits	10,16			; _g_sHIDPumpDevice._usMaxPowermA @ 32
	.bits	192,16			; _g_sHIDPumpDevice._ucPwrAttributes @ 48
	.bits	0,16			; _g_sHIDPumpDevice._ucSubclass @ 64
	.bits	0,16			; _g_sHIDPumpDevice._ucProtocol @ 80
	.bits	1,16			; _g_sHIDPumpDevice._ucNumInputReports @ 96
	.space	16
	.bits	_g_psReportIdle,32		; _g_sHIDPumpDevice._psReportIdle @ 128
	.bits	_USBReceiveEventCallback,32		; _g_sHIDPumpDevice._pfnRxCallback @ 160
	.bits	_g_sHIDPumpDevice,32		; _g_sHIDPumpDevice._pvRxCBData @ 192
	.bits	_USBTransmitEventCallback,32		; _g_sHIDPumpDevice._pfnTxCallback @ 224
	.bits	_g_sHIDPumpDevice,32		; _g_sHIDPumpDevice._pvTxCBData @ 256
	.bits	1,16			; _g_sHIDPumpDevice._bUseOutEndpoint @ 288
	.space	16
	.bits	_g_sPumpHIDDescriptor,32		; _g_sHIDPumpDevice._psHIDDescriptor @ 320
	.bits	_g_pPumpClassDescriptors,32		; _g_sHIDPumpDevice._ppClassDescriptors @ 352
	.bits	_g_pStringDescriptors,32		; _g_sHIDPumpDevice._ppStringDescriptors @ 384
	.bits	6,32			; _g_sHIDPumpDevice._ulNumStringDescriptors @ 416
	.bits	_g_sHIDInstance,32		; _g_sHIDPumpDevice._psPrivateHIDData @ 448

$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("g_sHIDPumpDevice")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_g_sHIDPumpDevice")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_addr _g_sHIDPumpDevice]
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$56, DW_AT_external
	.sect	".econst:.string:_g_pucPumpReportDescriptor"
	.clink
	.align	1
_g_pucPumpReportDescriptor:
	.bits	5,16			; _g_pucPumpReportDescriptor[0] @ 0
	.bits	1,16			; _g_pucPumpReportDescriptor[1] @ 16
	.bits	9,16			; _g_pucPumpReportDescriptor[2] @ 32
	.bits	165,16			; _g_pucPumpReportDescriptor[3] @ 48
	.bits	161,16			; _g_pucPumpReportDescriptor[4] @ 64
	.bits	1,16			; _g_pucPumpReportDescriptor[5] @ 80
	.bits	9,16			; _g_pucPumpReportDescriptor[6] @ 96
	.bits	166,16			; _g_pucPumpReportDescriptor[7] @ 112
	.bits	9,16			; _g_pucPumpReportDescriptor[8] @ 128
	.bits	167,16			; _g_pucPumpReportDescriptor[9] @ 144
	.bits	21,16			; _g_pucPumpReportDescriptor[10] @ 160
	.bits	0,16			; _g_pucPumpReportDescriptor[11] @ 176
	.bits	37,16			; _g_pucPumpReportDescriptor[12] @ 192
	.bits	255,16			; _g_pucPumpReportDescriptor[13] @ 208
	.bits	117,16			; _g_pucPumpReportDescriptor[14] @ 224
	.bits	8,16			; _g_pucPumpReportDescriptor[15] @ 240
	.bits	149,16			; _g_pucPumpReportDescriptor[16] @ 256
	.bits	15,16			; _g_pucPumpReportDescriptor[17] @ 272
	.bits	129,16			; _g_pucPumpReportDescriptor[18] @ 288
	.bits	2,16			; _g_pucPumpReportDescriptor[19] @ 304
	.bits	9,16			; _g_pucPumpReportDescriptor[20] @ 320
	.bits	169,16			; _g_pucPumpReportDescriptor[21] @ 336
	.bits	21,16			; _g_pucPumpReportDescriptor[22] @ 352
	.bits	0,16			; _g_pucPumpReportDescriptor[23] @ 368
	.bits	37,16			; _g_pucPumpReportDescriptor[24] @ 384
	.bits	255,16			; _g_pucPumpReportDescriptor[25] @ 400
	.bits	117,16			; _g_pucPumpReportDescriptor[26] @ 416
	.bits	8,16			; _g_pucPumpReportDescriptor[27] @ 432
	.bits	149,16			; _g_pucPumpReportDescriptor[28] @ 448
	.bits	15,16			; _g_pucPumpReportDescriptor[29] @ 464
	.bits	145,16			; _g_pucPumpReportDescriptor[30] @ 480
	.bits	2,16			; _g_pucPumpReportDescriptor[31] @ 496
	.bits	192,16			; _g_pucPumpReportDescriptor[32] @ 512

$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("g_pucPumpReportDescriptor")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_g_pucPumpReportDescriptor")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$221)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_addr _g_pucPumpReportDescriptor]
	.global	_g_pConfigString
	.sect	".econst:.string:_g_pConfigString"
	.clink
	.align	1
_g_pConfigString:
	.bits	36,16			; _g_pConfigString[0] @ 0
	.bits	3,16			; _g_pConfigString[1] @ 16
	.bits	72,16			; _g_pConfigString[2] @ 32
	.bits	0,16			; _g_pConfigString[3] @ 48
	.bits	73,16			; _g_pConfigString[4] @ 64
	.bits	0,16			; _g_pConfigString[5] @ 80
	.bits	68,16			; _g_pConfigString[6] @ 96
	.bits	0,16			; _g_pConfigString[7] @ 112
	.bits	32,16			; _g_pConfigString[8] @ 128
	.bits	0,16			; _g_pConfigString[9] @ 144
	.bits	67,16			; _g_pConfigString[10] @ 160
	.bits	0,16			; _g_pConfigString[11] @ 176
	.bits	111,16			; _g_pConfigString[12] @ 192
	.bits	0,16			; _g_pConfigString[13] @ 208
	.bits	110,16			; _g_pConfigString[14] @ 224
	.bits	0,16			; _g_pConfigString[15] @ 240
	.bits	102,16			; _g_pConfigString[16] @ 256
	.bits	0,16			; _g_pConfigString[17] @ 272
	.bits	105,16			; _g_pConfigString[18] @ 288
	.bits	0,16			; _g_pConfigString[19] @ 304
	.bits	103,16			; _g_pConfigString[20] @ 320
	.bits	0,16			; _g_pConfigString[21] @ 336
	.bits	117,16			; _g_pConfigString[22] @ 352
	.bits	0,16			; _g_pConfigString[23] @ 368
	.bits	114,16			; _g_pConfigString[24] @ 384
	.bits	0,16			; _g_pConfigString[25] @ 400
	.bits	97,16			; _g_pConfigString[26] @ 416
	.bits	0,16			; _g_pConfigString[27] @ 432
	.bits	116,16			; _g_pConfigString[28] @ 448
	.bits	0,16			; _g_pConfigString[29] @ 464
	.bits	105,16			; _g_pConfigString[30] @ 480
	.bits	0,16			; _g_pConfigString[31] @ 496
	.bits	111,16			; _g_pConfigString[32] @ 512
	.bits	0,16			; _g_pConfigString[33] @ 528
	.bits	110,16			; _g_pConfigString[34] @ 544
	.bits	0,16			; _g_pConfigString[35] @ 560

$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("g_pConfigString")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_g_pConfigString")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_addr _g_pConfigString]
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$220)
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("usb_canrx_mbox")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_usb_canrx_mbox")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("usb_tx_mbox")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_usb_tx_mbox")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
	.global	_BufferRx
_BufferRx:	.usect	".ebss",66,1,0
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("BufferRx")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_BufferRx")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_addr _BufferRx]
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$225)
	.dwattr $C$DW$62, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0505212 
	.sect	".text"
	.clink
	.global	_USB_Init

$C$DW$63	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Init")
	.dwattr $C$DW$63, DW_AT_low_pc(_USB_Init)
	.dwattr $C$DW$63, DW_AT_high_pc(0x00)
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_USB_Init")
	.dwattr $C$DW$63, DW_AT_external
	.dwattr $C$DW$63, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$63, DW_AT_TI_begin_line(0x64)
	.dwattr $C$DW$63, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$63, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../usb_dev_hid.c",line 100,column 20,is_stmt,address _USB_Init

	.dwfde $C$DW$CIE, _USB_Init

;***************************************************************
;* FNAME: _USB_Init                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_USB_Init:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../usb_dev_hid.c",line 102,column 1,is_stmt
$C$DW$64	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$64, DW_AT_low_pc(0x00)
	.dwattr $C$DW$64, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$63, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$63, DW_AT_TI_end_line(0x66)
	.dwattr $C$DW$63, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$63

	.sect	".text"
	.clink
	.global	_USBReceiveEventCallback

$C$DW$65	.dwtag  DW_TAG_subprogram, DW_AT_name("USBReceiveEventCallback")
	.dwattr $C$DW$65, DW_AT_low_pc(_USBReceiveEventCallback)
	.dwattr $C$DW$65, DW_AT_high_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_USBReceiveEventCallback")
	.dwattr $C$DW$65, DW_AT_external
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$65, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$65, DW_AT_TI_begin_line(0xba)
	.dwattr $C$DW$65, DW_AT_TI_begin_column(0x0f)
	.dwattr $C$DW$65, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../usb_dev_hid.c",line 188,column 1,is_stmt,address _USBReceiveEventCallback

	.dwfde $C$DW$CIE, _USBReceiveEventCallback
$C$DW$66	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pvCBData")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_pvCBData")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg12]
$C$DW$67	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ulEvent")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ulEvent")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg0]
$C$DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ulMsgData")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ulMsgData")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -16]
$C$DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pvMsgData")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_pvMsgData")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _USBReceiveEventCallback      FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_USBReceiveEventCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("pvCBData")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_pvCBData")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -4]
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("ulEvent")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ulEvent")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -6]
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("pvMsgData")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_pvMsgData")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -8]
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("counter")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_counter")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_breg20 -9]
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -12]
        MOVL      *-SP[8],XAR5          ; [CPU_] |188| 
        MOVL      *-SP[6],ACC           ; [CPU_] |188| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |188| 
	.dwpsn	file "../usb_dev_hid.c",line 200,column 3,is_stmt
        MOV       *-SP[9],#2000         ; [CPU_] |200| 
	.dwpsn	file "../usb_dev_hid.c",line 201,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |201| 
        MOVL      *-SP[12],ACC          ; [CPU_] |201| 
	.dwpsn	file "../usb_dev_hid.c",line 202,column 3,is_stmt
        B         $C$L10,UNC            ; [CPU_] |202| 
        ; branch occurs ; [] |202| 
$C$L1:    
	.dwpsn	file "../usb_dev_hid.c",line 213,column 7,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |213| 
	.dwpsn	file "../usb_dev_hid.c",line 214,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |214| 
        ; branch occurs ; [] |214| 
$C$L2:    
	.dwpsn	file "../usb_dev_hid.c",line 217,column 7,is_stmt
        MOVW      DP,#_USB_HID_DevicePtr ; [CPU_U] 
        MOVL      XAR5,#_BufferRx       ; [CPU_U] |217| 
        MOV       *-SP[1],#0            ; [CPU_] |217| 
        MOVL      XAR4,@_USB_HID_DevicePtr ; [CPU_] |217| 
        MOVB      ACC,#15               ; [CPU_] |217| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("_USBDHIDPacketRead")
	.dwattr $C$DW$75, DW_AT_TI_call
        LCR       #_USBDHIDPacketRead   ; [CPU_] |217| 
        ; call occurs [#_USBDHIDPacketRead] ; [] |217| 
        MOVW      DP,#_BufferRxLen      ; [CPU_U] 
        MOVL      @_BufferRxLen,ACC     ; [CPU_] |217| 
	.dwpsn	file "../usb_dev_hid.c",line 218,column 7,is_stmt
        MOVL      ACC,@_BufferRxLen     ; [CPU_] |218| 
        BF        $C$L14,EQ             ; [CPU_] |218| 
        ; branchcc occurs ; [] |218| 
	.dwpsn	file "../usb_dev_hid.c",line 219,column 9,is_stmt
        MOVW      DP,#_BufferRx+1       ; [CPU_U] 
        MOV       ACC,@_BufferRx+1 << #8 ; [CPU_] |219| 
        ADD       AL,@_BufferRx         ; [CPU_] |219| 
        MOV       @_BufferRx+1,AL       ; [CPU_] |219| 
	.dwpsn	file "../usb_dev_hid.c",line 221,column 9,is_stmt
        MOV       AL,@_BufferRx         ; [CPU_] |221| 
        CMPB      AL,#99                ; [CPU_] |221| 
        BF        $C$L3,NEQ             ; [CPU_] |221| 
        ; branchcc occurs ; [] |221| 
	.dwpsn	file "../usb_dev_hid.c",line 222,column 11,is_stmt
        MOVL      XAR5,#_BufferRx+1     ; [CPU_U] |222| 
        MOVL      XAR4,#_usb_canrx_mbox ; [CPU_U] |222| 
        MOVB      AL,#0                 ; [CPU_] |222| 
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_MBX_post")
	.dwattr $C$DW$76, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |222| 
        ; call occurs [#_MBX_post] ; [] |222| 
        B         $C$L14,UNC            ; [CPU_] |222| 
        ; branch occurs ; [] |222| 
$C$L3:    
	.dwpsn	file "../usb_dev_hid.c",line 224,column 11,is_stmt
        MOVL      XAR5,#_BufferRx+1     ; [CPU_U] |224| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |224| 
        MOVB      AL,#0                 ; [CPU_] |224| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_MBX_post")
	.dwattr $C$DW$77, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |224| 
        ; call occurs [#_MBX_post] ; [] |224| 
	.dwpsn	file "../usb_dev_hid.c",line 225,column 11,is_stmt
        MOVW      DP,#_BufferRx+5       ; [CPU_U] 
        MOV       AL,@_BufferRx+5       ; [CPU_] |225| 
        CMPB      AL,#2                 ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
        MOV       AL,@_BufferRx+6       ; [CPU_] |225| 
        CMPB      AL,#47                ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
        MOV       AL,@_BufferRx+7       ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
        MOV       AL,@_BufferRx+8       ; [CPU_] |225| 
        CMPB      AL,#111               ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
        MOV       AL,@_BufferRx+9       ; [CPU_] |225| 
        CMPB      AL,#114               ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
        MOV       AL,@_BufferRx+10      ; [CPU_] |225| 
        CMPB      AL,#101               ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
        MOV       AL,@_BufferRx+11      ; [CPU_] |225| 
        CMPB      AL,#122               ; [CPU_] |225| 
        BF        $C$L4,NEQ             ; [CPU_] |225| 
        ; branchcc occurs ; [] |225| 
	.dwpsn	file "../usb_dev_hid.c",line 225,column 176,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#4                 ; [CPU_] |225| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |225| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_setState")
	.dwattr $C$DW$78, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |225| 
        ; call occurs [#_setState] ; [] |225| 
$C$L4:    
	.dwpsn	file "../usb_dev_hid.c",line 226,column 11,is_stmt
        MOVW      DP,#_BufferRx+4       ; [CPU_U] 
        MOV       AL,@_BufferRx+4       ; [CPU_] |226| 
        CMPB      AL,#43                ; [CPU_] |226| 
        BF        $C$L14,NEQ            ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
        MOV       AL,@_BufferRx+5       ; [CPU_] |226| 
        CMPB      AL,#2                 ; [CPU_] |226| 
        BF        $C$L14,NEQ            ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
        MOV       AL,@_BufferRx+6       ; [CPU_] |226| 
        CMPB      AL,#32                ; [CPU_] |226| 
        BF        $C$L14,NEQ            ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
        MOV       AL,@_BufferRx+7       ; [CPU_] |226| 
        BF        $C$L14,NEQ            ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
        MOV       AL,@_BufferRx+8       ; [CPU_] |226| 
        CMPB      AL,#2                 ; [CPU_] |226| 
        BF        $C$L14,NEQ            ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
	.dwpsn	file "../usb_dev_hid.c",line 226,column 128,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#5                 ; [CPU_] |226| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |226| 
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_name("_setState")
	.dwattr $C$DW$79, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |226| 
        ; call occurs [#_setState] ; [] |226| 
	.dwpsn	file "../usb_dev_hid.c",line 229,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |229| 
        ; branch occurs ; [] |229| 
$C$L5:    
	.dwpsn	file "../usb_dev_hid.c",line 237,column 7,is_stmt
        MOVL      XAR5,*-SP[8]          ; [CPU_] |237| 
        MOVL      XAR4,#_BufferRx       ; [CPU_U] |237| 
        MOVL      *+XAR5[0],XAR4        ; [CPU_] |237| 
	.dwpsn	file "../usb_dev_hid.c",line 238,column 7,is_stmt
        MOVB      ACC,#64               ; [CPU_] |238| 
        MOVL      *-SP[12],ACC          ; [CPU_] |238| 
	.dwpsn	file "../usb_dev_hid.c",line 239,column 7,is_stmt
        B         $C$L14,UNC            ; [CPU_] |239| 
        ; branch occurs ; [] |239| 
$C$L6:    
	.dwpsn	file "../usb_dev_hid.c",line 257,column 7,is_stmt
        MOVL      XAR4,#_BufferRx       ; [CPU_U] |257| 
        MOVL      *-SP[12],XAR4         ; [CPU_] |257| 
	.dwpsn	file "../usb_dev_hid.c",line 258,column 7,is_stmt
        B         $C$L14,UNC            ; [CPU_] |258| 
        ; branch occurs ; [] |258| 
$C$L7:    
	.dwpsn	file "../usb_dev_hid.c",line 273,column 7,is_stmt
        MOVW      DP,#_USB_HID_DevicePtr ; [CPU_U] 
        MOVL      XAR4,@_USB_HID_DevicePtr ; [CPU_] |273| 
        MOV       AL,*-SP[16]           ; [CPU_] |273| 
        MOV       *+XAR4[5],AL          ; [CPU_] |273| 
	.dwpsn	file "../usb_dev_hid.c",line 274,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |274| 
        ; branch occurs ; [] |274| 
$C$L8:    
	.dwpsn	file "../usb_dev_hid.c",line 281,column 7,is_stmt
        MOVW      DP,#_USB_HID_DevicePtr ; [CPU_U] 
        MOVL      XAR4,@_USB_HID_DevicePtr ; [CPU_] |281| 
        MOVU      ACC,*+XAR4[5]         ; [CPU_] |281| 
        MOVL      *-SP[12],ACC          ; [CPU_] |281| 
	.dwpsn	file "../usb_dev_hid.c",line 282,column 7,is_stmt
        B         $C$L14,UNC            ; [CPU_] |282| 
        ; branch occurs ; [] |282| 
$C$L9:    
	.dwpsn	file "../usb_dev_hid.c",line 290,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |290| 
        MOVL      *-SP[12],ACC          ; [CPU_] |290| 
	.dwpsn	file "../usb_dev_hid.c",line 291,column 7,is_stmt
        B         $C$L14,UNC            ; [CPU_] |291| 
        ; branch occurs ; [] |291| 
$C$L10:    
	.dwpsn	file "../usb_dev_hid.c",line 202,column 3,is_stmt
        MOVL      XAR6,*-SP[6]          ; [CPU_] |202| 
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#36864            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        B         $C$L12,LT             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L5,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOVB      ACC,#6                ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        B         $C$L11,LT             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L9,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOVB      ACC,#0                ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L14,EQ             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOVB      ACC,#1                ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L1,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOVB      ACC,#2                ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L2,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        B         $C$L14,UNC            ; [CPU_] |202| 
        ; branch occurs ; [] |202| 
$C$L11:    
        MOVB      ACC,#7                ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L9,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOVB      ACC,#8                ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L9,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        B         $C$L14,UNC            ; [CPU_] |202| 
        ; branch occurs ; [] |202| 
$C$L12:    
        MOV       ACC,#36868            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        B         $C$L13,LT             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L8,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOV       ACC,#36865            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L14,EQ             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOV       ACC,#36866            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L6,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOV       ACC,#36867            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L14,EQ             ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        B         $C$L14,UNC            ; [CPU_] |202| 
        ; branch occurs ; [] |202| 
$C$L13:    
        MOV       ACC,#36869            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L7,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
        MOV       ACC,#36870            ; [CPU_] |202| 
        CMPL      ACC,XAR6              ; [CPU_] |202| 
        BF        $C$L5,EQ              ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
$C$L14:    
	.dwpsn	file "../usb_dev_hid.c",line 299,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |299| 
        MOVW      DP,#_UsbCounter       ; [CPU_U] 
        MOV       @_UsbCounter,AL       ; [CPU_] |299| 
	.dwpsn	file "../usb_dev_hid.c",line 300,column 3,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |300| 
	.dwpsn	file "../usb_dev_hid.c",line 301,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$65, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$65, DW_AT_TI_end_line(0x12d)
	.dwattr $C$DW$65, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$65

	.sect	".text"
	.clink
	.global	_USBTransmitEventCallback

$C$DW$81	.dwtag  DW_TAG_subprogram, DW_AT_name("USBTransmitEventCallback")
	.dwattr $C$DW$81, DW_AT_low_pc(_USBTransmitEventCallback)
	.dwattr $C$DW$81, DW_AT_high_pc(0x00)
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_USBTransmitEventCallback")
	.dwattr $C$DW$81, DW_AT_external
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$81, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$81, DW_AT_TI_begin_line(0x12f)
	.dwattr $C$DW$81, DW_AT_TI_begin_column(0x0f)
	.dwattr $C$DW$81, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../usb_dev_hid.c",line 305,column 1,is_stmt,address _USBTransmitEventCallback

	.dwfde $C$DW$CIE, _USBTransmitEventCallback
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pvCBData")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_pvCBData")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg12]
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ulEvent")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_ulEvent")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg0]
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ulMsgData")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_ulMsgData")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -10]
$C$DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pvMsgData")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_pvMsgData")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _USBTransmitEventCallback     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_USBTransmitEventCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("pvCBData")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_pvCBData")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -2]
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("ulEvent")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_ulEvent")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -4]
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("pvMsgData")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_pvMsgData")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[6],XAR5          ; [CPU_] |305| 
        MOVL      *-SP[4],ACC           ; [CPU_] |305| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |305| 
	.dwpsn	file "../usb_dev_hid.c",line 306,column 3,is_stmt
        B         $C$L16,UNC            ; [CPU_] |306| 
        ; branch occurs ; [] |306| 
$C$L15:    
	.dwpsn	file "../usb_dev_hid.c",line 309,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |309| 
        B         $C$L17,UNC            ; [CPU_] |309| 
        ; branch occurs ; [] |309| 
$C$L16:    
	.dwpsn	file "../usb_dev_hid.c",line 306,column 3,is_stmt
        MOVL      XAR6,*-SP[4]          ; [CPU_] |306| 
        MOVB      ACC,#5                ; [CPU_] |306| 
        CMPL      ACC,XAR6              ; [CPU_] |306| 
        BF        $C$L15,EQ             ; [CPU_] |306| 
        ; branchcc occurs ; [] |306| 
	.dwpsn	file "../usb_dev_hid.c",line 315,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |315| 
$C$L17:    
	.dwpsn	file "../usb_dev_hid.c",line 316,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$81, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$81, DW_AT_TI_end_line(0x13c)
	.dwattr $C$DW$81, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$81

	.sect	".text"
	.clink
	.global	_USB_Tick

$C$DW$90	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Tick")
	.dwattr $C$DW$90, DW_AT_low_pc(_USB_Tick)
	.dwattr $C$DW$90, DW_AT_high_pc(0x00)
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_USB_Tick")
	.dwattr $C$DW$90, DW_AT_external
	.dwattr $C$DW$90, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$90, DW_AT_TI_begin_line(0x1be)
	.dwattr $C$DW$90, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$90, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../usb_dev_hid.c",line 446,column 20,is_stmt,address _USB_Tick

	.dwfde $C$DW$CIE, _USB_Tick

;***************************************************************
;* FNAME: _USB_Tick                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_USB_Tick:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../usb_dev_hid.c",line 447,column 3,is_stmt
        MOVW      DP,#_UsbCounter       ; [CPU_U] 
        MOV       AL,@_UsbCounter       ; [CPU_] |447| 
        CMPB      AL,#1                 ; [CPU_] |447| 
        B         $C$L18,LOS            ; [CPU_] |447| 
        ; branchcc occurs ; [] |447| 
	.dwpsn	file "../usb_dev_hid.c",line 448,column 5,is_stmt
        DEC       @_UsbCounter          ; [CPU_] |448| 
	.dwpsn	file "../usb_dev_hid.c",line 449,column 3,is_stmt
        B         $C$L19,UNC            ; [CPU_] |449| 
        ; branch occurs ; [] |449| 
$C$L18:    
	.dwpsn	file "../usb_dev_hid.c",line 451,column 5,is_stmt
        MOV       @_UsbCounter,#0       ; [CPU_] |451| 
	.dwpsn	file "../usb_dev_hid.c",line 453,column 1,is_stmt
$C$L19:    
$C$DW$91	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$91, DW_AT_low_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$90, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$90, DW_AT_TI_end_line(0x1c5)
	.dwattr $C$DW$90, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$90

	.sect	".text"
	.clink
	.global	_USB_Connected

$C$DW$92	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Connected")
	.dwattr $C$DW$92, DW_AT_low_pc(_USB_Connected)
	.dwattr $C$DW$92, DW_AT_high_pc(0x00)
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_USB_Connected")
	.dwattr $C$DW$92, DW_AT_external
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$92, DW_AT_TI_begin_line(0x1c7)
	.dwattr $C$DW$92, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$92, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../usb_dev_hid.c",line 455,column 26,is_stmt,address _USB_Connected

	.dwfde $C$DW$CIE, _USB_Connected

;***************************************************************
;* FNAME: _USB_Connected                FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_USB_Connected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../usb_dev_hid.c",line 456,column 3,is_stmt
        MOVW      DP,#_UsbCounter       ; [CPU_U] 
        MOVB      AL,#0                 ; [CPU_] |456| 
        MOV       AH,@_UsbCounter       ; [CPU_] |456| 
        CMPB      AH,#0                 ; [CPU_] |456| 
        BF        $C$L20,EQ             ; [CPU_] |456| 
        ; branchcc occurs ; [] |456| 
        MOVB      AL,#1                 ; [CPU_] |456| 
$C$L20:    
	.dwpsn	file "../usb_dev_hid.c",line 457,column 1,is_stmt
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$92, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$92, DW_AT_TI_end_line(0x1c9)
	.dwattr $C$DW$92, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$92

	.sect	".text"
	.clink
	.global	_USB_Stop

$C$DW$94	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Stop")
	.dwattr $C$DW$94, DW_AT_low_pc(_USB_Stop)
	.dwattr $C$DW$94, DW_AT_high_pc(0x00)
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_USB_Stop")
	.dwattr $C$DW$94, DW_AT_external
	.dwattr $C$DW$94, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$94, DW_AT_TI_begin_line(0x1cb)
	.dwattr $C$DW$94, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$94, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../usb_dev_hid.c",line 459,column 20,is_stmt,address _USB_Stop

	.dwfde $C$DW$CIE, _USB_Stop

;***************************************************************
;* FNAME: _USB_Stop                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_USB_Stop:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../usb_dev_hid.c",line 460,column 3,is_stmt
        MOVW      DP,#_USB_HID_DevicePtr ; [CPU_U] 
        MOVL      XAR4,@_USB_HID_DevicePtr ; [CPU_] |460| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_USBDHIDTerm")
	.dwattr $C$DW$95, DW_AT_TI_call
        LCR       #_USBDHIDTerm         ; [CPU_] |460| 
        ; call occurs [#_USBDHIDTerm] ; [] |460| 
	.dwpsn	file "../usb_dev_hid.c",line 461,column 1,is_stmt
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$94, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$94, DW_AT_TI_end_line(0x1cd)
	.dwattr $C$DW$94, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$94

	.sect	".text"
	.clink
	.global	_USB_Start

$C$DW$97	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Start")
	.dwattr $C$DW$97, DW_AT_low_pc(_USB_Start)
	.dwattr $C$DW$97, DW_AT_high_pc(0x00)
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_USB_Start")
	.dwattr $C$DW$97, DW_AT_external
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$97, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$97, DW_AT_TI_begin_line(0x1d1)
	.dwattr $C$DW$97, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$97, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../usb_dev_hid.c",line 466,column 1,is_stmt,address _USB_Start

	.dwfde $C$DW$CIE, _USB_Start

;***************************************************************
;* FNAME: _USB_Start                    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_USB_Start:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("devicedescr")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_devicedescr")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../usb_dev_hid.c",line 467,column 34,is_stmt
        MOVL      XAR4,#_g_pHIDDeviceDescriptor ; [CPU_U] |467| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |467| 
	.dwpsn	file "../usb_dev_hid.c",line 468,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |468| 
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVB      XAR0,#12              ; [CPU_] |468| 
        MOVB      AL.LSB,@_ODP_Board_RevisionNumber ; [CPU_] |468| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |468| 
	.dwpsn	file "../usb_dev_hid.c",line 469,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |469| 
        MOVB      XAR0,#13              ; [CPU_] |469| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |469| 
	.dwpsn	file "../usb_dev_hid.c",line 470,column 3,is_stmt
        MOVW      DP,#_g_psHIDSections+6 ; [CPU_U] 
        MOVL      XAR4,#_g_sHIDInEndpointSection2 ; [CPU_U] |470| 
        MOVL      @_g_psHIDSections+6,XAR4 ; [CPU_] |470| 
	.dwpsn	file "../usb_dev_hid.c",line 471,column 3,is_stmt
        MOVL      XAR4,#_g_sHIDOutEndpointSection2 ; [CPU_U] |471| 
        MOVL      @_g_psHIDSections+8,XAR4 ; [CPU_] |471| 
	.dwpsn	file "../usb_dev_hid.c",line 473,column 3,is_stmt
        MOVL      XAR4,#_g_sHIDPumpDevice ; [CPU_U] |473| 
        MOVB      ACC,#0                ; [CPU_] |473| 
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("_USBDHIDInit")
	.dwattr $C$DW$99, DW_AT_TI_call
        LCR       #_USBDHIDInit         ; [CPU_] |473| 
        ; call occurs [#_USBDHIDInit] ; [] |473| 
        MOVW      DP,#_USB_HID_DevicePtr ; [CPU_U] 
        MOVL      @_USB_HID_DevicePtr,XAR4 ; [CPU_] |473| 
	.dwpsn	file "../usb_dev_hid.c",line 475,column 3,is_stmt
        MOVL      ACC,@_USB_HID_DevicePtr ; [CPU_] |475| 
        BF        $C$L21,NEQ            ; [CPU_] |475| 
        ; branchcc occurs ; [] |475| 
	.dwpsn	file "../usb_dev_hid.c",line 476,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |476| 
        B         $C$L22,UNC            ; [CPU_] |476| 
        ; branch occurs ; [] |476| 
$C$L21:    
	.dwpsn	file "../usb_dev_hid.c",line 478,column 5,is_stmt
        MOVB      AL,#1                 ; [CPU_] |478| 
$C$L22:    
	.dwpsn	file "../usb_dev_hid.c",line 479,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$97, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$97, DW_AT_TI_end_line(0x1df)
	.dwattr $C$DW$97, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$97

	.sect	".text"
	.clink
	.global	_TaskUsbRx

$C$DW$101	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskUsbRx")
	.dwattr $C$DW$101, DW_AT_low_pc(_TaskUsbRx)
	.dwattr $C$DW$101, DW_AT_high_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_TaskUsbRx")
	.dwattr $C$DW$101, DW_AT_external
	.dwattr $C$DW$101, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$101, DW_AT_TI_begin_line(0x1e1)
	.dwattr $C$DW$101, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$101, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../usb_dev_hid.c",line 481,column 21,is_stmt,address _TaskUsbRx

	.dwfde $C$DW$CIE, _TaskUsbRx

;***************************************************************
;* FNAME: _TaskUsbRx                    FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_TaskUsbRx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -11]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../usb_dev_hid.c",line 485,column 3,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |485| 
	.dwpsn	file "../usb_dev_hid.c",line 486,column 3,is_stmt
        MOVB      *-SP[9],#8,UNC        ; [CPU_] |486| 
	.dwpsn	file "../usb_dev_hid.c",line 487,column 3,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |487| 
	.dwpsn	file "../usb_dev_hid.c",line 488,column 8,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |488| 
	.dwpsn	file "../usb_dev_hid.c",line 488,column 12,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |488| 
        CMPB      AL,#8                 ; [CPU_] |488| 
        B         $C$L24,GEQ            ; [CPU_] |488| 
        ; branchcc occurs ; [] |488| 
$C$L23:    
	.dwpsn	file "../usb_dev_hid.c",line 489,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[12]          ; [CPU_] |489| 
        MOVZ      AR6,*-SP[12]          ; [CPU_] |489| 
        MOVZ      AR4,SP                ; [CPU_U] |489| 
        SUBB      XAR4,#8               ; [CPU_U] |489| 
        ADDL      XAR4,ACC              ; [CPU_] |489| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |489| 
	.dwpsn	file "../usb_dev_hid.c",line 488,column 17,is_stmt
        INC       *-SP[12]              ; [CPU_] |488| 
	.dwpsn	file "../usb_dev_hid.c",line 488,column 12,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |488| 
        CMPB      AL,#8                 ; [CPU_] |488| 
        B         $C$L23,LT             ; [CPU_] |488| 
        ; branchcc occurs ; [] |488| 
$C$L24:    
	.dwpsn	file "../usb_dev_hid.c",line 491,column 10,is_stmt
$C$L25:    
	.dwpsn	file "../usb_dev_hid.c",line 492,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |492| 
        MOVL      XAR4,#_usb_canrx_mbox ; [CPU_U] |492| 
        MOVB      AL,#0                 ; [CPU_] |492| 
        SUBB      XAR5,#11              ; [CPU_U] |492| 
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$104, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |492| 
        ; call occurs [#_MBX_pend] ; [] |492| 
        CMPB      AL,#0                 ; [CPU_] |492| 
        BF        $C$L26,EQ             ; [CPU_] |492| 
        ; branchcc occurs ; [] |492| 
	.dwpsn	file "../usb_dev_hid.c",line 493,column 7,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |493| 
        MOVB      AL,#2                 ; [CPU_] |493| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |493| 
        SUBB      XAR5,#11              ; [CPU_U] |493| 
$C$DW$105	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$105, DW_AT_low_pc(0x00)
	.dwattr $C$DW$105, DW_AT_name("_canDispatch")
	.dwattr $C$DW$105, DW_AT_TI_call
        LCR       #_canDispatch         ; [CPU_] |493| 
        ; call occurs [#_canDispatch] ; [] |493| 
$C$L26:    
	.dwpsn	file "../usb_dev_hid.c",line 495,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |495| 
        MOVB      AL,#1                 ; [CPU_] |495| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |495| 
        ; call occurs [#_SEM_pend] ; [] |495| 
	.dwpsn	file "../usb_dev_hid.c",line 491,column 10,is_stmt
        B         $C$L25,UNC            ; [CPU_] |491| 
        ; branch occurs ; [] |491| 
	.dwattr $C$DW$101, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$101, DW_AT_TI_end_line(0x1f1)
	.dwattr $C$DW$101, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$101

	.sect	".text"
	.clink
	.global	_TaskUsbTx

$C$DW$107	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskUsbTx")
	.dwattr $C$DW$107, DW_AT_low_pc(_TaskUsbTx)
	.dwattr $C$DW$107, DW_AT_high_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_TaskUsbTx")
	.dwattr $C$DW$107, DW_AT_external
	.dwattr $C$DW$107, DW_AT_TI_begin_file("../usb_dev_hid.c")
	.dwattr $C$DW$107, DW_AT_TI_begin_line(0x1f3)
	.dwattr $C$DW$107, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$107, DW_AT_TI_max_frame_size(-70)
	.dwpsn	file "../usb_dev_hid.c",line 499,column 21,is_stmt,address _TaskUsbTx

	.dwfde $C$DW$CIE, _TaskUsbTx

;***************************************************************
;* FNAME: _TaskUsbTx                    FR SIZE:  68           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 64 Auto,  2 SOE     *
;***************************************************************

_TaskUsbTx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR2            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 9, 2
	.dwcfi	cfa_offset, -4
        MOVZ      AR2,SP                ; [CPU_] 
        SUBB      FP,#4                 ; [CPU_U] 
        ADDB      SP,#66                ; [CPU_U] 
	.dwcfi	cfa_offset, -70
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$230)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_breg20 -65]
	.dwpsn	file "../usb_dev_hid.c",line 501,column 10,is_stmt
$C$L27:    
	.dwpsn	file "../usb_dev_hid.c",line 502,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |502| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |502| 
        MOV       AL,#65535             ; [CPU_] |502| 
        SUBB      XAR5,#64              ; [CPU_U] |502| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |502| 
        ; call occurs [#_MBX_pend] ; [] |502| 
	.dwpsn	file "../usb_dev_hid.c",line 503,column 5,is_stmt
        MOV       AL,*+FP[6]            ; [CPU_] |503| 
        ANDB      AL,#0xff              ; [CPU_] |503| 
        MOV       *+FP[5],AL            ; [CPU_] |503| 
	.dwpsn	file "../usb_dev_hid.c",line 504,column 5,is_stmt
        MOV       AL,*+FP[6]            ; [CPU_] |504| 
        LSR       AL,8                  ; [CPU_] |504| 
        MOV       *+FP[6],AL            ; [CPU_] |504| 
	.dwpsn	file "../usb_dev_hid.c",line 505,column 5,is_stmt
        MOVW      DP,#_USB_HID_DevicePtr ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |505| 
        MOVB      ACC,#15               ; [CPU_] |505| 
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |505| 
        SUBB      XAR5,#65              ; [CPU_U] |505| 
        MOVL      XAR4,@_USB_HID_DevicePtr ; [CPU_] |505| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_USBDHIDReportWrite")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_USBDHIDReportWrite  ; [CPU_] |505| 
        ; call occurs [#_USBDHIDReportWrite] ; [] |505| 
	.dwpsn	file "../usb_dev_hid.c",line 501,column 10,is_stmt
        B         $C$L27,UNC            ; [CPU_] |501| 
        ; branch occurs ; [] |501| 
	.dwattr $C$DW$107, DW_AT_TI_end_file("../usb_dev_hid.c")
	.dwattr $C$DW$107, DW_AT_TI_end_line(0x1fb)
	.dwattr $C$DW$107, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$107

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_USBDHIDTerm
	.global	_g_psHIDSections
	.global	_g_pHIDDeviceDescriptor
	.global	_canDispatch
	.global	_MBX_post
	.global	_setState
	.global	_MBX_pend
	.global	_SEM_pend
	.global	_BoardODdata
	.global	_USBDHIDPacketRead
	.global	_USBDHIDInit
	.global	_USBDHIDReportWrite
	.global	_ODP_Board_RevisionNumber
	.global	_TSK_timerSem
	.global	_usb_canrx_mbox
	.global	_can_tx_mbox
	.global	_usb_tx_mbox

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$82	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$82, DW_AT_byte_size(0x01)
$C$DW$111	.dwtag  DW_TAG_enumerator, DW_AT_name("HID_STATE_UNCONFIGURED"), DW_AT_const_value(0x00)
$C$DW$112	.dwtag  DW_TAG_enumerator, DW_AT_name("HID_STATE_IDLE"), DW_AT_const_value(0x01)
$C$DW$113	.dwtag  DW_TAG_enumerator, DW_AT_name("HID_STATE_WAIT_DATA"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$82

$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("tHIDState")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)
$C$DW$114	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$83)
$C$DW$T$84	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$114)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$115, DW_AT_name("cob_id")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$116, DW_AT_name("rtr")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$117, DW_AT_name("len")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$118, DW_AT_name("data")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$181	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$181, DW_AT_language(DW_LANG_C)
$C$DW$T$190	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$T$190, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$119, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$120, DW_AT_name("csSDO")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$121, DW_AT_name("csEmergency")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$122, DW_AT_name("csSYNC")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$123, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$124, DW_AT_name("csPDO")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$125, DW_AT_name("csLSS")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$138	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$138, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$126, DW_AT_name("errCode")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_name("errRegMask")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$128, DW_AT_name("active")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$172	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$172, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$172, DW_AT_language(DW_LANG_C)

$C$DW$T$173	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$173, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$T$173, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$173, DW_AT_byte_size(0x18)
$C$DW$129	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$129, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$173


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$130, DW_AT_name("index")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$131, DW_AT_name("subindex")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$132, DW_AT_name("size")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$133, DW_AT_name("address")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$178	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$178, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$178, DW_AT_language(DW_LANG_C)
$C$DW$T$179	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$179, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$T$179, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x06)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_name("ucDuration4mS")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ucDuration4mS")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$135, DW_AT_name("ucReportID")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_ucReportID")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$136, DW_AT_name("usTimeTillNextmS")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_usTimeTillNextmS")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$137, DW_AT_name("ulTimeSinceReportmS")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_ulTimeSinceReportmS")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("tHIDReportIdle")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)
$C$DW$T$90	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_address_class(0x16)

$C$DW$T$191	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$191, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$191, DW_AT_byte_size(0x06)
$C$DW$138	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$138, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$191


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x02)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$139, DW_AT_name("LSB")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_LSB")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$140, DW_AT_name("MSB")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_MSB")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("tShort")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x03)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$141, DW_AT_name("bDescriptorType")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_bDescriptorType")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$142, DW_AT_name("wDescriptorLength")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_wDescriptorLength")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27

$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("tHIDClassDescriptorInfo")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$29	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x03)
$C$DW$143	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$143, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x09)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$144, DW_AT_name("bLength")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_bLength")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$145, DW_AT_name("bDescriptorType")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_bDescriptorType")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$146, DW_AT_name("bcdHID")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_bcdHID")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$147, DW_AT_name("bCountryCode")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_bCountryCode")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$148, DW_AT_name("bNumDescriptors")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_bNumDescriptors")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$149, DW_AT_name("sClassDescriptor")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_sClassDescriptor")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30

$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("tHIDDescriptor")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$150	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$94)
$C$DW$T$95	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$150)
$C$DW$T$96	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$96, DW_AT_address_class(0x16)

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x08)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$151, DW_AT_name("bmRequestType")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_bmRequestType")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$152, DW_AT_name("bRequest")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_bRequest")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$153, DW_AT_name("wValue")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_wValue")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$154, DW_AT_name("wIndex")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$155, DW_AT_name("wLength")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_wLength")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("tUSBRequest")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_address_class(0x16)

$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x18)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$156, DW_AT_name("pfnGetDescriptor")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_pfnGetDescriptor")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$157, DW_AT_name("pfnRequestHandler")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_pfnRequestHandler")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$158, DW_AT_name("pfnInterfaceChange")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_pfnInterfaceChange")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$159, DW_AT_name("pfnConfigChange")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_pfnConfigChange")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$160, DW_AT_name("pfnDataReceived")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_pfnDataReceived")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$161, DW_AT_name("pfnDataSent")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_pfnDataSent")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$162, DW_AT_name("pfnResetHandler")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_pfnResetHandler")
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$163, DW_AT_name("pfnSuspendHandler")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_pfnSuspendHandler")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$164, DW_AT_name("pfnResumeHandler")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_pfnResumeHandler")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$165, DW_AT_name("pfnDisconnectHandler")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_pfnDisconnectHandler")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$166, DW_AT_name("pfnEndpointHandler")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_pfnEndpointHandler")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$167, DW_AT_name("pfnDeviceHandler")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_pfnDeviceHandler")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$50

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("tCustomHandlers")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)

$C$DW$T$53	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x04)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$168, DW_AT_name("ucSize")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_ucSize")
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$169, DW_AT_name("pucData")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_pucData")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53

$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("tConfigSection")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$170	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$54)
$C$DW$T$55	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$170)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$171	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$56)
$C$DW$T$57	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$171)
$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)

$C$DW$T$192	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$192, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$192, DW_AT_language(DW_LANG_C)
$C$DW$172	.dwtag  DW_TAG_subrange_type
	.dwendtag $C$DW$T$192


$C$DW$T$59	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x04)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$173, DW_AT_name("ucNumSections")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_ucNumSections")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$174, DW_AT_name("psSections")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_psSections")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("tConfigHeader")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)
$C$DW$175	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$66)
$C$DW$T$67	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$175)
$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$176	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$68)
$C$DW$T$69	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$176)
$C$DW$T$70	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_address_class(0x16)

$C$DW$T$61	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x03)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$177, DW_AT_name("cMultiplier")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_cMultiplier")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$178, DW_AT_name("bDoubleBuffer")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_bDoubleBuffer")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$179, DW_AT_name("usEPFlags")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_usEPFlags")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61

$C$DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("tFIFOEntry")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)

$C$DW$T$63	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x2d)
$C$DW$180	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$180, DW_AT_upper_bound(0x0e)
	.dwendtag $C$DW$T$63


$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x5a)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$181, DW_AT_name("sIn")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_sIn")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$182, DW_AT_name("sOut")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_sOut")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$64

$C$DW$T$73	.dwtag  DW_TAG_typedef, DW_AT_name("tFIFOConfig")
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)
$C$DW$183	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$73)
$C$DW$T$74	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$183)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)

$C$DW$T$76	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x24)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$184, DW_AT_name("sCallbacks")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_sCallbacks")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$185, DW_AT_name("pDeviceDescriptor")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_pDeviceDescriptor")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$186, DW_AT_name("ppConfigDescriptors")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_ppConfigDescriptors")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$187, DW_AT_name("ppStringDescriptors")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_ppStringDescriptors")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$188, DW_AT_name("ulNumStringDescriptors")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_ulNumStringDescriptors")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$189, DW_AT_name("psFIFOConfig")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_psFIFOConfig")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$190, DW_AT_name("pvInstance")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_pvInstance")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$76

$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("tDeviceInfo")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)

$C$DW$T$77	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$77, DW_AT_byte_size(0x09)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$191, DW_AT_name("bLength")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_bLength")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$192, DW_AT_name("bDescriptorType")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_bDescriptorType")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$193, DW_AT_name("wTotalLength")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_wTotalLength")
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$194, DW_AT_name("bNumInterfaces")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_bNumInterfaces")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$195, DW_AT_name("bConfigurationValue")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_bConfigurationValue")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$196, DW_AT_name("iConfiguration")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_iConfiguration")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$197, DW_AT_name("bmAttributes")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_bmAttributes")
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$198, DW_AT_name("bMaxPower")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_bMaxPower")
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$77

$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("tConfigDescriptor")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x16)

$C$DW$T$88	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x16)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$199, DW_AT_name("ulUSBBase")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_ulUSBBase")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$200, DW_AT_name("psDevInfo")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_psDevInfo")
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$201, DW_AT_name("psConfDescriptor")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_psConfDescriptor")
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$202, DW_AT_name("eHIDRxState")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_eHIDRxState")
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$203, DW_AT_name("eHIDTxState")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_eHIDTxState")
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$204, DW_AT_name("usDeferredOpFlags")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_usDeferredOpFlags")
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$205, DW_AT_name("usInReportSize")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_usInReportSize")
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$206, DW_AT_name("usInReportIndex")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_usInReportIndex")
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$207, DW_AT_name("usOutReportSize")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_usOutReportSize")
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$208, DW_AT_name("pucInReportData")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_pucInReportData")
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$209, DW_AT_name("pucOutReportData")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_pucOutReportData")
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$210, DW_AT_name("bConnected")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_bConnected")
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$211, DW_AT_name("bSendInProgress")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_bSendInProgress")
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$212, DW_AT_name("bGetRequestPending")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_bGetRequestPending")
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$213, DW_AT_name("ucINEndpoint")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_ucINEndpoint")
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$214, DW_AT_name("ucOUTEndpoint")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_ucOUTEndpoint")
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$215, DW_AT_name("ucInterface")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_ucInterface")
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$88

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("tHIDInstance")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)
$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)

$C$DW$T$99	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x1e)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$216, DW_AT_name("usVID")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_usVID")
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$217, DW_AT_name("usPID")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_usPID")
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$218, DW_AT_name("usMaxPowermA")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_usMaxPowermA")
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$219, DW_AT_name("ucPwrAttributes")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_ucPwrAttributes")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$220, DW_AT_name("ucSubclass")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_ucSubclass")
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$221, DW_AT_name("ucProtocol")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_ucProtocol")
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$222, DW_AT_name("ucNumInputReports")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_ucNumInputReports")
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$223, DW_AT_name("psReportIdle")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_psReportIdle")
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$224, DW_AT_name("pfnRxCallback")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_pfnRxCallback")
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$225, DW_AT_name("pvRxCBData")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_pvRxCBData")
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$226, DW_AT_name("pfnTxCallback")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_pfnTxCallback")
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$227, DW_AT_name("pvTxCBData")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_pvTxCBData")
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$228, DW_AT_name("bUseOutEndpoint")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_bUseOutEndpoint")
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$229, DW_AT_name("psHIDDescriptor")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_psHIDDescriptor")
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$230, DW_AT_name("ppClassDescriptors")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_ppClassDescriptors")
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$231, DW_AT_name("ppStringDescriptors")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_ppStringDescriptors")
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$232, DW_AT_name("ulNumStringDescriptors")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_ulNumStringDescriptors")
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$233, DW_AT_name("psPrivateHIDData")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_psPrivateHIDData")
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$99

$C$DW$T$194	.dwtag  DW_TAG_typedef, DW_AT_name("tUSBDHIDDevice")
	.dwattr $C$DW$T$194, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$194, DW_AT_language(DW_LANG_C)
$C$DW$T$195	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$195, DW_AT_type(*$C$DW$T$194)
	.dwattr $C$DW$T$195, DW_AT_address_class(0x16)
$C$DW$234	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$194)
$C$DW$T$196	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$196, DW_AT_type(*$C$DW$234)
$C$DW$T$197	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$197, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$197, DW_AT_address_class(0x16)

$C$DW$T$100	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$100, DW_AT_byte_size(0x12)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$235, DW_AT_name("bLength")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_bLength")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$236, DW_AT_name("bDescriptorType")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_bDescriptorType")
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$237, DW_AT_name("bcdUSB")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_bcdUSB")
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$238, DW_AT_name("bDeviceClass")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_bDeviceClass")
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$239, DW_AT_name("bDeviceSubClass")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_bDeviceSubClass")
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$240, DW_AT_name("bDeviceProtocol")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_bDeviceProtocol")
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$241, DW_AT_name("bMaxPacketSize0")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_bMaxPacketSize0")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$242, DW_AT_name("idVendor")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_idVendor")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$243, DW_AT_name("idProduct")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_idProduct")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$244, DW_AT_name("bcdDevice")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_bcdDevice")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$245, DW_AT_name("iManufacturer")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_iManufacturer")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$246, DW_AT_name("iProduct")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_iProduct")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$247, DW_AT_name("iSerialNumber")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_iSerialNumber")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$248, DW_AT_name("bNumConfigurations")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_bNumConfigurations")
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$100

$C$DW$T$198	.dwtag  DW_TAG_typedef, DW_AT_name("tDeviceDescriptor")
	.dwattr $C$DW$T$198, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$198, DW_AT_language(DW_LANG_C)
$C$DW$T$199	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$T$199, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x08)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$249, DW_AT_name("wListElem")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$250, DW_AT_name("wCount")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$251, DW_AT_name("fxn")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108

$C$DW$T$118	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$118, DW_AT_language(DW_LANG_C)
$C$DW$T$103	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$103, DW_AT_address_class(0x16)
$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x30)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$252, DW_AT_name("dataQue")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$253, DW_AT_name("freeQue")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$254, DW_AT_name("dataSem")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$255, DW_AT_name("freeSem")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$256, DW_AT_name("segid")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$257, DW_AT_name("size")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$258, DW_AT_name("length")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$259, DW_AT_name("name")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$T$200	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$200, DW_AT_language(DW_LANG_C)
$C$DW$T$202	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$202, DW_AT_address_class(0x16)
$C$DW$T$203	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$203, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$T$203, DW_AT_language(DW_LANG_C)

$C$DW$T$117	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$117, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$117, DW_AT_byte_size(0x04)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$260, DW_AT_name("next")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$261, DW_AT_name("prev")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$117

$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$116	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x16)

$C$DW$T$119	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$119, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$119, DW_AT_byte_size(0x10)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$262, DW_AT_name("job")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$263, DW_AT_name("count")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$264, DW_AT_name("pendQ")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$265, DW_AT_name("name")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$119

$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$205	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$205, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$205, DW_AT_address_class(0x16)
$C$DW$T$206	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$206, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$T$206, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$207	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$207, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$207, DW_AT_language(DW_LANG_C)

$C$DW$T$34	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$266	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$3)
$C$DW$267	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$33)
	.dwendtag $C$DW$T$34

$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x16)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("tStdRequest")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)

$C$DW$T$37	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$268	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$3)
$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$6)
$C$DW$270	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$37

$C$DW$T$38	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x16)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("tInterfaceCallback")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$271	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$3)
$C$DW$272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$T$40

$C$DW$T$41	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x16)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("tInfoCallback")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("tUSBEPIntHandler")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)

$C$DW$T$43	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$273	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$43

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("tUSBIntHandler")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$47	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$274	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$3)
$C$DW$275	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$13)
$C$DW$276	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$47

$C$DW$T$48	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$48, DW_AT_address_class(0x16)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("tUSBDeviceHandler")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)

$C$DW$T$105	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)
$C$DW$277	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$104)
	.dwendtag $C$DW$T$105

$C$DW$T$106	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$106, DW_AT_address_class(0x16)
$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)

$C$DW$T$141	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)
$C$DW$278	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$140)
	.dwendtag $C$DW$T$141

$C$DW$T$142	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$142, DW_AT_address_class(0x16)
$C$DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)
$C$DW$T$145	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$145, DW_AT_language(DW_LANG_C)
$C$DW$T$155	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$155, DW_AT_language(DW_LANG_C)
$C$DW$T$144	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$144, DW_AT_language(DW_LANG_C)
$C$DW$T$154	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$154, DW_AT_language(DW_LANG_C)
$C$DW$T$146	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$146, DW_AT_language(DW_LANG_C)

$C$DW$T$150	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)
$C$DW$279	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$140)
$C$DW$280	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$150

$C$DW$T$151	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$151, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$T$151, DW_AT_address_class(0x16)
$C$DW$T$183	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$183, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$T$183, DW_AT_language(DW_LANG_C)
$C$DW$T$152	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$152, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$T$152, DW_AT_language(DW_LANG_C)
$C$DW$T$156	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$T$156, DW_AT_language(DW_LANG_C)

$C$DW$T$174	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$174, DW_AT_language(DW_LANG_C)
$C$DW$281	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$140)
$C$DW$282	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$6)
$C$DW$283	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$9)
$C$DW$284	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$174

$C$DW$T$175	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$T$175, DW_AT_address_class(0x16)
$C$DW$T$176	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$T$176, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$157	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$285	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$285, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$286	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$6)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$286)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)
$C$DW$287	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$52)
$C$DW$T$71	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$287)
$C$DW$T$72	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_address_class(0x16)

$C$DW$T$214	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$214, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$214, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$214, DW_AT_byte_size(0x0c)
$C$DW$288	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$288, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$214


$C$DW$T$215	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$215, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$215, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$215, DW_AT_byte_size(0x02)
$C$DW$289	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$289, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$215


$C$DW$T$216	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$216, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$216, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$216, DW_AT_byte_size(0x04)
$C$DW$290	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$290, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$216


$C$DW$T$217	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$217, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$217, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$217, DW_AT_byte_size(0x10)
$C$DW$291	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$291, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$217


$C$DW$T$218	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$218, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$218, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$218, DW_AT_byte_size(0x0a)
$C$DW$292	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$292, DW_AT_upper_bound(0x09)
	.dwendtag $C$DW$T$218


$C$DW$T$219	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$219, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$219, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$219, DW_AT_byte_size(0x1c)
$C$DW$293	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$293, DW_AT_upper_bound(0x1b)
	.dwendtag $C$DW$T$219


$C$DW$T$220	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$220, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$220, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$220, DW_AT_byte_size(0x24)
$C$DW$294	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$294, DW_AT_upper_bound(0x23)
	.dwendtag $C$DW$T$220


$C$DW$T$221	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$221, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$221, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$221, DW_AT_byte_size(0x21)
$C$DW$295	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$295, DW_AT_upper_bound(0x20)
	.dwendtag $C$DW$T$221

$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)

$C$DW$T$225	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$225, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$225, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$225, DW_AT_byte_size(0x42)
$C$DW$296	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$296, DW_AT_upper_bound(0x41)
	.dwendtag $C$DW$T$225


$C$DW$T$227	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$227, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$227, DW_AT_language(DW_LANG_C)
$C$DW$297	.dwtag  DW_TAG_subrange_type
	.dwendtag $C$DW$T$227


$C$DW$T$228	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$228, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$228, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$228, DW_AT_byte_size(0x07)
$C$DW$298	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$298, DW_AT_upper_bound(0x06)
	.dwendtag $C$DW$T$228


$C$DW$T$230	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$230, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$230, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$230, DW_AT_byte_size(0x40)
$C$DW$299	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$299, DW_AT_upper_bound(0x3f)
	.dwendtag $C$DW$T$230

$C$DW$T$177	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$177, DW_AT_language(DW_LANG_C)
$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("tBoolean")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)
$C$DW$300	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$60)
$C$DW$T$87	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$300)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$148	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$148, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$231	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$231, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$231, DW_AT_language(DW_LANG_C)
$C$DW$301	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$9)
$C$DW$T$85	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$301)
$C$DW$302	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$9)
$C$DW$T$129	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$302)
$C$DW$T$130	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$T$130, DW_AT_address_class(0x16)
$C$DW$T$149	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$111, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$91	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
$C$DW$303	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$3)
$C$DW$304	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$13)
$C$DW$305	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$13)
$C$DW$306	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$91

$C$DW$T$92	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x16)
$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("tUSBCallback")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)

$C$DW$T$131	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$131, DW_AT_language(DW_LANG_C)
$C$DW$307	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$6)
$C$DW$308	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$131

$C$DW$T$132	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x16)
$C$DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
$C$DW$T$147	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$147, DW_AT_address_class(0x16)

$C$DW$T$158	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$158, DW_AT_language(DW_LANG_C)
$C$DW$309	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$140)
$C$DW$310	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$123)
$C$DW$311	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$6)
$C$DW$312	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$158

$C$DW$T$159	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$T$159, DW_AT_address_class(0x16)
$C$DW$T$160	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$T$160, DW_AT_language(DW_LANG_C)
$C$DW$313	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$160)
$C$DW$T$161	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$313)
$C$DW$T$162	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$T$162, DW_AT_address_class(0x16)
$C$DW$T$163	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$163, DW_AT_address_class(0x16)

$C$DW$T$167	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$167, DW_AT_language(DW_LANG_C)
$C$DW$314	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$140)
$C$DW$315	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$9)
$C$DW$316	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$167

$C$DW$T$168	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$T$168, DW_AT_address_class(0x16)
$C$DW$T$169	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$T$169, DW_AT_language(DW_LANG_C)
$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)
$C$DW$T$114	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)

$C$DW$T$170	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$170, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$170, DW_AT_byte_size(0x01)
$C$DW$317	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$318	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$170

$C$DW$T$171	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$171, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$T$171, DW_AT_language(DW_LANG_C)

$C$DW$T$136	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$136, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x01)
$C$DW$319	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$320	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$321	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$322	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$323	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$324	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$325	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$326	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$136

$C$DW$T$137	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)

$C$DW$T$153	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$153, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$153, DW_AT_byte_size(0x80)
$C$DW$327	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$327, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$153


$C$DW$T$120	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$120, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x06)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$328, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$329, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$330, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$331, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$332, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$333, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$120

$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
$C$DW$334	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$126)
$C$DW$T$127	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$334)
$C$DW$T$128	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$128, DW_AT_address_class(0x16)

$C$DW$T$180	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$180, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$180, DW_AT_byte_size(0x132)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$335, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$336, DW_AT_name("objdict")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$337, DW_AT_name("PDO_status")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$338, DW_AT_name("firstIndex")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$339, DW_AT_name("lastIndex")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$340, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$341, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$342, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$343, DW_AT_name("transfers")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$344, DW_AT_name("nodeState")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$345, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$346, DW_AT_name("initialisation")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$347, DW_AT_name("preOperational")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$348, DW_AT_name("operational")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$349, DW_AT_name("stopped")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$350, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$351, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$352, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$353, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$354, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$355, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$356, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$357, DW_AT_name("heartbeatError")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$358, DW_AT_name("NMTable")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$359, DW_AT_name("syncTimer")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$360, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$361, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$362, DW_AT_name("pre_sync")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$363, DW_AT_name("post_TPDO")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$364, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$365, DW_AT_name("toggle")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$366, DW_AT_name("canHandle")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$166)
	.dwattr $C$DW$367, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$368, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$369, DW_AT_name("globalCallback")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$370, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$371, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$372, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$373, DW_AT_name("dcf_request")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$374, DW_AT_name("error_state")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$375, DW_AT_name("error_history_size")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$376, DW_AT_name("error_number")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$377, DW_AT_name("error_first_element")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$378, DW_AT_name("error_register")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$379, DW_AT_name("error_cobid")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$380, DW_AT_name("error_data")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$381, DW_AT_name("post_emcy")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$382, DW_AT_name("lss_transfer")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$179)
	.dwattr $C$DW$383, DW_AT_name("eeprom_index")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$384, DW_AT_name("eeprom_size")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$180

$C$DW$T$139	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
$C$DW$T$140	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$T$140, DW_AT_address_class(0x16)

$C$DW$T$182	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$182, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$182, DW_AT_byte_size(0x0e)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$385, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$386, DW_AT_name("event_timer")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$387, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$388, DW_AT_name("last_message")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$182

$C$DW$T$124	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$T$124, DW_AT_language(DW_LANG_C)
$C$DW$T$125	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x16)

$C$DW$T$184	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$184, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$184, DW_AT_byte_size(0x14)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$389, DW_AT_name("nodeId")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$390, DW_AT_name("whoami")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$391, DW_AT_name("state")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$392, DW_AT_name("toggle")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$393, DW_AT_name("abortCode")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$394, DW_AT_name("index")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$395, DW_AT_name("subIndex")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$396, DW_AT_name("port")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$397, DW_AT_name("count")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$398, DW_AT_name("offset")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$399, DW_AT_name("datap")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$400, DW_AT_name("dataType")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$401, DW_AT_name("timer")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$402, DW_AT_name("Callback")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$184

$C$DW$T$134	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)

$C$DW$T$135	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$T$135, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$135, DW_AT_byte_size(0x3c)
$C$DW$403	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$403, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$135


$C$DW$T$188	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$188, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$188, DW_AT_byte_size(0x04)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$404, DW_AT_name("pSubindex")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$405, DW_AT_name("bSubCount")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$406, DW_AT_name("index")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$188

$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)
$C$DW$407	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$121)
$C$DW$T$122	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$407)
$C$DW$T$123	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_address_class(0x16)

$C$DW$T$164	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$164, DW_AT_language(DW_LANG_C)
$C$DW$408	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$9)
$C$DW$409	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$147)
$C$DW$410	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$163)
	.dwendtag $C$DW$T$164

$C$DW$T$165	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$165, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$T$165, DW_AT_address_class(0x16)
$C$DW$T$166	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$166, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$T$166, DW_AT_language(DW_LANG_C)

$C$DW$T$189	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$189, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$189, DW_AT_byte_size(0x08)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$411, DW_AT_name("bAccessType")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$412, DW_AT_name("bDataType")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$413, DW_AT_name("size")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$414, DW_AT_name("pObject")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$415, DW_AT_name("bProcessor")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$189

$C$DW$416	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$189)
$C$DW$T$185	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$185, DW_AT_type(*$C$DW$416)
$C$DW$T$186	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$186, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$T$186, DW_AT_language(DW_LANG_C)
$C$DW$T$187	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$T$187, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$417	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$417, DW_AT_location[DW_OP_reg0]
$C$DW$418	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$418, DW_AT_location[DW_OP_reg1]
$C$DW$419	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$419, DW_AT_location[DW_OP_reg2]
$C$DW$420	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$420, DW_AT_location[DW_OP_reg3]
$C$DW$421	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg20]
$C$DW$422	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg21]
$C$DW$423	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg22]
$C$DW$424	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$424, DW_AT_location[DW_OP_reg23]
$C$DW$425	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$425, DW_AT_location[DW_OP_reg24]
$C$DW$426	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$426, DW_AT_location[DW_OP_reg25]
$C$DW$427	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$427, DW_AT_location[DW_OP_reg26]
$C$DW$428	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$428, DW_AT_location[DW_OP_reg28]
$C$DW$429	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$429, DW_AT_location[DW_OP_reg29]
$C$DW$430	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$430, DW_AT_location[DW_OP_reg30]
$C$DW$431	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$431, DW_AT_location[DW_OP_reg31]
$C$DW$432	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$432, DW_AT_location[DW_OP_regx 0x20]
$C$DW$433	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$433, DW_AT_location[DW_OP_regx 0x21]
$C$DW$434	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$434, DW_AT_location[DW_OP_regx 0x22]
$C$DW$435	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$435, DW_AT_location[DW_OP_regx 0x23]
$C$DW$436	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$436, DW_AT_location[DW_OP_regx 0x24]
$C$DW$437	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$437, DW_AT_location[DW_OP_regx 0x25]
$C$DW$438	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$438, DW_AT_location[DW_OP_regx 0x26]
$C$DW$439	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$439, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$440	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$440, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$441	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg4]
$C$DW$442	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg6]
$C$DW$443	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg8]
$C$DW$444	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$444, DW_AT_location[DW_OP_reg10]
$C$DW$445	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$445, DW_AT_location[DW_OP_reg12]
$C$DW$446	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$446, DW_AT_location[DW_OP_reg14]
$C$DW$447	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$447, DW_AT_location[DW_OP_reg16]
$C$DW$448	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$448, DW_AT_location[DW_OP_reg17]
$C$DW$449	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$449, DW_AT_location[DW_OP_reg18]
$C$DW$450	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$450, DW_AT_location[DW_OP_reg19]
$C$DW$451	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$451, DW_AT_location[DW_OP_reg5]
$C$DW$452	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$452, DW_AT_location[DW_OP_reg7]
$C$DW$453	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$453, DW_AT_location[DW_OP_reg9]
$C$DW$454	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$454, DW_AT_location[DW_OP_reg11]
$C$DW$455	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$455, DW_AT_location[DW_OP_reg13]
$C$DW$456	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$456, DW_AT_location[DW_OP_reg15]
$C$DW$457	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$457, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$458	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$458, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$459	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$459, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$460	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$460, DW_AT_location[DW_OP_regx 0x30]
$C$DW$461	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$461, DW_AT_location[DW_OP_regx 0x33]
$C$DW$462	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$462, DW_AT_location[DW_OP_regx 0x34]
$C$DW$463	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$463, DW_AT_location[DW_OP_regx 0x37]
$C$DW$464	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$464, DW_AT_location[DW_OP_regx 0x38]
$C$DW$465	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$465, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$466	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$466, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$467	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$467, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$468	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$468, DW_AT_location[DW_OP_regx 0x40]
$C$DW$469	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$469, DW_AT_location[DW_OP_regx 0x43]
$C$DW$470	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$470, DW_AT_location[DW_OP_regx 0x44]
$C$DW$471	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$471, DW_AT_location[DW_OP_regx 0x47]
$C$DW$472	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$472, DW_AT_location[DW_OP_regx 0x48]
$C$DW$473	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$473, DW_AT_location[DW_OP_regx 0x49]
$C$DW$474	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$474, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$475	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$475, DW_AT_location[DW_OP_regx 0x27]
$C$DW$476	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$476, DW_AT_location[DW_OP_regx 0x28]
$C$DW$477	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$477, DW_AT_location[DW_OP_reg27]
$C$DW$478	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$478, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

