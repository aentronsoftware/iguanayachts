//###########################################################################
//
// FILE:   hw_memmap.h
//
// TITLE:  f2806x Memory map definitions for USB examples.
//
//###########################################################################
// $TI Release: F2806x C/C++ Header Files and Peripheral Examples V136 $
// $Release Date: Apr 15, 2013 $
//###########################################################################

#ifndef __HW_MEMMAP_H__
#define __HW_MEMMAP_H__

//*****************************************************************************
//
// The following are defines for the base address of the memories and
// peripherals.
//
//*****************************************************************************

#define USB0_BASE               0x00004000  // USB 0 Controller
#define UART0_BASE              0x00007050  // SCI-A
#define UART1_BASE              0x00007750  // SCI-B




#endif // __HW_MEMMAP_H__


