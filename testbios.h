/**********************************************************************

   testbios.h - external defines for BIOS support

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: external defines for BIOS support
   Edit  :

 *********************************************************************/


#ifndef _testbios
#define _testbios

void MemCopy(uint16 *SourceAddr, uint16* SourceEndAddr, uint16* DestAddr);

extern uint16 hwi_vec_runstart;
extern uint16 hwi_vec_loadstart;
extern uint16 hwi_vec_loadend;

extern uint16 codeA_loadstart;
extern uint16 codeA_loadend;
extern uint16 codeA_runstart;

extern uint16 ramf_loadstart;
extern uint16 ramf_loadend;
extern uint16 ramf_runstart;

extern uint16 bios_loadstart;
extern uint16 bios_loadend;
extern uint16 bios_runstart;

extern uint16 sysinit_loadstart;
extern uint16 sysinit_loadend;
extern uint16 sysinit_runstart;

extern Uint32 hwi_vec_loadsize;

extern uint16 RamfuncsLoadStart;
extern uint16 RamfuncsLoadEnd;
extern uint16 RamfuncsRunStart;

extern uint16 Cla1funcsLoadStart;
extern uint16 Cla1funcsLoadEnd;
extern uint16 Cla1funcsRunStart;

extern uint16 Cla1mathLoadStart;
extern uint16 Cla1mathLoadEnd;
extern uint16 Cla1mathRunStart;

#endif


