/**********************************************************************

   convert.c - number and unit conversion

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: number and unit conversion
   Edit  :

 *********************************************************************/


#include <math.h>
#include "uc.h"
#include "dspspecs.h"
#include "powerman.h"
#include "gateway_dict.h"
#include "error.h"
#include "convert.h"


volatile float CNV_CurrentUnit;
volatile int   CNV_CurrentRange;
float CNV_CoupleUnit;
float CNV_DegUnit = 360.0/4;
int   CNV_ADPRange;
int   CNV_ADMRange;
int   CNV_ISafe5s;
int   CNV_ISafe0s2;

#pragma CODE_SECTION(CNV_Round,"ramfuncs")
long CNV_Round(float arg)
/* rounds the argument to the nearest integer value */
{
  if (arg < 0.0)
  {
    return (-(long)(-arg+0.5));
  }
  else
  {
    return ((long)(arg+0.5));
  }
}

uint16 CNV_SwapWord(uint16 w)
{
  return ((w<<8)+(w>>8));
}

uint32 CNV_SwapLWord(uint32 lw)
{
  T4uint8 swapdata;

  swapdata[0] = (*(T4uint8*)&lw)[3];
  swapdata[1] = (*(T4uint8*)&lw)[2];
  swapdata[2] = (*(T4uint8*)&lw)[1];
  swapdata[3] = (*(T4uint8*)&lw)[0];
  return (*(uint32*)&swapdata);
}


/* END OF CONVERT.C */
