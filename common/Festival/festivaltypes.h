/*
This file is part of CanFestival, a library implementing CanOpen Stack. 

Copyright (C): Edouard TISSERANT and Francis DUPIN

See COPYING file for copyrights details.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _FESTIVALTYPES_H_
#define _FESTIVALTYPES_H_

//#include <string.h>
//#include <stdio.h>

// FLASH or RAM are defined in the CCS project build options
#ifdef BIOS_PRJ
  //#include "uposbioscfg.h"
//   #include "festivalcfg.h"
#endif

#ifdef RAM_PRJ
 //   #include "amc_ramcfg.h"
#endif



//#define DEBUG_ERR_CONSOLE_ON
//#define DEBUG_WAR_CONSOLE_ON

/*  Define the architecture : little_endian or big_endian
 -----------------------------------------------------
 Test :
 UNS32 v = 0x1234ABCD;
 char *data = &v;

 Result for a little_endian architecture :
 data[0] = 0xCD;
 data[1] = 0xAB;
 data[2] = 0x34;
 data[3] = 0x12;

 Result for a big_endian architecture :
 data[0] = 0x12;
 data[1] = 0x34;
 data[2] = 0xAB;
 data[3] = 0xCD;
 */

/* Integers */
#define INTEGER8  signed char
#define INTEGER12 short
#define INTEGER16 short
#define INTEGER24 long
#define INTEGER32 long
#define INTEGER40 long long
#define INTEGER48 long long
#define INTEGER56 long long
#define INTEGER64 long long

/* Unsigned integers */
#define UNS8   unsigned char
#define UNS12  unsigned short
#define UNS16  unsigned short
#define UNS32  unsigned long
#define UNS24  unsigned long
#define UNS40  unsigned long long
#define UNS48  unsigned long long
#define UNS56  unsigned long long
#define UNS64  unsigned long long

/* Reals */
#define REAL32	float
#define REAL64 double

/* aliases */
typedef UNS8 CAN_HANDLE;
typedef UNS8 CAN_PORT;

/* Definition of error and warning macros */
/* -------------------------------------- */
#if defined DEBUG_ERR_CONSOLE_ON || defined DEBUG_WAR_CONSOLE_ON
#include <stdio.h>
//#include "errors.h"
#endif

#define NULL 0

/* Definition of MSG_ERR */
/* --------------------- */
//          LOG_printf(&trace, "%s 0X%x",str, val);
//      _LOGmessage(num, "CanF Err 0x%X " str "0x%X", num, val);

#ifdef DEBUG_ERR_CONSOLE_ON
#    define MSG_ERR(num, str, val)            \
  Log_info0(str)
#else
#define MSG_ERR(num, str, val)
#endif

/* Definition of MSG_WAR */
/* --------------------- */
//#ifdef DEBUG_WAR_CONSOLE_ON
//#    define MSG_WAR(num, str, val)          \
//           LOG_printf(&trace, "%s 0X%x",str, val);
//		  _LOGmessage(num, "CanF war %x " str, num, val);
//#else
#define MSG_WAR(num, str, val)
//#endif

#endif //_FESTIVALTYPE_H_
