#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/clock.h>
#include <xdc/cfg/global.h> //for sys/bios variables

#include <co_data.h>
#include <timer.h>  //SetAlarm
#include "uc.h"

unsigned long TimeBuffer = 0;
unsigned long WaitTime = 0;
#define US_PER_TICK 1000U


void EnterMutex(void)
{
  Semaphore_pend(CanFestival_mutex, BIOS_WAIT_FOREVER);
}

void LeaveMutex(void)
{
  Semaphore_post(CanFestival_mutex);
}


#define maxval(a,b) ((a>b)?a:b)
void setTimer(TIMEVAL value)
{
  Semaphore_post(semaphoreCanTimer);			// continue taskFnCanTimer
}

void setTimer2(TIMEVAL value)
{
  WaitTime = maxval(value,1000)/US_PER_TICK;    // convert from microseconds to system clock ticks
  TimeBuffer = Clock_getTicks();      // time in ticks
}

TIMEVAL getElapsedTime(void)
{
  unsigned long tmp;

  tmp = (Clock_getTicks() - TimeBuffer);
  return tmp * US_PER_TICK;			/* 1 ticks/ms this convert time in ticks to us */
}

void taskFnCanTimer(UArg a0, UArg a1)
{
  TimeBuffer = Clock_getTicks();      // time in cpu ticks
  while(1)
  {
    Semaphore_pend(semaphoreCanTimer, WaitTime);
    EnterMutex();
    TimeDispatch();
    LeaveMutex();
  }
}

