// DS 402 motor handling according DS 402 V1.1 "Device Profile for Motor Modules"

#ifndef __DS_402_h__
#define __DS_402_h__


typedef union {
      UNS16 ControlWord;
      struct  {
        UNS16 SwitchOn        :1;     //Switch on             bit0
        UNS16 EnableVolt      :1;     //Enable voltage        bit1
        UNS16 QuickStop       :1;     //Quick Stop            bit2
        UNS16 EnableOperation :1;     //Enable Operation      bit3
        UNS16 OpModeSpecific  :3;
        UNS16 ResetFault      :1;     //bit 7
        UNS16 Halt            :1;     //bit 8
        UNS16 Oms             :1;
        UNS16 Rsvd            :1;
        UNS16 Manufacturer    :5;
      } AnyMode;
      struct  {
        UNS16 SwitchOn        :1;     //Switch on             bit0
        UNS16 EnableVolt      :1;     //Enable voltage        bit1
        UNS16 QuickStop       :1;     //Quick Stop            bit2
        UNS16 EnableOperation :1;     //Enable Operation      bit3
        UNS16 Rsvd0           :3;
        UNS16 ResetFault      :1;     
        UNS16 Halt            :1;
        UNS16 Oms             :1;
        UNS16 Rsvd            :1;
        UNS16 Manufacturer    :5;
      } VelocityMode;
      struct  {
        UNS16 SwitchOn        :1;     //Switch on             bit0
        UNS16 EnableVolt      :1;     //Enable voltage        bit1
        UNS16 QuickStop       :1;     //Quick Stop            bit2
        UNS16 EnableOperation :1;     //Enable Operation      bit3
        UNS16 NewSetPoint     :1;     
        UNS16 ChangeSetImm    :1;     
        UNS16 Abs_Rel         :1;     
        UNS16 ResetFault      :1;      
        UNS16 Halt            :1;     
        UNS16 ChangeSetPos    :1;     
        UNS16 Rsvd            :1;
        UNS16 Manufacturer    :5;
      } PositionMode;
}TControlword;


typedef union  {
    UNS16 StatusWord;
    struct  {
      UNS16 ReadyToSwitchOn   :1;     //bit 0
      UNS16 SwitchedOn        :1;     //
      UNS16 OperationEnabled  :1;     //
      UNS16 Fault             :1;     //
      UNS16 VoltageDisabled   :1;     //
      UNS16 NotQuickStop      :1;     //
      UNS16 SwitchOnDisabled  :1;     //
      UNS16 Warning           :1;     //
      UNS16 Manufacturer      :1;     //
      UNS16 Remote            :1;     //bit9
      UNS16 TargetReached     :1;     //
      UNS16 InternalLimit     :1;     //
      UNS16 OpModeSpecific    :2;     //
      UNS16 Manufacturers     :2;     //
    } AnyMode;
    struct  {
      UNS16 ReadyToSwitchOn   :1;     //bit 0
      UNS16 SwitchedOn        :1;     //
      UNS16 OperationEnabled  :1;     //
      UNS16 Fault             :1;     //
      UNS16 VoltageDisabled   :1;     //
      UNS16 NotQuickStop      :1;     //
      UNS16 SwitchOnDisabled  :1;     //
      UNS16 Warning           :1;     //
      UNS16 Manufacturer      :1;     //
      UNS16 Remote            :1;     //bit9
      UNS16 TargetReached     :1;     //
      UNS16 InternalLimit     :1;     //
      UNS16 Speed             :1;
      UNS16 MaxSlipError      :1;
      UNS16 Manufacturers     :2;     //
    } VelocityMode;
    struct  {
      UNS16 ReadyToSwitchOn   :1;     //bit 0
      UNS16 SwitchedOn        :1;     //
      UNS16 OperationEnabled  :1;     //
      UNS16 Fault             :1;     //
      UNS16 VoltageDisabled   :1;     //
      UNS16 NotQuickStop      :1;     //
      UNS16 SwitchOnDisabled  :1;     //
      UNS16 Warning           :1;     //
      UNS16 Manufacturer      :1;     //
      UNS16 Remote            :1;     //bit9
      UNS16 TargetReached     :1;     //
      UNS16 InternalLimit     :1;     //
      UNS16 SetPointAck       :1;     //bit 12
      UNS16 FollowError       :1;     //bit 13
      UNS16 Manufacturers     :2;     //
    } PositionMode;
    struct  {
      UNS16 ReadyToSwitchOn   :1;     //bit 0
      UNS16 SwitchedOn        :1;     //
      UNS16 OperationEnabled  :1;     //
      UNS16 Fault             :1;     //
      UNS16 VoltageDisabled   :1;     //
      UNS16 NotQuickStop      :1;     //
      UNS16 SwitchOnDisabled  :1;     //
      UNS16 Warning           :1;     //
      UNS16 Manufacturer      :1;     //
      UNS16 Remote            :1;     //bit9
      UNS16 TargetReached     :1;     //
      UNS16 InternalLimit     :1;     //
      UNS16 HomingAttained    :1;     //
      UNS16 HomingError       :1;     //
      UNS16 Manufacturers     :2;     //
    } HomingMode;
} TStatusword;

typedef union  {
      UNS16 SpecialWord;
    struct  {
      UNS16 Pos_PI_Done       :1;     //bit0
      UNS16 Pos_PI_Trigger    :1;
      UNS16 Pos_Record        :1;
      UNS16 Cur_PI_Done       :1;     //bit3
      UNS16 Cur_PI_Trigger    :1;     //bit4
      UNS16 Cur_Record        :1;
      UNS16 Cur_Pulse         :1;
      UNS16 Duty_Test         :1;     //bit7
      UNS16 Rotor_Direction   :1;
      UNS16 StopAftDecel      :1;
      UNS16 Trig_Record       :1;
      UNS16 AutoRecord        :1;     //bit11
      UNS16 ContinuousRecord  :1;     //bit12
      UNS16 AutoTrigg         :1;
      UNS16 Referenced        :1;
      UNS16 CamIndexEvent     :1;
    } SpControl;
} TSpecialcontrol;

typedef struct  {
      UNS32 tototo:16;
      UNS32 tatata:16;
} TSupported_drive_modes;

typedef struct {
  UNS8 SetCurGain:1;
  UNS8 SetPosGain:1;
  UNS8 SetVelGain:1;
  UNS8 NotUsed:5;
}TSetGains;

typedef struct  {
      UNS16 EnableUpLimit:1;
      UNS16 EnableLoLimit:1;
      UNS16 Unused:14;
} TEnablePositionLimits;

typedef struct  {
      UNS16 tototo:8;
      UNS16 tatata:8;
} TPolarity;

typedef struct  {
      UNS16 tototo:8;
      UNS16 tatata:8;
} TPositioning_option_code;





#define Not_Ready_To_Switch_On        0
#define Switch_On_Disabled            1
#define Ready_To_Switch_On            2
#define Switched_On                   3
#define Operation_Enabled             4
#define Quick_Stop_Active             5
#define Fault_State                   6

#define Profile_Position_Mode         1
#define Velocity_Mode                 2
#define Profile_Velocity_Mode         3
#define Profile_Torque_Mode           4
//reserved                            5
#define Profile_Homing_Mode           6


#endif //__DS_402_h__
