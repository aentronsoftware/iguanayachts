/*
This file is part of CanFestival, a library implementing CanOpen Stack.

Copyright (C): Edouard TISSERANT and Francis DUPIN

See COPYING file for copyrights details.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @defgroup can CAN Management
 * @ingroup userapi
 */

//PT: This header defines macros and prototype for the low-level CAN (no CANopen notions here)

#ifndef __can_h__
#define __can_h__

#define ERROR_ACTIVE  1
#define WARNING_LEVEL 2
#define ERROR_PASSIVE 3
#define BUS_OFF       4

#define CAN_PORT_INTERNAL 0
#define CAN_PORT_EXTERNAL 1
#define CAN_PORT_USB      2

#define NODE_ID_BOOT_PICOLLO  99 //Johnny 100 test

//+4 to differentiate SDO sent by stellaris from SDO sent by PC
//#define NODE_ID_PICOLLO   (1+4)
#define NODE_ID_STELLARIS     2

#define CAN_BAUDRATE     1000 //kbits/s
#define CAN_RX_MAILBOXES   16
#define CAN_TX_MAILBOXES   16
#define CAN_MSG_TIME_OUT 1000


/**
 * @brief The CAN message structure
 * @ingroup can
 * @{
 */
typedef struct {
  UNS16 cob_id;	/* l'ID du mesg */
  UNS8 rtr;			/**< remote transmission request. 0 if not rtr,
                   1 for a rtr message */
  UNS8 len;			/**< message length (0 to 8) */
  UNS8 data[8]; /**< data */
} Message;
/** @} */

//cob_id bitmasks
#define bFUNCTION_CODE 0x780   //4 bits function code
#define bNODE_ID      0x07F   //7 bits node id

#define Message_Initializer {0,0,0,{0,0,0,0,0,0,0,0}}

//============================= basic CAN function prototypes ==========================


//UNS8 canSend(CAN_PORT port, Message *m); defined as macro, see below
#include "processor.h"
//PT 23.07.12 declared canSend as macros so there are no problems with reentrancy
//if called from tasks with different priority levels (no mutex required)
//the bios function Mailbox_post is atomic, i.e. will not be interrupted
#ifdef PROC_COM
  #include <ti/sysbios/knl/Mailbox.h>
  #include <xdc/cfg/global.h> //for sys/bios variables
  #define canSend(port, message) ((port==CAN_PORT_INTERNAL) ? !Mailbox_post(mailboxCan0Tx, message, 0) : !Mailbox_post(mailboxCan1Tx, message, 0))
#endif

#ifdef PROC_DSP
  #include <tistdtypes.h>
  #include "gatewaycfg.h"
  #define canSend(port, message) ((port == CAN_PORT_INTERNAL) ? !MBX_post(&can_tx_mbox, message, 0) : !MBX_post(&usb_tx_mbox, message, 0))
#endif

void canInit(UNS16 baudrate, UNS8 nodeID);
interrupt void hwiFnCan0(void);
interrupt void manage_can_bus(void);

#endif /* __can_h__ */
