/*
This file is part of CanFestival, a library implementing CanOpen Stack.

Copyright (C): Edouard TISSERANT and Francis DUPIN

See COPYING file for copyrights details.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __objdictdef_h__
#define __objdictdef_h__

#define DIC_READ  0    //read access to dictionary
#define DIC_WRITE 1    //write access to dictionary

/************************ STRUCTURES ****************************/
/** This are some structs which are neccessary for creating the entries
 *  of the object dictionary.
 */
typedef const struct td_subindex
{
    UNS8                    bAccessType;
    UNS8                    bDataType; /* Defines of what datatype the entry is */
    UNS32                   size;      /* The size (in Byte) of the variable */
    void*                   pObject;   /* This is the pointer of the Variable */
    UNS8                    bProcessor; /* Define the processor who use the variable*/
} subindex;

/** Struct for creating entries in the communictaion profile
 */
typedef struct td_indextable
{
    subindex*   pSubindex;   /* Pointer to the subindex */
    UNS8   bSubCount;   /* the count of valid entries for this subindex
                         * This count here defines how many memory has been
                         * allocated. this memory does not have to be used.
                         */
    UNS16   index;
} indextable;

typedef struct s_quick_index{
	UNS16 SDO_SVR;
	UNS16 SDO_CLT;
	UNS16 PDO_RCV;
	UNS16 PDO_RCV_MAP;
	UNS16 PDO_TRS;
	UNS16 PDO_TRS_MAP;
}quick_index;

typedef struct {
  UNS16 index;    //Object dictionary index
  UNS16 subindex; //Object dictionary subindex
  UNS16 size;     //data byte size
  UNS16 address;  //eeprom address
}T_EepromIndexes;


/*typedef struct struct_CO_Data CO_Data; */
/* PT 09.07.12 add argument 'access' to distinguish between DIC_READ or DIC_WRITE access */
typedef UNS32 (*ODCallback_t)(CO_Data* d, const indextable *, UNS8 bSubindex, UNS8 access);
typedef const indextable * (*scanIndexOD_t)(UNS16 wIndex, UNS32 * errorCode, const ODCallback_t **Callback);

/************************** MACROS *********************************/

/* CANopen usefull helpers */
#define GET_NODE_ID(m)         (UNS16_LE(m.cob_id) & 0x7f)
#define GET_FUNCTION_CODE(m)   (UNS16_LE(m.cob_id) >> 7)

#endif /* __objdictdef_h__ */
