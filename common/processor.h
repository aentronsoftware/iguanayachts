/**********************************************************************

   processor.h - processor selection file

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: define the selected processor
   Edit  :

 *********************************************************************/


#ifndef PROCESSOR_H_
#define PROCESSOR_H_
#ifdef PART_LM3S9B92
  #define PROC_COM
#endif
#ifdef PART_TMS320F28069
  #define PROC_DSP
#endif



#endif /* PROCESSOR_H_ */
