/*
 * powerman.h
 *
 *  Created on: 12 juin 2012
 *      Author: remip
 */

#ifndef POWERMAN_H_
#define POWERMAN_H_


#define bPWR_NOT_EN_12V  0x0001
#define bPWR_BUZZER_ON   0x0002
#define bPWR_LED_STANDBY 0x0004
#define bPWR_LED_ON      0x0008
#define bPWR_NOT_EN_3_3V 0x0010
#define bPWR_VMOT_SHUNT  0x0020
#define bPWR_NOT_EN_LCD  0x0040
#define bPWR_NOT_EN_5V   0x0080
#define LED_OFF_PWR_ON   0xFE00


#define POWER_CONFIG_ON      (uint16)LED_OFF_PWR_ON  // All ON, leds OFF
#define POWER_CONFIG_STANDBY (uint16)(bPWR_NOT_EN_12V + bPWR_LED_STANDBY + bPWR_NOT_EN_LCD + LED_OFF_PWR_ON)
#define POWER_CONFIG_DEEP    (uint16)(POWER_CONFIG_STANDBY + bPWR_NOT_EN_5V +bPWR_NOT_EN_3_3V + LED_OFF_PWR_ON)

//Vmot timeout
#define PWR_VMOT_TIMEOUT 1200
//SleepMode
#define PWR_STATE_ON   0
#define PWR_STATE_OFF  1
#define PWR_STATE_DEEP 2
#define PWR_STATE_BOOT 3

#define PWR_VMOT_ON  0
#define PWR_VMOT_OFF 1

typedef union{
  uint16 ConfigWord;
  struct  {
    uint16 Reg1_fsel       :1;     //bit0
    uint16 Reg1_sw_en      :1;     //bit1
    uint16 NotRstOvercur1  :1;     //bit2
    uint16 Reg2_fsel       :1;     //bit3
    uint16 Reg2_sw_en      :1;     //bit4
    uint16 NotRstOvercur2  :1;     //bit5
    uint16 Buzzer          :1;     //bit6
    uint16 Ballast         :1;     //bit7
    uint16 unused          :8;
  } Config;
}TPwrConfig;

typedef union {
  uint16 CanIDWord;
  struct  {
    uint16 NotSD_WP        :1;     //bit0
    uint16 NotSD_CD        :1;     //bit1
    uint16 CanID           :8;     //bit2
    uint16 res             :6;
  } ConfID;
}TCanId;

typedef union {
  uint16 CanIDWord;
  struct  {
    uint16 NotSD_WP        :1;     //bit0
    uint16 NotSD_CD        :1;     //bit1
    uint16 CanID           :8;     //bit2
    uint16 res             :6;
  } ConfID;
} T_PwrConfID;

typedef struct {
  int cur1;
  int cur2;
  int vboard1;
  int vboard2;
}TADtable;

typedef struct {
  uint8 Error:2;
  uint8 Reset:1;
  uint8 DisableBuzz:1;
  uint8 DisableVmot:1;
  uint8 Resv:3;
} T_PwrStatus;

typedef struct {
  uint8 ErrorFlags:2;
  uint8 ResetActiv:1;
  uint8 Resv:5;
} T_PwrStatus2;

typedef struct {
  uint16 Fan3wire :1;
  uint16 Fan4wire :1;
  uint16 FanOn    :1;
  uint16 Res      :5;
  uint16 Duty     :8;
} T_PwrFanCtrl;

typedef struct S_PwrAppPublic{
/*  0 */  T_PwrStatus   Status;
/*  1 */  T_PwrStatus2  Status2;
/*  2 */  uint8       SleepMode;
/*  3 */  uint8       LedMode;
/*  4 */  TADtable    Tablead;
/*  C */  uint8     VmotCtrl;
/*  D */  uint8     VmotThres;
/*  E */  uint16    VmotTimeout;
/* 10 */  uint16    Vsupplymin;
/* 12 */  uint16    Vsupplymaxlow;
/* 14 */  uint16    Vsupplymaxhigh;
/* 16 */  TPwrConfig  PwrConfig;
/* 18 */  TPwrConfig  PwrOnConfig;
/* 1A */  TPwrConfig  PwrOffConfig;
/* 1C */  TPwrConfig  StartPwrConfig;
/* 1E */  T_PwrFanCtrl  FanCtrl;
/* 20 */  uint16        FanSpeed;
/* 22 */  uint16        HardwareRev;
} T_PwrAppPublic;

extern T_PwrAppPublic PwrAppPublic;

#define POWERMAN_DEVICECODE  0xE0 //

#endif /* POWERMAN_H_ */
