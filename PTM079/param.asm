;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Thu Aug 05 16:17:33 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../param.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Iguana_yachts\PTM079")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_PAR_Flag+0,32
	.bits	1,16			; _PAR_Flag @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TimeLogIndex+0,32
	.bits	8,16			; _TimeLogIndex @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_PAR_Capacity_Left+0,32
	.bits	0,32			; _PAR_Capacity_Left @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_PAR_Operating_Hours_day+0,32
	.bits	0,32			; _PAR_Operating_Hours_day @ 0

	.sect	".cinit"
	.align	1
	.field  	-4,16
	.field  	_PAR_Capacity_TotalLife_Used+0,32
	.bits		0,32
	.bits		0,32			; _PAR_Capacity_TotalLife_Used @ 0

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_1,16
	.field  	_PAR_StatEepromCommandState+0,32
	.bits	0,16			; _PAR_StatEepromCommandState[0] @ 0
	.bits	0,16			; _PAR_StatEepromCommandState[1] @ 16
	.bits	0,16			; _PAR_StatEepromCommandState[2] @ 32
	.bits	0,16			; _PAR_StatEepromCommandState[3] @ 48
	.bits	0,16			; _PAR_StatEepromCommandState[4] @ 64
$C$IR_1:	.set	5

	.sect	".cinit"
	.align	1
	.field  	-$C$IR_2,16
	.field  	_TimeLog+0,32
	.bits		0,32
	.bits		0,32			; _TimeLog[0]._date @ 0
	.bits	0,32			; _TimeLog[0]._error @ 64
	.bits		0,32
	.bits		0,32			; _TimeLog[1]._date @ 96
	.bits	0,32			; _TimeLog[1]._error @ 160
	.bits		0,32
	.bits		0,32			; _TimeLog[2]._date @ 192
	.bits	0,32			; _TimeLog[2]._error @ 256
	.bits		0,32
	.bits		0,32			; _TimeLog[3]._date @ 288
	.bits	0,32			; _TimeLog[3]._error @ 352
	.bits		0,32
	.bits		0,32			; _TimeLog[4]._date @ 384
	.bits	0,32			; _TimeLog[4]._error @ 448
	.bits		0,32
	.bits		0,32			; _TimeLog[5]._date @ 480
	.bits	0,32			; _TimeLog[5]._error @ 544
	.bits		0,32
	.bits		0,32			; _TimeLog[6]._date @ 576
	.bits	0,32			; _TimeLog[6]._error @ 640
	.bits		0,32
	.bits		0,32			; _TimeLog[7]._date @ 672
	.bits	0,32			; _TimeLog[7]._error @ 736
$C$IR_2:	.set	48


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$1


$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Voltage_Min")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ODP_Voltage_Min")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("CNV_CurrentRange")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_CNV_CurrentRange")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_Max")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ODP_Temperature_Max")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_OverTemp_ErrCounter")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ODP_CommError_OverTemp_ErrCounter")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_OverVoltage_ErrCounter")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_ODP_CommError_OverVoltage_ErrCounter")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Version")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_ODV_Version")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_Min")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_ODP_Temperature_Min")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("ODP_VersionParameters")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ODP_VersionParameters")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$13, DW_AT_declaration
	.dwattr $C$DW$13, DW_AT_external

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$9)
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$42)
$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$9)
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$14

	.global	_EepromIndexesSize
	.sect	".econst"
	.align	1
_EepromIndexesSize:
	.bits	249,16			; _EepromIndexesSize @ 0

$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("EepromIndexesSize")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_EepromIndexesSize")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_addr _EepromIndexesSize]
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$19, DW_AT_external
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Voltage_Max")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_ODP_Voltage_Max")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_Min")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_ODP_Current_Min")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$143)
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$147)
$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$40)
	.dwendtag $C$DW$22


$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$146)
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$40)
	.dwendtag $C$DW$26

$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_Max")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_ODP_Current_Max")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$143)
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$147)
$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$40)
	.dwendtag $C$DW$30

$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("gateway_dict_obj100A")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_gateway_dict_obj100A")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
	.global	_PAR_Flag
_PAR_Flag:	.usect	".ebss",1,1,0
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Flag")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_PAR_Flag")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_addr _PAR_Flag]
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$35, DW_AT_external
	.global	_TimeLogIndex
_TimeLogIndex:	.usect	".ebss",1,1,0
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_addr _TimeLogIndex]
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_external
	.global	_PAR_Capacity_Left
_PAR_Capacity_Left:	.usect	".ebss",2,1,1
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Left")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_PAR_Capacity_Left")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_addr _PAR_Capacity_Left]
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("CNV_CurrentUnit")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_CNV_CurrentUnit")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$194)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
	.global	_PAR_Operating_Hours_day
_PAR_Operating_Hours_day:	.usect	".ebss",2,1,1
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Operating_Hours_day")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_PAR_Operating_Hours_day")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_addr _PAR_Operating_Hours_day]
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$39, DW_AT_external
	.global	_PAR_Capacity_Total
_PAR_Capacity_Total:	.usect	".ebss",2,1,1
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_addr _PAR_Capacity_Total]
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_SerialNumber")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODP_Board_SerialNumber")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("gateway_dict_obj1018_Revision_Number")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_gateway_dict_obj1018_Revision_Number")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external

$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("_setODentry")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("__setODentry")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$72)
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$9)
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
$C$DW$49	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$3)
$C$DW$50	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$79)
$C$DW$51	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$6)
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$6)
$C$DW$53	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$45


$C$DW$54	.dwtag  DW_TAG_subprogram, DW_AT_name("_getODentry")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("__getODentry")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$72)
$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$9)
$C$DW$57	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
$C$DW$58	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$3)
$C$DW$59	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$79)
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$50)
$C$DW$61	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$6)
$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$6)
$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$54


$C$DW$64	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$13)
$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$81)
$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$123)
$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$64


$C$DW$69	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$69

$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_external
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$72, DW_AT_declaration
	.dwattr $C$DW$72, DW_AT_external
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$73, DW_AT_declaration
	.dwattr $C$DW$73, DW_AT_external
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CrcParameters")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_ODP_CrcParameters")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_external
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("gateway_dict_obj1018_Vendor_ID")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_gateway_dict_obj1018_Vendor_ID")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$75, DW_AT_declaration
	.dwattr $C$DW$75, DW_AT_external
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("gateway_dict_obj1018_Product_Code")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_gateway_dict_obj1018_Product_Code")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_external
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("gateway_dict_obj1018_Serial_Number")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_gateway_dict_obj1018_Serial_Number")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external
	.global	_PAR_Capacity_TotalLife_Used
_PAR_Capacity_TotalLife_Used:	.usect	".ebss",4,1,1
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_addr _PAR_Capacity_TotalLife_Used]
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$78, DW_AT_external
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_external
	.global	_PAR_StatEepromCommandState
_PAR_StatEepromCommandState:	.usect	".ebss",5,1,0
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("PAR_StatEepromCommandState")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_PAR_StatEepromCommandState")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _PAR_StatEepromCommandState]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$80, DW_AT_external
	.global	_STAT_PAGE2
	.sect	".econst:_STAT_PAGE2"
	.clink
	.align	2
_STAT_PAGE2:
	.bits	_TimeLog,32		; _STAT_PAGE2[0]._pdata @ 0
	.bits	8,16			; _STAT_PAGE2[0]._size @ 32
	.bits	1056,16			; _STAT_PAGE2[0]._address @ 48
	.bits	_TimeLog+6,32		; _STAT_PAGE2[1]._pdata @ 64
	.bits	8,16			; _STAT_PAGE2[1]._size @ 96
	.bits	1064,16			; _STAT_PAGE2[1]._address @ 112
	.bits	_TimeLog+12,32		; _STAT_PAGE2[2]._pdata @ 128
	.bits	8,16			; _STAT_PAGE2[2]._size @ 160
	.bits	1072,16			; _STAT_PAGE2[2]._address @ 176
	.bits	_TimeLog+18,32		; _STAT_PAGE2[3]._pdata @ 192
	.bits	8,16			; _STAT_PAGE2[3]._size @ 224
	.bits	1080,16			; _STAT_PAGE2[3]._address @ 240

$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE2")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_STAT_PAGE2")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_addr _STAT_PAGE2]
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$81, DW_AT_external
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$82, DW_AT_declaration
	.dwattr $C$DW$82, DW_AT_external
	.global	_STAT_PAGE4
	.sect	".econst:_STAT_PAGE4"
	.clink
	.align	2
_STAT_PAGE4:
	.bits	_TimeLog+24,32		; _STAT_PAGE4[0]._pdata @ 0
	.bits	8,16			; _STAT_PAGE4[0]._size @ 32
	.bits	1120,16			; _STAT_PAGE4[0]._address @ 48
	.bits	_TimeLog+30,32		; _STAT_PAGE4[1]._pdata @ 64
	.bits	8,16			; _STAT_PAGE4[1]._size @ 96
	.bits	1128,16			; _STAT_PAGE4[1]._address @ 112
	.bits	_TimeLog+36,32		; _STAT_PAGE4[2]._pdata @ 128
	.bits	8,16			; _STAT_PAGE4[2]._size @ 160
	.bits	1136,16			; _STAT_PAGE4[2]._address @ 176
	.bits	_TimeLog+42,32		; _STAT_PAGE4[3]._pdata @ 192
	.bits	8,16			; _STAT_PAGE4[3]._size @ 224
	.bits	1144,16			; _STAT_PAGE4[3]._address @ 240

$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE4")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_STAT_PAGE4")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_addr _STAT_PAGE4]
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$83, DW_AT_external
	.global	_STAT_PAGE5
	.sect	".econst:_STAT_PAGE5"
	.clink
	.align	2
_STAT_PAGE5:
	.bits	_PAR_Capacity_Left,32		; _STAT_PAGE5[0]._pdata @ 0
	.bits	4,16			; _STAT_PAGE5[0]._size @ 32
	.bits	1152,16			; _STAT_PAGE5[0]._address @ 48
	.bits	_PAR_Capacity_TotalLife_Used,32		; _STAT_PAGE5[1]._pdata @ 64
	.bits	8,16			; _STAT_PAGE5[1]._size @ 96
	.bits	1156,16			; _STAT_PAGE5[1]._address @ 112
	.bits	_PAR_Operating_Hours_day,32		; _STAT_PAGE5[2]._pdata @ 128
	.bits	4,16			; _STAT_PAGE5[2]._size @ 160
	.bits	1164,16			; _STAT_PAGE5[2]._address @ 176
	.bits	_ODP_CommError_OverTemp_ErrCounter,32		; _STAT_PAGE5[3]._pdata @ 192
	.bits	1,16			; _STAT_PAGE5[3]._size @ 224
	.bits	1168,16			; _STAT_PAGE5[3]._address @ 240
	.bits	_ODP_CommError_OverVoltage_ErrCounter,32		; _STAT_PAGE5[4]._pdata @ 256
	.bits	1,16			; _STAT_PAGE5[4]._size @ 288
	.bits	1169,16			; _STAT_PAGE5[4]._address @ 304
	.bits	_ODP_CommError_LowVoltage_ErrCounter,32		; _STAT_PAGE5[5]._pdata @ 320
	.bits	1,16			; _STAT_PAGE5[5]._size @ 352
	.bits	1170,16			; _STAT_PAGE5[5]._address @ 368

$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE5")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_STAT_PAGE5")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_addr _STAT_PAGE5]
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$84, DW_AT_external
	.global	_PAR_EEPROM_INDEXES
	.sect	".econst:_PAR_EEPROM_INDEXES"
	.clink
	.align	2
_PAR_EEPROM_INDEXES:
	.bits	32,16			; _PAR_EEPROM_INDEXES[0]._size @ 0
	.bits	1024,16			; _PAR_EEPROM_INDEXES[0]._address @ 16
	.bits	11,16			; _PAR_EEPROM_INDEXES[0]._nb @ 32
	.space	16
	.bits	_STAT_PAGE1,32		; _PAR_EEPROM_INDEXES[0]._indexes @ 64
	.bits	32,16			; _PAR_EEPROM_INDEXES[1]._size @ 96
	.bits	1056,16			; _PAR_EEPROM_INDEXES[1]._address @ 112
	.bits	4,16			; _PAR_EEPROM_INDEXES[1]._nb @ 128
	.space	16
	.bits	_STAT_PAGE2,32		; _PAR_EEPROM_INDEXES[1]._indexes @ 160
	.bits	32,16			; _PAR_EEPROM_INDEXES[2]._size @ 192
	.bits	1088,16			; _PAR_EEPROM_INDEXES[2]._address @ 208
	.bits	8,16			; _PAR_EEPROM_INDEXES[2]._nb @ 224
	.space	16
	.bits	_STAT_PAGE3,32		; _PAR_EEPROM_INDEXES[2]._indexes @ 256
	.bits	32,16			; _PAR_EEPROM_INDEXES[3]._size @ 288
	.bits	1120,16			; _PAR_EEPROM_INDEXES[3]._address @ 304
	.bits	4,16			; _PAR_EEPROM_INDEXES[3]._nb @ 320
	.space	16
	.bits	_STAT_PAGE4,32		; _PAR_EEPROM_INDEXES[3]._indexes @ 352
	.bits	32,16			; _PAR_EEPROM_INDEXES[4]._size @ 384
	.bits	1152,16			; _PAR_EEPROM_INDEXES[4]._address @ 400
	.bits	6,16			; _PAR_EEPROM_INDEXES[4]._nb @ 416
	.space	16
	.bits	_STAT_PAGE5,32		; _PAR_EEPROM_INDEXES[4]._indexes @ 448

$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("PAR_EEPROM_INDEXES")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_PAR_EEPROM_INDEXES")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_addr _PAR_EEPROM_INDEXES]
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$85, DW_AT_external
	.global	_STAT_PAGE3
	.sect	".econst:_STAT_PAGE3"
	.clink
	.align	2
_STAT_PAGE3:
	.bits	_TimeLog+4,32		; _STAT_PAGE3[0]._pdata @ 0
	.bits	4,16			; _STAT_PAGE3[0]._size @ 32
	.bits	1088,16			; _STAT_PAGE3[0]._address @ 48
	.bits	_TimeLog+10,32		; _STAT_PAGE3[1]._pdata @ 64
	.bits	4,16			; _STAT_PAGE3[1]._size @ 96
	.bits	1092,16			; _STAT_PAGE3[1]._address @ 112
	.bits	_TimeLog+16,32		; _STAT_PAGE3[2]._pdata @ 128
	.bits	4,16			; _STAT_PAGE3[2]._size @ 160
	.bits	1096,16			; _STAT_PAGE3[2]._address @ 176
	.bits	_TimeLog+22,32		; _STAT_PAGE3[3]._pdata @ 192
	.bits	4,16			; _STAT_PAGE3[3]._size @ 224
	.bits	1100,16			; _STAT_PAGE3[3]._address @ 240
	.bits	_TimeLog+28,32		; _STAT_PAGE3[4]._pdata @ 256
	.bits	4,16			; _STAT_PAGE3[4]._size @ 288
	.bits	1104,16			; _STAT_PAGE3[4]._address @ 304
	.bits	_TimeLog+34,32		; _STAT_PAGE3[5]._pdata @ 320
	.bits	4,16			; _STAT_PAGE3[5]._size @ 352
	.bits	1108,16			; _STAT_PAGE3[5]._address @ 368
	.bits	_TimeLog+40,32		; _STAT_PAGE3[6]._pdata @ 384
	.bits	4,16			; _STAT_PAGE3[6]._size @ 416
	.bits	1112,16			; _STAT_PAGE3[6]._address @ 432
	.bits	_TimeLog+46,32		; _STAT_PAGE3[7]._pdata @ 448
	.bits	4,16			; _STAT_PAGE3[7]._size @ 480
	.bits	1116,16			; _STAT_PAGE3[7]._address @ 496

$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE3")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_STAT_PAGE3")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_addr _STAT_PAGE3]
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$86, DW_AT_external
	.global	_STAT_PAGE1
	.sect	".econst:_STAT_PAGE1"
	.clink
	.align	2
_STAT_PAGE1:
	.bits	_ODP_OnTime,32		; _STAT_PAGE1[0]._pdata @ 0
	.bits	4,16			; _STAT_PAGE1[0]._size @ 32
	.bits	1024,16			; _STAT_PAGE1[0]._address @ 48
	.bits	_PAR_Flag,32		; _STAT_PAGE1[1]._pdata @ 64
	.bits	2,16			; _STAT_PAGE1[1]._size @ 96
	.bits	1028,16			; _STAT_PAGE1[1]._address @ 112
	.bits	_ODV_Gateway_Date_Time,32		; _STAT_PAGE1[2]._pdata @ 128
	.bits	8,16			; _STAT_PAGE1[2]._size @ 160
	.bits	1030,16			; _STAT_PAGE1[2]._address @ 176
	.bits	_ODV_Gateway_Errorcode,32		; _STAT_PAGE1[3]._pdata @ 192
	.bits	4,16			; _STAT_PAGE1[3]._size @ 224
	.bits	1038,16			; _STAT_PAGE1[3]._address @ 240
	.bits	_ODP_Temperature_Max,32		; _STAT_PAGE1[4]._pdata @ 256
	.bits	2,16			; _STAT_PAGE1[4]._size @ 288
	.bits	1042,16			; _STAT_PAGE1[4]._address @ 304
	.bits	_ODP_Current_Max,32		; _STAT_PAGE1[5]._pdata @ 320
	.bits	2,16			; _STAT_PAGE1[5]._size @ 352
	.bits	1044,16			; _STAT_PAGE1[5]._address @ 368
	.bits	_ODP_Current_Min,32		; _STAT_PAGE1[6]._pdata @ 384
	.bits	2,16			; _STAT_PAGE1[6]._size @ 416
	.bits	1046,16			; _STAT_PAGE1[6]._address @ 432
	.bits	_TimeLogIndex,32		; _STAT_PAGE1[7]._pdata @ 448
	.bits	2,16			; _STAT_PAGE1[7]._size @ 480
	.bits	1048,16			; _STAT_PAGE1[7]._address @ 496
	.bits	_ODP_Voltage_Max,32		; _STAT_PAGE1[8]._pdata @ 512
	.bits	2,16			; _STAT_PAGE1[8]._size @ 544
	.bits	1050,16			; _STAT_PAGE1[8]._address @ 560
	.bits	_ODP_Voltage_Min,32		; _STAT_PAGE1[9]._pdata @ 576
	.bits	2,16			; _STAT_PAGE1[9]._size @ 608
	.bits	1052,16			; _STAT_PAGE1[9]._address @ 624
	.bits	_ODP_Temperature_Min,32		; _STAT_PAGE1[10]._pdata @ 640
	.bits	2,16			; _STAT_PAGE1[10]._size @ 672
	.bits	1054,16			; _STAT_PAGE1[10]._address @ 688

$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("STAT_PAGE1")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_STAT_PAGE1")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_addr _STAT_PAGE1]
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$87, DW_AT_external
	.global	_TimeLog
_TimeLog:	.usect	".ebss",48,1,1
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("TimeLog")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_TimeLog")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _TimeLog]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$88, DW_AT_external
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("mailboxWriteParameters")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_mailboxWriteParameters")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$89, DW_AT_declaration
	.dwattr $C$DW$89, DW_AT_external
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("ODI_EEPROM_INDEXES")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_ODI_EEPROM_INDEXES")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$90, DW_AT_declaration
	.dwattr $C$DW$90, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2632012 
	.sect	".text"
	.clink
	.global	_PAR_AddLog

$C$DW$91	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$91, DW_AT_low_pc(_PAR_AddLog)
	.dwattr $C$DW$91, DW_AT_high_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$91, DW_AT_external
	.dwattr $C$DW$91, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$91, DW_AT_TI_begin_line(0x6e)
	.dwattr $C$DW$91, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$91, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../param.c",line 110,column 22,is_stmt,address _PAR_AddLog

	.dwfde $C$DW$CIE, _PAR_AddLog

;***************************************************************
;* FNAME: _PAR_AddLog                   FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_PAR_AddLog:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../param.c",line 111,column 3,is_stmt
        MOVB      AL,#9                 ; [CPU_] |111| 
        MOVL      XAR4,#_ODV_Gateway_Date_Time ; [CPU_U] |111| 
        MOVB      AH,#7                 ; [CPU_] |111| 
        MOVB      XAR5,#0               ; [CPU_] |111| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$92, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |111| 
        ; call occurs [#_I2C_Command] ; [] |111| 
	.dwpsn	file "../param.c",line 112,column 3,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |112| 
        CMPB      AL,#7                 ; [CPU_] |112| 
        B         $C$L1,HIS             ; [CPU_] |112| 
        ; branchcc occurs ; [] |112| 
	.dwpsn	file "../param.c",line 112,column 32,is_stmt
        INC       @_TimeLogIndex        ; [CPU_] |112| 
        B         $C$L2,UNC             ; [CPU_] |112| 
        ; branch occurs ; [] |112| 
$C$L1:    
	.dwpsn	file "../param.c",line 113,column 8,is_stmt
        MOV       @_TimeLogIndex,#0     ; [CPU_] |113| 
$C$L2:    
	.dwpsn	file "../param.c",line 114,column 3,is_stmt
        MOV       T,#6                  ; [CPU_] |114| 
        MPYXU     ACC,T,@_TimeLogIndex  ; [CPU_] |114| 
        MOVW      DP,#_ODV_Gateway_Date_Time ; [CPU_U] 
        MOVL      XAR6,@_ODV_Gateway_Date_Time ; [CPU_] |114| 
        MOVL      XAR7,@_ODV_Gateway_Date_Time+2 ; [CPU_] |114| 
        MOVL      XAR4,#_TimeLog        ; [CPU_U] |114| 
        ADDL      XAR4,ACC              ; [CPU_] |114| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |114| 
        MOVL      *+XAR4[2],XAR7        ; [CPU_] |114| 
	.dwpsn	file "../param.c",line 115,column 3,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MPYXU     ACC,T,@_TimeLogIndex  ; [CPU_] |115| 
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      XAR6,@_ODV_Gateway_Errorcode ; [CPU_] |115| 
        MOVL      XAR4,#_TimeLog+4      ; [CPU_U] |115| 
        ADDL      XAR4,ACC              ; [CPU_] |115| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |115| 
	.dwpsn	file "../param.c",line 116,column 3,is_stmt
        MOV       AL,#-1                ; [CPU_] |116| 
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$93, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |116| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |116| 
	.dwpsn	file "../param.c",line 117,column 3,is_stmt
        MOV       AL,#-3                ; [CPU_] |117| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |117| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |117| 
	.dwpsn	file "../param.c",line 118,column 3,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |118| 
        CMPB      AL,#4                 ; [CPU_] |118| 
        B         $C$L3,HIS             ; [CPU_] |118| 
        ; branchcc occurs ; [] |118| 
	.dwpsn	file "../param.c",line 119,column 5,is_stmt
        MOV       AL,#-2                ; [CPU_] |119| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$95, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |119| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |119| 
	.dwpsn	file "../param.c",line 120,column 3,is_stmt
        B         $C$L4,UNC             ; [CPU_] |120| 
        ; branch occurs ; [] |120| 
$C$L3:    
	.dwpsn	file "../param.c",line 120,column 9,is_stmt
        MOV       AL,#-4                ; [CPU_] |120| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$96, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |120| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |120| 
	.dwpsn	file "../param.c",line 121,column 1,is_stmt
$C$L4:    
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$91, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$91, DW_AT_TI_end_line(0x79)
	.dwattr $C$DW$91, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$91

	.sect	".text"
	.clink
	.global	_PAR_GetLogNB

$C$DW$98	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$98, DW_AT_low_pc(_PAR_GetLogNB)
	.dwattr $C$DW$98, DW_AT_high_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$98, DW_AT_external
	.dwattr $C$DW$98, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$98, DW_AT_TI_begin_line(0x7b)
	.dwattr $C$DW$98, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$98, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../param.c",line 123,column 28,is_stmt,address _PAR_GetLogNB

	.dwfde $C$DW$CIE, _PAR_GetLogNB
$C$DW$99	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nb")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_GetLogNB                 FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_PAR_GetLogNB:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("nb")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |123| 
	.dwpsn	file "../param.c",line 124,column 3,is_stmt
        MOV       T,#6                  ; [CPU_] |124| 
        MOVL      XAR4,#_TimeLog        ; [CPU_U] |124| 
        MPYXU     ACC,T,*-SP[1]         ; [CPU_] |124| 
        ADDL      XAR4,ACC              ; [CPU_] |124| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |124| 
        MOVW      DP,#_ODV_Gateway_Date_Time ; [CPU_U] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |124| 
        MOVL      @_ODV_Gateway_Date_Time,ACC ; [CPU_] |124| 
        MOVL      @_ODV_Gateway_Date_Time+2,XAR6 ; [CPU_] |124| 
	.dwpsn	file "../param.c",line 125,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      XAR4,#_TimeLog+4      ; [CPU_U] |125| 
        MPYXU     ACC,T,*-SP[1]         ; [CPU_] |125| 
        ADDL      XAR4,ACC              ; [CPU_] |125| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |125| 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |125| 
	.dwpsn	file "../param.c",line 126,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$98, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$98, DW_AT_TI_end_line(0x7e)
	.dwattr $C$DW$98, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$98

	.sect	".text"
	.clink
	.global	_PAR_TestEeprom

$C$DW$102	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_TestEeprom")
	.dwattr $C$DW$102, DW_AT_low_pc(_PAR_TestEeprom)
	.dwattr $C$DW$102, DW_AT_high_pc(0x00)
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_PAR_TestEeprom")
	.dwattr $C$DW$102, DW_AT_external
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$102, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$102, DW_AT_TI_begin_line(0x8d)
	.dwattr $C$DW$102, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$102, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../param.c",line 141,column 27,is_stmt,address _PAR_TestEeprom

	.dwfde $C$DW$CIE, _PAR_TestEeprom

;***************************************************************
;* FNAME: _PAR_TestEeprom               FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_PAR_TestEeprom:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -1]
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_breg20 -2]
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -3]
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_breg20 -19]
	.dwpsn	file "../param.c",line 143,column 13,is_stmt
        MOVB      *-SP[3],#1,UNC        ; [CPU_] |143| 
	.dwpsn	file "../param.c",line 145,column 8,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |145| 
	.dwpsn	file "../param.c",line 145,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |145| 
        CMPB      AL,#16                ; [CPU_] |145| 
        B         $C$L6,GEQ             ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
$C$L5:    
	.dwpsn	file "../param.c",line 146,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[1]           ; [CPU_] |146| 
        MOVZ      AR5,*-SP[1]           ; [CPU_] |146| 
        MOVZ      AR4,SP                ; [CPU_U] |146| 
        SUBB      XAR4,#19              ; [CPU_U] |146| 
        ADDL      XAR4,ACC              ; [CPU_] |146| 
        ADDB      XAR5,#1               ; [CPU_] |146| 
        MOV       ACC,AR5 << #8         ; [CPU_] |146| 
        ADD       AL,*-SP[1]            ; [CPU_] |146| 
        ADDB      AL,#2                 ; [CPU_] |146| 
        MOV       *+XAR4[0],AL          ; [CPU_] |146| 
	.dwpsn	file "../param.c",line 145,column 32,is_stmt
        INC       *-SP[1]               ; [CPU_] |145| 
	.dwpsn	file "../param.c",line 145,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |145| 
        CMPB      AL,#16                ; [CPU_] |145| 
        B         $C$L5,LT              ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
$C$L6:    
	.dwpsn	file "../param.c",line 147,column 3,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |147| 
	.dwpsn	file "../param.c",line 148,column 3,is_stmt
        B         $C$L12,UNC            ; [CPU_] |148| 
        ; branch occurs ; [] |148| 
$C$L7:    
	.dwpsn	file "../param.c",line 149,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |149| 
        MOVZ      AR5,*-SP[2]           ; [CPU_] |149| 
        MOVB      AL,#0                 ; [CPU_] |149| 
        MOVB      AH,#32                ; [CPU_] |149| 
        SUBB      XAR4,#19              ; [CPU_U] |149| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |149| 
        ; call occurs [#_I2C_Command] ; [] |149| 
        MOV       *-SP[3],AL            ; [CPU_] |149| 
	.dwpsn	file "../param.c",line 150,column 5,is_stmt
        MOVB      AL,#2                 ; [CPU_] |150| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |150| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |150| 
        ; call occurs [#_SEM_pend] ; [] |150| 
	.dwpsn	file "../param.c",line 151,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |151| 
        BF        $C$L8,EQ              ; [CPU_] |151| 
        ; branchcc occurs ; [] |151| 
	.dwpsn	file "../param.c",line 152,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |152| 
        MOVZ      AR5,*-SP[2]           ; [CPU_] |152| 
        MOVB      AL,#1                 ; [CPU_] |152| 
        MOVB      AH,#32                ; [CPU_] |152| 
        SUBB      XAR4,#19              ; [CPU_U] |152| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |152| 
        ; call occurs [#_I2C_Command] ; [] |152| 
        MOV       *-SP[3],AL            ; [CPU_] |152| 
$C$L8:    
	.dwpsn	file "../param.c",line 153,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |153| 
        MOVB      AL,#2                 ; [CPU_] |153| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |153| 
        ; call occurs [#_SEM_pend] ; [] |153| 
	.dwpsn	file "../param.c",line 154,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |154| 
        BF        $C$L13,EQ             ; [CPU_] |154| 
        ; branchcc occurs ; [] |154| 
	.dwpsn	file "../param.c",line 155,column 12,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |155| 
	.dwpsn	file "../param.c",line 155,column 16,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |155| 
        CMPB      AL,#16                ; [CPU_] |155| 
        B         $C$L11,GEQ            ; [CPU_] |155| 
        ; branchcc occurs ; [] |155| 
$C$L9:    
	.dwpsn	file "../param.c",line 156,column 9,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,SP                ; [CPU_U] |156| 
        MOV       ACC,*-SP[1]           ; [CPU_] |156| 
        SUBB      XAR4,#19              ; [CPU_U] |156| 
        ADDL      XAR4,ACC              ; [CPU_] |156| 
        MOV       AL,*-SP[1]            ; [CPU_] |156| 
        ADDB      AL,#1                 ; [CPU_] |156| 
        MOVZ      AR6,*+XAR4[0]         ; [CPU_] |156| 
        MOV       ACC,AL << #8          ; [CPU_] |156| 
        ADD       AL,*-SP[1]            ; [CPU_] |156| 
        ADDB      AL,#2                 ; [CPU_] |156| 
        MOVU      ACC,AL                ; [CPU_] |156| 
        CMPL      ACC,XAR6              ; [CPU_] |156| 
        BF        $C$L10,EQ             ; [CPU_] |156| 
        ; branchcc occurs ; [] |156| 
	.dwpsn	file "../param.c",line 157,column 11,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |157| 
	.dwpsn	file "../param.c",line 158,column 11,is_stmt
        B         $C$L11,UNC            ; [CPU_] |158| 
        ; branch occurs ; [] |158| 
$C$L10:    
	.dwpsn	file "../param.c",line 155,column 36,is_stmt
        INC       *-SP[1]               ; [CPU_] |155| 
	.dwpsn	file "../param.c",line 155,column 16,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |155| 
        CMPB      AL,#16                ; [CPU_] |155| 
        B         $C$L9,LT              ; [CPU_] |155| 
        ; branchcc occurs ; [] |155| 
	.dwpsn	file "../param.c",line 161,column 5,is_stmt
        B         $C$L11,UNC            ; [CPU_] |161| 
        ; branch occurs ; [] |161| 
$C$L11:    
	.dwpsn	file "../param.c",line 164,column 5,is_stmt
        ADD       *-SP[2],#32           ; [CPU_] |164| 
$C$L12:    
	.dwpsn	file "../param.c",line 148,column 10,is_stmt
        CMP       *-SP[2],#8192         ; [CPU_] |148| 
        B         $C$L13,GEQ            ; [CPU_] |148| 
        ; branchcc occurs ; [] |148| 
        MOV       AL,*-SP[3]            ; [CPU_] |148| 
        BF        $C$L7,NEQ             ; [CPU_] |148| 
        ; branchcc occurs ; [] |148| 
$C$L13:    
	.dwpsn	file "../param.c",line 166,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |166| 
        BF        $C$L14,EQ             ; [CPU_] |166| 
        ; branchcc occurs ; [] |166| 
	.dwpsn	file "../param.c",line 167,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |167| 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$111, DW_AT_TI_call
        LCR       #_PAR_SetDefaultParameters ; [CPU_] |167| 
        ; call occurs [#_PAR_SetDefaultParameters] ; [] |167| 
        MOV       *-SP[3],AL            ; [CPU_] |167| 
$C$L14:    
	.dwpsn	file "../param.c",line 168,column 3,is_stmt
	.dwpsn	file "../param.c",line 169,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$102, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$102, DW_AT_TI_end_line(0xa9)
	.dwattr $C$DW$102, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$102

	.sect	".text"
	.clink
	.global	_ComputeParamCRC

$C$DW$113	.dwtag  DW_TAG_subprogram, DW_AT_name("ComputeParamCRC")
	.dwattr $C$DW$113, DW_AT_low_pc(_ComputeParamCRC)
	.dwattr $C$DW$113, DW_AT_high_pc(0x00)
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_ComputeParamCRC")
	.dwattr $C$DW$113, DW_AT_external
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$113, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$113, DW_AT_TI_begin_line(0xb9)
	.dwattr $C$DW$113, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$113, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../param.c",line 185,column 29,is_stmt,address _ComputeParamCRC

	.dwfde $C$DW$CIE, _ComputeParamCRC

;***************************************************************
;* FNAME: _ComputeParamCRC              FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 24 Auto,  0 SOE     *
;***************************************************************

_ComputeParamCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("nb_data")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_nb_data")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -2]
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -18]
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -19]
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$117, DW_AT_location[DW_OP_breg20 -20]
$C$DW$118	.dwtag  DW_TAG_variable, DW_AT_name("indexcrc")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_indexcrc")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$118, DW_AT_location[DW_OP_breg20 -21]
$C$DW$119	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_breg20 -24]
$C$DW$120	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$120, DW_AT_location[DW_OP_breg20 -25]
	.dwpsn	file "../param.c",line 186,column 18,is_stmt
        MOVW      DP,#_EepromIndexesSize ; [CPU_U] 
        MOV       AL,@_EepromIndexesSize ; [CPU_] |186| 
        MOVL      XAR4,#_ODI_EEPROM_INDEXES+3 ; [CPU_U] |186| 
        ADDB      AL,#-1                ; [CPU_] |186| 
        MOVL      XAR5,#_ODI_EEPROM_INDEXES+2 ; [CPU_U] |186| 
        MOVU      ACC,AL                ; [CPU_] |186| 
        LSL       ACC,2                 ; [CPU_] |186| 
        ADDL      XAR4,ACC              ; [CPU_] |186| 
        MOV       AL,@_EepromIndexesSize ; [CPU_] |186| 
        ADDB      AL,#-1                ; [CPU_] |186| 
        MOVU      ACC,AL                ; [CPU_] |186| 
        LSL       ACC,2                 ; [CPU_] |186| 
        ADDL      XAR5,ACC              ; [CPU_] |186| 
        MOV       AL,*+XAR5[0]          ; [CPU_] |186| 
        ADD       AL,*+XAR4[0]          ; [CPU_] |186| 
        MOV       *-SP[2],AL            ; [CPU_] |186| 
	.dwpsn	file "../param.c",line 187,column 37,is_stmt
        MOV       *-SP[19],#0           ; [CPU_] |187| 
	.dwpsn	file "../param.c",line 187,column 47,is_stmt
        MOVB      *-SP[20],#32,UNC      ; [CPU_] |187| 
	.dwpsn	file "../param.c",line 188,column 15,is_stmt
        MOVB      ACC,#0                ; [CPU_] |188| 
        MOVL      *-SP[24],ACC          ; [CPU_] |188| 
	.dwpsn	file "../param.c",line 190,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |190| 
        MOVZ      AR5,SP                ; [CPU_U] |190| 
        MOV       AL,#8195              ; [CPU_] |190| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |190| 
        SUBB      XAR5,#21              ; [CPU_U] |190| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$121, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |190| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |190| 
        CMPB      AL,#0                 ; [CPU_] |190| 
        BF        $C$L15,EQ             ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
	.dwpsn	file "../param.c",line 191,column 5,is_stmt
        MOVU      ACC,*-SP[21]          ; [CPU_] |191| 
        MOVL      XAR4,#_ODI_EEPROM_INDEXES+3 ; [CPU_U] |191| 
        LSL       ACC,2                 ; [CPU_] |191| 
        ADDL      XAR4,ACC              ; [CPU_] |191| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |191| 
        LSR       AL,1                  ; [CPU_] |191| 
        MOV       *-SP[21],AL           ; [CPU_] |191| 
	.dwpsn	file "../param.c",line 192,column 3,is_stmt
        B         $C$L21,UNC            ; [CPU_] |192| 
        ; branch occurs ; [] |192| 
$C$L15:    
	.dwpsn	file "../param.c",line 194,column 5,is_stmt
        MOVB      *-SP[21],#1,UNC       ; [CPU_] |194| 
	.dwpsn	file "../param.c",line 196,column 3,is_stmt
        B         $C$L21,UNC            ; [CPU_] |196| 
        ; branch occurs ; [] |196| 
$C$L16:    
	.dwpsn	file "../param.c",line 197,column 5,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |197| 
        ADD       AL,*-SP[19]           ; [CPU_] |197| 
        CMP       AL,*-SP[2]            ; [CPU_] |197| 
        B         $C$L17,LOS            ; [CPU_] |197| 
        ; branchcc occurs ; [] |197| 
	.dwpsn	file "../param.c",line 197,column 29,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |197| 
        SUB       AL,*-SP[19]           ; [CPU_] |197| 
        MOV       *-SP[20],AL           ; [CPU_] |197| 
$C$L17:    
	.dwpsn	file "../param.c",line 198,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |198| 
        MOVZ      AR5,*-SP[19]          ; [CPU_] |198| 
        MOV       AH,*-SP[20]           ; [CPU_] |198| 
        MOVB      AL,#1                 ; [CPU_] |198| 
        SUBB      XAR4,#18              ; [CPU_U] |198| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$122, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |198| 
        ; call occurs [#_I2C_Command] ; [] |198| 
        MOV       *-SP[25],AL           ; [CPU_] |198| 
	.dwpsn	file "../param.c",line 200,column 5,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |200| 
        BF        $C$L18,NEQ            ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
	.dwpsn	file "../param.c",line 201,column 7,is_stmt
        MOVZ      AR0,*-SP[21]          ; [CPU_] |201| 
        MOVZ      AR4,SP                ; [CPU_U] |201| 
        SUBB      XAR4,#18              ; [CPU_U] |201| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |201| 
	.dwpsn	file "../param.c",line 202,column 7,is_stmt
        MOVZ      AR0,*-SP[21]          ; [CPU_] |202| 
        MOVZ      AR4,SP                ; [CPU_U] |202| 
        SUBB      XAR4,#18              ; [CPU_U] |202| 
        ADDB      XAR0,#1               ; [CPU_] |202| 
        MOV       *+XAR4[AR0],#0        ; [CPU_] |202| 
$C$L18:    
	.dwpsn	file "../param.c",line 204,column 5,is_stmt
        MOV       AL,*-SP[25]           ; [CPU_] |204| 
        BF        $C$L19,EQ             ; [CPU_] |204| 
        ; branchcc occurs ; [] |204| 
	.dwpsn	file "../param.c",line 205,column 7,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |205| 
        MOVZ      AR4,SP                ; [CPU_U] |205| 
        MOV       *-SP[1],AL            ; [CPU_] |205| 
        MOVL      ACC,*-SP[24]          ; [CPU_] |205| 
        MOVB      XAR5,#0               ; [CPU_] |205| 
        SUBB      XAR4,#18              ; [CPU_U] |205| 
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$123, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |205| 
        ; call occurs [#_getCRC32_cpu] ; [] |205| 
        MOVL      *-SP[24],ACC          ; [CPU_] |205| 
        B         $C$L20,UNC            ; [CPU_] |205| 
        ; branch occurs ; [] |205| 
$C$L19:    
	.dwpsn	file "../param.c",line 206,column 10,is_stmt
        MOVB      ACC,#0                ; [CPU_] |206| 
        B         $C$L22,UNC            ; [CPU_] |206| 
        ; branch occurs ; [] |206| 
$C$L20:    
	.dwpsn	file "../param.c",line 207,column 5,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |207| 
        ADD       *-SP[19],AL           ; [CPU_] |207| 
$C$L21:    
	.dwpsn	file "../param.c",line 196,column 9,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |196| 
        CMP       AL,*-SP[19]           ; [CPU_] |196| 
        B         $C$L16,HI             ; [CPU_] |196| 
        ; branchcc occurs ; [] |196| 
	.dwpsn	file "../param.c",line 209,column 3,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |209| 
$C$L22:    
	.dwpsn	file "../param.c",line 210,column 1,is_stmt
        SUBB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$113, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$113, DW_AT_TI_end_line(0xd2)
	.dwattr $C$DW$113, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$113

	.sect	".text"
	.clink
	.global	_PAR_UpdateCode

$C$DW$125	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$125, DW_AT_low_pc(_PAR_UpdateCode)
	.dwattr $C$DW$125, DW_AT_high_pc(0x00)
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$125, DW_AT_external
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$125, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$125, DW_AT_TI_begin_line(0xe3)
	.dwattr $C$DW$125, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$125, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../param.c",line 228,column 1,is_stmt,address _PAR_UpdateCode

	.dwfde $C$DW$CIE, _PAR_UpdateCode
$C$DW$126	.dwtag  DW_TAG_formal_parameter, DW_AT_name("set")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_set")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_UpdateCode               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_PAR_UpdateCode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("set")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_set")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg20 -1]
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[1],AL            ; [CPU_] |228| 
	.dwpsn	file "../param.c",line 229,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |229| 
        MOVL      *-SP[4],ACC           ; [CPU_] |229| 
	.dwpsn	file "../param.c",line 230,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |230| 
        BF        $C$L23,EQ             ; [CPU_] |230| 
        ; branchcc occurs ; [] |230| 
	.dwpsn	file "../param.c",line 231,column 5,is_stmt
        MOV       AL,#12824             ; [CPU_] |231| 
        MOV       AH,#2258              ; [CPU_] |231| 
        MOVL      *-SP[4],ACC           ; [CPU_] |231| 
$C$L23:    
	.dwpsn	file "../param.c",line 232,column 3,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |232| 
        MOVB      AL,#0                 ; [CPU_] |232| 
        MOVB      AH,#4                 ; [CPU_] |232| 
        MOVL      XAR5,#8188            ; [CPU_] |232| 
        SUBB      XAR4,#4               ; [CPU_U] |232| 
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$129, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |232| 
        ; call occurs [#_I2C_Command] ; [] |232| 
	.dwpsn	file "../param.c",line 233,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$125, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$125, DW_AT_TI_end_line(0xe9)
	.dwattr $C$DW$125, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$125

	.sect	".text"
	.clink
	.global	_PAR_SetDefaultParameters

$C$DW$131	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetDefaultParameters")
	.dwattr $C$DW$131, DW_AT_low_pc(_PAR_SetDefaultParameters)
	.dwattr $C$DW$131, DW_AT_high_pc(0x00)
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$131, DW_AT_external
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$131, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$131, DW_AT_TI_begin_line(0xfa)
	.dwattr $C$DW$131, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$131, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../param.c",line 250,column 43,is_stmt,address _PAR_SetDefaultParameters

	.dwfde $C$DW$CIE, _PAR_SetDefaultParameters
$C$DW$132	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_SetDefaultParameters     FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_PAR_SetDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_breg20 -2]
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_breg20 -3]
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -4]
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("index_param")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_index_param")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -5]
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],XAR4          ; [CPU_] |250| 
	.dwpsn	file "../param.c",line 251,column 12,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |251| 
	.dwpsn	file "../param.c",line 252,column 13,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |252| 
	.dwpsn	file "../param.c",line 254,column 21,is_stmt
        MOVB      ACC,#0                ; [CPU_] |254| 
        MOVL      *-SP[8],ACC           ; [CPU_] |254| 
	.dwpsn	file "../param.c",line 256,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |256| 
        MOVB      AH,#0                 ; [CPU_] |256| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |256| 
        MOV       AL,#8194              ; [CPU_] |256| 
        SUBB      XAR5,#5               ; [CPU_U] |256| 
$C$DW$138	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$138, DW_AT_low_pc(0x00)
	.dwattr $C$DW$138, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$138, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |256| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |256| 
        CMPB      AL,#0                 ; [CPU_] |256| 
        BF        $C$L26,NEQ            ; [CPU_] |256| 
        ; branchcc occurs ; [] |256| 
	.dwpsn	file "../param.c",line 257,column 5,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |257| 
	.dwpsn	file "../param.c",line 260,column 3,is_stmt
        B         $C$L26,UNC            ; [CPU_] |260| 
        ; branch occurs ; [] |260| 
$C$L24:    
	.dwpsn	file "../param.c",line 261,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |261| 
        MOVU      ACC,*-SP[5]           ; [CPU_] |261| 
        CMPL      ACC,XAR6              ; [CPU_] |261| 
        BF        $C$L25,NEQ            ; [CPU_] |261| 
        ; branchcc occurs ; [] |261| 
	.dwpsn	file "../param.c",line 261,column 27,is_stmt
        INC       *-SP[3]               ; [CPU_] |261| 
$C$L25:    
	.dwpsn	file "../param.c",line 262,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |262| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |262| 
        INC       *-SP[3]               ; [CPU_] |262| 
$C$DW$139	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$139, DW_AT_low_pc(0x00)
	.dwattr $C$DW$139, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$139, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |262| 
        ; call occurs [#_WritePermanentParam] ; [] |262| 
        MOVL      *-SP[8],ACC           ; [CPU_] |262| 
$C$L26:    
	.dwpsn	file "../param.c",line 260,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |260| 
        MOVL      XAR0,#304             ; [CPU_] |260| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |260| 
        CMP       AL,*-SP[3]            ; [CPU_] |260| 
        B         $C$L27,LOS            ; [CPU_] |260| 
        ; branchcc occurs ; [] |260| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |260| 
        BF        $C$L24,EQ             ; [CPU_] |260| 
        ; branchcc occurs ; [] |260| 
$C$L27:    
	.dwpsn	file "../param.c",line 264,column 3,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |264| 
        BF        $C$L29,NEQ            ; [CPU_] |264| 
        ; branchcc occurs ; [] |264| 
	.dwpsn	file "../param.c",line 265,column 5,is_stmt
        MOV       AL,#-1                ; [CPU_] |265| 
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$140, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |265| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |265| 
	.dwpsn	file "../param.c",line 266,column 5,is_stmt
        MOVB      AL,#1                 ; [CPU_] |266| 
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$141, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |266| 
        ; call occurs [#_PAR_UpdateCode] ; [] |266| 
        MOV       *-SP[4],AL            ; [CPU_] |266| 
	.dwpsn	file "../param.c",line 267,column 5,is_stmt
        CMPB      AL,#0                 ; [CPU_] |267| 
        BF        $C$L29,EQ             ; [CPU_] |267| 
        ; branchcc occurs ; [] |267| 
	.dwpsn	file "../param.c",line 268,column 7,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |268| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |268| 
$C$DW$142	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$142, DW_AT_low_pc(0x00)
	.dwattr $C$DW$142, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$142, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |268| 
        ; call occurs [#_WritePermanentParam] ; [] |268| 
        MOVL      *-SP[8],ACC           ; [CPU_] |268| 
	.dwpsn	file "../param.c",line 269,column 7,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |269| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |269| 
        BF        $C$L28,NEQ            ; [CPU_] |269| 
        ; branchcc occurs ; [] |269| 
        MOVB      XAR6,#1               ; [CPU_] |269| 
$C$L28:    
        MOV       *-SP[4],AR6           ; [CPU_] |269| 
$C$L29:    
	.dwpsn	file "../param.c",line 272,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |272| 
        BF        $C$L32,EQ             ; [CPU_] |272| 
        ; branchcc occurs ; [] |272| 
	.dwpsn	file "../param.c",line 273,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |273| 
        MOVB      AH,#0                 ; [CPU_] |273| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |273| 
        MOV       AL,#8195              ; [CPU_] |273| 
        SUBB      XAR5,#5               ; [CPU_U] |273| 
$C$DW$143	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$143, DW_AT_low_pc(0x00)
	.dwattr $C$DW$143, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$143, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |273| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |273| 
        CMPB      AL,#0                 ; [CPU_] |273| 
        BF        $C$L30,NEQ            ; [CPU_] |273| 
        ; branchcc occurs ; [] |273| 
	.dwpsn	file "../param.c",line 273,column 91,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |273| 
$C$L30:    
	.dwpsn	file "../param.c",line 274,column 5,is_stmt
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("_ComputeParamCRC")
	.dwattr $C$DW$144, DW_AT_TI_call
        LCR       #_ComputeParamCRC     ; [CPU_] |274| 
        ; call occurs [#_ComputeParamCRC] ; [] |274| 
        MOVW      DP,#_ODP_CrcParameters ; [CPU_U] 
        MOVL      @_ODP_CrcParameters,ACC ; [CPU_] |274| 
	.dwpsn	file "../param.c",line 275,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |275| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |275| 
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$145, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |275| 
        ; call occurs [#_WritePermanentParam] ; [] |275| 
        MOVL      *-SP[8],ACC           ; [CPU_] |275| 
	.dwpsn	file "../param.c",line 276,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |276| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |276| 
        BF        $C$L31,NEQ            ; [CPU_] |276| 
        ; branchcc occurs ; [] |276| 
        MOVB      XAR6,#1               ; [CPU_] |276| 
$C$L31:    
        MOV       *-SP[4],AR6           ; [CPU_] |276| 
$C$L32:    
	.dwpsn	file "../param.c",line 278,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |278| 
	.dwpsn	file "../param.c",line 279,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$131, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$131, DW_AT_TI_end_line(0x117)
	.dwattr $C$DW$131, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$131

	.sect	".text"
	.clink
	.global	_PAR_InitParam

$C$DW$147	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$147, DW_AT_low_pc(_PAR_InitParam)
	.dwattr $C$DW$147, DW_AT_high_pc(0x00)
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$147, DW_AT_external
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$147, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$147, DW_AT_TI_begin_line(0x129)
	.dwattr $C$DW$147, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$147, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../param.c",line 298,column 1,is_stmt,address _PAR_InitParam

	.dwfde $C$DW$CIE, _PAR_InitParam
$C$DW$148	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_InitParam                FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_PAR_InitParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -2]
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_breg20 -4]
$C$DW$151	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_breg20 -5]
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("index_param")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_index_param")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_breg20 -6]
$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[2],XAR4          ; [CPU_] |298| 
	.dwpsn	file "../param.c",line 299,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |299| 
        MOVL      *-SP[4],ACC           ; [CPU_] |299| 
	.dwpsn	file "../param.c",line 305,column 3,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |305| 
        MOVB      AL,#1                 ; [CPU_] |305| 
        MOVB      AH,#4                 ; [CPU_] |305| 
        MOVL      XAR5,#8188            ; [CPU_] |305| 
        SUBB      XAR4,#4               ; [CPU_U] |305| 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$154, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |305| 
        ; call occurs [#_I2C_Command] ; [] |305| 
        MOV       *-SP[5],AL            ; [CPU_] |305| 
	.dwpsn	file "../param.c",line 306,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |306| 
        BF        $C$L33,NEQ            ; [CPU_] |306| 
        ; branchcc occurs ; [] |306| 
	.dwpsn	file "../param.c",line 307,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |307| 
        MOVB      AL,#2                 ; [CPU_] |307| 
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$155, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |307| 
        ; call occurs [#_SEM_pend] ; [] |307| 
	.dwpsn	file "../param.c",line 308,column 5,is_stmt
        MOVB      AL,#1                 ; [CPU_] |308| 
        MOVB      AH,#4                 ; [CPU_] |308| 
        MOVZ      AR4,SP                ; [CPU_U] |308| 
        MOVL      XAR5,#8188            ; [CPU_] |308| 
        SUBB      XAR4,#4               ; [CPU_U] |308| 
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$156, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |308| 
        ; call occurs [#_I2C_Command] ; [] |308| 
        MOV       *-SP[5],AL            ; [CPU_] |308| 
$C$L33:    
	.dwpsn	file "../param.c",line 310,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |310| 
        BF        $C$L38,EQ             ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
	.dwpsn	file "../param.c",line 311,column 5,is_stmt
        MOV       AL,#12824             ; [CPU_] |311| 
        MOV       AH,#2258              ; [CPU_] |311| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |311| 
        BF        $C$L37,NEQ            ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
	.dwpsn	file "../param.c",line 312,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |312| 
        MOVB      AL,#2                 ; [CPU_] |312| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$157, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |312| 
        ; call occurs [#_SEM_pend] ; [] |312| 
	.dwpsn	file "../param.c",line 313,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |313| 
        MOVL      *-SP[4],ACC           ; [CPU_] |313| 
	.dwpsn	file "../param.c",line 314,column 7,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |314| 
        MOVB      AH,#0                 ; [CPU_] |314| 
        MOV       AL,#8194              ; [CPU_] |314| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |314| 
        SUBB      XAR5,#6               ; [CPU_U] |314| 
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$158, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |314| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |314| 
        CMPB      AL,#0                 ; [CPU_] |314| 
        BF        $C$L34,NEQ            ; [CPU_] |314| 
        ; branchcc occurs ; [] |314| 
	.dwpsn	file "../param.c",line 315,column 9,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |315| 
$C$L34:    
	.dwpsn	file "../param.c",line 317,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |317| 
        MOVL      XAR0,#302             ; [CPU_] |317| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |317| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |317| 
        LSL       ACC,2                 ; [CPU_] |317| 
        MOVZ      AR4,SP                ; [CPU_U] |317| 
        SUBB      XAR4,#10              ; [CPU_U] |317| 
        ADDL      XAR7,ACC              ; [CPU_] |317| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |317| 
	.dwpsn	file "../param.c",line 318,column 7,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |318| 
        MOVZ      AR5,*-SP[7]           ; [CPU_] |318| 
        MOV       AH,*-SP[8]            ; [CPU_] |318| 
        MOVB      AL,#1                 ; [CPU_] |318| 
        SUBB      XAR4,#4               ; [CPU_U] |318| 
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$159, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |318| 
        ; call occurs [#_I2C_Command] ; [] |318| 
        MOV       *-SP[5],AL            ; [CPU_] |318| 
	.dwpsn	file "../param.c",line 319,column 7,is_stmt
        CMPB      AL,#0                 ; [CPU_] |319| 
        BF        $C$L38,EQ             ; [CPU_] |319| 
        ; branchcc occurs ; [] |319| 
	.dwpsn	file "../param.c",line 320,column 9,is_stmt
        MOVW      DP,#_ODP_VersionParameters ; [CPU_U] 
        MOVU      ACC,@_ODP_VersionParameters ; [CPU_] |320| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |320| 
        BF        $C$L36,NEQ            ; [CPU_] |320| 
        ; branchcc occurs ; [] |320| 
	.dwpsn	file "../param.c",line 321,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |321| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_PAR_ReadAllPermanentParam")
	.dwattr $C$DW$160, DW_AT_TI_call
        LCR       #_PAR_ReadAllPermanentParam ; [CPU_] |321| 
        ; call occurs [#_PAR_ReadAllPermanentParam] ; [] |321| 
        MOV       *-SP[5],AL            ; [CPU_] |321| 
	.dwpsn	file "../param.c",line 322,column 11,is_stmt
        CMPB      AL,#0                 ; [CPU_] |322| 
        BF        $C$L35,EQ             ; [CPU_] |322| 
        ; branchcc occurs ; [] |322| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_ComputeParamCRC")
	.dwattr $C$DW$161, DW_AT_TI_call
        LCR       #_ComputeParamCRC     ; [CPU_] |322| 
        ; call occurs [#_ComputeParamCRC] ; [] |322| 
        MOVW      DP,#_ODP_CrcParameters ; [CPU_U] 
        CMPL      ACC,@_ODP_CrcParameters ; [CPU_] |322| 
        BF        $C$L35,NEQ            ; [CPU_] |322| 
        ; branchcc occurs ; [] |322| 
	.dwpsn	file "../param.c",line 323,column 13,is_stmt
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_PAR_ReadAllStatisticParam")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_PAR_ReadAllStatisticParam ; [CPU_] |323| 
        ; call occurs [#_PAR_ReadAllStatisticParam] ; [] |323| 
        MOV       *-SP[5],AL            ; [CPU_] |323| 
        B         $C$L38,UNC            ; [CPU_] |323| 
        ; branch occurs ; [] |323| 
$C$L35:    
	.dwpsn	file "../param.c",line 325,column 13,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |325| 
	.dwpsn	file "../param.c",line 326,column 13,is_stmt
        MOVL      XAR4,#524288          ; [CPU_U] |326| 
        MOVL      ACC,XAR4              ; [CPU_] |326| 
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$163, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |326| 
        ; call occurs [#_ERR_HandleWarning] ; [] |326| 
	.dwpsn	file "../param.c",line 328,column 9,is_stmt
        B         $C$L38,UNC            ; [CPU_] |328| 
        ; branch occurs ; [] |328| 
$C$L36:    
	.dwpsn	file "../param.c",line 330,column 11,is_stmt
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_name("_PAR_ReadAllStatisticParam")
	.dwattr $C$DW$164, DW_AT_TI_call
        LCR       #_PAR_ReadAllStatisticParam ; [CPU_] |330| 
        ; call occurs [#_PAR_ReadAllStatisticParam] ; [] |330| 
        MOV       *-SP[5],AL            ; [CPU_] |330| 
	.dwpsn	file "../param.c",line 331,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |331| 
$C$DW$165	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$165, DW_AT_low_pc(0x00)
	.dwattr $C$DW$165, DW_AT_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$165, DW_AT_TI_call
        LCR       #_PAR_SetDefaultParameters ; [CPU_] |331| 
        ; call occurs [#_PAR_SetDefaultParameters] ; [] |331| 
        MOV       *-SP[5],AL            ; [CPU_] |331| 
	.dwpsn	file "../param.c",line 334,column 5,is_stmt
        B         $C$L38,UNC            ; [CPU_] |334| 
        ; branch occurs ; [] |334| 
$C$L37:    
	.dwpsn	file "../param.c",line 336,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |336| 
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_name("_PAR_SetDefaultParameters")
	.dwattr $C$DW$166, DW_AT_TI_call
        LCR       #_PAR_SetDefaultParameters ; [CPU_] |336| 
        ; call occurs [#_PAR_SetDefaultParameters] ; [] |336| 
        MOV       *-SP[5],AL            ; [CPU_] |336| 
$C$L38:    
	.dwpsn	file "../param.c",line 339,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |339| 
	.dwpsn	file "../param.c",line 340,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$167	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$167, DW_AT_low_pc(0x00)
	.dwattr $C$DW$167, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$147, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$147, DW_AT_TI_end_line(0x154)
	.dwattr $C$DW$147, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$147

	.sect	".text"
	.clink
	.global	_PAR_ReadPermanentParam

$C$DW$168	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_ReadPermanentParam")
	.dwattr $C$DW$168, DW_AT_low_pc(_PAR_ReadPermanentParam)
	.dwattr $C$DW$168, DW_AT_high_pc(0x00)
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$168, DW_AT_external
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$168, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$168, DW_AT_TI_begin_line(0x168)
	.dwattr $C$DW$168, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$168, DW_AT_TI_max_frame_size(-24)
	.dwpsn	file "../param.c",line 361,column 1,is_stmt,address _PAR_ReadPermanentParam

	.dwfde $C$DW$CIE, _PAR_ReadPermanentParam
$C$DW$169	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg12]
$C$DW$170	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_ReadPermanentParam       FR SIZE:  22           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            5 Parameter, 17 Auto,  0 SOE     *
;***************************************************************

_PAR_ReadPermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -24
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_breg20 -8]
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_breg20 -9]
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_breg20 -10]
$C$DW$174	.dwtag  DW_TAG_variable, DW_AT_name("datap")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_breg20 -12]
$C$DW$175	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_breg20 -14]
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -18]
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_breg20 -20]
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_breg20 -21]
$C$DW$179	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$179, DW_AT_location[DW_OP_breg20 -22]
        MOV       *-SP[9],AL            ; [CPU_] |361| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |361| 
	.dwpsn	file "../param.c",line 362,column 9,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |362| 
        MOVL      XAR0,#304             ; [CPU_] |362| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |362| 
        MOV       *-SP[10],AL           ; [CPU_] |362| 
	.dwpsn	file "../param.c",line 369,column 3,is_stmt
        CMP       AL,*-SP[9]            ; [CPU_] |369| 
        B         $C$L44,LEQ            ; [CPU_] |369| 
        ; branchcc occurs ; [] |369| 
	.dwpsn	file "../param.c",line 370,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |370| 
        MOVL      XAR0,#302             ; [CPU_] |370| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |370| 
        MOV       ACC,*-SP[9] << 2      ; [CPU_] |370| 
        MOVZ      AR4,SP                ; [CPU_U] |370| 
        SUBB      XAR4,#18              ; [CPU_U] |370| 
        ADDL      XAR7,ACC              ; [CPU_] |370| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |370| 
	.dwpsn	file "../param.c",line 371,column 5,is_stmt
        MOVU      ACC,*-SP[16]          ; [CPU_] |371| 
        MOVL      *-SP[20],ACC          ; [CPU_] |371| 
	.dwpsn	file "../param.c",line 372,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |372| 
        SUBB      XAR4,#20              ; [CPU_U] |372| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |372| 
        MOV       *-SP[3],#0            ; [CPU_] |372| 
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |372| 
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |372| 
        MOV       AL,*-SP[18]           ; [CPU_] |372| 
        MOV       AH,*-SP[17]           ; [CPU_] |372| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |372| 
        MOVZ      AR5,SP                ; [CPU_U] |372| 
        SUBB      XAR5,#12              ; [CPU_U] |372| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("__setODentry")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #__setODentry         ; [CPU_] |372| 
        ; call occurs [#__setODentry] ; [] |372| 
        MOVL      *-SP[14],ACC          ; [CPU_] |372| 
	.dwpsn	file "../param.c",line 373,column 5,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |373| 
        BF        $C$L45,NEQ            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
	.dwpsn	file "../param.c",line 374,column 7,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |374| 
	.dwpsn	file "../param.c",line 375,column 7,is_stmt
        MOV       *-SP[22],#0           ; [CPU_] |375| 
	.dwpsn	file "../param.c",line 376,column 7,is_stmt
        B         $C$L41,UNC            ; [CPU_] |376| 
        ; branch occurs ; [] |376| 
$C$L39:    
	.dwpsn	file "../param.c",line 377,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |377| 
        MOVZ      AR5,*-SP[15]          ; [CPU_] |377| 
        MOV       AH,*-SP[16]           ; [CPU_] |377| 
        MOVB      AL,#1                 ; [CPU_] |377| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |377| 
        ; call occurs [#_I2C_Command] ; [] |377| 
        MOV       *-SP[22],AL           ; [CPU_] |377| 
	.dwpsn	file "../param.c",line 378,column 9,is_stmt
        CMPB      AL,#0                 ; [CPU_] |378| 
        BF        $C$L40,NEQ            ; [CPU_] |378| 
        ; branchcc occurs ; [] |378| 
	.dwpsn	file "../param.c",line 379,column 11,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |379| 
        MOVB      AL,#30                ; [CPU_] |379| 
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$182, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |379| 
        ; call occurs [#_SEM_pend] ; [] |379| 
$C$L40:    
	.dwpsn	file "../param.c",line 381,column 9,is_stmt
        INC       *-SP[21]              ; [CPU_] |381| 
$C$L41:    
	.dwpsn	file "../param.c",line 376,column 14,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |376| 
        CMPB      AL,#3                 ; [CPU_] |376| 
        B         $C$L42,HIS            ; [CPU_] |376| 
        ; branchcc occurs ; [] |376| 
        MOV       AL,*-SP[22]           ; [CPU_] |376| 
        BF        $C$L39,EQ             ; [CPU_] |376| 
        ; branchcc occurs ; [] |376| 
$C$L42:    
	.dwpsn	file "../param.c",line 383,column 7,is_stmt
        MOV       AL,*-SP[22]           ; [CPU_] |383| 
        BF        $C$L43,NEQ            ; [CPU_] |383| 
        ; branchcc occurs ; [] |383| 
	.dwpsn	file "../param.c",line 384,column 9,is_stmt
        MOV       AL,#32                ; [CPU_] |384| 
        MOV       AH,#2048              ; [CPU_] |384| 
        MOVL      *-SP[14],ACC          ; [CPU_] |384| 
        B         $C$L45,UNC            ; [CPU_] |384| 
        ; branch occurs ; [] |384| 
$C$L43:    
	.dwpsn	file "../param.c",line 386,column 9,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |386| 
        SUBB      XAR4,#20              ; [CPU_U] |386| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |386| 
        MOV       *-SP[3],#0            ; [CPU_] |386| 
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |386| 
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |386| 
        MOV       AL,*-SP[18]           ; [CPU_] |386| 
        MOV       AH,*-SP[17]           ; [CPU_] |386| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |386| 
        MOVZ      AR5,SP                ; [CPU_U] |386| 
        SUBB      XAR5,#12              ; [CPU_U] |386| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("__setODentry")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #__setODentry         ; [CPU_] |386| 
        ; call occurs [#__setODentry] ; [] |386| 
        MOVL      *-SP[14],ACC          ; [CPU_] |386| 
	.dwpsn	file "../param.c",line 389,column 3,is_stmt
        B         $C$L45,UNC            ; [CPU_] |389| 
        ; branch occurs ; [] |389| 
$C$L44:    
	.dwpsn	file "../param.c",line 391,column 5,is_stmt
        MOV       AL,#32                ; [CPU_] |391| 
        MOV       AH,#2048              ; [CPU_] |391| 
        MOVL      *-SP[14],ACC          ; [CPU_] |391| 
$C$L45:    
	.dwpsn	file "../param.c",line 392,column 3,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |392| 
	.dwpsn	file "../param.c",line 393,column 1,is_stmt
        SUBB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$168, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$168, DW_AT_TI_end_line(0x189)
	.dwattr $C$DW$168, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$168

	.sect	".text"
	.clink
	.global	_PAR_ReadAllPermanentParam

$C$DW$185	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_ReadAllPermanentParam")
	.dwattr $C$DW$185, DW_AT_low_pc(_PAR_ReadAllPermanentParam)
	.dwattr $C$DW$185, DW_AT_high_pc(0x00)
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_PAR_ReadAllPermanentParam")
	.dwattr $C$DW$185, DW_AT_external
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$185, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$185, DW_AT_TI_begin_line(0x19a)
	.dwattr $C$DW$185, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$185, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 411,column 1,is_stmt,address _PAR_ReadAllPermanentParam

	.dwfde $C$DW$CIE, _PAR_ReadAllPermanentParam
$C$DW$186	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_ReadAllPermanentParam    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_PAR_ReadAllPermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$187	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$187, DW_AT_location[DW_OP_breg20 -2]
$C$DW$188	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$188, DW_AT_location[DW_OP_breg20 -3]
$C$DW$189	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$189, DW_AT_location[DW_OP_breg20 -4]
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],XAR4          ; [CPU_] |411| 
	.dwpsn	file "../param.c",line 412,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |412| 
        MOVL      XAR0,#304             ; [CPU_] |412| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |412| 
        MOV       *-SP[3],AL            ; [CPU_] |412| 
	.dwpsn	file "../param.c",line 413,column 13,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |413| 
	.dwpsn	file "../param.c",line 414,column 21,is_stmt
        MOVB      ACC,#0                ; [CPU_] |414| 
        MOVL      *-SP[6],ACC           ; [CPU_] |414| 
	.dwpsn	file "../param.c",line 415,column 3,is_stmt
        B         $C$L47,UNC            ; [CPU_] |415| 
        ; branch occurs ; [] |415| 
$C$L46:    
	.dwpsn	file "../param.c",line 416,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |416| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |416| 
        INC       *-SP[4]               ; [CPU_] |416| 
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("_PAR_ReadPermanentParam")
	.dwattr $C$DW$191, DW_AT_TI_call
        LCR       #_PAR_ReadPermanentParam ; [CPU_] |416| 
        ; call occurs [#_PAR_ReadPermanentParam] ; [] |416| 
        MOVL      *-SP[6],ACC           ; [CPU_] |416| 
$C$L47:    
	.dwpsn	file "../param.c",line 415,column 9,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |415| 
        CMP       AL,*-SP[4]            ; [CPU_] |415| 
        B         $C$L48,LEQ            ; [CPU_] |415| 
        ; branchcc occurs ; [] |415| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |415| 
        BF        $C$L46,EQ             ; [CPU_] |415| 
        ; branchcc occurs ; [] |415| 
$C$L48:    
	.dwpsn	file "../param.c",line 418,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |418| 
        MOVB      XAR6,#0               ; [CPU_] |418| 
        BF        $C$L49,NEQ            ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
        MOVB      XAR6,#1               ; [CPU_] |418| 
$C$L49:    
        MOV       AL,AR6                ; [CPU_] |418| 
	.dwpsn	file "../param.c",line 419,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$185, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$185, DW_AT_TI_end_line(0x1a3)
	.dwattr $C$DW$185, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$185

	.sect	".text"
	.clink
	.global	_WritePermanentParam

$C$DW$193	.dwtag  DW_TAG_subprogram, DW_AT_name("WritePermanentParam")
	.dwattr $C$DW$193, DW_AT_low_pc(_WritePermanentParam)
	.dwattr $C$DW$193, DW_AT_high_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_WritePermanentParam")
	.dwattr $C$DW$193, DW_AT_external
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$193, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$193, DW_AT_TI_begin_line(0x1b7)
	.dwattr $C$DW$193, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$193, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../param.c",line 440,column 1,is_stmt,address _WritePermanentParam

	.dwfde $C$DW$CIE, _WritePermanentParam
$C$DW$194	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg12]
$C$DW$195	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _WritePermanentParam          FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter, 17 Auto,  0 SOE     *
;***************************************************************

_WritePermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -10]
$C$DW$197	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$197, DW_AT_location[DW_OP_breg20 -11]
$C$DW$198	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_breg20 -16]
$C$DW$199	.dwtag  DW_TAG_variable, DW_AT_name("datap")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_breg20 -18]
$C$DW$200	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_breg20 -20]
$C$DW$201	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_breg20 -22]
$C$DW$202	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_breg20 -23]
$C$DW$203	.dwtag  DW_TAG_variable, DW_AT_name("data_type")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_data_type")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_breg20 -24]
        MOV       *-SP[11],AL           ; [CPU_] |440| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |440| 
	.dwpsn	file "../param.c",line 448,column 3,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |448| 
        MOVL      XAR0,#304             ; [CPU_] |448| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |448| 
        CMP       AL,*-SP[11]           ; [CPU_] |448| 
        B         $C$L50,LOS            ; [CPU_] |448| 
        ; branchcc occurs ; [] |448| 
	.dwpsn	file "../param.c",line 449,column 5,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |449| 
        MOVL      XAR0,#302             ; [CPU_] |449| 
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |449| 
        MOV       ACC,*-SP[11] << 2     ; [CPU_] |449| 
        MOVZ      AR4,SP                ; [CPU_U] |449| 
        SUBB      XAR4,#16              ; [CPU_U] |449| 
        ADDL      XAR7,ACC              ; [CPU_] |449| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |449| 
	.dwpsn	file "../param.c",line 450,column 5,is_stmt
        MOVU      ACC,*-SP[14]          ; [CPU_] |450| 
        MOVL      *-SP[20],ACC          ; [CPU_] |450| 
	.dwpsn	file "../param.c",line 451,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |451| 
        MOVZ      AR4,SP                ; [CPU_U] |451| 
        SUBB      XAR5,#20              ; [CPU_U] |451| 
        SUBB      XAR4,#24              ; [CPU_U] |451| 
        MOVU      ACC,AR5               ; [CPU_] |451| 
        MOVL      *-SP[2],ACC           ; [CPU_] |451| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |451| 
        MOV       *-SP[5],#0            ; [CPU_] |451| 
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |451| 
        MOVB      *-SP[7],#4,UNC        ; [CPU_] |451| 
        MOV       AH,*-SP[15]           ; [CPU_] |451| 
        MOV       AL,*-SP[16]           ; [CPU_] |451| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |451| 
        MOVZ      AR5,SP                ; [CPU_U] |451| 
        SUBB      XAR5,#18              ; [CPU_U] |451| 
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("__getODentry")
	.dwattr $C$DW$204, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |451| 
        ; call occurs [#__getODentry] ; [] |451| 
        MOVL      *-SP[22],ACC          ; [CPU_] |451| 
	.dwpsn	file "../param.c",line 453,column 5,is_stmt
        MOVL      ACC,*-SP[22]          ; [CPU_] |453| 
        BF        $C$L51,NEQ            ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
	.dwpsn	file "../param.c",line 454,column 7,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |454| 
        MOVZ      AR5,*-SP[13]          ; [CPU_] |454| 
        MOV       AH,*-SP[14]           ; [CPU_] |454| 
        MOVB      AL,#0                 ; [CPU_] |454| 
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$205, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |454| 
        ; call occurs [#_I2C_Command] ; [] |454| 
        MOV       *-SP[23],AL           ; [CPU_] |454| 
	.dwpsn	file "../param.c",line 455,column 7,is_stmt
        CMPB      AL,#0                 ; [CPU_] |455| 
        BF        $C$L51,NEQ            ; [CPU_] |455| 
        ; branchcc occurs ; [] |455| 
	.dwpsn	file "../param.c",line 456,column 9,is_stmt
        MOV       AL,#32                ; [CPU_] |456| 
        MOV       AH,#2048              ; [CPU_] |456| 
        MOVL      *-SP[22],ACC          ; [CPU_] |456| 
	.dwpsn	file "../param.c",line 458,column 3,is_stmt
        B         $C$L51,UNC            ; [CPU_] |458| 
        ; branch occurs ; [] |458| 
$C$L50:    
	.dwpsn	file "../param.c",line 460,column 5,is_stmt
        MOV       AL,#5                 ; [CPU_] |460| 
        MOV       AH,#1284              ; [CPU_] |460| 
        MOVL      *-SP[22],ACC          ; [CPU_] |460| 
$C$L51:    
	.dwpsn	file "../param.c",line 461,column 3,is_stmt
        MOVL      ACC,*-SP[22]          ; [CPU_] |461| 
	.dwpsn	file "../param.c",line 462,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$206	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$206, DW_AT_low_pc(0x00)
	.dwattr $C$DW$206, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$193, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$193, DW_AT_TI_end_line(0x1ce)
	.dwattr $C$DW$193, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$193

	.sect	".text"
	.clink
	.global	_PAR_WriteAllPermanentParam

$C$DW$207	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$207, DW_AT_low_pc(_PAR_WriteAllPermanentParam)
	.dwattr $C$DW$207, DW_AT_high_pc(0x00)
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$207, DW_AT_external
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$207, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$207, DW_AT_TI_begin_line(0x1d1)
	.dwattr $C$DW$207, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$207, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 465,column 46,is_stmt,address _PAR_WriteAllPermanentParam

	.dwfde $C$DW$CIE, _PAR_WriteAllPermanentParam
$C$DW$208	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PAR_WriteAllPermanentParam   FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_PAR_WriteAllPermanentParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -2]
$C$DW$210	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_breg20 -3]
$C$DW$211	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$211, DW_AT_location[DW_OP_breg20 -4]
$C$DW$212	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],XAR4          ; [CPU_] |465| 
	.dwpsn	file "../param.c",line 466,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |466| 
        MOVL      XAR0,#304             ; [CPU_] |466| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |466| 
        MOV       *-SP[3],AL            ; [CPU_] |466| 
	.dwpsn	file "../param.c",line 467,column 13,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |467| 
	.dwpsn	file "../param.c",line 468,column 21,is_stmt
        MOVB      ACC,#0                ; [CPU_] |468| 
        MOVL      *-SP[6],ACC           ; [CPU_] |468| 
	.dwpsn	file "../param.c",line 470,column 3,is_stmt
        B         $C$L53,UNC            ; [CPU_] |470| 
        ; branch occurs ; [] |470| 
$C$L52:    
	.dwpsn	file "../param.c",line 471,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |471| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |471| 
        INC       *-SP[4]               ; [CPU_] |471| 
$C$DW$213	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$213, DW_AT_low_pc(0x00)
	.dwattr $C$DW$213, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$213, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |471| 
        ; call occurs [#_WritePermanentParam] ; [] |471| 
        MOVL      *-SP[6],ACC           ; [CPU_] |471| 
$C$L53:    
	.dwpsn	file "../param.c",line 470,column 9,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |470| 
        CMP       AL,*-SP[4]            ; [CPU_] |470| 
        B         $C$L54,LEQ            ; [CPU_] |470| 
        ; branchcc occurs ; [] |470| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |470| 
        BF        $C$L52,EQ             ; [CPU_] |470| 
        ; branchcc occurs ; [] |470| 
$C$L54:    
	.dwpsn	file "../param.c",line 473,column 3,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |473| 
	.dwpsn	file "../param.c",line 474,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$207, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$207, DW_AT_TI_end_line(0x1da)
	.dwattr $C$DW$207, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$207

	.sect	".text"
	.clink
	.global	_WriteStatisticParam

$C$DW$215	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteStatisticParam")
	.dwattr $C$DW$215, DW_AT_low_pc(_WriteStatisticParam)
	.dwattr $C$DW$215, DW_AT_high_pc(0x00)
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_WriteStatisticParam")
	.dwattr $C$DW$215, DW_AT_external
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$215, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$215, DW_AT_TI_begin_line(0x1e9)
	.dwattr $C$DW$215, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$215, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../param.c",line 489,column 40,is_stmt,address _WriteStatisticParam

	.dwfde $C$DW$CIE, _WriteStatisticParam
$C$DW$216	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _WriteStatisticParam          FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 25 Auto,  0 SOE     *
;***************************************************************

_WriteStatisticParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -28
$C$DW$217	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_breg20 -1]
$C$DW$218	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_breg20 -2]
$C$DW$219	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$219, DW_AT_location[DW_OP_breg20 -4]
$C$DW$220	.dwtag  DW_TAG_variable, DW_AT_name("buff")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_buff")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_breg20 -20]
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_breg20 -22]
$C$DW$222	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_breg20 -23]
$C$DW$223	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_breg20 -24]
$C$DW$224	.dwtag  DW_TAG_variable, DW_AT_name("k")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_breg20 -25]
        MOV       *-SP[1],AL            ; [CPU_] |489| 
	.dwpsn	file "../param.c",line 491,column 21,is_stmt
        MOV       AH,#2048              ; [CPU_] |491| 
        MOV       AL,#32                ; [CPU_] |491| 
        MOVL      *-SP[4],ACC           ; [CPU_] |491| 
	.dwpsn	file "../param.c",line 495,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |495| 
        B         $C$L55,GEQ            ; [CPU_] |495| 
        ; branchcc occurs ; [] |495| 
	.dwpsn	file "../param.c",line 495,column 18,is_stmt
        NOT       AL                    ; [CPU_] |495| 
        MOV       *-SP[1],AL            ; [CPU_] |495| 
$C$L55:    
	.dwpsn	file "../param.c",line 496,column 3,is_stmt
        CMPB      AL,#5                 ; [CPU_] |496| 
        B         $C$L61,GEQ            ; [CPU_] |496| 
        ; branchcc occurs ; [] |496| 
	.dwpsn	file "../param.c",line 497,column 5,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |497| 
	.dwpsn	file "../param.c",line 497,column 9,is_stmt
        MOV       *-SP[25],#0           ; [CPU_] |497| 
	.dwpsn	file "../param.c",line 498,column 5,is_stmt
        B         $C$L59,UNC            ; [CPU_] |498| 
        ; branch occurs ; [] |498| 
$C$L56:    
	.dwpsn	file "../param.c",line 499,column 7,is_stmt
        MPYB      ACC,T,#6              ; [CPU_] |499| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |499| 
        ADDL      XAR4,ACC              ; [CPU_] |499| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |499| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[23] << 2     ; [CPU_] |499| 
        ADDL      XAR4,ACC              ; [CPU_] |499| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |499| 
        MOVL      *-SP[22],ACC          ; [CPU_] |499| 
	.dwpsn	file "../param.c",line 500,column 7,is_stmt
        MOV       *-SP[24],#0           ; [CPU_] |500| 
	.dwpsn	file "../param.c",line 501,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        B         $C$L58,UNC            ; [CPU_] |501| 
        ; branch occurs ; [] |501| 
$C$L57:    
	.dwpsn	file "../param.c",line 502,column 9,is_stmt
        MOVZ      AR7,*-SP[25]          ; [CPU_] |502| 
        MOVZ      AR4,SP                ; [CPU_U] |502| 
        MOVL      XAR5,*-SP[22]         ; [CPU_] |502| 
        MOV       ACC,AR7               ; [CPU_] |502| 
        MOVL      P,ACC                 ; [CPU_] |502| 
        SUBB      XAR4,#20              ; [CPU_U] |502| 
        MOVB      AL,#1                 ; [CPU_] |502| 
        ADD       AL,AR7                ; [CPU_] |502| 
        MOVZ      AR7,*XAR5++           ; [CPU_] |502| 
        MOV       *-SP[25],AL           ; [CPU_] |502| 
        MOVL      *-SP[22],XAR5         ; [CPU_] |502| 
        MOVL      ACC,P                 ; [CPU_] |502| 
        ADDL      XAR4,ACC              ; [CPU_] |502| 
        MOV       *+XAR4[0],AR7         ; [CPU_] |502| 
	.dwpsn	file "../param.c",line 503,column 9,is_stmt
        ADD       *-SP[24],#2           ; [CPU_] |503| 
$C$L58:    
	.dwpsn	file "../param.c",line 501,column 14,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |501| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |501| 
        MPYB      ACC,T,#6              ; [CPU_] |501| 
        ADDL      XAR4,ACC              ; [CPU_] |501| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |501| 
        MOV       ACC,*-SP[23] << 2     ; [CPU_] |501| 
        ADDL      XAR4,ACC              ; [CPU_] |501| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |501| 
        CMP       AL,*-SP[24]           ; [CPU_] |501| 
        B         $C$L57,HI             ; [CPU_] |501| 
        ; branchcc occurs ; [] |501| 
	.dwpsn	file "../param.c",line 505,column 7,is_stmt
        INC       *-SP[23]              ; [CPU_] |505| 
$C$L59:    
	.dwpsn	file "../param.c",line 498,column 12,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |498| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+2 ; [CPU_U] |498| 
        MPYB      ACC,T,#6              ; [CPU_] |498| 
        ADDL      XAR4,ACC              ; [CPU_] |498| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |498| 
        CMP       AL,*-SP[23]           ; [CPU_] |498| 
        B         $C$L56,HI             ; [CPU_] |498| 
        ; branchcc occurs ; [] |498| 
	.dwpsn	file "../param.c",line 507,column 5,is_stmt
        MPYB      ACC,T,#6              ; [CPU_] |507| 
        MOVL      XAR6,#_PAR_EEPROM_INDEXES ; [CPU_U] |507| 
        MOVZ      AR4,SP                ; [CPU_U] |507| 
        MOVL      XAR5,#_PAR_EEPROM_INDEXES+1 ; [CPU_U] |507| 
        ADDL      XAR6,ACC              ; [CPU_] |507| 
        SUBB      XAR4,#20              ; [CPU_U] |507| 
        MPYB      ACC,T,#6              ; [CPU_] |507| 
        ADDL      XAR5,ACC              ; [CPU_] |507| 
        MOVB      AL,#0                 ; [CPU_] |507| 
        MOVZ      AR5,*+XAR5[0]         ; [CPU_] |507| 
        MOV       AH,*+XAR6[0]          ; [CPU_] |507| 
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$225, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |507| 
        ; call occurs [#_I2C_Command] ; [] |507| 
        MOV       *-SP[2],AL            ; [CPU_] |507| 
	.dwpsn	file "../param.c",line 509,column 5,is_stmt
        CMPB      AL,#0                 ; [CPU_] |509| 
        BF        $C$L60,EQ             ; [CPU_] |509| 
        ; branchcc occurs ; [] |509| 
	.dwpsn	file "../param.c",line 509,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |509| 
        MOVL      *-SP[4],ACC           ; [CPU_] |509| 
$C$L60:    
	.dwpsn	file "../param.c",line 510,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_PAR_StatEepromCommandState ; [CPU_U] |510| 
        MOV       ACC,*-SP[1]           ; [CPU_] |510| 
        ADDL      XAR4,ACC              ; [CPU_] |510| 
        MOVB      *+XAR4[0],#2,UNC      ; [CPU_] |510| 
$C$L61:    
	.dwpsn	file "../param.c",line 512,column 3,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |512| 
	.dwpsn	file "../param.c",line 513,column 1,is_stmt
        SUBB      SP,#26                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$215, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$215, DW_AT_TI_end_line(0x201)
	.dwattr $C$DW$215, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$215

	.sect	".text"
	.clink
	.global	_PAR_WriteStatisticParam

$C$DW$227	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$227, DW_AT_low_pc(_PAR_WriteStatisticParam)
	.dwattr $C$DW$227, DW_AT_high_pc(0x00)
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$227, DW_AT_external
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$227, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$227, DW_AT_TI_begin_line(0x203)
	.dwattr $C$DW$227, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$227, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../param.c",line 515,column 44,is_stmt,address _PAR_WriteStatisticParam

	.dwfde $C$DW$CIE, _PAR_WriteStatisticParam
$C$DW$228	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PAR_WriteStatisticParam      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_PAR_WriteStatisticParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$229	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$229, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |515| 
	.dwpsn	file "../param.c",line 516,column 3,is_stmt
        MOVL      XAR4,#_mailboxWriteParameters ; [CPU_U] |516| 
        MOVZ      AR5,SP                ; [CPU_U] |516| 
        MOVB      AL,#0                 ; [CPU_] |516| 
        SUBB      XAR5,#1               ; [CPU_U] |516| 
$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_MBX_post")
	.dwattr $C$DW$230, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |516| 
        ; call occurs [#_MBX_post] ; [] |516| 
        MOVU      ACC,AL                ; [CPU_] |516| 
	.dwpsn	file "../param.c",line 517,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$227, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$227, DW_AT_TI_end_line(0x205)
	.dwattr $C$DW$227, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$227

	.sect	".text"
	.clink
	.global	_PAR_ReadAllStatisticParam

$C$DW$232	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_ReadAllStatisticParam")
	.dwattr $C$DW$232, DW_AT_low_pc(_PAR_ReadAllStatisticParam)
	.dwattr $C$DW$232, DW_AT_high_pc(0x00)
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_PAR_ReadAllStatisticParam")
	.dwattr $C$DW$232, DW_AT_external
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$232, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$232, DW_AT_TI_begin_line(0x20f)
	.dwattr $C$DW$232, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$232, DW_AT_TI_max_frame_size(-26)
	.dwpsn	file "../param.c",line 527,column 38,is_stmt,address _PAR_ReadAllStatisticParam

	.dwfde $C$DW$CIE, _PAR_ReadAllStatisticParam

;***************************************************************
;* FNAME: _PAR_ReadAllStatisticParam    FR SIZE:  24           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 23 Auto,  0 SOE     *
;***************************************************************

_PAR_ReadAllStatisticParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
$C$DW$233	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$233, DW_AT_location[DW_OP_breg20 -1]
$C$DW$234	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$234, DW_AT_location[DW_OP_breg20 -2]
$C$DW$235	.dwtag  DW_TAG_variable, DW_AT_name("buff")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_buff")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_breg20 -18]
$C$DW$236	.dwtag  DW_TAG_variable, DW_AT_name("pdata")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_breg20 -20]
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -21]
$C$DW$238	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -22]
$C$DW$239	.dwtag  DW_TAG_variable, DW_AT_name("k")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_k")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_breg20 -23]
	.dwpsn	file "../param.c",line 528,column 13,is_stmt
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |528| 
	.dwpsn	file "../param.c",line 529,column 16,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |529| 
	.dwpsn	file "../param.c",line 533,column 3,is_stmt
        B         $C$L67,UNC            ; [CPU_] |533| 
        ; branch occurs ; [] |533| 
$C$L62:    
	.dwpsn	file "../param.c",line 534,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |534| 
        MOVZ      AR4,SP                ; [CPU_U] |534| 
        MOVL      XAR6,#_PAR_EEPROM_INDEXES ; [CPU_U] |534| 
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |534| 
        MOVL      XAR5,#_PAR_EEPROM_INDEXES+1 ; [CPU_U] |534| 
        SUBB      XAR4,#18              ; [CPU_U] |534| 
        ADDL      XAR6,ACC              ; [CPU_] |534| 
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |534| 
        ADDL      XAR5,ACC              ; [CPU_] |534| 
        MOVB      AL,#1                 ; [CPU_] |534| 
        MOVZ      AR5,*+XAR5[0]         ; [CPU_] |534| 
        MOV       AH,*+XAR6[0]          ; [CPU_] |534| 
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$240, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |534| 
        ; call occurs [#_I2C_Command] ; [] |534| 
        MOV       *-SP[1],AL            ; [CPU_] |534| 
	.dwpsn	file "../param.c",line 536,column 5,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |536| 
	.dwpsn	file "../param.c",line 536,column 9,is_stmt
        MOV       *-SP[23],#0           ; [CPU_] |536| 
	.dwpsn	file "../param.c",line 537,column 5,is_stmt
        MOV       T,#6                  ; [CPU_] |537| 
        B         $C$L66,UNC            ; [CPU_] |537| 
        ; branch occurs ; [] |537| 
$C$L63:    
	.dwpsn	file "../param.c",line 538,column 7,is_stmt
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |538| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |538| 
        ADDL      XAR4,ACC              ; [CPU_] |538| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |538| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[21] << 2     ; [CPU_] |538| 
        ADDL      XAR4,ACC              ; [CPU_] |538| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |538| 
        MOVL      *-SP[20],ACC          ; [CPU_] |538| 
	.dwpsn	file "../param.c",line 539,column 7,is_stmt
        MOV       *-SP[22],#0           ; [CPU_] |539| 
	.dwpsn	file "../param.c",line 540,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        B         $C$L65,UNC            ; [CPU_] |540| 
        ; branch occurs ; [] |540| 
$C$L64:    
	.dwpsn	file "../param.c",line 541,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |541| 
        MOVB      AH,#1                 ; [CPU_] |541| 
        MOVL      XAR4,*-SP[20]         ; [CPU_] |541| 
        MOVZ      AR7,SP                ; [CPU_U] |541| 
        ADD       AH,AL                 ; [CPU_] |541| 
        SUBB      XAR7,#18              ; [CPU_U] |541| 
        MOVL      XAR5,XAR4             ; [CPU_] |541| 
        MOV       *-SP[23],AH           ; [CPU_] |541| 
        MOV       ACC,AL                ; [CPU_] |541| 
        ADDL      XAR7,ACC              ; [CPU_] |541| 
        ADDB      XAR5,#1               ; [CPU_] |541| 
        MOVL      *-SP[20],XAR5         ; [CPU_] |541| 
        MOV       AL,*XAR7              ; [CPU_] |541| 
        MOV       *+XAR4[0],AL          ; [CPU_] |541| 
	.dwpsn	file "../param.c",line 542,column 9,is_stmt
        ADD       *-SP[22],#2           ; [CPU_] |542| 
$C$L65:    
	.dwpsn	file "../param.c",line 540,column 14,is_stmt
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |540| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+4 ; [CPU_U] |540| 
        ADDL      XAR4,ACC              ; [CPU_] |540| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |540| 
        MOV       ACC,*-SP[21] << 2     ; [CPU_] |540| 
        ADDL      XAR4,ACC              ; [CPU_] |540| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |540| 
        CMP       AL,*-SP[22]           ; [CPU_] |540| 
        B         $C$L64,HI             ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
	.dwpsn	file "../param.c",line 544,column 7,is_stmt
        INC       *-SP[21]              ; [CPU_] |544| 
$C$L66:    
	.dwpsn	file "../param.c",line 537,column 12,is_stmt
        MPYXU     ACC,T,*-SP[2]         ; [CPU_] |537| 
        MOVL      XAR4,#_PAR_EEPROM_INDEXES+2 ; [CPU_U] |537| 
        ADDL      XAR4,ACC              ; [CPU_] |537| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |537| 
        CMP       AL,*-SP[21]           ; [CPU_] |537| 
        B         $C$L63,HI             ; [CPU_] |537| 
        ; branchcc occurs ; [] |537| 
	.dwpsn	file "../param.c",line 546,column 5,is_stmt
        INC       *-SP[2]               ; [CPU_] |546| 
$C$L67:    
	.dwpsn	file "../param.c",line 533,column 10,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |533| 
        CMPB      AL,#5                 ; [CPU_] |533| 
        B         $C$L68,HIS            ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
        MOV       AL,*-SP[1]            ; [CPU_] |533| 
        BF        $C$L62,NEQ            ; [CPU_] |533| 
        ; branchcc occurs ; [] |533| 
$C$L68:    
	.dwpsn	file "../param.c",line 548,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |548| 
	.dwpsn	file "../param.c",line 549,column 1,is_stmt
        SUBB      SP,#24                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$232, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$232, DW_AT_TI_end_line(0x225)
	.dwattr $C$DW$232, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$232

	.sect	".text"
	.clink
	.global	_PAR_FindODPermanentParamIndex

$C$DW$242	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$242, DW_AT_low_pc(_PAR_FindODPermanentParamIndex)
	.dwattr $C$DW$242, DW_AT_high_pc(0x00)
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$242, DW_AT_external
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$242, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$242, DW_AT_TI_begin_line(0x242)
	.dwattr $C$DW$242, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$242, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../param.c",line 579,column 1,is_stmt,address _PAR_FindODPermanentParamIndex

	.dwfde $C$DW$CIE, _PAR_FindODPermanentParamIndex
$C$DW$243	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_reg12]
$C$DW$244	.dwtag  DW_TAG_formal_parameter, DW_AT_name("index")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_reg0]
$C$DW$245	.dwtag  DW_TAG_formal_parameter, DW_AT_name("subindex")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_reg1]
$C$DW$246	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array_index")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_FindODPermanentParamIndex FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_PAR_FindODPermanentParamIndex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$247	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_breg20 -2]
$C$DW$248	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_breg20 -3]
$C$DW$249	.dwtag  DW_TAG_variable, DW_AT_name("subindex")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_breg20 -4]
$C$DW$250	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_breg20 -6]
$C$DW$251	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -7]
$C$DW$252	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_breg20 -8]
$C$DW$253	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_breg20 -9]
$C$DW$254	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[4],AH            ; [CPU_] |579| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |579| 
        MOV       *-SP[3],AL            ; [CPU_] |579| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |579| 
	.dwpsn	file "../param.c",line 580,column 12,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |580| 
        MOVL      XAR0,#304             ; [CPU_] |580| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |580| 
        MOV       *-SP[7],AL            ; [CPU_] |580| 
	.dwpsn	file "../param.c",line 581,column 12,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |581| 
	.dwpsn	file "../param.c",line 582,column 16,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |582| 
	.dwpsn	file "../param.c",line 585,column 3,is_stmt
        MOVL      XAR0,#302             ; [CPU_] |586| 
        B         $C$L72,UNC            ; [CPU_] |585| 
        ; branch occurs ; [] |585| 
$C$L69:    
	.dwpsn	file "../param.c",line 586,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |586| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |586| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |586| 
        LSL       ACC,2                 ; [CPU_] |586| 
        MOVZ      AR4,SP                ; [CPU_U] |586| 
        SUBB      XAR4,#14              ; [CPU_U] |586| 
        ADDL      XAR7,ACC              ; [CPU_] |586| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |586| 
	.dwpsn	file "../param.c",line 587,column 5,is_stmt
        MOVZ      AR7,*-SP[14]          ; [CPU_] |587| 
        MOVB      XAR6,#0               ; [CPU_] |587| 
        MOVU      ACC,*-SP[3]           ; [CPU_] |587| 
        CMPL      ACC,XAR7              ; [CPU_] |587| 
        BF        $C$L70,NEQ            ; [CPU_] |587| 
        ; branchcc occurs ; [] |587| 
        MOVZ      AR7,*-SP[13]          ; [CPU_] |587| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |587| 
        CMPL      ACC,XAR7              ; [CPU_] |587| 
        BF        $C$L70,NEQ            ; [CPU_] |587| 
        ; branchcc occurs ; [] |587| 
        MOVB      XAR6,#1               ; [CPU_] |587| 
$C$L70:    
        MOV       *-SP[9],AR6           ; [CPU_] |587| 
	.dwpsn	file "../param.c",line 588,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |588| 
        BF        $C$L71,EQ             ; [CPU_] |588| 
        ; branchcc occurs ; [] |588| 
	.dwpsn	file "../param.c",line 589,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |589| 
        MOV       AL,*-SP[8]            ; [CPU_] |589| 
        MOV       *+XAR4[0],AL          ; [CPU_] |589| 
        B         $C$L72,UNC            ; [CPU_] |589| 
        ; branch occurs ; [] |589| 
$C$L71:    
	.dwpsn	file "../param.c",line 591,column 7,is_stmt
        INC       *-SP[8]               ; [CPU_] |591| 
$C$L72:    
	.dwpsn	file "../param.c",line 585,column 10,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |585| 
        CMP       AL,*-SP[8]            ; [CPU_] |585| 
        B         $C$L73,LOS            ; [CPU_] |585| 
        ; branchcc occurs ; [] |585| 
        MOV       AL,*-SP[9]            ; [CPU_] |585| 
        BF        $C$L69,EQ             ; [CPU_] |585| 
        ; branchcc occurs ; [] |585| 
$C$L73:    
	.dwpsn	file "../param.c",line 593,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |593| 
	.dwpsn	file "../param.c",line 594,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$255	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$255, DW_AT_low_pc(0x00)
	.dwattr $C$DW$255, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$242, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$242, DW_AT_TI_end_line(0x252)
	.dwattr $C$DW$242, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$242

	.sect	".text"
	.clink
	.global	_PAR_FindODPermanentParamIndex2

$C$DW$256	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_FindODPermanentParamIndex2")
	.dwattr $C$DW$256, DW_AT_low_pc(_PAR_FindODPermanentParamIndex2)
	.dwattr $C$DW$256, DW_AT_high_pc(0x00)
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_PAR_FindODPermanentParamIndex2")
	.dwattr $C$DW$256, DW_AT_external
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$256, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$256, DW_AT_TI_begin_line(0x26a)
	.dwattr $C$DW$256, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$256, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../param.c",line 619,column 1,is_stmt,address _PAR_FindODPermanentParamIndex2

	.dwfde $C$DW$CIE, _PAR_FindODPermanentParamIndex2
$C$DW$257	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$257, DW_AT_location[DW_OP_reg12]
$C$DW$258	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_reg0]
$C$DW$259	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array_index")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_FindODPermanentParamIndex2 FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_PAR_FindODPermanentParamIndex2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$260	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_breg20 -2]
$C$DW$261	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_breg20 -3]
$C$DW$262	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$262, DW_AT_location[DW_OP_breg20 -6]
$C$DW$263	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$263, DW_AT_location[DW_OP_breg20 -7]
$C$DW$264	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_breg20 -8]
$C$DW$265	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_breg20 -9]
$C$DW$266	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$266, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[3],AL            ; [CPU_] |619| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |619| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |619| 
	.dwpsn	file "../param.c",line 620,column 12,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |620| 
        MOVL      XAR0,#304             ; [CPU_] |620| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |620| 
        MOV       *-SP[7],AL            ; [CPU_] |620| 
	.dwpsn	file "../param.c",line 621,column 12,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |621| 
	.dwpsn	file "../param.c",line 622,column 16,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |622| 
	.dwpsn	file "../param.c",line 625,column 3,is_stmt
        MOVL      XAR0,#302             ; [CPU_] |626| 
        B         $C$L77,UNC            ; [CPU_] |625| 
        ; branch occurs ; [] |625| 
$C$L74:    
	.dwpsn	file "../param.c",line 626,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |626| 
        MOVU      ACC,*-SP[8]           ; [CPU_] |626| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |626| 
        LSL       ACC,2                 ; [CPU_] |626| 
        MOVZ      AR4,SP                ; [CPU_U] |626| 
        SUBB      XAR4,#14              ; [CPU_U] |626| 
        ADDL      XAR7,ACC              ; [CPU_] |626| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |626| 
	.dwpsn	file "../param.c",line 627,column 5,is_stmt
        MOVZ      AR7,*-SP[11]          ; [CPU_] |627| 
        MOVB      XAR6,#0               ; [CPU_] |627| 
        MOVU      ACC,*-SP[3]           ; [CPU_] |627| 
        CMPL      ACC,XAR7              ; [CPU_] |627| 
        BF        $C$L75,NEQ            ; [CPU_] |627| 
        ; branchcc occurs ; [] |627| 
        MOVB      XAR6,#1               ; [CPU_] |627| 
$C$L75:    
        MOV       *-SP[9],AR6           ; [CPU_] |627| 
	.dwpsn	file "../param.c",line 628,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |628| 
        BF        $C$L76,EQ             ; [CPU_] |628| 
        ; branchcc occurs ; [] |628| 
	.dwpsn	file "../param.c",line 629,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |629| 
        MOV       AL,*-SP[8]            ; [CPU_] |629| 
        MOV       *+XAR4[0],AL          ; [CPU_] |629| 
        B         $C$L77,UNC            ; [CPU_] |629| 
        ; branch occurs ; [] |629| 
$C$L76:    
	.dwpsn	file "../param.c",line 631,column 7,is_stmt
        INC       *-SP[8]               ; [CPU_] |631| 
$C$L77:    
	.dwpsn	file "../param.c",line 625,column 10,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |625| 
        CMP       AL,*-SP[8]            ; [CPU_] |625| 
        B         $C$L78,LOS            ; [CPU_] |625| 
        ; branchcc occurs ; [] |625| 
        MOV       AL,*-SP[9]            ; [CPU_] |625| 
        BF        $C$L74,EQ             ; [CPU_] |625| 
        ; branchcc occurs ; [] |625| 
$C$L78:    
	.dwpsn	file "../param.c",line 633,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |633| 
	.dwpsn	file "../param.c",line 634,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$256, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$256, DW_AT_TI_end_line(0x27a)
	.dwattr $C$DW$256, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$256

	.sect	".text"
	.clink
	.global	_PAR_GetEepromIndexes

$C$DW$268	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetEepromIndexes")
	.dwattr $C$DW$268, DW_AT_low_pc(_PAR_GetEepromIndexes)
	.dwattr $C$DW$268, DW_AT_high_pc(0x00)
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_PAR_GetEepromIndexes")
	.dwattr $C$DW$268, DW_AT_external
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$268, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$268, DW_AT_TI_begin_line(0x293)
	.dwattr $C$DW$268, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$268, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../param.c",line 659,column 84,is_stmt,address _PAR_GetEepromIndexes

	.dwfde $C$DW$CIE, _PAR_GetEepromIndexes
$C$DW$269	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg12]
$C$DW$270	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array_index")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg0]
$C$DW$271	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indexes")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$271, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _PAR_GetEepromIndexes         FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_PAR_GetEepromIndexes:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$272	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$272, DW_AT_location[DW_OP_breg20 -2]
$C$DW$273	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$273, DW_AT_location[DW_OP_breg20 -3]
$C$DW$274	.dwtag  DW_TAG_variable, DW_AT_name("indexes")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$274, DW_AT_location[DW_OP_breg20 -6]
$C$DW$275	.dwtag  DW_TAG_variable, DW_AT_name("n")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_n")
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$275, DW_AT_location[DW_OP_breg20 -7]
$C$DW$276	.dwtag  DW_TAG_variable, DW_AT_name("result")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_result")
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$276, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[3],AL            ; [CPU_] |659| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |659| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |659| 
	.dwpsn	file "../param.c",line 661,column 12,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |661| 
        MOVL      XAR0,#304             ; [CPU_] |661| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |661| 
        MOV       *-SP[7],AL            ; [CPU_] |661| 
	.dwpsn	file "../param.c",line 663,column 3,is_stmt
        MOVB      AH,#0                 ; [CPU_] |663| 
        CMP       AL,*-SP[3]            ; [CPU_] |663| 
        B         $C$L79,LOS            ; [CPU_] |663| 
        ; branchcc occurs ; [] |663| 
        MOVB      AH,#1                 ; [CPU_] |663| 
$C$L79:    
        MOV       *-SP[8],AH            ; [CPU_] |663| 
	.dwpsn	file "../param.c",line 664,column 3,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |664| 
        BF        $C$L80,EQ             ; [CPU_] |664| 
        ; branchcc occurs ; [] |664| 
	.dwpsn	file "../param.c",line 665,column 5,is_stmt
        MOVL      XAR5,*-SP[2]          ; [CPU_] |665| 
        MOVL      XAR0,#302             ; [CPU_] |665| 
        MOVL      XAR7,*+XAR5[AR0]      ; [CPU_] |665| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |665| 
        MOVU      ACC,*-SP[3]           ; [CPU_] |665| 
        LSL       ACC,2                 ; [CPU_] |665| 
        ADDL      XAR7,ACC              ; [CPU_] |665| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |665| 
$C$L80:    
	.dwpsn	file "../param.c",line 667,column 3,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |667| 
	.dwpsn	file "../param.c",line 668,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$268, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$268, DW_AT_TI_end_line(0x29c)
	.dwattr $C$DW$268, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$268

	.sect	".text"
	.clink
	.global	_PAR_StoreODSubIndex

$C$DW$278	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$278, DW_AT_low_pc(_PAR_StoreODSubIndex)
	.dwattr $C$DW$278, DW_AT_high_pc(0x00)
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$278, DW_AT_external
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$278, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$278, DW_AT_TI_begin_line(0x2b7)
	.dwattr $C$DW$278, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$278, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 696,column 1,is_stmt,address _PAR_StoreODSubIndex

	.dwfde $C$DW$CIE, _PAR_StoreODSubIndex
$C$DW$279	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_reg12]
$C$DW$280	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_reg0]
$C$DW$281	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$281, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _PAR_StoreODSubIndex          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_PAR_StoreODSubIndex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$282	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$282, DW_AT_location[DW_OP_breg20 -2]
$C$DW$283	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_breg20 -3]
$C$DW$284	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_breg20 -4]
$C$DW$285	.dwtag  DW_TAG_variable, DW_AT_name("array_index")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_array_index")
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$285, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[4],AH            ; [CPU_] |696| 
        MOV       *-SP[3],AL            ; [CPU_] |696| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |696| 
	.dwpsn	file "../param.c",line 699,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |699| 
        MOVZ      AR5,SP                ; [CPU_U] |699| 
        SUBB      XAR5,#5               ; [CPU_U] |699| 
$C$DW$286	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$286, DW_AT_low_pc(0x00)
	.dwattr $C$DW$286, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$286, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |699| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |699| 
        CMPB      AL,#0                 ; [CPU_] |699| 
        BF        $C$L82,EQ             ; [CPU_] |699| 
        ; branchcc occurs ; [] |699| 
	.dwpsn	file "../param.c",line 700,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |700| 
        MOVL      XAR4,#_mailboxWriteParameters ; [CPU_U] |700| 
        MOVB      AL,#0                 ; [CPU_] |700| 
        SUBB      XAR5,#5               ; [CPU_U] |700| 
$C$DW$287	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$287, DW_AT_low_pc(0x00)
	.dwattr $C$DW$287, DW_AT_name("_MBX_post")
	.dwattr $C$DW$287, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |700| 
        ; call occurs [#_MBX_post] ; [] |700| 
        CMPB      AL,#0                 ; [CPU_] |700| 
        BF        $C$L81,EQ             ; [CPU_] |700| 
        ; branchcc occurs ; [] |700| 
	.dwpsn	file "../param.c",line 701,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |701| 
        B         $C$L83,UNC            ; [CPU_] |701| 
        ; branch occurs ; [] |701| 
$C$L81:    
	.dwpsn	file "../param.c",line 703,column 7,is_stmt
        MOV       AL,#5                 ; [CPU_] |703| 
        MOV       AH,#1284              ; [CPU_] |703| 
        B         $C$L83,UNC            ; [CPU_] |703| 
        ; branch occurs ; [] |703| 
$C$L82:    
	.dwpsn	file "../param.c",line 706,column 5,is_stmt
        MOV       ACC,#3076 << 15       ; [CPU_] |706| 
$C$L83:    
	.dwpsn	file "../param.c",line 707,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$278, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$278, DW_AT_TI_end_line(0x2c3)
	.dwattr $C$DW$278, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$278

	.sect	".text"
	.clink
	.global	_TaskStoreParam

$C$DW$289	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskStoreParam")
	.dwattr $C$DW$289, DW_AT_low_pc(_TaskStoreParam)
	.dwattr $C$DW$289, DW_AT_high_pc(0x00)
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_TaskStoreParam")
	.dwattr $C$DW$289, DW_AT_external
	.dwattr $C$DW$289, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$289, DW_AT_TI_begin_line(0x2d4)
	.dwattr $C$DW$289, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$289, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../param.c",line 725,column 1,is_stmt,address _TaskStoreParam

	.dwfde $C$DW$CIE, _TaskStoreParam

;***************************************************************
;* FNAME: _TaskStoreParam               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_TaskStoreParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$290	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_breg20 -1]
$C$DW$291	.dwtag  DW_TAG_variable, DW_AT_name("indexcrc")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_indexcrc")
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$291, DW_AT_location[DW_OP_breg20 -2]
$C$DW$292	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_breg20 -4]
$C$DW$293	.dwtag  DW_TAG_variable, DW_AT_name("crc_to_be_computed")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_crc_to_be_computed")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_breg20 -5]
	.dwpsn	file "../param.c",line 729,column 28,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |729| 
	.dwpsn	file "../param.c",line 730,column 3,is_stmt
$C$DW$294	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$294, DW_AT_low_pc(0x00)
	.dwattr $C$DW$294, DW_AT_name("_canOpenInit")
	.dwattr $C$DW$294, DW_AT_TI_call
        LCR       #_canOpenInit         ; [CPU_] |730| 
        ; call occurs [#_canOpenInit] ; [] |730| 
	.dwpsn	file "../param.c",line 731,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AH,#0                 ; [CPU_] |731| 
        MOVZ      AR5,SP                ; [CPU_U] |731| 
        MOV       AL,#8195              ; [CPU_] |731| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |731| 
        SUBB      XAR5,#2               ; [CPU_U] |731| 
$C$DW$295	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$295, DW_AT_low_pc(0x00)
	.dwattr $C$DW$295, DW_AT_name("_PAR_FindODPermanentParamIndex")
	.dwattr $C$DW$295, DW_AT_TI_call
        LCR       #_PAR_FindODPermanentParamIndex ; [CPU_] |731| 
        ; call occurs [#_PAR_FindODPermanentParamIndex] ; [] |731| 
        CMPB      AL,#0                 ; [CPU_] |731| 
        BF        $C$L84,NEQ            ; [CPU_] |731| 
        ; branchcc occurs ; [] |731| 
	.dwpsn	file "../param.c",line 731,column 86,is_stmt
        MOVB      *-SP[2],#1,UNC        ; [CPU_] |731| 
$C$L84:    
	.dwpsn	file "../param.c",line 732,column 9,is_stmt
$C$L85:    
	.dwpsn	file "../param.c",line 733,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |733| 
        MOVL      XAR4,#_mailboxWriteParameters ; [CPU_U] |733| 
        MOVB      AL,#10                ; [CPU_] |733| 
        SUBB      XAR5,#1               ; [CPU_U] |733| 
$C$DW$296	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$296, DW_AT_low_pc(0x00)
	.dwattr $C$DW$296, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$296, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |733| 
        ; call occurs [#_MBX_pend] ; [] |733| 
        CMPB      AL,#0                 ; [CPU_] |733| 
        BF        $C$L89,EQ             ; [CPU_] |733| 
        ; branchcc occurs ; [] |733| 
	.dwpsn	file "../param.c",line 734,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |734| 
        MOVL      *-SP[4],ACC           ; [CPU_] |734| 
	.dwpsn	file "../param.c",line 735,column 7,is_stmt
        MOVZ      AR6,*-SP[1]           ; [CPU_] |735| 
        MOVU      ACC,*-SP[2]           ; [CPU_] |735| 
        CMPL      ACC,XAR6              ; [CPU_] |735| 
        BF        $C$L86,NEQ            ; [CPU_] |735| 
        ; branchcc occurs ; [] |735| 
	.dwpsn	file "../param.c",line 735,column 30,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |735| 
        B         $C$L85,UNC            ; [CPU_] |735| 
        ; branch occurs ; [] |735| 
$C$L86:    
	.dwpsn	file "../param.c",line 736,column 12,is_stmt
        MOVZ      AR6,*-SP[1]           ; [CPU_] |736| 
        MOVU      ACC,*-SP[2]           ; [CPU_] |736| 
        CMPL      ACC,XAR6              ; [CPU_] |736| 
        BF        $C$L88,EQ             ; [CPU_] |736| 
        ; branchcc occurs ; [] |736| 
        MOV       AL,*-SP[1]            ; [CPU_] |736| 
        B         $C$L88,LT             ; [CPU_] |736| 
        ; branchcc occurs ; [] |736| 
	.dwpsn	file "../param.c",line 737,column 9,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |737| 
$C$DW$297	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$297, DW_AT_low_pc(0x00)
	.dwattr $C$DW$297, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$297, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |737| 
        ; call occurs [#_WritePermanentParam] ; [] |737| 
        MOVL      *-SP[4],ACC           ; [CPU_] |737| 
	.dwpsn	file "../param.c",line 738,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |738| 
        BF        $C$L87,NEQ            ; [CPU_] |738| 
        ; branchcc occurs ; [] |738| 
	.dwpsn	file "../param.c",line 739,column 11,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |739| 
        B         $C$L85,UNC            ; [CPU_] |739| 
        ; branch occurs ; [] |739| 
$C$L87:    
	.dwpsn	file "../param.c",line 740,column 14,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |740| 
	.dwpsn	file "../param.c",line 741,column 7,is_stmt
        B         $C$L85,UNC            ; [CPU_] |741| 
        ; branch occurs ; [] |741| 
$C$L88:    
	.dwpsn	file "../param.c",line 742,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |742| 
        B         $C$L85,GEQ            ; [CPU_] |742| 
        ; branchcc occurs ; [] |742| 
	.dwpsn	file "../param.c",line 743,column 9,is_stmt
$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_name("_WriteStatisticParam")
	.dwattr $C$DW$298, DW_AT_TI_call
        LCR       #_WriteStatisticParam ; [CPU_] |743| 
        ; call occurs [#_WriteStatisticParam] ; [] |743| 
        MOVL      *-SP[4],ACC           ; [CPU_] |743| 
	.dwpsn	file "../param.c",line 745,column 5,is_stmt
        B         $C$L85,UNC            ; [CPU_] |745| 
        ; branch occurs ; [] |745| 
$C$L89:    
	.dwpsn	file "../param.c",line 746,column 10,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |746| 
        BF        $C$L85,EQ             ; [CPU_] |746| 
        ; branchcc occurs ; [] |746| 
	.dwpsn	file "../param.c",line 747,column 7,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |747| 
	.dwpsn	file "../param.c",line 748,column 7,is_stmt
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_ComputeParamCRC")
	.dwattr $C$DW$299, DW_AT_TI_call
        LCR       #_ComputeParamCRC     ; [CPU_] |748| 
        ; call occurs [#_ComputeParamCRC] ; [] |748| 
        MOVW      DP,#_ODP_CrcParameters ; [CPU_U] 
        MOVL      @_ODP_CrcParameters,ACC ; [CPU_] |748| 
	.dwpsn	file "../param.c",line 749,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |749| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |749| 
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_WritePermanentParam")
	.dwattr $C$DW$300, DW_AT_TI_call
        LCR       #_WritePermanentParam ; [CPU_] |749| 
        ; call occurs [#_WritePermanentParam] ; [] |749| 
	.dwpsn	file "../param.c",line 732,column 9,is_stmt
        B         $C$L85,UNC            ; [CPU_] |732| 
        ; branch occurs ; [] |732| 
	.dwattr $C$DW$289, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$289, DW_AT_TI_end_line(0x2f0)
	.dwattr $C$DW$289, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$289

	.sect	".text"
	.clink
	.global	_PAR_SetParamDependantVars

$C$DW$301	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$301, DW_AT_low_pc(_PAR_SetParamDependantVars)
	.dwattr $C$DW$301, DW_AT_high_pc(0x00)
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$301, DW_AT_external
	.dwattr $C$DW$301, DW_AT_TI_begin_file("../param.c")
	.dwattr $C$DW$301, DW_AT_TI_begin_line(0x2f8)
	.dwattr $C$DW$301, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$301, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../param.c",line 762,column 1,is_stmt,address _PAR_SetParamDependantVars

	.dwfde $C$DW$CIE, _PAR_SetParamDependantVars

;***************************************************************
;* FNAME: _PAR_SetParamDependantVars    FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_PAR_SetParamDependantVars:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../param.c",line 764,column 3,is_stmt
        MOVW      DP,#_gateway_dict_obj100A ; [CPU_U] 
        MOVB      @_gateway_dict_obj100A,#79,UNC ; [CPU_] |764| 
	.dwpsn	file "../param.c",line 765,column 3,is_stmt
        MOVW      DP,#_ODV_Version      ; [CPU_U] 
        MOVB      @_ODV_Version,#52,UNC ; [CPU_] |765| 
	.dwpsn	file "../param.c",line 766,column 3,is_stmt
        MOVL      XAR4,#1137            ; [CPU_U] |766| 
        MOVW      DP,#_gateway_dict_obj1018_Vendor_ID ; [CPU_U] 
        MOVL      @_gateway_dict_obj1018_Vendor_ID,XAR4 ; [CPU_] |766| 
	.dwpsn	file "../param.c",line 767,column 3,is_stmt
        MOVW      DP,#_gateway_dict_obj1018_Product_Code ; [CPU_U] 
        MOVL      XAR4,#4200            ; [CPU_U] |767| 
        MOVL      @_gateway_dict_obj1018_Product_Code,XAR4 ; [CPU_] |767| 
	.dwpsn	file "../param.c",line 768,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |768| 
        BF        $C$L90,EQ             ; [CPU_] |768| 
        ; branchcc occurs ; [] |768| 
        MOVB      ACC,#104              ; [CPU_] |768| 
        CMPL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |768| 
        B         $C$L91,HIS            ; [CPU_] |768| 
        ; branchcc occurs ; [] |768| 
$C$L90:    
	.dwpsn	file "../param.c",line 770,column 4,is_stmt
        MOVB      ACC,#99               ; [CPU_] |770| 
        MOVL      @_ODP_Board_RevisionNumber,ACC ; [CPU_] |770| 
	.dwpsn	file "../param.c",line 771,column 4,is_stmt
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOVB      @_ODP_Board_BaudRate,#250,UNC ; [CPU_] |771| 
$C$L91:    
	.dwpsn	file "../param.c",line 773,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_RevisionNumber ; [CPU_] |773| 
        MOVW      DP,#_gateway_dict_obj1018_Revision_Number ; [CPU_U] 
        MOVL      @_gateway_dict_obj1018_Revision_Number,ACC ; [CPU_] |773| 
	.dwpsn	file "../param.c",line 774,column 3,is_stmt
        MOVW      DP,#_ODP_Board_SerialNumber ; [CPU_U] 
        MOVL      ACC,@_ODP_Board_SerialNumber ; [CPU_] |774| 
        MOVW      DP,#_gateway_dict_obj1018_Serial_Number ; [CPU_U] 
        MOVL      @_gateway_dict_obj1018_Serial_Number,ACC ; [CPU_] |774| 
	.dwpsn	file "../param.c",line 775,column 3,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOV32     R0H,@_ODP_Battery_Capacity ; [CPU_] |775| 
        MPYF32    R0H,R0H,#17761        ; [CPU_] |775| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16512        ; [CPU_] |775| 
$C$DW$302	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$302, DW_AT_low_pc(0x00)
	.dwattr $C$DW$302, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$302, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |775| 
        ; call occurs [#_CNV_Round] ; [] |775| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |775| 
	.dwpsn	file "../param.c",line 777,column 3,is_stmt
        MOVW      DP,#_CNV_CurrentUnit  ; [CPU_U] 
        MOVIZ     R0H,#16256            ; [CPU_] |777| 
        MOV32     @_CNV_CurrentUnit,R0H ; [CPU_] |777| 
	.dwpsn	file "../param.c",line 778,column 3,is_stmt
        MOVW      DP,#_CNV_CurrentRange ; [CPU_U] 
        MOV       @_CNV_CurrentRange,#2047 ; [CPU_] |778| 
	.dwpsn	file "../param.c",line 779,column 1,is_stmt
$C$DW$303	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$303, DW_AT_low_pc(0x00)
	.dwattr $C$DW$303, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$301, DW_AT_TI_end_file("../param.c")
	.dwattr $C$DW$301, DW_AT_TI_end_line(0x30b)
	.dwattr $C$DW$301, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$301

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_ERR_HandleWarning
	.global	_canOpenInit
	.global	_ODP_Voltage_Min
	.global	_CNV_CurrentRange
	.global	_ODP_Temperature_Max
	.global	_ODP_CommError_OverTemp_ErrCounter
	.global	_ODP_CommError_OverVoltage_ErrCounter
	.global	_ODV_Version
	.global	_ODP_CommError_LowVoltage_ErrCounter
	.global	_ODP_Temperature_Min
	.global	_ODP_Board_BaudRate
	.global	_ODP_VersionParameters
	.global	_I2C_Command
	.global	_ODP_Voltage_Max
	.global	_ODP_Current_Min
	.global	_MBX_pend
	.global	_SEM_pend
	.global	_ODP_Current_Max
	.global	_MBX_post
	.global	_gateway_dict_obj100A
	.global	_CNV_CurrentUnit
	.global	_ODP_Board_SerialNumber
	.global	_ODP_Board_RevisionNumber
	.global	_gateway_dict_obj1018_Revision_Number
	.global	_ODP_Battery_Capacity
	.global	__setODentry
	.global	__getODentry
	.global	_getCRC32_cpu
	.global	_CNV_Round
	.global	_ODP_OnTime
	.global	_BoardODdata
	.global	_ODV_Gateway_Errorcode
	.global	_ODP_CrcParameters
	.global	_gateway_dict_obj1018_Vendor_ID
	.global	_gateway_dict_obj1018_Product_Code
	.global	_gateway_dict_obj1018_Serial_Number
	.global	_ODV_Gateway_Date_Time
	.global	_TSK_timerSem
	.global	_mailboxWriteParameters
	.global	_ODI_EEPROM_INDEXES

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$122	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$122, DW_AT_byte_size(0x01)
$C$DW$304	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$305	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$122

$C$DW$T$123	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$306, DW_AT_name("cob_id")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$307, DW_AT_name("rtr")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$308, DW_AT_name("len")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$309, DW_AT_name("data")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$310, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$311, DW_AT_name("csSDO")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$312, DW_AT_name("csEmergency")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$313, DW_AT_name("csSYNC")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$314, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$315, DW_AT_name("csPDO")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$316, DW_AT_name("csLSS")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$317, DW_AT_name("errCode")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$318, DW_AT_name("errRegMask")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$319, DW_AT_name("active")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)

$C$DW$T$105	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x18)
$C$DW$320	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$320, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$105


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$321, DW_AT_name("index")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$322, DW_AT_name("subindex")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$323, DW_AT_name("size")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$324, DW_AT_name("address")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$325	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$110)
$C$DW$T$124	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$325)

$C$DW$T$125	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$125, DW_AT_byte_size(0x3e4)
$C$DW$326	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$326, DW_AT_upper_bound(0xf8)
	.dwendtag $C$DW$T$125

$C$DW$T$111	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$111, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x06)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$327, DW_AT_name("date")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_date")
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$328, DW_AT_name("error")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_error")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$127	.dwtag  DW_TAG_typedef, DW_AT_name("T_StatLog")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)

$C$DW$T$128	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$128, DW_AT_byte_size(0x30)
$C$DW$329	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$329, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$128


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x04)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$330, DW_AT_name("pdata")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_pdata")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$331, DW_AT_name("size")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$332, DW_AT_name("address")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("T_StatIndexes")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$333	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$26)
$C$DW$T$27	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$333)
$C$DW$T$28	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x16)

$C$DW$T$131	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$131, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$131, DW_AT_byte_size(0x2c)
$C$DW$334	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$334, DW_AT_upper_bound(0x0a)
	.dwendtag $C$DW$T$131


$C$DW$T$132	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$132, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x10)
$C$DW$335	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$335, DW_AT_upper_bound(0x03)
	.dwendtag $C$DW$T$132


$C$DW$T$133	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$133, DW_AT_byte_size(0x20)
$C$DW$336	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$336, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$133


$C$DW$T$134	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x18)
$C$DW$337	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$337, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$134


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x06)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$338, DW_AT_name("size")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$339, DW_AT_name("address")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$340, DW_AT_name("nb")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_nb")
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$341, DW_AT_name("indexes")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_indexes")
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$136	.dwtag  DW_TAG_typedef, DW_AT_name("T_StatPages")
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$136, DW_AT_language(DW_LANG_C)
$C$DW$342	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$136)
$C$DW$T$137	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$342)

$C$DW$T$138	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$138, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$138, DW_AT_byte_size(0x1e)
$C$DW$343	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$343, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$138


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x08)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$344, DW_AT_name("wListElem")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$345, DW_AT_name("wCount")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$346, DW_AT_name("fxn")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37

$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$32	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$32, DW_AT_address_class(0x16)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)

$C$DW$T$44	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$44, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x30)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$347, DW_AT_name("dataQue")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$348, DW_AT_name("freeQue")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$349, DW_AT_name("dataSem")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$350, DW_AT_name("freeSem")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$351, DW_AT_name("segid")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$352, DW_AT_name("size")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$353, DW_AT_name("length")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$354, DW_AT_name("name")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44

$C$DW$T$140	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$140, DW_AT_language(DW_LANG_C)
$C$DW$T$142	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$142, DW_AT_address_class(0x16)
$C$DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)

$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x04)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$355, DW_AT_name("next")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$356, DW_AT_name("prev")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$45, DW_AT_address_class(0x16)

$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x10)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$357, DW_AT_name("job")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$358, DW_AT_name("count")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$359, DW_AT_name("pendQ")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$360, DW_AT_name("name")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48

$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$145	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$145, DW_AT_address_class(0x16)
$C$DW$T$146	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$T$146, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$147	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)

$C$DW$T$34	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$361	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$33)
	.dwendtag $C$DW$T$34

$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x16)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)

$C$DW$T$73	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)
$C$DW$362	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$72)
	.dwendtag $C$DW$T$73

$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_language(DW_LANG_C)
$C$DW$T$77	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$77, DW_AT_language(DW_LANG_C)
$C$DW$T$87	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$87, DW_AT_language(DW_LANG_C)
$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)

$C$DW$T$82	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)
$C$DW$363	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$72)
$C$DW$364	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$82

$C$DW$T$83	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_address_class(0x16)
$C$DW$T$115	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$115, DW_AT_language(DW_LANG_C)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$T$88	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)

$C$DW$T$106	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$106, DW_AT_language(DW_LANG_C)
$C$DW$365	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$72)
$C$DW$366	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$6)
$C$DW$367	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$9)
$C$DW$368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$106

$C$DW$T$107	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$107, DW_AT_address_class(0x16)
$C$DW$T$108	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$89	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$89, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$369	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$369, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$50	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$50, DW_AT_address_class(0x16)
$C$DW$370	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$6)
$C$DW$T$61	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$370)
$C$DW$T$62	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$T$62, DW_AT_address_class(0x16)

$C$DW$T$167	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$167, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$167, DW_AT_byte_size(0x10)
$C$DW$371	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$371, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$167

$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$80	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$169	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$169, DW_AT_language(DW_LANG_C)
$C$DW$372	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$9)
$C$DW$T$59	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$372)
$C$DW$T$60	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$60, DW_AT_address_class(0x16)
$C$DW$T$81	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x16)

$C$DW$T$174	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$174, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$174, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$174, DW_AT_byte_size(0x05)
$C$DW$373	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$373, DW_AT_upper_bound(0x04)
	.dwendtag $C$DW$T$174


$C$DW$T$175	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$175, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x10)
$C$DW$374	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$374, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$175

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$375	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$10)
$C$DW$T$177	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$375)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$63	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)
$C$DW$376	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$6)
$C$DW$377	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$63

$C$DW$T$64	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_address_class(0x16)
$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$378	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$72)
$C$DW$379	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$53)
$C$DW$380	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$6)
$C$DW$381	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)
$C$DW$382	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$92)
$C$DW$T$93	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$382)
$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_address_class(0x16)

$C$DW$T$99	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
$C$DW$383	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$72)
$C$DW$384	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$9)
$C$DW$385	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$99

$C$DW$T$100	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$100, DW_AT_address_class(0x16)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$386	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$16)
$C$DW$T$194	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$194, DW_AT_type(*$C$DW$386)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$42	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$42, DW_AT_address_class(0x16)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)

$C$DW$T$102	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$102, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x01)
$C$DW$387	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$388	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$102

$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)

$C$DW$T$68	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$68, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x01)
$C$DW$389	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$390	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$391	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$392	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$393	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$394	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$395	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$396	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$68

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)

$C$DW$T$85	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x80)
$C$DW$397	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$397, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$85


$C$DW$T$49	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$49, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x06)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$398, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$399, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$400, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$401, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$402, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$403, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$49

$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
$C$DW$404	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$56)
$C$DW$T$57	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$404)
$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)

$C$DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$112, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x132)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$405, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$406, DW_AT_name("objdict")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$407, DW_AT_name("PDO_status")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$408, DW_AT_name("firstIndex")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$409, DW_AT_name("lastIndex")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$410, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$411, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$412, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$413, DW_AT_name("transfers")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$414, DW_AT_name("nodeState")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$415, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$416, DW_AT_name("initialisation")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$417, DW_AT_name("preOperational")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$418, DW_AT_name("operational")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$419, DW_AT_name("stopped")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$420, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$421, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$422, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$423, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$424, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$425, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$426, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$427, DW_AT_name("heartbeatError")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$428, DW_AT_name("NMTable")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$429, DW_AT_name("syncTimer")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$430, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$431, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$432, DW_AT_name("pre_sync")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$433, DW_AT_name("post_TPDO")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$434, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$435, DW_AT_name("toggle")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$436, DW_AT_name("canHandle")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$437, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$438, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$439, DW_AT_name("globalCallback")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$440, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$441, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$442, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$443, DW_AT_name("dcf_request")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$444, DW_AT_name("error_state")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$445, DW_AT_name("error_history_size")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$446, DW_AT_name("error_number")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$447, DW_AT_name("error_first_element")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$448, DW_AT_name("error_register")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$449, DW_AT_name("error_cobid")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$450, DW_AT_name("error_data")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$451, DW_AT_name("post_emcy")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$452, DW_AT_name("lss_transfer")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$453, DW_AT_name("eeprom_index")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$454, DW_AT_name("eeprom_size")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$112

$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_address_class(0x16)

$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x0e)
$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$455, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$456, DW_AT_name("event_timer")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$457, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$458, DW_AT_name("last_message")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x16)

$C$DW$T$116	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$116, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x14)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$459, DW_AT_name("nodeId")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$460, DW_AT_name("whoami")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$461, DW_AT_name("state")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$462, DW_AT_name("toggle")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$463, DW_AT_name("abortCode")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$464, DW_AT_name("index")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$465, DW_AT_name("subIndex")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$466, DW_AT_name("port")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$467, DW_AT_name("count")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$468, DW_AT_name("offset")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$469, DW_AT_name("datap")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$470, DW_AT_name("dataType")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$471, DW_AT_name("timer")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$472, DW_AT_name("Callback")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$116

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x3c)
$C$DW$473	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$473, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$67


$C$DW$T$120	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$120, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x04)
$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$474, DW_AT_name("pSubindex")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$475, DW_AT_name("bSubCount")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$476, DW_AT_name("index")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$120

$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$477	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$51)
$C$DW$T$52	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$477)
$C$DW$T$53	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_address_class(0x16)

$C$DW$T$96	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)
$C$DW$478	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$9)
$C$DW$479	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$79)
$C$DW$480	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$95)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x16)
$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$121	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$121, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$121, DW_AT_byte_size(0x08)
$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$481, DW_AT_name("bAccessType")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$482, DW_AT_name("bDataType")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$483, DW_AT_name("size")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$484, DW_AT_name("pObject")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$485, DW_AT_name("bProcessor")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$121

$C$DW$486	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$121)
$C$DW$T$117	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$486)
$C$DW$T$118	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_language(DW_LANG_C)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$487	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$487, DW_AT_location[DW_OP_reg0]
$C$DW$488	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$488, DW_AT_location[DW_OP_reg1]
$C$DW$489	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$489, DW_AT_location[DW_OP_reg2]
$C$DW$490	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$490, DW_AT_location[DW_OP_reg3]
$C$DW$491	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$491, DW_AT_location[DW_OP_reg20]
$C$DW$492	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$492, DW_AT_location[DW_OP_reg21]
$C$DW$493	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg22]
$C$DW$494	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$494, DW_AT_location[DW_OP_reg23]
$C$DW$495	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$495, DW_AT_location[DW_OP_reg24]
$C$DW$496	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$496, DW_AT_location[DW_OP_reg25]
$C$DW$497	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$497, DW_AT_location[DW_OP_reg26]
$C$DW$498	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$498, DW_AT_location[DW_OP_reg28]
$C$DW$499	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$499, DW_AT_location[DW_OP_reg29]
$C$DW$500	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$500, DW_AT_location[DW_OP_reg30]
$C$DW$501	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$501, DW_AT_location[DW_OP_reg31]
$C$DW$502	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$502, DW_AT_location[DW_OP_regx 0x20]
$C$DW$503	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$503, DW_AT_location[DW_OP_regx 0x21]
$C$DW$504	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$504, DW_AT_location[DW_OP_regx 0x22]
$C$DW$505	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$505, DW_AT_location[DW_OP_regx 0x23]
$C$DW$506	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$506, DW_AT_location[DW_OP_regx 0x24]
$C$DW$507	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$507, DW_AT_location[DW_OP_regx 0x25]
$C$DW$508	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$508, DW_AT_location[DW_OP_regx 0x26]
$C$DW$509	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$509, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$510	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$510, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$511	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$511, DW_AT_location[DW_OP_reg4]
$C$DW$512	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$512, DW_AT_location[DW_OP_reg6]
$C$DW$513	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$513, DW_AT_location[DW_OP_reg8]
$C$DW$514	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$514, DW_AT_location[DW_OP_reg10]
$C$DW$515	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$515, DW_AT_location[DW_OP_reg12]
$C$DW$516	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$516, DW_AT_location[DW_OP_reg14]
$C$DW$517	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$517, DW_AT_location[DW_OP_reg16]
$C$DW$518	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$518, DW_AT_location[DW_OP_reg17]
$C$DW$519	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$519, DW_AT_location[DW_OP_reg18]
$C$DW$520	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$520, DW_AT_location[DW_OP_reg19]
$C$DW$521	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$521, DW_AT_location[DW_OP_reg5]
$C$DW$522	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$522, DW_AT_location[DW_OP_reg7]
$C$DW$523	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$523, DW_AT_location[DW_OP_reg9]
$C$DW$524	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$524, DW_AT_location[DW_OP_reg11]
$C$DW$525	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$525, DW_AT_location[DW_OP_reg13]
$C$DW$526	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$526, DW_AT_location[DW_OP_reg15]
$C$DW$527	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$527, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$528	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$528, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$529	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$529, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$530	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$530, DW_AT_location[DW_OP_regx 0x30]
$C$DW$531	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$531, DW_AT_location[DW_OP_regx 0x33]
$C$DW$532	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$532, DW_AT_location[DW_OP_regx 0x34]
$C$DW$533	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$533, DW_AT_location[DW_OP_regx 0x37]
$C$DW$534	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$534, DW_AT_location[DW_OP_regx 0x38]
$C$DW$535	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$535, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$536	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$536, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$537	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$537, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$538	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$538, DW_AT_location[DW_OP_regx 0x40]
$C$DW$539	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$539, DW_AT_location[DW_OP_regx 0x43]
$C$DW$540	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$540, DW_AT_location[DW_OP_regx 0x44]
$C$DW$541	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$541, DW_AT_location[DW_OP_regx 0x47]
$C$DW$542	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$542, DW_AT_location[DW_OP_regx 0x48]
$C$DW$543	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$543, DW_AT_location[DW_OP_regx 0x49]
$C$DW$544	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$544, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$545	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$545, DW_AT_location[DW_OP_regx 0x27]
$C$DW$546	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$546, DW_AT_location[DW_OP_regx 0x28]
$C$DW$547	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$547, DW_AT_location[DW_OP_reg27]
$C$DW$548	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$548, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

