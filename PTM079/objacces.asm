;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Thu Aug 05 16:17:35 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Iguana_yachts\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("memcpy")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_memcpy")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$3)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$98)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$1

;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0757612 
	.sect	".text"
	.clink
	.global	__getDataSubindex

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("_getDataSubindex")
	.dwattr $C$DW$5, DW_AT_low_pc(__getDataSubindex)
	.dwattr $C$DW$5, DW_AT_high_pc(0x00)
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("__getDataSubindex")
	.dwattr $C$DW$5, DW_AT_external
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$5, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$5, DW_AT_TI_begin_line(0x5d)
	.dwattr $C$DW$5, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$5, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 96,column 31,is_stmt,address __getDataSubindex

	.dwfde $C$DW$CIE, __getDataSubindex
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg12]
$C$DW$7	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_reg0]
$C$DW$8	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_reg1]
$C$DW$9	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdata_subindex")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_pdata_subindex")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: __getDataSubindex             FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

__getDataSubindex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_breg20 -2]
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -3]
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -4]
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("pdata_subindex")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_pdata_subindex")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_breg20 -6]
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("ptrTable")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_ptrTable")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -8]
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("Callback")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -10]
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -12]
        MOV       *-SP[4],AH            ; [CPU_] |96| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |96| 
        MOV       *-SP[3],AL            ; [CPU_] |96| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |96| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 101,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |101| 
        MOVZ      AR5,SP                ; [CPU_U] |101| 
        MOVB      XAR0,#250             ; [CPU_] |101| 
        SUBB      XAR5,#10              ; [CPU_U] |101| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |101| 
        MOVZ      AR4,SP                ; [CPU_U] |101| 
        SUBB      XAR4,#12              ; [CPU_U] |101| 
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_call
	.dwattr $C$DW$17, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |101| 
        ; call occurs [XAR7] ; [] |101| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |101| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 103,column 3,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |103| 
        BF        $C$L1,EQ              ; [CPU_] |103| 
        ; branchcc occurs ; [] |103| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 104,column 5,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |104| 
        B         $C$L3,UNC             ; [CPU_] |104| 
        ; branch occurs ; [] |104| 
$C$L1:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 105,column 3,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |105| 
        MOV       AL,*-SP[4]            ; [CPU_] |105| 
        CMP       AL,*+XAR4[2]          ; [CPU_] |105| 
        B         $C$L2,LO              ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 108,column 5,is_stmt
        MOV       AL,#17                ; [CPU_] |108| 
        MOV       AH,#1545              ; [CPU_] |108| 
        B         $C$L3,UNC             ; [CPU_] |108| 
        ; branch occurs ; [] |108| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 110,column 3,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |110| 
        MOVL      XAR6,*+XAR4[0]        ; [CPU_] |110| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |110| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |110| 
        LSL       ACC,3                 ; [CPU_] |110| 
        ADDL      XAR6,ACC              ; [CPU_] |110| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |110| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 111,column 3,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |111| 
$C$L3:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 112,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$5, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$5, DW_AT_TI_end_line(0x70)
	.dwattr $C$DW$5, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$5

	.sect	".text"
	.clink
	.global	__getODentry

$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("_getODentry")
	.dwattr $C$DW$19, DW_AT_low_pc(__getODentry)
	.dwattr $C$DW$19, DW_AT_high_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("__getODentry")
	.dwattr $C$DW$19, DW_AT_external
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$19, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$19, DW_AT_TI_begin_line(0x85)
	.dwattr $C$DW$19, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$19, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 142,column 1,is_stmt,address __getODentry

	.dwfde $C$DW$CIE, __getODentry
$C$DW$20	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_reg12]
$C$DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg0]
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg1]
$C$DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pDestData")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_pDestData")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg14]
$C$DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pExpectedSize")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_pExpectedSize")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -24]
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pDataType")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_pDataType")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -26]
$C$DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_name("checkAccess")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_checkAccess")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -27]
$C$DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("endianize")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_endianize")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -28]
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mode")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -29]

;***************************************************************
;* FNAME: __getODentry                  FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 20 Auto,  0 SOE     *
;***************************************************************

__getODentry:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -2]
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -3]
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -4]
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("pDestData")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_pDestData")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -6]
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -8]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("szData")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_szData")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -10]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ptrTable")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ptrTable")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -12]
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("Callback")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[4],AH            ; [CPU_] |142| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |142| 
        MOV       *-SP[3],AL            ; [CPU_] |142| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |142| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 149,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |149| 
        MOVZ      AR5,SP                ; [CPU_U] |149| 
        MOVB      XAR0,#250             ; [CPU_] |149| 
        SUBB      XAR5,#14              ; [CPU_U] |149| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |149| 
        MOVZ      AR4,SP                ; [CPU_U] |149| 
        SUBB      XAR4,#8               ; [CPU_U] |149| 
$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_call
	.dwattr $C$DW$37, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |149| 
        ; call occurs [XAR7] ; [] |149| 
        MOVL      *-SP[12],XAR4         ; [CPU_] |149| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 151,column 3,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |151| 
        BF        $C$L4,EQ              ; [CPU_] |151| 
        ; branchcc occurs ; [] |151| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 152,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |152| 
        B         $C$L29,UNC            ; [CPU_] |152| 
        ; branch occurs ; [] |152| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 153,column 3,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |153| 
        MOV       AL,*-SP[4]            ; [CPU_] |153| 
        CMP       AL,*+XAR4[2]          ; [CPU_] |153| 
        B         $C$L5,LO              ; [CPU_] |153| 
        ; branchcc occurs ; [] |153| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 156,column 5,is_stmt
        MOV       AL,#17                ; [CPU_] |156| 
        MOV       AH,#1545              ; [CPU_] |156| 
        B         $C$L29,UNC            ; [CPU_] |156| 
        ; branch occurs ; [] |156| 
$C$L5:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 159,column 3,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |159| 
        BF        $C$L6,EQ              ; [CPU_] |159| 
        ; branchcc occurs ; [] |159| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |159| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |159| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |159| 
        LSL       ACC,3                 ; [CPU_] |159| 
        ADDL      XAR4,ACC              ; [CPU_] |159| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |159| 
        BF        $C$L6,NTC             ; [CPU_] |159| 
        ; branchcc occurs ; [] |159| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 162,column 5,is_stmt
        MOV       AL,#1                 ; [CPU_] |162| 
        MOV       AH,#1537              ; [CPU_] |162| 
        B         $C$L29,UNC            ; [CPU_] |162| 
        ; branch occurs ; [] |162| 
$C$L6:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 167,column 3,is_stmt
        MOV       AL,*-SP[29]           ; [CPU_] |167| 
        CMPB      AL,#4                 ; [CPU_] |167| 
        BF        $C$L8,EQ              ; [CPU_] |167| 
        ; branchcc occurs ; [] |167| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 168,column 5,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |168| 
        BF        $C$L7,EQ              ; [CPU_] |168| 
        ; branchcc occurs ; [] |168| 
        MOVL      XAR4,*-SP[14]         ; [CPU_] |168| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |168| 
        LSL       ACC,1                 ; [CPU_] |168| 
        ADDL      XAR4,ACC              ; [CPU_] |168| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |168| 
        BF        $C$L7,EQ              ; [CPU_] |168| 
        ; branchcc occurs ; [] |168| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 169,column 7,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |169| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |169| 
        MOVL      XAR5,*-SP[12]         ; [CPU_] |169| 
        LSL       ACC,1                 ; [CPU_] |169| 
        ADDL      XAR4,ACC              ; [CPU_] |169| 
        MOVL      XAR7,*+XAR4[0]        ; [CPU_] |169| 
        MOV       AL,*-SP[4]            ; [CPU_] |169| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |169| 
        MOVB      AH,#0                 ; [CPU_] |169| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_TI_call
	.dwattr $C$DW$38, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |169| 
        ; call occurs [XAR7] ; [] |169| 
        MOVL      *-SP[8],ACC           ; [CPU_] |169| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 170,column 7,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |170| 
        BF        $C$L8,EQ              ; [CPU_] |170| 
        ; branchcc occurs ; [] |170| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 172,column 11,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |172| 
        B         $C$L29,UNC            ; [CPU_] |172| 
        ; branch occurs ; [] |172| 
$C$L7:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 176,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |176| 
        MOVB      XAR0,#254             ; [CPU_] |176| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |176| 
        BF        $C$L8,EQ              ; [CPU_] |176| 
        ; branchcc occurs ; [] |176| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |176| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |176| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |176| 
        LSL       ACC,3                 ; [CPU_] |176| 
        ADDL      XAR4,ACC              ; [CPU_] |176| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |176| 
        BF        $C$L8,EQ              ; [CPU_] |176| 
        ; branchcc occurs ; [] |176| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 177,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |177| 
        MOVB      XAR0,#254             ; [CPU_] |177| 
        MOV       AL,*-SP[4]            ; [CPU_] |177| 
        MOVL      XAR5,*-SP[12]         ; [CPU_] |177| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |177| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |177| 
        MOVB      AH,#0                 ; [CPU_] |177| 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_TI_call
	.dwattr $C$DW$39, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |177| 
        ; call occurs [XAR7] ; [] |177| 
        MOVL      *-SP[8],ACC           ; [CPU_] |177| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 178,column 9,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |178| 
        BF        $C$L8,EQ              ; [CPU_] |178| 
        ; branchcc occurs ; [] |178| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 180,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |180| 
        B         $C$L29,UNC            ; [CPU_] |180| 
        ; branch occurs ; [] |180| 
$C$L8:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 187,column 3,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |187| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |187| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |187| 
        MOVL      XAR5,*-SP[26]         ; [CPU_] |187| 
        LSL       ACC,3                 ; [CPU_] |187| 
        ADDL      XAR4,ACC              ; [CPU_] |187| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |187| 
        MOV       *+XAR5[0],AL          ; [CPU_] |187| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 188,column 3,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |188| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |188| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |188| 
        LSL       ACC,3                 ; [CPU_] |188| 
        ADDL      XAR4,ACC              ; [CPU_] |188| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |188| 
        MOVL      *-SP[10],ACC          ; [CPU_] |188| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 190,column 3,is_stmt
        MOVL      XAR4,*-SP[24]         ; [CPU_] |190| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |190| 
        BF        $C$L9,EQ              ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
        MOVL      XAR4,*-SP[24]         ; [CPU_] |190| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |190| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |190| 
        BF        $C$L9,EQ              ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
        MOVL      XAR4,*-SP[26]         ; [CPU_] |190| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |190| 
        CMPB      AL,#9                 ; [CPU_] |190| 
        B         $C$L28,LO             ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
        MOVL      XAR4,*-SP[24]         ; [CPU_] |190| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |190| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |190| 
        B         $C$L28,LOS            ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
$C$L9:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 212,column 5,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |212| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |212| 
        CMPB      AL,#9                 ; [CPU_] |212| 
        BF        $C$L21,EQ             ; [CPU_] |212| 
        ; branchcc occurs ; [] |212| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 215,column 4,is_stmt
        MOV       AL,*-SP[29]           ; [CPU_] |215| 
        BF        $C$L15,NEQ            ; [CPU_] |215| 
        ; branchcc occurs ; [] |215| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 216,column 9,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |216| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |216| 
        CMPB      AL,#1                 ; [CPU_] |216| 
        B         $C$L14,LOS            ; [CPU_] |216| 
        ; branchcc occurs ; [] |216| 
        MOVL      XAR4,*-SP[26]         ; [CPU_] |216| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |216| 
        CMPB      AL,#9                 ; [CPU_] |216| 
        B         $C$L10,LO             ; [CPU_] |216| 
        ; branchcc occurs ; [] |216| 
        MOVL      XAR4,*-SP[26]         ; [CPU_] |216| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |216| 
        CMPB      AL,#15                ; [CPU_] |216| 
        B         $C$L14,LOS            ; [CPU_] |216| 
        ; branchcc occurs ; [] |216| 
$C$L10:    

$C$DW$40	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -15]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -16]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 218,column 14,is_stmt
        MOV       *-SP[15],#0           ; [CPU_] |218| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 218,column 21,is_stmt
        MOV       *-SP[16],#0           ; [CPU_] |218| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 219,column 13,is_stmt
        MOV       *-SP[16],#0           ; [CPU_] |219| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 219,column 21,is_stmt
        MOVU      ACC,*-SP[16]          ; [CPU_] |219| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |219| 
        B         $C$L20,HIS            ; [CPU_] |219| 
        ; branchcc occurs ; [] |219| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 220,column 8,is_stmt
        TBIT      *-SP[16],#0           ; [CPU_] |220| 
        BF        $C$L12,TC             ; [CPU_] |220| 
        ; branchcc occurs ; [] |220| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 221,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |221| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |221| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |221| 
        MOVZ      AR0,*-SP[15]          ; [CPU_] |221| 
        LSL       ACC,3                 ; [CPU_] |221| 
        ADDL      XAR4,ACC              ; [CPU_] |221| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |221| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |221| 
        MOVZ      AR0,*-SP[16]          ; [CPU_] |221| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |221| 
        ANDB      AL,#0xff              ; [CPU_] |221| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |221| 
        B         $C$L13,UNC            ; [CPU_] |221| 
        ; branch occurs ; [] |221| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 223,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |223| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |223| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |223| 
        MOVZ      AR0,*-SP[15]          ; [CPU_] |223| 
        LSL       ACC,3                 ; [CPU_] |223| 
        ADDL      XAR4,ACC              ; [CPU_] |223| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |223| 
        MOVB      AL,#1                 ; [CPU_] |223| 
        ADD       AL,AR0                ; [CPU_] |223| 
        MOV       *-SP[15],AL           ; [CPU_] |223| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |223| 
        MOVZ      AR0,*-SP[16]          ; [CPU_] |223| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |223| 
        LSR       AL,8                  ; [CPU_] |223| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |223| 
$C$L13:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 219,column 34,is_stmt
        INC       *-SP[16]              ; [CPU_] |219| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 219,column 21,is_stmt
        MOVU      ACC,*-SP[16]          ; [CPU_] |219| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |219| 
        B         $C$L11,LO             ; [CPU_] |219| 
        ; branchcc occurs ; [] |219| 
	.dwendtag $C$DW$40

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 225,column 6,is_stmt
        B         $C$L20,UNC            ; [CPU_] |225| 
        ; branch occurs ; [] |225| 
$C$L14:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 225,column 13,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |225| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |225| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |225| 
        LSL       ACC,3                 ; [CPU_] |225| 
        ADDL      XAR4,ACC              ; [CPU_] |225| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |225| 
        MOVL      XAR5,*+XAR4[4]        ; [CPU_] |225| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |225| 
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("___memcpy_ff")
	.dwattr $C$DW$43, DW_AT_TI_call
        LCR       #___memcpy_ff         ; [CPU_] |225| 
        ; call occurs [#___memcpy_ff] ; [] |225| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 226,column 4,is_stmt
        B         $C$L20,UNC            ; [CPU_] |226| 
        ; branch occurs ; [] |226| 
$C$L15:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 227,column 12,is_stmt
        CMPB      AL,#1                 ; [CPU_] |227| 
        BF        $C$L16,EQ             ; [CPU_] |227| 
        ; branchcc occurs ; [] |227| 
        CMPB      AL,#4                 ; [CPU_] |227| 
        BF        $C$L17,NEQ            ; [CPU_] |227| 
        ; branchcc occurs ; [] |227| 
$C$L16:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 228,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |228| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |228| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |228| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |228| 
        LSL       ACC,3                 ; [CPU_] |228| 
        ADDL      XAR4,ACC              ; [CPU_] |228| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |228| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |228| 
        B         $C$L20,UNC            ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
$C$L17:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 229,column 12,is_stmt
        CMPB      AL,#3                 ; [CPU_] |229| 
        BF        $C$L20,NEQ            ; [CPU_] |229| 
        ; branchcc occurs ; [] |229| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 230,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |230| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |230| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |230| 
        LSL       ACC,3                 ; [CPU_] |230| 
        ADDL      XAR4,ACC              ; [CPU_] |230| 
        MOVB      ACC,#1                ; [CPU_] |230| 
        MOVL      XAR5,*+XAR4[4]        ; [CPU_] |230| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |230| 
        B         $C$L18,HIS            ; [CPU_] |230| 
        ; branchcc occurs ; [] |230| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |230| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |230| 
        B         $C$L19,UNC            ; [CPU_] |230| 
        ; branch occurs ; [] |230| 
$C$L18:    
        MOVL      ACC,*-SP[10]          ; [CPU_] |230| 
$C$L19:    
        MOVL      XAR4,*-SP[6]          ; [CPU_] |230| 
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("___memcpy_ff")
	.dwattr $C$DW$44, DW_AT_TI_call
        LCR       #___memcpy_ff         ; [CPU_] |230| 
        ; call occurs [#___memcpy_ff] ; [] |230| 
$C$L20:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 231,column 7,is_stmt
        MOVL      XAR4,*-SP[24]         ; [CPU_] |231| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |231| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |231| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 232,column 5,is_stmt
        B         $C$L27,UNC            ; [CPU_] |232| 
        ; branch occurs ; [] |232| 
$C$L21:    

$C$DW$45	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("ptr")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_ptr")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -16]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ptr_start")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ptr_start")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -18]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ptr_end")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ptr_end")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -20]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 238,column 19,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |238| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |238| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |238| 
        LSL       ACC,3                 ; [CPU_] |238| 
        ADDL      XAR4,ACC              ; [CPU_] |238| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |238| 
        MOVL      *-SP[16],ACC          ; [CPU_] |238| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 239,column 25,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |239| 
        MOVL      *-SP[18],ACC          ; [CPU_] |239| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 241,column 23,is_stmt
        MOVL      XAR4,*-SP[24]         ; [CPU_] |241| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |241| 
        BF        $C$L22,EQ             ; [CPU_] |241| 
        ; branchcc occurs ; [] |241| 
        MOVL      XAR4,*-SP[24]         ; [CPU_] |241| 
        MOVL      XAR6,*+XAR4[0]        ; [CPU_] |241| 
        B         $C$L23,UNC            ; [CPU_] |241| 
        ; branch occurs ; [] |241| 
$C$L22:    
        MOVL      XAR6,*-SP[10]         ; [CPU_] |241| 
$C$L23:    
        MOVL      ACC,*-SP[16]          ; [CPU_] |241| 
        ADDL      ACC,XAR6              ; [CPU_] |241| 
        MOVL      *-SP[20],ACC          ; [CPU_] |241| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 243,column 9,is_stmt
$C$L24:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 243,column 16,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |243| 
        MOV       AL,*XAR4++            ; [CPU_] |243| 
        MOVL      *-SP[16],XAR4         ; [CPU_] |243| 
        BF        $C$L25,EQ             ; [CPU_] |243| 
        ; branchcc occurs ; [] |243| 
        MOVL      XAR6,*-SP[16]         ; [CPU_] |243| 
        MOVL      ACC,*-SP[20]          ; [CPU_] |243| 
        CMPL      ACC,XAR6              ; [CPU_] |243| 
        B         $C$L24,HI             ; [CPU_] |243| 
        ; branchcc occurs ; [] |243| 
$C$L25:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 247,column 9,is_stmt
        MOVL      XAR4,*-SP[12]         ; [CPU_] |247| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |247| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |247| 
        LSL       ACC,3                 ; [CPU_] |247| 
        ADDL      XAR4,ACC              ; [CPU_] |247| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |247| 
        MOVL      *-SP[6],ACC           ; [CPU_] |247| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 248,column 9,is_stmt
        MOVL      XAR4,*-SP[24]         ; [CPU_] |248| 
        MOVL      ACC,*-SP[16]          ; [CPU_] |248| 
        SUBL      ACC,*-SP[18]          ; [CPU_] |248| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |248| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 250,column 9,is_stmt
        MOVL      XAR4,*-SP[24]         ; [CPU_] |250| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |250| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |250| 
        B         $C$L26,LOS            ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 251,column 13,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |251| 
        MOV       *+XAR4[0],#0          ; [CPU_] |251| 
$C$L26:    
	.dwendtag $C$DW$45

$C$L27:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 254,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |254| 
        B         $C$L29,UNC            ; [CPU_] |254| 
        ; branch occurs ; [] |254| 
$C$L28:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 257,column 5,is_stmt
        MOVL      XAR4,*-SP[24]         ; [CPU_] |257| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |257| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |257| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 260,column 5,is_stmt
        MOV       AL,#16                ; [CPU_] |260| 
        MOV       AH,#1543              ; [CPU_] |260| 
$C$L29:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 262,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$19, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$19, DW_AT_TI_end_line(0x106)
	.dwattr $C$DW$19, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$19

	.sect	".text"
	.clink
	.global	__setODentry

$C$DW$50	.dwtag  DW_TAG_subprogram, DW_AT_name("_setODentry")
	.dwattr $C$DW$50, DW_AT_low_pc(__setODentry)
	.dwattr $C$DW$50, DW_AT_high_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("__setODentry")
	.dwattr $C$DW$50, DW_AT_external
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$50, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$50, DW_AT_TI_begin_line(0x119)
	.dwattr $C$DW$50, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$50, DW_AT_TI_max_frame_size(-20)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 289,column 1,is_stmt,address __setODentry

	.dwfde $C$DW$CIE, __setODentry
$C$DW$51	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg12]
$C$DW$52	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg0]
$C$DW$53	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg1]
$C$DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pSourceData")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_pSourceData")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg14]
$C$DW$55	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pExpectedSize")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_pExpectedSize")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -22]
$C$DW$56	.dwtag  DW_TAG_formal_parameter, DW_AT_name("checkAccess")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_checkAccess")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -23]
$C$DW$57	.dwtag  DW_TAG_formal_parameter, DW_AT_name("endianize")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_endianize")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -24]
$C$DW$58	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sdo")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_sdo")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -25]

;***************************************************************
;* FNAME: __setODentry                  FR SIZE:  18           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

__setODentry:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -20
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -2]
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -3]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -4]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("pSourceData")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_pSourceData")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -6]
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("szData")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_szData")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -8]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("dataType")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -9]
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -12]
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ptrTable")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ptrTable")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -14]
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("Callback")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -16]
        MOV       *-SP[4],AH            ; [CPU_] |289| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |289| 
        MOV       *-SP[3],AL            ; [CPU_] |289| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |289| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 296,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |296| 
        MOVZ      AR5,SP                ; [CPU_U] |296| 
        MOVB      XAR0,#250             ; [CPU_] |296| 
        SUBB      XAR5,#16              ; [CPU_U] |296| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |296| 
        MOVZ      AR4,SP                ; [CPU_U] |296| 
        SUBB      XAR4,#12              ; [CPU_U] |296| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_TI_call
	.dwattr $C$DW$68, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |296| 
        ; call occurs [XAR7] ; [] |296| 
        MOVL      *-SP[14],XAR4         ; [CPU_] |296| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 297,column 3,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |297| 
        BF        $C$L30,EQ             ; [CPU_] |297| 
        ; branchcc occurs ; [] |297| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 298,column 5,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |298| 
        B         $C$L53,UNC            ; [CPU_] |298| 
        ; branch occurs ; [] |298| 
$C$L30:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 300,column 3,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |300| 
        MOV       AL,*-SP[4]            ; [CPU_] |300| 
        CMP       AL,*+XAR4[2]          ; [CPU_] |300| 
        B         $C$L31,LO             ; [CPU_] |300| 
        ; branchcc occurs ; [] |300| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 303,column 5,is_stmt
        MOV       AL,#17                ; [CPU_] |303| 
        MOV       AH,#1545              ; [CPU_] |303| 
        B         $C$L53,UNC            ; [CPU_] |303| 
        ; branch occurs ; [] |303| 
$C$L31:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 305,column 3,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |305| 
        BF        $C$L32,EQ             ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
        MOVL      XAR4,*-SP[14]         ; [CPU_] |305| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |305| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |305| 
        LSL       ACC,3                 ; [CPU_] |305| 
        ADDL      XAR4,ACC              ; [CPU_] |305| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |305| 
        BF        $C$L32,NTC            ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 308,column 5,is_stmt
        MOV       AL,#2                 ; [CPU_] |308| 
        MOV       AH,#1537              ; [CPU_] |308| 
        B         $C$L53,UNC            ; [CPU_] |308| 
        ; branch occurs ; [] |308| 
$C$L32:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 312,column 3,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |312| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |312| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |312| 
        LSL       ACC,3                 ; [CPU_] |312| 
        ADDL      XAR4,ACC              ; [CPU_] |312| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |312| 
        MOV       *-SP[9],AL            ; [CPU_] |312| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 313,column 3,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |313| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |313| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |313| 
        LSL       ACC,3                 ; [CPU_] |313| 
        ADDL      XAR4,ACC              ; [CPU_] |313| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |313| 
        MOVL      *-SP[8],ACC           ; [CPU_] |313| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 315,column 3,is_stmt
        MOVL      XAR4,*-SP[22]         ; [CPU_] |315| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |315| 
        BF        $C$L33,EQ             ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
        MOVL      XAR4,*-SP[22]         ; [CPU_] |315| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |315| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |315| 
        BF        $C$L33,EQ             ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
        MOV       AL,*-SP[9]            ; [CPU_] |315| 
        CMPB      AL,#9                 ; [CPU_] |315| 
        BF        $C$L52,NEQ            ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
        MOVL      XAR4,*-SP[22]         ; [CPU_] |315| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |315| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |315| 
        B         $C$L52,LOS            ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
$C$L33:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 338,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |338| 
        MOVB      XAR0,#14              ; [CPU_] |338| 
        MOV       AL,*-SP[9]            ; [CPU_] |338| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |338| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |338| 
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_TI_call
	.dwattr $C$DW$69, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |338| 
        ; call occurs [XAR7] ; [] |338| 
        MOVL      *-SP[12],ACC          ; [CPU_] |338| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 339,column 7,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |339| 
        BF        $C$L34,EQ             ; [CPU_] |339| 
        ; branchcc occurs ; [] |339| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 341,column 9,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |341| 
        B         $C$L53,UNC            ; [CPU_] |341| 
        ; branch occurs ; [] |341| 
$C$L34:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 345,column 7,is_stmt
        MOV       AL,*-SP[25]           ; [CPU_] |345| 
        BF        $C$L40,NEQ            ; [CPU_] |345| 
        ; branchcc occurs ; [] |345| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 346,column 9,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |346| 
        CMPB      AL,#1                 ; [CPU_] |346| 
        B         $C$L39,LOS            ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
        CMPB      AL,#9                 ; [CPU_] |346| 
        B         $C$L35,LO             ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
        CMPB      AL,#15                ; [CPU_] |346| 
        B         $C$L39,LOS            ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
$C$L35:    

$C$DW$70	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -17]
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("j")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_j")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -18]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 348,column 17,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |348| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 348,column 24,is_stmt
        MOV       *-SP[18],#0           ; [CPU_] |348| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 349,column 16,is_stmt
        MOV       *-SP[18],#0           ; [CPU_] |349| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 349,column 24,is_stmt
        MOVU      ACC,*-SP[18]          ; [CPU_] |349| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |349| 
        B         $C$L44,HIS            ; [CPU_] |349| 
        ; branchcc occurs ; [] |349| 
$C$L36:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 350,column 11,is_stmt
        TBIT      *-SP[18],#0           ; [CPU_] |350| 
        BF        $C$L37,TC             ; [CPU_] |350| 
        ; branchcc occurs ; [] |350| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 351,column 12,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |351| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |351| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |351| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |351| 
        MOVZ      AR0,*-SP[18]          ; [CPU_] |351| 
        LSL       ACC,3                 ; [CPU_] |351| 
        ADDL      XAR4,ACC              ; [CPU_] |351| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |351| 
        MOV       AL,*+XAR5[AR0]        ; [CPU_] |351| 
        MOVZ      AR0,*-SP[17]          ; [CPU_] |351| 
        ANDB      AL,#0xff              ; [CPU_] |351| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |351| 
        B         $C$L38,UNC            ; [CPU_] |351| 
        ; branch occurs ; [] |351| 
$C$L37:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 353,column 12,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |353| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |353| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |353| 
        LSL       ACC,3                 ; [CPU_] |353| 
        MOVZ      AR6,*-SP[17]          ; [CPU_] |353| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |353| 
        ADDL      XAR4,ACC              ; [CPU_] |353| 
        MOVB      AL,#1                 ; [CPU_] |353| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |353| 
        MOVZ      AR0,*-SP[18]          ; [CPU_] |353| 
        ADD       AL,AR6                ; [CPU_] |353| 
        MOV       *-SP[17],AL           ; [CPU_] |353| 
        MOVL      ACC,XAR4              ; [CPU_] |353| 
        ADDU      ACC,AR6               ; [CPU_] |353| 
        MOVL      XAR4,ACC              ; [CPU_] |353| 
        MOV       ACC,*+XAR5[AR0] << #8 ; [CPU_] |353| 
        ADD       *+XAR4[0],AL          ; [CPU_] |353| 
$C$L38:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 349,column 37,is_stmt
        INC       *-SP[18]              ; [CPU_] |349| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 349,column 24,is_stmt
        MOVU      ACC,*-SP[18]          ; [CPU_] |349| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |349| 
        B         $C$L36,LO             ; [CPU_] |349| 
        ; branchcc occurs ; [] |349| 
	.dwendtag $C$DW$70

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 355,column 9,is_stmt
        B         $C$L44,UNC            ; [CPU_] |355| 
        ; branch occurs ; [] |355| 
$C$L39:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 355,column 16,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |355| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |355| 
        MOVL      XAR5,*-SP[22]         ; [CPU_] |355| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |355| 
        LSL       ACC,3                 ; [CPU_] |355| 
        ADDL      XAR4,ACC              ; [CPU_] |355| 
        MOVL      ACC,*+XAR5[0]         ; [CPU_] |355| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |355| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |355| 
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("___memcpy_ff")
	.dwattr $C$DW$73, DW_AT_TI_call
        LCR       #___memcpy_ff         ; [CPU_] |355| 
        ; call occurs [#___memcpy_ff] ; [] |355| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 356,column 7,is_stmt
        B         $C$L44,UNC            ; [CPU_] |356| 
        ; branch occurs ; [] |356| 
$C$L40:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 357,column 12,is_stmt
        CMPB      AL,#1                 ; [CPU_] |357| 
        BF        $C$L41,NEQ            ; [CPU_] |357| 
        ; branchcc occurs ; [] |357| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 358,column 9,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |358| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |358| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |358| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |358| 
        LSL       ACC,3                 ; [CPU_] |358| 
        ADDL      XAR4,ACC              ; [CPU_] |358| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |358| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |358| 
        B         $C$L44,UNC            ; [CPU_] |358| 
        ; branch occurs ; [] |358| 
$C$L41:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 359,column 12,is_stmt
        CMPB      AL,#3                 ; [CPU_] |359| 
        BF        $C$L44,NEQ            ; [CPU_] |359| 
        ; branchcc occurs ; [] |359| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 360,column 9,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |360| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |360| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |360| 
        LSL       ACC,3                 ; [CPU_] |360| 
        ADDL      XAR4,ACC              ; [CPU_] |360| 
        MOVB      ACC,#1                ; [CPU_] |360| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |360| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |360| 
        B         $C$L42,HIS            ; [CPU_] |360| 
        ; branchcc occurs ; [] |360| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |360| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |360| 
        B         $C$L43,UNC            ; [CPU_] |360| 
        ; branch occurs ; [] |360| 
$C$L42:    
        MOVL      ACC,*-SP[8]           ; [CPU_] |360| 
$C$L43:    
        MOVL      XAR5,*-SP[6]          ; [CPU_] |360| 
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("___memcpy_ff")
	.dwattr $C$DW$74, DW_AT_TI_call
        LCR       #___memcpy_ff         ; [CPU_] |360| 
        ; call occurs [#___memcpy_ff] ; [] |360| 
$C$L44:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 368,column 7,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |368| 
        CMPB      AL,#9                 ; [CPU_] |368| 
        BF        $C$L45,NEQ            ; [CPU_] |368| 
        ; branchcc occurs ; [] |368| 
        MOVL      XAR4,*-SP[22]         ; [CPU_] |368| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |368| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |368| 
        B         $C$L45,LOS            ; [CPU_] |368| 
        ; branchcc occurs ; [] |368| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 369,column 9,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |369| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |369| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |369| 
        MOVL      XAR5,*-SP[22]         ; [CPU_] |369| 
        LSL       ACC,3                 ; [CPU_] |369| 
        ADDL      XAR4,ACC              ; [CPU_] |369| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |369| 
        ADDL      ACC,*+XAR5[0]         ; [CPU_] |369| 
        MOVL      XAR4,ACC              ; [CPU_] |369| 
        MOV       *+XAR4[0],#0          ; [CPU_] |369| 
$C$L45:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 374,column 7,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |374| 
        CMPB      AL,#2                 ; [CPU_] |374| 
        BF        $C$L47,NEQ            ; [CPU_] |374| 
        ; branchcc occurs ; [] |374| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 375,column 9,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |375| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |375| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |375| 
        LSL       ACC,3                 ; [CPU_] |375| 
        ADDL      XAR4,ACC              ; [CPU_] |375| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |375| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |375| 
        ANDB      AL,#0x80              ; [CPU_] |375| 
        CMPB      AL,#128               ; [CPU_] |375| 
        BF        $C$L46,NEQ            ; [CPU_] |375| 
        ; branchcc occurs ; [] |375| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 376,column 11,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |376| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |376| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |376| 
        LSL       ACC,3                 ; [CPU_] |376| 
        ADDL      XAR4,ACC              ; [CPU_] |376| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |376| 
        OR        *+XAR4[0],#0xff00     ; [CPU_] |376| 
        B         $C$L47,UNC            ; [CPU_] |376| 
        ; branch occurs ; [] |376| 
$C$L46:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 378,column 11,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |378| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |378| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |378| 
        LSL       ACC,3                 ; [CPU_] |378| 
        ADDL      XAR4,ACC              ; [CPU_] |378| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |378| 
        AND       *+XAR4[0],#0x00ff     ; [CPU_] |378| 
$C$L47:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 381,column 7,is_stmt
        MOVL      XAR4,*-SP[22]         ; [CPU_] |381| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |381| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |381| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 384,column 7,is_stmt
        MOV       AL,*-SP[25]           ; [CPU_] |384| 
        CMPB      AL,#1                 ; [CPU_] |384| 
        BF        $C$L49,EQ             ; [CPU_] |384| 
        ; branchcc occurs ; [] |384| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 385,column 9,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |385| 
        BF        $C$L48,EQ             ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |385| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |385| 
        LSL       ACC,1                 ; [CPU_] |385| 
        ADDL      XAR4,ACC              ; [CPU_] |385| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |385| 
        BF        $C$L48,EQ             ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 386,column 11,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |386| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |386| 
        MOVL      XAR5,*-SP[14]         ; [CPU_] |386| 
        LSL       ACC,1                 ; [CPU_] |386| 
        ADDL      XAR4,ACC              ; [CPU_] |386| 
        MOVL      XAR7,*+XAR4[0]        ; [CPU_] |386| 
        MOV       AL,*-SP[4]            ; [CPU_] |386| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |386| 
        MOVB      AH,#1                 ; [CPU_] |386| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_TI_call
	.dwattr $C$DW$75, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |386| 
        ; call occurs [XAR7] ; [] |386| 
        MOVL      *-SP[12],ACC          ; [CPU_] |386| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 387,column 11,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |387| 
        BF        $C$L49,EQ             ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 388,column 15,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |388| 
        B         $C$L53,UNC            ; [CPU_] |388| 
        ; branch occurs ; [] |388| 
$C$L48:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 391,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |391| 
        MOVB      XAR0,#254             ; [CPU_] |391| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |391| 
        BF        $C$L49,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
        MOVL      XAR4,*-SP[14]         ; [CPU_] |391| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |391| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |391| 
        LSL       ACC,3                 ; [CPU_] |391| 
        ADDL      XAR4,ACC              ; [CPU_] |391| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |391| 
        BF        $C$L49,EQ             ; [CPU_] |391| 
        ; branchcc occurs ; [] |391| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 392,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |392| 
        MOVB      XAR0,#254             ; [CPU_] |392| 
        MOV       AL,*-SP[4]            ; [CPU_] |392| 
        MOVL      XAR5,*-SP[14]         ; [CPU_] |392| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |392| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |392| 
        MOVB      AH,#1                 ; [CPU_] |392| 
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_call
	.dwattr $C$DW$76, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |392| 
        ; call occurs [XAR7] ; [] |392| 
        MOVL      *-SP[12],ACC          ; [CPU_] |392| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 393,column 13,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |393| 
        BF        $C$L49,EQ             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 395,column 17,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |395| 
        B         $C$L53,UNC            ; [CPU_] |395| 
        ; branch occurs ; [] |395| 
$C$L49:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 402,column 7,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |402| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |402| 
        MOVU      ACC,*-SP[4]           ; [CPU_] |402| 
        LSL       ACC,3                 ; [CPU_] |402| 
        ADDL      XAR4,ACC              ; [CPU_] |402| 
        TBIT      *+XAR4[0],#2          ; [CPU_] |402| 
        BF        $C$L51,NTC            ; [CPU_] |402| 
        ; branchcc occurs ; [] |402| 
        MOV       AL,*-SP[25]           ; [CPU_] |402| 
        CMPB      AL,#1                 ; [CPU_] |402| 
        BF        $C$L51,EQ             ; [CPU_] |402| 
        ; branchcc occurs ; [] |402| 
        CMPB      AL,#4                 ; [CPU_] |402| 
        BF        $C$L51,EQ             ; [CPU_] |402| 
        ; branchcc occurs ; [] |402| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 403,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |403| 
        MOVB      XAR0,#252             ; [CPU_] |403| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |403| 
        BF        $C$L50,EQ             ; [CPU_] |403| 
        ; branchcc occurs ; [] |403| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 404,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |404| 
        MOVB      XAR0,#252             ; [CPU_] |404| 
        MOV       AL,*-SP[3]            ; [CPU_] |404| 
        MOV       AH,*-SP[4]            ; [CPU_] |404| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |404| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |404| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_TI_call
	.dwattr $C$DW$77, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |404| 
        ; call occurs [XAR7] ; [] |404| 
        MOVL      *-SP[12],ACC          ; [CPU_] |404| 
$C$L50:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 405,column 9,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |405| 
        BF        $C$L51,EQ             ; [CPU_] |405| 
        ; branchcc occurs ; [] |405| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 406,column 11,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |406| 
        B         $C$L53,UNC            ; [CPU_] |406| 
        ; branch occurs ; [] |406| 
$C$L51:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 408,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |408| 
        B         $C$L53,UNC            ; [CPU_] |408| 
        ; branch occurs ; [] |408| 
$C$L52:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 410,column 7,is_stmt
        MOVL      XAR4,*-SP[22]         ; [CPU_] |410| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |410| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |410| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 412,column 7,is_stmt
        MOV       AL,#16                ; [CPU_] |412| 
        MOV       AH,#1543              ; [CPU_] |412| 
$C$L53:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 414,column 1,is_stmt
        SUBB      SP,#18                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$50, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$50, DW_AT_TI_end_line(0x19e)
	.dwattr $C$DW$50, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$50

	.sect	".text"
	.clink
	.global	_scanIndexOD

$C$DW$79	.dwtag  DW_TAG_subprogram, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$79, DW_AT_low_pc(_scanIndexOD)
	.dwattr $C$DW$79, DW_AT_high_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$79, DW_AT_external
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$79, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$79, DW_AT_TI_begin_line(0x1aa)
	.dwattr $C$DW$79, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$79, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 427,column 1,is_stmt,address _scanIndexOD

	.dwfde $C$DW$CIE, _scanIndexOD
$C$DW$80	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg12]
$C$DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg0]
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("errorCode")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg14]
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Callback")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_breg20 -10]

;***************************************************************
;* FNAME: _scanIndexOD                  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_scanIndexOD:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -2]
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -3]
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[3],AL            ; [CPU_] |427| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |427| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |427| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 428,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |428| 
        MOVB      XAR0,#250             ; [CPU_] |428| 
        MOVL      XAR5,*-SP[10]         ; [CPU_] |428| 
        MOVL      XAR7,*+XAR4[AR0]      ; [CPU_] |428| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |428| 
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_TI_call
	.dwattr $C$DW$87, DW_AT_TI_indirect
        LCR       *XAR7                 ; [CPU_] |428| 
        ; call occurs [XAR7] ; [] |428| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 429,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$88	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$88, DW_AT_low_pc(0x00)
	.dwattr $C$DW$88, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$79, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$79, DW_AT_TI_end_line(0x1ad)
	.dwattr $C$DW$79, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$79

	.sect	".text"
	.clink
	.global	_RegisterSetODentryCallBack

$C$DW$89	.dwtag  DW_TAG_subprogram, DW_AT_name("RegisterSetODentryCallBack")
	.dwattr $C$DW$89, DW_AT_low_pc(_RegisterSetODentryCallBack)
	.dwattr $C$DW$89, DW_AT_high_pc(0x00)
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_RegisterSetODentryCallBack")
	.dwattr $C$DW$89, DW_AT_external
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$89, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$89, DW_AT_TI_begin_line(0x1b9)
	.dwattr $C$DW$89, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$89, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 442,column 1,is_stmt,address _RegisterSetODentryCallBack

	.dwfde $C$DW$CIE, _RegisterSetODentryCallBack
$C$DW$90	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg12]
$C$DW$91	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg0]
$C$DW$92	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg1]
$C$DW$93	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Callback")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _RegisterSetODentryCallBack   FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_RegisterSetODentryCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_breg20 -2]
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_breg20 -3]
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_breg20 -4]
$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("Callback")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AH            ; [CPU_] |442| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |442| 
        MOV       *-SP[3],AL            ; [CPU_] |442| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |442| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 453,column 3,is_stmt
        MOVB      ACC,#1                ; [CPU_] |453| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 455,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$89, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$89, DW_AT_TI_end_line(0x1c7)
	.dwattr $C$DW$89, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$89

	.sect	".text"
	.clink
	.global	_RegisterSetODentryCallBacks

$C$DW$99	.dwtag  DW_TAG_subprogram, DW_AT_name("RegisterSetODentryCallBacks")
	.dwattr $C$DW$99, DW_AT_low_pc(_RegisterSetODentryCallBacks)
	.dwattr $C$DW$99, DW_AT_high_pc(0x00)
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_RegisterSetODentryCallBacks")
	.dwattr $C$DW$99, DW_AT_external
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$99, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$99, DW_AT_TI_begin_line(0x1ca)
	.dwattr $C$DW$99, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$99, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 458,column 121,is_stmt,address _RegisterSetODentryCallBacks

	.dwfde $C$DW$CIE, _RegisterSetODentryCallBacks
$C$DW$100	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg12]
$C$DW$101	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_reg0]
$C$DW$102	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindexMin")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_bSubindexMin")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg1]
$C$DW$103	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindexMax")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_bSubindexMax")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -9]
$C$DW$104	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Callback")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _RegisterSetODentryCallBacks  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_RegisterSetODentryCallBacks:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -2]
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_breg20 -3]
$C$DW$107	.dwtag  DW_TAG_variable, DW_AT_name("bSubindexMin")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_bSubindexMin")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_breg20 -4]
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("Callback")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AH            ; [CPU_] |458| 
        MOVL      *-SP[6],XAR5          ; [CPU_] |458| 
        MOV       *-SP[3],AL            ; [CPU_] |458| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |458| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 469,column 3,is_stmt
        MOVB      ACC,#1                ; [CPU_] |469| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 471,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$99, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$99, DW_AT_TI_end_line(0x1d7)
	.dwattr $C$DW$99, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$99

	.sect	".text"
	.clink
	.global	__storeODSubIndex

$C$DW$110	.dwtag  DW_TAG_subprogram, DW_AT_name("_storeODSubIndex")
	.dwattr $C$DW$110, DW_AT_low_pc(__storeODSubIndex)
	.dwattr $C$DW$110, DW_AT_high_pc(0x00)
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("__storeODSubIndex")
	.dwattr $C$DW$110, DW_AT_external
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$110, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$110, DW_AT_TI_begin_line(0x1e0)
	.dwattr $C$DW$110, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$110, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 480,column 66,is_stmt,address __storeODSubIndex

	.dwfde $C$DW$CIE, __storeODSubIndex
$C$DW$111	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg12]
$C$DW$112	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wIndex")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg0]
$C$DW$113	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: __storeODSubIndex             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

__storeODSubIndex:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -2]
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("wIndex")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_wIndex")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -3]
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AH            ; [CPU_] |480| 
        MOV       *-SP[3],AL            ; [CPU_] |480| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |480| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 480,column 67,is_stmt
        MOVB      ACC,#0                ; [CPU_] |480| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c",line 480,column 88,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$110, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Iguana_yachts/common/Festival/objacces.c")
	.dwattr $C$DW$110, DW_AT_TI_end_line(0x1e0)
	.dwattr $C$DW$110, DW_AT_TI_end_column(0x58)
	.dwendentry
	.dwendtag $C$DW$110

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___memcpy_ff

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$118, DW_AT_name("cob_id")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$119, DW_AT_name("rtr")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$120, DW_AT_name("len")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$121, DW_AT_name("data")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$88	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$122, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$123, DW_AT_name("csSDO")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$124, DW_AT_name("csEmergency")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$125, DW_AT_name("csSYNC")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$126, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$127, DW_AT_name("csPDO")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$128, DW_AT_name("csLSS")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$129, DW_AT_name("errCode")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$130, DW_AT_name("errRegMask")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$131, DW_AT_name("active")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$79	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)

$C$DW$T$80	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x18)
$C$DW$132	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$132, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$80


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$133, DW_AT_name("index")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$134, DW_AT_name("subindex")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$135, DW_AT_name("size")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$136, DW_AT_name("address")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$85	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
$C$DW$T$86	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_address_class(0x16)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)

$C$DW$T$48	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$47)
	.dwendtag $C$DW$T$48

$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$62	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$62, DW_AT_language(DW_LANG_C)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$61	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$138	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$47)
$C$DW$139	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$81	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$140	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$47)
$C$DW$141	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$6)
$C$DW$142	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$9)
$C$DW$143	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$81

$C$DW$T$82	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x16)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)
$C$DW$144	.dwtag  DW_TAG_TI_far_type
$C$DW$T$97	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$144)
$C$DW$T$98	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x16)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$145	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$145, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$25	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$25, DW_AT_address_class(0x16)
$C$DW$146	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$6)
$C$DW$T$36	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$146)
$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x16)
$C$DW$T$84	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$147	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$9)
$C$DW$T$34	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$147)
$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x16)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$38	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$148	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$6)
$C$DW$149	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$38

$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)

$C$DW$T$65	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$150	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$47)
$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$28)
$C$DW$152	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$6)
$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$65

$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)
$C$DW$T$67	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$154	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$67)
$C$DW$T$68	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$154)
$C$DW$T$69	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x16)
$C$DW$T$70	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$T$70, DW_AT_address_class(0x16)

$C$DW$T$74	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$74, DW_AT_language(DW_LANG_C)
$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$47)
$C$DW$156	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$9)
$C$DW$157	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$74

$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$77	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$77, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$77, DW_AT_byte_size(0x01)
$C$DW$158	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$159	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$77

$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)

$C$DW$T$43	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$43, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x01)
$C$DW$160	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$161	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$162	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$163	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$164	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$165	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$166	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$167	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$43

$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)

$C$DW$T$60	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x80)
$C$DW$168	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$168, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$60


$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x06)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$169, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$170, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$171, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$172, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$173, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$174, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$175	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$31)
$C$DW$T$32	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$175)
$C$DW$T$33	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_address_class(0x16)

$C$DW$T$87	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$87, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x132)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$176, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$177, DW_AT_name("objdict")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$178, DW_AT_name("PDO_status")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$179, DW_AT_name("firstIndex")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$180, DW_AT_name("lastIndex")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$181, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$182, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$183, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$184, DW_AT_name("transfers")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$185, DW_AT_name("nodeState")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$186, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$187, DW_AT_name("initialisation")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$188, DW_AT_name("preOperational")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$189, DW_AT_name("operational")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$190, DW_AT_name("stopped")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$191, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$192, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$193, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$194, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$195, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$196, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$197, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$198, DW_AT_name("heartbeatError")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$199, DW_AT_name("NMTable")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$200, DW_AT_name("syncTimer")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$201, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$202, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$203, DW_AT_name("pre_sync")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$204, DW_AT_name("post_TPDO")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$205, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$206, DW_AT_name("toggle")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$207, DW_AT_name("canHandle")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$208, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$209, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$210, DW_AT_name("globalCallback")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$211, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$212, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$213, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$214, DW_AT_name("dcf_request")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$215, DW_AT_name("error_state")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$216, DW_AT_name("error_history_size")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$217, DW_AT_name("error_number")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$218, DW_AT_name("error_first_element")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$219, DW_AT_name("error_register")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$220, DW_AT_name("error_cobid")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$221, DW_AT_name("error_data")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$222, DW_AT_name("post_emcy")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$223, DW_AT_name("lss_transfer")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$224, DW_AT_name("eeprom_index")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$225, DW_AT_name("eeprom_size")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$87

$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$89	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$89, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x0e)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$226, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$227, DW_AT_name("event_timer")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$228, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$229, DW_AT_name("last_message")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$89

$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$30	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x16)

$C$DW$T$91	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$91, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$91, DW_AT_byte_size(0x14)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$230, DW_AT_name("nodeId")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$231, DW_AT_name("whoami")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$232, DW_AT_name("state")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$233, DW_AT_name("toggle")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$234, DW_AT_name("abortCode")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$235, DW_AT_name("index")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$236, DW_AT_name("subIndex")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$237, DW_AT_name("port")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$238, DW_AT_name("count")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$239, DW_AT_name("offset")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$240, DW_AT_name("datap")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$241, DW_AT_name("dataType")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$242, DW_AT_name("timer")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$243, DW_AT_name("Callback")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$91

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)

$C$DW$T$42	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x3c)
$C$DW$244	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$244, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$42


$C$DW$T$95	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$95, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$95, DW_AT_byte_size(0x04)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$245, DW_AT_name("pSubindex")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$246, DW_AT_name("bSubCount")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$247, DW_AT_name("index")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$95

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$248	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$26)
$C$DW$T$27	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$248)
$C$DW$T$28	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x16)

$C$DW$T$71	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$249	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$9)
$C$DW$250	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$54)
$C$DW$251	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$70)
	.dwendtag $C$DW$T$71

$C$DW$T$72	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_address_class(0x16)
$C$DW$T$73	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_language(DW_LANG_C)

$C$DW$T$96	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$96, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x08)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$252, DW_AT_name("bAccessType")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$253, DW_AT_name("bDataType")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$254, DW_AT_name("size")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$255, DW_AT_name("pObject")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$256, DW_AT_name("bProcessor")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$96

$C$DW$257	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$96)
$C$DW$T$92	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$257)
$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$102	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$102, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$258	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$258, DW_AT_location[DW_OP_reg0]
$C$DW$259	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$259, DW_AT_location[DW_OP_reg1]
$C$DW$260	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$260, DW_AT_location[DW_OP_reg2]
$C$DW$261	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$261, DW_AT_location[DW_OP_reg3]
$C$DW$262	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$262, DW_AT_location[DW_OP_reg20]
$C$DW$263	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$263, DW_AT_location[DW_OP_reg21]
$C$DW$264	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$264, DW_AT_location[DW_OP_reg22]
$C$DW$265	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$265, DW_AT_location[DW_OP_reg23]
$C$DW$266	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$266, DW_AT_location[DW_OP_reg24]
$C$DW$267	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_reg25]
$C$DW$268	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_reg26]
$C$DW$269	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg28]
$C$DW$270	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg29]
$C$DW$271	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_reg30]
$C$DW$272	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_reg31]
$C$DW$273	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_regx 0x20]
$C$DW$274	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$274, DW_AT_location[DW_OP_regx 0x21]
$C$DW$275	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$275, DW_AT_location[DW_OP_regx 0x22]
$C$DW$276	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_regx 0x23]
$C$DW$277	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_regx 0x24]
$C$DW$278	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_regx 0x25]
$C$DW$279	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_regx 0x26]
$C$DW$280	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$281	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$282	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_reg4]
$C$DW$283	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg6]
$C$DW$284	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_reg8]
$C$DW$285	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_reg10]
$C$DW$286	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_reg12]
$C$DW$287	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_reg14]
$C$DW$288	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_reg16]
$C$DW$289	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_reg17]
$C$DW$290	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_reg18]
$C$DW$291	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_reg19]
$C$DW$292	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_reg5]
$C$DW$293	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_reg7]
$C$DW$294	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_reg9]
$C$DW$295	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_reg11]
$C$DW$296	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_reg13]
$C$DW$297	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$297, DW_AT_location[DW_OP_reg15]
$C$DW$298	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$298, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$299	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$299, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$300	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$300, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$301	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$301, DW_AT_location[DW_OP_regx 0x30]
$C$DW$302	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$302, DW_AT_location[DW_OP_regx 0x33]
$C$DW$303	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$303, DW_AT_location[DW_OP_regx 0x34]
$C$DW$304	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$304, DW_AT_location[DW_OP_regx 0x37]
$C$DW$305	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$305, DW_AT_location[DW_OP_regx 0x38]
$C$DW$306	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$306, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$307	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$307, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$308	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$308, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$309	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$309, DW_AT_location[DW_OP_regx 0x40]
$C$DW$310	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$310, DW_AT_location[DW_OP_regx 0x43]
$C$DW$311	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$311, DW_AT_location[DW_OP_regx 0x44]
$C$DW$312	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$312, DW_AT_location[DW_OP_regx 0x47]
$C$DW$313	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$313, DW_AT_location[DW_OP_regx 0x48]
$C$DW$314	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$314, DW_AT_location[DW_OP_regx 0x49]
$C$DW$315	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$315, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$316	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$316, DW_AT_location[DW_OP_regx 0x27]
$C$DW$317	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$317, DW_AT_location[DW_OP_regx 0x28]
$C$DW$318	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$318, DW_AT_location[DW_OP_reg27]
$C$DW$319	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$319, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

