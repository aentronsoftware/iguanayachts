;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Thu Aug 05 16:17:32 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../lcd.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Iguana_yachts\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$37)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$47)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$9)
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$31)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$9)
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$4

$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
	.global	_DIVISOR
	.sect	".econst:_DIVISOR"
	.clink
	.align	2
_DIVISOR:
	.bits		0x1,32
	.bits		0,32			; _DIVISOR[0] @ 0
	.bits		0xa,32
	.bits		0,32			; _DIVISOR[1] @ 64
	.bits		0x64,32
	.bits		0,32			; _DIVISOR[2] @ 128
	.bits		0x3e8,32
	.bits		0,32			; _DIVISOR[3] @ 192
	.bits		0x2710,32
	.bits		0,32			; _DIVISOR[4] @ 256
	.bits		0x186a0,32
	.bits		0,32			; _DIVISOR[5] @ 320
	.bits		0xf4240,32
	.bits		0,32			; _DIVISOR[6] @ 384
	.bits		0x989680,32
	.bits		0,32			; _DIVISOR[7] @ 448
	.bits		0x5f5e100,32
	.bits		0,32			; _DIVISOR[8] @ 512
	.bits		0x3b9aca00,32
	.bits		0,32			; _DIVISOR[9] @ 576
	.bits		0x540be400,32
	.bits		0x2,32			; _DIVISOR[10] @ 640
	.bits		0x4876e800,32
	.bits		0x17,32			; _DIVISOR[11] @ 704

$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("DIVISOR")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_DIVISOR")
	.dwattr $C$DW$10, DW_AT_location[DW_OP_addr _DIVISOR]
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$10, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0577212 
	.sect	".text"
	.clink
	.global	_LCD_Start

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("LCD_Start")
	.dwattr $C$DW$11, DW_AT_low_pc(_LCD_Start)
	.dwattr $C$DW$11, DW_AT_high_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_LCD_Start")
	.dwattr $C$DW$11, DW_AT_external
	.dwattr $C$DW$11, DW_AT_TI_begin_file("../lcd.c")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x14)
	.dwattr $C$DW$11, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$11, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../lcd.c",line 20,column 21,is_stmt,address _LCD_Start

	.dwfde $C$DW$CIE, _LCD_Start

;***************************************************************
;* FNAME: _LCD_Start                    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_LCD_Start:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../lcd.c",line 22,column 3,is_stmt
        MOV       *-SP[2],#3134         ; [CPU_] |22| 
	.dwpsn	file "../lcd.c",line 23,column 3,is_stmt
        MOV       *-SP[1],#518          ; [CPU_] |23| 
	.dwpsn	file "../lcd.c",line 24,column 3,is_stmt
        MOVB      AL,#7                 ; [CPU_] |24| 
        MOVB      AH,#4                 ; [CPU_] |24| 
        MOVB      XAR5,#0               ; [CPU_] |24| 
        MOVZ      AR4,SP                ; [CPU_U] |24| 
        SUBB      XAR4,#2               ; [CPU_U] |24| 
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$13, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |24| 
        ; call occurs [#_I2C_Command] ; [] |24| 
	.dwpsn	file "../lcd.c",line 25,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$11, DW_AT_TI_end_file("../lcd.c")
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x19)
	.dwattr $C$DW$11, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$11

	.sect	".text"
	.clink
	.global	_LCD_WriteText

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("LCD_WriteText")
	.dwattr $C$DW$15, DW_AT_low_pc(_LCD_WriteText)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_LCD_WriteText")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_TI_begin_file("../lcd.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x1b)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "../lcd.c",line 27,column 56,is_stmt,address _LCD_WriteText

	.dwfde $C$DW$CIE, _LCD_WriteText
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pos")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg0]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("text")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg12]
$C$DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lenght")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LCD_WriteText                FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 15 Auto,  0 SOE     *
;***************************************************************

_LCD_WriteText:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("pos")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -1]
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("text")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -4]
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("lenght")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_breg20 -5]
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -6]
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -7]
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -8]
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -15]
        MOV       *-SP[5],AH            ; [CPU_] |27| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |27| 
        MOV       *-SP[1],AL            ; [CPU_] |27| 
	.dwpsn	file "../lcd.c",line 31,column 3,is_stmt
        ADD       AL,#16512             ; [CPU_] |31| 
        MOV       *-SP[15],AL           ; [CPU_] |31| 
	.dwpsn	file "../lcd.c",line 33,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |33| 
        CMPB      AL,#13                ; [CPU_] |33| 
        B         $C$L12,HIS            ; [CPU_] |33| 
        ; branchcc occurs ; [] |33| 
	.dwpsn	file "../lcd.c",line 34,column 10,is_stmt
        MOVB      *-SP[8],#2,UNC        ; [CPU_] |34| 
        B         $C$L11,UNC            ; [CPU_] |34| 
        ; branch occurs ; [] |34| 
$C$L1:    
	.dwpsn	file "../lcd.c",line 35,column 7,is_stmt
        MOVZ      AR0,*-SP[8]           ; [CPU_] |35| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |35| 
        SUBB      XAR0,#2               ; [CPU_U] |35| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |35| 
        MOV       *-SP[6],AL            ; [CPU_] |35| 
	.dwpsn	file "../lcd.c",line 36,column 7,is_stmt
        CMPB      AL,#32                ; [CPU_] |36| 
        B         $C$L2,LO              ; [CPU_] |36| 
        ; branchcc occurs ; [] |36| 
        CMPB      AL,#47                ; [CPU_] |36| 
        B         $C$L2,HI              ; [CPU_] |36| 
        ; branchcc occurs ; [] |36| 
	.dwpsn	file "../lcd.c",line 37,column 9,is_stmt
        MOVB      AL,#128               ; [CPU_] |37| 
        ADD       AL,*-SP[6]            ; [CPU_] |37| 
        MOV       *-SP[7],AL            ; [CPU_] |37| 
	.dwpsn	file "../lcd.c",line 38,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |38| 
        ; branch occurs ; [] |38| 
$C$L2:    
	.dwpsn	file "../lcd.c",line 39,column 12,is_stmt
        CMPB      AL,#48                ; [CPU_] |39| 
        B         $C$L3,LO              ; [CPU_] |39| 
        ; branchcc occurs ; [] |39| 
        CMPB      AL,#63                ; [CPU_] |39| 
        B         $C$L3,HI              ; [CPU_] |39| 
        ; branchcc occurs ; [] |39| 
	.dwpsn	file "../lcd.c",line 40,column 9,is_stmt
        MOVB      AL,#128               ; [CPU_] |40| 
        ADD       AL,*-SP[6]            ; [CPU_] |40| 
        MOV       *-SP[7],AL            ; [CPU_] |40| 
	.dwpsn	file "../lcd.c",line 41,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |41| 
        ; branch occurs ; [] |41| 
$C$L3:    
	.dwpsn	file "../lcd.c",line 42,column 12,is_stmt
        CMPB      AL,#65                ; [CPU_] |42| 
        B         $C$L4,LO              ; [CPU_] |42| 
        ; branchcc occurs ; [] |42| 
        CMPB      AL,#79                ; [CPU_] |42| 
        B         $C$L4,HI              ; [CPU_] |42| 
        ; branchcc occurs ; [] |42| 
	.dwpsn	file "../lcd.c",line 43,column 9,is_stmt
        MOVB      AL,#128               ; [CPU_] |43| 
        ADD       AL,*-SP[6]            ; [CPU_] |43| 
        MOV       *-SP[7],AL            ; [CPU_] |43| 
	.dwpsn	file "../lcd.c",line 44,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |44| 
        ; branch occurs ; [] |44| 
$C$L4:    
	.dwpsn	file "../lcd.c",line 45,column 12,is_stmt
        CMPB      AL,#80                ; [CPU_] |45| 
        B         $C$L5,LO              ; [CPU_] |45| 
        ; branchcc occurs ; [] |45| 
        CMPB      AL,#90                ; [CPU_] |45| 
        B         $C$L5,HI              ; [CPU_] |45| 
        ; branchcc occurs ; [] |45| 
	.dwpsn	file "../lcd.c",line 46,column 9,is_stmt
        MOVB      AL,#128               ; [CPU_] |46| 
        ADD       AL,*-SP[6]            ; [CPU_] |46| 
        MOV       *-SP[7],AL            ; [CPU_] |46| 
	.dwpsn	file "../lcd.c",line 47,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |47| 
        ; branch occurs ; [] |47| 
$C$L5:    
	.dwpsn	file "../lcd.c",line 48,column 12,is_stmt
        CMPB      AL,#97                ; [CPU_] |48| 
        B         $C$L6,LO              ; [CPU_] |48| 
        ; branchcc occurs ; [] |48| 
        CMPB      AL,#111               ; [CPU_] |48| 
        B         $C$L6,HI              ; [CPU_] |48| 
        ; branchcc occurs ; [] |48| 
	.dwpsn	file "../lcd.c",line 49,column 9,is_stmt
        MOVB      AL,#128               ; [CPU_] |49| 
        ADD       AL,*-SP[6]            ; [CPU_] |49| 
        MOV       *-SP[7],AL            ; [CPU_] |49| 
	.dwpsn	file "../lcd.c",line 50,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |50| 
        ; branch occurs ; [] |50| 
$C$L6:    
	.dwpsn	file "../lcd.c",line 51,column 12,is_stmt
        CMPB      AL,#112               ; [CPU_] |51| 
        B         $C$L7,LO              ; [CPU_] |51| 
        ; branchcc occurs ; [] |51| 
        CMPB      AL,#122               ; [CPU_] |51| 
        B         $C$L7,HI              ; [CPU_] |51| 
        ; branchcc occurs ; [] |51| 
	.dwpsn	file "../lcd.c",line 52,column 9,is_stmt
        MOVB      AL,#128               ; [CPU_] |52| 
        ADD       AL,*-SP[6]            ; [CPU_] |52| 
        MOV       *-SP[7],AL            ; [CPU_] |52| 
	.dwpsn	file "../lcd.c",line 53,column 7,is_stmt
        B         $C$L8,UNC             ; [CPU_] |53| 
        ; branch occurs ; [] |53| 
$C$L7:    
	.dwpsn	file "../lcd.c",line 54,column 12,is_stmt
        MOVB      *-SP[7],#160,UNC      ; [CPU_] |54| 
$C$L8:    
	.dwpsn	file "../lcd.c",line 55,column 7,is_stmt
        TBIT      *-SP[8],#0            ; [CPU_] |55| 
        BF        $C$L9,TC              ; [CPU_] |55| 
        ; branchcc occurs ; [] |55| 
	.dwpsn	file "../lcd.c",line 55,column 23,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |55| 
        MOVZ      AR4,SP                ; [CPU_U] |55| 
        MOV       AH,*-SP[7]            ; [CPU_] |55| 
        LSR       AL,1                  ; [CPU_] |55| 
        SUBB      XAR4,#15              ; [CPU_U] |55| 
        MOVZ      AR0,AL                ; [CPU_] |55| 
        MOV       *+XAR4[AR0],AH        ; [CPU_] |55| 
        B         $C$L10,UNC            ; [CPU_] |55| 
        ; branch occurs ; [] |55| 
$C$L9:    
	.dwpsn	file "../lcd.c",line 56,column 12,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |56| 
        MOVZ      AR4,SP                ; [CPU_U] |56| 
        LSR       AL,1                  ; [CPU_] |56| 
        SUBB      XAR4,#15              ; [CPU_U] |56| 
        MOVZ      AR6,AL                ; [CPU_] |56| 
        MOVL      ACC,XAR4              ; [CPU_] |56| 
        ADDU      ACC,AR6               ; [CPU_] |56| 
        MOVL      XAR4,ACC              ; [CPU_] |56| 
        MOV       ACC,*-SP[7] << #8     ; [CPU_] |56| 
        ADD       *+XAR4[0],AL          ; [CPU_] |56| 
$C$L10:    
	.dwpsn	file "../lcd.c",line 34,column 25,is_stmt
        INC       *-SP[8]               ; [CPU_] |34| 
$C$L11:    
	.dwpsn	file "../lcd.c",line 34,column 14,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |34| 
        ADDB      AL,#2                 ; [CPU_] |34| 
        CMP       AL,*-SP[8]            ; [CPU_] |34| 
        B         $C$L1,HI              ; [CPU_] |34| 
        ; branchcc occurs ; [] |34| 
	.dwpsn	file "../lcd.c",line 58,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |58| 
        MOV       AH,*-SP[5]            ; [CPU_] |58| 
        MOVB      AL,#7                 ; [CPU_] |58| 
        MOVB      XAR5,#128             ; [CPU_] |58| 
        SUBB      XAR4,#15              ; [CPU_U] |58| 
        ADDB      AH,#2                 ; [CPU_] |58| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$26, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |58| 
        ; call occurs [#_I2C_Command] ; [] |58| 
        MOV       *-SP[6],AL            ; [CPU_] |58| 
	.dwpsn	file "../lcd.c",line 60,column 1,is_stmt
$C$L12:    
        SUBB      SP,#16                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("../lcd.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x3c)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.clink
	.global	_LCD_ReadText

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("LCD_ReadText")
	.dwattr $C$DW$28, DW_AT_low_pc(_LCD_ReadText)
	.dwattr $C$DW$28, DW_AT_high_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_LCD_ReadText")
	.dwattr $C$DW$28, DW_AT_external
	.dwattr $C$DW$28, DW_AT_TI_begin_file("../lcd.c")
	.dwattr $C$DW$28, DW_AT_TI_begin_line(0x3f)
	.dwattr $C$DW$28, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$28, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../lcd.c",line 63,column 55,is_stmt,address _LCD_ReadText

	.dwfde $C$DW$CIE, _LCD_ReadText
$C$DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pos")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg0]
$C$DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("text")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg12]
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lenght")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LCD_ReadText                 FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_LCD_ReadText:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("pos")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -1]
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("text")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -4]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("lenght")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -5]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -6]
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -7]
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -8]
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -14]
        MOV       *-SP[5],AH            ; [CPU_] |63| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |63| 
        MOV       *-SP[1],AL            ; [CPU_] |63| 
	.dwpsn	file "../lcd.c",line 67,column 3,is_stmt
        MOVB      AL,#128               ; [CPU_] |67| 
        ADD       AL,*-SP[1]            ; [CPU_] |67| 
        MOV       *-SP[14],AL           ; [CPU_] |67| 
	.dwpsn	file "../lcd.c",line 68,column 3,is_stmt
        MOVB      XAR5,#0               ; [CPU_] |68| 
        MOVB      AH,#1                 ; [CPU_] |68| 
        MOVZ      AR4,SP                ; [CPU_U] |68| 
        MOVB      AL,#7                 ; [CPU_] |68| 
        SUBB      XAR4,#14              ; [CPU_U] |68| 
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$39, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |68| 
        ; call occurs [#_I2C_Command] ; [] |68| 
        MOV       *-SP[6],AL            ; [CPU_] |68| 
	.dwpsn	file "../lcd.c",line 69,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |69| 
        CMPB      AL,#13                ; [CPU_] |69| 
        B         $C$L24,HIS            ; [CPU_] |69| 
        ; branchcc occurs ; [] |69| 
	.dwpsn	file "../lcd.c",line 70,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |70| 
        MOVB      AL,#8                 ; [CPU_] |70| 
        MOV       AH,*-SP[5]            ; [CPU_] |70| 
        MOVB      XAR5,#96              ; [CPU_] |70| 
        SUBB      XAR4,#14              ; [CPU_U] |70| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$40, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |70| 
        ; call occurs [#_I2C_Command] ; [] |70| 
        MOV       *-SP[6],AL            ; [CPU_] |70| 
	.dwpsn	file "../lcd.c",line 71,column 10,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |71| 
        B         $C$L23,UNC            ; [CPU_] |71| 
        ; branch occurs ; [] |71| 
$C$L13:    
	.dwpsn	file "../lcd.c",line 72,column 7,is_stmt
        TBIT      *-SP[8],#0            ; [CPU_] |72| 
        BF        $C$L14,TC             ; [CPU_] |72| 
        ; branchcc occurs ; [] |72| 
	.dwpsn	file "../lcd.c",line 72,column 23,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |72| 
        MOVZ      AR4,SP                ; [CPU_U] |72| 
        LSR       AL,1                  ; [CPU_] |72| 
        SUBB      XAR4,#14              ; [CPU_U] |72| 
        MOVZ      AR0,AL                ; [CPU_] |72| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |72| 
        ANDB      AL,#0xff              ; [CPU_] |72| 
        MOV       *-SP[6],AL            ; [CPU_] |72| 
        B         $C$L15,UNC            ; [CPU_] |72| 
        ; branch occurs ; [] |72| 
$C$L14:    
	.dwpsn	file "../lcd.c",line 73,column 12,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |73| 
        MOVZ      AR4,SP                ; [CPU_U] |73| 
        LSR       AL,1                  ; [CPU_] |73| 
        SUBB      XAR4,#14              ; [CPU_U] |73| 
        MOVZ      AR0,AL                ; [CPU_] |73| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |73| 
        LSR       AL,8                  ; [CPU_] |73| 
        MOV       *-SP[6],AL            ; [CPU_] |73| 
$C$L15:    
	.dwpsn	file "../lcd.c",line 74,column 7,is_stmt
        CMPB      AL,#160               ; [CPU_] |74| 
        B         $C$L16,LO             ; [CPU_] |74| 
        ; branchcc occurs ; [] |74| 
        CMPB      AL,#175               ; [CPU_] |74| 
        B         $C$L16,HI             ; [CPU_] |74| 
        ; branchcc occurs ; [] |74| 
	.dwpsn	file "../lcd.c",line 75,column 9,is_stmt
        ADD       AL,#65408             ; [CPU_] |75| 
        MOV       *-SP[7],AL            ; [CPU_] |75| 
	.dwpsn	file "../lcd.c",line 76,column 7,is_stmt
        B         $C$L22,UNC            ; [CPU_] |76| 
        ; branch occurs ; [] |76| 
$C$L16:    
	.dwpsn	file "../lcd.c",line 77,column 12,is_stmt
        CMPB      AL,#176               ; [CPU_] |77| 
        B         $C$L17,LO             ; [CPU_] |77| 
        ; branchcc occurs ; [] |77| 
        CMPB      AL,#191               ; [CPU_] |77| 
        B         $C$L17,HI             ; [CPU_] |77| 
        ; branchcc occurs ; [] |77| 
	.dwpsn	file "../lcd.c",line 78,column 9,is_stmt
        ADD       AL,#65408             ; [CPU_] |78| 
        MOV       *-SP[7],AL            ; [CPU_] |78| 
	.dwpsn	file "../lcd.c",line 79,column 7,is_stmt
        B         $C$L22,UNC            ; [CPU_] |79| 
        ; branch occurs ; [] |79| 
$C$L17:    
	.dwpsn	file "../lcd.c",line 80,column 12,is_stmt
        CMPB      AL,#193               ; [CPU_] |80| 
        B         $C$L18,LO             ; [CPU_] |80| 
        ; branchcc occurs ; [] |80| 
        CMPB      AL,#207               ; [CPU_] |80| 
        B         $C$L18,HI             ; [CPU_] |80| 
        ; branchcc occurs ; [] |80| 
	.dwpsn	file "../lcd.c",line 81,column 9,is_stmt
        ADD       AL,#65408             ; [CPU_] |81| 
        MOV       *-SP[7],AL            ; [CPU_] |81| 
	.dwpsn	file "../lcd.c",line 82,column 7,is_stmt
        B         $C$L22,UNC            ; [CPU_] |82| 
        ; branch occurs ; [] |82| 
$C$L18:    
	.dwpsn	file "../lcd.c",line 83,column 12,is_stmt
        CMPB      AL,#208               ; [CPU_] |83| 
        B         $C$L19,LO             ; [CPU_] |83| 
        ; branchcc occurs ; [] |83| 
        CMPB      AL,#223               ; [CPU_] |83| 
        B         $C$L19,HI             ; [CPU_] |83| 
        ; branchcc occurs ; [] |83| 
	.dwpsn	file "../lcd.c",line 84,column 9,is_stmt
        ADD       AL,#65408             ; [CPU_] |84| 
        MOV       *-SP[7],AL            ; [CPU_] |84| 
	.dwpsn	file "../lcd.c",line 85,column 7,is_stmt
        B         $C$L22,UNC            ; [CPU_] |85| 
        ; branch occurs ; [] |85| 
$C$L19:    
	.dwpsn	file "../lcd.c",line 86,column 12,is_stmt
        CMPB      AL,#225               ; [CPU_] |86| 
        B         $C$L20,LO             ; [CPU_] |86| 
        ; branchcc occurs ; [] |86| 
        CMPB      AL,#239               ; [CPU_] |86| 
        B         $C$L20,HI             ; [CPU_] |86| 
        ; branchcc occurs ; [] |86| 
	.dwpsn	file "../lcd.c",line 87,column 9,is_stmt
        ADD       AL,#65408             ; [CPU_] |87| 
        MOV       *-SP[7],AL            ; [CPU_] |87| 
	.dwpsn	file "../lcd.c",line 88,column 7,is_stmt
        B         $C$L22,UNC            ; [CPU_] |88| 
        ; branch occurs ; [] |88| 
$C$L20:    
	.dwpsn	file "../lcd.c",line 89,column 12,is_stmt
        CMPB      AL,#240               ; [CPU_] |89| 
        B         $C$L21,LO             ; [CPU_] |89| 
        ; branchcc occurs ; [] |89| 
        CMPB      AL,#255               ; [CPU_] |89| 
        B         $C$L21,HI             ; [CPU_] |89| 
        ; branchcc occurs ; [] |89| 
	.dwpsn	file "../lcd.c",line 90,column 9,is_stmt
        ADD       AL,#65408             ; [CPU_] |90| 
        MOV       *-SP[7],AL            ; [CPU_] |90| 
	.dwpsn	file "../lcd.c",line 91,column 7,is_stmt
        B         $C$L22,UNC            ; [CPU_] |91| 
        ; branch occurs ; [] |91| 
$C$L21:    
	.dwpsn	file "../lcd.c",line 92,column 12,is_stmt
        MOVB      *-SP[7],#32,UNC       ; [CPU_] |92| 
$C$L22:    
	.dwpsn	file "../lcd.c",line 93,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |93| 
        MOVZ      AR0,*-SP[8]           ; [CPU_] |93| 
        MOV       AL,*-SP[7]            ; [CPU_] |93| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |93| 
	.dwpsn	file "../lcd.c",line 71,column 23,is_stmt
        INC       *-SP[8]               ; [CPU_] |71| 
$C$L23:    
	.dwpsn	file "../lcd.c",line 71,column 14,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |71| 
        CMP       AL,*-SP[8]            ; [CPU_] |71| 
        B         $C$L13,HI             ; [CPU_] |71| 
        ; branchcc occurs ; [] |71| 
	.dwpsn	file "../lcd.c",line 96,column 1,is_stmt
$C$L24:    
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$28, DW_AT_TI_end_file("../lcd.c")
	.dwattr $C$DW$28, DW_AT_TI_end_line(0x60)
	.dwattr $C$DW$28, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$28

	.sect	".text"
	.clink
	.global	_LCD_WriteInteger

$C$DW$42	.dwtag  DW_TAG_subprogram, DW_AT_name("LCD_WriteInteger")
	.dwattr $C$DW$42, DW_AT_low_pc(_LCD_WriteInteger)
	.dwattr $C$DW$42, DW_AT_high_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_LCD_WriteInteger")
	.dwattr $C$DW$42, DW_AT_external
	.dwattr $C$DW$42, DW_AT_TI_begin_file("../lcd.c")
	.dwattr $C$DW$42, DW_AT_TI_begin_line(0x64)
	.dwattr $C$DW$42, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$42, DW_AT_TI_max_frame_size(-30)
	.dwpsn	file "../lcd.c",line 100,column 73,is_stmt,address _LCD_WriteInteger

	.dwfde $C$DW$CIE, _LCD_WriteInteger
$C$DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pos")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg0]
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("text")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg12]
$C$DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lenght")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg1]
$C$DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_name("signe")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_signe")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _LCD_WriteInteger             FR SIZE:  28           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter, 24 Auto,  0 SOE     *
;***************************************************************

_LCD_WriteInteger:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#28                ; [CPU_U] 
	.dwcfi	cfa_offset, -30
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("pos")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -5]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("text")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -8]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("lenght")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -9]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("signe")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_signe")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -10]
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -11]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -12]
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_breg20 -13]
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_breg20 -20]
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("itemp")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_itemp")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -24]
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("temp64")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_temp64")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -28]
        MOV       *-SP[10],AR5          ; [CPU_] |100| 
        MOV       *-SP[9],AH            ; [CPU_] |100| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |100| 
        MOV       *-SP[5],AL            ; [CPU_] |100| 
	.dwpsn	file "../lcd.c",line 101,column 23,is_stmt
        MOV       *-SP[13],#0           ; [CPU_] |101| 
	.dwpsn	file "../lcd.c",line 104,column 17,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |104| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |104| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |104| 
        MOVL      *-SP[28],ACC          ; [CPU_] |104| 
        MOVL      *-SP[26],XAR6         ; [CPU_] |104| 
	.dwpsn	file "../lcd.c",line 105,column 3,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |105| 
        BF        $C$L26,EQ             ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "../lcd.c",line 106,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |106| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |106| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |106| 
        MOVL      *-SP[24],ACC          ; [CPU_] |106| 
        MOVL      *-SP[22],XAR6         ; [CPU_] |106| 
	.dwpsn	file "../lcd.c",line 107,column 5,is_stmt
        CMP64     ACC:P                 ; [CPU_] |107| 
        MOVL      P,*-SP[24]            ; [CPU_] |107| 
        MOVL      ACC,*-SP[22]          ; [CPU_] |107| 
        CMP64     ACC:P                 ; [CPU_] |107| 
        B         $C$L25,GEQ            ; [CPU_] |107| 
        ; branchcc occurs ; [] |107| 
	.dwpsn	file "../lcd.c",line 108,column 7,is_stmt
        MOVL      P,*-SP[24]            ; [CPU_] |108| 
        MOVL      ACC,*-SP[22]          ; [CPU_] |108| 
        NEG64     ACC:P                 ; [CPU_] |108| 
        MOVL      *-SP[24],P            ; [CPU_] |108| 
        MOVL      *-SP[22],ACC          ; [CPU_] |108| 
	.dwpsn	file "../lcd.c",line 109,column 7,is_stmt
        MOVB      *-SP[13],#1,UNC       ; [CPU_] |109| 
	.dwpsn	file "../lcd.c",line 110,column 7,is_stmt
        MOVB      *-SP[19],#173,UNC     ; [CPU_] |110| 
$C$L25:    
	.dwpsn	file "../lcd.c",line 112,column 5,is_stmt
        MOVL      XAR6,*-SP[22]         ; [CPU_] |112| 
        MOVL      ACC,*-SP[24]          ; [CPU_] |112| 
        MOVL      *-SP[28],ACC          ; [CPU_] |112| 
        MOVL      *-SP[26],XAR6         ; [CPU_] |112| 
$C$L26:    
	.dwpsn	file "../lcd.c",line 114,column 3,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |114| 
        ADD       AL,#16512             ; [CPU_] |114| 
        MOV       *-SP[20],AL           ; [CPU_] |114| 
	.dwpsn	file "../lcd.c",line 116,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |116| 
        CMPB      AL,#13                ; [CPU_] |116| 
        B         $C$L31,HIS            ; [CPU_] |116| 
        ; branchcc occurs ; [] |116| 
	.dwpsn	file "../lcd.c",line 117,column 5,is_stmt
        B         $C$L30,UNC            ; [CPU_] |117| 
        ; branch occurs ; [] |117| 
$C$L27:    
	.dwpsn	file "../lcd.c",line 118,column 7,is_stmt
        SUB       AL,*-SP[13]           ; [CPU_] |118| 
        MOVL      XAR4,#_DIVISOR        ; [CPU_U] |118| 
        ADDB      AL,#-1                ; [CPU_] |118| 
        MOVU      ACC,AL                ; [CPU_] |118| 
        LSL       ACC,2                 ; [CPU_] |118| 
        ADDL      XAR4,ACC              ; [CPU_] |118| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |118| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |118| 
        MOVL      *-SP[4],ACC           ; [CPU_] |118| 
        MOVL      *-SP[2],XAR6          ; [CPU_] |118| 
        MOVL      P,*-SP[28]            ; [CPU_] |118| 
        MOVL      ACC,*-SP[26]          ; [CPU_] |118| 
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("ULL$$DIV")
	.dwattr $C$DW$57, DW_AT_TI_call
        LCR       #ULL$$DIV             ; [CPU_] |118| 
        ; call occurs [#ULL$$DIV] ; [] |118| 
        MOV       *-SP[11],P            ; [CPU_] |118| 
	.dwpsn	file "../lcd.c",line 119,column 7,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |119| 
        MOVL      XAR4,#_DIVISOR        ; [CPU_U] |119| 
        SUB       AL,*-SP[13]           ; [CPU_] |119| 
        ADDB      AL,#-1                ; [CPU_] |119| 
        MOVU      ACC,AL                ; [CPU_] |119| 
        LSL       ACC,2                 ; [CPU_] |119| 
        ADDL      XAR4,ACC              ; [CPU_] |119| 
        MOVL      XAR6,*+XAR4[0]        ; [CPU_] |119| 
        MOVL      XAR7,*+XAR4[2]        ; [CPU_] |119| 
        MOVL      *-SP[4],XAR6          ; [CPU_] |119| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |119| 
        MOV       PL,*-SP[11]           ; [CPU_] |119| 
        MOV       PH,#0                 ; [CPU_] |119| 
        MOVB      ACC,#0                ; [CPU_] |119| 
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$58, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |119| 
        ; call occurs [#LL$$MPY] ; [] |119| 
        MOVL      XAR7,ACC              ; [CPU_] |119| 
        MOVL      XAR6,P                ; [CPU_] |119| 
        MOVL      ACC,*-SP[26]          ; [CPU_] |119| 
        MOVL      P,*-SP[28]            ; [CPU_] |119| 
        SUBUL     P,XAR6                ; [CPU_] |119| 
        MOVL      *-SP[28],P            ; [CPU_] |119| 
        SUBBL     ACC,XAR7              ; [CPU_] |119| 
        MOVL      *-SP[26],ACC          ; [CPU_] |119| 
	.dwpsn	file "../lcd.c",line 120,column 7,is_stmt
        MOVB      AL,#176               ; [CPU_] |120| 
        ADD       AL,*-SP[11]           ; [CPU_] |120| 
        MOV       *-SP[12],AL           ; [CPU_] |120| 
	.dwpsn	file "../lcd.c",line 121,column 7,is_stmt
        TBIT      *-SP[13],#0           ; [CPU_] |121| 
        BF        $C$L28,TC             ; [CPU_] |121| 
        ; branchcc occurs ; [] |121| 
	.dwpsn	file "../lcd.c",line 121,column 23,is_stmt
        MOVZ      AR0,*-SP[13]          ; [CPU_] |121| 
        MOVZ      AR4,SP                ; [CPU_U] |121| 
        MOV       AH,*-SP[12]           ; [CPU_] |121| 
        SUBB      XAR4,#20              ; [CPU_U] |121| 
        ADDB      XAR0,#2               ; [CPU_] |121| 
        MOV       AL,AR0                ; [CPU_] |121| 
        LSR       AL,1                  ; [CPU_] |121| 
        MOVZ      AR0,AL                ; [CPU_] |121| 
        MOV       *+XAR4[AR0],AH        ; [CPU_] |121| 
        B         $C$L29,UNC            ; [CPU_] |121| 
        ; branch occurs ; [] |121| 
$C$L28:    
	.dwpsn	file "../lcd.c",line 122,column 12,is_stmt
        MOVZ      AR5,*-SP[13]          ; [CPU_] |122| 
        MOVZ      AR4,SP                ; [CPU_U] |122| 
        SUBB      XAR4,#20              ; [CPU_U] |122| 
        ADDB      XAR5,#2               ; [CPU_] |122| 
        MOV       AL,AR5                ; [CPU_] |122| 
        LSR       AL,1                  ; [CPU_] |122| 
        MOVZ      AR5,AL                ; [CPU_] |122| 
        MOVL      ACC,XAR4              ; [CPU_] |122| 
        ADDU      ACC,AR5               ; [CPU_] |122| 
        MOVL      XAR4,ACC              ; [CPU_] |122| 
        MOV       ACC,*-SP[12] << #8    ; [CPU_] |122| 
        ADD       *+XAR4[0],AL          ; [CPU_] |122| 
$C$L29:    
	.dwpsn	file "../lcd.c",line 123,column 7,is_stmt
        INC       *-SP[13]              ; [CPU_] |123| 
$C$L30:    
	.dwpsn	file "../lcd.c",line 117,column 12,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |117| 
        CMP       AL,*-SP[13]           ; [CPU_] |117| 
        B         $C$L27,HI             ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
	.dwpsn	file "../lcd.c",line 125,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |125| 
        MOV       AH,*-SP[9]            ; [CPU_] |125| 
        MOVB      AL,#7                 ; [CPU_] |125| 
        MOVB      XAR5,#128             ; [CPU_] |125| 
        SUBB      XAR4,#20              ; [CPU_U] |125| 
        ADDB      AH,#2                 ; [CPU_] |125| 
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$59, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |125| 
        ; call occurs [#_I2C_Command] ; [] |125| 
        MOV       *-SP[11],AL           ; [CPU_] |125| 
	.dwpsn	file "../lcd.c",line 127,column 1,is_stmt
$C$L31:    
        SUBB      SP,#28                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$42, DW_AT_TI_end_file("../lcd.c")
	.dwattr $C$DW$42, DW_AT_TI_end_line(0x7f)
	.dwattr $C$DW$42, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$42

	.sect	".text"
	.clink
	.global	_LCD_ReadInteger

$C$DW$61	.dwtag  DW_TAG_subprogram, DW_AT_name("LCD_ReadInteger")
	.dwattr $C$DW$61, DW_AT_low_pc(_LCD_ReadInteger)
	.dwattr $C$DW$61, DW_AT_high_pc(0x00)
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_LCD_ReadInteger")
	.dwattr $C$DW$61, DW_AT_external
	.dwattr $C$DW$61, DW_AT_TI_begin_file("../lcd.c")
	.dwattr $C$DW$61, DW_AT_TI_begin_line(0x81)
	.dwattr $C$DW$61, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$61, DW_AT_TI_max_frame_size(-24)
	.dwpsn	file "../lcd.c",line 129,column 60,is_stmt,address _LCD_ReadInteger

	.dwfde $C$DW$CIE, _LCD_ReadInteger
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pos")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg0]
$C$DW$63	.dwtag  DW_TAG_formal_parameter, DW_AT_name("text")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg12]
$C$DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lenght")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LCD_ReadInteger              FR SIZE:  22           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter, 18 Auto,  0 SOE     *
;***************************************************************

_LCD_ReadInteger:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -24
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("pos")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -5]
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("text")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_text")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -8]
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("lenght")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_lenght")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -9]
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -10]
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -11]
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("retry")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_retry")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -12]
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -18]
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("temp64")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_temp64")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -22]
        MOV       *-SP[9],AH            ; [CPU_] |129| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |129| 
        MOV       *-SP[5],AL            ; [CPU_] |129| 
	.dwpsn	file "../lcd.c",line 130,column 13,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |130| 
	.dwpsn	file "../lcd.c",line 131,column 19,is_stmt
        MOVB      *-SP[12],#3,UNC       ; [CPU_] |131| 
	.dwpsn	file "../lcd.c",line 133,column 17,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |133| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |133| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |133| 
        MOVL      *-SP[22],ACC          ; [CPU_] |133| 
        MOVL      *-SP[20],XAR6         ; [CPU_] |133| 
	.dwpsn	file "../lcd.c",line 134,column 3,is_stmt
        B         $C$L34,UNC            ; [CPU_] |134| 
        ; branch occurs ; [] |134| 
$C$L32:    
	.dwpsn	file "../lcd.c",line 135,column 5,is_stmt
        MOVB      AL,#128               ; [CPU_] |135| 
        ADD       AL,*-SP[5]            ; [CPU_] |135| 
        MOV       *-SP[18],AL           ; [CPU_] |135| 
	.dwpsn	file "../lcd.c",line 136,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |136| 
        MOVB      AH,#1                 ; [CPU_] |136| 
        MOVB      XAR5,#0               ; [CPU_] |136| 
        MOVB      AL,#7                 ; [CPU_] |136| 
        SUBB      XAR4,#18              ; [CPU_U] |136| 
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$73, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |136| 
        ; call occurs [#_I2C_Command] ; [] |136| 
        MOV       *-SP[10],AL           ; [CPU_] |136| 
	.dwpsn	file "../lcd.c",line 137,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |137| 
        CMPB      AL,#13                ; [CPU_] |137| 
        B         $C$L33,HIS            ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
        MOV       AL,*-SP[10]           ; [CPU_] |137| 
        BF        $C$L33,EQ             ; [CPU_] |137| 
        ; branchcc occurs ; [] |137| 
	.dwpsn	file "../lcd.c",line 138,column 7,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |138| 
        MOVB      AL,#2                 ; [CPU_] |138| 
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$74, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |138| 
        ; call occurs [#_SEM_pend] ; [] |138| 
	.dwpsn	file "../lcd.c",line 139,column 7,is_stmt
        MOVB      AL,#8                 ; [CPU_] |139| 
        MOVZ      AR4,SP                ; [CPU_U] |139| 
        MOV       AH,*-SP[9]            ; [CPU_] |139| 
        MOVB      XAR5,#96              ; [CPU_] |139| 
        SUBB      XAR4,#18              ; [CPU_U] |139| 
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$75, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |139| 
        ; call occurs [#_I2C_Command] ; [] |139| 
        MOV       *-SP[10],AL           ; [CPU_] |139| 
$C$L33:    
	.dwpsn	file "../lcd.c",line 141,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |141| 
        MOVB      AL,#2                 ; [CPU_] |141| 
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$76, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |141| 
        ; call occurs [#_SEM_pend] ; [] |141| 
	.dwpsn	file "../lcd.c",line 142,column 5,is_stmt
        DEC       *-SP[12]              ; [CPU_] |142| 
$C$L34:    
	.dwpsn	file "../lcd.c",line 134,column 10,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |134| 
        BF        $C$L35,EQ             ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
        MOV       AL,*-SP[10]           ; [CPU_] |134| 
        BF        $C$L32,EQ             ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
$C$L35:    
	.dwpsn	file "../lcd.c",line 144,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |144| 
        CMPB      AL,#13                ; [CPU_] |144| 
        B         $C$L40,HIS            ; [CPU_] |144| 
        ; branchcc occurs ; [] |144| 
        MOV       AL,*-SP[10]           ; [CPU_] |144| 
        BF        $C$L40,EQ             ; [CPU_] |144| 
        ; branchcc occurs ; [] |144| 
	.dwpsn	file "../lcd.c",line 145,column 10,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |145| 
        B         $C$L39,UNC            ; [CPU_] |145| 
        ; branch occurs ; [] |145| 
$C$L36:    
	.dwpsn	file "../lcd.c",line 146,column 7,is_stmt
        MOVB      ACC,#10               ; [CPU_] |146| 
        MOVL      *-SP[4],ACC           ; [CPU_] |146| 
        MOV       *-SP[2],#0            ; [CPU_] |146| 
        MOV       *-SP[1],#0            ; [CPU_] |146| 
        MOVL      P,*-SP[22]            ; [CPU_] |146| 
        MOVL      ACC,*-SP[20]          ; [CPU_] |146| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$77, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |146| 
        ; call occurs [#LL$$MPY] ; [] |146| 
        MOVL      *-SP[22],P            ; [CPU_] |146| 
        MOVL      *-SP[20],ACC          ; [CPU_] |146| 
	.dwpsn	file "../lcd.c",line 147,column 7,is_stmt
        TBIT      *-SP[11],#0           ; [CPU_] |147| 
        BF        $C$L37,TC             ; [CPU_] |147| 
        ; branchcc occurs ; [] |147| 
	.dwpsn	file "../lcd.c",line 147,column 23,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |147| 
        MOVZ      AR4,SP                ; [CPU_U] |147| 
        LSR       AL,1                  ; [CPU_] |147| 
        SUBB      XAR4,#18              ; [CPU_U] |147| 
        MOVZ      AR0,AL                ; [CPU_] |147| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |147| 
        ANDB      AL,#0xff              ; [CPU_] |147| 
        MOV       *-SP[10],AL           ; [CPU_] |147| 
        B         $C$L38,UNC            ; [CPU_] |147| 
        ; branch occurs ; [] |147| 
$C$L37:    
	.dwpsn	file "../lcd.c",line 148,column 12,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |148| 
        MOVZ      AR4,SP                ; [CPU_U] |148| 
        LSR       AL,1                  ; [CPU_] |148| 
        SUBB      XAR4,#18              ; [CPU_U] |148| 
        MOVZ      AR0,AL                ; [CPU_] |148| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |148| 
        LSR       AL,8                  ; [CPU_] |148| 
        MOV       *-SP[10],AL           ; [CPU_] |148| 
$C$L38:    
	.dwpsn	file "../lcd.c",line 149,column 7,is_stmt
        MOVB      AL,#176               ; [CPU_] |149| 
        SUB       *-SP[10],AL           ; [CPU_] |149| 
	.dwpsn	file "../lcd.c",line 150,column 7,is_stmt
        MOV       PH,#0                 ; [CPU_] |150| 
        MOV       PL,*-SP[10]           ; [CPU_] |150| 
        MOVB      ACC,#0                ; [CPU_] |150| 
        ADDUL     P,*-SP[22]            ; [CPU_] |150| 
        ADDCL     ACC,*-SP[20]          ; [CPU_] |150| 
        MOVL      *-SP[22],P            ; [CPU_] |150| 
        MOVL      *-SP[20],ACC          ; [CPU_] |150| 
	.dwpsn	file "../lcd.c",line 145,column 23,is_stmt
        INC       *-SP[11]              ; [CPU_] |145| 
$C$L39:    
	.dwpsn	file "../lcd.c",line 145,column 14,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |145| 
        CMP       AL,*-SP[11]           ; [CPU_] |145| 
        B         $C$L36,HI             ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
	.dwpsn	file "../lcd.c",line 152,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |152| 
        MOVL      XAR6,*-SP[20]         ; [CPU_] |152| 
        MOVL      ACC,*-SP[22]          ; [CPU_] |152| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |152| 
        MOVL      *+XAR4[2],XAR6        ; [CPU_] |152| 
	.dwpsn	file "../lcd.c",line 154,column 1,is_stmt
$C$L40:    
        SUBB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$61, DW_AT_TI_end_file("../lcd.c")
	.dwattr $C$DW$61, DW_AT_TI_end_line(0x9a)
	.dwattr $C$DW$61, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$61

	.sect	".text"
	.clink
	.global	_LCD_SetPosition

$C$DW$79	.dwtag  DW_TAG_subprogram, DW_AT_name("LCD_SetPosition")
	.dwattr $C$DW$79, DW_AT_low_pc(_LCD_SetPosition)
	.dwattr $C$DW$79, DW_AT_high_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_LCD_SetPosition")
	.dwattr $C$DW$79, DW_AT_external
	.dwattr $C$DW$79, DW_AT_TI_begin_file("../lcd.c")
	.dwattr $C$DW$79, DW_AT_TI_begin_line(0x9c)
	.dwattr $C$DW$79, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$79, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../lcd.c",line 156,column 46,is_stmt,address _LCD_SetPosition

	.dwfde $C$DW$CIE, _LCD_SetPosition
$C$DW$80	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pos")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg0]
$C$DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_name("blink")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_blink")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LCD_SetPosition              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_LCD_SetPosition:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("pos")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_pos")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_breg20 -1]
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("blink")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_blink")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_breg20 -2]
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("buffer")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_buffer")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[2],AH            ; [CPU_] |156| 
        MOV       *-SP[1],AL            ; [CPU_] |156| 
	.dwpsn	file "../lcd.c",line 158,column 3,is_stmt
        MOVB      AL,#128               ; [CPU_] |158| 
        ADD       AL,*-SP[1]            ; [CPU_] |158| 
        MOV       *-SP[3],AL            ; [CPU_] |158| 
	.dwpsn	file "../lcd.c",line 159,column 3,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |159| 
        BF        $C$L41,EQ             ; [CPU_] |159| 
        ; branchcc occurs ; [] |159| 
	.dwpsn	file "../lcd.c",line 159,column 14,is_stmt
        ADD       *-SP[3],#3840         ; [CPU_] |159| 
        B         $C$L42,UNC            ; [CPU_] |159| 
        ; branch occurs ; [] |159| 
$C$L41:    
	.dwpsn	file "../lcd.c",line 160,column 8,is_stmt
        ADD       *-SP[3],#3072         ; [CPU_] |160| 
$C$L42:    
	.dwpsn	file "../lcd.c",line 161,column 3,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |161| 
        MOVB      AL,#7                 ; [CPU_] |161| 
        MOVB      AH,#2                 ; [CPU_] |161| 
        MOVB      XAR5,#0               ; [CPU_] |161| 
        SUBB      XAR4,#3               ; [CPU_U] |161| 
$C$DW$85	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$85, DW_AT_low_pc(0x00)
	.dwattr $C$DW$85, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$85, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |161| 
        ; call occurs [#_I2C_Command] ; [] |161| 
	.dwpsn	file "../lcd.c",line 162,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$79, DW_AT_TI_end_file("../lcd.c")
	.dwattr $C$DW$79, DW_AT_TI_end_line(0xa2)
	.dwattr $C$DW$79, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$79

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_SEM_pend
	.global	_I2C_Command
	.global	_TSK_timerSem
	.global	ULL$$DIV
	.global	LL$$MPY

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x08)
$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$87, DW_AT_name("wListElem")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$88, DW_AT_name("wCount")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$89, DW_AT_name("fxn")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26

$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$21	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$21, DW_AT_address_class(0x16)
$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x04)
$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$90, DW_AT_name("next")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$91, DW_AT_name("prev")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$27	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$27, DW_AT_address_class(0x16)

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x10)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$92, DW_AT_name("job")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$93, DW_AT_name("count")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$94, DW_AT_name("pendQ")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$95, DW_AT_name("name")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33

$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$22)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x16)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)

$C$DW$T$50	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x02)
$C$DW$97	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$97, DW_AT_upper_bound(0x01)
	.dwendtag $C$DW$T$50


$C$DW$T$53	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x07)
$C$DW$98	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$98, DW_AT_upper_bound(0x06)
	.dwendtag $C$DW$T$53


$C$DW$T$55	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x06)
$C$DW$99	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$99, DW_AT_upper_bound(0x05)
	.dwendtag $C$DW$T$55

$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$40	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$40, DW_AT_address_class(0x16)
$C$DW$100	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$15)
$C$DW$T$58	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$100)

$C$DW$T$59	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x30)
$C$DW$101	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$101, DW_AT_upper_bound(0x0b)
	.dwendtag $C$DW$T$59

$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$31	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$31, DW_AT_address_class(0x16)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg0]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_reg1]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_reg2]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_reg3]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg20]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg21]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg22]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg23]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg24]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg25]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg26]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg28]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg29]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg30]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg31]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x20]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x21]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x22]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x23]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x24]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x25]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x26]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg4]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg6]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_reg8]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_reg10]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_reg12]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_reg14]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg16]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg17]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg18]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg19]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg5]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg7]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg9]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg11]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg13]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg15]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x30]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x33]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x34]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x37]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x38]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x40]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x43]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x44]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x47]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x48]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x49]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x27]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x28]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg27]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

