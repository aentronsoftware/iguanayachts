/******************************************************************************

 param.c - parameters

 ------------------------------------------------------------------------------
 Author: PTM: RP           Date: 15.10.2010
 For   : Smtec
 Compil: Code Composer     Target: DSP280x
 descr.: system parameters :  encoder etc.
 Edit  :
 *****************************************************************************/

#define _PARAM_

#include "gatewaycfg.h"

#include "uc.h"
#include "dspspecs.h"
#include "gateway_dict.h"   //for ODP_VersionParameters, also #include <includes.h> (festival)
#include "F2806x_I2c_defines.h"
#include "i2c.h"
#include "convert.h"
#include "param.h"
#include "ERROR.H"
#include "crc.h"
#include "crc_tbl.h"

#define PARAM_CODE 147993112
uint16 PAR_Flag = 1;

#define LOG_NB    8
T_StatLog TimeLog[LOG_NB] = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
uint8 TimeLogIndex = LOG_NB;

uint32 PAR_Capacity_Left = 0;  //capacity left since last charge
uint32 PAR_Capacity_Total;             //total capacity of the module
uint64 PAR_Capacity_TotalLife_Used = 0; //total capacity received during charge of the module
uint32 PAR_Operating_Hours_day = 0; //total Operating Hourr

const T_StatIndexes STAT_PAGE1[11] =
{{(void*)&ODP_OnTime, 4, 1024}                      //0 on time
,{(void*)&PAR_Flag, 2, 1028}                        //1 flag
,{(void*)&ODV_Gateway_Date_Time, 8, 1030}           //2 date_time
,{(void*)&ODV_Gateway_Errorcode, 4, 1038}           //3 error
,{(void*)&ODP_Temperature_Max, 2, 1042}             //4 max temp
,{(void*)&ODP_Current_Max, 2, 1044}                 //6 max cur
,{(void*)&ODP_Current_Min, 2, 1046}                 //7 min cur
,{(void*)&TimeLogIndex, 2, 1048}                    //8 index log
,{(void*)&ODP_Voltage_Max, 2, 1050}                 //9 max voltage
,{(void*)&ODP_Voltage_Min, 2, 1052}                 //10 min voltage
,{(void*)&ODP_Temperature_Min, 2, 1054}             //5 min temp
};

const T_StatIndexes STAT_PAGE2[4] =
{{(void*)&TimeLog[0].date, 8, 1056}                 //0 date
,{(void*)&TimeLog[1].date, 8, 1064}                 //1 date
,{(void*)&TimeLog[2].date, 8, 1072}                 //2 date
,{(void*)&TimeLog[3].date, 8, 1080}                 //3 date
};

const T_StatIndexes STAT_PAGE3[LOG_NB] =
{{(void*)&TimeLog[0].error, 4, 1088}                 //0 error
,{(void*)&TimeLog[1].error, 4, 1092}                 //1 error
,{(void*)&TimeLog[2].error, 4, 1096}                 //2 error
,{(void*)&TimeLog[3].error, 4, 1100}                 //3 error
,{(void*)&TimeLog[4].error, 4, 1104}                 //4 error,
,{(void*)&TimeLog[5].error, 4, 1108}                 //5 error
,{(void*)&TimeLog[6].error, 4, 1112}                 //6 error
,{(void*)&TimeLog[7].error, 4, 1116}                 //7 error
};

const T_StatIndexes STAT_PAGE4[4] =
{{(void*)&TimeLog[4].date, 8, 1120}                  //0 date
,{(void*)&TimeLog[5].date, 8, 1128}                  //1 date
,{(void*)&TimeLog[6].date, 8, 1136}                  //2 date
,{(void*)&TimeLog[7].date, 8, 1144}                  //3 date
};

const T_StatIndexes STAT_PAGE5[6] =
{{(void*)&PAR_Capacity_Left, 4, 1152}                //0 capacity left
,{(void*)&PAR_Capacity_TotalLife_Used, 8, 1156}      //1 total cap used
,{(void*)&PAR_Operating_Hours_day, 4, 1164}		     //4 Working hours
,{(void*)&ODP_CommError_OverTemp_ErrCounter, 1, 1168}                   //1 total cap used
,{(void*)&ODP_CommError_OverVoltage_ErrCounter, 1, 1169}                   //1 total cap used
,{(void*)&ODP_CommError_LowVoltage_ErrCounter, 1, 1170}                   //1 total cap used
};

const T_StatPages PAR_EEPROM_INDEXES[PAR_STAT_PAGE] =
//page 32 = statistic page 1
{{EEPROM_PAGESIZE, 1024, sizeof(STAT_PAGE1)/sizeof(T_StatIndexes), STAT_PAGE1}
,{EEPROM_PAGESIZE, 1056, sizeof(STAT_PAGE2)/sizeof(T_StatIndexes), STAT_PAGE2}
,{EEPROM_PAGESIZE, 1088, sizeof(STAT_PAGE3)/sizeof(T_StatIndexes), STAT_PAGE3}
,{EEPROM_PAGESIZE, 1120, sizeof(STAT_PAGE4)/sizeof(T_StatIndexes), STAT_PAGE4}
,{EEPROM_PAGESIZE, 1152, sizeof(STAT_PAGE5)/sizeof(T_StatIndexes), STAT_PAGE5}
};


uint16 PAR_StatEepromCommandState[PAR_STAT_PAGE]=
{EEPROM_COMMAND_NONE
,EEPROM_COMMAND_NONE
,EEPROM_COMMAND_NONE
,EEPROM_COMMAND_NONE
,EEPROM_COMMAND_NONE
};


uint32 WritePermanentParam(CO_Data* d, int index);
uint32 ComputeParamCRC(void);

void PAR_AddLog(void){
  I2C_Command(RTC_READ,(char *)&ODV_Gateway_Date_Time,7,0);
  if (TimeLogIndex < LOG_NB-1) TimeLogIndex++;
  else TimeLogIndex = 0;
  TimeLog[TimeLogIndex].date = ODV_Gateway_Date_Time;
  TimeLog[TimeLogIndex].error = ODV_Gateway_Errorcode;
  PAR_WriteStatisticParam(-1);
  PAR_WriteStatisticParam(-3);
  if (TimeLogIndex < 4){
    PAR_WriteStatisticParam(-2);
  }else PAR_WriteStatisticParam(-4);
}

void PAR_GetLogNB(uint8 nb){
  ODV_Gateway_Date_Time = TimeLog[nb].date;
  ODV_Gateway_Errorcode = TimeLog[nb].error;
}

/**
 * @ingroup
 * @fn PAR_TestEeprom
 * @brief test the whole eeprom and if ok set default parameters else return FALSE
 * @param void
 * - type:
 * - range:
 * - description:
 * @return
 * - type: unsigned char
 * - range: true(success), false(failed)
 * - description: 0 test bad 1 test ok
 */
uint8 PAR_TestEeprom(void){
  int16 i, j;
  bool1 res = TRUE;
  uint8 buffer[EEPROM_PAGESIZE/2];
  for (i=0;i<EEPROM_PAGESIZE/2;i++)
    buffer[i] = ((i+1)<<8)+i+2;
  j=0;
  while (j<EEPROM_SIZE && res){
    res = I2C_Command(MEM_WRITE, (char*)&buffer, EEPROM_PAGESIZE, j);
    TSK_sleep(2);
    if (res)
      res = I2C_Command(MEM_READ, (char*)&buffer, EEPROM_PAGESIZE, j);
    TSK_sleep(2);
    if (res){
      for (i=0;i<EEPROM_PAGESIZE/2;i++){
        if (buffer[i] != ((i+1)<<8)+i+2){
          res = FALSE;
          break;
        }
      }
    }
    else
      break;
    j += EEPROM_PAGESIZE;
  }
  if (res)
    res = PAR_SetDefaultParameters(BoardODdata);
  return res;
}

const uint16 EepromIndexesSize = sizeof(ODI_EEPROM_INDEXES)/sizeof(T_EepromIndexes);
/**
 * @ingroup
 * @fn ComputeParamCRC
 * @brief
 * @param
 * - type: void
 * - range:
 * - description:
 * @return
 * - type: unsigned long
 * - range:
 * - description: return crc32 if ok else return 0
 */
uint32 ComputeParamCRC(void){
  uint16 nb_data = ODI_EEPROM_INDEXES[EepromIndexesSize-1].address + ODI_EEPROM_INDEXES[EepromIndexesSize-1].size;
  uint16 data[EEPROM_PAGESIZE/2], j = 0, size = EEPROM_PAGESIZE, indexcrc;
  uint32 temp = INIT_CRC32;
  bool1 res;
  if (PAR_FindODPermanentParamIndex(BoardODdata, ODA_PAR_CrcParameters, &indexcrc)){
    indexcrc=ODI_EEPROM_INDEXES[indexcrc].address/2;
  }
  else{
    indexcrc=1;
  }
  while(j < nb_data){
    if (j + size > nb_data) size = nb_data - j;
    res = I2C_Command(MEM_READ, (char*)&data, size, j);
    //remove crc value
    if (j == 0){
      data[indexcrc] = 0;
      data[indexcrc+1] = 0;
    }
    if (res)
      temp = getCRC32_cpu(temp, (uint16*)&data, EVEN, size);
    else return 0;
    j += size;
  }
  return temp;
}


/**
 * @ingroup
 * @fn bool1 PAR_UpdateCode(bool1 set)
 * @brief Update the eeprom param code.
 * The param code is set after the eeprom is initialized
 * @param set
 * - type:bool1
 * - range:
 * - description: set(true) or reset(false) the
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_UpdateCode(bool1 set)
{
  uint32 buffer = 0;
  if (set)
    buffer = PARAM_CODE;
  return(I2C_Command(MEM_WRITE, (char*)&buffer, 4, EEPROM_SIZE - 4));
}

/**
 * @ingroup
 * @fn bool1 PAR_SetDefaultParameters(CO_Data* d)
 * @brief Set the parameters to the default value from the object dictionary.
 * The parameters from the flash code (already in the OD)
 * are written in the EEPROM
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_SetDefaultParameters(CO_Data* d){
  uint16 i = 0;
  bool1 res = FALSE;
  uint16 index_param;
  uint32 error_code = OD_SUCCESSFUL;

  if(!PAR_FindODPermanentParamIndex(BoardODdata, ODA_PAR_VersionParameters, &index_param)){
    index_param = 0; //Don't write the version before all parameters are written successfully
  }

  while((i< d->eeprom_size) && (error_code == OD_SUCCESSFUL)){
    if (i == index_param) ++i;
    error_code = WritePermanentParam(d, i++);
  }
  if (error_code == OD_SUCCESSFUL){
    PAR_WriteStatisticParam(-1);
    res = PAR_UpdateCode(TRUE);
    if(res){
      error_code = WritePermanentParam(d, index_param); //Write parameter version
      res = error_code == OD_SUCCESSFUL;
    }
  }
  if (res){
    if (!PAR_FindODPermanentParamIndex(BoardODdata, ODA_PAR_CrcParameters, &index_param)) index_param=1;
    ODP_CrcParameters = ComputeParamCRC();
    error_code = WritePermanentParam(BoardODdata, index_param);//1 = CrcParameters
    res = error_code == OD_SUCCESSFUL;
  }
  return (res);
}

/**
 * @ingroup
 * @fn bool1 PAR_InitParam(CO_Data* d)
 * @brief Init the parameters from the object dictionary.
 * If the last position of the EEPROM don't have the value PARAM_CODE
 * the parameters from the flash code (already in the OD)
 * are written in the EEPROM
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_InitParam(CO_Data* d)
{
  uint32 buffer = 0;
  bool1 res;
  uint16 index_param;
  T_EepromIndexes indexes;

  //Read the last position of the EEPROM to check if PARAM_CODE is set
  res = I2C_Command(MEM_READ, (char*)&buffer, 4, EEPROM_SIZE - 4);
  if (!res) {//retry
    TSK_sleep(2);
    res = I2C_Command(MEM_READ, (char*)&buffer, 4, EEPROM_SIZE - 4);
  }
  if (res) {
    if (buffer == PARAM_CODE) {
      TSK_sleep(2);
      buffer = 0;
      if(!PAR_FindODPermanentParamIndex(BoardODdata, ODA_PAR_VersionParameters, &index_param)){
        index_param = 0;
      }
      indexes = d->eeprom_index[index_param];
      res = I2C_Command(MEM_READ, (char*)&buffer, indexes.size, indexes.address);
      if (res) {
        if (buffer == ODP_VersionParameters) {
          res = PAR_ReadAllPermanentParam(d);
          if(res && (ODP_CrcParameters == ComputeParamCRC()))
            res = PAR_ReadAllStatisticParam();
          else{
            res = FALSE;
            ERR_HandleWarning(WARN_CRC);
          }
        }
        else {
          res = PAR_ReadAllStatisticParam();
          res = PAR_SetDefaultParameters(d);
        }
      }
    }
    else {
      res = PAR_SetDefaultParameters(d);
    }
  }
  return (res);
}

/**
 * @ingroup
 * @fn uint32 PAR_ReadPermanentParam(CO_Data* d, int index)
 * @brief Read one permanent parameter from EEPROM
 * and store it in the object dictionary
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @param
 * - type: int
 * - range: 0..EEPROM_INDEXES_MAX-1
 * - description: index of the parameter in the EEPROM array
 * @return
 * - type:unsigned long
 * - range: OD_SUCCESSFUL if OK, other value if problem
 * - description: Error code
 */
uint32 PAR_ReadPermanentParam(CO_Data* d, int index)
{
  int n = d->eeprom_size;
  uint32 datap;
  uint32 error_code;
  T_EepromIndexes indexes;
  uint32 size;
  uint16 i;
  bool1 res;
  if (index < n) {
    indexes = d->eeprom_index[index];
    size = indexes.size;
    error_code = _setODentry(d, indexes.index, indexes.subindex, &datap, &size, 0, 1, SET_OD_ENTRY_MODE_1);
    if ((error_code == OD_SUCCESSFUL)) {
      i = 0;
      res = FALSE;
      while (i < 3 && !res) {
        res = I2C_Command(MEM_READ, (char*)datap, indexes.size, indexes.address);
        if (!res){
          TSK_sleep(30);
        }
        ++i;
      }
      if (!res)
        error_code = SDOABT_LOCAL_ERROR;
      else{
        error_code = _setODentry(d, indexes.index, indexes.subindex, &datap, &size, 0, 1, SET_OD_ENTRY_MODE_1);//recall it for sign extension for int8
      }
    }
  }
  else
    error_code = SDOABT_LOCAL_ERROR;
  return (error_code);
}


/**
 * @ingroup
 * @fn bool1 PAR_ReadAllPermanentParam(void)
 * @brief Read all the permanent parameters from the EEPROM
 * and store it in the object dictionary
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_ReadAllPermanentParam(CO_Data* d)
{
  int n = d->eeprom_size;
  int index = 0;
  uint32 error_code = OD_SUCCESSFUL;
  while((index<n) && (error_code == OD_SUCCESSFUL)){
    error_code = PAR_ReadPermanentParam(d, index++);
  }
  return (error_code == OD_SUCCESSFUL);
}

/**
 * @ingroup
 * @fn uint32 WritePermanentParam(CO_Data* d, int index)
 * @brief Write one parameter from the object dictionary
 * in EEPROM
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @param
 * - type:int
 * - range: 0..EEPROM_INDEXES_MAX-1
 * - description: index of the parameter in the EEPROM array
 * @return
 * - type:unsigned long
 * - range: OD_SUCCESSFUL if success, other value if failed
 * - description: error code
 */
uint32 WritePermanentParam(CO_Data* d, int index)
{
  T_EepromIndexes indexes;
  uint32 datap;
  uint32 size;
  uint32 error_code;
  bool1 res;
  uint8 data_type;

  if (index < d->eeprom_size) {
    indexes = d->eeprom_index[index];
    size = indexes.size;
    error_code = _getODentry(d, indexes.index, indexes.subindex, &datap, &size, &data_type, 0, 1, GET_OD_ENTRY_MODE_4);
    //old GET_OD_ENTRY_MODE_1
    if (error_code == OD_SUCCESSFUL) {
      res = I2C_Command(MEM_WRITE, (char*)datap, indexes.size, indexes.address);
      if (!res)//removed retrys because they are already included in I2C_Command.
        error_code = SDOABT_LOCAL_ERROR;
    }
  }
  else
    error_code = SDOABT_OUT_OF_MEMORY;
  return (error_code);
}


uint32 PAR_WriteAllPermanentParam(CO_Data* d){
  int n = d->eeprom_size;
  int index = 0;
  uint32 error_code = OD_SUCCESSFUL;

  while((index<n) && (error_code == OD_SUCCESSFUL)){
    error_code = WritePermanentParam(d,index++);
  }
  return(error_code);
}

////////////////////////////////////////////////////////////
/// @ingroup param
/// @fn uint32 WriteStatisticParam(int16 index)
/// @brief write statistics page in special part of eeprom
/// @param index
/// - type: short
/// - range: [-PAR_STAT_PAGE; PAR_STAT_PAGE-1]
/// - description: it is the index of page in PAR_EEPROM_INDEXES
/// @return error_code
/// - type: unsigned long
/// - range: [0;]
/// - description: OD_SUCCESSFUL if ok, error code otherwise
////////////////////////////////////////////////////////////
uint32 WriteStatisticParam(int16 index){
  bool1 res;
  uint32 error_code = SDOABT_LOCAL_ERROR;
  uint16 buff[EEPROM_PAGESIZE/2];
  uint16* pdata;
  int16 i, j, k;
  if (index < 0) index = -index - 1;
  if (index < PAR_STAT_PAGE){
    i=0;k=0;
    while (i<PAR_EEPROM_INDEXES[index].nb){
      pdata = PAR_EEPROM_INDEXES[index].indexes[i].pdata;
      j=0;
      while (j<PAR_EEPROM_INDEXES[index].indexes[i].size){
        buff[k++] = *pdata++;
        j+=2;
      }
      i++;
    }
    res = I2C_Command(MEM_WRITE, (char*)buff, PAR_EEPROM_INDEXES[index].size,
                      PAR_EEPROM_INDEXES[index].address);
    if (res) error_code = OD_SUCCESSFUL;
    PAR_StatEepromCommandState[index] = EEPROM_COMMAND_TERMINATED;
  }
  return (error_code);
}

uint32 PAR_WriteStatisticParam(int16 index){
  return MBX_post(&mailboxWriteParameters, &index,0);
}

////////////////////////////////////////////////////////////
/// @ingroup param
/// @fn uint32 ReadAllStatisticParam(void)
/// @brief read all statistics var in special part of eeprom
/// @return res
/// - type: boolean
/// - description: TRUE if ok, FALSE if not
////////////////////////////////////////////////////////////
bool1 PAR_ReadAllStatisticParam(void){
  bool1 res = TRUE;
  uint16 index = 0;
  uint16 buff[EEPROM_PAGESIZE/2];
  uint16* pdata;
  int16 i, j, k;
  while (index < PAR_STAT_PAGE && res){
    res = I2C_Command(MEM_READ, (char*)buff, PAR_EEPROM_INDEXES[index].size,
                      PAR_EEPROM_INDEXES[index].address);
    i=0;k=0;
    while (i<PAR_EEPROM_INDEXES[index].nb){
      pdata = PAR_EEPROM_INDEXES[index].indexes[i].pdata;
      j=0;
      while (j<PAR_EEPROM_INDEXES[index].indexes[i].size){
        *pdata++ = buff[k++];
        j+=2;
      }
      i++;
    }
    index++;
  }
  return (res);
}


/**
 * @ingroup
 * @fn bool1 PAR_FindODPermanentParamIndex(CO_Data* d, uint16 index, uint8 subindex,
 * ,uint16 *array_index)
 * @brief Find the index of an object dictionary parameter in the EEPROM array
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @param
 * - type:unsigned short
 * - range: 0..0xFFFF
 * - description: object dictionary index
 * @param
 * - type: unsigned char
 * - range: 0..0xFF
 * - description: object dictionary subindex
 * @param
 * - type: unsigned short*
 * - range:
 * - description: index of the parameter
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_FindODPermanentParamIndex(CO_Data* d, uint16 index, uint8 subindex,uint16 *array_index)
{
  uint16 n = d->eeprom_size;
  uint16 i = 0;
  bool1 result = FALSE;
  T_EepromIndexes indexes;

  while ((i < n) && (!result)) {
    indexes = d->eeprom_index[i];
    result = (indexes.index == index) && (indexes.subindex == subindex);
    if (result)
      *array_index = i;
    else
      ++i;
  }
  return (result);
}

/**
 * @ingroup
 * @fn bool1 PAR_FindODPermanentParamIndex(CO_Data* d, uint16 index, uint8 subindex,
 * ,uint16 *array_index)
 * @brief Find the index of an object dictionary parameter in the EEPROM array
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @param
 * - type:uint16
 * - range: 0..0xFFFF
 * - description: eeprom address
 * @param
 * - type: unsigned short*
 * - range:
 * - description: index of the parameter
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_FindODPermanentParamIndex2(CO_Data* d, uint16 address,uint16 *array_index)
{
  uint16 n = d->eeprom_size;
  uint16 i = 0;
  bool1 result = FALSE;
  T_EepromIndexes indexes;

  while ((i < n) && (!result)) {
    indexes = d->eeprom_index[i];
    result = indexes.address == address;
    if (result)
      *array_index = i;
    else
      ++i;
  }
  return (result);
}



/**
 * @ingroup
 * @fn bool1 PAR_GetEepromIndexes(CO_Data* d,uint16 array_index,T_EepromIndexes *indexes){
 * @brief Get the eeprom indexes from the index array_index
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
 * @param
 * - type: unsigned short
 * - range: 0..eeprom index size
 * - description: array index
 * @param
 * - type:CO_Data*, unsigned short, T_EepromIndexes*
 * - range:
 * - description: eeprom indexes
 * @return
 * - type:bool1
 * - range: true(success), false(failed)
 * - description: command result
 */
bool1 PAR_GetEepromIndexes(CO_Data* d, uint16 array_index,T_EepromIndexes *indexes){

  uint16 n = d->eeprom_size;
  bool1 result;
  result = array_index <n;
  if(result){
    *indexes = d->eeprom_index[array_index];
  }
  return(result);
}


/**
 * @ingroup
 * @fn uint32 PAR_StoreODSubIndex(CO_Data* d, uint16 wIndex, uint8 bSubindex)
 * @brief Function that is used by the object dictionary
 * to save a parameter.
 * The function write the index of the parameter in a mailbox
 * and a task must check the mailbox to write the eeprom
 * @param
 * - type:CO_Data*
 * - range:
 * - description: pointer on the object dictionary
  * @param
 * - type: unsigned short
 * - range:
 * - description: object dictionary index
 * @param
 * - type: unsigned char
 * - range:
 * - description: object dictionary subindex
 * @return
 * - type:unsigned long
 * - range: OD_SUCCESSFUL if success, other value if failed
 * - description: error code
 */
uint32 PAR_StoreODSubIndex(CO_Data* d, uint16 wIndex, uint8 bSubindex)
{
  uint16 array_index;

  if (PAR_FindODPermanentParamIndex(d, wIndex, bSubindex, &array_index)) {
    if (MBX_post(&mailboxWriteParameters, &array_index,0))
      return (OD_SUCCESSFUL);
    else
      return (SDOABT_OUT_OF_MEMORY);
  }
  else
    return (OD_NO_SUCH_OBJECT);
}

/**
 * @ingroup
 * @fn void TaskStoreParam(void)
 * @brief Task to write the parameters in EEPROM
 * Check the mailboxWriteParameters to know if
 * new parameters must be written.
 * @param
 * - type:void
 * - range:
 * - description:
 * @return
 * - type:void
 * - range:
 * - description:
 */
void TaskStoreParam(void)
{
  int16 index;
  uint16 indexcrc;
  uint32 error_code;
  bool1 crc_to_be_computed = FALSE;
  canOpenInit();//need to be done here for next line to be valid
  if (!PAR_FindODPermanentParamIndex(BoardODdata, ODA_PAR_CrcParameters, &indexcrc)) indexcrc=1;
  while(1){
    if (MBX_pend(&mailboxWriteParameters, &index, 10)){
      error_code = OD_SUCCESSFUL;
      if (index == indexcrc) crc_to_be_computed = TRUE;
      else if (index != indexcrc && index >= 0){//if not CrcParameters
        error_code = WritePermanentParam(BoardODdata, index);
        if (error_code == OD_SUCCESSFUL)
          crc_to_be_computed = TRUE;
        else crc_to_be_computed = FALSE;
      }
      else if (index < 0){
        error_code = WriteStatisticParam(index);
      }
    }
    else if (crc_to_be_computed){
      crc_to_be_computed = FALSE;
      ODP_CrcParameters = ComputeParamCRC();
      WritePermanentParam(BoardODdata, indexcrc);//1 = CrcParameters
    }
  }
}

extern UNS16 gateway_dict_obj100A;
extern UNS32 gateway_dict_obj1018_Revision_Number;
extern UNS32 gateway_dict_obj1018_Serial_Number;
extern UNS32 gateway_dict_obj1018_Vendor_ID;
extern UNS32 gateway_dict_obj1018_Product_Code;

void PAR_SetParamDependantVars(void)
/* update all variables that depends on the system parameters */
{
  //CNV_CurrentUnit en [mA/incAD]
  gateway_dict_obj100A = HW_VERSION;
  ODV_Version = NGTEST_VERSION;
  gateway_dict_obj1018_Vendor_ID = USB_VID_PHILIPS;
  gateway_dict_obj1018_Product_Code = USB_PID_ELSIUM;
  if ((ODP_Board_RevisionNumber == 0) || (ODP_Board_RevisionNumber > 0x68))
  {
	  ODP_Board_RevisionNumber = 0x63;
	  ODP_Board_BaudRate = 0xFA;
  }
  gateway_dict_obj1018_Revision_Number = ODP_Board_RevisionNumber;
  gateway_dict_obj1018_Serial_Number = ODP_Board_SerialNumber;
  PAR_Capacity_Total = CNV_Round(ODP_Battery_Capacity*3600*CUR_SCALE); //in 0.250*1ms

  CNV_CurrentUnit = 1.0;
  CNV_CurrentRange= CUR_AD_RESOL/2;
}

