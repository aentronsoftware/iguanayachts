/*
 * lcd.h
 *
 *  Created on: 23 nov. 2015
 *      Author: remip
 */

#ifndef LCD_H_
#define LCD_H_

void LCD_Start(void);
void LCD_WriteText(uint16 line,char* text,uint16 lenght);
void LCD_ReadText(uint16 pos,char* text,uint16 lenght);
void LCD_WriteInteger(uint16 pos,uint64* text,uint16 lenght,bool1 signe);
void LCD_ReadInteger(uint16 pos,uint64* text,uint16 lenght);
void LCD_SetPosition(uint16 pos, bool1 blink);

#endif /* LCD_H_ */
