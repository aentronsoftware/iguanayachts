/**********************************************************************

   i2c.c - gestion de l'i2c

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   :
   Compil: TI CCS5.1
   Target: PTM054
   descr.: function for accessing powerman and the eeprom
   Edit  :

 *********************************************************************/

#include "processor.h"
#ifdef PROC_DSP
  #include "gatewaycfg.h"    //must be include before uC.h
  #include "uc.h"
  #include <clk.h>
  #include "dspspecs.h"
  #include "powerman.h"
  #include "gateway_dict.h"
  #include "F2806x_I2c_defines.h"
  #include "i2c.h"
  //SysBios to DSPBios translation macros
  #define Mailbox_post(mbox, msg, t)   MBX_post(&mbox, msg, t)
  #define Mailbox_pend(mbox, msg, t)   MBX_pend(&mbox, msg, t)
  #define Semaphore_post(sem)          SEM_post(&sem)
  #define Semaphore_pend(sem,t)        SEM_pend(&sem, t)
  #define Task_sleep(t)                TSK_sleep(t)
  #define Task_exit()                  TSK_exit(t)
  #define BIOS_WAIT_FOREVER            SYS_FOREVER
  #define Clock_getTicks()             CLK_getltime()
#endif

uint16   EepOddAdr = 0;  //to know if address is odd
uint16   EepState = 0;   //e2prom interrupt state machine

uint16 EepSetup(uint16 device_code, uint16 address,uint16 nb, uint16 address_size);
uint16 EepPageWrite(uint16 device_code, uint16 address, uint16 pagenb, uint16* buffer, uint16 address_size);

void I2C_Init(void){
  // Initialize I2C
  I2caRegs.I2CMDR.all = 0;      // reset i2c
  I2caRegs.I2CFFTX.all = 0;     // tx fifo reset
  I2caRegs.I2CFFRX.all = 0;     // rx fifo reset
  I2caRegs.I2CSAR = 0x0050;     // Slave address - EEPROM control code
  I2caRegs.I2CPSC.all = 3;      // Prescaler - need 7-12 Mhz on module clk 10MHz=sysclk/(I2CPSC+1)
  I2caRegs.I2CCLKL = 55;        // NOTE: must be non zero 55
  I2caRegs.I2CCLKH = 50;        // NOTE: must be non zero I2CCLK=10MHz/(I2CCLKL+I2CCLKH+10)
  I2caRegs.I2CIER.all = 0x00;   // Enable ARDY interrupts 0x24
  I2caRegs.I2CMDR.all = 0x0020; // Take I2C out of reset
                                // Stop I2C when suspended
  I2caRegs.I2CFFTX.all = 0x6000;// Enable FIFO mode and TXFIFO
  I2caRegs.I2CFFRX.all = 0x2040;// Enable RXFIFO, clear RXFFINT
}


interrupt void I2C_INT1A(void)     // I2C-A
{
  uint16 IntSource;// i, len;
  //uint16* buffer;
  // Read interrupt source
  IntSource = I2caRegs.I2CISRC.all;
  // Interrupt source = stop condition detected
  if(IntSource == I2C_SCD_ISRC)
  {
    if (EepState == I2C_SEND_ADDRESS)
    {//eeprom is now programming the page must wait 10ms before new data
      EepState = I2C_WRITE_BUSY;
    }
  }
  // Interrupt source = Register Access Ready
  // This interrupt is used to determine when the EEPROM address setup portion of the
  // read data communication is complete. Since no stop bit is commanded, this flag
  // tells us when the message has been sent instead of the SCD flag. If a NACK is
  // received, clear the NACK bit and command a stop. Otherwise, move on to the read
  // data portion of the communication.
  //else
  /*if(IntSource == I2C_ARDY_ISRC)
  {
    if(I2caRegs.I2CSTR.bit.NACK == 1)
    {
       //I2caRegs.I2CMDR.bit.STP = 1;
       I2caRegs.I2CSTR.all = I2C_CLR_NACK_BIT;
       EepState = I2C_NACK_ERROR;
    }
    else if (EepState == I2C_SEND_NOSTOP_BUSY)
      EepState = I2C_SEND_NOSTOP;
  }*/
  // Enable future I2C (PIE Group 8) interrupts
  PieCtrlRegs.PIEACK.all = PIEACK_GROUP8;
}

uint16 EepSetup(uint16 device_code, uint16 address,uint16 nb, uint16 address_size)
{
  // Wait until the STP bit is cleared from any previous master communication.
  // Clearing of this bit by the module is delayed until after the SCD bit is
  // set. If this bit is not checked prior to initiating a new message, the
  // I2C could get confused.
  if (EepState != I2C_SUCCESS){
    EALLOW;
    I2caRegs.I2CCNT = 0;
    I2caRegs.I2CMDR.all = 0;      // reset i2c
    I2caRegs.I2CFFTX.bit.TXFFRST = 0;//reset tx fifo
    I2caRegs.I2CFFRX.bit.RXFFRST = 0;//reset rx fifo
    GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 0;//GPIO
    GpioDataRegs.GPBDAT.bit.GPIO33 = 0;
    EDIS;
    DELAY_US(100);
    GpioDataRegs.GPBDAT.bit.GPIO33 = 1;
    DELAY_US(100);
    EALLOW;
    GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 1;//I2C
    EDIS;
    //reinit i2c
    I2caRegs.I2CMDR.bit.IRS = 1; // remove reset
    I2caRegs.I2CFFTX.bit.TXFFRST = 1;//remove tx fifo reset condition
    I2caRegs.I2CFFRX.bit.RXFFRST = 1;//remove rx fifo reset condition
    I2caRegs.I2CMDR.bit.STP = 0;     //remove Stop condition
  }
  if (I2caRegs.I2CMDR.bit.STP == 1)
  {
    I2caRegs.I2CMDR.bit.STP = 0;     //remove Stop condition
    return I2C_STP_NOT_READY_ERROR;
  }
  if (address+nb > EEPROM_SIZE) return(I2C_ERROR);
  // Setup slave address
  /* the 2 most significant bits of the address (bit 8 and 9) must be writen
     on the bit 1 and 2 from the device selection byte
     the low byte of the address must be written into the address byte
     -> (high byte and 0000 0011) shift one step to left */
  if (address_size == 2){
    I2caRegs.I2CSAR = (device_code>>1);
  }
  else
    I2caRegs.I2CSAR = (device_code>>1) | ((address>>8) & 0x03);
  // Check if bus busy
  if (I2caRegs.I2CSTR.bit.BB == 1)
  {
     return I2C_BUS_BUSY_ERROR;
  }
  // Setup number of bytes to send
  // MsgBuffer + Address
  I2caRegs.I2CCNT = nb+address_size;

  // Setup data to send
  if(address_size == 2){
    I2caRegs.I2CDXR = (address>>8);
  }
  I2caRegs.I2CDXR = address & 0xFF;
  return I2C_SUCCESS;
}

#define EEP_TIMEOUT  10
#define ADS_TIMEOUT   2

bool1 I2C_EepRead(uint16 device_code, uint16* buffer, uint16 nb, uint16 address)
{
  uint16 res;
  Uint32 ftime, sample_time;
  uint16 wait_nb;
  res = EepSetup(device_code,address,0,2);
  if (res == I2C_SUCCESS)
  {
    // Send start as master transmitter without stop
    I2caRegs.I2CMDR.all = 0x6620; // 0x6(FREE+STT) 6(MST+TRX) 2 (IRS) 0
    //wait address sent
    //EepState = I2C_SEND_NOSTOP_BUSY;
    ftime = Clock_getTicks();
    do {
      sample_time = Clock_getTicks();
      res = I2caRegs.I2CSTR.bit.ARDY;//EepState;
      if (res == 0){
        if ((sample_time - ftime)>EEP_TIMEOUT){
          res = I2C_ERROR;
        }
      }
    } while(res == 0);
  }
  if (res == 1)
  {
    I2caRegs.I2CSTR.bit.ARDY = 1;
    res = I2C_SUCCESS;
    if (address+nb > EEPROM_SIZE) return(I2C_ERROR);
    I2caRegs.I2CCNT = nb;
    // Send start as master receiver with stop
    I2caRegs.I2CMDR.all = 0x6C20;// 0x6(FREE+STT) C(STP+MST) 2 (IRS) 0
    ftime = Clock_getTicks();
    while (nb>0)
    {
      if (nb >= 2) wait_nb = 2; else wait_nb = nb;
      if (I2caRegs.I2CFFRX.bit.RXFFST>= wait_nb)
      {
      ftime = Clock_getTicks();
        (*buffer) = I2caRegs.I2CDRR;
        if (wait_nb>1) (*buffer) += I2caRegs.I2CDRR<<8;
        ++buffer;
        nb -= wait_nb;
      }
      if ((Clock_getTicks() - ftime)>EEP_TIMEOUT)
      {res = I2C_ERROR; nb=0;}
    }
  }
  EepState = res;
  return(res == I2C_SUCCESS);
}


bool1 I2C_AdsRead(uint16 device_code, uint16* buffer, uint16 nb, uint16 address)
{
  uint16 res;
  Uint32 ftime, sample_time;
  uint16 wait_nb;
  res = EepSetup(device_code,address,0,1);

  if (res == I2C_SUCCESS)
  {
    // Send start as master transmitter without stop
    I2caRegs.I2CMDR.all = 0x6620; // 0x6(FREE+STT) 6(MST+TRX) 2 (IRS) 0
    //wait address sent
    //while (I2caRegs.I2CSTR.bit.XSMT==1);
    //EepState = I2C_SEND_NOSTOP_BUSY;
    ftime = Clock_getTicks();
    do {
      sample_time = Clock_getTicks();
      res=I2caRegs.I2CSTR.bit.ARDY;
      if (res == 0){
        if ((sample_time - ftime)>ADS_TIMEOUT){
          res = I2C_ERROR;
        }
      }
    }while(res == 0);
  }

  if (res == 1)
  {
    I2caRegs.I2CSTR.bit.ARDY = 1;
    res = I2C_SUCCESS;
    I2caRegs.I2CCNT = nb;
    // Send start as master receiver with stop
    I2caRegs.I2CMDR.all = 0x6C20;// 0x6(FREE+STT) C(STP+MST) 2 (IRS) 0
    ftime = Clock_getTicks();
    while (nb>0)
    {
      if (nb >= 2) wait_nb = 2; else wait_nb = nb;
      if (I2caRegs.I2CFFRX.bit.RXFFST>= wait_nb)
      {
        ftime = Clock_getTicks();
        (*buffer) = I2caRegs.I2CDRR;
        if (wait_nb>1) (*buffer) += I2caRegs.I2CDRR<<8;
        ++buffer;
        nb -= wait_nb;
      }
      if ((Clock_getTicks() - ftime)>ADS_TIMEOUT){
        res = I2C_ERROR; break;
      }
    }
  }
  EepState = res;
  return(res == I2C_SUCCESS);
}



bool1 I2C_EepWrite(uint16 device_code,uint16* buffer,uint16 nb,uint16 address)
{
  uint16 res, pagenb;
  EepOddAdr = FALSE;
  do
  {
    pagenb = EEPROM_PAGESIZE - (address & (EEPROM_PAGESIZE-1));
    if (pagenb > nb) pagenb = nb;
    res = EepPageWrite(device_code,address,pagenb,buffer,2);
    if (res == I2C_SUCCESS)
      TSK_sleep(11);
    else
      return(FALSE);
    address += pagenb;
    buffer += (pagenb>>1);
    nb -= pagenb;
  } while(nb >0);
  return(res == I2C_SUCCESS);
}

bool1 I2C_AdsWrite(uint16 device_code,uint16* buffer,uint16 nb,uint16 address)
{
  uint16 res, wait_nb;
  Uint32 ftime;

  res = EepSetup(device_code,address,nb,1);
  if (res == I2C_SUCCESS)
  {
    // Send start as master transmitter with stop
    I2caRegs.I2CSTR.bit.SCD = 1;
    I2caRegs.I2CMDR.all = 0x6E20;
    ftime = Clock_getTicks();
    while (I2caRegs.I2CSTR.bit.SCD == 0){
      if (nb >= 2) wait_nb = 2; else wait_nb = nb;
      if ((I2caRegs.I2CFFTX.bit.TXFFST <= 2) && (nb>0))
      {
        ftime = Clock_getTicks();
        I2caRegs.I2CDXR = (*buffer) & 0xFF;
        if (wait_nb>1){
          I2caRegs.I2CDXR = (*buffer)>>8;
        }
        ++buffer;
        nb -= wait_nb;
      }
      if ((Clock_getTicks() - ftime)>ADS_TIMEOUT) {
        res = I2C_ERROR; break;
      }
      if (I2caRegs.I2CSTR.bit.NACK || I2caRegs.I2CSTR.bit.ARBL) {
        I2caRegs.I2CSTR.bit.NACK = 1;
        I2caRegs.I2CSTR.bit.ARBL = 1;
        res = I2C_NACK_ERROR; break;
      }
    }
  }
  EepState = res;
  return(res == I2C_SUCCESS);
}



uint16 EepPageWrite(uint16 device_code, uint16 address, uint16 pagenb, uint16* buffer, uint16 address_size)
{
  uint16 res, wait_nb;
  Uint32 ftime;
  res = EepSetup(device_code,address,pagenb,address_size);
  if (res == I2C_SUCCESS)
  {
    // Send start as master transmitter with stop
    I2caRegs.I2CSTR.bit.SCD = 1;
    I2caRegs.I2CMDR.all = 0x6E20;
    //wait address sent
    ftime = Clock_getTicks();
    while (I2caRegs.I2CSTR.bit.XSMT==1) {
      if ((Clock_getTicks() - ftime)>EEP_TIMEOUT){
        res = I2C_ERROR; break;
      }
      if (I2caRegs.I2CSTR.bit.NACK) {
        res = I2C_NACK_ERROR;
        I2caRegs.I2CSTR.bit.NACK = 1;//clear nack
        break;
      }
    }
  }
  if (res == I2C_SUCCESS) {
    if (EepOddAdr) {
      I2caRegs.I2CDXR = (*buffer)>>8;
      ++buffer;
      --pagenb;
      EepOddAdr = FALSE;
    }
    ftime = Clock_getTicks();
    while (I2caRegs.I2CSTR.bit.SCD == 0)
    {
      if (pagenb >= 2) wait_nb = 2; else wait_nb = pagenb;
      if ((I2caRegs.I2CFFTX.bit.TXFFST <= 2) && (pagenb > 0))
      {
        ftime = Clock_getTicks();
        I2caRegs.I2CDXR = (*buffer) & 0xFF;
        if (wait_nb>1){
          I2caRegs.I2CDXR = (*buffer)>>8;
        }
        else EepOddAdr = TRUE;
        ++buffer;
        pagenb -= wait_nb;
      }
      if ((Clock_getTicks() - ftime)>EEP_TIMEOUT){
        res = I2C_ERROR; break;
      }
      if (I2caRegs.I2CSTR.bit.NACK || I2caRegs.I2CSTR.bit.ARBL) {
        I2caRegs.I2CSTR.bit.NACK = 1;
        I2caRegs.I2CSTR.bit.ARBL = 1;
        res = I2C_NACK_ERROR; break;
      }
    }
  }
  EepState = res;
  return(res);
}

/*
 * I2C_Command
 */
#define I2C_COMMAND_RETRY 3
bool1 I2C_Command(uint16 cmd, char * data, uint16 length, uint16 address)
{
  bool1 res = FALSE;
  uint32 start;
  int8 i = 0, retry = I2C_COMMAND_RETRY;
  TI2C_mailbox i2cmsg;
  uint8 statusi2c = I2C_PENDING;

  i2cmsg.status = &statusi2c;
  i2cmsg.data = (uint8 *)data;
  i2cmsg.length = length;
  i2cmsg.address = address;
  switch (cmd) {
    case MEM_WRITE:
      i2cmsg.devsel = EEPROM_DEVICECODE;
      i2cmsg.operation = I2C_WRITE;
      break;
    case MEM_READ:
      i2cmsg.devsel = EEPROM_DEVICECODE;
      i2cmsg.operation = I2C_READ;
      break;
    case POW_READ:
      i2cmsg.devsel = POWERMAN_DEVICECODE;
      i2cmsg.operation = I2C_READ;
      break;
    case POW_WRITE:
      i2cmsg.devsel = POWERMAN_DEVICECODE;
      i2cmsg.operation = I2C_WRITE;
      break;
    case ADS_READ:
      i2cmsg.devsel = ADS1015_DEVICECODE;
      i2cmsg.operation = I2C_READ;
      break;
    case ADS_WRITE:
      i2cmsg.devsel = ADS1015_DEVICECODE;
      i2cmsg.operation = I2C_WRITE;
      break;
    case LCD_WRITE:
      i2cmsg.devsel = PCF2116_DEVICECODE;
      i2cmsg.operation = I2C_WRITE;
      break;
    case LCD_READ:
      i2cmsg.devsel = PCF2116_DEVICECODE;
      i2cmsg.operation = I2C_READ;
      retry = 1;
      break;
    case RTC_WRITE:
      i2cmsg.devsel = DS1307_DEVICECODE;
      i2cmsg.operation = I2C_WRITE;
      break;
    case RTC_READ:
      i2cmsg.devsel = DS1307_DEVICECODE;
      i2cmsg.operation = I2C_READ;
      break;
  }
  while ((i<retry) && !res){
    start = Clock_getTicks();
    if (Mailbox_post(mailboxI2C_SendAndReceive, &i2cmsg, I2C_POST_TIMEOUT)){
      while ((statusi2c <= I2C_PROCESSING) && (Clock_getTicks() - start < I2C_RESULT_TIMEOUT)){
        Task_sleep(1);
      }
      if ((statusi2c == I2C_READ_OK) || (statusi2c == I2C_VERIFY_OK) || (statusi2c == I2C_WRITE_OK))
        res = TRUE;
    }
    ++i;
    if (!res)
      Task_sleep(2);
  }
  return(res);
}


/**
 * @ingroup
 * @fn void taskFnI2C_SendAndReceive(void)
 * @brief Task to handle all i2c communications
 * Check the mailboxI2C_SendAndReceive to know if
 * new communication must be done.
 * @param
 * - type:void
 * - range:
 * - description:
 * @return
 * - type:void
 * - range:
 * - description:
 */
#define ERROR_COUNT_MAX    1
#define MAX_DATA_LENGTH   64
void taskFnI2C_SendAndReceive(void){
  TI2C_mailbox i2c_mail;
  uint8 data[MAX_DATA_LENGTH];
  int8 i, error_count;
  bool1 res;
  while(1){
    Mailbox_pend(mailboxI2C_SendAndReceive, &i2c_mail, BIOS_WAIT_FOREVER); /* wait for a message in i2c mailbox */
    *i2c_mail.status = I2C_PROCESSING;
    error_count = 0;
    res = FALSE;
    switch (i2c_mail.operation) {
      case I2C_WRITE:
        while ((!res) && (error_count < ERROR_COUNT_MAX)){
          if(i2c_mail.devsel == EEPROM_DEVICECODE){
            res = I2C_EepWrite(i2c_mail.devsel,(uint16*)i2c_mail.data,i2c_mail.length,i2c_mail.address);
          }
          else{
            res = I2C_AdsWrite(i2c_mail.devsel,(uint16*)i2c_mail.data,i2c_mail.length,i2c_mail.address);
          }
          if (!res){
            ++error_count;
            //Task_sleep(1);
          }
        }
        if (res) *i2c_mail.status = I2C_WRITE_OK;
        else *i2c_mail.status = I2C_WRITE_FAILED;
        break;
      case I2C_READ:
        while ((!res) && (error_count < ERROR_COUNT_MAX)){
          if(i2c_mail.devsel == EEPROM_DEVICECODE){
            res = I2C_EepRead(i2c_mail.devsel,(uint16*)i2c_mail.data,i2c_mail.length,i2c_mail.address);
          }
          else{
            res = I2C_AdsRead(i2c_mail.devsel,(uint16*)i2c_mail.data,i2c_mail.length,i2c_mail.address);
          }
          if (!res){
            ++error_count;
            //Task_sleep(1);
          }
        }
        if (res) *i2c_mail.status = I2C_READ_OK;
        else *i2c_mail.status = I2C_READ_FAILED;
        break;
      case I2C_WRITE_VERIFY:
        while ((!res) && (error_count < ERROR_COUNT_MAX)){
          res = I2C_EepWrite(i2c_mail.devsel,(uint16*)i2c_mail.data,i2c_mail.length,i2c_mail.address);
          if (!res){
            ++error_count;
            Task_sleep(1);
          }
        }
        if (res){
          res = FALSE;
          while ((!res) && (error_count < ERROR_COUNT_MAX)){
            res = I2C_EepRead(i2c_mail.devsel,(uint16*)data,i2c_mail.length,i2c_mail.address);
            if (!res){
              ++error_count;
              Task_sleep(1);
            }
          }
          if (res){
            *i2c_mail.status = I2C_VERIFY_OK;
            for (i=0;i<i2c_mail.length;i++){//length is in byte
              if ((data[i/2] & 0xFF) != (*i2c_mail.data & 0xFF)){
                *i2c_mail.status = I2C_VERIFY_FAILED;
                break;
              }
              if (++i<i2c_mail.length){
                if ((data[i/2] & 0xFF00) != (*i2c_mail.data & 0xFF00)){
                  *i2c_mail.status = I2C_VERIFY_FAILED;
                  break;
                }
              }
              *i2c_mail.data++;
            }
          }
          else *i2c_mail.status = I2C_VERIFY_FAILED;
        }
        else *i2c_mail.status = I2C_WRITE_FAILED;
        break;
    }
    DELAY_US(50);
    //Task_sleep(1);
  }
}


